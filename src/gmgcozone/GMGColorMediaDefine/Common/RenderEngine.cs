﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web.UI;
using Amazon.ElasticLoadBalancing.Model;
using Amazon.IdentityManagement.Model;
using GMG.CoZone.InteropWrapper;

namespace GMGColorMediaDefine.Common
{
    # region Render Classes

    public unsafe class RenderEngineInstance : RenderEngineObj
    {
        #region Delegates
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void PDFErrorCB(UInt16 errGroup, UInt16 errID, string errorDescription, void* userRef);
        #endregion

        #region PInvokes
        //class AOI_Instance * AOI_Instance::Startup(char const *,char const *)        
        [DllImport(Settings.DllName32, EntryPoint = "?Startup@RenderInstance@@SAXPBD00@Z", CallingConvention = CallingConvention.Cdecl)]
        private static extern HInstance* _Startup32(string uniqueID, string licenseKey, string signature);
        [DllImport(Settings.DllName64, EntryPoint = "?Startup@RenderInstance@@SAXPEBD00@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern HInstance* _Startup64(string uniqueID, string licenseKey, string signature);

        //AOI_Instance::AOI_Instance()
        [DllImport(Settings.DllName32, EntryPoint = "??0RenderInstance@@QAE@_NM@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctor32(HInstance* instance, bool useFontAntiAliasing, float greekSize);
        [DllImport(Settings.DllName64, EntryPoint = "??0RenderInstance@@QEAA@_NM@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctor64(HInstance* instance, bool useFontAntiAliasing, float greekSize);

        //AOI_Instance::~AOI_Instance(void)
        [DllImport(Settings.DllName32, EntryPoint = "??1RenderInstance@@QAE@XZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern void dtor32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "??1RenderInstance@@QEAA@XZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern void dtor64(HInstance* instance);

        //void AOI_Instance::Terminate(void)
        [DllImport(Settings.DllName32, EntryPoint = "?Terminate@RenderInstance@@SAXXZ")]
        private static extern void _Terminate32();
        [DllImport(Settings.DllName64, EntryPoint = "?Terminate@RenderInstance@@SAXXZ")]
        private static extern void _Terminate64();

        #endregion

        #region Members
        private PDFErrorCB mErrorHandler = null;
        #endregion

        #region Internal
        protected override int instanceSize()
        {
            return sizeof(HInstance) + sizeof(IntPtr);
        }
        #endregion

        #region Constructors
        protected internal RenderEngineInstance(HInstance* instance)
            : base(instance)
        {
        }
        #endregion

        #region Methods
        public static RenderEngineInstance Startup(string uniqueID, string licenseKey, string signature)
        {
            HInstance* instance;
            if (Is32Bit())
                instance = _Startup32(uniqueID, licenseKey, signature);
            else
                instance = _Startup64(uniqueID, licenseKey, signature);
            return new RenderEngineInstance(instance);
        }

        public RenderEngineInstance(bool useFontAntiAliasing, float greekSize)
            : base(true)
        {
            if (Is32Bit())
                ctor32(mInstance, useFontAntiAliasing, greekSize);
            else
                ctor64(mInstance, useFontAntiAliasing, greekSize);
        }

        public override void Dispose()
        {
            if (Is32Bit())
                dtor32(mInstance);
            else
                dtor64(mInstance);
            base.Dispose();
        }

        public static void Terminate()
        {
            if (Is32Bit())
                _Terminate32();
            else
                _Terminate64();
        }

        public PDFErrorCB ErrorHandler()
        {
            return mErrorHandler;
        }

        #endregion
    };

    public unsafe class SoftProofRenderer : RenderEngineObj
    {
        #region PInvokes

        //AOI_SoftProofRenderer::AOI_SoftProofRenderer(AOI_Document)
        [DllImport(Settings.DllName32, EntryPoint = "??0SoftProofRenderer@@QAE@PAVRenderInstance@@PBG_N@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern int ctor32(HInstance* instance, HInstance* aoiDocument, string documentPath, bool doRendering);
        [DllImport(Settings.DllName64, EntryPoint = "??0SoftProofRenderer@@QEAA@PEAVRenderInstance@@PEBG_N@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern int ctor64(HInstance* instance, HInstance* aoiDocument, string documentPath, bool doRendering);

        //AOI_SoftProofRenderer::~AOI_SoftProofRenderer(void)
        [DllImport(Settings.DllName32, EntryPoint = "??1SoftProofRenderer@@QAE@XZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern void dtor32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "??1SoftProofRenderer@@QEAA@XZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern void dtor64(HInstance* instance);

        [DllImport(Settings.DllName32, EntryPoint = "?InitRenderer@SoftProofRenderer@@SA_NXZ", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool _InitRenderer32();
        [DllImport(Settings.DllName64, EntryPoint = "?InitRenderer@SoftProofRenderer@@SA_NXZ", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool _InitRenderer64();

        //bool AOI_SoftProofRenderer::Setup(const UTF16Char* profileFile, const UTF16Char* cmmFile);
        [DllImport(Settings.DllName32, EntryPoint = "?Setup@SoftProofRenderer@@QAE_NPBG0I_N@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _Setup32(HInstance* instance, string profileFile, string cmmFile, uint maxNrOfChannels, bool useCustomProfile);
        [DllImport(Settings.DllName64, EntryPoint = "?Setup@SoftProofRenderer@@QEAA_NPEBG0I_N@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _Setup64(HInstance* instance, string profileFile, string cmmFile, uint maxNrOfChannels, bool useCustomProfile);

        //bool AOI_SoftProofRenderer::Setup(const UTF16Char* profileFile, const UTF16Char* cmmFile);
        [DllImport(Settings.DllName32, EntryPoint = "?SaveDocumentEmbeddedProfile@SoftProofRenderer@@QAE_NPBGPAG@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _SaveDocumentEmbeddedProfile32(HInstance* instance, string embeddedProfileSavePath, StringBuilder profileName);
        [DllImport(Settings.DllName64, EntryPoint = "?SaveDocumentEmbeddedProfile@SoftProofRenderer@@QEAA_NPEBGPEAG@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _SaveDocumentEmbeddedProfile64(HInstance* instance, string embeddedProfileSavePath, StringBuilder profileName);

        //bool AOI_SoftProofRenderer::Render(const UTF16Char* outputPath, const UTF16Char* thumbnailtype, int32_t X, int32_t Y, int32_t tileWidth, int32_t tileHeight,  uint32_t quality,  uint64 _t visibleplates );
        [DllImport(Settings.DllName32, EntryPoint = "?RenderPage@SoftProofRenderer@@QAE_NPBGPBDIII@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _RenderPage32(HInstance* instance, string outputPath, [MarshalAs(UnmanagedType.LPStr)] string thumbnailType, uint thumbnailMaxSize, uint pageNumber, UInt32 quality);
        [DllImport(Settings.DllName64, EntryPoint = "?RenderPage@SoftProofRenderer@@QEAA_NPEBGPEBDIII@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _RenderPage64(HInstance* instance, string outputPath, [MarshalAs(UnmanagedType.LPStr)] string thumbnailType, uint thumbnailMaxSize, uint pageNumber, UInt32 quality);

        //uint32_t AOI_SoftProofRenderer::GetChannelCount();
        [DllImport(Settings.DllName32, EntryPoint = "?GetDocumentChannelCount@SoftProofRenderer@@QAEIXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern UInt32 _GetDocumentChannelCount32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "?GetDocumentChannelCount@SoftProofRenderer@@QEAAIXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern UInt32 _GetDocumentChannelCount64(HInstance* instance);

        //const char* AOI_SoftProofRenderer::GetChannelName(uint32_t index) const;
        [DllImport(Settings.DllName32, EntryPoint = "?GetDocumentChannelName@SoftProofRenderer@@QAEPBDI@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern IntPtr _GetDocumentChannelName32(HInstance* instance, UInt32 index);
        [DllImport(Settings.DllName64, EntryPoint = "?GetDocumentChannelName@SoftProofRenderer@@QEAAPEBDI@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern IntPtr _GetDocumentChannelName64(HInstance* instance, UInt32 index);

        //uint32_t AOI_SoftProofRenderer::GetChannelCount();
        [DllImport(Settings.DllName32, EntryPoint = "?GetPagesCount@SoftProofRenderer@@QBEIXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern UInt32 _GetPagesCount32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "?GetPagesCount@SoftProofRenderer@@QEBAIXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern UInt32 _GetPagesCount64(HInstance* instance);

        //uint32_t AOI_SoftProofRenderer::GetChannelCount();
        [DllImport(Settings.DllName32, EntryPoint = "?GetPageDimenssions@SoftProofRenderer@@QBEXIAAM0@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _GetPageDimenssions32(HInstance* instance, uint pageNumber, out float width, out float height);
        [DllImport(Settings.DllName64, EntryPoint = "?GetPageDimenssions@SoftProofRenderer@@QEBAXIAEAM0@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _GetPageDimenssions64(HInstance* instance, uint pageNumber, out float width, out float height);

        // bool GetImageSize(int32_t& width, int32_t& height) const;
        [DllImport(Settings.DllName32, EntryPoint = "?GetImageSize@SoftProofRenderer@@QBE_NAAH0@Z", CallingConvention = CallingConvention.ThisCall)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _GetImageSize32(HInstance* instance, out int width, out int height);
        [DllImport(Settings.DllName64, EntryPoint = "?GetImageSize@SoftProofRenderer@@QEBA_NAEAH0@Z", CallingConvention = CallingConvention.ThisCall)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _GetImageSize64(HInstance* instance, out int width, out int height);

        [DllImport(Settings.DllName32, EntryPoint = "?IsRGBDocumentColorSpace@SoftProofRenderer@@QBE_NXZ", CallingConvention = CallingConvention.ThisCall)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _IsRGBDocumentColorSpace32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "?IsRGBDocumentColorSpace@SoftProofRenderer@@QEBA_NXZ", CallingConvention = CallingConvention.ThisCall)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _IsRGBDocumentColorSpace64(HInstance* instance);

        //bool RenderTiles( const TileSettings& tileSettings, const UTF16Char* outputPath, int16_t channelIndex, const UTF16Char* multichannelRender );
        [DllImport(Settings.DllName32, EntryPoint = "?RenderRawData@SoftProofRenderer@@QAE_NABVTileSettings@@PBGIAAI@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _RenderRawData32(HInstance* instance, HInstance* settings, string outputPath, uint pageNumber, out int outputDPI);
        [DllImport(Settings.DllName64, EntryPoint = "?RenderRawData@SoftProofRenderer@@QEAA_NAEBVTileSettings@@PEBGIAEAI@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _RenderRawData64(HInstance* instance, HInstance* settings, string outputPath, uint pageNumber, out int outputDPI);

        [DllImport(Settings.DllName32, EntryPoint = "?InitTileSettings@SoftProofRenderer@@QAEXABVTileSettings@@IPBGII@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern void _InitTileSettings32(HInstance* instance, HInstance* settings, uint pageNumber, string pageOutputPath, uint pageWidth, uint pageHeight);
        [DllImport(Settings.DllName64, EntryPoint = "?InitTileSettings@SoftProofRenderer@@QEAAXAEBVTileSettings@@IPEBGII@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern void _InitTileSettings64(HInstance* instance, HInstance* settings, uint pageNumber, string pageOutputPath, uint pageWidth, uint pageHeight);

        //bool RenderTiles( const TileSettings& tileSettings, const UTF16Char* outputPath, int16_t channelIndex, const UTF16Char* multichannelRender );
        [DllImport(Settings.DllName32, EntryPoint = "?DownSampleTiles@SoftProofRenderer@@QAE_NPAEI@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _DownSampleTiles32(HInstance* instance, IntPtr outputProfileBuffer, uint outputProfileBufferLength);
        [DllImport(Settings.DllName64, EntryPoint = "?DownSampleTiles@SoftProofRenderer@@QEAA_NPEAEI@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _DownSampleTiles64(HInstance* instance, IntPtr outputProfileBuffer, uint outputProfileBufferLength);

        //bool RenderTiles( const TileSettings& tileSettings, const UTF16Char* outputPath, int16_t channelIndex, const UTF16Char* multichannelRender );
        [DllImport(Settings.DllName32, EntryPoint = "?CreateFirstLevelTiles@SoftProofRenderer@@QAE_NPAEI0I@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern bool _CreateFirstLevelTiles32(HInstance* instance, IntPtr bandData, uint bandSize, IntPtr outputProfileBuffer, uint outputProfileBufferLength);
        [DllImport(Settings.DllName64, EntryPoint = "?CreateFirstLevelTiles@SoftProofRenderer@@QEAA_NPEAEI0I@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern bool _CreateFirstLevelTiles64(HInstance* instance, IntPtr bandData, uint bandSize, IntPtr outputProfileBuffer, uint outputProfileBufferLength);

        //bool RenderTiles( const TileSettings& tileSettings, const UTF16Char* outputPath, int16_t channelIndex, const UTF16Char* multichannelRender );
        [DllImport(Settings.DllName32, EntryPoint = "?CreateTransform@SoftProofRenderer@@SAPAXPBD00_NI@Z", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _CreateTransform32(string inputProfilePath, string monitorProfile, string outputProfilePath, bool simulatePaperTint, uint channelCount);
        [DllImport(Settings.DllName64, EntryPoint = "?CreateTransform@SoftProofRenderer@@SAPEAXPEBD00_NI@Z", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _CreateTransform64(string inputProfilePath, string monitorProfile, string outputProfilePath, bool simulatePaperTint, uint channelCount);

        //bool RenderTiles( const TileSettings& tileSettings, const UTF16Char* outputPath, int16_t channelIndex, const UTF16Char* multichannelRender );
        [DllImport(Settings.DllName32, EntryPoint = "?DoTransform@SoftProofRenderer@@QAEXPAXPBX0I_KI@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _DoTransform32(HInstance* instance, IntPtr transform, IntPtr inputBuffer, IntPtr outputBuffer, uint size, ulong channelMask, uint channelCount);
        [DllImport(Settings.DllName64, EntryPoint = "?DoTransform@SoftProofRenderer@@QEAAXPEAXPEBX0I_KI@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _DoTransform64(HInstance* instance, IntPtr transform, IntPtr inputBuffer, IntPtr outputBuffer, uint size, ulong channelMask, uint channelCount);

        [DllImport(Settings.DllName32, EntryPoint = "?DeleteTransform@SoftProofRenderer@@SAXPAX@Z", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _DeleteTransform32(IntPtr transform);
        [DllImport(Settings.DllName64, EntryPoint = "?DeleteTransform@SoftProofRenderer@@SAXPEAX@Z", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _DeleteTransform64(IntPtr transform);

        [DllImport(Settings.DllName32, EntryPoint = "?GetSpotColorCIELab@SoftProofRenderer@@QAEXIAAVSpotColorCieLab@@@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _GetSpotColorCIELab32(HInstance* instance, uint index, out SpotColorCieLab spotColor);
        [DllImport(Settings.DllName64, EntryPoint = "?GetSpotColorCIELab@SoftProofRenderer@@QEAAXIAEAVSpotColorCieLab@@@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _GetSpotColorCIELab64(HInstance* instance, uint index, out SpotColorCieLab spotColor);

        [DllImport(Settings.DllName32, EntryPoint = "?GetDocumentSpotColorsCount@SoftProofRenderer@@QAEIXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern uint _GetDocumentSpotColorCount32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "?GetDocumentSpotColorsCount@SoftProofRenderer@@QEAAIXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern uint _GetDocumentSpotColorCount64(HInstance* instance);


        [DllImport(Settings.DllName32, EntryPoint = "?GetProfilePaperWhiteCIELab@SoftProofRenderer@@QAEXAAVSpotColorCieLab@@@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _GetProfilePaperWhiteCIELab32(HInstance* instance, out SpotColorCieLab spotColor);
        [DllImport(Settings.DllName64, EntryPoint = "?GetProfilePaperWhiteCIELab@SoftProofRenderer@@QEAAXAEAVSpotColorCieLab@@@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern void _GetProfilePaperWhiteCIELab64(HInstance* instance, out SpotColorCieLab spotColor);

        [DllImport(Settings.DllName32, EntryPoint = "?CreateMergedProfile@SoftProofRenderer@@SAHPBDPANI000@Z")]
        private static extern int _CreateMergedProfile32(string channelName, IntPtr labValues, uint steps, string inputProfilePath, string outputProfilePath, string tempProfilePath);
        [DllImport(Settings.DllName64, EntryPoint = "?CreateMergedProfile@SoftProofRenderer@@SAHPEBDPEANI000@Z")]
        private static extern int _CreateMergedProfile64(string channelName, IntPtr labValues, uint steps, string inputProfilePath, string outputProfilePath, string tempProfilePath);

        [DllImport(Settings.DllName32, EntryPoint = "?ReplaceProfileWhitePoint@SoftProofRenderer@@SAHPBDNNN0@Z")]
        private static extern int _AdjustProfileWhitePoint32(string inputProfilePath, double L, double a, double b, string outputProfilePath);
        [DllImport(Settings.DllName64, EntryPoint = "?ReplaceProfileWhitePoint@SoftProofRenderer@@SAHPEBDNNN0@Z")]
        private static extern int _AdjustProfileWhitePoint64(string inputProfilePath, double L, double a, double b, string outputProfilePath);

        #endregion

        #region Internal
        protected override int instanceSize()
        {
            return sizeof(HInstance) + 3 * sizeof(IntPtr);
        }
        #endregion

        #region Constructors
        public SoftProofRenderer(RenderEngineInstance instance, string documentPath, bool doRendering = true)
            : base(true)
        {
            if (Is32Bit())
                ctor32(mInstance, instance, documentPath, doRendering);
            else
                ctor64(mInstance, instance, documentPath, doRendering);
        }

        #endregion

        #region Methods 

        public override void Dispose()
        {
            if (Is32Bit())
                dtor32(mInstance);
            else
                dtor64(mInstance);
            base.Dispose();
        }

        public static bool InitRenderer()
        {
            if (Is32Bit())
                return _InitRenderer32();
            else
                return _InitRenderer64();
        }

        public bool Setup(string referenceProfile, string outputProfile, uint maxNrOfChannels, bool useCustomProfile)
        {
            if (Is32Bit())
                return _Setup32(mInstance, referenceProfile, outputProfile, maxNrOfChannels, useCustomProfile);
            else
                return _Setup64(mInstance, referenceProfile, outputProfile, maxNrOfChannels, useCustomProfile);
        }

        public bool SaveDocumentEmbeddedProfile(string folderPath, StringBuilder profileName)
        {
            if (Is32Bit())
                return _SaveDocumentEmbeddedProfile32(mInstance, folderPath, profileName);
            else
                return _SaveDocumentEmbeddedProfile64(mInstance, folderPath, profileName);
        }

        public bool RenderPage(string outputPath, string thumbnailType, uint thumbnailMaxSize, uint pageNumber, UInt32 quality = 100)
        {
            if (Is32Bit())
                return _RenderPage32(mInstance, outputPath, thumbnailType, thumbnailMaxSize, pageNumber, quality);
            else
                return _RenderPage64(mInstance, outputPath, thumbnailType, thumbnailMaxSize, pageNumber, quality);
        }

        public UInt32 GetDocumentChannelCount()
        {
            return Is32Bit() ? _GetDocumentChannelCount32(mInstance) : _GetDocumentChannelCount64(mInstance);
        }

        public string GetDocumentChannelName(UInt32 channelIdx)
        {
            IntPtr channelNamePtr = Is32Bit() ? _GetDocumentChannelName32(mInstance, channelIdx) : _GetDocumentChannelName64(mInstance, channelIdx);
            return Marshal.PtrToStringAnsi(channelNamePtr);
        }

        public UInt32 GetPagesCount()
        {
            return Is32Bit() ? _GetPagesCount32(mInstance) : _GetPagesCount64(mInstance);
        }

        public void GetPageDimenssions(uint pageNumber, out float width, out float height)
        {
            if (Is32Bit())
                _GetPageDimenssions32(mInstance, pageNumber, out width, out height);
            else
                _GetPageDimenssions64(mInstance, pageNumber, out width, out height);
        }

        public bool GetImageSize(out int width, out int height)
        {
            if (Is32Bit())
                return _GetImageSize32(mInstance, out width, out height);
            else
                return _GetImageSize64(mInstance, out width, out height);
        }

        public bool IsDocumentRGBColorSpace()
        {
            if (Is32Bit())
                return _IsRGBDocumentColorSpace32(mInstance);
            else
                return _IsRGBDocumentColorSpace64(mInstance);
        }

        public bool RenderRawData(RenderEngineTileSettings settings, string outputPath, uint pageNumber, out int outputDPI)
        {
            if (Is32Bit())
                return _RenderRawData32(mInstance, settings, outputPath, pageNumber, out outputDPI);
            else
                return _RenderRawData64(mInstance, settings, outputPath, pageNumber, out outputDPI);
        }

        public bool DownSampleTiles(IntPtr outputProfileBuffer, uint outputProfileBufferLength)
        {
            if (Is32Bit())
                return _DownSampleTiles32(mInstance, outputProfileBuffer, outputProfileBufferLength);
            else
                return _DownSampleTiles64(mInstance, outputProfileBuffer, outputProfileBufferLength);
        }

        public bool CreateFirstLevelTiles(IntPtr bandData, uint bandSize, IntPtr outputProfileBuffer, uint outputProfileBufferLength)
        {
            if (Is32Bit())
                return _CreateFirstLevelTiles32(mInstance, bandData, bandSize, outputProfileBuffer, outputProfileBufferLength);
            else
                return _CreateFirstLevelTiles64(mInstance, bandData, bandSize, outputProfileBuffer, outputProfileBufferLength);
        }

        public void InitTileSettings(RenderEngineTileSettings settings, uint pageNumber, string pageOutputFolderPath, uint pageWidth, uint pageHeight)
        {
            if (Is32Bit())
                _InitTileSettings32(mInstance, settings, pageNumber, pageOutputFolderPath, pageWidth, pageHeight);
            else
                _InitTileSettings64(mInstance, settings, pageNumber, pageOutputFolderPath, pageWidth, pageHeight);
        }

        public static IntPtr CreateTransform(string inputProfile, string monitorProfile, string outputProfile, bool simualtePaperTint, uint channelCount)
        {
            if (Is32Bit())
                return _CreateTransform32(inputProfile, monitorProfile, outputProfile, simualtePaperTint, channelCount);
            else
                return _CreateTransform64(inputProfile, monitorProfile, outputProfile, simualtePaperTint, channelCount);
        }

        public void DoTransform(IntPtr transform, IntPtr inputBuffer, IntPtr outputBuffer, uint size, ulong channelMask, uint channelCount)
        {
            if (Is32Bit())
                _DoTransform32(mInstance, transform, inputBuffer, outputBuffer, size, channelMask, channelCount);
            else
                _DoTransform64(mInstance, transform, inputBuffer, outputBuffer, size, channelMask, channelCount);
        }

        public static void DeleteTransform(IntPtr transform)
        {
            if (Is32Bit())
                 _DeleteTransform32(transform);
            else
                 _DeleteTransform64(transform);
        }

        public static int CreateMergedProfile(string channelName, double[] labValuesArray, uint steps, string inputProfilePath,
            string outputProfilePath, string tempProfilePath)
        {
            fixed (double* labValuesPtr = labValuesArray)
            {
                IntPtr labValues = (IntPtr) labValuesPtr;

                if (Is32Bit())
                    return _CreateMergedProfile32(channelName, labValues, steps, inputProfilePath, outputProfilePath,
                        tempProfilePath);
                else
                    return _CreateMergedProfile64(channelName, labValues, steps, inputProfilePath, outputProfilePath,
                        tempProfilePath);
            }
        }

        public static int AdjustProfilePaperWhite(string inputProfilePath, double L, double a, double b,
            string outputProfilePath)
        {
            if (Is32Bit())
                return _AdjustProfileWhitePoint32(inputProfilePath, L, a, b, outputProfilePath);
            else
                return _AdjustProfileWhitePoint64(inputProfilePath, L, a, b, outputProfilePath);
        }

        public void GetSpotColorCIELab(uint index, out SpotColorCieLab spotColor)
        {
            if (Is32Bit())
                 _GetSpotColorCIELab32(mInstance, index, out spotColor);
            else
                 _GetSpotColorCIELab64(mInstance, index, out spotColor);
        }

        public void GetProfilePaperWhiteCIELab(out SpotColorCieLab spotColor)
        {
            if (Is32Bit())
                 _GetProfilePaperWhiteCIELab32(mInstance, out spotColor);
            else
                _GetProfilePaperWhiteCIELab64(mInstance, out spotColor);
        }

        public uint GetDocumentSpotColorsCount()
        {
            if (Is32Bit())
                return _GetDocumentSpotColorCount32(mInstance);
            else
                return _GetDocumentSpotColorCount64(mInstance);
        }

        public void ApplyTransformAndCreateFirstLevelTiles(IntPtr transformHandler, byte[] bandRawByteArray,
            uint channelCount, ulong channelMask, IntPtr outputProfileData, uint outputProfileSize)
        {
            uint rawbandSize = (uint)bandRawByteArray.Length;
            fixed (byte* rawData = bandRawByteArray)
            {
                IntPtr rawDataPtr = (IntPtr) rawData;

                uint pixelSize = rawbandSize/channelCount;
                uint transformBandSize = pixelSize * 3;
                byte[] transformedBandByteArray = new byte[transformBandSize];
                fixed (byte* transformedData = transformedBandByteArray)
                {
                    IntPtr bandTransformedata = (IntPtr) transformedData;
                    DoTransform(transformHandler, rawDataPtr, bandTransformedata, pixelSize, channelMask, channelCount);
                    CreateFirstLevelTiles(bandTransformedata, transformBandSize, outputProfileData, outputProfileSize);
                }
            }
        }

        #endregion
    }

    public unsafe class  RenderEngineTileSettings : RenderEngineObj
    {
        #region Pinvokes

        [DllImport(Settings.DllName32, EntryPoint = "??0TileSettings@@QAE@IIIIMFI_K@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctor32(HInstance* instance, uint tileSize, uint minZoomSize, uint maxOutputsize, uint maxMemoryUsage, float maxDpi, Int16 jpegQuality, uint minPreviewSize, UInt64 visiblePlates);
        [DllImport(Settings.DllName64, EntryPoint = "??0TileSettings@@QEAA@IIIIMFI_K@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctor64(HInstance* instance, uint tileSize, uint minZoomSize, uint maxOutputsize, uint maxMemoryUsage, float maxDpi, Int16 jpegQuality, uint minPreviewSize, UInt64 visiblePlates);

        #endregion

        #region Internal
        protected override int instanceSize()
        {
            return sizeof(HInstance) + 5 * sizeof(UInt32) + sizeof(float) + sizeof(UInt64) + sizeof(Int16);
        }
        #endregion

        #region Constructors
        public RenderEngineTileSettings(uint tileSize, uint minZoomSize, uint maxMemoryUsage, uint maxOutputsize, float maxDpi, Int16 jpegQuality, uint minPreviewSize, UInt64 visiblePlates = UInt64.MaxValue)
            : base(true)
        {
            if (Is32Bit())
                ctor32(mInstance, tileSize, minZoomSize, maxMemoryUsage, maxOutputsize, maxDpi, jpegQuality, minPreviewSize, visiblePlates);
            else
                ctor64(mInstance, tileSize, minZoomSize, maxMemoryUsage, maxOutputsize, maxDpi, jpegQuality, minPreviewSize, visiblePlates);
        }

        #endregion

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SpotColorCieLab
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string ChannelName;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] Lab;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public byte[] Rgb;
    }

    #endregion

    #region Utilities

    internal class Settings
    {
        public const string DllName32 = "SoftProofingRenderEngine_32.dll";
        public const string DllName64 = "SoftProofingRenderEngine_64.dll";
    };

    #endregion
}
