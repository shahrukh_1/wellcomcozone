﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using GMG.CoZone.Component.Logging;
using GMGColorDAL;

namespace GMGColorMediaDefine.Common
{
    public static class ServiceLog
    {
        private static readonly Logger _log =
            new Logger(GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.ActiveConfiguration + " " + GMGColorConfiguration.AppConfiguration.AWSRegion
                                                                                : String.Empty
                        , "DBErrorLogger", "MediaDefineLogger");

        /// <summary>
        /// Log method
        /// </summary>
        /// <param name="message">proper message associated with the log</param>
        /// <param name="ex">The Exception that was thrown</param>
        public static void Log(string message, Exception ex)
        {
            _log.Log(new LoggingException(message, ex));
        }

        /// <summary>
        /// Log method
        /// </summary>
        /// <param name="notification"></param>
        public static void Log(LoggingNotification notification)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int) notification.Severity)
            {
                _log.Log(notification);
            }
        }
    }
}
