﻿using System;
using GMGColorMediaDefine.Interfaces;
using Newtonsoft.Json;
using GMG.CoZone.Common.Module.Enums;
using GMGColorDAL;
using GMGColorBusinessLogic;
using GMG.CoZone.Common.Interfaces;
using System.Net;

namespace GMGColorMediaDefine.Common
{
    public class ApprovalStatusMessageSender : IApprovalStatusMessageSender
    {
        private readonly IGMGConfiguration _gmgConfiguration;
        private readonly IAWSSNSService _awsSnsService;

        private const string ERROR_IMG_PATH = "/content/img/noimagError-128.png";
        private const string VIDEO_IMG_PATH = "/content/img/video.png";

        public ApprovalStatusMessageSender(IGMGConfiguration gmgConfiguration, IAWSSNSService awsSnsService)
        {
            _gmgConfiguration = gmgConfiguration;
            _awsSnsService = awsSnsService;
        }

        public void SendMessage(ApprovalStatusMessageModel messageData)
        {
            var data = JsonConvert.SerializeObject(new
            {
                ApprovalId = messageData.ApprovalId,
                Status = messageData.IsError  ?  Enum.GetName(typeof(ApprovalProgressStatusEnum), ApprovalProgressStatusEnum.Error) 
                                              : messageData.Progress != 100
                                              ? Enum.GetName(typeof(ApprovalProgressStatusEnum), ApprovalProgressStatusEnum.InProgress)
                                              : Enum.GetName(typeof(ApprovalProgressStatusEnum), ApprovalProgressStatusEnum.Success),
                Progress = messageData.Progress,
                Thumbnail = messageData.IsError 
                            ? 
                            ERROR_IMG_PATH 
                            : 
                            messageData.ApprovalType == ApprovalTypeEnum.Movie 
                            ? 
                            VIDEO_IMG_PATH 
                            : 
                            Approval.GetThumbnailImagePath(messageData.ApprovalId, messageData.Context).Replace("https", "http")
            });

            SnsLocalSwitch(data, messageData.Domain);
        }

        private void SnsLocalSwitch(string data, string domain)
        {
            if (_gmgConfiguration.IsEnabledS3Bucket)
            {
                _awsSnsService.PublishStatus(data);
            }
            else
            {
                var url = "http://" + domain + "/SNSReceiver.ashx";
                using (var wb = new WebClient())
                {
                    wb.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var response = wb.UploadString(url, data);
                }
            }
        }
    }
}
