﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.FileTransfer.DomainModels
{
    public class FileTransferModel
    {
        public int ID { get; set; }
        public int AccountID { get; set; }
        public string FileName { get; set; }
        public string FolderPath { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public string Message { get; set; }
        public int Creator { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int Modifier { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public List<int> InternalUsers { get; set; }
        public List<int> ExternalUsers { get; set; }
        public string ExternalEmails { get; set; }
        public string IndividualFileNames { get; set; } 
    }
}
