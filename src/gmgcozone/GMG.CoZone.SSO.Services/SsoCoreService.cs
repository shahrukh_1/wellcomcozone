﻿using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.Interfaces;
using Microsoft.Practices.Unity;
using GMG.CoZone.SSO.Interfaces.Auth0Interfaces;
using System;
using GMG.CoZone.Common.Module.Enums;
using System.Linq;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.SSO.Services
{
    public class SsoCoreService : ISsoCore
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly string _settingsKey = Enum.GetName(typeof(SettingsKeyEnum), SettingsKeyEnum.SingleSignOn);

        public SsoCoreService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [Dependency]
        public IAuth0Plugin Auth0Plugin { get; set; }

        [Dependency]
        public ISamlPlugin SamlPlugin { get; set; }

        public SsoOptionsEnum ActiveSsoConfiguration(int accountId)
        {
            var accSettings = _unitOfWork.AccountSettingsRepository.Get(acs => acs.Account == accountId && acs.Name == _settingsKey).FirstOrDefault();
            if (accSettings != null)
            {
                return accSettings.Value.Contains(Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Auth0)) ? SsoOptionsEnum.Auth0 : SsoOptionsEnum.Saml;
            }
            else
            {
                return SsoOptionsEnum.None;
            }
        }

        public bool IsSsoAccountEnabled(int accountId)
        {
            return _unitOfWork.AccountSettingsRepository.Get(acs => acs.Account == accountId).FirstOrDefault() != null;
        }

        public bool IsSsoUser(int accountId, string userEmail)
        {
            var dbUser = _unitOfWork.UserRepository.Get(us => us.Account == accountId && us.EmailAddress == userEmail).FirstOrDefault();
            return dbUser != null && dbUser.IsSsoUser;
        }
    }
}
