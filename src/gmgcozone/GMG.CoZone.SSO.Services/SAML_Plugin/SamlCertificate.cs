﻿using System.Net;
using System.Security.Cryptography.X509Certificates;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Services.SAML_Plugin
{
    public class SamlCertificate : ISamlCertificate
    {
        public X509Certificate2 Load(SsoSamlConfigurationModel ssoSettings)
        {
            var accountCertificate = new X509Certificate2();

            if (ssoSettings.AccountSsoSettingsModel.CertificateName.Contains("http"))
            {
                WebClient client = new WebClient();
                accountCertificate.Import(client.DownloadData(ssoSettings.AccountSsoSettingsModel.CertificateName));
            }
            else
            {
                accountCertificate.Import(ssoSettings.AccountSsoSettingsModel.CertificateName);
            }
            return accountCertificate;
        }
    }
}