﻿using System;
using System.IO;
using System.Xml;
using System.IO.Compression;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Services.SAML_Plugin
{
    public class SamlAuthRequest : ISamlAuthRequest
    {
        private readonly string _id;
        private readonly string _issueInstant;

        public SamlAuthRequest()
        {
            _id = "_" + Guid.NewGuid();
            _issueInstant = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

        public string GetRequest(SsoSamlConfigurationModel settings)
        {
            using (var writer = new StringWriter())
            {
                var writerSetting = new XmlWriterSettings { OmitXmlDeclaration = true };

                using (var xmlWriter = XmlWriter.Create(writer, writerSetting))
                {
                    xmlWriter.WriteStartElement("samlp", "AuthnRequest", "urn:oasis:names:tc:SAML:2.0:protocol");
                    xmlWriter.WriteAttributeString("ID", _id);
                    xmlWriter.WriteAttributeString("Version", "2.0");
                    xmlWriter.WriteAttributeString("IssueInstant", _issueInstant);
                    xmlWriter.WriteAttributeString("ProtocolBinding", "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST");
                    xmlWriter.WriteAttributeString("AssertionConsumerServiceURL", settings.AssertionConsumerServiceUrl);
                    xmlWriter.WriteAttributeString("Destination", settings.AccountSsoSettingsModel.IdpIssuerUrl);
                    xmlWriter.WriteStartElement("saml", "Issuer", "urn:oasis:names:tc:SAML:2.0:assertion");
                    xmlWriter.WriteString(settings.AccountSsoSettingsModel.EntityId);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                }

                var toEncodeAsBytes = System.Text.Encoding.ASCII.GetBytes(writer.ToString());
                using (var output = new MemoryStream())
                {
                    using (var zip = new DeflateStream(output, CompressionMode.Compress))
                        zip.Write(toEncodeAsBytes, 0, toEncodeAsBytes.Length);
                    var compressed = output.ToArray();
                    return Convert.ToBase64String(compressed);
                }
            }
        }
    }
}