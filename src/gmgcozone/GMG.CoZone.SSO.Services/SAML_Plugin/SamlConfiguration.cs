﻿using System;
using System.Configuration;
using GMG.CoZone.Common.Module.ExtensionMethods;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.DomainModels;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMG.CoZone.Common.Module.Enums;
using System.Linq;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.SSO.Services.SAML_Plugin
{
    public class SamlConfiguration : ISamlConfiguration
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly string _settingsKey = Enum.GetName(typeof(SettingsKeyEnum), SettingsKeyEnum.SingleSignOn);

        public SamlConfiguration(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public SsoSamlConfigurationModel LoadProprieties(int accountId, string accountGuid, string accountRegion)
        {
            var dbAccSetings = _unitOfWork.AccountSettingsRepository.Get(filter: acs => acs.Name == _settingsKey && acs.Account == accountId).FirstOrDefault();

            GMGColorDAL.GMGColorLogging.log.InfoFormat("In LoadProprieties method after getting AccountSettingsRepository ");

            if (dbAccSetings != null)
            {
                var ssoConfigurationModel = new SsoSamlConfigurationModel()
                {
                    AccountSsoSettingsModel = dbAccSetings.Value.ToObject<SsoSamlSettingsModel>(),
                    AssertionConsumerServiceUrl = ConfigurationManager.AppSettings["AssertionConsumerServiceUrl"]
                };
                ssoConfigurationModel.AccountSsoSettingsModel.CertificateName = GetCertificatePath(accountGuid, ssoConfigurationModel.AccountSsoSettingsModel.CertificateName, accountRegion);

                GMGColorDAL.GMGColorLogging.log.InfoFormat("In LoadProprieties method after getting GetCertificatePath ");


                return ssoConfigurationModel;
            }
            return null;
        }

        private string GetCertificatePath(string accountGuid, string fileName, string accountRegion)
        {
            var path = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion),
                                     GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath,
                                     accountGuid,
                                     GMGColorConfiguration.AppConfiguration.CertificateFolderRelativePath,
                                     fileName).Replace(@"\", @"/").Replace("//", "/").TrimEnd('/');

            if (path.Contains("http"))
            {
                path = path.Insert(path.IndexOf(":", StringComparison.Ordinal) + 1, "/");
            }
            return path;
        }
    }
}