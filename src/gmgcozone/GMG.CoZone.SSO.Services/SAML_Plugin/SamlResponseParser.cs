﻿using System;
using System.Xml;
using System.Security.Cryptography.Xml;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.DomainModels;
using System.Security.Cryptography;

namespace GMG.CoZone.SSO.Services.SAML_Plugin
{
    public class SamlResponseParser : ISamlResponseParser
    {
        private readonly ISamlCertificate _samlCertificate;

        public SamlResponseParser(ISamlCertificate samlCertificate)
        {
            _samlCertificate = samlCertificate;
        }
       
        /// <summary>
        /// Parse IdP response
        /// </summary>
        /// <param name="response">IdP response</param>
        /// <param name="ssoSettings">Account sso settings</param>
        /// <returns></returns>
        public SsoSamlResponseModel ParseResponse(string response, SsoSamlConfigurationModel ssoSettings)
        {
            var encodingObj = new System.Text.ASCIIEncoding();
            var xmlDoc = new XmlDocument {PreserveWhitespace = true};

            xmlDoc.LoadXml(encodingObj.GetString(Convert.FromBase64String(response)));

            if (!IsAuthenticated(xmlDoc, ssoSettings) && !ResponseIsValid(xmlDoc)) return null;

            return BuildSsoModelResponse(xmlDoc);
        }

        /// <summary>
        /// Check if the user is autheticated by the IdP
        /// </summary>
        /// <param name="xmlDoc">The saml assertion</param>
        /// <param name="ssoSettings">The application configuration settings</param>
        /// <returns></returns>
        private bool IsAuthenticated(XmlDocument xmlDoc, SsoSamlConfigurationModel ssoSettings)
        {
            var manager = new XmlNamespaceManager(xmlDoc.NameTable);
            manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
            var nodeList = xmlDoc.SelectNodes("//ds:Signature", manager);

            CryptoConfig.AddAlgorithm(typeof(RsaPkCs1Sha256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

            if (nodeList != null)
            {
                var signedXml = new SignedXml(xmlDoc);
                var samlCertificate = _samlCertificate.Load(ssoSettings);

                foreach (XmlNode node in nodeList)
                {
                    signedXml.LoadXml((XmlElement)node);
                    GMGColorDAL.GMGColorLogging.log.InfoFormat("signedXml algorith: " + signedXml.SignatureMethod);

                    return signedXml.CheckSignature(samlCertificate, true);
                }
            }
            return false;
        }

        /// <summary>
        /// Check if the assertion has expired
        /// </summary>
        /// <param name="xmlDoc">The saml assertion</param>
        /// <returns></returns>
        private bool ResponseIsValid(XmlDocument xmlDoc)
        {
            var manager = new XmlNamespaceManager(xmlDoc.NameTable);
            manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
            manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
            manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");

            var selectSingleNode = xmlDoc.SelectSingleNode("/samlp:Response/saml:Assertion/saml:Subject/saml:SubjectConfirmation/saml:SubjectConfirmationData/@NotOnOrAfter", manager);
            if (selectSingleNode != null)
            {
                var notOnOrAfter = Convert.ToDateTime(selectSingleNode.InnerText);
                if (DateTime.UtcNow > notOnOrAfter)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Build to response model from the response
        /// </summary>
        /// <param name="xmlDoc">The response document</param>
        /// <returns></returns>
        private SsoSamlResponseModel BuildSsoModelResponse(XmlDocument xmlDoc)
        {
            var manager = new XmlNamespaceManager(xmlDoc.NameTable);
            manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
            manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
            manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");

            var selectSingleNode = xmlDoc.SelectSingleNode("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name='email']", manager);
            var singleNode = xmlDoc.SelectSingleNode("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name='username']", manager);
            var xmlNode = xmlDoc.SelectSingleNode("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name='is_portal_user']", manager);
            if (selectSingleNode != null && singleNode != null && xmlNode != null)
            {
                return new SsoSamlResponseModel
                {
                    Email = selectSingleNode.InnerText,
                    Username = singleNode.InnerText,
                    IsPortalUser = xmlNode.InnerText
                };
            }
            return null;
        }
    }
}