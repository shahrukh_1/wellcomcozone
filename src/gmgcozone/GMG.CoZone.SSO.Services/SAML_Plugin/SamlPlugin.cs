﻿using System.Web;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Services.SAML_Plugin
{
    public class SamlPlugin : ISamlPlugin
    {
        private SsoSamlConfigurationModel _accounSsoSettings;

        private readonly ISamlAuthRequest _samlAuthRequest;
        private readonly ISamlConfiguration _samlConfiguration;
        private readonly ISamlResponseParser _samlResponseParser;


        public SamlPlugin(ISamlAuthRequest samlAuthRequest, 
                          ISamlConfiguration samlConfiguration, 
                          ISamlResponseParser samlResponseParser)
        {
            _samlAuthRequest = samlAuthRequest;
            _samlConfiguration = samlConfiguration;
            _samlResponseParser = samlResponseParser;
        }

        public string GetIdpUrl(int accountId, string accountGuid, string accountRegion)
        {
            _accounSsoSettings = _accounSsoSettings ?? _samlConfiguration.LoadProprieties(accountId, accountGuid, accountRegion);

            GMGColorDAL.GMGColorLogging.log.InfoFormat("In GetIdpUrl method after getting LoadProprieties ");

            if (_accounSsoSettings == null)
            {
                return string.Empty;
            }
            return _accounSsoSettings.AccountSsoSettingsModel.IdpIssuerUrl +
                    "?SAMLRequest=" +
                    HttpContext.Current.Server.UrlEncode(_samlAuthRequest.GetRequest(_accounSsoSettings));
        }

        public SsoSamlResponseModel ParseResponse(string samlResponse, int accountId, string accountGuid, string accountRegion)
        {
            if (string.IsNullOrEmpty(samlResponse)) { return null;}

            _accounSsoSettings = _accounSsoSettings ?? _samlConfiguration.LoadProprieties(accountId, accountGuid, accountRegion);

            return _samlResponseParser.ParseResponse(samlResponse, _accounSsoSettings);
        }
    }
}
