﻿using GMG.CoZone.SSO.Interfaces.Auth0Interfaces;
using GMG.CoZone.SSO.DomainModels;
using GMG.CoZone.Common.Module.ExtensionMethods;
using System.Linq;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.SSO.Services.Auth0_Plugin
{
    public class Auth0Plugin : IAuth0Plugin
    {
        private readonly IUnitOfWork _unitOfWork;

        public Auth0Plugin(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public SsoAuth0SettingsModel GetSettings(string hostName)
        {
            var accountId = _unitOfWork.AccountRepository.Get(ac => ac.Domain == hostName || ac.CustomDomain == hostName).FirstOrDefault().ID;
            var accSettings = _unitOfWork.AccountSettingsRepository.Get(acs => acs.Account == accountId).FirstOrDefault();
            if (accSettings != null)
            {
                return accSettings.Value.ToObject<SsoAuth0SettingsModel>();
            }
            return null;
        }
    }
}
