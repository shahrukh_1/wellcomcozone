﻿using GMG.CoZone.Common.Module.Enums;
using System.ComponentModel.DataAnnotations;

namespace GMG.CoZone.Settings.VM
{
    public class SsoSamlSettingsViewModel
    {
        [Required]
        public string EntityId { get; set; }

        [Required]
        public string IdpIssuerUrl { get; set; }

        public string CertificateName { get; set; }

        public string Type { get; set; }
    }
}
