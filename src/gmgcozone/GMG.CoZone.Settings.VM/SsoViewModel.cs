﻿using GMG.CoZone.Common.Module.Enums;
using System;

namespace GMG.CoZone.Settings.VM
{
    public class SsoViewModel
    {
        public SsoOptionsEnum ActiveConfiguration { get; set; }
        public SsoAuth0SettingsViewModel Auth0 { get; set; }
        public SsoSamlSettingsViewModel Saml { get; set; }

        public SsoViewModel()
        {
            Auth0 = new SsoAuth0SettingsViewModel() { Type = Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Auth0) };
            Saml = new SsoSamlSettingsViewModel() { Type = Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Saml) };
        }
    }
}
