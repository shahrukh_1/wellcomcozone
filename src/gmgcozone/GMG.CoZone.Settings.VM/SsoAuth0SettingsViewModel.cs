﻿using GMG.CoZone.Common.Module.Enums;
using System.ComponentModel.DataAnnotations;

namespace GMG.CoZone.Settings.VM
{
    public class SsoAuth0SettingsViewModel
    {
        [Required]
        public string Domain { get; set; }

        [Required]
        public string ClientId { get; set; }

        [Required]
        public string ClientSecret { get; set; }

        public string Type { get; set; }
    }
}
