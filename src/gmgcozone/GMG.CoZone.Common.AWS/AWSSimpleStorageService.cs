﻿using System.Collections.Generic;
using Amazon.S3;
using Amazon.S3.IO;
using GMG.CoZone.Common.AWS.Interfaces;
using GMG.CoZone.Common.Interfaces;
using System.Linq;
using System;

namespace GMG.CoZone.Common.AWS
{
    public class AWSSimpleStorageService : IAWSSimpleStorageService
    {
        private readonly IGMGConfiguration _gmgConfiguration;
        private readonly IAmazonS3 _amazonS3Client;

        public AWSSimpleStorageService(IGMGConfiguration gmgConfiguration)
        {
            _gmgConfiguration = gmgConfiguration;
            if (_gmgConfiguration.IsEnabledS3Bucket)
            {
                _amazonS3Client = new AmazonS3Client(_gmgConfiguration.AWSAccessKey, _gmgConfiguration.AWSSecretKey);
            }
        }

        public void DeleteFile(string fileName, string accountRegion)
        {
            try
            {
                var fileInfo = new S3FileInfo(_amazonS3Client, _gmgConfiguration.DataFolderName(accountRegion), fileName);
                fileInfo.Delete();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteFolderContent(List<string> content, string accountRegion)
        {
            foreach (var item in content)
            {
                if (item.EndsWith("\\"))
                {
                    var folderInfo = new S3DirectoryInfo(_amazonS3Client, _gmgConfiguration.DataFolderName(accountRegion), item);
                    folderInfo.Delete(true);

                }
                else
                {
                    var fileInfo = new S3FileInfo(_amazonS3Client, _gmgConfiguration.DataFolderName(accountRegion), item);
                    fileInfo.Delete();
                }
            }
        }

        public List<string> GetFolderContent(string folderName, string accountRegion)
        {
            var contentList = new List<string>();

            var dirInfo = new S3DirectoryInfo(_amazonS3Client, _gmgConfiguration.DataFolderName(accountRegion), "approvals\\" + folderName);

            var tempDirData = dirInfo.GetDirectories();
            var tempFileData = dirInfo.GetFiles();

            foreach (var dir in tempDirData)
            {
                contentList.Add(dir.FullName.Remove(0, dir.FullName.IndexOf("\\") + 1));
            }

            foreach (var file in tempFileData)
            {
                contentList.Add(file.FullName.Remove(0, file.FullName.IndexOf("\\") + 1));
            }
            return contentList;
        }
    }
}
