﻿using System.Web;

namespace GMG.CoZone.Common.AWS.Interfaces
{
    /// <summary>
    /// CoZone AWS SNS Interface
    /// </summary>
    public interface IAWSSimpleNotificationService
    {
        /// <summary>
        /// Sends a message to all of a topic's subscribed endpoints
        /// </summary>
        /// <param name="topicArn">The topic you want to publish to</param>
        /// <param name="message">The message you want to send to the topic.</param>
        /// <returns>The response from the Publish service method, as returned by SimpleNotificationService</returns>
        void WriteMessage(string topicArn, string message);

        /// <summary>
        /// Check if SNS Topic exists
        /// </summary>
        /// <param name="AWSAccessKey">AWS access key</param>
        /// <param name="AWSSecretKey">AWS secret key</param>
        /// <param name="AWSRegion">AWS region</param>
        /// <param name="TopicARN">SNS topic</param>
        /// <returns>True or false</returns>
        bool CheckTopicExists(string topicArn);

        /// <summary>
        /// Get the topic ARN
        /// </summary>
        /// <param name="topicName">Topic Name</param>
        /// <returns>Topic ARN</returns>
        string GetTopicArn(string topicName);

        /// <summary>
        /// Prepares to subscribe an endpoint by sending the endpoint a confirmation message
        /// </summary>
        /// <param name="topicArn">The ARN of the topic you want to subscribe to</param>
        /// <param name="protocol">The protocol you want to use</param>
        /// <param name="endpoint">The endpoint that you want to receive notifications</param>
        /// <returns>The response from the Subscribe service method, as returned by SimpleNotificationService</returns>
        void CreateSubscriber(string topicArn, string protocol, string endpoint);


        /// <summary>
        /// Verifies an endpoint owner's intent to receive messages by validating the token sent to the endpoint by an earlier Subscribe action
        /// </summary>
        /// <param name="topicArn">The ARN of the topic for which you wish to confirm a subscription</param>
        /// <param name="token">Short-lived token sent to an endpoint during the Subscribe action</param>
        void ConfirmSubscription(string topicArn, string token);

        /// <summary>
        /// Process SNS Message and Confirm Subscription or Process Notification
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="message">The message body</param>
        string ProcessMessage(HttpContext context, string message);
    }
}
