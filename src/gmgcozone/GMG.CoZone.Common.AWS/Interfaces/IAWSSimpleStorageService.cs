﻿using System.Collections.Generic;

namespace GMG.CoZone.Common.AWS.Interfaces
{
    public interface IAWSSimpleStorageService
    {
        List<string> GetFolderContent(string folderName, string accountRegion);
        void DeleteFile(string fileName, string accountRegion);
        void DeleteFolderContent(List<string> content, string accountRegion);
    }
}
