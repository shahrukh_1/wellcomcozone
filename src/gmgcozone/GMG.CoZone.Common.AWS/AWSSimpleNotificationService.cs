﻿using Amazon.SimpleNotificationService.Model;
using GMG.CoZone.Common.AWS.Interfaces;
using Amazon.SimpleNotificationService;
using GMG.CoZone.Common.Interfaces;
using System.Linq;
using System;
using System.Web;
using Amazon.SimpleNotificationService.Util;

namespace GMG.CoZone.Common.AWS
{
    public class AWSSimpleNotificationService : IAWSSimpleNotificationService
    {
        private IGMGConfiguration _gmgConfiguration;
        private AmazonSimpleNotificationServiceClient _snsClient;

        public AWSSimpleNotificationService(IGMGConfiguration gmgConfiguration)
        {
            _gmgConfiguration = gmgConfiguration;

            if (_gmgConfiguration.IsEnabledS3Bucket)
            {
                Amazon.RegionEndpoint region = Amazon.RegionEndpoint.GetBySystemName(_gmgConfiguration.AWSRegion);
                _snsClient = new AmazonSimpleNotificationServiceClient(_gmgConfiguration.AWSAccessKey, _gmgConfiguration.AWSSecretKey, region);
            }
        }

        public bool CheckTopicExists(string topicARN)
        {
            try
            {
                return _snsClient.ListTopics().Topics.Any(tp => tp.TopicArn == topicARN);
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw;
            }
        }

        public void WriteMessage(string topicArn, string message)
        {
            try
            {
                _snsClient.Publish(new PublishRequest
                {
                    Message = message,
                    TopicArn = topicArn
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw;
            }
        }

        public void CreateSubscriber(string topicArn, string protocol, string endpoint)
        {
            try
            {
                _snsClient.Subscribe(new SubscribeRequest {
                    TopicArn = topicArn,
                    Protocol = protocol, 
                    Endpoint = endpoint
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw;
            }
        }

        public string GetTopicArn(string topicName)
        {
            try
            {
                return _snsClient.FindTopic(topicName).TopicArn;
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw;
            }
        }

        public void ConfirmSubscription(string topicArn, string token)
        {
            try
            {
                _snsClient.ConfirmSubscription(new ConfirmSubscriptionRequest
                {
                    Token = token,
                    TopicArn = topicArn
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw;
            }
        }

        public string ProcessMessage(HttpContext context, string message)
        {
            var messageObj = Message.ParseMessage(message);

            if (!messageObj.IsMessageSignatureValid()) { throw new Exception("Amazon SNS Message signature is invalid!"); }

            if (messageObj.IsSubscriptionType)
            {
                ConfirmSubscription(context, messageObj);
            }
            else if (messageObj.IsNotificationType)
            {
                return ProcessNotification(context, messageObj);
            }
            return string.Empty;
        }

        private void ConfirmSubscription(HttpContext context, Message messageObj)
        {
            if (!IsValidTopic(messageObj.TopicArn)) { return; }

            try
            {
                ConfirmSubscription(messageObj.TopicArn, messageObj.Token);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string ProcessNotification(HttpContext context, Message messageObj)
        {
            return messageObj.MessageText;
        }

        private bool IsValidTopic(string topicArn)
        {
            if (GetTopicArn(_gmgConfiguration.AWSSNSMediaDefineResponseTopic) == topicArn)
            {
                return true;
            }
            return false;
        }
    }
}
