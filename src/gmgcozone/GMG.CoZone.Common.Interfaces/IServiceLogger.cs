﻿using GMG.CoZone.Component.Logging;
using System;

namespace GMG.CoZone.Common.Interfaces
{
    /// <summary>
    /// Service logger interface
    /// </summary>
    public interface IServiceLogger
    {
        /// <summary>
        /// Error log method
        /// </summary>
        /// <param name="message">Proper message associated with the log</param>
        /// <param name="exception">The Exception that was thrown</param>
        void Log(string message, Exception exception);

        /// <summary>
        /// Notification log method
        /// </summary>
        /// <param name="notification"></param>
        void Log(LoggingNotification notification);
    }
}
