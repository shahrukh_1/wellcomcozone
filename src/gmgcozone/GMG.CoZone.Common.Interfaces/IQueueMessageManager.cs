﻿using System.Collections.Generic;
using System.Messaging;

namespace GMG.CoZone.Common.Interfaces
{
    /// <summary>
    /// Interface manager for writing and reading messages from queues
    /// </summary>
    public interface IQueueMessageManager
    {
        /// <summary>
        /// Based on configurations writes a message to queue
        /// </summary>
        /// <param name="queueName">The name of the queue where to send the message</param>
        /// <param name="message">Message object as string</param>
        void  WriteMessageToQueue(string queueName, string message);

        /// <summary>
        /// Send a message to a AWS queue
        /// </summary>
        /// <param name="queueName">The name of the queue where to send the message</param>
        /// <param name="message">Message object as string</param>
        void WriteMessageToAWSQueue(string queueName, string message);

        /// <summary>
        /// Send a message to a Local queue
        /// </summary>
        /// <param name="queueName">The name of the queue where to send the message</param>
        /// <param name="message">Message object as string</param>
        void WriteMessageToLocalQueue(string queueName, string message);

        /// <summary>
        /// Reads all messages from a local queue
        /// </summary>
        /// <param name="queueName">The name of the queue to read</param>
        /// <returns>A list of test connection messages</returns>
        List<string> ReadMessagesFromLocalQueue(string queueName);

        /// <summary>
        /// Read all messages from a AWS queue
        /// </summary>
        /// <param name="queueName">The name of the queue to read</param>
        /// <returns>A list of AWS messages</returns>
        List<Amazon.SQS.Model.Message> ReadMessagesFromAWSQueue(string queueName);

        /// <summary>
        /// wait for aws queue message
        /// </summary>
        /// <param name="queueName">The name of the queue where to wait for</param>
        /// <param name="parameter">The name of the parameter to wait for</param>
        /// <param name="value">The value of the parameter</param>
        /// <param name="waitingSeconds">The number of the seconds to wait</param>
        string WaitForAWSMessage(string queueName, string parameter, string value, int waitingSeconds);

        /// <summary>
        /// wait for local queue message
        /// </summary>
        /// <param name="queueName">The name of the queue where to wait for</param>
        /// <param name="parameter">The name of the parameter to wait for</param>
        /// <param name="value">The value of the parameter</param>
        /// <param name="waitingSeconds">The number of the seconds to wait</param>
        string WaitForLocalMessage(string queueName, string parameter, string value, int waitingSeconds);

    }
}
