﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMG.CoZone.Common.DomainModels;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMG.CoZone.Common.Interfaces
{
    public interface IDownloadService
    {
        Dictionary<int, string> GetDownloadPaths(FileDownloadModel model, int loggedUserId, Role.RoleName loggedAccountRole = Role.RoleName.AccountAdministrator);
    }
}
