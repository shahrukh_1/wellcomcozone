﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMG.CoZone.Common.DomainModels;
using GMGColorDAL.Common;

namespace GMG.CoZone.Common.Interfaces
{
    public interface IFtpService
    {
        Dictionary<string, string> GetDictionaryForFtpQueue(string groupKey, AppModule applicationModule, FTPAction ftpAction);
        Dictionary<string, string> GetDictionaryForFtpQueue(int tempObjId, AppModule applicationModule, FTPAction ftpAction);
        List<int> AddFtpPresetJobDownloads(List<FtpPresetJobDownloadModel> ftpPresetJobDownloadModel);
        void AddMessageToQueue(List<int> tempObjIdLists, string groupKey, AppModule fromAppModule);
    }
}
