﻿namespace GMG.CoZone.Common.Interfaces
{
    public interface IGMGConfiguration
    {
        //AWS
        bool IsEnabledS3Bucket { get; }
        string AWSAccessKey { get; }
        string AWSSecretKey { get; }
        string AWSRegion { get; }
        string AWSMailServer { get; }
        string AWSSMTPUserName { get; }
        string AWSSMTPPassword { get; }
        string AWSSNSMediaDefineResponseTopic { get; }
        bool IsEnabledAmazonSQS { get; }

        string ActiveConfiguration { get; }
        int LogLevel { get; }
        bool IsSendEmails { get; }
        int NumberOfEmailsToSend { get; }
        string DataFolderPrefix { get; }
        string MailServerType { get; }
        string MailServer { get; }
        string MailServerPort { get; }
        string CustomSmtpWriteQueueName { get; }
        string CustomSmtpReadQueueName { get; }
        string ServerProtocol { get; }
        string DomainUrl { get; }
        string MediaDefineResponseQueueName { get; }
        string PathToProcessingFolder { get; }

        string DataFolderName(string accountRegion);
        string PathToDataFolder(string accountRegion);

        //Media Processor
        int DefaultImageSize { get; }
        int HighResImageSize { get; }
    }
}
