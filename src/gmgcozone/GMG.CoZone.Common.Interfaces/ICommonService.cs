﻿using GMGColorDAL;

namespace GMG.CoZone.Common.Interfaces
{
    public interface ICommonService
    {
        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="accountId">User Account Id</param>
        /// <returns></returns>
        User GetUserByEmail(string email, int accountId);
        
        /// <summary>
        /// Get accounts details
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>Account object</returns>
        Account GetAccountById(int accountId);
    }
}
