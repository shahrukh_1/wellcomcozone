﻿using System.Collections.Generic;
using System.Drawing;

namespace GMG.CoZone.Common.Interfaces
{
    public interface IColorHelper
    {
        double GetNextAvailableColorInGrayscale(List<float> grayScaleColors);
        Color ColorFromHSV(double hue, double saturation, double value);
        string GetColorAsHex(Color c);
    }
}
