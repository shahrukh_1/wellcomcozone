﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMG.CoZone.Common.DomainModels;

namespace GMG.CoZone.Common.Interfaces
{
    public interface IModuleDownlaodService
    {
        void SetFtpDownloadPaths(FileDownloadModel model, int loggedUserId);
        Dictionary<int, string> GetDownloadPaths(FileDownloadModel model, int loggedUserId);
    }
}
