﻿using GMG.CoZone.Common.DomainModels;
using System.Web;

namespace GMG.CoZone.Common.Interfaces
{
    public interface IAWSSNSService
    {
        /// <summary>
        /// Publish approval status message to SNS Topic
        /// </summary>
        /// <param name="approvalId">The approval id</param>
        /// <param name="status">The approval status</param>
        void PublishStatus(string messageObj);

        /// <summary>
        /// Create new SNS Topic subscription
        /// </summary>
        void CreateSubscription(string domain);

        /// <summary>
        /// Process SNS Post request
        /// </summary>
        /// <param name="context">The http context</param>
        MediaDefineApprovalProgressStatusModel ProcessPost(HttpContext context);
    }
}
