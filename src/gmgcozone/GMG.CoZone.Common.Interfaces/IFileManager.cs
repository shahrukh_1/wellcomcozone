﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GMG.CoZone.Common.Interfaces
{
    public interface IFileManager
    {
        bool FileExists(string relativeFilePath, string accountRegion);

        Stream GetMemoryStreamOfAFile(string relativeFilePath, string accountRegion);

        bool DeleteFileWithBackup(string relativeFilePath, string accountRegion);

        bool DeleteFile(string relativeFilePath, string accountRegion);

        bool FolderExists(string relativeFolderPath, string accountRegion, bool isCreateIfNotExists = false, DateTime? expireDate = null);

        List<string> GetFoldersContent(string folderName, string accountRegion);

        void DeleteFolderContent(List<string> folderContent, string accountRegion);
    }
}
