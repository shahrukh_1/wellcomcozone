﻿using System;
using GMGColorDAL;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Collaborate.Interfaces.Annotation;

namespace GMG.CoZone.Collaborate.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GMGColorContext _context = new GMGColorContext();
        private IGenericRepository<Approval> _approvalRepository;
        private IGenericRepository<ApprovalCollaborator> _approvalCollaboratorRepository;
        private IApprovalAnnotationRepository _approvalAnnotationRepository;
        private IGenericRepository<ApprovalUserViewInfo> _approvalUserViewInfoRepository;
        private IGenericRepository<ApprovalCollaboratorRole> _approvalCollaboratorRoleoRepository;
        private IGenericRepository<SharedApproval> _sharedApprovalRepository;
        private IGenericRepository<ApprovalCollaboratorDecision> _approvalCollaboratorDecisionRepository;
        private IGenericRepository<ApprovalPage> _approvalPageRepository;
        private IGenericRepository<ApprovalJobPhaseApproval> _approvalJobPhaseApprovalRepository;
        private IGenericRepository<ApprovalJobPhase> _approvalJobPhaseRepository;
        private IGenericRepository<AccountSetting> _accountSettingsRepository;
        private IGenericRepository<ValueDataType> _valueDataTypeRepository;
        private IGenericRepository<Account> _accountRepository;


        public IGenericRepository<Account> AccountRepository
        {
            get { return _accountRepository ?? (new GenericRepository<Account>(_context)); }
        }

        public IGenericRepository<ValueDataType> ValueDataTypeRepository
        {
            get { return _valueDataTypeRepository ?? (new GenericRepository<ValueDataType>(_context)); }
        }

        public IGenericRepository<AccountSetting> AccountSettingsRepository
        {
            get { return _accountSettingsRepository ?? (new GenericRepository<AccountSetting>(_context)); }
        }

        public IGenericRepository<Approval> ApprovalRepository
        {
            get { return _approvalRepository ?? (new GenericRepository<Approval>(_context)); }
        }

        public IGenericRepository<ApprovalCollaborator> ApprovalCollaboratorRepository
        {
            get { return _approvalCollaboratorRepository ?? new GenericRepository<ApprovalCollaborator>(_context); }
        }

        public IApprovalAnnotationRepository ApprovalAnnotationRepository
        {
            get { return _approvalAnnotationRepository ?? new ApprovalAnnotationRepository(_context); }
        }

        public IGenericRepository<ApprovalUserViewInfo> ApprovalUserViewInfoRepository
        {
            get { return _approvalUserViewInfoRepository ?? new GenericRepository<ApprovalUserViewInfo>(_context); }
        }

        public IGenericRepository<ApprovalCollaboratorRole> ApprovalCollaboratorRoleRepository
        {
            get { return _approvalCollaboratorRoleoRepository ?? new GenericRepository<ApprovalCollaboratorRole>(_context); }
        }

        public IGenericRepository<SharedApproval> SharedApprovalRepository
        {
            get { return _sharedApprovalRepository ?? new GenericRepository<SharedApproval>(_context); }
        }
        public IGenericRepository<ApprovalCollaboratorDecision> ApprovalCollaboratorDecisionRepository
        {
            get { return _approvalCollaboratorDecisionRepository ?? new GenericRepository<ApprovalCollaboratorDecision>(_context); }
        }

        public IGenericRepository<ApprovalPage> ApprovalPageRepository
        {
            get { return _approvalPageRepository ?? new GenericRepository<ApprovalPage>(_context); }
        }
        public IGenericRepository<ApprovalJobPhaseApproval> ApprovalJobPhaseApprovalRepository
        {
            get { return _approvalJobPhaseApprovalRepository ?? new GenericRepository<ApprovalJobPhaseApproval>(_context); }
        }
        public IGenericRepository<ApprovalJobPhase> ApprovalJobPhaseRepository
        {
            get { return _approvalJobPhaseRepository ?? new GenericRepository<ApprovalJobPhase>(_context); }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

