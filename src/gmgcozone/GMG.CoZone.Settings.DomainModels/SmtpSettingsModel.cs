﻿namespace GMG.CoZone.Settings.DomainModels
{
    public class SmtpSettingsModel
    {
        public SmtpSettingsModel()
        {
            IsEnabled = false;
        }

        public string ServerName { get; set; }

        public int? Port { get; set; }

        public bool IsPasswordEnabled { get; set; }

        public bool IsEnabled { get; set; }

        public bool EnableSsl { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
