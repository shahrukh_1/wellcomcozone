﻿namespace GMG.CoZone.Settings.DomainModels
{
    public class AccountSettingsDataModel
    {
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public string AccountGuid { get; set; }
        public string SettingValueType { get; set; }
        public string AccountTempPath { get; set; }
        public string AccountRegion { get; set; }
    }
}
