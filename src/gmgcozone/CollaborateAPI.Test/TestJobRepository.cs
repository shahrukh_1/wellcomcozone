﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
using GMG.CollaborateAPI.BL;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.JobModels;
using GMG.CollaborateAPI.BL.Repositories;
using NUnit.Framework;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace CollaborateAPI.Test
{
    [TestFixture]
    public class TestJobRepository
    {
        [TestCase]
        public void Test_Start_Session()
        {
            #region Test Config

            var result = TestHelpers.StartSession();
            #endregion
            
            #region Test Run
            Assert.IsNotNull(result);
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpSession(result);
            #endregion
        }

        [TestCase]
        public void Test_Create_New_Job_Metadata()
        {
            #region Test Config
            //Initialization
            CreateJobMetadataResponse result;
            CreateJobRequest jobRequestModel;
            JobRepository repository;

            //Start Session
            var sessionKey = TestHelpers.StartSession();
            
            // Confirm Session
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);
            #endregion

            #region Test Run
            using (var context = new DbContextBL())
            {
                repository = new JobRepository(context);
                jobRequestModel = TestHelpers.CreateJobRequestData(sessionKey);
                result = (CreateJobMetadataResponse)repository.CreateJobMetadata(jobRequestModel, out TestHelpers.statusCodeEnum);

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCodeEnum.Ok, TestHelpers.statusCodeEnum);
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJobMetadata(result.resourceID);
            #endregion
        }

        [TestCase]
        public void Test_Create_Job()
        {
            #region Test Config
            const string filePath = "\\..\\..\\TestFiles\\TF_1.jpg";
            CreateJobResponse result;
            
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            #endregion

            #region Test Run
            result = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, filePath, TestHelpers.statusCodeEnum);
            
            //Asserts
            Assert.IsEmpty(result.warningMessages);
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(result.guid);
            #endregion
        }

        [TestCase]
        public void Test_Create_Approval_Version_Metadata()
        {
            #region Test Config

            CreateJobMetadataResponse result;
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);
            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                var approvalGuid = (from j in context.Jobs select j.Guid).FirstOrDefault();
                var repository = new JobRepository(context);
                var approvalData = TestHelpers.CreateApprovalRequest(sessionKey, approvalGuid);

                result = (CreateJobMetadataResponse)repository.CreateApprovalVersionMetadata(approvalData, out TestHelpers.statusCodeEnum);

                //Assert
                Assert.IsNotNull(result.resourceID);
                Assert.AreEqual(HttpStatusCodeEnum.Ok, TestHelpers.statusCodeEnum);
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJobMetadata(result.resourceID);
            #endregion
        }

        [TestCase]
        public void Test_Upload_Approval_File()
        {
            #region Test Config
            const string filePath = "\\..\\..\\TestFiles\\TF_2.jpg";

            //Start Session
            CreateJobResponse result;
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);
            #endregion

            #region Test Run

            using (var fileStream = new MemoryStream(File.ReadAllBytes(TestHelpers.appDomain + filePath)))
            {
                using (var context = new DbContextBL())
                {
                    var approvalGuid = (from j in context.Jobs select j.Guid).FirstOrDefault();
                    var repository = new JobRepository(context);
                    var approvalData = TestHelpers.CreateApprovalRequest(sessionKey, approvalGuid);
                    var job = (CreateJobMetadataResponse)repository.CreateApprovalVersionMetadata(approvalData, out TestHelpers.statusCodeEnum);
                    var model = new UploadApprovalFileRequest()
                    {
                        sessionKey = sessionKey,
                        resourceID = job.resourceID
                    };
                    result = (CreateJobResponse)repository.CreateJob(model, fileStream, out TestHelpers.statusCodeEnum);

                    //Assert
                    Assert.AreEqual(HttpStatusCodeEnum.Created, TestHelpers.statusCodeEnum);
                    Assert.IsEmpty(result.warningMessages);
                }
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(result.guid);
            #endregion
        }

        [TestCase]
        public void Test_Get_Jobs_Job_Not_Found()
        {
            #region Test Config
            var collabModel = new CollaborateAPIIdentityModel
            {
                guid = Guid.NewGuid().ToString()
            };
            #endregion

            #region Test Run
            using (var context = new DbContextBL())
            {
                var repository = new JobRepository(context);
                HttpStatusCodeEnum statusCodeEnum;
                var jsonResult = new JavaScriptSerializer().Serialize(repository.GetJob(collabModel, out statusCodeEnum));
                Assert.IsFalse(jsonResult.Contains("OK"));
            }
            #endregion

            #region Test Cleanup
            //Not Required
            #endregion
        }

        [TestCase]
        public void Test_Delete_Job()
        {
            #region Test Config
            const string filePath = "\\..\\..\\TestFiles\\TF_1.jpg";

            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create Job
            var newJob = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, filePath, TestHelpers.statusCodeEnum);
            var model = new CollaborateAPIIdentityModel()
            {
                sessionKey = sessionKey,
                guid = newJob.guid
            };
            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                var repository = new JobRepository(context);
                var result = (DeleteJobResponse)repository.DeleteJob(model, out TestHelpers.statusCodeEnum);

                Assert.AreEqual(newJob.guid, result.guid);
            }

            #endregion

            #region Test Cleanup
            //Not Required
            #endregion
        }

        [TestCase]
        public void Test_Get_Job()
        {
            #region Test Config
            const string filePath = "\\..\\..\\TestFiles\\TF_1.jpg";
            GetJobResponse jobResult;
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create Job
            var newJob = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, filePath, TestHelpers.statusCodeEnum);
            var model = new CollaborateAPIIdentityModel()
            {
                sessionKey = sessionKey,
                guid = newJob.guid
            };
            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                var repository = new JobRepository(context);
                var result = repository.GetJob(model, out TestHelpers.statusCodeEnum);
                
                //Assert
                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                jobResult = (GetJobResponse) result;
                Assert.AreEqual(model.guid, jobResult.jobGuid);
                Assert.IsNotEmpty(jobResult.jobTitle);
                Assert.IsNotNull(jobResult.approvals);
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResult.jobGuid);
            #endregion
        }

        [TestCase]
        public void Test_List_Jobs()
        {
            #region Test Config
            const string filePath = "\\..\\..\\TestFiles\\TF_1.jpg";
            const int jobNumber = 4;
            var jobList = new List<CreateJobResponse>();
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create jobs
            for (int i = 0; i < jobNumber; i++)
            {
                jobList.Add(TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, filePath, TestHelpers.statusCodeEnum));
            }

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                var repository = new JobRepository(context);
                var model = new CollaborateAPIBaseModel()
                {
                    sessionKey = sessionKey
                };

                var result = (ListJobsResponse)repository.ListJobs(model, out TestHelpers.statusCodeEnum);
                
                //Assert
                foreach (var job in jobList)
                {
                    var exists = result.jobs.Find(j => j.jobGuid == job.guid);
                    Assert.IsNotNull(exists);
                }
               
                Assert.IsNotNull(result.jobsCount);
                Assert.AreEqual(jobNumber, result.jobsCount);
                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
            }
            
            #endregion

            #region Test CleanUp

            foreach (var job in jobList)
            {
                TestHelpers.CleanUpJob(job.guid);
            }
            #endregion
        }

        [TestCase]
        public void Test_Update_Job()
        {
            #region Test Config

            //Start Session
            var sessionKey = TestHelpers.StartSession();
            const string filePath = "\\..\\..\\TestFiles\\TF_1.jpg";
            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);
            var jobRespone = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, filePath, TestHelpers.statusCodeEnum);
            UpdateJobResponse result;

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                var model = new UpdateJobRequest()
                {
                    sessionKey = sessionKey,
                    guid = jobRespone.guid,
                    jobTitle = "Update Job Title",
                    jobStatus = "CRQ"
                };
                var repository = new JobRepository(context);
                var errors = "";
                result = (UpdateJobResponse)repository.UpdateJob(model, out TestHelpers.statusCodeEnum, out errors);

                //Assert
                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                Assert.IsNotNull(result.jobGuid);
                Assert.AreEqual(result.jobGuid, jobRespone.guid);
            }

            #endregion

            #region Test CleanUp

            TestHelpers.CleanUpJob(result.jobGuid);

            #endregion
        }

        [TestCase]
        public void Test_GetStatus_Job()
        {
            #region Test Config
            const string filePath = "\\..\\..\\TestFiles\\TF_1.jpg";
            GetJobActivityResponse jobResult;
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create Job
            var newJob = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, filePath, TestHelpers.statusCodeEnum);
            var model = new CollaborateAPIIdentityModel()
            {
                sessionKey = sessionKey,
                guid = newJob.guid
            };
            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                var repository = new JobRepository(context);
                var result = repository.GetJobActivity(model, out TestHelpers.statusCodeEnum);

                //Assert
                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                jobResult = (GetJobActivityResponse)result;
                Assert.AreEqual(model.guid, jobResult.guid);
                Assert.IsNotNull(jobResult.approvals);
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResult.guid);
            #endregion
        }
    }
}