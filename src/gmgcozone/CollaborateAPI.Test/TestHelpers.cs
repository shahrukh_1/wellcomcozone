﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using GMG.CollaborateAPI.BL;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Repositories;
using GMGColorBusinessLogic;
using GMGColorDAL;
using NUnit.Framework;

namespace CollaborateAPI.Test
{
    public class TestHelpers
    {
        #region Proprieties
        public const string Password = "password";
        public static HttpStatusCodeEnum statusCodeEnum;
        public static readonly string appDomain = System.AppDomain.CurrentDomain.BaseDirectory;
        #endregion

        #region Methods
        
        public static string StartSession()
        {
            // Model Data
            var sessionModel = new StartSessionRequest()
            {
                username = "Client",
                accountURL = "client.gmgcozonelocal.com"
            };

            using (var context = new DbContextBL())
            {
                var userSession = new SessionRepository(context);
                HttpStatusCodeEnum statusEnum;
                var sesionKey = (StartSessionResponse)userSession.ProcessStartupRequest(sessionModel, out statusEnum);
                return sesionKey.sessionKey;
            }
        }

        public static void ConfirmSession(string sessionKey, string password)
        {
            string encodedHash, hashedAccessKey;
            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.GetEncoding(1252).GetBytes(password));
                encodedHash = Encoding.GetEncoding(1252).GetString(hash);
            }
            string accessKey = encodedHash + sessionKey;
            using (var hasher = new SHA256Managed())
            {
                hashedAccessKey = Convert.ToBase64String(hasher.ComputeHash(Encoding.UTF8.GetBytes(accessKey)));
            }

            using (var context = new DbContextBL())
            {
                var userSession = new SessionRepository(context);
                HttpStatusCodeEnum statusCodeEnum;
                var confirmReques = new ConfirmIdentityRequest()
                {
                    sessionKey = sessionKey,
                    accessKey = hashedAccessKey
                };
                var confirm = userSession.ProcessConfirmationRequest(confirmReques, out statusCodeEnum);
                Assert.IsNotNull(confirm);
                Assert.AreEqual(HttpStatusCodeEnum.Ok, statusCodeEnum);
            }
        }

        public static CreateJobRequest CreateJobRequestData(string sessionKey)
        {
            return new CreateJobRequest()
            {
                jobName = "Abstract-Rainbow-Colours-Wallpapers-5.jpg",
                approval = CreateApprovalRequest(sessionKey)
            };
        }

        public static CreateJobResponse CreateJob(string sessionKey, string appDomain, string filePath, HttpStatusCodeEnum statusCodeEnum)
        {
            using (var fileStream = new MemoryStream(File.ReadAllBytes(appDomain + filePath)))
            {
                using (var context = new DbContextBL())
                {
                    var repository = new JobRepository(context);
                    var jobData = CreateJobRequestData(sessionKey);
                    var jobMetaData = (CreateJobMetadataResponse)repository.CreateJobMetadata(jobData, out statusCodeEnum);
                    var fileModel = new UploadApprovalFileRequest()
                    {
                        sessionKey = sessionKey,
                        resourceID = jobMetaData.resourceID
                    };
                    return (CreateJobResponse)repository.CreateJob(fileModel, fileStream, out statusCodeEnum);
                }
            }
        }

        public static CreateApprovalRequest CreateApprovalRequest(string sessionKey, string guid = null)
        {
            return new CreateApprovalRequest()
            {
                fileName = "Abstract - Rainbow - Colours - Wallpapers - 5",
                fileType = "jpg",
                deadline = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                sessionKey = sessionKey,
                collaborators = new List<Collaborator>()
                    {
                        new Collaborator()
                        {
                            username = "Client",
                            approvalRole = "ANR",
                            email = "administrator.user-client@gmgcozone.com",
                            isExternal = false,
                            pdm = true
                        }
                    },
                folder = string.Empty,
                settings = new Settings()
                {
                    allowUsersToDownloadOriginalFile = true,
                    lockProofWhenAllDecisionsAreMade = true,
                    lockProofWhenFirstDecisionIsMade = false,
                    onlyOneDecisionRequired = false
                },
                guid = guid
            };
        }

        public static void CleanUpSession(string sessionKey)
        {
            using (var context = new DbContextBL())
            {
                var collaboratorApiSession = (from s in context.CollaborateAPISessions
                                              where s.SessionKey == sessionKey
                                              select s).FirstOrDefault();
                context.CollaborateAPISessions.Remove(collaboratorApiSession);
                context.SaveChanges();
            }
        }

        public static void CleanUpJobMetadata(string resourceId)
        {
            using (var context = new DbContextBL())
            {
                var jobMetadata = (from jm in context.CollaborateAPIJobMetadatas
                                   where jm.ResourceID == resourceId
                                   select jm).FirstOrDefault();
                context.CollaborateAPIJobMetadatas.Remove(jobMetadata);
                context.SaveChanges();
            }
        }

        public static string CleanUpJob(string guid)
        {
            using (var context = new DbContextBL())
            {
                var approval = (from j in context.Jobs
                                join a in context.Approvals on j.ID equals a.Job
                                where j.Guid == guid
                                select a).FirstOrDefault();

                return Approval.DeleteApproval(approval.ID, context);
            }
        }
        #endregion
    }
}
