﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using GMG.CollaborateAPI.BL;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.Approval_Models;
using GMG.CollaborateAPI.BL.Repositories;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal;

namespace CollaborateAPI.Test
{
    [TestFixture]
    public class TestApprovalRepository
    {
        #region Private

        const string _filePath = "\\..\\..\\TestFiles\\TF_1.jpg";

        #endregion

        [Test]
        public void Test_Update_Approval()
        {
            #region Test Config
            UpdateApprovalResponse result;
            CreateApprovalRequest versionData;
            CreateJobResponse version;
            var errors = "";

            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create job
            var jobResponse = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, _filePath, TestHelpers.statusCodeEnum);

            //Create version
            using (var fileStream = new MemoryStream(File.ReadAllBytes(TestHelpers.appDomain + _filePath)))
            {
                using (var context = new DbContextBL())
                {
                    var repository = new JobRepository(context);
                    versionData = TestHelpers.CreateApprovalRequest(sessionKey, jobResponse.guid);
                    var job = (CreateJobMetadataResponse)repository.CreateApprovalVersionMetadata(versionData, out TestHelpers.statusCodeEnum);
                    var model = new UploadApprovalFileRequest()
                    {
                        sessionKey = sessionKey,
                        resourceID = job.resourceID
                    };
                    version = (CreateJobResponse)repository.CreateJob(model, fileStream, out TestHelpers.statusCodeEnum);
                }
            }

            #endregion
                
            #region Test Run

            using (var context = new DbContextBL())
            {
                //Create repository instances
                var approvalRepo = new ApprovalRepository(context);

                //Get approval id
                var approvalID = (from j in context.Jobs
                    join a in context.Approvals on j.ID equals a.Job
                    where j.Guid == version.guid
                    select a.Guid).FirstOrDefault();

                if (version.guid == null) return;
                var model = new UpdateApprovalRequest()
                {
                    sessionKey = sessionKey,
                    guid = approvalID,
                    collaborators = versionData.collaborators,
                    deadline = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
                };
                var response = approvalRepo.UpdateApprovalVersion(model, out TestHelpers.statusCodeEnum, out errors);
                
                Assert.IsEmpty(errors);
                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                result = (UpdateApprovalResponse) response;
                Assert.IsNotNull(result.guid);
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(version.guid);
            #endregion
        }

        [Test]
        public void Test_Delete_Approval()
        {
            #region Test Config
            DeleteApprovalResponse result;
 
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create job
            var jobResponse = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, _filePath, TestHelpers.statusCodeEnum);

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                //Create repository instances
                var approvalRepo = new ApprovalRepository(context);

                if (jobResponse.approval.guid == null) return;
                var model = new CollaborateAPIIdentityModel()
                {
                    sessionKey = sessionKey,
                    guid = jobResponse.approval.guid,                   
                };
                var response = approvalRepo.DeleteApproval(model, out TestHelpers.statusCodeEnum);

                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                result = (DeleteApprovalResponse)response;
                Assert.IsNotNull(result.guid);
            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResponse.guid);
            #endregion
        }

        [Test]
        public void Test_Restore_Approval()
        {
            #region Test Config
            RestoreApprovalResponse result;

            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create job
            var jobResponse = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, _filePath, TestHelpers.statusCodeEnum);

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                //Create repository instances
                var approvalRepo = new ApprovalRepository(context);

                if (jobResponse.approval.guid == null) return;
                var model = new CollaborateAPIIdentityModel()
                {
                    sessionKey = sessionKey,
                    guid = jobResponse.approval.guid,
                };
                approvalRepo.DeleteApproval(model, out TestHelpers.statusCodeEnum);
                var response = approvalRepo.RestoreApproval(model, out TestHelpers.statusCodeEnum);

                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                result = (RestoreApprovalResponse)response;
                Assert.IsNotNull(result.guid);

            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResponse.guid);
            #endregion
        }

        [Test]
        public void Test_GetStatus_Approval()
        {
            #region Test Config
            GetApprovalActivityResponse result;

            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create job
            var jobResponse = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, _filePath, TestHelpers.statusCodeEnum);

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                //Create repository instances
                var approvalRepo = new ApprovalRepository(context);

                if (jobResponse.approval.guid == null) return;
                var model = new CollaborateAPIIdentityModel()
                {
                    sessionKey = sessionKey,
                    guid = jobResponse.approval.guid,
                };

                var response = approvalRepo.GetApprovalActivity(model, out TestHelpers.statusCodeEnum);

                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                result = (GetApprovalActivityResponse)response;
                Assert.IsNotNull(result.guid);

            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResponse.guid);
            #endregion
        }

        [Test]
        public void Test_Get_Approval()
        {
            #region Test Config
            GetApprovalResponse result;

            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create job
            var jobResponse = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, _filePath, TestHelpers.statusCodeEnum);

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                //Create repository instances
                var approvalRepo = new ApprovalRepository(context);

                if (jobResponse.approval.guid == null) return;
                var model = new CollaborateAPIIdentityModel()
                {
                    sessionKey = sessionKey,
                    guid = jobResponse.approval.guid,
                };

                //give time for the timeout to be processed (request fails if thumbnail doesn t exist)
                Thread.Sleep(TimeSpan.FromSeconds(5));
                var response = approvalRepo.GetApproval(model, out TestHelpers.statusCodeEnum);

                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                result = (GetApprovalResponse)response;
                Assert.IsNotNull(result.guid);

            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResponse.guid);
            #endregion
        }

        [Test]
        public void Test_GetWithAccess_Approval()
        {
            #region Test Config
            GetApprovalWithAccessResponse result;
            
            //Start Session
            var sessionKey = TestHelpers.StartSession();

            //Confirm Session 
            TestHelpers.ConfirmSession(sessionKey, TestHelpers.Password);

            //Create job
            var jobResponse = TestHelpers.CreateJob(sessionKey, TestHelpers.appDomain, _filePath, TestHelpers.statusCodeEnum);

            #endregion

            #region Test Run

            using (var context = new DbContextBL())
            {
                //Create repository instances
                var approvalRepo = new ApprovalRepository(context);

                if (jobResponse.approval.guid == null) return;
                var model = new CollaborateAPIIdentityModel()
                {
                    sessionKey = sessionKey,
                    guid = jobResponse.approval.guid,
                };

                var response = approvalRepo.GetApprovalWithAccess(model, out TestHelpers.statusCodeEnum);

                Assert.AreEqual(TestHelpers.statusCodeEnum, HttpStatusCodeEnum.Ok);
                result = (GetApprovalWithAccessResponse)response;
                Assert.IsNotNull(result.guid);

            }
            #endregion

            #region Test CleanUp
            TestHelpers.CleanUpJob(jobResponse.guid);
            #endregion
        }
    }
}
