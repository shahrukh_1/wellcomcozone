﻿namespace GMG.CoZone.SSO.DomainModels
{
    public class SsoSamlResponseModel
    {
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string IsPortalUser { get; set; }
    }
}
