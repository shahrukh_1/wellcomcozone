﻿namespace GMG.CoZone.SSO.DomainModels
{
    public class SsoSamlConfigurationModel
    {
        public SsoSamlSettingsModel AccountSsoSettingsModel { get; set; }
        public string AssertionConsumerServiceUrl { get; set; }

        public SsoSamlConfigurationModel()
        {
            AccountSsoSettingsModel = new SsoSamlSettingsModel();
        }
    }
}
