﻿namespace GMG.CoZone.SSO.DomainModels
{
    public class SsoSamlSettingsModel
    {
        public string EntityId { get; set; }
        public string IdpIssuerUrl { get; set; }
        public string CertificateName { get; set; }
        public string Type { get; set; }
    }
}
