﻿using GMG.CoZone.Common.Module.Enums;
using System;

namespace GMG.CoZone.SSO.DomainModels
{
    public class SsoModel
    {
        public SsoOptionsEnum ActiveConfiguration { get; set; }
        public SsoAuth0SettingsModel Auth0 { get; set; }
        public SsoSamlSettingsModel Saml { get; set; }

        public SsoModel()
        {
            Auth0 = new SsoAuth0SettingsModel() { Type = Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Auth0) };
            Saml = new SsoSamlSettingsModel() { Type = Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Saml) };
        }
    }
}
