﻿namespace GMG.CoZone.SSO.DomainModels
{
    public class SsoAuth0SettingsModel
    {
        public string Domain { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string Type { get; set; }
    }
}
