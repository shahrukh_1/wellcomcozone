﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GMG.CoZone.InteropWrapper
{
    public unsafe class MemoryManager
    {
        #region Constants
        const int HEAP_ZERO_MEMORY = 0x00000008;
        #endregion

        #region Constructors
        private MemoryManager()
        {
        }
        #endregion

        #region Methods
        public static void* Alloc(int size)
        {
            void* result = (void*)Marshal.AllocHGlobal(size);
            if (result == null)
            {
                throw new OutOfMemoryException();
            }
            return result;
        }

        public static void Free(void* block)
        {
            var blockToFree = (IntPtr)block;
            Marshal.FreeHGlobal(blockToFree);
            blockToFree = IntPtr.Zero;
            GC.SuppressFinalize(blockToFree);
        }
        #endregion
    }
}
