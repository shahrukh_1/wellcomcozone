﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GMG.CoZone.InteropWrapper
{
    [StructLayout(LayoutKind.Sequential)]
    public struct LabValue
    {
        public double L;
        public double a;
        public double b;
    }
}
