﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GMG.CoZone.InteropWrapper
{
    public unsafe struct HInstance
    {
        public IntPtr* _vftable;
    };

    public unsafe class RenderEngineObj : IDisposable
    {
        #region Members
        protected HInstance* mInstance = null;
        protected bool mSpaceAllocated = false;
        protected static bool? mIs32Bit = null;
        #endregion

        #region Invoke

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
            [In] IntPtr hProcess,
            [Out] out bool wow64Process
        );

        public static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Internal
        protected virtual int instanceSize()
        {
            return 0;
        }

        protected static bool Is32Bit()
        {
            return IntPtr.Size == 4;
        }

        #endregion

        #region Constructors
        protected internal RenderEngineObj(bool allocate)
        {
            mSpaceAllocated = allocate && instanceSize() > 0;
            if (mSpaceAllocated)
                mInstance = (HInstance*)MemoryManager.Alloc(instanceSize());
        }

        protected internal RenderEngineObj(HInstance* instance)
        {
            mSpaceAllocated = false;
            mInstance = instance;
        }
        #endregion

        #region Methods
        public virtual void Dispose()
        {
            if (mSpaceAllocated)
                MemoryManager.Free(mInstance);
            mInstance = null;
        }

        #endregion

        #region Operators
        public static bool operator ==(RenderEngineObj inst1, RenderEngineObj inst2)
        {
            try
            {
                bool inst1IsNull = System.Object.ReferenceEquals(inst1, null);
                bool inst2IsNull = System.Object.ReferenceEquals(inst2, null);
                if (inst1IsNull == inst2IsNull)
                    if (inst2IsNull == true)
                        return true;
                    else
                        return inst1.mInstance == inst2.mInstance;
            }
            catch (Exception)
            {
            }

            return false;
        }

        public static bool operator !=(RenderEngineObj inst1, RenderEngineObj inst2)
        {
            return !(inst1 == inst2);
        }

        public static implicit operator HInstance*(RenderEngineObj obj)
        {
            return obj != null ? obj.mInstance : null;
        }
        #endregion
    };
}
