﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GMG.CoZone.InteropWrapper
{
    public unsafe class ICCProfileManager : RenderEngineObj
    {
        #region Private Fields

        private GCHandle _profileDataHandler;

        #endregion

        #region PInvokes

        [DllImport(Settings.DllName32, EntryPoint = "??0ICCProfileManager@@QAE@PAXI@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctor32(HInstance* instance, IntPtr profileData, uint profileSize );
        [DllImport(Settings.DllName64, EntryPoint = "??0ICCProfileManager@@QEAA@PEAXI@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctor64(HInstance* instance, IntPtr profileData, uint profileSize );

        [DllImport(Settings.DllName32, EntryPoint = "??0ICCProfileManager@@QAE@PBD@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctorFromFile32(HInstance* instance, string profilePath);
        [DllImport(Settings.DllName64, EntryPoint = "??0ICCProfileManager@@QEAA@PEBD@Z", CallingConvention = CallingConvention.ThisCall)]
        private static extern int ctorFromFile64(HInstance* instance, string profilePath);


        [DllImport(Settings.DllName32, EntryPoint = "??1ICCProfileManager@@QAE@XZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern void dtor32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "??1ICCProfileManager@@QEAA@XZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern void dtor64(HInstance* instance);


        [DllImport(Settings.DllName32, EntryPoint = "?GetCalibrationDateTag@ICCProfileManager@@QAE_JXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern long _ReadCalibrationTimeTag32(HInstance* instance);
        [DllImport(Settings.DllName64, EntryPoint = "?GetCalibrationDateTag@ICCProfileManager@@QEAA_JXZ", CallingConvention = CallingConvention.ThisCall)]
        private static extern long _ReadCalibrationTimeTag64(HInstance* instance);

        [DllImport(Settings.DllName32, EntryPoint = "?GetProfileCreator@ICCProfileManager@@QAE_NPAG@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.I1)]
        private static extern bool _GetProfileCreator32(HInstance* instance, StringBuilder creatorSignature);
         [return: MarshalAs(UnmanagedType.I1)]
         [DllImport(Settings.DllName64, EntryPoint = "?GetProfileCreator@ICCProfileManager@@QEAA_NPEAG@Z", CallingConvention = CallingConvention.ThisCall, CharSet = CharSet.Unicode)]
        private static extern bool _GetProfileCreator64(HInstance* instance, StringBuilder creatorSignature);

         [DllImport(Settings.DllName32, EntryPoint = "?GetWhitePointLab@ICCProfileManager@@QAEXPAUcmsCIELab@@@Z", CallingConvention = CallingConvention.ThisCall)]
         private static extern void _GetWhitePointLab32(HInstance* instance, ref LabValue Lab);
         [DllImport(Settings.DllName64, EntryPoint = "?GetWhitePointLab@ICCProfileManager@@QEAAXPEAUcmsCIELab@@@Z", CallingConvention = CallingConvention.ThisCall)]
         private static extern void _GetWhitePointLab64(HInstance* instance, ref LabValue Lab);

         [DllImport(Settings.DllName32, EntryPoint = "?IsValid@ICCProfileManager@@QBE_NXZ", CallingConvention = CallingConvention.ThisCall)]
         [return: MarshalAs(UnmanagedType.I1)]
         private static extern bool _IsValid32(HInstance* instance);
         [return: MarshalAs(UnmanagedType.I1)]
         [DllImport(Settings.DllName64, EntryPoint = "?IsValid@ICCProfileManager@@QEBA_NXZ", CallingConvention = CallingConvention.ThisCall)]
         private static extern bool _IsValid64(HInstance* instance);

         [DllImport(Settings.DllName32, EntryPoint = "?IsValidAsSimulationProfile@ICCProfileManager@@QBE_NXZ", CallingConvention = CallingConvention.ThisCall)]
         [return: MarshalAs(UnmanagedType.I1)]
         private static extern bool _IsValidAsSimulationProfile32(HInstance* instance);
         [DllImport(Settings.DllName64, EntryPoint = "?IsValidAsSimulationProfile@ICCProfileManager@@QEBA_NXZ", CallingConvention = CallingConvention.ThisCall)]
         [return: MarshalAs(UnmanagedType.I1)]
         private static extern bool _IsValidAsSimulationProfile64(HInstance* instance);

         [DllImport(Settings.DllName32, EntryPoint = "?IsCMYKorRGB@ICCProfileManager@@QAE_NAA_NPAUcmsCIELab@@@Z", CallingConvention = CallingConvention.ThisCall)]
         [return: MarshalAs(UnmanagedType.I1)]
         private static extern bool _IsCMYKorRGB32(HInstance* instance, ref bool isRGB, ref LabValue Lab);
         [DllImport(Settings.DllName64, EntryPoint = "?IsCMYKorRGB@ICCProfileManager@@QEAA_NAEA_NPEAUcmsCIELab@@@Z", CallingConvention = CallingConvention.ThisCall)]
         [return: MarshalAs(UnmanagedType.I1)]
         private static extern bool _IsCMYKorRGB64(HInstance* instance, ref bool isRGB, ref LabValue Lab);

         [DllImport(Settings.DllName32, EntryPoint = "?CalculateDelta2000@ICCProfileManager@@SANPBUcmsCIELab@@0NNN@Z", CallingConvention = CallingConvention.Cdecl)]
         private static extern double _CalculateDelta200032(LabValue firstColor, LabValue secondColor, double Kl, double Kc, double Kh);
         [DllImport(Settings.DllName64, EntryPoint = "?CalculateDelta2000@ICCProfileManager@@SANPEBUcmsCIELab@@0NNN@Z", CallingConvention = CallingConvention.Cdecl)]
         private static extern double _CalculateDelta200064(LabValue firstColor, LabValue secondColor, double Kl, double Kc, double Kh);

        #endregion

        #region Internal
        protected override int instanceSize()
        {
            return sizeof(HInstance) + sizeof(IntPtr);
        }
        #endregion

        #region Constructors
        public ICCProfileManager(byte[] profileData)
            : base(true)
        {
            _profileDataHandler = GCHandle.Alloc(profileData, GCHandleType.Pinned);
            IntPtr profileDataPtr = _profileDataHandler.AddrOfPinnedObject();

            if (Is32Bit())
                ctor32(mInstance,  profileDataPtr, (uint)profileData.Length);
            else
                ctor64(mInstance,  profileDataPtr, (uint)profileData.Length);
        }

        public ICCProfileManager(string profilePath)
            : base(true)
        {
            if (Is32Bit())
                ctorFromFile32(mInstance, profilePath);
            else
                ctorFromFile64(mInstance, profilePath);
        }
        #endregion

        #region Methods

        public override void Dispose()
        {
            if (Is32Bit())
                dtor32(mInstance);
            else
                dtor64(mInstance);
            
            if (_profileDataHandler.IsAllocated)
            {
                _profileDataHandler.Free();
            }

            base.Dispose();
        }

        public DateTime ReadCalibrationTimeTag()
        {
            long timeStamp = Is32Bit() ?  _ReadCalibrationTimeTag32(mInstance) : _ReadCalibrationTimeTag64(mInstance);
            if (timeStamp != 0)
            {
                return new DateTime(1970, 1, 1).AddSeconds(timeStamp);
            }
            else
            {
                return default(DateTime);
            }
        }

        public string GetCreator()
        {
            var creatorSignature = new StringBuilder(255);
            if (Is32Bit())
            {
                _GetProfileCreator32(mInstance, creatorSignature);
            }
            else
            {
                _GetProfileCreator64(mInstance, creatorSignature);
            }

            return creatorSignature.ToString().Trim();
        }

        public LabValue GetWhitePointLab()
        {
            var whiteLab = new LabValue();

            if (Is32Bit())
            {
                _GetWhitePointLab32(mInstance, ref whiteLab);
            }
            else
            {
                _GetWhitePointLab64(mInstance, ref whiteLab);
            }

            return whiteLab;
        }

        public bool IsValid()
        {
            return Is32Bit()
               ? _IsValid32(mInstance)
               : _IsValid64(mInstance);
        }

        public bool IsValidAsSimulationProfile()
        {
            return Is32Bit()
               ? _IsValidAsSimulationProfile32(mInstance)
               : _IsValidAsSimulationProfile64(mInstance);
        }

        public bool IsCMYKorRGB(ref bool isRGB, ref LabValue whitePoint)
        {
            return Is32Bit()
               ? _IsCMYKorRGB32(mInstance, ref isRGB, ref whitePoint)
               : _IsCMYKorRGB64(mInstance, ref isRGB, ref whitePoint);
        }

        public static double CalculateDelta2000(LabValue firstColor, LabValue secondColor)
        {
            return Is32Bit()
                ? _CalculateDelta200032(firstColor, secondColor, 1, 1, 1)
                : _CalculateDelta200064(firstColor, secondColor, 1, 1, 1);
        }

        #endregion

        #region Utilities

        internal class Settings
        {
            public const string DllName32 = "SoftProofingRenderEngine_32.dll";
            public const string DllName64 = "SoftProofingRenderEngine_64.dll";
        };

        #endregion
    }
}
