﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Collaborate.DomainModels;
using GMG.CoZone.Common;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Repositories.Interfaces;
using GMGColor.Resources;
using GMGColorDAL;
using AppModule = GMG.CoZone.Common.AppModule;

namespace GMG.CoZone.Collaborate.Services
{
    public class CollaborateDownloadService : ICollaborateDownloadService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFtpService _ftpService;

        public CollaborateDownloadService(IUnitOfWork unitOfWork, IFtpService ftpService)
        {
            _unitOfWork = unitOfWork;
            _ftpService = ftpService;
        }

        public void SetFtpDownloadPaths(FileDownloadModel model, int loggedUserId)
        {
            if (_unitOfWork.UserRepository.IsApprovalFromCurrentAccount(model.FileIds, model.FolderIds, model.AccountId))
            {
                string groupKey = null;
                var approvalFileModels = model.FolderIds.Count > 0 ? GetApprovalsToDownloadFromFolder(model.FolderIds, loggedUserId, model.DownloadVersionTypes, model.ShowAllFilesToAdmins)
                                                                   : GetApprovalsToDownload(model.FileIds, loggedUserId, model.DownloadVersionTypes, model.ShowAllFilesToAdmins);

                if (approvalFileModels.Count() > 1)
                {
                    groupKey = Guid.NewGuid().ToString();
                }

                var jobIds = approvalFileModels.Select(a => a.Id).ToList();

                var ftpPresetJobDownloadModels = jobIds.Select(job => new FtpPresetJobDownloadModel { Preset = model.SelectedPresetId, GroupKey = groupKey, ApprovalJob = job }).ToList();
                var tempObjIdLists = _ftpService.AddFtpPresetJobDownloads(ftpPresetJobDownloadModels);

                _ftpService.AddMessageToQueue(tempObjIdLists, groupKey, AppModule.Collaborate);
            }
        }

        public Dictionary<int, string> GetDownloadPaths(FileDownloadModel model, int loggedUserId)
        {
            try
            {
                if (!_unitOfWork.UserRepository.IsApprovalFromCurrentAccount(model.FileIds, model.FolderIds, model.AccountId)
                    || !_unitOfWork.UserRepository.IsUserHasAccess(model.FileIds, loggedUserId))
                    return null;

                if (model.FolderIds.Count > 0)
                {
                    return GetDownloadPathsForApprovalsFromFolders(model.FolderIds, loggedUserId, model.DownloadVersionTypes, model.ShowAllFilesToAdmins);
                }

                return GetDownloadPathsForApprovals(model.FileIds, loggedUserId, model.DownloadVersionTypes, model.ShowAllFilesToAdmins);

            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, model.AccountId, "Error in Base Controller -> ApprovalBL -> GetApprovalDownloadPaths Method. Exception : {0} ", ex.Message);
                throw new Exception(Resources.txtFileDownloadError);
            }
        }

        private Dictionary<int, string> GetDownloadPathsForApprovals(List<int> fileIds, int loggedUserId, DownloadVersionTypes downloadVersionTypes, bool adminCanViewAllApprovals)
        {
            List<ApprovalFileModel> approvalFileModels = GetApprovalsToDownload(fileIds, loggedUserId, downloadVersionTypes, adminCanViewAllApprovals);

            //TODO this line must be uncommented after send notificcation functionality will be refatored. The return type of this method will be only List<string>. The logic bellow should be removed
            //return GetCollaborateJobPaths(approvalFileModels);

            return approvalFileModels.ToDictionary(approvalFileModel => approvalFileModel.Id, approvalFileModel => "approvals/" + approvalFileModel.Guid + "/" + approvalFileModel.FileName);
        }

        private List<ApprovalFileModel> GetApprovalsToDownload(List<int> fileIds, int loggedUserId, DownloadVersionTypes downloadVersionTypes, bool adminCanViewAllApprovals)
        {
            List<ApprovalFileModel> approvalFileModels;

            if (downloadVersionTypes == DownloadVersionTypes.CurrentVersion)
            {
                approvalFileModels = _unitOfWork.ApprovalRepository.GetApprovalsByIdAndCollaborator(fileIds, loggedUserId, adminCanViewAllApprovals);
            }
            else
            {
                var jobIds = _unitOfWork.ApprovalRepository.GetApprovalJobIds(fileIds);
                approvalFileModels = _unitOfWork.ApprovalRepository.GetApprovalVersionsByJobIdAndCollaborator(jobIds, loggedUserId, adminCanViewAllApprovals);
            }

            return approvalFileModels;
        }

        private Dictionary<int, string> GetDownloadPathsForApprovalsFromFolders(List<int> folderIds, int loggedUserId, DownloadVersionTypes downloadVersionTypes, bool adminCanViewAllApprovals)
        {
            var approvalFileModels = GetApprovalsToDownloadFromFolder(folderIds, loggedUserId, downloadVersionTypes, adminCanViewAllApprovals);

            //TODO this line must be uncommented after send notificcation functionality will be refatored. The return type of this method will be only List<string>. The logic bellow should be removed
            //return GetCollaborateJobPaths(approvalFileModels);

            return approvalFileModels.ToDictionary(approvalFileModel => approvalFileModel.Id, approvalFileModel => "approvals/" + approvalFileModel.Guid + "/" + approvalFileModel.FileName);
        }

        private List<ApprovalFileModel> GetApprovalsToDownloadFromFolder(List<int> folderIds, int loggedUserId, DownloadVersionTypes downloadVersionTypes, bool adminCanViewAllApprovals)
        {
            List<ApprovalFileModel> approvalFileModels = downloadVersionTypes == DownloadVersionTypes.CurrentVersion ? _unitOfWork.ApprovalRepository.GetMaxApprovalVersionsByFolderIds(folderIds, loggedUserId, adminCanViewAllApprovals)
                                                                                             : _unitOfWork.ApprovalRepository.GetApprovalVersionsByFolderIds(folderIds, loggedUserId, adminCanViewAllApprovals);
            return approvalFileModels;
        }

        private List<string> GetCollaborateJobPaths(List<ApprovalFileModel> approvalFileModels)
        {
            return approvalFileModels.Select(approval => "approvals/" + approval.Guid + "/" + approval.FileName).ToList();
        }
    }
}
