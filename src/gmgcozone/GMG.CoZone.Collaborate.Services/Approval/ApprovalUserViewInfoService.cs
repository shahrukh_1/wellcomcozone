﻿using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.Collaborate.Services.Approval
{
    public class ApprovalUserViewInfoService : IApprovalUserViewInfoService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ApprovalUserViewInfoService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int? GetLatestVisitedApproval(int userId)
        {
            return _unitOfWork.ApprovalUserViewInfoRepository.GetLatestVisitedApproval(userId);
        }
    }
}
