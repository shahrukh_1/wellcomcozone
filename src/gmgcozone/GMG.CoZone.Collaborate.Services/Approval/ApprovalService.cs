﻿using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Collaborate.DomainModels;
using GMG.CoZone.Collaborate.Services.Soad_State;
using GMG.CoZone.Collaborate.Interfaces;

using GMG.CoZone.Collaborate.DomainModels.Annotation;
using GMG.CoZone.Repositories.Interfaces;
using System;
using GMG.CoZone.Common.DomainModels;

namespace GMG.CoZone.Collaborate.Services.Approval
{
    public class ApprovalService : IApprovalService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ApprovalService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }        

        public List<ApprovalSoadModel> GetApprovalSoadDetails(List<int> approvals)
        {
            var approvalStateDto = _unitOfWork.ApprovalRepository.Get(a => approvals.Contains(a.ID))
                                                 .Select(app => new ApprovalSoadStateModel()
                                                 {
                                                     Approval = app.ID,
                                                     Phase = app.CurrentPhase,
                                                     ProgressState = app.SOADState
                                                 }).ToList();

            return GetApprovalProgressState(approvalStateDto);
        }
        
        public List<CollaboratorSoadModel> GetUserSoadDetialsForSpecifiedApproval(int approval)
        {
            List<int> inProgressOrCompletedPhases = new List<int>();
            var approvalCurrentPhase = _unitOfWork.ApprovalRepository.Get(filter: a => a.ID == approval)
                                                                     .Select(a => a.CurrentPhase).FirstOrDefault();
            if (approvalCurrentPhase.HasValue)
            {
                var currentPhase = _unitOfWork.ApprovalJobPhaseRepository.Get(filter: p => p.ID == approvalCurrentPhase.Value)
                                                                         .Select(p => new CurrentPhaseModel() { Position = p.Position, ApprovalJobWorkflow = p.ApprovalJobWorkflow })
                                                                         .FirstOrDefault();
                inProgressOrCompletedPhases = _unitOfWork.ApprovalJobPhaseRepository.Get(filter: p => p.Position <= currentPhase.Position && p.IsActive && p.ApprovalJobWorkflow == currentPhase.ApprovalJobWorkflow)
                                                                                    .Select(p => p.ID)
                                                                                    .ToList();
            }

            //var inProgressOrCompletedPhases 
            var userSoadStateModel = _unitOfWork.ApprovalCollaboratorRepository.Get(filter: acl => acl.Approval == approval && (!acl.Phase.HasValue || (acl.Phase.HasValue && inProgressOrCompletedPhases.Contains(acl.Phase.Value))))
                                                         .Select(acl => new CollaboratorSoadStateModel()
                                                         {
                                                             Approval = approval,
                                                             Phase = acl.Phase,
                                                             ProgressState = acl.SOADState,
                                                             CollaboratorId = acl.Collaborator
                                                         }).ToList();

            //add external collaborators
            userSoadStateModel.AddRange(_unitOfWork.SharedApprovalRepository.Get(filter: sa => sa.Approval == approval && (!sa.Phase.HasValue || (sa.Phase.HasValue && inProgressOrCompletedPhases.Contains(sa.Phase.Value))))
                                                         .Select(acl => new CollaboratorSoadStateModel()
                                                         {
                                                             Approval = approval,
                                                             Phase = acl.Phase,
                                                             ProgressState = acl.SOADState,
                                                             CollaboratorId = acl.ExternalCollaborator,
                                                             IsExternal = true
                                                         }).ToList());

            return GetCollaboratorProgressState(userSoadStateModel);
        }

        public List<ApprovalSoadModel> GetPhasesSoadDetialsForSpecifiedApproval(int approval)
        {
            var phaseStateModels = _unitOfWork.ApprovalJobPhaseApprovalRepository.Get(filter: a => a.Approval == approval).Select(ajpa => new ApprovalSoadStateModel()
            {
                Approval = ajpa.Approval,
                Phase = ajpa.Phase,
                ProgressState = ajpa.SOADState
            }).ToList();

            return GetApprovalProgressState(phaseStateModels);
        }

        private List<CollaboratorSoadModel> GetCollaboratorProgressState(List<CollaboratorSoadStateModel> collaboratorSoadStatesModels)
        {
            List<CollaboratorSoadModel> approvalSoadDtos = new List<CollaboratorSoadModel>();

            foreach (var collaboratorSoadStatesModel in collaboratorSoadStatesModels)
            {
                var state = ((SoadProgressState)collaboratorSoadStatesModel.ProgressState);
                SOADStateMachine soad = new SOADStateMachine(state, collaboratorSoadStatesModel.Approval, collaboratorSoadStatesModel.CollaboratorId, collaboratorSoadStatesModel.Phase, collaboratorSoadStatesModel.IsExternal);
                soad.StartProgress();

                approvalSoadDtos.Add(new CollaboratorSoadModel()
                {
                    Approval = collaboratorSoadStatesModel.Approval,
                    Phase = collaboratorSoadStatesModel.Phase,
                    ProgressStateClass = soad.StateCssClass,
                    Collaborator = collaboratorSoadStatesModel.CollaboratorId,
                    IsExternal = collaboratorSoadStatesModel.IsExternal
                });
            }
            return approvalSoadDtos;
        }

        private List<ApprovalSoadModel> GetApprovalProgressState(List<ApprovalSoadStateModel> approvalStateDtos)
        {
            List<ApprovalSoadModel> approvalSoadDtos = new List<ApprovalSoadModel>();

            foreach (var approvalStateDto in approvalStateDtos)
            {
                var state = ((SoadProgressState)approvalStateDto.ProgressState);
                SOADStateMachine soad = new SOADStateMachine(state, approvalStateDto.Approval, approvalStateDto.Phase);
                soad.StartProgress();

                approvalSoadDtos.Add(new ApprovalSoadModel()
                {
                    Approval = approvalStateDto.Approval,
                    Phase = approvalStateDto.Phase,
                    ProgressStateClass = soad.StateCssClass
                });
            }
            return approvalSoadDtos;
        }

        public Dictionary<int, int> GetNotViewedAnnotations(List<int> ids, int iD)
        {
            var approvalLastOpenedDate = _unitOfWork.ApprovalUserViewInfoRepository.Get(filter: uv => ids.Contains(uv.Approval) && uv.User == iD).GroupBy(x => x.Approval, (key, g) => g.OrderByDescending(e => new { e.ViewedDate }).FirstOrDefault()).Select(item => new ApprovalLastOpenedDate { Approval = item.Approval, ViewedDate = item.ViewedDate }).ToList();
            var annotations = _unitOfWork.ApprovalPageRepository.Get(filter: ap => ids.Contains(ap.Approval) && ap.ApprovalAnnotations.Count() > 0, includeProperties: "ApprovalAnnotations").Select(a => new AnnotationsModel { Approval = a.Approval, NotViewed = a.ApprovalAnnotations.Where(ann => ann.Modifier != iD).Select(annotation => annotation.ModifiedDate) }).ToList();

            var dictionary = new Dictionary<int, int>();
            foreach (var annotation in annotations)
            {
                var lastOpenedDate = approvalLastOpenedDate.FirstOrDefault(al => al.Approval == annotation.Approval);
                var notViewedAnnotationsCount = lastOpenedDate != null ? annotation.NotViewed.Count(v => v >= lastOpenedDate.ViewedDate) : annotation.NotViewed.Count();
                if (notViewedAnnotationsCount > 0)
                {
                    dictionary.Add(annotation.Approval, notViewedAnnotationsCount);
                }               
            }
            return dictionary;
        }

        public GMGColorDAL.Approval.ApprovalTypeEnum GetApprovalType(int approvalId)
        {            
            return _unitOfWork.ApprovalRepository.GetApprovalType(approvalId);
        }

        public int? GetApprovalIdForTranscoding(string transcodingJobId)
        {
            return _unitOfWork.ApprovalRepository.GetApprovalIdForTranscoding(transcodingJobId);
        }

        public string GetVideoThumbnailPath(int approvalId)
        {
            var approvalInfo = _unitOfWork.AccountRepository.GetApprovalInfo(approvalId);
            return _unitOfWork.ApprovalRepository.GetVideoThumbnailPath(approvalInfo);
        }

        public void ResetApprovalProcessingDate(int approvalId)
        {
            _unitOfWork.ApprovalRepository.ResetApprovalProcessingDate(approvalId);
        }

        public ApprovalProcessingModel GetApprovalProcessingData(int approvalId)
        {
            var dbObject = _unitOfWork.ApprovalRepository.GetByID(approvalId);
            return new ApprovalProcessingModel
            {
                ID = dbObject.ID,
                ProcessingInProgress = dbObject.ProcessingInProgress,
                RestrictedZoom = dbObject.RestrictedZoom,
                ScreenshotUrl = dbObject.ScreenshotUrl,
                WebPageSnapshotDelay = dbObject.WebPageSnapshotDelay
            };
        }
    }
}
