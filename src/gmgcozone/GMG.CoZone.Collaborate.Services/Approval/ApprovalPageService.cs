﻿using GMG.CoZone.Collaborate.Interfaces.Proofstudio;
using GMG.CoZone.Repositories;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.Collaborate.Services.Approval
{
    public class ApprovalPageService : IApprovalPageService
    {
        private readonly IUnitOfWork _unitOfWork = new UnitOfWork();

        public int GetApprovalFirstPageId(int approvalId)
        {
            return _unitOfWork.ApprovalPageRepository.GetApprovalFirstPageId(approvalId);
        }
    }
}
