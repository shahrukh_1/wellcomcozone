﻿using System;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Collaborate.Repositories;
using GMGColorDAL;
using System.Linq;
using GMG.CoZone.Collaborate.Models;

namespace GMG.CoZone.Collaborate.Services.Settings
{
    /// <summary>
    /// Class that implements the ISettingsService
    /// </summary>
    public class SettingsService : ISettingsService
    {
        private readonly IUnitOfWork _unitOfWork = new UnitOfWork();
        private ISettingsEmailSenderService _settingsEmailSenderService = new SettingsEmailSenderService();

        /// <summary>
        /// Get the Account settings using the AccountSettings repository
        /// </summary>
        /// <param name="loggedAccount">The acccount id</param>
        /// <param name="accountSettingsKey">The account settings Key</param>
        /// <returns>The settings type key</returns>
        public string GetAccountSettings(int loggedAccount, string accountSettingsKey)
        {
            return _unitOfWork.AccountSettingsRepository.Get(filter: vtp => vtp.Name == accountSettingsKey && vtp.Account == loggedAccount).Select(vtp => vtp.Value).FirstOrDefault();
        }

        /// <summary>
        /// Add or Update the account settings
        /// </summary>
        /// <param name="settings">User changes for the account settings</param>
        /// <param name="loggedAccount">The acccount id</param>
        /// <param name="accountSettingsKey">The account settings Key</param>
        /// <param name="valueTypeKey">The settings type key</param>
        public void AddUpdateSmtAccountSettings(string settings, int loggedAccount, string accountSettingsKey, string valueTypeKey)
        {
            var existingSmtpSettings = _unitOfWork.AccountSettingsRepository.Get(filter: vtp => vtp.Account == loggedAccount && vtp.Name == accountSettingsKey).FirstOrDefault();
            if (existingSmtpSettings == null)
            {
                _unitOfWork.AccountSettingsRepository.Add(new AccountSetting
                {
                    Account = loggedAccount,
                    Name = accountSettingsKey,
                    ValueDataType = _unitOfWork.ValueDataTypeRepository.Get(filter: vtp => vtp.Key == valueTypeKey).Select(vtp => vtp.ID).FirstOrDefault(),
                    Description = accountSettingsKey,
                    Value = settings,
                    CreatedDate = DateTime.UtcNow,
                    ModifiedData = DateTime.UtcNow,
                    Creator = loggedAccount,
                    Modifier = loggedAccount
                });
            }
            else
            {
                existingSmtpSettings.Value = settings;
                existingSmtpSettings.Modifier = loggedAccount;
                existingSmtpSettings.ModifiedData = DateTime.UtcNow;
                _unitOfWork.AccountSettingsRepository.Update(existingSmtpSettings);
            }
            _unitOfWork.Save();
        }

        /// <summary>
        /// Disable/Delete account SMPT configuration settings
        /// </summary>
        /// <param name="loggedAccount">The acccount id</param>
        /// <param name="accountSettingsKey">The account settings Key</param>
        public void DisableSmtpAccountSettings(int loggedAccount, string accountSettingsKey)
        {
            var entityIdToDelete = _unitOfWork.AccountSettingsRepository.Get(filter: vtp => vtp.Name == accountSettingsKey && vtp.Account == loggedAccount).Select(vtp => vtp.ID).FirstOrDefault();
            if (entityIdToDelete != 0)
            {
                _unitOfWork.AccountSettingsRepository.Delete(entityIdToDelete);
                _unitOfWork.Save();
            }
        }

        /// <summary>
        /// Test the Custom SMTP settings by sending a email using ISettingsEmailSenderService
        /// </summary>
        /// <param name="customSmtpTestSettings">The SMTP settings</param>
        /// <param name="loggedAccount">The acccount id</param>
        /// <param name="emailLogoPath">The account settings Key</param>
        public string TestSmtpAccountSettings(CustomSmtpTestModel customSmtpTestSettings, int loggedAccount)
        {
            var accountDetails = _unitOfWork.AccountRepository.GetByID(loggedAccount);

            customSmtpTestSettings.EmailAccountData.Domain = accountDetails.Domain;
            customSmtpTestSettings.EmailAccountData.AccountName = accountDetails.Name;

            return _settingsEmailSenderService.SendEmail(customSmtpTestSettings);
        }
    }
}
