﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Collaborate.Interfaces.Annotation;
using GMG.CoZone.Collaborate.DomainModels.Annotation;
using GMGColorDAL.Common;
using GMG.CoZone.Repositories.Interfaces;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;

namespace GMG.CoZone.Collaborate.Services.Annotation
{
    public class AnnotationsInReportService: IAnnotationsInReportService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AnnotationsInReportService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// Returns annotations for Individual report type
        public List<IndividualAnnotationModel> GetReportAnnotationsOnGroups(int[] groupAnnotationIds, int[] annotationIdsToLoad, string datePattern, string timezone, int timeFormat)
        {
            IApprovalAnnotationRepository approvalAnnotationsRepository = (IApprovalAnnotationRepository)_unitOfWork.ApprovalAnnotationRepository;

            var shapeAndMarkerAnnotations = approvalAnnotationsRepository.GetShapeAndMarkerAnnotations(groupAnnotationIds).ToList();

            String[] spearator = { "</p>" };
            foreach (var ann in shapeAndMarkerAnnotations)
            {
                ann.comment = ann.Comment.Split(spearator, StringSplitOptions.None).ToList();
                if (ann.comment[ann.comment.Count - 1] == "")
                {
                    ann.comment.RemoveAt(ann.comment.Count - 1);
                }
                if (ann.Replies.Count > 0)
                {
                    foreach (var rep in ann.Replies)
                    {
                        rep.comment = rep.Comment.Split(spearator, StringSplitOptions.None).ToList();
                        if (rep.comment[rep.comment.Count - 1] == "")
                        {
                            rep.comment.RemoveAt(rep.comment.Count - 1);
                        }
                    }
                }
            }

            var markerAnnotations = approvalAnnotationsRepository.GetMarkerAnnotations(groupAnnotationIds).ToList();

            foreach (var ann in markerAnnotations)
            {
                ann.comment = ann.Comment.Split(spearator, StringSplitOptions.None).ToList();
                if (ann.comment[ann.comment.Count - 1] == "")
                {
                    ann.comment.RemoveAt(ann.comment.Count - 1);
                }
                if (ann.Replies.Count > 0)
                {
                    foreach (var rep in ann.Replies)
                    {
                        rep.comment = rep.Comment.Split(spearator, StringSplitOptions.None).ToList();
                        if (rep.comment[rep.comment.Count - 1] == "")
                        {
                            rep.comment.RemoveAt(rep.comment.Count - 1);
                        }
                    }
                }
            }

            var annotationsInfo = shapeAndMarkerAnnotations.Union(markerAnnotations);

            var orderedByCreatedDate = approvalAnnotationsRepository.GetAnnotationsOrderedbyCreatedDay(annotationIdsToLoad);

            var orderedList = ExtractSingleAndOrderedAnnotations(annotationsInfo, groupAnnotationIds);

            return FormatDateAndPutAnnotationIndex(orderedList, orderedByCreatedDate, datePattern, timezone, timeFormat);
        }

        private List<IndividualAnnotationModel> ExtractSingleAndOrderedAnnotations(IEnumerable<IndividualAnnotationModel> annotationsInfo, int[] groupAnnotationIds)
        {
            var orderedList = new List<IndividualAnnotationModel>();

            foreach (int id in groupAnnotationIds)
            {
                orderedList.Add(annotationsInfo.SingleOrDefault(t => t.ID == id));
            }

            return orderedList;

        }

        private List<IndividualAnnotationModel> FormatDateAndPutAnnotationIndex(List<IndividualAnnotationModel> orderedList, IEnumerable<IGrouping<int, int>> orderedByCreatedDate, string datePattern, string timezone, int timeFormat)
        {
            foreach (IndividualAnnotationModel annotation in orderedList)
            {                
                annotation.AnnotatedDate = FormatAnnotatedDate(annotation.UTCCreatedDate, datePattern, timezone, timeFormat);

                foreach (var reply in annotation.Replies)
                {
                    reply.AnnotatedDate = FormatAnnotatedDate(reply.UTCReplyDate, datePattern, timezone, timeFormat);
                }

                //get the index of the current annotation within the document
                foreach (var item in orderedByCreatedDate.SelectMany(group => group.Select((x, i) => new { Value = x, Index = i }).Where(item => item.Value == annotation.ID)))
                {
                    annotation.AnnotationIndex = item.Index + 1;
                }
            }

            return orderedList;
        }

        private string FormatAnnotatedDate(DateTime dtUtcTime, string datePattern, string timezone, int timeFormat)
        {
            DateTime annotationTime = GMGColorFormatData.GetUserTimeFromUTC(dtUtcTime, timezone);
            return GMGColorFormatData.GetFormattedDate(annotationTime, datePattern) + " " +
                GMGColorFormatData.GetFormattedTime(annotationTime, timeFormat);
        }
    }
}
