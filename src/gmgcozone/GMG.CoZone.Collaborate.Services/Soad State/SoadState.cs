﻿namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public abstract class SoadState
    {
        public SOADStateMachine SoadStateMachine { get; set; }
        public int ApprovalId { get; set; }
        public int? PhaseId { get; set; }
        public int? CollaboratorId { get; set; }
        public bool IsExternal { get; set; }

        public abstract string GetNextState();
    }
}
