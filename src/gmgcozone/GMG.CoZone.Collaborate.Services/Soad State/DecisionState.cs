﻿using GMG.CoZone.Collaborate.Interfaces;

namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public class DecisionState : SoadState
    {
        private readonly ISoadStateService _soadStateService = new SoadStateService();

        private readonly bool _updateStateWithoutCheck;

        // This constructor will create new state taking values from old state
        public DecisionState(SoadState state, bool updateStateWithoutCheck = false)
            : this(state.SoadStateMachine, state.ApprovalId, state.PhaseId, state.CollaboratorId, state.IsExternal, updateStateWithoutCheck)
        {

        }

        // this constructor will be used by the other one
        public DecisionState(SOADStateMachine approvalSoad, int approvalId, int? phaseId, int? userId, bool isExternal, bool updateStateWithoutCheck)
        {
            SoadStateMachine = approvalSoad;
            ApprovalId = approvalId;
            PhaseId = phaseId;
            CollaboratorId = userId;
            IsExternal = isExternal;
            _updateStateWithoutCheck = updateStateWithoutCheck;
        }
      
        public override string GetNextState()
        {
            SoadStateMachine.NeedDoMoveToNextState = false;
            var state = (int)SoadProgressState.MadeAnnotation;

            int collaboratorRoleID = _soadStateService.GetANRCollaboratorRoleID();

            if (_updateStateWithoutCheck || !SoadStateMachine.IsUserSoad && _soadStateService.AllCollaboratorsHaveMadeDecisions(ApprovalId, PhaseId, collaboratorRoleID) 
                || SoadStateMachine.IsUserSoad && _soadStateService.CollaboratorHasMadeDecision(ApprovalId, PhaseId, CollaboratorId, IsExternal))
            {
                SoadStateMachine.StateCssClass = "decision-state";
                state = (int)SoadProgressState.MadeDecision;
            }

            if (SoadStateMachine.UpdateStateInDatabase && SoadStateMachine.LastSavedState != state)
            {
                if (SoadStateMachine.IsUserSoad)
                {
                    UpdateUserState(state);
                }
                else
                {
                    UpdateState(state);
                }
            }
            return SoadStateMachine.StateCssClass;
        }

        private void UpdateState(int state)
        {
            _soadStateService.SaveApprovalSoadState(ApprovalId, state);
            if (PhaseId != null)
            {
                _soadStateService.SavePhaseSoadState(ApprovalId, PhaseId.GetValueOrDefault(), state);
            }
        }

        private void UpdateUserState(int state)
        {
            _soadStateService.SaveUserSoadState(ApprovalId, CollaboratorId.GetValueOrDefault(), state, PhaseId, IsExternal);
        }
    }
}
