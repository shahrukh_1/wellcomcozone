﻿using GMG.CoZone.Collaborate.Interfaces;

namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public class AnnotationState : SoadState
    {
        private readonly ISoadStateService _soadStateService = new SoadStateService();

        // This constructor will create new state taking values from old state
        public AnnotationState(SoadState state)
            : this(state.SoadStateMachine, state.ApprovalId, state.PhaseId, state.CollaboratorId, state.IsExternal)
        {

        }

        // this constructor will be used by the other one
        public AnnotationState(SOADStateMachine approvalSoad, int approvalId, int? phaseId, int? userId, bool isExternal)
        {
            SoadStateMachine = approvalSoad;
            ApprovalId = approvalId;
            PhaseId = phaseId;
            CollaboratorId = userId;
            IsExternal = isExternal;
        }

        public override string GetNextState()
        {
            var state = (int)SoadProgressState.Opened;
            SoadStateMachine.NeedDoMoveToNextState = false;

            int collaboratorRoleID = _soadStateService.GetANRCollaboratorRoleID();
            int[] collaboratorRolesListID = _soadStateService.GetANRAndRVWCollaboratorRolesListID();

            if (!SoadStateMachine.IsUserSoad && _soadStateService.AllCollaboratorsMadeAnnotations(ApprovalId, PhaseId, collaboratorRolesListID)
                || SoadStateMachine.IsUserSoad && _soadStateService.CollaboratorMadeAnnotation(ApprovalId, PhaseId, CollaboratorId, IsExternal))
            {
                SoadStateMachine.StateCssClass = "annotation-state";
                GoToNextState();
            }
            else if (!SoadStateMachine.IsUserSoad && _soadStateService.AllCollaboratorsHaveMadeDecisions(ApprovalId, PhaseId, collaboratorRoleID) ||
                SoadStateMachine.IsUserSoad && _soadStateService.CollaboratorHasMadeDecision(ApprovalId, PhaseId, CollaboratorId, IsExternal))
            {
                GoToNextState(true);
            }
            else if (SoadStateMachine.UpdateStateInDatabase && SoadStateMachine.LastSavedState != state)
            {
                if (SoadStateMachine.IsUserSoad)
                {
                    UpdateUserState(state);
                }
                else
                {
                    UpdateState(state);
                }
            }
            
            return SoadStateMachine.StateCssClass;
        }

        private void UpdateState(int state)
        {
            _soadStateService.SaveApprovalSoadState(ApprovalId, state);
            if (PhaseId != null)
            {
                _soadStateService.SavePhaseSoadState(ApprovalId, PhaseId.GetValueOrDefault(), state);
            }
        }

        private void UpdateUserState(int state)
        {
            _soadStateService.SaveUserSoadState(ApprovalId, CollaboratorId.GetValueOrDefault(), state, PhaseId, IsExternal);
        }

        private void GoToNextState(bool updateStateWithoutCheck = false)
        {
            SoadStateMachine.CurrentState = new DecisionState(this, updateStateWithoutCheck);
            SoadStateMachine.NeedDoMoveToNextState = true;
            SoadStateMachine.UpdateStateInDatabase = true;
        }
    }
}
