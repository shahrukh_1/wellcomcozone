﻿using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Repositories.Interfaces;
using GMG.CoZone.Repositories;
using GMG.CoZone.Collaborate.DomainModels;

namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public class SoadStateService : ISoadStateService
    {
        private readonly IUnitOfWork _unitOfWork = new UnitOfWork();

        public bool IsApprovalOpenedByAllCollaborators(int approvalId, int? phaseId)
        {
            var approval = _unitOfWork.ApprovalRepository.Get(filter: a => a.ID == approvalId,
                                                              orderBy: null,
                                                              includeProperties: "ApprovalCollaborators, ApprovalUserViewInfoes, SharedApprovals")
                                                         .Select(a => new
                                                         {
                                                             InternalCollaboratorsThatOpened = a.ApprovalUserViewInfoes.Where(auv => auv.Phase == phaseId && auv.User.HasValue).Select(auv => auv.User.Value).Distinct(),
                                                             ExternalCollaboratorsThatOpened = a.ApprovalUserViewInfoes.Where(auv => auv.Phase == phaseId && auv.ExternalUser.HasValue).Select(auv => auv.ExternalUser.Value).Distinct(),
                                                             InternalCollaborators = a.ApprovalCollaborators.Where(ac => ac.Phase == phaseId).Select(ac => ac.Collaborator),
                                                             ExternalCollaborators = a.SharedApprovals.Where(sha => sha.Phase == phaseId).Select(sha => sha.ExternalCollaborator)
                                                         }).FirstOrDefault();
            if (approval != null)
            {
                var internalCollaboratorsThatOpened = new HashSet<int>(approval.InternalCollaboratorsThatOpened);
                var externalCollaboratorsThatOpened = new HashSet<int>(approval.ExternalCollaboratorsThatOpened);

                var internalCollaborators = new HashSet<int>(approval.InternalCollaborators);
                var externalCollaborators = new HashSet<int>(approval.ExternalCollaborators);

                var allInternalCollaboratorsHaveOpened = internalCollaboratorsThatOpened.SetEquals(internalCollaborators);
                var allExternalCollaboratorsHaveOpened = externalCollaboratorsThatOpened.SetEquals(externalCollaborators);

                return (allExternalCollaboratorsHaveOpened && allInternalCollaboratorsHaveOpened);
            }

            return false;
        }
        
        public bool AllCollaboratorsMadeAnnotations(int approvalId, int? phaseId, int[] collaboratorsRoleIds)
        {
            var collaboratorsAndAnnotations = _unitOfWork.ApprovalCollaboratorRepository.GetAllCollaboratorsAndAnnotations(approvalId, phaseId, collaboratorsRoleIds);

            if (collaboratorsAndAnnotations != null)
            {
                var internalAnnotationCreators = new HashSet<int>(collaboratorsAndAnnotations.InternalAnnotationsCreators);
                var externalAnnotationCreators = new HashSet<int>(collaboratorsAndAnnotations.ExternalAnnotationsCreators);

                var internalCollaborators = new HashSet<int>(collaboratorsAndAnnotations.InternalCollaborators);
                var externalCollaborators = new HashSet<int>(collaboratorsAndAnnotations.ExternalCollaborators);

                var allInternalCollaboratorsMadeAnnotations = internalAnnotationCreators.SetEquals(internalCollaborators);
                var allExternalCollaboratorsMadeAnnotations = externalAnnotationCreators.SetEquals(externalCollaborators);

                return (allExternalCollaboratorsMadeAnnotations && allInternalCollaboratorsMadeAnnotations);
            }
            return false;
        }
        
        public bool AllCollaboratorsHaveMadeDecisions(int approvalId, int? phaseId, int collaboratorRoleId)
        {
            var collaboratorsAndDecisions = _unitOfWork.ApprovalCollaboratorRepository.GetAllCollaboratorsAndDecisions(approvalId, phaseId, collaboratorRoleId);

            if (collaboratorsAndDecisions != null)
            {
                var internalCollaboratorsDecision = new HashSet<int>(collaboratorsAndDecisions.InternalCollaboratorsDecision);
                var externalCollaboratorsDecision = new HashSet<int>(collaboratorsAndDecisions.ExternalCollaboratorsDecision);

                var internalCollaborators = new HashSet<int>(collaboratorsAndDecisions.InternalCollaborators);
                var externalCollaborators = new HashSet<int>(collaboratorsAndDecisions.ExternalCollaborators);

                var allExternalCollaboratorsMadeDecisions = internalCollaboratorsDecision.SetEquals(internalCollaborators);
                var allInternalCollaboratorsMadeDecisions = externalCollaboratorsDecision.SetEquals(externalCollaborators);
                
                return (allExternalCollaboratorsMadeDecisions && allInternalCollaboratorsMadeDecisions);
            }

            return false;
        }

        public bool CollaboratorMadeAnnotation(int approvalId, int? phaseId, int? collaboratorId, bool isExternal)
        {
            var annotation = _unitOfWork.ApprovalPageRepository.Get(filter: ap => ap.Approval == approvalId,
                                                               orderBy: null,
                                                               includeProperties: "ApprovalAnnotations")
                                                          .Select(a => a.ApprovalAnnotations.FirstOrDefault(an => an.Phase == phaseId && ((isExternal && an.ExternalCreator == collaboratorId) || an.Creator == collaboratorId)))
                                                          .FirstOrDefault();

            return annotation != null;
        }

        public bool IsApprovalOpenedByCollaborator(int approvalId, int? phaseId, int? userId, bool isExternal = false)
        {
            return _unitOfWork.ApprovalUserViewInfoRepository.Get(filter: a => a.Approval == approvalId && a.Phase == phaseId && ((isExternal && a.ExternalUser == userId) || a.User == userId)).Any();
        }

        public bool CollaboratorHasMadeDecision(int approvalId, int? phaseId, int? collaborateId, bool isExternal)
        {
            return  _unitOfWork.ApprovalCollaboratorDecisionRepository.Get(filter: acd => acd.Approval == approvalId 
                                                                                   && acd.Phase == phaseId 
                                                                                   && ((isExternal && acd.ExternalCollaborator == collaborateId) || acd.Collaborator == collaborateId) 
                                                                                   && acd.Decision != null).Any();
        }

        public void SaveApprovalSoadState(int approval, int state)
        {
            UpdateApprovalState(approval, state);
            _unitOfWork.Save();
        }

        public void SavePhaseSoadState(int approval, int phase, int state)
        {
             UpdatePhaseSoadStateByApprovalId(approval, phase, state);
            _unitOfWork.Save();
        }

        public void SaveUserSoadState(int approval, int collaborator, int state, int? phase, bool isExternal)
        {
            if (isExternal)
            {
                var collaboratorEntity = _unitOfWork.SharedApprovalRepository.Get(filter: acl => acl.ExternalCollaborator == collaborator && acl.Approval == approval && acl.Phase == phase).FirstOrDefault();
                collaboratorEntity.SOADState = state;

                _unitOfWork.SharedApprovalRepository.Update(collaboratorEntity);
            }
            else
            {
                var collaboratorEntity = _unitOfWork.ApprovalCollaboratorRepository.Get(filter: acl => acl.Collaborator == collaborator && acl.Approval == approval && acl.Phase == phase).FirstOrDefault();
                collaboratorEntity.SOADState = state;

                _unitOfWork.ApprovalCollaboratorRepository.Update(collaboratorEntity);
            }
           
            _unitOfWork.Save();
        }

        public void ResetSOADState(int approval, int phase)
        {
            var state = (int)SoadProgressState.Sent;

            UpdateApprovalState(approval, state);

            //Update SOAD state for approval phase
            UpdatePhaseSoadStateByApprovalId(approval, phase, state);

            //Update SOAD state for approval collaborators
            UpdateAllInternalCollaboratorsSoadStateByApprovalId(approval, state);

            //Update SOAD state for approval external collaborators
            UpdateAllExternalCollaboratorsSoadStateByApprovalId(approval, state);

            _unitOfWork.Save();
        }

        public int GetANRCollaboratorRoleID()
        {
            return _unitOfWork.ApprovalCollaboratorRoleRepository.GetApprovalCollaboratorRoleID("ANR");
        }

        public int[] GetANRAndRVWCollaboratorRolesListID()
        {
            string[] keys = { "ANR", "RVW" };

            return _unitOfWork.ApprovalCollaboratorRoleRepository.GetApprovalCollaboratorRolesListID(keys);
        }

        private void UpdateAllInternalCollaboratorsSoadStateByApprovalId(int approvalId, int state)
        {
            var internalCollaboratorsEntities = _unitOfWork.ApprovalCollaboratorRepository.Get(filter: acl => acl.Approval == approvalId).ToList();

            foreach (var internalCollaboratorsEntity in internalCollaboratorsEntities)
            {
                internalCollaboratorsEntity.SOADState = state;

                _unitOfWork.ApprovalCollaboratorRepository.Update(internalCollaboratorsEntity);
            }           
        }

        private void UpdateAllExternalCollaboratorsSoadStateByApprovalId(int approvalId, int state)
        {
            var externalCollaboratorsEntities = _unitOfWork.SharedApprovalRepository.Get(filter: acl => acl.Approval == approvalId).ToList();

            foreach (var externalCollaboratorsEntity in externalCollaboratorsEntities)
            {
                externalCollaboratorsEntity.SOADState = state;

                _unitOfWork.SharedApprovalRepository.Update(externalCollaboratorsEntity);
            }
        }

        private void UpdatePhaseSoadStateByApprovalId(int approvalId, int phaseId, int state)
        {
            var approvalJobPhaseApprovalEntity = _unitOfWork.ApprovalJobPhaseApprovalRepository.Get(filter: ajpa => ajpa.Approval == approvalId && ajpa.Phase == phaseId).FirstOrDefault();
            approvalJobPhaseApprovalEntity.SOADState = state;

            _unitOfWork.ApprovalJobPhaseApprovalRepository.Update(approvalJobPhaseApprovalEntity);
        }

        private void UpdateApprovalState(int approval, int state)
        {
            string query = "UPDATE Approval SET SOADState = @state WHERE ID = @approval";
            _unitOfWork.UpdateApprovalState(query, state, approval);
        }
    }
}
