﻿namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public class SentState : SoadState
    {
        // This constructor will create new state taking values from old state
        public SentState(SoadState state)
            : this(state.SoadStateMachine, state.ApprovalId, state.PhaseId, state.CollaboratorId, state.IsExternal)
        {
            
        }

        // this constructor will be used by the other one
        public SentState(SOADStateMachine approvalSoad, int approvalId, int? phaseId, int? userId, bool isExternal)
        {
            SoadStateMachine = approvalSoad;
            ApprovalId = approvalId;
            PhaseId = phaseId;
            CollaboratorId = userId;
            IsExternal = isExternal;
        }

        public override string GetNextState()
        {
            SoadStateMachine.NeedDoMoveToNextState = false;
            GoToNextState();

            return SoadStateMachine.StateCssClass;
        }

        private void GoToNextState()
        {
            this.SoadStateMachine.CurrentState = new OpenedState(this);
            SoadStateMachine.NeedDoMoveToNextState = true;
        }
    }
}
