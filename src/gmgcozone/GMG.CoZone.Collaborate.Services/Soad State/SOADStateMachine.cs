﻿namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public class SOADStateMachine
    {
        public SoadState CurrentState;
        public string StateCssClass;
        public bool NeedDoMoveToNextState;
        public bool UpdateStateInDatabase;
        public bool IsUserSoad;
        public int LastSavedState;

        public SOADStateMachine(SoadProgressState progressState, int approvalId, int? phaseId)
        {
            NeedDoMoveToNextState = true;
            LastSavedState = (int) progressState;

            switch (progressState)
            {
               case SoadProgressState.Sent:
                    CurrentState = new SentState(this, approvalId, phaseId, null, false);
                    StateCssClass = "sent-state";
                    break;
                case SoadProgressState.Opened:
                    CurrentState = new OpenedState(this, approvalId, phaseId, null, false);
                    StateCssClass = "opened-state";
                    break;
                case SoadProgressState.MadeAnnotation:
                    CurrentState = new AnnotationState(this, approvalId, phaseId, null, false);
                    StateCssClass = "annotation-state";
                    break;
                case SoadProgressState.MadeDecision:
                    CurrentState = new DecisionState(this, approvalId, phaseId, null, false, false);
                    StateCssClass = "decision-state";
                    break;
            }
        }

        public SOADStateMachine(SoadProgressState progressState, int approvalId, int? userId, int? phaseId, bool isExternal)
        {
            NeedDoMoveToNextState = true;
            IsUserSoad = true;
            LastSavedState = (int)progressState;

            switch (progressState)
            {
                case SoadProgressState.Sent:
                    CurrentState = new SentState(this, approvalId, phaseId, userId, isExternal);
                    StateCssClass = "sent-state";
                    break;
                case SoadProgressState.Opened:
                    CurrentState = new OpenedState(this, approvalId, phaseId, userId, isExternal);
                    StateCssClass = "opened-state";
                    break;
                case SoadProgressState.MadeAnnotation:
                    CurrentState = new AnnotationState(this, approvalId, phaseId, userId, isExternal);
                    StateCssClass = "annotation-state";
                    break;
                case SoadProgressState.MadeDecision:
                    CurrentState = new DecisionState(this, approvalId, phaseId, userId, isExternal, false);
                    StateCssClass = "decision-state";
                    break;
            }
        }

        public void StartProgress()
        {
            while (NeedDoMoveToNextState)
            {
                StateCssClass = CurrentState.GetNextState();
            }
        }
    }
    
    public enum SoadProgressState
    {
        Sent = 0,
        Opened,
        MadeAnnotation,
        MadeDecision
    }
}
