﻿using GMG.CoZone.Collaborate.Interfaces;

namespace GMG.CoZone.Collaborate.Services.Soad_State
{
    public class OpenedState : SoadState
    {
        private readonly ISoadStateService _soadStateService = new SoadStateService();

        // This constructor will create new state taking values from old state
        public OpenedState(SoadState state)
            : this(state.SoadStateMachine, state.ApprovalId, state.PhaseId, state.CollaboratorId, state.IsExternal)
        {

        }

        // this constructor will be used by the other one
        public OpenedState(SOADStateMachine approvalSoad, int approvalId, int? phaseId, int? collaboratorId, bool isExternal)
        {
            SoadStateMachine = approvalSoad;
            ApprovalId = approvalId;
            PhaseId = phaseId;
            CollaboratorId = collaboratorId;
            IsExternal = isExternal;
        }

        public override string GetNextState()
        {
            var state = (int)SoadProgressState.Sent;
            SoadStateMachine.NeedDoMoveToNextState = false;

            if (!SoadStateMachine.IsUserSoad && _soadStateService.IsApprovalOpenedByAllCollaborators(ApprovalId, PhaseId) ||
                SoadStateMachine.IsUserSoad && _soadStateService.IsApprovalOpenedByCollaborator(ApprovalId, PhaseId, CollaboratorId, IsExternal))
            {
                SoadStateMachine.StateCssClass = "opened-state";
                GoToNextState();
            }
            else if (SoadStateMachine.UpdateStateInDatabase && SoadStateMachine.LastSavedState != state)
            {
                if (SoadStateMachine.IsUserSoad)
                {
                    UpdateUserState(state);
                }
                else
                {
                    UpdateState(state);
                }
            }
                
            return SoadStateMachine.StateCssClass;
        }

        private void UpdateState(int state)
        {
            _soadStateService.SaveApprovalSoadState(ApprovalId, state);
            if (PhaseId != null)
            {
                _soadStateService.SavePhaseSoadState(ApprovalId, PhaseId.GetValueOrDefault(), state);
            }
        }

        private void UpdateUserState(int state)
        {
            _soadStateService.SaveUserSoadState(ApprovalId, CollaboratorId.GetValueOrDefault(), state, PhaseId, IsExternal);
        }

        private void GoToNextState()
        {
            this.SoadStateMachine.CurrentState = new AnnotationState(this);
            SoadStateMachine.NeedDoMoveToNextState = true;
            SoadStateMachine.UpdateStateInDatabase = true;
        }
    }
}
