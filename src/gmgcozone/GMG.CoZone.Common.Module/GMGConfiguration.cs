﻿using System;
using GMG.CoZone.Common.Interfaces;
using System.Configuration;
using System.Net;
using System.IO;
using System.Linq;

namespace GMG.CoZone.Common.Module
{
    public class GMGConfiguration : IGMGConfiguration
    {
        /// <summary>
        /// Constants
        /// </summary>
        private const string CONFIGURATION_VALUE_LOCAL = "local";
        private const string CONFIGURATION_VALUE_AMAZONE = "AWS";

        /// <summary>
        /// AWS
        /// </summary>
        private bool? amazonS3BucketEnabled = null;
        private string amazonRegion = string.Empty;
        private string amazonAccessKey = string.Empty;
        private string amazonSecretKey = string.Empty;
        private bool? amazonSQSEnabled = null;
        private string _awsSnsMediDefineResponseTopic = string.Empty;

        /// <summary>
        /// Application
        /// </summary>
        private int logLevel = -1;
        private string siteConfiguration = string.Empty;
        private string namingPrefix = string.Empty;
        private string metadataRequestUrl = "http://169.254.169.254/latest/meta-data/placement/availability-zone";
        private string _serverProtocol = string.Empty;
        private string _mediaDefineLocalQueue = string.Empty;

        /// <summary>
        /// Email
        /// </summary>
        private int numberOfEmailsToSend = 0;
        private bool? isSendEmails = null;
        private string mailServerType = string.Empty;
        private string awsMailServer = string.Empty;
        private string mailServer = string.Empty;
        private string mailServerPort = string.Empty;
        private string awsSmtpUserName = string.Empty;
        private string awsSmtpPassword = string.Empty;
        private string customSmtpWriteQueueName = string.Empty;
        private string customSmtpReadQueueName = string.Empty;
        private string _domainUrl = string.Empty;

        /// <summary>
        /// Path
        /// </summary>
        private string dataFolderName = string.Empty;
        private string pathToDataFolder = string.Empty;
        private string _pathToProcessingFolder = string.Empty;

        // Media Processor
        private int _defaultImageSize = 0;
        private int _highResImageSize = 0;

        const string AWS_AccessKey = "AKIAR6KWPSILSIVWAX5Z";
        const string AWS_SecretKey = "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";
        const string AWS_SMTPUserName = "AKIAR6KWPSILWN7BABFV";
        const string AWS_SMTPPassword = "BHiVfLvSl26oyqchR6fOdCkfq9MjjyHyvd93mYfQQCaj";

        public string ActiveConfiguration
        {
            get
            {
                if (String.IsNullOrEmpty(siteConfiguration))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ActiveConfiguration"]))
                    {
                        siteConfiguration = ConfigurationManager.AppSettings["ActiveConfiguration"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ActiveConfiguration"));
                    }
                }

                return siteConfiguration;
            }
        }

        public string AWSRegion
        {
            get
            {
                if (String.IsNullOrEmpty(amazonRegion))
                {
                    try
                    {
                        WebClient client = new WebClient();
                        Stream data = client.OpenRead(metadataRequestUrl);
                        StreamReader reader = new StreamReader(data);
                        string content = reader.ReadToEnd().Trim();
                        data.Close();
                        reader.Close();
                        
                        Amazon.RegionEndpoint endpoint = Amazon.RegionEndpoint.EnumerableAllRegions.SingleOrDefault(o => content.StartsWith(o.SystemName));
                        if (endpoint != null)
                        {
                            amazonRegion = endpoint.SystemName;
                        }
                        else
                        {
                            throw new Exception(String.Format("GMGColorConfiguration class: Unable to resolve endpoint. metadata value : {0}", content));
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("GMGColorMediaConfiguration class: Unable to send the request to availability-zone. URL: {0}, Exception : {1}", metadataRequestUrl, ex.Message));
                    }
                }

                return amazonRegion;
            }
        }

        public bool IsEnabledS3Bucket
        {
            get
            {
                if (amazonS3BucketEnabled == null)
                {
                    string dataStorageType = string.Empty;

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DataStorageType"]))
                    {
                        dataStorageType = ConfigurationManager.AppSettings["DataStorageType"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DataStorageType"));
                    }

                    amazonS3BucketEnabled = (dataStorageType == CONFIGURATION_VALUE_AMAZONE);
                }

                return (bool)amazonS3BucketEnabled;
            }
        }

        public int LogLevel
        {
            get
            {
                if (logLevel == -1)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogLevel"]))
                    {
                        logLevel = Convert.ToInt32(ConfigurationManager.AppSettings["LogLevel"]);
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "LogLevel"));
                    }
                }

                return logLevel;
            }
        }

        public bool IsSendEmails
        {
            get
            {
                if (isSendEmails == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsSendEmails"]))
                    {
                        try
                        {
                            isSendEmails = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSendEmails"]);
                        }
                        catch
                        {
                            isSendEmails = true;
                        }
                    }
                    else
                    {
                        isSendEmails = true;
                    }
                }

                return (bool)isSendEmails;
            }
        }

        public int NumberOfEmailsToSend
        {
            get
            {
                if (numberOfEmailsToSend == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["NumberOfEmailsToSend"], out numberOfEmailsToSend))
                    {
                        numberOfEmailsToSend = 20;
                    }
                }
                return numberOfEmailsToSend;
            }
        }

        public string DataFolderPrefix
        {
            get
            {
                if (String.IsNullOrEmpty(namingPrefix))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DataFolderPrefix"]))
                    {
                        namingPrefix = ConfigurationManager.AppSettings["DataFolderPrefix"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DataFolderPrefix"));
                    }
                }

                return namingPrefix;
            }
        }

        public string AWSAccessKey
        {
            get
            {
                if (String.IsNullOrEmpty(amazonAccessKey))
                {
                    if (!String.IsNullOrEmpty(AWS_AccessKey))
                    {
                        amazonAccessKey = AWS_AccessKey;
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSAccessKey"));
                    }
                }

                return amazonAccessKey;
            }
        }

        public string AWSSecretKey
        {
            get
            {
                if (String.IsNullOrEmpty(amazonSecretKey))
                {
                    if (!String.IsNullOrEmpty(AWS_SecretKey))
                    {
                        amazonSecretKey = AWS_SecretKey;
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSecretKey"));
                    }
                }

                return amazonSecretKey;
            }
        }

        public string AWSSNSMediaDefineResponseTopic
        {
            get
            {
                if (string.IsNullOrEmpty(_awsSnsMediDefineResponseTopic))
                {
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSSNSMediaDefineResponseTopic"]))
                    {
                        _awsSnsMediDefineResponseTopic = ConfigurationManager.AppSettings["AWSSNSMediaDefineResponseTopic"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSMediaDefineResponseTopic"));
                    }
                }
                return _awsSnsMediDefineResponseTopic;
            }
        }

        public string MailServerType
        {
            get
            {
                if (String.IsNullOrEmpty(mailServerType))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MailServerType"]))
                    {
                        mailServerType = ConfigurationManager.AppSettings["MailServerType"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServerType"));
                    }
                }

                return mailServerType;
            }
        }

        public string AWSMailServer
        {
            get
            {
                if (String.IsNullOrEmpty(awsMailServer))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSMailServer"]))
                    {
                        awsMailServer = ConfigurationManager.AppSettings["AWSMailServer"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSMailServer"));
                    }
                }

                return awsMailServer;
            }
        }

        public string MailServer
        {
            get
            {
                if (String.IsNullOrEmpty(mailServer))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MailServer"]))
                    {
                        mailServer = ConfigurationManager.AppSettings["MailServer"].ToString();
                    }
                    else
                    {
                        mailServer = "localhost";
                    }
                }

                return mailServer;
            }
        }

        public string MailServerPort
        {
            get
            {
                if (String.IsNullOrEmpty(mailServerPort))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MailServerPort"]))
                    {
                        mailServerPort = ConfigurationManager.AppSettings["MailServerPort"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServerPort"));
                    }
                }

                return mailServerPort;
            }
        }

        public string AWSSMTPUserName
        {
            get
            {
                if (String.IsNullOrEmpty(awsSmtpUserName))
                {
                    if (!String.IsNullOrEmpty(AWS_SMTPUserName))
                    {
                        awsSmtpUserName = AWS_SMTPUserName;
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSMTPUserName"));
                    }
                }

                return awsSmtpUserName;
            }
        }

        public string AWSSMTPPassword
        {
            get
            {
                if (String.IsNullOrEmpty(awsSmtpPassword))
                {
                    if (!String.IsNullOrEmpty(AWS_SMTPPassword))
                    {
                        awsSmtpPassword = AWS_SMTPPassword;
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSMTPPassword"));
                    }
                }

                return awsSmtpPassword;
            }
        }

        public string DataFolderName(string accountRegion)
        {
            if (IsEnabledS3Bucket && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
            {
                dataFolderName = DataFolderPrefix + "-" + ActiveConfiguration + "-data-" + accountRegion;
            }
            else if (String.IsNullOrEmpty(dataFolderName))
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemDataFolderName"]))
                {
                    dataFolderName = ConfigurationManager.AppSettings["FileSystemDataFolderName"].ToString().Trim();
                }
                else
                {
                    throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemDataFolderName"));
                }
            }

            return dataFolderName;
        }

        public string PathToDataFolder(string accountRegion)
        {
            if (IsEnabledS3Bucket && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
            {
                // Special case for US Standard *
                if (this.AWSRegion == "us-east-1")
                {
                    pathToDataFolder = "https://" + DataFolderName(accountRegion) + ".s3.amazonaws.com/";
                }
                else
                {
                    pathToDataFolder = "https://" + DataFolderName(accountRegion) + ".s3-" + accountRegion + ".amazonaws.com/";
                }
            }
            else if (String.IsNullOrEmpty(pathToDataFolder))
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemPathToDataFolder"]))
                {
                    pathToDataFolder = ConfigurationManager.AppSettings["FileSystemPathToDataFolder"].ToString().Trim();
                }
                else
                {
                    throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder"));
                }
            }

            return pathToDataFolder;
        }

        public string CustomSmtpWriteQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    customSmtpWriteQueueName = this.ActiveConfiguration + "-customsmtpwrite-queue";
                }
                else if (String.IsNullOrEmpty(customSmtpWriteQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["CustomSmtpWriteQueueName"]))
                    {
                        customSmtpWriteQueueName = ConfigurationManager.AppSettings["CustomSmtpWriteQueueName"].ToString().Trim();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "CustomSmtpWriteQueueName"));
                    }
                }

                return customSmtpWriteQueueName;
            }
        }

        public string CustomSmtpReadQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    customSmtpReadQueueName = this.ActiveConfiguration + "-customsmtpread-queue";
                }
                else if (String.IsNullOrEmpty(customSmtpReadQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["CustomSmtpReadQueueName"]))
                    {
                        customSmtpReadQueueName = ConfigurationManager.AppSettings["CustomSmtpReadQueueName"].ToString().Trim();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "CustomSmtpReadQueueName"));
                    }
                }

                return customSmtpReadQueueName;
            }
        }

        public bool IsEnabledAmazonSQS
        {
            get
            {
                if (amazonSQSEnabled == null)
                {
                    string queueType = string.Empty;

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["QueueType"]))
                    {
                        queueType = ConfigurationManager.AppSettings["QueueType"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "QueueType"));
                    }

                    amazonSQSEnabled = (queueType == CONFIGURATION_VALUE_AMAZONE);
                }

                return (bool)amazonSQSEnabled;
            }
        }

        public string ServerProtocol
        {
            get
            {
                if (String.IsNullOrEmpty(_serverProtocol))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerProtocol"]))
                    {
                        _serverProtocol = ConfigurationManager.AppSettings["ServerProtocol"].ToString();
                    }
                    else
                    {
                        _serverProtocol = "http";
                        throw new Exception(string.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ServerProtocol"));
                    }
                }
                return _serverProtocol;
            }
        }

        public string DomainUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_domainUrl))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DomainUrl"]))
                    {
                        _domainUrl = ConfigurationManager.AppSettings["DomainUrl"].ToString();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DomainUrl"));
                    }
                }

                return _domainUrl;
            }
        }

        public string MediaDefineResponseQueueName
        {
            get
            {
                if (String.IsNullOrEmpty(_mediaDefineLocalQueue))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ApprovalProcessResponseQueue"]))
                    {
                        _mediaDefineLocalQueue = ConfigurationManager.AppSettings["ApprovalProcessResponseQueue"].ToString().Trim();
                    }
                    else
                    {
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalProcessResponseQueue"));
                    }
                }

                return _mediaDefineLocalQueue;
            }
        }

        public int DefaultImageSize
        {
            get
            {
                if (_defaultImageSize == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultImageSize"]))
                    {
                        _defaultImageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultImageSize"]);
                    }
                    else
                    {
                        _defaultImageSize = 7000;
                        throw new Exception(string.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DefaultImageSize"));
                    }
                }

                return _defaultImageSize;
            }
        }

        public int HighResImageSize
        {
            get
            {
                if (_highResImageSize == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["HighResImageSize"]))
                    {
                        _highResImageSize = Convert.ToInt32(ConfigurationManager.AppSettings["HighResImageSize"]);
                    }
                    else
                    {
                        _highResImageSize = 15000;
                        throw new Exception(string.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "HighResImageSize"));
                    }
                }

                return _highResImageSize;
            }
        }

        public string PathToProcessingFolder
        {
            get
            {
                if (String.IsNullOrEmpty(_pathToProcessingFolder))
                {

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PathToProcessingFolder"]))
                    {
                        _pathToProcessingFolder = ConfigurationManager.AppSettings["PathToProcessingFolder"].ToString().Trim();
                    }
                    else
                    {
                        _pathToProcessingFolder = @"C:\Temp\";
                        throw new Exception(string.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PathToProcessingFolder"));
                    }
                }

                return _pathToProcessingFolder;
            }
        }
    }
}
