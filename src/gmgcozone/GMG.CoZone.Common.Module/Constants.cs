﻿namespace GMG.CoZone.Common.Module
{
    public static class Constants
    {
        public const string EmailRegex = @"^(""[^""]+""@[a-zA-Z0-9\-_]+[\.][a-zA-Z0-9\.\-_]+)$|^([a-zA-Z0-9\\\/$!%=_\.+\-""]+@[a-zA-Z0-9\-_]+[\.]+[a-zA-Z0-9\.\-_]*)$";
    }
}
