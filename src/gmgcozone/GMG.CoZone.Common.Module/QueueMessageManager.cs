﻿using System;
using GMG.CoZone.Common.Interfaces;
using System.Messaging;
using System.Collections.Generic;
using GMGColor.AWS;
using System.IO;
using System.Xml.Linq;
using System.Threading;
using System.Linq;

namespace GMG.CoZone.Common.Module
{
    public class QueueMessageManager : IQueueMessageManager
    {
        private IGMGConfiguration _gmgConfiguration;

        public QueueMessageManager(IGMGConfiguration gmgConfiguration)
        {
            _gmgConfiguration = gmgConfiguration;
        }

        public List<Amazon.SQS.Model.Message> ReadMessagesFromAWSQueue(string queueName)
        {
            var messageList = new List<Amazon.SQS.Model.Message>();
            try
            {
                var queueMessageList = AWSSimpleQueueService.ReadMessages(queueName,
                                                                          _gmgConfiguration.AWSAccessKey,
                                                                          _gmgConfiguration.AWSSecretKey,
                                                                          _gmgConfiguration.AWSRegion);

                if (queueMessageList.Messages.Count > 0)
                {
                    foreach (var message in queueMessageList.Messages)
                    {
                        messageList.Add(message);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return messageList;
        }

        public List<string> ReadMessagesFromLocalQueue(string queueName)
        {
            var messageList = new List<string>();
            MessageQueue messageQueue = new MessageQueue(queueName);
            messageQueue.Formatter = new BinaryMessageFormatter();
            var msgEnumerator = messageQueue.GetMessageEnumerator2();

            while (msgEnumerator.MoveNext(new TimeSpan(0, 0, 1)))
            {
                var msg = messageQueue.ReceiveById(msgEnumerator.Current.Id, new TimeSpan(0, 0, 1));
                var reader = new StreamReader(msg.BodyStream);
                var msgBody = reader.ReadToEnd();
                msgBody = msgBody.Substring(msgBody.IndexOf("{"), msgBody.LastIndexOf("}") + 1 - msgBody.IndexOf("{"));
                messageList.Add(msgBody);
            }
            return messageList;
        }

        public void WriteMessageToAWSQueue(string queueName, string message)
        {
            AWSSimpleQueueService.CreateMessage(message,
                                                queueName,
                                                _gmgConfiguration.AWSAccessKey,
                                                _gmgConfiguration.AWSSecretKey,
                                                _gmgConfiguration.AWSRegion);
        }

        public void WriteMessageToLocalQueue(string queueName, string message)
        {
            var messageQueue = new MessageQueue(queueName);

            if (messageQueue != null)
            {
                var queueMessage = new Message();
                queueMessage.Body = message;
                messageQueue.Send(queueMessage);
            }
        }

        public void WriteMessageToQueue(string queueName, string message)
        {
            if (_gmgConfiguration.IsEnabledAmazonSQS)
            {
                try
                {
                    WriteMessageToAWSQueue(queueName, message);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                WriteMessageToLocalQueue(queueName, message);
            }
        }

        public string WaitForAWSMessage(string queueName, string parameter, string value, int waitingSeconds)
        {
            var connectionError = string.Empty;
            var isFound = false;
            var start = DateTime.UtcNow;

            while (!isFound && DateTime.UtcNow.Subtract(start).Seconds < waitingSeconds)
            {
                var queueMessages = ReadMessagesFromAWSQueue(queueName);

                foreach (var message in queueMessages)
                {
                    XDocument sqsMessage = XDocument.Parse(message.Body);

                    if (sqsMessage.Descendants(parameter).FirstOrDefault() != null)
                    {
                        if (value == sqsMessage.Descendants(parameter).FirstOrDefault().Value)
                        {
                            isFound = true;

                            AWSSimpleQueueService.DeleteMessage(queueName, message, _gmgConfiguration.AWSAccessKey, _gmgConfiguration.AWSSecretKey, _gmgConfiguration.AWSRegion);
                            break;
                        }
                    }
                }
                Thread.Sleep(100);
            }
            return connectionError;
        }

        public string WaitForLocalMessage(string queueName, string parameter, string value, int waitingSeconds)
        {
            var connectionError = string.Empty;
            var isFound = false;
            var start = DateTime.UtcNow;

            var messageQueue = new MessageQueue(queueName);
            messageQueue.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

            while (!isFound && DateTime.UtcNow.Subtract(start).Seconds < waitingSeconds)
            {
                var msgEnumerator = messageQueue.GetMessageEnumerator2();

                while (msgEnumerator.MoveNext())
                {
                    var msg = msgEnumerator.Current;
                    XDocument sqsMessage = XDocument.Parse(msg.Body.ToString());

                    if (sqsMessage.Descendants(parameter).FirstOrDefault() != null)
                    {
                        if (value == sqsMessage.Descendants(parameter).FirstOrDefault().Value)
                        {
                            msgEnumerator.RemoveCurrent();
                            isFound = true;
                            break;
                        }
                    }
                }
            }
            return connectionError;
        }
    }
}
