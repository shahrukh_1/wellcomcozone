﻿using System;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Component.Logging;

namespace GMG.CoZone.Common.Module.Loggers
{
    public class ServiceLogger : IServiceLogger
    {
        private const string _errorLoggerName = "DBErrorLogger";
        private const string _emailSenderLoggerName = "EmailSenderLogger";

        private IGMGConfiguration _gmgConfiguration;
        private Logger _serviceLogger;

        public ServiceLogger(IGMGConfiguration gmgConfiguration)
        {
            _gmgConfiguration = gmgConfiguration;
            _serviceLogger = new Logger(_gmgConfiguration.IsEnabledS3Bucket
                                        ?
                                        _gmgConfiguration.ActiveConfiguration + " " + _gmgConfiguration.AWSRegion
                                        : String.Empty, _errorLoggerName, _emailSenderLoggerName);
        }
       
        public void Log(LoggingNotification notification)
        {
            if (_gmgConfiguration.LogLevel <= (int)notification.Severity)
            {
                _serviceLogger.Log(notification);
            }
        }

        public void Log(string message, Exception exception)
        {
            _serviceLogger.Log(new LoggingException(message, exception));
        }
    }
}
