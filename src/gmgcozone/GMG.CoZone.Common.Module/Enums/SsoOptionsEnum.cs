﻿namespace GMG.CoZone.Common.Module.Enums
{
    public enum SsoOptionsEnum
    {
        Auth0 = 0,
        Saml = 1,
        None = 2
    }
}
