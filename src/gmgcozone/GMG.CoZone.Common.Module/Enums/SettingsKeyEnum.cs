﻿namespace GMG.CoZone.Common.Module.Enums
{
    public enum SettingsKeyEnum
    {
        PDF2SWFArguments,
        SoftproofingCalibrationTimeoutDays,
        ContactSupportEmail,
        ProofStudioBackgroundColor,
        ProofStudioPenWidth,
        ProofStudioShowAnnotationsForExternalUsers,
        CustomSmtpServer,
        SingleSignOn
    }
}
