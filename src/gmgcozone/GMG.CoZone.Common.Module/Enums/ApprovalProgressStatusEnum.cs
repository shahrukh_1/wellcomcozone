﻿namespace GMG.CoZone.Common.Module.Enums
{
    public enum ApprovalProgressStatusEnum
    {
        Error, 
        Success,
        InProgress
    }
}
