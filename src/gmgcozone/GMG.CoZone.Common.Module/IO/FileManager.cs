﻿using System;
using System.IO;
using GMG.CoZone.Common.Interfaces;
using GMGColor.AWS;
using System.Collections.Generic;
using GMG.CoZone.Common.AWS.Interfaces;

namespace GMG.CoZone.Common.Module.IO
{
    public class FileManager : IFileManager
    {
        private readonly IGMGConfiguration _gmgConfiguration;
        private readonly IAWSSimpleStorageService _awsSimpleStorageService;

        private const string APPROVAL_FOLDER = "/approvals/";

        public FileManager(IGMGConfiguration gmgConfiguration, IAWSSimpleStorageService awsSimpleStorageService)
        {
            _gmgConfiguration = gmgConfiguration;
            _awsSimpleStorageService = awsSimpleStorageService;
        }

        public bool DeleteFileWithBackup(string relativeFilePath, string accountRegion)
        {
            bool success = false;

            try
            {
                if (_gmgConfiguration.IsEnabledS3Bucket)
                {
                    FolderExists("backup/", accountRegion, true);

                    var destinationPath = "backup/" + relativeFilePath;

                    AWSSimpleStorageService.CopyFileInsideBucket(_gmgConfiguration.DataFolderName(accountRegion), relativeFilePath, destinationPath,
                        _gmgConfiguration.AWSAccessKey,
                        _gmgConfiguration.AWSSecretKey);

                    success = AWSSimpleStorageService.FileExistsInBucketThenDelete(_gmgConfiguration.DataFolderName(accountRegion), 
                                                                                   relativeFilePath,
                                                                                   _gmgConfiguration.AWSAccessKey,
                                                                                   _gmgConfiguration.AWSSecretKey);
                }
                else
                {
                    File.Delete(_gmgConfiguration.PathToDataFolder(accountRegion) + relativeFilePath);
                    success = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return success;
        }

        public bool FileExists(string relativeFilePath, string accountRegion)
        {
            bool fileExists = false;

            if (_gmgConfiguration.IsEnabledS3Bucket)
            {
                fileExists = AWSSimpleStorageService.FileExistsInBucket(_gmgConfiguration.DataFolderName(accountRegion), 
                                                                        relativeFilePath, 
                                                                        _gmgConfiguration.AWSAccessKey, 
                                                                        _gmgConfiguration.AWSSecretKey);
            }
            else
            {
                var fi = new FileInfo(_gmgConfiguration.PathToDataFolder(accountRegion) + relativeFilePath);
                fileExists = fi.Exists;
            }

            return fileExists;
        }

        public Stream GetMemoryStreamOfAFile(string relativeFilePath, string accountRegion)
        {
            Stream stream;

            if (_gmgConfiguration.IsEnabledS3Bucket)
            {
                stream = AWSSimpleStorageService.GetFileStream(_gmgConfiguration.DataFolderName(accountRegion), 
                                                               relativeFilePath, 
                                                               _gmgConfiguration.AWSAccessKey, 
                                                               _gmgConfiguration.AWSSecretKey);
            }
            else
            {
                stream = new FileStream(_gmgConfiguration.PathToDataFolder(accountRegion) + relativeFilePath, FileMode.Open, FileAccess.Read);
            }
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        public bool FolderExists(string relativeFolderPath, string accountRegion, bool isCreateIfNotExists = false, DateTime? expireDate = default(DateTime?))
        {
            bool folderExists = false;

            if (_gmgConfiguration.IsEnabledS3Bucket)
            {
                string dataFolderName = _gmgConfiguration.DataFolderName(accountRegion);
                folderExists = AWSSimpleStorageService.FolderExistsInBucket(dataFolderName, 
                                                                            relativeFolderPath,
                                                                            _gmgConfiguration.AWSAccessKey, 
                                                                            _gmgConfiguration.AWSSecretKey);
                if (isCreateIfNotExists && !folderExists)
                {
                    folderExists = AWSSimpleStorageService.CreateNewFolder(dataFolderName, 
                                                                           relativeFolderPath,
                                                                           _gmgConfiguration.AWSAccessKey, 
                                                                           _gmgConfiguration.AWSSecretKey, 
                                                                           expireDate);
                }
            }
            else
            {
                string folderPath = _gmgConfiguration.PathToDataFolder(accountRegion) + relativeFolderPath;
                folderExists = Directory.Exists(folderPath);

                if (isCreateIfNotExists && !folderExists)
                {
                    try
                    {
                        Directory.CreateDirectory(folderPath);
                        folderExists = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return folderExists;
        }

        public List<string> GetFoldersContent(string folderName, string accountRegion)
        {
            var folderPath = _gmgConfiguration.PathToDataFolder(accountRegion) + APPROVAL_FOLDER + folderName;

            if (!_gmgConfiguration.IsEnabledS3Bucket)
            {
                return new List<string>(Directory.GetFileSystemEntries(folderPath));
            }
            else
            {
                return _awsSimpleStorageService.GetFolderContent(folderName, accountRegion);
            }
        }

        public void DeleteFolderContent(List<string> folderContent, string accountRegion)
        {
            if (!_gmgConfiguration.IsEnabledS3Bucket)
            {
                DeleteLocalFolderContent(folderContent, accountRegion);
            }
            else
            {
                _awsSimpleStorageService.DeleteFolderContent(folderContent, accountRegion);
            }
        }

        private void DeleteLocalFolderContent(List<string> folderContent, string accountRegion)
        {
            foreach (var item in folderContent)
            {
                try
                {
                    var fileAttr = File.GetAttributes(item);
                    if (fileAttr.HasFlag(FileAttributes.Directory))
                    {
                        Directory.Delete(item, true);
                    }
                    else
                    {
                        File.Delete(item);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public bool DeleteFile(string filePath, string accountRegion)
        {
            var success = false;
            try
            {
                if (!_gmgConfiguration.IsEnabledS3Bucket)
                {
                    File.Delete(filePath);
                    success = true;
                }
                else
                {
                    _awsSimpleStorageService.DeleteFile(filePath, accountRegion);
                    success = true;
                }
                return success;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
