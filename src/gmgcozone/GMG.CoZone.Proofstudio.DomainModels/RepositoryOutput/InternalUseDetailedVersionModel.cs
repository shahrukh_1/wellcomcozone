﻿namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUseDetailedVersionModel
    {
        public int ID { set; get; }
        public string Guid { set; get; }
        public string ErrorMessage { set; get; }
        public bool IsDeleted { set; get; }
        public bool IsLocked { set; get; }
        public int Creator { set; get; }
        public string ApprovalTypeName { set; get; }
        public bool AllowDownloadOriginal { set; get; }
        public int VersionNumber { set; get; }
        public string FileName { set; get; }
        public string Extention { set; get; }
        public string AccountRegion { set; get; }
        public int ApprovalType { set; get; }
        public string AccountDomain { set; get; }
        public bool isVideo { set; get; }
        public int FirstPageNumber { set; get; }
        public int TotalPagesCount { set; get; }
        public string JobStatus { set; get; }
        public bool IsRGBColorSpace { set; get; }
        public int? CurrentPhase { set; get; }
        public string PhaseName { set; get; }
        public string VersionLabel { set; get; }
        public bool IsPhaseCompleted { set; get; }
        public bool shouldBeViewedByAll { set; get; }

        public bool RestrictedZoom { set; get; }
    }
}
