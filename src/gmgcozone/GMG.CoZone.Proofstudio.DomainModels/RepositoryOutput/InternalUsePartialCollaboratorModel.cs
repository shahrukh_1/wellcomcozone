﻿using System;

namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUsePartialCollaboratorModel
    {
        public int ApprovalCollaboratorID { set; get; }
        public string FamilyName { set; get; }
        public string GivenName { set; get; }
        public bool IsExternal { set; get; }
        public int Collaborator { set; get; }
        public DateTime UserCreatedDate { set; get; }        
        public int AccountID { set; get; }
        public string UserPhotoPath { set; get; }
        public string UserGuid { set; get; }
        public int ProofStudioColor { set; get; }        

        public int ApprovalCollaboratorRole { set; get; }
        public int Approval_version { get; set; }
        public bool IsLocked { get; set; }
        public int? CurrentPhase { get; set; }
        public int? Phase { get; set; }
        public int Version { get; set; }
        public int Owner { get; set; }
        public int Job { get; set; }
        public int ApprovalId { get; set; }

        public string UserColor { set; get; }
        public DateTime ExpireDate { set; get; }
        public bool IsExpired { set; get; }
        public bool? IsAcessRevoked { set; get; }
        public string SubstituteUserName { set; get; }
        public int UserStatus { set; get; }
    }
}
