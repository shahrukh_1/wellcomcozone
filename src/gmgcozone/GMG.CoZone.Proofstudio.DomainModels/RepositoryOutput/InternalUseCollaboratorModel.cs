﻿using System;

namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUseCollaboratorModel
    {
        public int ID { set; get; }
        public string FamilyName { set; get; }
        public string GivenName { set; get; }
        public bool IsExternal { set; get; }
        public string CollaboratorAvatar { set; get; }
        public int CollaboratorRole { set; get; }
        public int Collaborator { set; get; }
        public DateTime AssignedDate { set; get; }
        public string UserColor { set; get; }
        public int AccountID { set; get; }
        public string AccountRegion { set; get; }
        public string AccountDomain { set; get; }
        public string UserPhotoPath { set; get; }
        public string UserGuid { set; get; }
        public bool? IsAcessRevoked { set; get; }
        public string SubstituteUserName { set; get; }
        public bool UserStatus { set; get; }
    }
}
