﻿namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUseVersionModel
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Guid { set; get; }
        public string AccountRegion { set; get; }
        public string AccountDomain { set; get; }
        public int VersionNumber { set; get; }
        public bool IsVideo { set; get; }
        public string VersionLabel { set; get; }
    }
}
