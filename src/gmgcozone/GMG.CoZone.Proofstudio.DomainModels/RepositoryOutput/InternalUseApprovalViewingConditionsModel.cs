﻿namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUseApprovalViewingConditionsModel
    {
        public int? CustomProfile { set; get; }
        public int? EmbeddedProfile { set; get; }
        public int? SimulationProfile { set; get; }
    }
}
