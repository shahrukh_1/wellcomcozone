﻿namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUseJobViewingConditionsModel
    {
        public int ID { set; get; }
        public int? SimulationProfile { set; get; }
        public int? PaperTint { set; get; }
    }
}
