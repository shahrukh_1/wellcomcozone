﻿using System;

namespace GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput
{
    public class InternalUseCollaboratorLinkModel
    {
        public int ID { set; get; }
        public DateTime ExpireDate { set; get; }
        public bool IsExpired { set; get; }
        public int approval_role { set; get; }
        public int approval_version { set; get; }
        public int PrimaryGroup { set; get; }
        public int? Decision { set; get; }
        public int? CurrentPhase { set; get; }
        public bool IsLocked { set; get; }
        public int? Phase { set; get; }
        public int Version { set; get; }

        public string Permission { set; get; }
        public int Owner { set; get; }
        public int Job { set; get; }
        public int? JobOwner { set; get; }
        public int ApprovalId { set; get; }
    }
}
