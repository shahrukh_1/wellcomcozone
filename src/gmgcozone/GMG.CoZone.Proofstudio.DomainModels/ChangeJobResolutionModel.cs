﻿namespace GMG.CoZone.Proofstudio.DomainModels
{
    public class ChangeJobResolutionModel
    {
        public int ApprovalId { get; set; }
        public bool JobIsHighRes { get; set; }
    }
}
