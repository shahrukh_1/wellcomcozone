﻿using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using System.Collections.Generic;

namespace GMG.CoZone.Proofstudio.DomainModels.Responses
{
    public class ApprovalUserGroupsResponseModel
    {
        public List<UserGroupModel> user_groups { get; set; }
        public List<UserGroupDecisionModel> user_group_links { get; set; }
        public ApprovalUserGroupsResponseModel()
        {
            user_groups = new List<UserGroupModel>();
            user_group_links = new List<UserGroupDecisionModel>();
        }

    }
}
