﻿using System.Collections.Generic;

namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalsBasicResponseModel
    {
        public List<ApprovalBasicDataModel> approval_basic { get; set; }
    }
}
