﻿using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using System.Collections.Generic;

namespace GMG.CoZone.Proofstudio.DomainModels.Responses
{
    public class ApprovalInitialVersionsResponseModel
    {
        public List<ApprovalModel> approvals { get; set; }
        public List<ApprovalVersionModel> versions { get; set; }
        public List<SoftProofingSessionParamsModel> softProofingParams { get; set; }

        public ApprovalInitialVersionsResponseModel()
        {
            approvals = new List<ApprovalModel>();
            versions = new List<ApprovalVersionModel>();
            softProofingParams = new List<SoftProofingSessionParamsModel>();
        }
    }
}
