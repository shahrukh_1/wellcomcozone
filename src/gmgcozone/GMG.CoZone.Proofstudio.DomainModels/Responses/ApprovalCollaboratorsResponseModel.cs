﻿using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using System.Collections.Generic;

namespace GMG.CoZone.Proofstudio.DomainModels.Responses
{
    public class ApprovalCollaboratorsResponseModel
    {
        public List<ApprovalCollaboratorModel> internal_collaborators { get; set; }
        public List<ApprovalCollaboratorLinkModel> internal_collaborators_links { get; set; }

        public List<ApprovalCollaboratorModel> external_collaborators { get; set; }
        public List<ApprovalCollaboratorLinkModel> external_collaborators_links { get; set; }

        public ApprovalCollaboratorsResponseModel()
        {            
            internal_collaborators = new List<ApprovalCollaboratorModel>();
            internal_collaborators_links = new List<ApprovalCollaboratorLinkModel>();
            external_collaborators = new List<ApprovalCollaboratorModel>();
            external_collaborators_links = new List<ApprovalCollaboratorLinkModel>();
        }
    }
}
