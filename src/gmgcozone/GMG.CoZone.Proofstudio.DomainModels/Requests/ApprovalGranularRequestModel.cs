﻿namespace GMG.CoZone.Proofstudio.DomainModels.Requests
{
    public class ApprovalGranularRequestModel : BaseGetEntityRequestModel
    {        
        public string approvalBasic { get; set; }
    }
}
