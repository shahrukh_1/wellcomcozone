﻿namespace GMG.CoZone.Proofstudio.DomainModels.Requests
{
    public class ApprovalRequestModel : BaseGetEntityRequestModel
    {
        private new int? Id { get; set; }
        public int approvalId { get; set; }
        public string approvalsIds { get; set; }
        public bool? isPreviousVersion { get; set; }
        public int ? selectedHtmlPageId { get; set; }
    }
}
