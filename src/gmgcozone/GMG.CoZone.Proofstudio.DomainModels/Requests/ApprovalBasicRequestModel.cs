﻿namespace GMG.CoZone.Proofstudio.DomainModels.Requests
{
    public class ApprovalBasicRequestModel : BaseGetEntityRequestModel
    {
        private new int? Id { get; set; }        
        public string approvalsIds { get; set; }

    }
}
