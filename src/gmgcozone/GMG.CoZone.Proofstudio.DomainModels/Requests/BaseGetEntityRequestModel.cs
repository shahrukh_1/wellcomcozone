﻿namespace GMG.CoZone.Proofstudio.DomainModels.Requests
{
    public class BaseGetEntityRequestModel
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
    }
}
