﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalBasicDataModel
    {
        public int ID { get; set; }
        public int ApprovalId { get; set; }
        public int JobId { get; set; }
        public int? CurrentPhaseId { get; set; }
        public int FirstPageId { get; set; }
        public int? PDM { get; set; }
    }
}
