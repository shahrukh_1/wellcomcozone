﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalModel
    {
        public int ID { get; set; }
        public int Creator { get; set; }
        public string ErrorMessage { get; set; }
        public string Title { get; set; }
        public string ApprovalType { get; set; }
        public int[] Versions { get; set; }
    }
}
