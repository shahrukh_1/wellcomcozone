﻿using System;

namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ErrorModel : Exception
    {
        public ErrorApiCodesEnum code { get; set; }
        public String message { get; set; }
        public ErrorModel(ErrorApiCodesEnum code, String message) : base(message)
        {
            this.code = code;
            this.message = message;
        }
    }
}
