﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalCollaboratorModel
    {
        public int ID { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string AssignedDate { get; set; }
        public string CollaboratorAvatar { get; set; }
        public int? CollaboratorRole { get; set; }
        public string UserColor { get; set; }
        public bool? IsAcessRevoked { get; set; }
        public string SubstituteUserName { get; set; }
        public bool UserStatus { get; set; }
    }
}
