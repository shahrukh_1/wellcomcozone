﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class UserGroupDecisionModel
    {
        public int? ID { get; set; }
        public int user_group { get; set; }
        public int? decision { get; set; }
        public int? approval_version { get; set; }
    }
}
