﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalVersionModel
    {
        public int ID { get; set; }
        public int Creator { get; set; }
        public string ErrorMessage { get; set; }
        public string Guid { get; set; }
        public bool IsDeleted { get; set; }
        public int? Parent { get; set; }
        public string Title { get; set; }
        public int VersionNumber { get; set; }
        public bool IsLocked { get; set; }
        public bool AllowDownloadOriginal { get; set; }
        public int? Decision { get; set; }
        public string ApprovalType { get; set; }
        public string MovieFilePath { get; set; }
        public string VersionThumbnail { get; set; }
        public string JobStatus { get; set; }
        public bool IsRGBColorSpace { get; set; }
        public int PagesCount { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsRejected { get; set; }
        public int? CurrentPhase { get; set; }
        public string CurrentPhaseName { get; set; }
        public string VersionLabel { get; set; }
        public string PageVersionLabel { get; set; }
        public int OrderInDropDownMenu { get; set; }
        public bool CurrentPhaseShouldBeViewedByAll { get; set; }
        public bool RestrictedZoom { get; set; }
        public string AccountRegion { get; set; }
        public string AccountDomain { get; set; }
        public string VersionSufix { get; set; }
    }
}
