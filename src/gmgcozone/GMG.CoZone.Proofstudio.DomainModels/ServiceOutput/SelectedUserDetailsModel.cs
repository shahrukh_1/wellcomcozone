﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class SelectedUserDetailsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BgColor { get; set; }
        public bool IsExternal { get; set; }
    }
}
