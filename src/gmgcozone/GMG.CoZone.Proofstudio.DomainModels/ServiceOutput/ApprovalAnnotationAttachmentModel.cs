﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalAnnotationAttachmentModel
    {
        public int ID { get; set; }
        public int ApprovalAnnotation { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string Guid { get; set; }
    }
}
