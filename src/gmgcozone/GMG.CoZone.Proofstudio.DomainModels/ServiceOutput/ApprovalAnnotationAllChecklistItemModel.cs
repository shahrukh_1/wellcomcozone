﻿
namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalAnnotationAllChecklistItemModel
    {
            public int ID { get; set; }
            public int ChecklistItemID { get; set; }
            public string ChecklistItemName { get; set; }
            public string ChecklistItemValue { get; set; }
            public string Creator { get; set; }
            public string CreatedDate { get; set; }
    }
}
