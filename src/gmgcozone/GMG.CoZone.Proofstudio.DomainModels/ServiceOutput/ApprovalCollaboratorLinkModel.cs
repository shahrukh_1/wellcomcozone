﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalCollaboratorLinkModel
    {
        public int ID { get; set; }
        public int collaborator_id { get; set; }
        public int approval_version { get; set; }
        public int approval_role { get; set; }
        public bool IsExpired { get; set; }
        public string ExpireDate { get; set; }
        public int? Decision { get; set; }
        public int? UserGroup { get; set; }
        public bool CanApprove { get; set; }
        public bool CanAnnotate { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsRejected { get; set; }
    }
}
