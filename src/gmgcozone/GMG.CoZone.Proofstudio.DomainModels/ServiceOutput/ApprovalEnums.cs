﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public enum ApprovalType
    {
        Image = 0,
        Video = 1,
        Swf = 2
    }

    public enum ErrorApiCodesEnum
    {
        NoContent = 204,
        BadRequest = 400,
        Forbidden = 403,
        NotFound = 404,
        Conflict = 409,
        InternalServerError = 500
    }
}
