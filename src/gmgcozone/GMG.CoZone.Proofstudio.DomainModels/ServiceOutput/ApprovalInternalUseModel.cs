﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalInternalUseModel
    {
        public int ID { get; set; }
        public int Creator { get; set; }
        public string ErrorMessage { get; set; }
        public string Title { get; set; }
        public int Account { get; set; }
        public string ApprovalTypeName { get; set; }
        public bool IsRGBColorSpace { get; set; }
        public int Job { get; set; }
        public int? CurrentPhase { get; set; }
    }
}
