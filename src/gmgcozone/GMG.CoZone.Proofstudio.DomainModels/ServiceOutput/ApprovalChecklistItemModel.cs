﻿
namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
  public  class ApprovalChecklistItemModel
    {
        public int ID { get; set; }
        public int Id { get; set; }
        public string Item { get; set; }
        public bool IsItemChecked { get; set; }
        public string ItemValue { get; set; } 
        public int ? TotalChanges { get; set; }
        public bool ShowNoOfChanges { get; set; }
       
    }
}
