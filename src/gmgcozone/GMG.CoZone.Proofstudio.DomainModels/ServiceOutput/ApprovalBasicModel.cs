﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class ApprovalBasicModel
    {
        public int ApprovalId { get; set; }
        public int JobId { get; set; }
        public int? CurrentPhaseId { get; set; }
    }
}
