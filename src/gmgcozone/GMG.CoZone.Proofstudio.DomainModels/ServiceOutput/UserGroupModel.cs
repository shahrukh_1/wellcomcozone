﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class UserGroupModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
