﻿namespace GMG.CoZone.Proofstudio.DomainModels.ServiceOutput
{
    public class SoftProofingSessionParamsModel
    {
        public int ID { get; set; }
        public string OutputRgbProfileName { get; set; }
        public string SessionGuid { get; set; }
        public string ActiveChannels { get; set; }
        public int Version { get; set; }
        public int? SimulationProfileId { get; set; }
        public int? EmbeddedProfileId { get; set; }
        public int? CustomProfileId { get; set; }
        public int? PaperTintId { get; set; }
        public bool DefaultSession { get; set; }
        public bool HighResolution { get; set; }
        public bool EnableHighResolution { get; set; }
        public bool HighResolutionInProgress { get; set; }
    }
}
