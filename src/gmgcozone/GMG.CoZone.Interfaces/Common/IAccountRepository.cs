﻿using GMG.CoZone.Collaborate.DomainModels;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        ApprovalInfoModel GetApprovalInfo(int approvalId);
    }
}
