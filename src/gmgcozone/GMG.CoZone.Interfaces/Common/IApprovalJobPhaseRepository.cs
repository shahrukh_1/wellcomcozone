﻿using System.Collections.Generic;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common
{
    public interface IApprovalJobPhaseRepository : IGenericRepository<ApprovalJobPhase>
    {
        List<int> GetInternalUserVisiblePhases(int approvalId, int? currentPhase);
        List<int> GetExternalUserVisiblePhases(int approvalId, int? currentPhase);
    }
}
