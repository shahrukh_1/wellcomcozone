﻿using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common
{
    public interface ISoftProofingSessionParamRepository: IGenericRepository<SoftProofingSessionParam>
    {
        SoftProofingSessionParamsModel GetExistingProofingSessionParameters(int approvalId, InternalUseJobViewingConditionsModel jobViewingConditions);
        SoftProofingSessionParamsModel GetExistingProofingSessionParameters(int approvalId, int? paperTintId, InternalUseApprovalViewingConditionsModel viewingConditions);

        SoftProofingSessionParamsModel GetDefaultCMYKProofingSessionParameters(int approvalId);
        SoftProofingSessionParamsModel GetDefaultRGBProofingSessionParameters(int approvalId);

        SoftProofingSessionParamsModel CreateNewSoftproofingParameters(int approvalId, InternalUseJobViewingConditionsModel viewingConditions, bool highRes);
        SoftProofingSessionParamsModel CreateNewSoftproofingParameters(int approvalId, int? paperTintId, InternalUseApprovalViewingConditionsModel viewingConditions);

        InternalUseJobViewingConditionsModel GetJobViewingCondition(int jobId);
        ViewingCondition GetViewingConditionByJobId(int jobId); 
        InternalUseApprovalViewingConditionsModel GetApprovalViewingCondition(int approvalId);

        int GetApprovalIdByProofStudioSession(string sessionId);
        void ResetDefaultForNonHighResSessionParams(int approvalId);
        SoftProofingSessionParam GetDefaultSessionParamsByApprovalId(int approvalId);
        string GetApprovalHighResSoftProofingSessionInProgress(int approvalId);
    }
}
