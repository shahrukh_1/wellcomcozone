﻿using System.Collections.Generic;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;
using GMG.CoZone.Collaborate.DomainModels;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalRepository : IGenericRepository<Approval>
    {
        Approval.ApprovalTypeEnum GetApprovalType(int approvalId);

        string GetApprovalTimezone(int approvalId);

        ApprovalInternalUseModel GetApprovalForExternalUser(int approvalId, string userKey);
        ApprovalInternalUseModel GetApprovalForInternalUser(int approvalId, int loggedUserId, bool userIsAdmin, bool userIsManager);

        int GetJobId(int approvalId);

        List<InternalUseVersionModel> GetExternalUserVersions(int jobId, int approvalId, int? currentPhase, string userKey);
        List<InternalUseVersionModel> GetInternalUserVersions(int jobId, int approvalId, int? currentPhase, string userKey, bool isAdministrator);

        int? GetVersionParent(int versionId);
        InternalUseDetailedVersionModel GetDetailedVersion(int versionId);

        bool IsLastVersion(int approvalId);

        List<ApprovalBasicDataModel> GetApprovalsBasics(List<int> approvalsIds);

        int? GetApprovalIdForTranscoding(string transcodingJobId);
        string GetVideoThumbnailPath(ApprovalInfoModel approvalInfo);
      
        List<int> GetApprovalJobIds(List<int> approvalIds);
        List<ApprovalFileModel> GetApprovalsByIdAndCollaborator(List<int> approvalIds, int loggedUserId, bool adminCanViewAllApprovals);
        List<ApprovalFileModel> GetApprovalVersionsByJobIdAndCollaborator(List<int> approvalIds, int loggedUserId, bool adminCanViewAllApprovals);
        



        List<ApprovalFileModel> GetMaxApprovalVersionsByFolderIds(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals);
        List<ApprovalFileModel> GetApprovalVersionsByFolderIds(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals);

        void ResetApprovalProcessingDate(int approvalId);
        bool CheckIfApprovalIsNotDeleted(int approvalId);
    }
}
