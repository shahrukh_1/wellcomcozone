﻿using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalCollaboratorDecisionRepository :IGenericRepository<ApprovalCollaboratorDecision>
    {
        int? GetInternalUserApprovalDecision(int approvalId, string userGuid);
        int? GetExternalUserApprovalDecision(int approvalId, string userGuid);
    }
}
