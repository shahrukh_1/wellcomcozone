﻿using GMGColorDAL;
using System.Collections.Generic;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalCollaboratorRoleRepository : IGenericRepository<ApprovalCollaboratorRole>
    {
        ApprovalCollaboratorRole GetApprovalCollaboratorRole(string approvalCollaboratorRoleKey);

        int GetApprovalCollaboratorRoleID(string approvalCollaboratorRoleKey);

        List<ApprovalCollaboratorRole> GetApprovalCollaboratorRolesList(string[] approvalCollaboratorRolesKeys);

        int[] GetApprovalCollaboratorRolesListID(string[] approvalCollaboratorRolesKeys);
    }
}
