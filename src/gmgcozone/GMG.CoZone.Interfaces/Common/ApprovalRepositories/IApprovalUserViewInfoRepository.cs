﻿using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalUserViewInfoRepository : IGenericRepository<ApprovalUserViewInfo>
    {
        int? GetLatestVisitedApproval(int userId);
    }
}
