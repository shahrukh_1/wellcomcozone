﻿using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalPageRepository : IGenericRepository<ApprovalPage>
    {
        int GetApprovalFirstPageId(int approvalId);
        void ResetApprovalPagesProgress(int approvalId);
    }
}
