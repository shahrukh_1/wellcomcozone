﻿using System;
using System.Collections.Generic;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;
using GMG.CoZone.Collaborate.DomainModels;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalCollaboratorRepository : IGenericRepository<ApprovalCollaborator>
    {
        List<UserGroupModel> GetJobPrimayGroups(int jobId, string userKey, bool isExternalUser);
        List<UserGroupDecisionModel> GetJobPrimaryGroupDecisions(string userGuid, int jobId);

        [Obsolete("This method is obsolete. Call GetInternalUserPartialCollaborators and GetInternalUserCollaborators instead.", false)]
        List<InternalUseCollaboratorModel> GetInternalUserCollaborators(IEnumerable<int> versions, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);
        [Obsolete("This method is obsolete. Call GetExternalUserPartialCollaborators and GetExternalUserCollaborators instead.", false)]
        List<InternalUseCollaboratorModel> GetExternalUserCollaborators(IEnumerable<int> versions, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);

        List<string> GetPredefinedUserColors(int accountID, List<string> avoidColors);
        void AttachColorToExternalCollaborator(int externalCollaboratorID, string userColor);

        List<InternalUseCollaboratorLinkModel> GetInternalUserCollaboratorLinksForUpdateApprovalDecision(int jobId, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);
        List<InternalUseCollaboratorLinkModel> GetExternalUserCollaboratorLinksForUpdateApprovalDecision(int jobId, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);

        [Obsolete("This method is obsolete. Call GetInternalUserPartialCollaborators and GetInternalUserCollaboratorLinks instead.", false)]
        List<InternalUseCollaboratorLinkModel> GetInternalUserCollaboratorLinks(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);
        [Obsolete("This method is obsolete. Call GetExternalUserPartialCollaborators and GetExternalUserCollaboratorLinks instead.", false)]
        List<InternalUseCollaboratorLinkModel> GetExternalUserCollaboratorLinks(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);

        List<InternalUsePartialCollaboratorModel> GetInternalUserPartialCollaborators(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);
        List<InternalUseCollaboratorModel> GetInternalUserCollaborators(List<InternalUsePartialCollaboratorModel> internalUserPartialCollaborators);        
        List<InternalUseCollaboratorLinkModel> GetInternalUserCollaboratorLinks(List<InternalUsePartialCollaboratorModel> internalUserPartialCollaborators);

        List<InternalUsePartialCollaboratorModel> GetExternalUserPartialCollaborators(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases);
        List<InternalUseCollaboratorModel> GetExternalUserCollaborators(List<InternalUsePartialCollaboratorModel> partialExternalCollaborates);
        List<InternalUseCollaboratorLinkModel> GetExternalUserCollaboratorLinks(List<InternalUsePartialCollaboratorModel> partialExternalCollaborates);

        ApprovalCollaboratorsAndDecisionsModel GetAllCollaboratorsAndDecisions(int approvalId, int? phaseId, int approverAndReviewerRoleID);
        ApprovalCollaboratorsAndAnnotationsModel GetAllCollaboratorsAndAnnotations(int approvalId, int? phaseId, int[] approverAndReviewerRolesIDs);
    }
}
