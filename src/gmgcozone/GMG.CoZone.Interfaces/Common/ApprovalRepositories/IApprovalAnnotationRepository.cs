﻿using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Collaborate.DomainModels.Annotation;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories
{
    public interface IApprovalAnnotationRepository : IGenericRepository<ApprovalAnnotation>
    {
        IEnumerable<IGrouping<int, int>> GetAnnotationsOrderedbyCreatedDay(int[] annotationIdsToLoad);
        IEnumerable<IndividualAnnotationModel> GetShapeAndMarkerAnnotations(int[] annotationIds);
        IEnumerable<IndividualAnnotationModel> GetMarkerAnnotations(int[] annotationIds);
    }
}
