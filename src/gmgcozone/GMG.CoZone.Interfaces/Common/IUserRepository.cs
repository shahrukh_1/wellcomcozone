﻿using GMGColorDAL;
using System.Collections;
using System.Collections.Generic;

namespace GMG.CoZone.Repositories.Interfaces.Common
{
    public interface IUserRepository : IGenericRepository<User>
    {
        bool UserIsAdministrator(string userKey);
        bool UserIsManager(string userKey);
        bool InternalUserCanViewAllPhasesAnnotations(string userKey);
        bool ExternalUserCanViewAllPhasesAnnotations(string userKey);
        int GetUserIdByGuid(string userKey);

        bool IsApprovalFromCurrentAccount(List<int> approvalIds, List<int> folderIds, int acountId);

        bool IsFromCurrentAccount(List<int> IdList, string userGuid, string typeOf);

        bool IsUserHasAccess(List<int> approvalIds, int userId);

    }
}
