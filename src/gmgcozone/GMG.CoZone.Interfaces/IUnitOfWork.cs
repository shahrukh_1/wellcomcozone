﻿using GMG.CoZone.Repositories.Interfaces.Common;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMG.CoZone.Repositories.Interfaces.Deliver;
using GMG.CoZone.Repositories.Interfaces.FileTransfer;
using GMG.CoZone.Repositories.Interfaces.Settings;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }
        IAccountRepository AccountRepository { get; }
        IGenericRepository<ValueDataType> ValueDataTypeRepository { get; }
        IAccountSettingsRepository AccountSettingsRepository { get; }
        IAccountSettingsValueTypeRepository AccountSettingsValueTypeRepository { get; }
        IApprovalRepository ApprovalRepository { get; }
        IApprovalCollaboratorRepository ApprovalCollaboratorRepository { get; }
        IApprovalAnnotationRepository ApprovalAnnotationRepository { get; }
        IApprovalUserViewInfoRepository ApprovalUserViewInfoRepository { get; }
        IApprovalCollaboratorRoleRepository ApprovalCollaboratorRoleRepository { get; }
        IGenericRepository<SharedApproval> SharedApprovalRepository { get; }
        IApprovalCollaboratorDecisionRepository ApprovalCollaboratorDecisionRepository { get; }
        IApprovalPageRepository ApprovalPageRepository { get; }
        IGenericRepository<ApprovalJobPhaseApproval> ApprovalJobPhaseApprovalRepository { get; }
        IApprovalJobPhaseRepository ApprovalJobPhaseRepository { get; }
        ISoftProofingSessionParamRepository SoftProofingSessionParamRepository { get; }
        IGenericRepository<FTPPresetJobDownload> FtpPresetJobDownloadRepository { get; }
        IDeliverJobRepository DeliverJobRepository { get; }
        IFileTransferRepository FileTransferRepository { get; }

        void Save();
        int UpdateApprovalState(string query, int state, int approval);
    }
}
