﻿using GMG.CoZone.Collaborate.DomainModels;
using GMG.CoZone.FileTransfer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Repositories.Interfaces.FileTransfer
{
    public interface IFileTransferRepository
    {
       int SaveTransfer(FileTransferModel model);
       void SoftDeleteFileTransfer(int id);
       List<int> GetExternalUsersForTransfer(int fileTransferId);
       List<FileTransferModel> GetFileTransfersByIds(List<int> fileTransferIdss, int accountId);
       List<ApprovalFileModel> GetMaxApprovalVersionsByFolderIds(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals);
       void SetDownloadDateForExternal(int fileTransferExternalId);
       void SetDownloadDateForInternal(int fileTransferInternalId);
    }
}
