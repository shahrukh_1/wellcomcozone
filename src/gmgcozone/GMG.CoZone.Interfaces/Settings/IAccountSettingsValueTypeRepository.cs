﻿using GMG.CoZone.Repositories.Interfaces.Common;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Settings
{
    public interface IAccountSettingsValueTypeRepository : IGenericRepository<ValueDataType>
    {
    }
}
