﻿using GMG.CoZone.Repositories.Interfaces.Common;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Interfaces.Settings
{
    public interface IAccountSettingsRepository : IGenericRepository<AccountSetting>
    {
        bool IsShowAllFilesToAdminsChecked(int accountId, string ShowAllFilesToAdminSetting);
    }
}
