﻿using GMG.CoZone.Deliver.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Repositories.Interfaces.Deliver
{
    public interface IDeliverJobRepository
    {
        List<DeliverFileModel> GetDeliverJobsByIds(List<int> deliverJobs);
    }
}
