﻿using System;

namespace GMG.CoZone.Common.Util
{
    /// <summary>
    /// Interface used for retring important methods
    /// </summary>
    public interface IRetryMechanism
    {
        /// <summary>
        /// Try method for a specified number of times and sleep interval
        /// </summary>
        /// <param name="actionToRun">Method to run</param>
        /// <param name="retryInterval">Sleep interval between tries</param>
        /// <param name="retryCount">Number of retries, default value 3</param>
        void Run(Action actionToRun, TimeSpan retryInterval, int retryCount = 3);

        /// <summary>
        /// Try method for a specified number of times and sleep interval
        /// </summary>
        /// <typeparam name="T">The type parameter</typeparam>
        /// <param name="actionToRun">Method to run</param>
        /// <param name="retryInterval">Sleep interval between tries</param>
        /// <param name="retryCount">Number of retries, default value 3</param>
        /// <returns>return type</returns>
        T Run<T>(Func<T> actionToRun, TimeSpan retryInterval, int retryCount = 3);
    }
}
