﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace GMG.CoZone.Common.Util
{
    public class RetryMechanism : IRetryMechanism
    {
        public void Run(Action action, TimeSpan retryInterval, int retryCount = 3)
        {
            Run<object>(() =>
            {
                action();
                return null;
            }, retryInterval, retryCount);
        }

        public T Run<T>(Func<T> action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exceptions = new List<Exception>();
            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    if (retry > 0)
                    {
                        Thread.Sleep(retryInterval);
                    }
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}
