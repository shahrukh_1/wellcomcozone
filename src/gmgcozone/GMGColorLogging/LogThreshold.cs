﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elmah
{
    public class LogThreshold
    {
        public enum Threshold
        {
            Fatal,
            Error,
            Warning,
            Info,
            Debug
        }

        public static Threshold GetThreshold(string threshold)
        {
            switch (threshold)
            {
                case "Fatal": return Threshold.Fatal;
                case "Error": return Threshold.Error;
                case "Warning":return Threshold.Warning;
                case "Info": return Threshold.Info;
                case "Debug":
                default: return Threshold.Debug;
            }
        }
    }
}
