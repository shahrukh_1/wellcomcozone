#region License, Terms and Author(s)
//
// ELMAH - Error Logging Modules and Handlers for ASP.NET
// Copyright (c) 2004-9 Atif Aziz. All rights reserved.
//
//  Author(s):
//
//      Atif Aziz, http://www.raboof.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#endregion

[assembly: Elmah.Scc("$Id: ErrorSignal.cs 566 2009-05-11 10:37:10Z azizatif $")]

namespace Elmah
{
    #region Imports

    using System;
    using System.Collections;
    using System.Web;

    #endregion

    public sealed class ErrorSignal
    {
        public event ErrorSignalEventHandler Raised;

        private static Hashtable _signalByApp;
        private static readonly object _lock = new object();

        public void Raise(Exception e)
        {
            this.Raise(e, (HttpContext)null);
        }

        public void Raise(Exception e, HttpContext context)
        {
            if (context == null)
                context = HttpContext.Current;
            ErrorSignalEventHandler handler = Raised;

            if (handler != null)
                handler(this, new ErrorSignalEventArgs(e, context), string.Empty, LogThreshold.Threshold.Error, 0);
        }

        public void Raise(string logMessage, Exception e)
        {
            this.Raise(logMessage, e, LogThreshold.Threshold.Error);
        }

        public void Raise(string logMessage, Exception e, LogThreshold.Threshold threshold)
        {
            this.Raise(logMessage, e, threshold, 0);
        }

        public void Raise(string logMessage, Exception e, LogThreshold.Threshold threshold, int account)
        {
            HttpContext context = HttpContext.Current;
            ErrorSignalEventHandler handler = Raised;

            if (handler != null)
                handler(this, new ErrorSignalEventArgs(e, context, account, threshold), logMessage, threshold, account);
        }

        public static ErrorSignal FromCurrentContext()
        {
            return FromContext(HttpContext.Current);
        }

        public static ErrorSignal FromContext(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            return Get(context.ApplicationInstance);
        }

        public static ErrorSignal Get(HttpApplication application)
        {
            if (application == null)
                throw new ArgumentNullException("application");

            lock (_lock)
            {
                //
                // Allocate map of object per application on demand.
                //

                if (_signalByApp == null)
                    _signalByApp = new Hashtable();

                //
                // Get the list of modules fot the application. If this is
                // the first registration for the supplied application object
                // then setup a new and empty list.
                //

                ErrorSignal signal = (ErrorSignal)_signalByApp[application];

                if (signal == null)
                {
                    signal = new ErrorSignal();
                    _signalByApp.Add(application, signal);
                    application.Disposed += new EventHandler(OnApplicationDisposed);
                }

                return signal;
            }
        }

        private static void OnApplicationDisposed(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;

            lock (_lock)
            {
                if (_signalByApp == null)
                    return;

                _signalByApp.Remove(application);

                if (_signalByApp.Count == 0)
                    _signalByApp = null;
            }
        }
    }

    //public delegate void ErrorSignalEventHandler(object sender, ErrorSignalEventArgs args);
    public delegate void ErrorSignalEventHandler(object sender, ErrorSignalEventArgs args, string logMessage, LogThreshold.Threshold threshold, int account);

    [Serializable]
    public sealed class ErrorSignalEventArgs : EventArgs
    {
        private readonly Exception _exception;
        [NonSerialized]
        private readonly HttpContext _context;
        private readonly int _account;
        private readonly LogThreshold.Threshold _threshold;

        public ErrorSignalEventArgs(Exception e, HttpContext context)
            : this(e, context, 0, LogThreshold.Threshold.Error)
        {
        }

        public ErrorSignalEventArgs(Exception e, HttpContext context, int account, LogThreshold.Threshold threshold)
        {
            if (e == null)
                throw new ArgumentNullException("e");

            _exception = e;
            _context = context;
            _account = account;
            _threshold = threshold;
        }

        public Exception Exception
        {
            get { return _exception; }
        }

        public HttpContext Context
        {
            get { return _context; }
        }

        public int Account
        {
            get { return _account; }
        }

        public LogThreshold.Threshold Threshold
        {
            get { return _threshold; }
        }

        public override string ToString()
        {
            return Mask.EmptyString(Exception.Message, Exception.GetType().FullName);
        }
    }
}