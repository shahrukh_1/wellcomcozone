﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.IO;
using Browshot;

namespace BrowshotScreenshotService
{
    public static class BrowshotService
    {
        public enum BrowshotStatus
        {
            Error = -1,
            Pending = 0,
            Success = 1
        }

        private static BrowshotClient browshot;

        private static BrowshotClient SnapshotClient(string accessKey)
        {
            if (browshot == null)
            {
                browshot = new BrowshotClient(accessKey); 
            }

            return browshot;
        }

        #region ScreenshotCreate

        public static int ScreenshotCreate(string url, int instanceId, int delay, string accessKey)
        {
            try
            {
                Hashtable arguments = new Hashtable();
                arguments.Add("cache", 60);
                arguments.Add("size", "page");
                //arguments.Add("delay", delay);
                arguments.Add("instance_id", instanceId);
                Dictionary<string, object> result = BrowshotService.SnapshotClient(accessKey).ScreenshotCreate(url, arguments);

                int id = 0;
                if (result != null)
                {
                    if (result.ContainsKey("id"))
                    {
                        id = int.Parse(result["id"].ToString());
                    }
                    else
                    {
                        return 0;
                    }
                }
                return id;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static int ScreenshotInfo(int id, string savePath, string accessKey)
        {
            try
            {
                Dictionary<string, object> result = BrowshotService.SnapshotClient(accessKey).ScreenshotInfo(id);


                if (result.ContainsKey("status"))
                {
                    if (result["status"].ToString() == "finished")
                    {
                        Image thumbnail = BrowshotService.SnapshotClient(accessKey).Thumbnail(id, null);
                        thumbnail.Save(savePath);
                        return (int)BrowshotStatus.Success;
                    }
                    else if (result["status"].ToString() == "error")
                    {
                        return (int)BrowshotStatus.Error;
                    }
                }
                return (int)BrowshotStatus.Pending;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static BrowshotStatus GetScreenshotStatus(int id, string accessKey)
        {
            try
            {
                Dictionary<string, object> result = BrowshotService.SnapshotClient(accessKey).ScreenshotInfo(id);

                if (result.ContainsKey("status"))
                {
                    if (result["status"].ToString() == "finished")
                    {
                        return BrowshotStatus.Success;
                    }
                    else if (result["status"].ToString() == "error")
                    {
                        return BrowshotStatus.Error;
                    }
                }
                return BrowshotStatus.Pending;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static Stream GetThumbnailStream(int id, string accessKey)
        {
            return BrowshotService.SnapshotClient(accessKey).GetThumbnailStream(id, null);
        }

        public static int GetFirefoxInstanceID(string accessKey)
        {
            try
            {
                Dictionary<string, object> list = BrowshotService.SnapshotClient(accessKey).InstanceList();

                Object[] free = (Object[])list["free"];

                int instanceId = 0;
                if (free.Length > 0)
                {
                    foreach (Object obj in free)
                    {
                        Dictionary<string, object> ins = (Dictionary<string, object>)obj;
                        Dictionary<string, object> browser = (Dictionary<string, object>)ins["browser"];
                        if (browser["name"].ToString() == "Firefox")
                        {
                            instanceId = int.Parse(ins["id"].ToString());
                            break;
                        }
                    }
                }
                return instanceId;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
