﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
namespace GMG.WebHelpers.Job
{
    public class JobClient
    {
        #region Fields
        
        #endregion

        #region Methods

        public void PushStatus(JobStatusDetails jobDetails, string url)
        {
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (Stream stream = httpWebRequest.GetRequestStream())
                    {
                        string postData = new JavaScriptSerializer().Serialize(jobDetails);

                        byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                        stream.Write(byteArray, 0, byteArray.Length);
                    }

                    httpWebRequest.KeepAlive = false;

                    HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                    string responseStatus = response.StatusCode.ToString();
                    response.Close();   
                }
            }
            catch (WebException)
            {
                throw new WebException("Push updated job status failed!");
            }
            finally
            {
                
            }
        }

        #endregion
    }
}