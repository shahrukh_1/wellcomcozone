﻿using GMG.CoZone.Common;
using System;

namespace GMGColorNotificationService
{
    public static class NotificationTypeParser
    {
        public static NotificationType? ParseString(string eventType)
        {   
            switch (eventType)
            {
                case "CCM": return NotificationType.ACommentIsMade;
                case "CRC": return NotificationType.RepliesToMyComments;
                case "CNC": return NotificationType.NewAnnotationAdded;
                case "AAA": return NotificationType.NewApprovalAdded;
                case "ADW": return NotificationType.ApprovalWasDownloaded;
                case "AAU": return NotificationType.ApprovalWasUpdated;
                case "AAD": return NotificationType.ApprovalWasDeleted;
                case "ASC": return NotificationType.ApprovalStatusChanged;
                case "AVC": return NotificationType.NewVersionWasCreated;
                case "AAS": return NotificationType.ApprovalWasShared;
                case "UUA": return NotificationType.UserWasAdded;
                case "GGC": return NotificationType.GroupWasCreated;
                case "MPC": return NotificationType.PlanWasChanged;
                case "MAD": return NotificationType.AccountWasDeleted;
                case "MAA": return NotificationType.AccountWasAdded;
                case "DFS": return NotificationType.DeliverFileWasSubmitted;
                case "DFE": return NotificationType.DeliverFileErrored;
                case "DFC": return NotificationType.DeliverFileCompleted;
                case "AWR": return NotificationType.WorkstationRegistered;
                case "IAR": return NotificationType.SharedWorkflowAccessRevoked;
                case "IUD": return NotificationType.InvitedUserDeleted;
                case "IFS": return NotificationType.InvitedUserFileSubmitted;
                case "HSA": return NotificationType.ShareRequestAccepted;
                case "HSD": return NotificationType.SharedWorkflowDeleted;
                case "HUD": return NotificationType.HostAdminUserDeleted;
                case "HFS": return NotificationType.HostAdminUserFileSubmitted;
                case "DFT": return NotificationType.DeliverFileTimeOut;
                case "IWL": return NotificationType.UserWelcome;
                case "ISW": return NotificationType.SharedWorkflowInvitation;
                case "IDP": return NotificationType.DeliverNewPairingCode;
                case "ILP": return NotificationType.LostPassword;
                case "IRP": return NotificationType.ResetPassword;
                case "IAM": return NotificationType.ApprovalReminder;
                case "IBL": return NotificationType.BillingReport;
                case "IAS": return NotificationType.AccountSupport;
                case "DPE": return NotificationType.DemoPlanExpired;               
                case "ILR": return NotificationType.PlanLimitReached;
                case "IVW": return NotificationType.PlanLimitWarning;
                case "ACC": return NotificationType.ApprovalChangesCompleted;
                case "APR": return NotificationType.ApprovalWasRejected;
                case "APO": return NotificationType.ApprovalOverdue;
                case "ACP": return NotificationType.ApprovalStatusChangedByPdm;
                case "PWD": return NotificationType.PhaseWasDeactivated;
                case "PWA": return NotificationType.PhaseWasActivated;
                case "WPR": return NotificationType.WorkflowPhaseRerun;
                case "FTA": return NotificationType.NewFileTransferAdded;
                case "FTD": return NotificationType.FileTransferDownloaded;
                case "IUC": return NotificationType.AtUserInComment;
                case "IUR": return NotificationType.AtUserInReply;
                case "OOF": return NotificationType.OutOfOffice;
                case "CIE": return NotificationType.ChecklistItemEdited;
                case "AJC": return NotificationType.ApprovalJobCompleted;
                case "PFN": return NotificationType.NewProjectFolder;
                case "PFU": return NotificationType.ProjectFolderWasUpdated;
                case "PFS": return NotificationType.ProjectFolderWasShared;
            }
            return null;
        }

        public static string GetKey(NotificationType notificationType)
        {
            switch (notificationType)
            {
                case NotificationType.ACommentIsMade: return "CCM";
                case NotificationType.RepliesToMyComments: return "CRC";
                case NotificationType.NewAnnotationAdded: return "CNC";
                case NotificationType.NewApprovalAdded: return "AAA";
                case NotificationType.ApprovalWasDownloaded: return "ADW";
                case NotificationType.ApprovalWasUpdated: return "AAU";
                case NotificationType.ApprovalWasDeleted: return "AAD";
                case NotificationType.ApprovalStatusChanged: return "ASC";
                case NotificationType.NewVersionWasCreated: return "AVC";
                case NotificationType.ApprovalWasShared: return "AAS";
                case NotificationType.UserWasAdded: return "UUA";
                case NotificationType.GroupWasCreated: return "GGC";
                case NotificationType.PlanWasChanged: return "MPC";
                case NotificationType.AccountWasDeleted: return "MAD";
                case NotificationType.AccountWasAdded: return "MAA";
                case NotificationType.DeliverFileWasSubmitted: return "DFS";
                case NotificationType.DeliverFileErrored: return "DFE";
                case NotificationType.DeliverFileCompleted: return "DFC";
                case NotificationType.WorkstationRegistered: return "AWR";
                case NotificationType.SharedWorkflowAccessRevoked: return "IAR";
                case NotificationType.InvitedUserDeleted: return "IUD";
                case NotificationType.InvitedUserFileSubmitted: return "IFS";
                case NotificationType.ShareRequestAccepted: return "HSA";
                case NotificationType.SharedWorkflowDeleted: return "HSD";
                case NotificationType.HostAdminUserDeleted: return "HUD";
                case NotificationType.HostAdminUserFileSubmitted: return "HFS";
                case NotificationType.DeliverFileTimeOut: return "DFT";
                case NotificationType.UserWelcome: return "IWL";
                case NotificationType.SharedWorkflowInvitation:return "ISW";
                case NotificationType.DeliverNewPairingCode: return "IDP";
                case NotificationType.LostPassword: return "ILP";
                case NotificationType.ResetPassword: return "IRP"; 
                case NotificationType.ApprovalReminder: return "IAM";
                case NotificationType.BillingReport: return "IBL";
                case NotificationType.AccountSupport: return "IAS";
                case NotificationType.DemoPlanExpired: return "DPE";
                case NotificationType.PlanLimitReached: return "ILR";
                case NotificationType.PlanLimitWarning: return "IVW";
                case NotificationType.ApprovalChangesCompleted: return "ACC";
                case NotificationType.ApprovalWasRejected: return "APR";
                case NotificationType.ApprovalOverdue: return "APO";
                case NotificationType.ApprovalStatusChangedByPdm: return "ACP";
                case NotificationType.PhaseWasDeactivated: return "PWD";
                case NotificationType.PhaseWasActivated: return "PWA";
                case NotificationType.WorkflowPhaseRerun: return "WPR";
                case NotificationType.NewFileTransferAdded:return "FTA";
                case NotificationType.FileTransferDownloaded:return "FTD";
                case NotificationType.NewAnnotationAtUserInComment: return "IUC";
                case NotificationType.AtUserInComment: return "IUC";
                case NotificationType.AtUserInReply: return "IUR";
                case NotificationType.OutOfOffice: return "OOF";
                case NotificationType.ChecklistItemEdited: return "CIE";
                case NotificationType.ApprovalJobCompleted: return "AJC";
                case NotificationType.NewProjectFolder: return "PFN";
                case NotificationType.ProjectFolderWasShared: return "PFS";
                case NotificationType.ProjectFolderWasUpdated: return "PFU";
            }
            return String.Empty;
        }

    }
}

