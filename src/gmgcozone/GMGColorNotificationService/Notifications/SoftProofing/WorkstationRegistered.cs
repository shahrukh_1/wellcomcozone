﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class WorkstationRegistered : Notification
    {
        #region Properties

        public string WorkstationName { get; set; }

        public int Account { get; set; }

        #endregion

        #region Ctors

        public WorkstationRegistered()
        {
            NotificationType = NotificationType.WorkstationRegistered;
        }

        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationWorkstationRegistration);
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = SetHeader(template, context);
            emailBody = emailBody.Replace("<$timestamp$>",  GMGColorFormatData.GetTimeDifference(CreatedDate));
            emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");

            string url = string.Empty;

            string accountDomain = (from a in context.Accounts
                                    where a.ID == Account
                                    select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).FirstOrDefault();

            url = string.Format("{0}://{1}/SoftProofing/Settings?type=Workstations", GMGColorConfiguration.AppConfiguration.ServerProtocol, accountDomain);

            string userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            emailBody = string.Format(emailBody, userImageLocation, WorkstationName ?? string.Empty, url);

            return emailBody;
        }

        #endregion
    }
}
