﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class DeliverNotification : Notification
    {
        #region Properties

        public string[] JobsGuids { get; set; }

        public int? ExternalRecipient { get; set; }

        public bool IsSharedJob { get; set; }

        /// <summary>
        /// A string containing all jobs file names (used to customize the email subject and title)
        /// </summary>
        public string FilesName { get; set; }

        /// <summary>
        /// A string containing all jobs user names (used to customize the email subject and title)
        /// </summary>
        public string UsersName { get; set; }

        #endregion

        #region Constructors

        public DeliverNotification(NotificationType eventType)
        {
            NotificationType = eventType;
        }

        #endregion

        #region Merthods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context, true);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);

            email.subject = GetSubject();
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", email.subject);

            if (ExternalRecipient != null)
            {
                string optionalMsg = (from djd in context.DeliverJobDatas
                                      join dj in context.DeliverJobs on djd.DeliverJob equals dj.ID
                                      where JobsGuids.Contains(dj.Guid)
                                      select djd.EmailOptionalMsg).FirstOrDefault();

                emailOuterTemplate = string.IsNullOrEmpty(optionalMsg) ? emailOuterTemplate.Replace("<$is_visible_message$>", "none !important;") : emailOuterTemplate.Replace("<$message$>", optionalMsg);
            }

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;
            string userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            FilesName = UsersName = string.Empty;

            foreach (string jobGuid in JobsGuids)
            {
                string currentJobBody = template;
                currentJobBody = SetHeader(currentJobBody, context);

                string sharedAccountUrl = string.Empty;
                //get the account url of the shared workflow host admin if job was sent to the shared workflow
                if (IsSharedJob)
                {
                    sharedAccountUrl = (from u in context.Users
                                        join ac in context.Accounts on u.Account equals ac.ID
                                        where u.ID == InternalRecipient.Value
                                        select (ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain
                                         )).FirstOrDefault();
                }

                var job = (from dj in context.DeliverJobs
                           join j in context.Jobs on dj.Job equals j.ID
                           join ac in context.Accounts on j.Account equals ac.ID
                           join u in context.Users on dj.Owner equals u.ID
                           where dj.Guid == jobGuid
                           select
                               new
                               {
                                   dj.FileName,
                                   AccountUrl = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                   JobId = dj.ID,
                                   UserName = u.GivenName + " " + u.FamilyName,
                                   ac.Region
                               }).FirstOrDefault();

                if (job != null)
                {
                    FilesName += job.FileName + ", ";
                    UsersName += job.UserName + ", ";


                    //check whether the user is external or not and create specific job details url
                    string jobDetailsLink;
                    if (ExternalRecipient != null)
                    {
                        var extUserGuid =
                            DeliverExternalCollaborator.GetDeliverExternalCollaboratorGuid(ExternalRecipient.Value,
                                context);

                        jobDetailsLink = string.Format("{0}://{1}/Deliver/ExternalUserJobDetailLink?userId={2}&job={3}",
                            GMGColorConfiguration.AppConfiguration.ServerProtocol, job.AccountUrl,
                            extUserGuid, job.JobId);
                    }
                    else
                    {
                        jobDetailsLink = string.Format("{0}://{1}/Deliver/JobDetailLink?selectedinstance={2}&isSharedJob={3}",
                            GMGColorConfiguration.AppConfiguration.ServerProtocol, IsSharedJob ? sharedAccountUrl : job.AccountUrl, job.JobId, IsSharedJob);
                    }

                    currentJobBody = currentJobBody.Replace("<$timestamp$>",
                        GMGColorFormatData.GetTimeDifference(CreatedDate));
                    currentJobBody = FormatEmailBody(currentJobBody, userImageLocation, job.UserName, job.FileName, jobDetailsLink,
                        jobGuid,
                        context);
                    currentJobBody = currentJobBody.Replace("<$thumbnailUrl$>", DeliverJob.GetThumbnailImagePath(jobGuid, job.AccountUrl, job.FileName, job.Region));

                    emailBody += currentJobBody;
                }
            }

            FilesName = FilesName.Remove(FilesName.Length - 2, 2);
            UsersName = UsersName.Remove(UsersName.Length - 2, 2);

            return emailBody;
        }

        public override string GetLocale(GMGColorContext context)
        {
            string localeName = String.Empty;

            if (InternalRecipient != null)
            {
                localeName = base.GetLocale(context);
            }
            else if (ExternalRecipient != null)
            {
                localeName = (from e in context.DeliverExternalCollaborators
                              join l in context.Locales on e.Locale equals l.ID
                              where e.ID == ExternalRecipient
                              select l.Name).FirstOrDefault();
            }

            return localeName;
        }

        #endregion

        #region Abstract methods

        protected abstract string FormatEmailBody(string emailBody, string userImageLocation, string userName, string fileName, string jobDetailsLink, string jobGuid, GMGColorContext context);

        public virtual string GetSubject()
        {
            return EmailUtils.GetEmailTitle(NotificationType);
        }

        #endregion
    }
}
