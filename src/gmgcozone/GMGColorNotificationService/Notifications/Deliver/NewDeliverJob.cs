﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewDeliverJob : DeliverNotification
    {
        #region Constructors

        public NewDeliverJob()
            : base(NotificationType.DeliverFileWasSubmitted)
        {
        }

        #endregion

        #region Override Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationNewDeliverHeader);
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, string fileName, string jobDetailsLink, string jobGuid, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, fileName, jobDetailsLink);
        }

        public override string GetSubject()
        {
            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);

            emailTitle += " on '" + FilesName + "' by " + UsersName;

            return emailTitle;
        }

        #endregion
    }
}
