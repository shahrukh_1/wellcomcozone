﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class DeliverJobError : DeliverNotification
    {
        #region Constructors

        public DeliverJobError() :
            base(NotificationType.DeliverFileErrored)
        {
        }

        #endregion

        #region Abstract Methods

        protected override string SetHeader(string template, GMGColorDAL.GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationDeliverJobProcessingError);
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, string fileName, string jobDetailsLink, string jobGuid, GMGColorDAL.GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, string.Empty, fileName, userName, jobDetailsLink);
        }

        public override string GetSubject()
        {
            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);

            emailTitle = emailTitle.Replace('.', ' ');

            emailTitle += " on '" + FilesName + "' by " + UsersName;

            return emailTitle;
        }

        #endregion
    }
}
