﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class DeliverJobTimeout : DeliverNotification
    {
        #region Constructors

        public DeliverJobTimeout()
            : base(NotificationType.DeliverFileTimeOut)
        {
        }

        #endregion

        #region Abstract Methods


        protected override string SetHeader(string template, GMGColorDAL.GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationDeliverJobTimeOut);
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, string fileName, string jobDetailsLink, string jobGuid, GMGColorDAL.GMGColorContext context)
        {
            int? transmissionTimeout = (from dj in context.DeliverJobs
                                       join czwf in context.ColorProofCoZoneWorkflows on dj.CPWorkflow equals  czwf.ID
                                       where dj.Guid == jobGuid
                                       select czwf.TransmissionTimeout).FirstOrDefault();

            return string.Format(emailBody, userImageLocation, fileName, userName, transmissionTimeout.GetValueOrDefault(), jobDetailsLink);
        }

        public override string GetSubject()
        {
            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);

            emailTitle += " on '" + FilesName + "' by " + UsersName;

            return emailTitle;
        }

        #endregion
    }
}
