﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ProjectFolderWasUpdated : ProjectFolderNotification
    {
        #region Properties

        public int ProjectFolderID { get; set; }

        #endregion

        #region Ctors

        public ProjectFolderWasUpdated() : base(NotificationType.ProjectFolderWasUpdated)
        {

        }

        #endregion

        #region Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            ProjectFolder objProjectFolder = (from pf in context.ProjectFolders
                                   where pf.ID == ProjectFolderID
                                   select pf).FirstOrDefault();

            if (objProjectFolder != null)
            {
                string ProjectFolderName =   objProjectFolder.Name;

                string projectFolderIndexURL = ProjectFolder.GetProjectFolderURL(objProjectFolder.Account1);
                string ProjectFolderNameUrl = "<strong><a style=\"color: #1997c4; text-decoration: none;\" href=\"" + projectFolderIndexURL + "\">" + ProjectFolderName + "</a></strong>";

                emailBody = template;
                emailBody = SetHeader(emailBody, context);
                emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
                emailBody = string.Format(emailBody, userImageLocation, userName, ProjectFolderNameUrl);
            }
            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationProjectFolderWasUpdatedheader);
        }

        #endregion

    }
}
