﻿using System;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class ProjectFolderNotification : Notification
    {

        #region Properties

        //public int ProjectFolderID { get; set; }
        public int Account { get; set; }

        #endregion

        #region Ctors

        public ProjectFolderNotification(NotificationType notificationType)
        {
            NotificationType = notificationType;
        }

        #endregion
    }
}
