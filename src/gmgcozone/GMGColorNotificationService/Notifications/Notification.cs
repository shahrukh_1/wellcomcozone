﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using GMGColorDAL;
using System.ComponentModel;
using System.Text;
using GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class Notification
    {
        #region Properties
        [XmlIgnore]
        public int? InternalRecipient { get; set; }
        [XmlIgnore]
        public NotificationType NotificationType { get; protected set; }
        [XmlIgnore]
        public DateTime CreatedDate { get; set; }
        [XmlIgnore]
        public int? EventCreator { get; set; }
        public int? ApprovalID { get; set; }
        public int? ExternalRecipient { get; set; }

        #endregion

        #region Virtual Methods

        /// <summary>
        /// Initialize the notification item with the given common fields and the custom fields XML
        /// </summary>
        /// <param name="internalRecipient"></param>
        /// <param name="notificationType"></param>
        /// <param name="createdDate"></param>
        /// <param name="eventCreator"></param>
        /// <param name="customFieldsXML"></param>
        public virtual void Init(int? internalRecipient, DateTime createdDate, int? eventCreator, string customFieldsXML)
        {
            InternalRecipient = internalRecipient;
            CreatedDate = createdDate;
            EventCreator = eventCreator;
            CustomFieldsFromXML(customFieldsXML);
        }


        /// <summary>
        ///  Serialize the custom fields (properties not marked as serializable) into a XML string
        /// </summary>
        /// <returns>The XML string containing the custom properties serialized as XML</returns>
        public virtual string CustomFieldsToXML()
        {
            var stringwriter = new StringWriter();
            var serializer = new XmlSerializer(GetType());
            serializer.Serialize(stringwriter, this);
            return stringwriter.ToString();
        }

        /// <summary>
        /// Loads the custom properties from the given xml string
        /// </summary>
        /// <param name="customFieldsXml"></param>
        protected virtual void CustomFieldsFromXML(string customFieldsXml)
        {
            XDocument xml = XDocument.Parse(customFieldsXml);
            XElement objNode = (from c in xml.Descendants(GetType().Name) select c).FirstOrDefault();

            if (objNode != null)
            {
                IEnumerable<XElement> list = objNode.Elements();

                foreach (XElement element in list)
                {
                    PropertyInfo propertyInfo = GetType().GetProperty(element.Name.LocalName);
                    if (propertyInfo.PropertyType.IsArray || element.HasElements)
                    {
                        LoadCollectionFromNode(element, propertyInfo);
                    }
                    else
                    {
                        object value = null;
                        if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            if (!String.IsNullOrEmpty(element.Value))
                            {
                                TypeConverter conv = TypeDescriptor.GetConverter(propertyInfo.PropertyType);
                                value = conv.ConvertFrom(element.Value);
                            }
                        }
                        else
                        {
                            value = Convert.ChangeType(element.Value, propertyInfo.PropertyType);
                        }
                        
                        propertyInfo.SetValue(this, value, null);    
                    }
                }
            }
        }

        /// <summary>
        /// Gets Thread Locale Name
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual string GetLocale(GMGColorContext context)
        {
            string localeName = String.Empty;

            if (InternalRecipient != null)
            {
                localeName = (from u in context.Users 
                              join l in context.Locales on u.Locale equals l.ID
                              where u.ID == InternalRecipient
                              select l.Name).FirstOrDefault();
            }

            return localeName;
        }

        /// <summary>
        /// Create email item to be sent based on notification
        /// </summary>
        /// <param name="emailBodyTemplate"></param>
        /// <param name="occurrenceTemplate"></param>
        /// <param name="emailOuterTemplate"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate,
            string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.Value, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailSubject = GetNotificationSubject(context);
            email.subject = emailSubject;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //return null if body is empty(meaning that data has been deleted from DB)
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected virtual string GetNotificationSubject(GMGColorContext context)
        {
            return EmailUtils.GetEmailTitle(NotificationType);
        }

        #endregion

        #region Abstract Methods

        protected abstract string SetHeader(string template, GMGColorContext context);

        public abstract string CreateBody(string template, GMGColorContext context, bool passAnnotationID);

        #endregion

        #region Public Methods

        public string GetEventCreatorUserFullName(GMGColorContext context)
        {
            string userFullName = string.Empty;

            User eventCreatorUser = GetEventCreatorUser(context);

            if (eventCreatorUser != null)
            {
                userFullName = eventCreatorUser.GivenName + " " + eventCreatorUser.FamilyName;
            }

            return userFullName;
        }

        public static Approval GetApprovalById(int approval, GMGColorContext context)
        {
            Approval objApproval = (from a in context.Approvals
                                    where a.ID == approval
                                    select a).FirstOrDefault();
            return objApproval;
        }

        public static FileTransfer GetFileTransferById(int fileTransfer, GMGColorContext context)
        {
            FileTransfer objFileTransfer = (from a in context.FileTransfers
                                        where a.ID == fileTransfer
                                        select a).FirstOrDefault();
            return objFileTransfer;
        }

        public string GetApprovalJobTitle(int approvalId, GMGColorContext context)
        {
            StringBuilder fileNames = new StringBuilder();

            Approval objApproval = GetApprovalById(approvalId, context);

            if (objApproval != null)
            {
                fileNames.Append(objApproval.Job1.Title);
            }

            return fileNames.ToString();
        }

        public string GetApprovalJobTitle(int[] approvalsIds, GMGColorContext context)
        {
            StringBuilder fileNames = new StringBuilder();

            if (approvalsIds.Count() > 0)
            {
                for (int i = 0; i < approvalsIds.Count(); i++)
                {
                    Approval objApproval = GetApprovalById(approvalsIds[i], context);

                    if (objApproval != null)
                    {
                        fileNames.Append(objApproval.Job1.Title);
                    }

                    // Do not put comma after last word
                    if (i != approvalsIds.Count() - 1)
                    {
                        fileNames.Append(", ");
                    }
                }
            }

            return fileNames.ToString();
        }

        public string GetApprovalLastChangedDecision(int[] approvalsIds, GMGColorContext context)
        {
            string status = string.Empty;

            if (approvalsIds.Count() > 0)
            {
                for (int i = 0; i < approvalsIds.Count(); i++)
                {
                    Approval objApproval = GetApprovalById(approvalsIds[i], context);

                    if (objApproval != null)
                    {
                        User eventCreator = GetEventCreatorUser(context);

                        if (eventCreator != null)
                        {
                            List<ApprovalCollaboratorDecision> eventCreatorDecisions = new List<ApprovalCollaboratorDecision>();

                            eventCreatorDecisions = objApproval.ApprovalCollaboratorDecisions.Where(d => d.Collaborator.HasValue && d.Decision.HasValue  && d.Collaborator == eventCreator.ID).ToList();

                            if (eventCreatorDecisions.Count > 0 && eventCreatorDecisions.Last() != null)
                            {
                                ApprovalDecision lastDecision = eventCreatorDecisions.Last().ApprovalDecision;

                                status = ApprovalDecision.GetLocalizedApprovalDecisionName(lastDecision);
                            }
                        }
                    }
                }
            }

            return status;
        }

        public string GetNotificationSubjectText(string message, int approvalId, GMGColorContext context)
        {
            string userName = GetEventCreatorUserFullName(context);

            string fileName = GetApprovalJobTitle(approvalId, context);

            return string.Format(Resources.lblNotification_SubjectTemplate, message, userName, fileName);
        }

        public string GetNotificationStatusChangedSubjectText(string message, int[] approvalsIds, GMGColorContext context, bool isPDM = false)
        {
            string userName = GetEventCreatorUserFullName(context);

            if (isPDM)
            {
                userName = "PDM " + userName;
            }

            string fileNames = GetApprovalJobTitle(approvalsIds, context);

            string status = GetApprovalLastChangedDecision(approvalsIds, context);

            return string.Format(Resources.lblNotification_StatusChangeSubjectTemplate, message, status, userName, fileNames);
        }

        public string GetNotificationApprovalJobCompleteSubjectText(string message, int[] approvalsIds, GMGColorContext context, bool isPDM = false)
        {
            string userName = GetEventCreatorUserFullName(context);

            if (isPDM)
            {
                userName = "PDM " + userName;
            }

            string fileNames = GetApprovalJobTitle(approvalsIds, context);

            return string.Format(Resources.lblNotification_All_Decisions_Completed, message, userName, fileNames);
        }

        public string GetNotificationSubjectOnlyFileText(string message, int approvalId, GMGColorContext context)
        {
            string fileName = GetApprovalJobTitle(approvalId, context);

            return string.Format(Resources.lblNotification_SubjectTemplateOnlyFile, message, fileName);
        }

        public string GetNotificationSubjectTextForMultipleFiles(string message, int[] approvalsIds, GMGColorContext context)
        {
            string userName = GetEventCreatorUserFullName(context);

            string fileName = GetApprovalJobTitle(approvalsIds, context);

            return string.Format(Resources.lblNotification_SubjectTemplate, message, userName, fileName);
        }

        public string GetNotificationSubjectTextForPenultimateVersion(string message, int approvalId, GMGColorContext context)
        {
            string userName = GetEventCreatorUserFullName(context);

            Job job = (from j in context.Jobs
                         join a in context.Approvals on j.ID equals a.Job
                         where a.ID == approvalId
                         select j).FirstOrDefault();

            string fileName = string.Empty;

            if (job != null) 
            {
                List<Approval> availableApprovals = job.Approvals.Where(a => a.IsDeleted == false).ToList();
                
                if (availableApprovals != null && availableApprovals.Count > 0)
                {
                    approvalId = availableApprovals[availableApprovals.Count - 2].ID;
                    fileName = GetApprovalJobTitle(approvalId, context);    
                }
            }

            return string.Format(Resources.lblNotification_SubjectTemplate, message, userName, fileName);
        }

        public string GetNotificationSubjectTextForDeletedAccount(int eventCreator, string accountName, GMGColorContext context)
        {
            var eventCreatorName = context.Users.Where(u => u.ID == eventCreator).Select(n => n.GivenName + " " + n.FamilyName).FirstOrDefault();
            return string.Format(Resources.lblNotification_AccountDeletedSubjectTemplate, accountName, eventCreatorName);
        }

        #endregion

        #region Private Methods       

        /// <summary>
        /// Reads the child nodes values and set them as the value of the collection property
        /// </summary>
        /// <param name="parentElement"></param>
        /// <param name="propertyInfo"></param>
        private void LoadCollectionFromNode(XElement parentElement, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                int length = parentElement.Elements().Count();
                var elementType = propertyInfo.PropertyType.GetElementType();
                var array = Array.CreateInstance(elementType, length);

                for (int i = 0; i < length; i++)
                    array.SetValue(Convert.ChangeType(parentElement.Elements().ElementAt(i).Value, elementType), i);

                propertyInfo.SetValue(this, array, null);

                //var values = (from e in parentElement.Elements() select Convert.ChangeType(e.Value, propertyInfo.PropertyType.GetElementType())).ToArray();
                //propertyInfo.SetValue(this, values, null);
            }
            else
            {
                var values = (from e in parentElement.Elements() select e.Value).ToList();
                propertyInfo.SetValue(this, values, null);
            }
        }

        public User GetEventCreatorUser(GMGColorContext context)
        {
            User eventCreator = (from u in context.Users
                                 where u.ID == EventCreator.Value
                                 select u).FirstOrDefault();

            if (eventCreator != null)
            {
                return eventCreator;
            }

            return null;
        }


        #endregion
    }
}
