﻿using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using System;
using System.Linq;

namespace GMGColorNotificationService.Notifications.Approvals
{
    public class ApprovalJobCompleted : ApprovalNotification
    {
        #region Properties

        public int NewDecisionId { get; set; }

        #endregion

        #region Constructors

        public ApprovalJobCompleted()
            : base(NotificationType.ApprovalJobCompleted)
        {
        }

        public ApprovalJobCompleted(NotificationType notificationType)
            : base(notificationType)
        {
        }

        #endregion

        #region Override Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = ApprovalFolderId(context).HasValue ? Resources.notificationApprovalJobCompletedFolderHeader : Resources.notificationApprovalJobCompletedHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;
            string creatorFullName;
            string userImageLocation;
            User objRecipientUser = null;
            ExternalCollaborator objExternalRecipientUser = null;

            if (EventCreator != null)
            {
                //internal made decision
                objRecipientUser =
                    (from u in context.Users
                     where u.ID == EventCreator
                     select u).FirstOrDefault();

                creatorFullName = objRecipientUser.GivenName + " " + objRecipientUser.FamilyName;

                userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            }
            else
            {
                //external made decision
                objExternalRecipientUser =
                   (from ex in context.ExternalCollaborators
                    where ex.ID == ExternalCreator
                    select ex).FirstOrDefault();

                creatorFullName = objExternalRecipientUser.GivenName + " " + objExternalRecipientUser.FamilyName;

                int approvalId = ApprovalsIds[0];
                string accountDomain = (from a in context.Accounts
                                        join j in context.Jobs on a.ID equals j.Account
                                        join app in context.Approvals on j.ID equals app.Job
                                        where app.ID == approvalId
                                        select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).FirstOrDefault();

                userImageLocation = User.GetDefaultImagePath(accountDomain);
            }

            string decisionKey = (from apd in context.ApprovalDecisions
                                  where apd.ID == NewDecisionId
                                  select apd.Key).FirstOrDefault();

            var customDecision = objExternalRecipientUser != null
                                                        ? (from acd in context.ApprovalCustomDecisions
                                                           join ec in context.ExternalCollaborators on acd.Account equals ec.Account
                                                           where ec.ID == ExternalCreator && acd.Decision == NewDecisionId && acd.Locale == ec.Locale
                                                           select acd.Name).FirstOrDefault()
                                                        : (from acd in context.ApprovalCustomDecisions
                                                           join u in context.Users on acd.Account equals u.Account
                                                           where u.ID == EventCreator && acd.Decision == NewDecisionId && acd.Locale == u.Locale
                                                           select acd.Name).FirstOrDefault();


            string newDecisionName = !string.IsNullOrEmpty(customDecision) ? customDecision : ApprovalDecision.GetLocalizedApprovalDecision(decisionKey);

            for (int i = ApprovalsIds.Length - 1; i >= 0; i--)
            {
                _currentApprovalID = ApprovalsIds[i];
                string currentApprovalBody = template;

                currentApprovalBody = SetHeader(currentApprovalBody, context);
                currentApprovalBody = currentApprovalBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));

                SharedApproval objSharedApproval = null;

                if (ExternalCreator != null)
                {
                    objSharedApproval = (from sh in context.SharedApprovals
                                         where sh.Approval == _currentApprovalID && sh.ExternalCollaborator == ExternalCreator
                                         select sh).FirstOrDefault();
                }

                Approval objApproval = (from ap in context.Approvals
                                        where ap.ID == _currentApprovalID
                                        select ap).FirstOrDefault();

                if (objApproval != null)
                {
                    string approvalViewUrl = Approval.GetApprovalHtml5ViewURL(objApproval, objRecipientUser, objExternalRecipientUser, objSharedApproval, ExternalCreator != null, 0);

                    string commentedApproval = "<strong><a style=\"color: #1997c4; text-decoration: none;\" href=\"" + approvalViewUrl + "\">" + objApproval.Job1.Title + "</a></strong>";

                    string folderName = Approval.GetFolderName(_currentApprovalID, context);
                    if (folderName != null)
                    {
                        currentApprovalBody = string.Format(currentApprovalBody, userImageLocation, creatorFullName, commentedApproval, folderName);
                    }
                    else
                    {
                        currentApprovalBody = string.Format(currentApprovalBody, userImageLocation, creatorFullName, commentedApproval);
                    }

                    string approvalThumbLocation = "";
                    if (objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip)
                    {
                        approvalThumbLocation = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + Account.GetDomain(objApproval.Job1.Account, context) + "/content/img/html.gif";
                        currentApprovalBody = currentApprovalBody.Replace("<$approval_thumbnail$>", approvalThumbLocation + "\" style= \"width:100%");
                    }
                    else
                    {
                        approvalThumbLocation = objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(objApproval.ID, Account.GetDomain(objApproval.Job1.Account, context), context)
                                                                                                                        : Approval.GetThumbnailImagePath(objApproval.ID, context);
                        currentApprovalBody = currentApprovalBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
                    }
                    currentApprovalBody = currentApprovalBody.Replace("<$body$>", null);

                    emailBody += currentApprovalBody;
                }
                else
                {
                    //current approval has been deleted exclude it from email
                    ApprovalsIds = ApprovalsIds.Where(val => val != _currentApprovalID).ToArray();
                }
            }

            return emailBody;
        }

        #endregion

        #region Methods

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationApprovalJobCompleteSubjectText(Resources.lblNotification_All_Decisions_Completed, ApprovalsIds, context);
        }

        #endregion
    }
}
