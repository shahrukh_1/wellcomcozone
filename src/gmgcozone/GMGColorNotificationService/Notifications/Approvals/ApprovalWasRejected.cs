﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL.Common;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalWasRejected : ApprovalNotification
    {
        #region Properties
        #endregion

        #region Constructor

        public ApprovalWasRejected()
            :base(NotificationType.ApprovalWasRejected)
        {
        }
        #endregion

        #region Override Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = ApprovalFolderId(context).HasValue ? Resources.notificationApprovalWasRejectedInFolderHeader : Resources.notificationApprovalWasRejectedHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;
            string creatorFullName = string.Empty;
            string userImageLocation;

            if (EventCreator != null)
            {
                //internal made decision
                var objCreatorUser =
                    (from u in context.Users
                     where u.ID == EventCreator.Value
                     select u).FirstOrDefault();

                if (objCreatorUser != null)
                    creatorFullName = objCreatorUser.GivenName + " " + objCreatorUser.FamilyName;

                userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            }
            else
            {
                //external made decision
                var objExternalCreatorUser =
                   (from ex in context.ExternalCollaborators
                    where ex.ID == ExternalCreator
                    select ex).FirstOrDefault();

                if (objExternalCreatorUser != null)
                    creatorFullName = objExternalCreatorUser.GivenName + " " + objExternalCreatorUser.FamilyName;

                int approvalId = ApprovalsIds[0];
                string accountDomain = (from a in context.Accounts
                                        join j in context.Jobs on a.ID equals j.Account
                                        join app in context.Approvals on j.ID equals app.Job
                                        where app.ID == approvalId
                                        select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).FirstOrDefault();

                userImageLocation = User.GetDefaultImagePath(accountDomain);
            }

            for (int i = ApprovalsIds.Length - 1; i >= 0; i--)
            {
                _currentApprovalID = ApprovalsIds[i];
                string currentApprovalBody = template;

                SharedApproval objSharedApproval = null;

                if (ExternalCreator != null)
                {
                    objSharedApproval = (from sh in context.SharedApprovals
                                         where sh.Approval == _currentApprovalID && sh.ExternalCollaborator == ExternalCreator
                                         select sh).FirstOrDefault();
                }

                Approval objApproval = (from ap in context.Approvals
                                        where ap.ID == _currentApprovalID
                                        select ap).FirstOrDefault();

                var objRecipientUser = User.GetObject(InternalRecipient.GetValueOrDefault(), context);
                if (objApproval != null)
                {
                    string approvalViewUrl = Approval.GetApprovalHtml5ViewURL(objApproval, objRecipientUser, null, objSharedApproval, false, 0);

                    string approvalUrl = "<strong><a style=\"color: #1997c4; text-decoration: none;\" href=\"" + approvalViewUrl + "\">" + objApproval.Job1.Title + "</a></strong>";
                  
                    currentApprovalBody = SetHeader(currentApprovalBody, context);
                    currentApprovalBody = currentApprovalBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                    currentApprovalBody = currentApprovalBody.Replace("<$is_visible_thumbnail$>", "none !important;");

                    string folderName = Approval.GetFolderName(_currentApprovalID, context);
                    currentApprovalBody = folderName != null ? string.Format(currentApprovalBody, userImageLocation, creatorFullName, approvalUrl, folderName)
                                                             : string.Format(currentApprovalBody, userImageLocation, creatorFullName, approvalUrl);

                    currentApprovalBody = currentApprovalBody.Replace("<$body$>", null);

                    emailBody += currentApprovalBody;
                }
                else
                {
                    //current approval has been deleted exclude it from email
                    ApprovalsIds = ApprovalsIds.Where(val => val != _currentApprovalID).ToArray();
                }
            }

            return emailBody;
        }

        #endregion
		
		#region Methods

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForMultipleFiles(Resources.lblNotification_ApprovalWasRejected, ApprovalsIds, context);
        }

        #endregion
    }
}
