﻿
using GMGColorDAL;
using GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalStatusChangedByPDM : ApprovalStatusChanged
    {
        #region Properties

        #endregion

        #region Constructors

        public ApprovalStatusChangedByPDM()
            : base(NotificationType.ApprovalStatusChangedByPdm)
        {
        }

        #endregion

        #region Override Methods
        
        #endregion

        #region Methods

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationStatusChangedSubjectText(Resources.lblNotifitation_StatusChanged, ApprovalsIds, context, true);
        }

        #endregion
    }
}
