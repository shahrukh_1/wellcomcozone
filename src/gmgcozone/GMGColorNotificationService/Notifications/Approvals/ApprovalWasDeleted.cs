﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL.Common;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalWasDeleted : ApprovalNotification
    {
        #region Properties
        #endregion

        #region Constructor

        public ApprovalWasDeleted()
            :base(NotificationType.ApprovalWasDeleted)
        {
        }
        #endregion

        #region Override Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = ApprovalFolderId(context).HasValue ? Resources.notificationApprovalDeletedInFolderHeader : Resources.notificationApprovalDeletedHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;

            var creatorFullName =
                    (from u in context.Users
                     where u.ID == EventCreator
                     select u.GivenName + " " + u.FamilyName).FirstOrDefault();

            string userImageLocation = User.GetImagePath(EventCreator.Value, true, context);

            for (int i = ApprovalsIds.Length - 1; i>=0; i--)
            {
                string currentApprovalBody = template;

                _currentApprovalID = ApprovalsIds[i];
                var approvalDetails = (from ap in context.Approvals
                                       let folder = (from f in context.Folders
                                                     where f.ID == (ap.Folders.Any() ? (int?)ap.Folders.FirstOrDefault().ID : null)
                                                     select f).FirstOrDefault()
                                       join j in context.Jobs on ap.Job equals j.ID
                                       where ap.ID == _currentApprovalID
                                       select new
                                            {
                                                ApprovalType = ap.ApprovalType.Key,
                                                Account = ap.Job1.Account,
                                                ApprovalName = ap.Job1.Title,
                                                ApprovalFolderName = folder.Name
                                            }
                                        ).FirstOrDefault();

                if (approvalDetails != null)
                {
                    currentApprovalBody = SetHeader(currentApprovalBody, context);
                    currentApprovalBody = currentApprovalBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                    currentApprovalBody = currentApprovalBody.Replace("<$is_visible_thumbnail$>", "none !important;");

                    if (String.IsNullOrEmpty(approvalDetails.ApprovalFolderName))
                    {
                        currentApprovalBody = string.Format(currentApprovalBody, userImageLocation, creatorFullName,
                            approvalDetails.ApprovalName);
                    }
                    else
                    {
                        currentApprovalBody = string.Format(currentApprovalBody, userImageLocation, creatorFullName,
                            approvalDetails.ApprovalName, approvalDetails.ApprovalFolderName);
                    }

                    string approvalThumbLocation = "";
                    if (approvalDetails.ApprovalType == (int)Approval.ApprovalTypeEnum.Zip)
                    {
                        approvalThumbLocation = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + Account.GetDomain(approvalDetails.Account, context) + "/content/img/html.gif";
                        currentApprovalBody = currentApprovalBody.Replace("<$approval_thumbnail$>", approvalThumbLocation + "\" style= \"width:100%");
                    }
                    else
                    {
                        approvalThumbLocation = approvalDetails.ApprovalType == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(_currentApprovalID, Account.GetDomain(approvalDetails.Account, context), context)
                                                                                                                        : Approval.GetThumbnailImagePath(_currentApprovalID, context);
                        currentApprovalBody = currentApprovalBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
                    }
                    emailBody += currentApprovalBody;
                }
                else
                {
                    //current approval has been deleted exclude it from email
                    ApprovalsIds = ApprovalsIds.Where(val => val != _currentApprovalID).ToArray();
                }
            }

            return emailBody;
        }

        #endregion
		
		#region Methods

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForMultipleFiles(Resources.lblNotification_ApprovalDeletedTitle, ApprovalsIds, context);
        }

        #endregion
    }
}
