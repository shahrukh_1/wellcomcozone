﻿using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using System;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalWasUpdated : ApprovalNotification
    {
        #region Properties
        #endregion

        #region Constructor

        public ApprovalWasUpdated()
            :base(NotificationType.ApprovalWasUpdated)
        {
        }
        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationApprovalUpdatedHeader);
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            var approvalName = string.Empty;

            if (ApprovalsIds.Count() > 0)
            {
                var approvalId = ApprovalsIds[0];
                approvalName = (from a in context.Approvals
                                where a.ID == approvalId
                                select a.Job1.Title).FirstOrDefault();
            }
            return string.Format(emailBody, userImageLocation, userName, approvalName);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForMultipleFiles(Resources.lblNotification_ApprovalUpdatedTitle, ApprovalsIds, context);
        }

        #endregion
    }
}
