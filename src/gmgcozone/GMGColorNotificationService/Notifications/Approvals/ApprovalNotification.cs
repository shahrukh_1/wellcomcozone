﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using System.Text;
using System.IO;
using System.Collections.Generic;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class ApprovalNotification :  Notification
    {
        #region Private
        
        protected int _currentApprovalID = -1;

        #endregion

        #region Properties

        public string OptionalMessage { get; set; }

        public int? ExternalCreator { get; set; }

        public int? ExternalRecipient { get; set; }

        public int[] ApprovalsIds { get; set; }
        
        #endregion

        #region Ctor

        protected ApprovalNotification(NotificationType notificationType)
        {
            NotificationType = notificationType;
        }
        #endregion

        #region Public Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailSubject = GetNotificationSubject(context);
            email.subject = emailSubject;   

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = string.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (  from u in context.Users
                             where u.ID == EventCreator.Value
                             select new
                             {
                                 FullName = u.GivenName + " " + u.FamilyName,
                                 UserID = u.ID
                             }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            for (int i = ApprovalsIds.Length - 1; i>=0; i--)
            {
                string body = template;

                body = SetHeader(body, context);
                body = body.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                body = GetRoleBasedNotificationApprovalTemplate(ApprovalsIds[i], body, OptionalMessage, context);

                // approval was deleted or the recipient is not a collaboratore anymore
                if (!String.IsNullOrEmpty(body))
                {
                    body = FormatEmailBody(body, userImageLocation, userName, context);
                    emailBody += body;
                }
                else
                {
                    //current approval has been deleted exclude it from email
                    ApprovalsIds = ApprovalsIds.Where(val => val != ApprovalsIds[i]).ToArray();
                }
            }

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        protected int? ApprovalFolderId(GMGColorContext context)
        {
            if (ApprovalsIds.Count() > 0)
            {
                return Approval.GetFolder(_currentApprovalID, context);
            }

            return null;
        }

        /// <summary>
        /// Returns the email template based on user type, internal or external
        /// </summary>
        /// <param name="approval"></param>
        /// <param name="emailBody"></param>
        /// <param name="notificationBody"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetRoleBasedNotificationApprovalTemplate(int approval, string emailBody, string notificationBody, GMGColorContext context)
        {
            User objRecipientUser = null;
            ExternalCollaborator objRecipientExUser = null;
            SharedApproval objSharedApproval = null;
            var approvalCollaboratorRole = ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;

            bool showViewButton = true;

            bool isExternalUser = ExternalRecipient.GetValueOrDefault() > 0;

            Approval objApproval = GetApprovalById(approval, context);

            //approval has been deleted from db
            if (objApproval == null)
            {
                return String.Empty;
            }
            bool showDownloadButton = objApproval.AllowDownloadOriginal;

            if (isExternalUser)
            {
                objRecipientExUser = (from ec in context.ExternalCollaborators
                                        where ec.ID == ExternalRecipient
                                        select ec).FirstOrDefault();

                // ignore notification email for the external recipient that gain access to the current approval 
                // and after email was created he has lost the access
                if (objRecipientExUser == null)
                {
                   return string.Empty;
                }

                objSharedApproval = (from sa in context.SharedApprovals
                                        where
                                            sa.Approval == approval &&
                                            sa.ExternalCollaborator == ExternalRecipient &&
                                            sa.Phase == objApproval.CurrentPhase
                                            
                                        select sa).FirstOrDefault();

                if (objSharedApproval != null)
                {
                    showViewButton = objSharedApproval.IsSharedURL;
                    showDownloadButton = objSharedApproval.IsSharedDownloadURL;
                    approvalCollaboratorRole = ApprovalCollaboratorRole.GetRole(objSharedApproval.ApprovalCollaboratorRole, context);
                }
            }
            else
            {
                objRecipientUser = User.GetObject(InternalRecipient.GetValueOrDefault(), context);

                ApprovalCollaborator objApprovalCollaborator = objApproval.ApprovalCollaborators.Where(o => o.Collaborator == objRecipientUser.ID).Take(1).SingleOrDefault();

                // ignore notification email for the recipient that gain access to the current approval 
                // and after email was created he has lost the access
                if (objApprovalCollaborator == null)
                {
                    return string.Empty;
                }
                approvalCollaboratorRole = ApprovalCollaboratorRole.GetRole(objApprovalCollaborator.ApprovalCollaboratorRole, context);
            }

            string approvalName = objApproval.Job1.Title + ((objApproval.Version > 1) ? " v" + objApproval.Version.ToString() : "");
            string approvalThumbLocation = "";


            if (objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip)
            {
                approvalThumbLocation = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + Account.GetDomain(objApproval.Job1.Account, context) + "/content/img/html.gif";
            }
            else
            {
                approvalThumbLocation = objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(objApproval.ID, Account.GetDomain(objApproval.Job1.Account, context), context)
                                                                                                                               : Approval.GetThumbnailImagePath(objApproval.ID, context);
            }

            string approvalFileSize = GMGColorFormatData.GetFileSizeString(objApproval.Size);
            string approvalDownloadPath = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + (objApproval.Job1.Account1.IsCustomDomainActive ? objApproval.Job1.Account1.CustomDomain : objApproval.Job1.Account1.Domain) + "/Download/DownloadApproval?adhoc=0&exgousg=" + ((isExternalUser) ? objRecipientExUser.Guid : objRecipientUser.Guid) + "&apg=" + objApproval.Guid;

            string approvalHtmlViewUrl = Approval.GetApprovalHtml5ViewURL(objApproval, objRecipientUser, objRecipientExUser, objSharedApproval, isExternalUser, 0);

            switch (approvalCollaboratorRole)
            {
                case ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly:
                    emailBody = emailBody.Replace("<$notificationHeader$>", Resources.notificationApprovalSharedReadOnlyHeader);
                    emailBody = emailBody.Replace("<$pharagraphBody1$>", Resources.pharagraphShareFileReadOnlyBody1);
                    emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);
                    break;
                case ApprovalCollaboratorRole.ApprovalRoleName.Reviewer:
                    emailBody = emailBody.Replace("<$notificationHeader$>", Resources.notificationApprovalSharedReviewerHeader);
                    emailBody = emailBody.Replace("<$pharagraphBody1$>", Resources.pharagraphShareFileReviewerBody1);
                    emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);
                    break;
                default:
                    emailBody = emailBody.Replace("<$notificationHeader$>", Resources.notificationApprovalSharedReadOnlyHeader);
                    emailBody = emailBody.Replace("<$pharagraphBody1$>", Resources.pharagraphShareFileApproverAndReviewerBody1);

                    if (objApproval.PrimaryDecisionMaker != null && objApproval.PrimaryDecisionMaker > 0)
                        emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Format(Resources.pharagraphShareFileApproverAndReviewerBody2, objApproval.GetPrimaryDecisionMakerFullName(context)));
                    else
                        emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);
                    break;
            }

            if(objApproval.Deadline != null)
            {
            DateTime Deadline = (DateTime)objApproval.Deadline;
            DateTime DeadlineDateTime =  GMGColorFormatData.GetUserTimeFromUTC(Deadline, objApproval.Job1.Account1.TimeZone);

            string sharedApprovalDueDate = GMGColorFormatData.GetFormattedDate(DeadlineDateTime, objApproval.Job1.Account1, context);
            string sharedApprovalDueTime = GMGColorFormatData.GetFormattedTime(DeadlineDateTime, objApproval.Job1.Account1.TimeFormat);

            sharedApprovalDueDate = sharedApprovalDueDate + " " + sharedApprovalDueTime;

            if (objApproval.Deadline > DateTime.MinValue)
            {
                emailBody = emailBody.Replace("<$lblDue$>", String.Format("{0}:", Resources.lblDue));
                emailBody = emailBody.Replace("<$dueDate$>", sharedApprovalDueDate);
            }

            }
            else
            {
                emailBody = emailBody.Replace("<$lblDue$>", string.Empty);
                emailBody = emailBody.Replace("<$dueDate$>", string.Empty);
            }

            // Adding Phase Deadline 
            var PhaseDeadline = (from ajp in context.ApprovalJobPhases
                                 join a in context.Approvals on ajp.ID equals a.CurrentPhase
                                 where
                                     a.ID == objApproval.ID
                                 select ajp.Deadline).FirstOrDefault();

            if (PhaseDeadline !=null)
            {

            DateTime PhaseDeadlineDue = (DateTime)PhaseDeadline;
            
            DateTime UserTimeZoneDateTime = GMGColorFormatData.GetUserTimeFromUTC(PhaseDeadlineDue, objApproval.Job1.Account1.TimeZone);
            string sharedApprovalPhaseDueDate = GMGColorFormatData.GetFormattedDate(UserTimeZoneDateTime, objApproval.Job1.Account1, context);
            string sharedApprovalPhaseDueTime = GMGColorFormatData.GetFormattedTime(UserTimeZoneDateTime, objApproval.Job1.Account1.TimeFormat);
            
            sharedApprovalPhaseDueDate = sharedApprovalPhaseDueDate + " " + sharedApprovalPhaseDueTime;

            if (PhaseDeadline > DateTime.MinValue)
            {
                emailBody = emailBody.Replace("<$lblPhaseDeadline$>", String.Format("{0}:", Resources.lblPhaseDeadline));
                emailBody = emailBody.Replace("<$PhasedueDate$>", sharedApprovalPhaseDueDate);
            }
            
            }
            else
            {
                emailBody = emailBody.Replace("<$lblPhaseDeadline$>", "<br/>");
                emailBody = emailBody.Replace("<$PhasedueDate$>", String.Empty);
            }

            if (objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip)
            {
                string htmlstyle = "\" style= \"width:100%";
                emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation + htmlstyle);
            }
            else
            {
                emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
            }
                
            emailBody = emailBody.Replace("<$filename$>", approvalName);
            emailBody = emailBody.Replace("<$filesize$>", approvalFileSize);

            if (showViewButton)
            {
                emailBody = emailBody.Replace("<$lblView$>", Resources.btnView);
                emailBody = emailBody.Replace("<$approvalViewURL$>", approvalHtmlViewUrl);
                emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#33BBEC");  
            }
            else
            {
                emailBody = emailBody.Replace("<$is_visible_view_button$>", "none !important;");
                emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#ffffff");  
            }

            if (showDownloadButton)
            {
                emailBody = emailBody.Replace("<$lblDownload$>", Resources.btnDownload);
                emailBody = emailBody.Replace("<$approvalDownloadURL$>", approvalDownloadPath);
                emailBody = emailBody.Replace("<$bgColorOnDownloadButton$>", "#ED1556");  
            }
            else
            {
                emailBody = emailBody.Replace("<$is_visible_download_button$>", "none !important;");
                emailBody = emailBody.Replace("<$bgColorOnDownloadButton$>", "#ffffff");  
            }

            if (notificationBody != null)
            {
                notificationBody = notificationBody.Replace("\n", "<br>");
            }
            emailBody = string.IsNullOrEmpty(notificationBody) ? emailBody.Replace("<$is_visible_message$>", "none !important;") : emailBody.Replace("<$message$>", notificationBody);

            return emailBody;
        }

        public override string GetLocale( GMGColorContext context)
        {
            string localeName = String.Empty;

            if (InternalRecipient != null)
            {
                localeName = base.GetLocale(context);
            }
            else if (ExternalRecipient != null)
            {
                 localeName = (from e in context.ExternalCollaborators 
                                join l in context.Locales on e.Locale equals l.ID
                                where e.ID == ExternalRecipient
                                select l.Name).FirstOrDefault();
            }

            return localeName;
        }

        #endregion

        #region Abstract Methods

        protected abstract string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context);

        #endregion
    }
}
