﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalOverdue : ApprovalNotification
    {
        #region Properties

        public int? PhaseId { get; set; }

        #endregion

        #region Ctors

        public ApprovalOverdue(): base(NotificationType.ApprovalOverdue)
        {
            
        }

        #endregion

        #region Methods
        
        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailSubject = GetNotificationSubject(context);
            email.subject = emailSubject;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            var notificationType = PhaseId == null
                 ? NotificationType.ApprovalOverdue
                 : NotificationType.PhaseOverdue;

            string emailTitle = EmailUtils.GetEmailTitle(notificationType);
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = string.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            for (int i = ApprovalsIds.Length - 1; i >= 0; i--)
            {
                string body = template;

                body = SetHeader(body, context);
                body = body.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                body = GetNotificationApprovalTemplate(ApprovalsIds[i], body, OptionalMessage, context);

                // approval was deleted or the recipient is not a collaboratore anymore
                if (!String.IsNullOrEmpty(body))
                {
                    body = FormatEmailBody(body, userImageLocation, userName, context);
                    emailBody += body;
                }
                else
                {
                    //current approval has been deleted exclude it from email
                    ApprovalsIds = ApprovalsIds.Where(val => val != ApprovalsIds[i]).ToArray();
                }
            }

            return emailBody;
        }

        /// <summary>
        /// Returns the email template 
        /// </summary>
        /// <param name="approval"></param>
        /// <param name="emailBody"></param>
        /// <param name="notificationBody"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetNotificationApprovalTemplate(int approval, string emailBody, string notificationBody, GMGColorContext context)
        {
            User objRecipientUser = null;
            ExternalCollaborator objRecipientExUser = null;
            SharedApproval objSharedApproval = null;

            Approval objApproval = (from a in context.Approvals
                                    where a.ID == approval
                                    select a).FirstOrDefault();

            //approval has been deleted from db
            if (objApproval == null)
            {
                return String.Empty;
            }
            bool showDownloadButton = objApproval.AllowDownloadOriginal;

            bool isExternalUser = ExternalRecipient.GetValueOrDefault() > 0;
            if (isExternalUser)
            {
                objRecipientExUser = (from ec in context.ExternalCollaborators
                                      where ec.ID == ExternalRecipient
                                      select ec).FirstOrDefault();

                // ignore notification email for the external recipient that gain access to the current approval 
                // and after email was created he has lost the access
                if (objRecipientExUser == null)
                {
                    return string.Empty;
                }

                objSharedApproval = (from sa in context.SharedApprovals
                                     where
                                         sa.Approval == approval &&
                                         sa.ExternalCollaborator == ExternalRecipient
                                     select sa).FirstOrDefault();
            }
            else
            {
                objRecipientUser = User.GetObject(InternalRecipient.GetValueOrDefault(), context);

                ApprovalCollaborator objApprovalCollaborator = objApproval.ApprovalCollaborators.Where(o => o.Collaborator == objRecipientUser.ID).Take(1).SingleOrDefault();

                // ignore notification email for the recipient that gain access to the current approval 
                // and after email was created he has lost the access
                if (objApprovalCollaborator == null)
                {
                    return string.Empty;
                }
            }

            string approvalName = objApproval.Job1.Title + ((objApproval.Version > 1) ? " v" + objApproval.Version.ToString() : "");

            string approvalThumbLocation = "";
            if (objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip)
            {
                approvalThumbLocation = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + Account.GetDomain(objApproval.Job1.Account, context) + "/content/img/html.gif";
            }
            else
            {
                approvalThumbLocation = objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(objApproval.ID, Account.GetDomain(objApproval.Job1.Account, context), context)
                                                                                                                               : Approval.GetThumbnailImagePath(objApproval.ID, context);
            }
            string approvalFileSize = GMGColorFormatData.GetFileSizeString(objApproval.Size);
            
            string approvalDownloadPath = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + (objApproval.Job1.Account1.IsCustomDomainActive ? objApproval.Job1.Account1.CustomDomain : objApproval.Job1.Account1.Domain) + "/Download/DownloadApproval?adhoc=0&exgousg=" + ((isExternalUser) ? objRecipientExUser.Guid : objRecipientUser.Guid) + "&apg=" + objApproval.Guid;
            
            string approvalHtmlViewUrl = Approval.GetApprovalHtml5ViewURL(objApproval, objRecipientUser, objRecipientExUser, objSharedApproval, isExternalUser, 0);
            
            emailBody = emailBody.Replace("<$pharagraphBody1$>", string.Empty);
            
            emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);

            emailBody = emailBody.Replace("<$lblDue$>", "<br/>");
            emailBody = emailBody.Replace("<$dueDate$>", string.Empty);

            emailBody = emailBody.Replace("<$approval_thumbnail$>", (objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip ? approvalThumbLocation + "\" style= \"width:100%" : approvalThumbLocation));
            emailBody = emailBody.Replace("<$filename$>", approvalName);
            emailBody = emailBody.Replace("<$filesize$>", approvalFileSize);

            emailBody = emailBody.Replace("<$lblView$>", Resources.btnView);
            emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#33BBEC");
            emailBody = emailBody.Replace("<$approvalViewURL$>", approvalHtmlViewUrl);


            if (showDownloadButton)
            {
                emailBody = emailBody.Replace("<$lblDownload$>", Resources.btnDownload);
                emailBody = emailBody.Replace("<$approvalDownloadURL$>", approvalDownloadPath);
                emailBody = emailBody.Replace("<$bgColorOnDownloadButton$>", "#ED1556"); 
            }
            else
            {
                emailBody = emailBody.Replace("<$is_visible_download_button$>", "none !important;");
                emailBody = emailBody.Replace("<$bgColorOnDownloadButton$>", "#ffffff");  
            }

            emailBody = string.IsNullOrEmpty(notificationBody) ? emailBody.Replace("<$is_visible_message$>", "none !important;") : emailBody.Replace("<$message$>", notificationBody);

            return emailBody;
        }


        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return PhaseId != null ? template.Replace("<$notificationHeader$>", Resources.notificationPhaseOverdueHeader) : template.Replace("<$notificationHeader$>", Resources.notificationApprovalOverdueHeader);
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            if (PhaseId != null)
            {
                
                var approvalJobPhase = context.ApprovalJobPhases.FirstOrDefault(ajp => ajp.ID == PhaseId);

                var approvalId = ApprovalsIds[0];
                var approval = context.Approvals.FirstOrDefault(a => a.ID == approvalId);

                return string.Format(emailBody, userImageLocation, userName, approvalJobPhase?.Name, approval?.FileName);
            }
            return string.Format(emailBody, userImageLocation, userName, Resources.lblYou);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return PhaseId != null ? GetNotificationSubjectOnlyFileText(Resources.lblNotification_PhaseOverdue, ApprovalsIds[0], context) : GetNotificationSubjectOnlyFileText(Resources.lblNotification_ApprovalOverdue, ApprovalsIds[0], context);
        }

        #endregion
    }   
}
