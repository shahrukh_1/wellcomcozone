﻿using GMGColor.Resources;
using GMGColorDAL;
using System;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalWasShared : ApprovalNotification
    {
        #region Ctors

        public ApprovalWasShared()
            : base(NotificationType.ApprovalWasShared)
        {
        }
        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template;
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, Resources.lblYou);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForMultipleFiles(Resources.lblNotification_ApprovalWasShared, ApprovalsIds, context);
        }

        #endregion
    }
}
