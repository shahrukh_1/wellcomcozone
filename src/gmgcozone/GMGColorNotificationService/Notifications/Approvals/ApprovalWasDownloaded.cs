﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL.Common;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalWasDownloaded : ApprovalNotification
    {
        #region Properties
        #endregion

        #region Constructor

        public ApprovalWasDownloaded()
            :base(NotificationType.ApprovalWasDownloaded)
        {
        }
        #endregion

        #region Override Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = ApprovalFolderId(context).HasValue ? Resources.notificationApprovalDownloadedInFolderHeader : Resources.notificationApprovalDownloadedHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;

            string creatorFullName, userImageLocation;

            if (EventCreator != null)
            {
                 creatorFullName =
                    (from u in context.Users
                     where u.ID == EventCreator
                     select u.GivenName + " " + u.FamilyName).FirstOrDefault();

                 userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            }
            else
            {
                creatorFullName = (from exc in context.ExternalCollaborators
                                   where exc.ID == ExternalCreator
                                   select exc.GivenName + " " + exc.FamilyName).FirstOrDefault();

                int approvalId = ApprovalsIds[0];
                string accountDomain = (from a in context.Accounts
                                        join j in context.Jobs on a.ID equals j.Account
                                        join app in context.Approvals on j.ID equals app.Job
                                        where app.ID == approvalId
                                        select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).FirstOrDefault();

                userImageLocation = User.GetDefaultImagePath(accountDomain);
            }


            for(int i = ApprovalsIds.Length - 1; i>=0; i--)
            {
                string currentApprovalBody = template;

                _currentApprovalID = ApprovalsIds[i];

                var approvalDetails = (from ap in context.Approvals
                                       let folder = (from f in context.Folders
                                                     where f.ID == (ap.Folders.Any() ? (int?)ap.Folders.FirstOrDefault().ID : null)
                                                     select f).FirstOrDefault()
                                       join j in context.Jobs on ap.Job equals j.ID
                                       where ap.ID == _currentApprovalID
                                       select new
                                       {
                                           ApprovalName = ap.Job1.Title,
                                           ApprovalFolderName = folder != null ? folder.Name : ""
                                       }
                                        ).FirstOrDefault();

                if (approvalDetails != null)
                {
                    currentApprovalBody = SetHeader(currentApprovalBody, context);
                    currentApprovalBody = currentApprovalBody.Replace("<$timestamp$>",
                        GMGColorFormatData.GetTimeDifference(CreatedDate));
                    currentApprovalBody = currentApprovalBody.Replace("<$is_visible_thumbnail$>", "none !important;");

                    if (approvalDetails.ApprovalFolderName == String.Empty)
                    {
                        currentApprovalBody = string.Format(currentApprovalBody, userImageLocation, creatorFullName,
                            approvalDetails.ApprovalName);
                    }
                    else
                    {
                        currentApprovalBody = string.Format(currentApprovalBody, userImageLocation, creatorFullName,
                            approvalDetails.ApprovalName, approvalDetails.ApprovalFolderName);
                    }

                    emailBody += currentApprovalBody;
                }
                else
                {
                    //current approval has been deleted exclude it from email
                    ApprovalsIds = ApprovalsIds.Where(val => val != _currentApprovalID).ToArray();
                }
            }

            return emailBody;
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForMultipleFiles(Resources.lblNotification_ApprovalDownloadedTitle, ApprovalsIds, context);
        }

        #endregion
    }
}
