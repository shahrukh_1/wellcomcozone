﻿using GMGColor.Resources;
using GMGColorDAL;
using System.Linq;
using System.Text;
using System.IO;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewApprovalAdded : ApprovalNotification
    {
        #region Ctor

        public NewApprovalAdded()
            : base(NotificationType.NewApprovalAdded)
        {

        }
        #endregion
        
        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template;
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, Resources.lblYou);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForMultipleFiles(Resources.lblNotification_NewApprovalAddedTitle, ApprovalsIds, context);
        }

        #endregion
    }
}
