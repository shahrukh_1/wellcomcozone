﻿using GMGColor.Resources;
using GMGColorDAL;
using System;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewVersionWasCreated: ApprovalNotification
    {
        #region Ctors

        public NewVersionWasCreated()
            : base(NotificationType.NewVersionWasCreated)
        {
        }

        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template;
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, Resources.lblYou);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForPenultimateVersion(Resources.lblNotification_NewVersionCreatedTitle, ApprovalsIds[0], context);
        }

        #endregion
    }
}
