﻿using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class InvitedUserFileSubmitted : DeliverNotification
    {
        #region Ctors

        public InvitedUserFileSubmitted()
            : base(NotificationType.InvitedUserFileSubmitted)
        {
        }

        #endregion

        #region Override Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationInvitedUserFileSubmitted);
        }

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, string fileName, string jobDetailsLink, string jobGuid, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, fileName, jobDetailsLink);
        }

        #endregion
    }
}
