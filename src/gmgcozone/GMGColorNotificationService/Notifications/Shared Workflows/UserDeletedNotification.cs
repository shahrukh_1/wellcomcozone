﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class UserDeletedNotification : Notification
    {
        #region Properties

        public int DeletedUserID { get; set; }

        #endregion

        #region Ctors

        protected UserDeletedNotification(NotificationType notificationType)
        {
            NotificationType = notificationType;
        }

        #endregion

        #region Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            var loggedUser = (from u in context.Users
                              where u.ID == DeletedUserID
                              select new
                              {
                                  FullName = u.GivenName + " " + u.FamilyName,
                              }).FirstOrDefault();
     
            emailBody = SetHeader(emailBody, context);
            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
            emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
            emailBody = string.Format(emailBody, userImageLocation, userName, loggedUser != null? loggedUser.FullName : string.Empty);

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationDeliverUserDeleted);
        }

        #endregion
    }
}
