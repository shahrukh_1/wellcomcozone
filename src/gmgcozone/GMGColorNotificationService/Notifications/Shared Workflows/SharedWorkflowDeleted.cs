﻿using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class SharedWorkflowDeleted : SharedWorkflowNotification
    {
        #region Ctors

        public SharedWorkflowDeleted() 
            : base(NotificationType.SharedWorkflowDeleted)
        {
        }

        #endregion

        #region Methods

       
        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationSharedWorkflowDeleted);
        }

        #endregion

        #region Abstract Methods

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName)
        {
            return string.Format(emailBody, userImageLocation, userName, WorkflowName);
        }

        #endregion
    }
}
