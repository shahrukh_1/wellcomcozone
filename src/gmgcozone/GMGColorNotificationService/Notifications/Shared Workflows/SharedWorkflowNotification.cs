﻿using System;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class SharedWorkflowNotification : Notification
    {
        #region Properties

        public string WorkflowName { get; set; }

        #endregion

        #region Ctors

        protected SharedWorkflowNotification(NotificationType notificationType)
        {
            NotificationType = notificationType;
        }

        #endregion

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            emailBody = SetHeader(emailBody, context);
            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
            emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
            emailBody = FormatEmailBody(emailBody, userImageLocation, userName);

            return emailBody;
        }
      

        #region Abstract Methods

        protected abstract string FormatEmailBody(string emailBody, string userImageLocation, string userName);

        #endregion
    }
}
