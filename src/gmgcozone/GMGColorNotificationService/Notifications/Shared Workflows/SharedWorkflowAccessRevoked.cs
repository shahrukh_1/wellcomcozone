﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class SharedWorkflowAccessRevoked : SharedWorkflowNotification
    {
        #region Ctors

        public SharedWorkflowAccessRevoked()
            : base(NotificationType.SharedWorkflowAccessRevoked)
        {
        }

        #endregion
        
        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationInvitedUserAccessRevoked);
        }

        #endregion

        #region Abstract Methods

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName)
        {
            return string.Format(emailBody, userImageLocation, WorkflowName, userName);
        }

        #endregion
    }
}
