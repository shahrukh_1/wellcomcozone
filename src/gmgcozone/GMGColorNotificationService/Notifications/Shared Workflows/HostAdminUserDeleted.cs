﻿using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class HostAdminUserDeleted : UserDeletedNotification
    {
        #region Ctors

        public HostAdminUserDeleted()
            : base(NotificationType.HostAdminUserDeleted)
        {
        }

        #endregion
    }
}
