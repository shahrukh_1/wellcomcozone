﻿using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class InvitedUserDeleted : UserDeletedNotification
    {
        #region Ctors

        public InvitedUserDeleted() 
            :base(NotificationType.InvitedUserDeleted)
        {
        }

        #endregion
    }
}
