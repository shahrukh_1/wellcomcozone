﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ResetPassword : Notification
    {
        #region Properties

        public string NewPassword { get; set; }

        #endregion

        #region Constructor

        public ResetPassword()
        {
            NotificationType = NotificationType.ResetPassword;
        }
        #endregion

        #region Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.Value, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            email.subject = emailTitle;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, emailBodyTemplate);
            emailOuterTemplate = CreateBody(emailOuterTemplate, context, false);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string mailBody = template;

            string hostUrl = (from u in context.Users
                              join ac in context.Accounts on u.Account equals ac.ID
                              where u.ID == InternalRecipient
                              select ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain).FirstOrDefault();

            mailBody = mailBody.Replace("<$pharagraphLostPassword$>", Resources.pharagraphLostPassword);
            mailBody = mailBody.Replace("<$lblYourNewPasswordIs$>", Resources.lblYourNewPasswordIs);
            mailBody = mailBody.Replace("<$pharagraphLostPasswordBody1$>", Resources.pharagraphLostPasswordBody1);
            mailBody = mailBody.Replace("<$pharagraphLostPasswordBody2$>", Resources.pharagraphLostPasswordBody2);
            mailBody = mailBody.Replace("<$password$>", NewPassword);
            mailBody = mailBody.Replace("<$welcomeURL$>", GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + hostUrl + "");
            mailBody = mailBody.Replace("<$accountURL$>", GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + hostUrl + "");

            return mailBody;
        }

        #endregion
    }
}
