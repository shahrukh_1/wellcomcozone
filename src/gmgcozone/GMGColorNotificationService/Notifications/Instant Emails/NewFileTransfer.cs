﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;
using GMGColorDAL.Common;


namespace GMGColorNotificationService
{
    public class NewFileTransfer : ApprovalNotification
    {
        #region Private

        //protected int _currentApprovalID = -1;

        #endregion

        #region Properties

        //public string OptionalMessage { get; set; }

        //public int? ExternalCreator { get; set; }

        //public int? ExternalRecipient { get; set; }

        //public int ApprovalId { get; set; }

        #endregion

        #region Constructors

        public NewFileTransfer() : base(NotificationType.NewFileTransferAdded)
        {
        }

        #endregion

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, Resources.lblYou);
        }

        


        #region Public Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            email.subject = emailTitle;
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = string.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

           
                string body = template;

                body = SetHeader(body, context);
                body = body.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                body = GetRoleBasedNotificationApprovalTemplate(ApprovalsIds[0], body, OptionalMessage, context);

                // approval was deleted or the recipient is not a collaboratore anymore
                if (!String.IsNullOrEmpty(body))
                {
                    body = FormatEmailBody(body, userImageLocation, userName, context);
                    emailBody += body;
                }
                
           

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template;
        }

       

        /// <summary>
        /// Returns the email template based on user type, internal or external
        /// </summary>
        /// <param name="approval"></param>
        /// <param name="emailBody"></param>
        /// <param name="notificationBody"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetRoleBasedNotificationApprovalTemplate(int fileTransfer, string emailBody, string notificationBody, GMGColorContext context)
        {

            User objRecipientUser = null;
            ExternalCollaborator objRecipientExUser = null;
          
            FileTransferUserExternal objSharedApproval = null;
            

            //bool showViewButton = false;

            bool isExternalUser = ExternalRecipient.GetValueOrDefault() > 0;

            FileTransfer objApproval = GetFileTransferById(fileTransfer, context);

            //approval has been deleted from db
            if (objApproval == null)
            {
                return String.Empty;
            }
            //bool showDownloadButton = true;

            if (isExternalUser)
            {
                objRecipientExUser = (from ec in context.ExternalCollaborators
                                      where ec.ID == ExternalRecipient
                                      select ec).FirstOrDefault();



                // ignore notification email for the external recipient that gain access to the current approval 
                // and after email was created he has lost the access
                if (objRecipientExUser == null)
                {
                    return string.Empty;
                }



                objSharedApproval = (from sa in context.FileTransferUserExternals
                                     where
                                         sa.FileTransferID == fileTransfer &&
                                         sa.UserExternalID == ExternalRecipient
                                     select sa).FirstOrDefault();

              
            }
            else
            {
                objRecipientUser = User.GetObject(InternalRecipient.GetValueOrDefault(), context);

                FileTransferUserInternal objApprovalCollaborator = objApproval.FileTransferUserInternals.Where(o => o.UserInternalID == objRecipientUser.ID).Take(1).SingleOrDefault();

                // ignore notification email for the recipient that gain access to the current approval 
                // and after email was created he has lost the access
                if (objApprovalCollaborator == null)
                {
                    return string.Empty;
                }
               
            }

            string approvalName = objApproval.FileName;
            //string approvalThumbLocation = objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(objApproval.ID, Account.GetDomain(objApproval.Job1.Account, context), context)
            //                                                                                                                    : Approval.GetThumbnailImagePath(objApproval.ID, context);

            //   string approvalDownloadPath = GMGColorConfiguration.AppConfiguration.ServerProtocol +
            //  "://" + (objApproval.Job1.Account1.IsCustomDomainActive ? objApproval.Job1.Account1.CustomDomain : objApproval.Job1.Account1.Domain) 
            //  + "/Download/DownloadApproval?adhoc=0&exgousg=" + ((isExternalUser) ? objRecipientExUser.Guid : objRecipientUser.Guid) 
            //  + "&apg=" + objApproval.Guid;

            int adhoc = 0;
            if (isExternalUser == true) adhoc = 1;
            emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");

            string approvalDownloadPath = GMGColorConfiguration.AppConfiguration.ServerProtocol +
                "://" + (objApproval.Account.IsCustomDomainActive ? objApproval.Account.CustomDomain : objApproval.Account.Domain) +
                "/Download/DownloadFileTransfer?adhoc="+adhoc+"&exgousg=" + ((isExternalUser) ? objRecipientExUser.Guid : objRecipientUser.Guid)
                + "&apg=" + objApproval.Guid;

            


            emailBody = emailBody.Replace("<$notificationHeader$>", Resources.notificationNewFileTransferHeader);
            emailBody = emailBody.Replace("<$pharagraphBody1$>", Resources.pharagraphFileTransferBody1);
            emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);
          
          


            string sharedApprovalDueDate = GMGColorFormatData.GetFormattedDate(objApproval.ExpirationDate, objApproval.Account, context);

            if (objApproval.ExpirationDate > DateTime.MinValue)
            {
                emailBody = emailBody.Replace("<$lblDue$>", String.Format("{0}:", Resources.lblFileTransferExpiresOn));
                emailBody = emailBody.Replace("<$dueDate$>", sharedApprovalDueDate);
            }
            else
            {
                emailBody = emailBody.Replace("<$lblDue$>", "<br/>");
            }

           // emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
            emailBody = emailBody.Replace("<$filename$>", approvalName);
           

           //do not show the view button
                emailBody = emailBody.Replace("<$is_visible_view_button$>", "none !important;");
                emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#ffffff");
            //----

            emailBody = emailBody.Replace("<$lblDownload$>", Resources.btnDownload);
            emailBody = emailBody.Replace("<$approvalDownloadURL$>", approvalDownloadPath);
            emailBody = emailBody.Replace("<$bgColorOnDownloadButton$>", "#ED1556");


            emailBody = string.IsNullOrEmpty(notificationBody) ? emailBody.Replace("<$is_visible_message$>", "none !important;") : emailBody.Replace("<$message$>", notificationBody);

            return emailBody;
        }

        public override string GetLocale(GMGColorContext context)
        {
            string localeName = String.Empty;

            if (InternalRecipient != null)
            {
                localeName = base.GetLocale(context);
            }
            else if (ExternalRecipient != null)
            {
                localeName = (from e in context.ExternalCollaborators
                              join l in context.Locales on e.Locale equals l.ID
                              where e.ID == ExternalRecipient
                              select l.Name).FirstOrDefault();
            }

            return localeName;
        }

        #endregion



       

    }
}
