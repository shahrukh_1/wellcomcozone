﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class BillingReport : Notification
    {
        #region Properties

        public string Subject { get; set; }

        public string ReportBody { get; set; }

        public string GrandTotal { get; set; }

        public string Message { get; set; }

        #endregion

        #region Constructors

        public BillingReport()
        {
            NotificationType = NotificationType.BillingReport;
        }

        #endregion

        #region Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.Value, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            email.subject = Subject;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, emailBodyTemplate);
            emailOuterTemplate = CreateBody(emailOuterTemplate, context, false);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string mailBody = template;
            var userInfo = (from u in context.Users
                              join ac in context.Accounts on u.Account equals ac.ID
                              where u.ID == InternalRecipient
                              select new 
                              {
                                  hostUrl = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                  ac.Name,
                                  u.EmailAddress
                              }).FirstOrDefault();

            string accountUrl = "<a href=" + GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + userInfo.hostUrl + "\">" + userInfo.hostUrl + "</a>";

            mailBody = mailBody.Replace("<$title$>", Subject);
            mailBody = mailBody.Replace("<$accountURL$>", accountUrl);
            mailBody = mailBody.Replace("<$message$>", Message);
            mailBody = mailBody.Replace("<$sitename$>", userInfo.Name);
            mailBody = mailBody.Replace("<$useremail$>", userInfo.EmailAddress);
            mailBody = mailBody.Replace("<$ReportDetails$>", ReportBody);
            mailBody = mailBody.Replace("<$GrandTotal$>", GrandTotal);

            mailBody = mailBody.Replace("<$lblGrandTotal$>", Resources.lblGrandTotal);

            return mailBody;
        }

        #endregion
    }
}
