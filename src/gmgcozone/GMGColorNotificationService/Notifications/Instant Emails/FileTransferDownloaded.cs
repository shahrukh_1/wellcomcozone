﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;
using GMGColorDAL.Common;


namespace GMGColorNotificationService
{
    public class FileTransferDownloaded : ApprovalNotification
    {
        #region Private

        #endregion

        #region Properties
        
        #endregion

        #region Constructors

        public FileTransferDownloaded() : base(NotificationType.FileTransferDownloaded)
        {
        }

        #endregion

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            return string.Format(emailBody, userImageLocation, userName, Resources.lblYou);
        }




        #region Public Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            email.subject = emailTitle;
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = string.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            if (EventCreator != null)
            {
                //internal user downloaded the file transfer
                var creator = (from u in context.Users
                               where u.ID == EventCreator.Value
                               select new
                               {
                                   FullName = u.GivenName + " " + u.FamilyName,
                                   UserID = u.ID
                               }).FirstOrDefault();

                if (creator != null)
                {
                    userName = creator.FullName;
                    userImageLocation = User.GetImagePath(creator.UserID, true, context);
                }
            }
            else
            {
                //external user downloaded the file transfer
                var externalCreator = (from u in context.ExternalCollaborators
                                       where u.ID == ExternalCreator.Value
                                       select new
                                       {
                                           FullName = u.GivenName + " " + u.FamilyName,
                                           UserID = u.ID
                                       }).FirstOrDefault();

               

                int approvalId = ApprovalsIds[0];
                string accountDomain = (from a in context.Accounts
                                        join j in context.ExternalCollaborators on a.ID equals j.Account
                                        join x in context.FileTransferUserExternals on approvalId equals x.FileTransferID
                                        where j.ID == x.UserExternalID
                                        select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).FirstOrDefault();

                
                if (externalCreator != null)
                {
                    userName = externalCreator.FullName;
                    userImageLocation = User.GetDefaultImagePath(accountDomain);
                }
            }

           

            string body = template;

            body = SetHeader(body, context);
            body = body.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
            body = GetRoleBasedNotificationApprovalTemplate(ApprovalsIds[0], body, string.Empty, context);

            // approval was deleted or the recipient is not a collaboratore anymore
            if (!String.IsNullOrEmpty(body))
            {
                body = FormatEmailBody(body, userImageLocation, userName, context);
                emailBody += body;
            }



            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template;
        }



        /// <summary>
        /// Returns the email template based on user type, internal or external
        /// </summary>
        /// <param name="approval"></param>
        /// <param name="emailBody"></param>
        /// <param name="notificationBody"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetRoleBasedNotificationApprovalTemplate(int fileTransfer, string emailBody, string notificationBody, GMGColorContext context)
        {

            User objRecipientUser = null;
            ExternalCollaborator objRecipientExUser = null;

            FileTransferUserExternal objSharedApproval = null;


            //bool showViewButton = false;

            bool isExternalUser = ExternalRecipient.GetValueOrDefault() > 0;

            FileTransfer objApproval = GetFileTransferById(fileTransfer, context);

            //approval has been deleted from db
            if (objApproval == null)
            {
                return String.Empty;
            }
           
                objRecipientUser = User.GetObject(InternalRecipient.GetValueOrDefault(), context);

                FileTransferUserInternal objApprovalCollaborator = objApproval.FileTransferUserInternals.Where(o => o.UserInternalID == objRecipientUser.ID).Take(1).SingleOrDefault();

            string approvalName = objApproval.FileName;
          
            emailBody = emailBody.Replace("<$notificationHeader$>", Resources.notificationDownloadedFileTransferHeader);
            emailBody = emailBody.Replace("<$pharagraphBody1$>", string.Empty);
            emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);


            //set the download date
            emailBody = emailBody.Replace("<$lblDue$>", String.Format("{0}", Resources.lblDownloadDate));

            if (EventCreator != null)
            {
                //internal user downloaded the file transfer
                var objCreatorUser = User.GetObject(EventCreator.GetValueOrDefault(), context);

                FileTransferUserInternal objInternalUser = objApproval.FileTransferUserInternals.Where(o => o.UserInternalID == objCreatorUser.ID).Take(1).SingleOrDefault();
                string internalUserDownloadDate = GMGColorFormatData.GetFormattedDate(objInternalUser.DownloadDate, objApproval.Account, context);
                emailBody = emailBody.Replace("<$dueDate$>", internalUserDownloadDate);


            }
            else
            {
                //external user downloaded the file transfer
                FileTransferUserExternal objApprovalCollaboratorExternal = objApproval.FileTransferUserExternals.Where(o => o.UserExternalID == ExternalCreator).Take(1).SingleOrDefault();
                var externalUserDownloadDate = GMGColorFormatData.GetFormattedDate(objApprovalCollaboratorExternal.DownloadDate, objApproval.Account, context);
                emailBody = emailBody.Replace("<$dueDate$>", externalUserDownloadDate);
            }

            emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");

            // emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
            emailBody = emailBody.Replace("<$filename$>", approvalName);


            //do not show the view button
            emailBody = emailBody.Replace("<$is_visible_view_button$>", "none !important;");
            emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#ffffff");
            //----

            //do not show the download button
            emailBody = emailBody.Replace("<$is_visible_download_button$>", "none !important;");
            emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#ffffff");
            //----
            
           


            emailBody = string.IsNullOrEmpty(notificationBody) ? emailBody.Replace("<$is_visible_message$>", "none !important;") : emailBody.Replace("<$message$>", notificationBody);

            return emailBody;
        }

        public override string GetLocale(GMGColorContext context)
        {
           
            string localeName = base.GetLocale(context);
            
            return localeName;
        }

        #endregion





    }
}

