﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ApprovalReminder : Notification
    {
        #region Properties

        public int? ExternalRecipient { get; set; }

        public int ApprovalId { get; set; }

        #endregion

        #region Constructors

        public ApprovalReminder()
        {
            NotificationType = NotificationType.ApprovalReminder;
        }

        #endregion

        #region Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            email.subject = emailTitle;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            emailBodyTemplate = CreateBody(emailBodyTemplate, context, false);
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBodyTemplate);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string mailBody = template;
            Approval objApproval = (from ap in context.Approvals
                where ap.ID == ApprovalId
                select ap).FirstOrDefault();

            User objRecipientUser = null;
            ExternalCollaborator objRecipientExUser = null;
            SharedApproval objSharedApproval = null;


            if (ExternalRecipient != null)
            {
                objRecipientExUser = (from exc in context.ExternalCollaborators
                                    where exc.ID == ExternalRecipient
                                    select exc).FirstOrDefault();

                objSharedApproval = (from sh in context.SharedApprovals
                                     where sh.ExternalCollaborator == ExternalRecipient && sh.Approval == ApprovalId
                                     select sh).FirstOrDefault();
            }
            else
            {
                objRecipientUser = (from exc in context.Users
                                    where exc.ID == InternalRecipient
                                    select exc).FirstOrDefault();

            }

            string approvalViewUrl = Approval.GetApprovalHtml5ViewURL(objApproval, objRecipientUser, objRecipientExUser, objSharedApproval, ExternalRecipient != null, 0);

            mailBody = mailBody.Replace("<$pharagraphReminderMessage$>", String.Format(Resources.notificationApprovalReminder, objApproval.Job1.Title, objApproval.Version));
            mailBody = mailBody.Replace("<$approvalURL$>", approvalViewUrl);
            mailBody = mailBody.Replace("<$btnView$>", Resources.btnView);

            return mailBody;
        }

        public override string GetLocale(GMGColorContext context)
        {
            string localeName = String.Empty;

            if (InternalRecipient != null)
            {
                localeName = base.GetLocale(context);
            }
            else if (ExternalRecipient != null)
            {
                localeName = (from e in context.ExternalCollaborators
                              join l in context.Locales on e.Locale equals l.ID
                              where e.ID == ExternalRecipient
                              select l.Name).FirstOrDefault();
            }

            return localeName;
        }

        #endregion
    }
}
