﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class UserWelcome : Notification
    {
        #region Properties

        public string EmailTitle { get; set; }

        #endregion

        #region Constructors

        public UserWelcome()
        {
            NotificationType = NotificationType.UserWelcome;
        }

        #endregion

        #region Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.Value, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string s = emailInfo.EmailDetails.AccountName.Substring(emailInfo.EmailDetails.AccountName.Length - 2);
            if (s == "'s")
            {
                emailInfo.EmailDetails.AccountName = emailInfo.EmailDetails.AccountName.Remove(emailInfo.EmailDetails.AccountName.Length - 2);
            }
            email.subject = String.Format(Resources.lblWelcomeTo, emailInfo.EmailDetails.AccountName);

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, emailBodyTemplate);
            emailOuterTemplate = CreateBody(emailOuterTemplate, context, false);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", String.Format(Resources.lblWelcomeTo, emailInfo.EmailDetails.AccountName));

            emailOuterTemplate = EmailUtils.PopulateFooterForWelcomeUser(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            var receiverInfo = (from us in context.Users
                join ac in context.Accounts on us.Account equals ac.ID
                where us.ID == InternalRecipient
                select new
                {
                    us.Guid,
                    us.ID,
                    AccountUrl = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain
                }).FirstOrDefault();

            string mailBody = template;
            string welcomeUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + receiverInfo.AccountUrl + "/Auth/Welcome?guid=" + receiverInfo.Guid + "&id=" + receiverInfo.ID + "";

            mailBody = mailBody.Replace("<$btnConfirmAccountAndLogin$>", Resources.btnConfirmAccountAndLogin);
            mailBody = mailBody.Replace("<$pharagraphWelcomeBody1$>", Resources.pharagraphWelcomeBody1);
            mailBody = mailBody.Replace("<$pharagraphWelcomeBody2$>", Resources.pharagraphWelcomeBody2);
            mailBody = mailBody.Replace("<$welcomeURL$>", welcomeUrl);

            return mailBody;
        }

        #endregion
    }
}
