﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewDeliverPairingCode : Notification
    {
        #region Properties

        public string PairingCode { get; set; }

        public bool IsNewCPInstance { get; set; }

        public string CPInstanceName { get; set; }

        public string ReceiverEmail { get; set; }

        #endregion

        #region Constructors

        public NewDeliverPairingCode()
        {
            NotificationType = NotificationType.DeliverNewPairingCode;
        }

        #endregion

        #region Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(EventCreator.Value, false, context);

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            email.subject = emailTitle;

            email.toEmail = ReceiverEmail;
            email.toName = ReceiverEmail;
            email.fromName = emailInfo.EmailDetails.FromName;
            email.fromEmail = emailInfo.EmailDetails.FromEmail;
            email.Account = emailInfo.EmailDetails.AccountId;

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, emailBodyTemplate);
            emailOuterTemplate = CreateBody(emailOuterTemplate, context, false);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateNewDeliverPairingCodeTemplate(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string mailBody = template;

            string hostUrl = (from u in context.Users
                join ac in context.Accounts on u.Account equals ac.ID
                where u.ID == EventCreator
                select ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain).FirstOrDefault();

            if (IsNewCPInstance)
            {
                mailBody = mailBody.Replace("<$pharagraphNewInstance$>",
                                            String.Format(Resources.notificationNewCPInstanceAdded, CPInstanceName,
                                                          PairingCode));
            }
            else
            {
                mailBody = mailBody.Replace("<$pharagraphNewInstance$>",
                                           String.Format(Resources.notificationNewCPInstanceReseted, CPInstanceName,
                                                         PairingCode));
            }

            mailBody = mailBody.Replace("<$accountURL$>", GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + hostUrl + "");

            return mailBody;
        }

        public override string GetLocale(GMGColorContext context)
        {
            string localeName = (from u in context.Users 
                                join l in context.Locales on u.Locale equals l.ID
                                where u.ID == EventCreator
                                select l.Name).FirstOrDefault();

            return localeName;
        }

        #endregion
    }
}
