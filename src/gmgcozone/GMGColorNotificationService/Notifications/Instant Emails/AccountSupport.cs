﻿using System;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class AccountSupport: Notification
    {
        #region Properties

        public string ReceiverName { get; set; }

        public string ToEmail { get; set; }

        public string FromName { get; set; }

        public string FromEmail { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public string[] Attachments { get; set; }

        #endregion

        #region Constructors

        public AccountSupport()
        {
            NotificationType = NotificationType.AccountSupport;
        }

        #endregion

        #region Methods
        
        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            Email email = new Email();
            email.fromEmail = FromEmail;
            email.fromName = FromName;
            email.toEmail = ToEmail;
            email.toName = ReceiverName;
            email.bodyHtml = Message;
            email.subject = Subject;
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            email.Attachments = string.Join(";", Attachments);
            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            throw new NotImplementedException();
        }

        public override string GetLocale( GMGColorContext context)
        {
            return "en-US";
        }

        #endregion
    }
}
