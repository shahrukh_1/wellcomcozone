﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class ShareWorkflowInvitation : Notification
    {
        #region Properties

        public string WorkflowName { get; set; }
        public string CompanyName { get; set; }
        public string LoggedUserName { get; set; }
        public string AcceptUrl { get; set; }
        public string RejectUrl { get; set; }

        #endregion

        #region Constructors

        public ShareWorkflowInvitation()
        {
            NotificationType = NotificationType.SharedWorkflowInvitation;
        }

        #endregion

        #region Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.Value, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            email.subject = emailTitle;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, emailBodyTemplate);
            emailOuterTemplate = CreateBody(emailOuterTemplate, context, false);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return String.Empty;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string mailBody = template;

            mailBody = mailBody.Replace("<$pharagraphShareInfo$>", String.Format(Resources.notificationParagraphShareWorkflow, LoggedUserName, CompanyName));
            mailBody = mailBody.Replace("<$workflowName$>", WorkflowName);
            mailBody = mailBody.Replace("<$pharagraphShareMessage$>", Resources.notificationParagraphShareWorkflowInfo);
            mailBody = mailBody.Replace("<$btnRejectShareUrl$>", Resources.lblReject);
            mailBody = mailBody.Replace("<$btnAcceptShareUrl$>", Resources.lblAccept);
            mailBody = mailBody.Replace("<$rejectShareUrl$>", RejectUrl);
            mailBody = mailBody.Replace("<$acceptShareUrl$>", AcceptUrl);

            return mailBody;
        }

        #endregion
    }
}
