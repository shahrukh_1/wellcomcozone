﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class PhaseWasDeactivated : WorkflowNotification
    {
        #region Constructors

        public PhaseWasDeactivated()
            : base(NotificationType.PhaseWasDeactivated)
        {
        }

        #endregion

        #region Override methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationPhaseWasDeactivatedHeader);
        }

        #endregion

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectOnlyFileText(Resources.lblNotification_PhaseWasDeactivated, ApprovalId, context);
        }
    }
}
