﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;
using GMGColor.Resources;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class WorkflowPhaseRerun : WorkflowNotification
    {
        #region Constructors

        public WorkflowPhaseRerun(): base(NotificationType.WorkflowPhaseRerun)
        {
        }

        #endregion

        #region Override methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationWorkflowPhaseRerunHeader);
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = string.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }
          
            string body = template;

            body = SetHeader(body, context);
            body = body.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));

            Approval approval = (from a in context.Approvals
                                    where a.ID == ApprovalId
                                    select a).FirstOrDefault();

            //approval has been deleted from db
            if (approval == null)
            {
                return String.Empty;
            }  

            string approvalName = approval.Job1.Title + ((approval.Version > 1) ? " v" + approval.Version.ToString() : "");
            body = GetNotificationApprovalTemplate(approval, body, approvalName, context);

            // approval was deleted or the recipient is not a collaboratore anymore
            if (!String.IsNullOrEmpty(body))
            {
                body = string.Format(body, userImageLocation, userName, approvalName, GetPhaseName(Phase, context));               
            }
            emailBody += body;
            return emailBody;
        }

        /// <summary>
        /// Returns the email template 
        /// </summary>
        /// <param name="approval"></param>
        /// <param name="emailBody"></param>
        /// <param name="notificationBody"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetNotificationApprovalTemplate(Approval approval, string emailBody, string approvalName, GMGColorContext context)
        {
            string approvalThumbLocation, approvalFileSize, approvalHtmlViewUrl;

            approvalThumbLocation = approval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ?
                Approval.GetVideoImagePath(approval.ID, Account.GetDomain(approval.Job1.Account, context), context) : Approval.GetThumbnailImagePath(approval.ID, context);
            approvalFileSize = GMGColorFormatData.GetFileSizeString(approval.Size);

            approvalHtmlViewUrl = GetApprovalURL(approval, context);

            emailBody = CustomizeEmailBody(emailBody, approvalName, approvalThumbLocation, approvalFileSize, approvalHtmlViewUrl);

            return emailBody;
        }

        private static string CustomizeEmailBody(string emailBody, string approvalName, string approvalThumbLocation, string approvalFileSize, string approvalHtmlViewUrl)
        {
            emailBody = emailBody.Replace("<$notificationHeader$>", Resources.notificationApprovalOverdueHeader);
            emailBody = emailBody.Replace("<$pharagraphBody1$>", string.Empty);

            emailBody = emailBody.Replace("<$pharagraphBody2$>", string.Empty);

            emailBody = emailBody.Replace("<$lblDue$>", "<br/>");
            emailBody = emailBody.Replace("<$dueDate$>", string.Empty);

            emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
            emailBody = emailBody.Replace("<$filename$>", approvalName);
            emailBody = emailBody.Replace("<$filesize$>", approvalFileSize);

            emailBody = emailBody.Replace("<$lblView$>", Resources.btnView);
            emailBody = emailBody.Replace("<$bgColorOnViewButton$>", "#33BBEC");
            emailBody = emailBody.Replace("<$approvalViewURL$>", approvalHtmlViewUrl);

            emailBody = emailBody.Replace("<$is_visible_download_button$>", "none !important;");
            emailBody = emailBody.Replace("<$is_visible_message$>", "none !important;");
            emailBody = emailBody.Replace("<$bgColorOnDownloadButton$>", "#ffffff");

            return emailBody;
        }

        private string GetApprovalURL(Approval approval, GMGColorContext context)
        {
            User user;
            ExternalCollaborator externalCollaborator;
            SharedApproval sharedApproval = new SharedApproval();
            string approvalHtmlViewUrl;

            user = User.GetObject(InternalRecipient.GetValueOrDefault(), context);
           
            bool isExternalCollaborator = ExternalRecipient.HasValue;
            externalCollaborator = isExternalCollaborator ? context.ExternalCollaborators.SingleOrDefault(ex => ex.ID == ExternalRecipient.Value) : null;

            if (isExternalCollaborator)
            {
                sharedApproval = (from sh in context.SharedApprovals
                                  where sh.Approval == approval.ID && sh.ExternalCollaborator == ExternalRecipient
                                  select sh).FirstOrDefault();
            }

            approvalHtmlViewUrl = Approval.GetApprovalHtml5ViewURL(approval, user, externalCollaborator, sharedApproval, isExternalCollaborator, 0);

            return approvalHtmlViewUrl;
        }

        #endregion

        protected override string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectOnlyFileText(Resources.lblNotification_WorkflowPhaseRerun, ApprovalId, context);
        }
    }
}
