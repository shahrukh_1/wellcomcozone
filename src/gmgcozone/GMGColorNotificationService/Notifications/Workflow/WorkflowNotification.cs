﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class WorkflowNotification : Notification
    {
        public int ApprovalId { get; set; }

        public int Phase { get; set; }
      
        public int? ExternalRecipient { get; set; }

        #region Constructor

        protected WorkflowNotification(NotificationType notificationType)
        {
            NotificationType = notificationType;
        }

        #endregion

        #region Overwrite Methods

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailSubject = GetNotificationSubject(context);
            email.subject = emailSubject;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, false);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;

            emailBody = SetHeader(emailBody, context);

            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
           
            var phaseName = GetPhaseName(Phase, context);
            var approvalDetails = (from a in context.Approvals
                                     let folder = (from f in context.Folders
                                                   where f.ID == (a.Folders.Any() ? (int?)a.Folders.FirstOrDefault().ID : null)
                                                   select f).FirstOrDefault()
                                     where a.ID == ApprovalId
                                     select new
                                            {
                                                ApprovalName = a.Job1.Title,
                                                ApprovalFolderName = folder.Name
                                            }
                                         ).FirstOrDefault();
            
            // the annotation and/or approval was deleted
            if (approvalDetails == null)
                return String.Empty;

            string userName = String.Empty;
            string userImageLocation = String.Empty;

            if (EventCreator.HasValue)
            {
                userName = User.GetUserFullName(EventCreator.Value, context);
                userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            }

            var approval = Approval.GetApproval(ApprovalId, context);
            if (approval != null)
            {
                string approvalThumbLocation = "";

                if (approval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip)
                {
                    approvalThumbLocation = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + Account.GetDomain(approval.Job1.Account, context) + "/content/img/html.gif";
                }
                else
                {
                    approvalThumbLocation = approval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(approval.ID, Account.GetDomain(approval.Job1.Account, context), context)
                                                                                                                   : Approval.GetThumbnailImagePath(approval.ID, context);
                }


                emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
            }

            string modifiedApproval = "<strong> " + approvalDetails.ApprovalName + "</strong>";
            if (!String.IsNullOrEmpty(approvalDetails.ApprovalFolderName))
            {
                emailBody = string.Format(emailBody, userImageLocation, userName, modifiedApproval, approvalDetails.ApprovalFolderName);
            }
            else
            {
                emailBody = string.Format(emailBody, userImageLocation, userName, phaseName, modifiedApproval);
            }
            emailBody = emailBody.Replace("<$body$>", string.Empty);
            
            return emailBody;
        }

        public string GetPhaseName(int phase, GMGColorContext context)
        {
 	        return (from ajp in context.ApprovalJobPhases
                    where ajp.ID == phase
                    select ajp.Name).FirstOrDefault();
        }
        #endregion

        #region Abstract Methods

        protected abstract string FormatEmailBody(string emailBody, string userImageLocation, string userName, GMGColorContext context);

        #endregion
    }
}
