﻿using System;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class AccountNotification : Notification
    {
        #region Properties

        public string AccountName { get; set; }

        #endregion

        #region Ctor

        public AccountNotification(NotificationType annotatioNotificationType)
        {
            NotificationType = annotatioNotificationType;
        }

        #endregion

        #region Public Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            emailBody = SetHeader(emailBody, context);
            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
            emailBody = string.Format(emailBody, userImageLocation, userName, AccountName);

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
