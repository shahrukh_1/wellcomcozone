﻿using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class AccountWasAdded : AccountNotification
    {
        #region Ctors

        public AccountWasAdded() 
            : base(NotificationType.AccountWasAdded)
        {
            
        }

        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationAccountCreatedHeader);
        }

        #endregion
    }
}
