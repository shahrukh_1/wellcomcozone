﻿using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class AccountWasDeleted : AccountNotification
    {
        #region Ctors

        public AccountWasDeleted()
            :base(NotificationType.AccountWasDeleted)
        {
            
        }

        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationAccountDeletedHeader);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectTextForDeletedAccount(EventCreator.Value, AccountName, context);
        }

        #endregion
    }
}
