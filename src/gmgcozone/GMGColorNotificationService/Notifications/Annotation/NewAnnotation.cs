﻿using System;
using System.Resources;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewAnnotation : AnnotationNotification
    {
        #region CTors

        public NewAnnotation():
            base(NotificationType.NewAnnotationAdded)
        {
            
        }
        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = (!ApprovalFolderId(context).HasValue) ? Resources.notificationNewCommentHeader : Resources.notificationNewCommentToFolderHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectText(Resources.lblNotification_NewCommentIsMadeTitle, ApprovalId, context);
        }

        #endregion
    }
}
