﻿using System;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewAnnotationComment : AnnotationNotification
    {
        #region CTors

        public NewAnnotationComment() 
            : base(NotificationType.ACommentIsMade)
        {
            
        }
        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = (!ApprovalFolderId(context).HasValue) ? Resources.notificationCommentHeader : Resources.notificationCommentToFolderHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectText(Resources.lblNotification_CommentIsMadeTitle, ApprovalId, context);
        }

        #endregion
    }
}
