﻿using System;
using GMGColorDAL;
using System.Linq;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class AnnotationNotification: Notification
    {
        #region Custom Properties

        public string Comment { get; set; }

        public int ApprovalId { get; set; }

        public int ApprovalAnnotationId { get; set; }

        public int? ExternalCreator { get; set; }

        public int? ExternalRecipient { get; set; }

        #endregion

        #region Ctor

        protected AnnotationNotification(NotificationType annotatioNotificationType)
        {
            NotificationType = annotatioNotificationType;
        }
        #endregion

        #region Overwrite Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;

            emailBody = SetHeader(emailBody, context);

            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));

            bool isExternalUser = ExternalRecipient.GetValueOrDefault() > 0;

            string userName = String.Empty;
            string userImageLocation = String.Empty;
            string approvalHtmlViewUrl = String.Empty;

            var data = (from aa in context.ApprovalAnnotations
                        where aa.ID == ApprovalAnnotationId
                        select new { aa.ExternalCreator, aa.Creator }).FirstOrDefault();

            var approvalDetails =   (from a in context.Approvals
                                     let folder = (from f in context.Folders
                                                   where f.ID == (a.Folders.Any() ? (int?)a.Folders.FirstOrDefault().ID : null)
                                                   select f).FirstOrDefault()
                                     where a.ID == ApprovalId
                                     select new
                                            {
                                                ApprovalName = a.Job1.Title,
                                                ApprovalFolderName = folder.Name
                                            }
                                         ).FirstOrDefault();
            
            // the annotation and/or approval was deleted
            if (data == null || approvalDetails == null)
                return String.Empty;

            if (data.ExternalCreator.HasValue)
            {
                int externalUserId = data.ExternalCreator.GetValueOrDefault();

                //get external user info drom db
                var externalInfo = (from a in context.Accounts
                    join ex in context.ExternalCollaborators on a.ID equals ex.Account
                    where ex.ID == externalUserId
                    select new
                    {
                        AccDomain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain,
                        userName = ex.GivenName + " " + ex.FamilyName

                    }).FirstOrDefault();

                if (externalInfo != null)
                {
                    userName = externalInfo.userName;
                    userImageLocation = User.GetDefaultImagePath(externalInfo.AccDomain);
                }
            }
            else
            {
                userName =
                    (from u in context.Users
                     where u.ID == EventCreator
                     select u.GivenName + " " + u.FamilyName).FirstOrDefault();

                userImageLocation = User.GetImagePath(EventCreator.Value, true, context);
            }
           
            var objRecipientUser = User.GetObject(InternalRecipient.GetValueOrDefault(), context);
            var objRecipientExUser = ExternalCollaborator.GetObjectExternal(ExternalRecipient.GetValueOrDefault(), context);

            SharedApproval objSharedApproval = null;
            objSharedApproval = (from sa in context.SharedApprovals
                                 where
                                     sa.Approval == ApprovalId &&
                                     sa.ExternalCollaborator == ExternalRecipient
                                 select sa).FirstOrDefault();

            Approval objApproval = DALUtils.GetObject<Approval>(ApprovalId, context);
            if (objApproval != null)
            {
                approvalHtmlViewUrl = Approval.GetApprovalHtml5ViewURL(objApproval, objRecipientUser, objRecipientExUser, objSharedApproval, isExternalUser, ApprovalAnnotationId);
                string approvalThumbLocation = "";

                if (objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Zip)
                {
                    approvalThumbLocation = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + Account.GetDomain(objApproval.Job1.Account, context) + "/content/img/html.gif";
                    emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation + "\" style= \"width:100%");
                }
                else
                {
                    approvalThumbLocation = objApproval.ApprovalType.Key == (int)Approval.ApprovalTypeEnum.Movie ? Approval.GetVideoImagePath(objApproval.ID, Account.GetDomain(objApproval.Job1.Account, context), context)
                                                                                                                     : Approval.GetThumbnailImagePath(objApproval.ID, context);
                    emailBody = emailBody.Replace("<$approval_thumbnail$>", approvalThumbLocation);
                }

            }

            string commentedApproval = "<strong><a style=\"color: #1997c4; text-decoration: none;\" href=\"" + approvalHtmlViewUrl + "\">" + approvalDetails.ApprovalName + "</a></strong>";
            if (!String.IsNullOrEmpty(approvalDetails.ApprovalFolderName))
            {
                emailBody = string.Format(emailBody, userImageLocation, userName, commentedApproval, approvalDetails.ApprovalFolderName);
            }
            else
            {
                emailBody = string.Format(emailBody, userImageLocation, userName, commentedApproval);
            }
            emailBody = emailBody.Replace("<$body$>", Comment);
            emailBody = emailBody.Replace("<$approvalViewURL$>", approvalHtmlViewUrl);

            return emailBody;
        }

        //public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        //{
        //    EmailInfo emailInfo = new EmailInfo();
        //    emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

        //    //if recipient has been deleted meanwhile don't send email
        //    if (emailInfo.EmailDetails.IsRecipientDeleted)
        //    {
        //        return null;
        //    }

        //    Email email = new Email();
        //    email.bodyText = string.Empty;
        //    email.toCC = string.Empty;

        //    string emailSubject = GetNotificationSubject(context);
        //    email.subject = emailSubject;

        //    EmailUtils.SetEmailDetails(emailInfo, email);

        //    emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
        //    string emailBody = CreateBody(emailBodyTemplate, context);

        //    //check if email body is empty
        //    if (String.IsNullOrEmpty(emailBody))
        //    {
        //        return null;
        //    }

        //    string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
        //    emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
        //    emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

        //    emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

        //    emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

        //    email.bodyHtml = emailOuterTemplate;

        //    return email;
        //}

        public override Email GetNotificationEmail(string emailBodyTemplate, string occurrenceTemplate, string emailOuterTemplate, GMGColorContext context, bool passAnnotationID)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(InternalRecipient.HasValue ? InternalRecipient.Value : ExternalRecipient.Value, ExternalRecipient.HasValue, context);

            //if recipient has been deleted meanwhile don't send email
            if (emailInfo.EmailDetails.IsRecipientDeleted)
            {
                return null;
            }

            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailSubject = GetNotificationSubject(context);
            email.subject = emailSubject;

            EmailUtils.SetEmailDetails(emailInfo, email);

            emailOuterTemplate = EmailUtils.SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);
            string emailBody = CreateBody(emailBodyTemplate, context, passAnnotationID);

            //check if email body is empty
            if (String.IsNullOrEmpty(emailBody))
            {
                return null;
            }

            string emailTitle = EmailUtils.GetEmailTitle(NotificationType);
            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", emailBody);
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = EmailUtils.PopulateFooter(emailOuterTemplate, emailInfo);

            emailOuterTemplate = EmailUtils.CleanupEmail(emailOuterTemplate);

            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        #endregion

        #region Protected Methods

        protected int? ApprovalFolderId(GMGColorContext context)
        {
            return Approval.GetFolder(ApprovalId, context);
        }

        #endregion
    }
}
