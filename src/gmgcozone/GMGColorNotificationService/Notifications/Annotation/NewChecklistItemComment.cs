﻿using System;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
   public class NewChecklistItemComment : AnnotationNotification
    {
        #region CTors
        public NewChecklistItemComment() 
            : base(NotificationType.ChecklistItemEdited)
        {

        }
        #endregion
        #region Methods
        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = Resources.notificationChecklistHeader;//(!ApprovalFolderId(context).HasValue) ? Resources.notificationCommentHeader : Resources.notificationCommentToFolderHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectText(Resources.lblNotification_ChecklistItemEdited, ApprovalId, context);
        }

        #endregion
    }
}
