﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class NewReplyToMyComment : AnnotationNotification
    {
        #region CTors

        public NewReplyToMyComment()
            : base(NotificationType.RepliesToMyComments)
        {
            
        }
        #endregion

        #region Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            string eventHeader = (!ApprovalFolderId(context).HasValue) ? Resources.notificationRepliedCommentHeader : Resources.notificationRepliedCommentToFolderHeader;
            return template.Replace("<$notificationHeader$>", eventHeader);
        }

        protected override string GetNotificationSubject(GMGColorContext context)
        {
            return GetNotificationSubjectText(Resources.lblNotification_RepliedToCommentTitle, ApprovalId, context);
        }

        #endregion
    }
}
