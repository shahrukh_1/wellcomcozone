﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class GroupWasCreated : AdministrationNotification
    {
        #region Properties

        public int UserGroupID { get; set; }

        #endregion

        #region Ctors

        public GroupWasCreated() : base(NotificationType.GroupWasCreated)
        {
            
        }

        #endregion

        #region Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = String.Empty;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            UserGroup userGroup = (from ug in context.UserGroups
                                    where ug.ID == UserGroupID
                                    select ug).FirstOrDefault();

            if (userGroup != null)
            {
                string groupName = userGroup.Name;
                int userCount = userGroup.UserGroupUsers.Count;

                emailBody = template;
                emailBody = SetHeader(emailBody, context);
                emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
                emailBody = string.Format(emailBody, userImageLocation, userName, groupName, userCount);
            }
            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationNewGroupCreatedHeader);
        }

        #endregion
    }
}
