﻿using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class PlanLimitWarning : AdministrationNotification
    {
        #region Properties

        public string AppModule { get; set; }

        public bool IsStorageWarning { get; set; }

        #endregion

        #region Ctors

        public PlanLimitWarning() : base(NotificationType.PlanLimitWarning)
        {
            
        }

        #endregion

        #region Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;
            string userImageLocation = string.Empty;

            var userInfo = (from a in context.Accounts
                                join u in context.Users on a.ID equals u.Account
                                where u.ID == InternalRecipient.Value
                                select new
                                {
                                    AccDomain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain
                                }).FirstOrDefault();

            if (userInfo != null)
            {
                userImageLocation = User.GetDefaultImagePath(userInfo.AccDomain);
            }
            
            emailBody = SetHeader(emailBody, context);
            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
            emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
            emailBody = string.Format(emailBody, userImageLocation, AppModule);

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", IsStorageWarning? Resources.lblStorageSizeWarning : Resources.notificationPlanLimitWarningHeader);
        }

        #endregion
    }   
}
