﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class OutOfOffice : AdministrationNotification
    {
        #region Properties

        public int SubstitutedUserID { get; set; }

        #endregion


        #region Ctors

        public OutOfOffice()
            : base(NotificationType.OutOfOffice)
        {

        }
        #endregion

        #region Methods
        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            var emailBody = template;
            string userImageLocation = string.Empty;
            string userName = string.Empty;
            string dateFormat = string.Empty;
          
            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID,
                               Account = u.Account
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
                dateFormat = (from a in context.Accounts
                              join p in context.DateFormats on a.DateFormat equals p.ID
                              where a.ID == creator.Account
                              select p.Pattern).FirstOrDefault();
            }
                    
            var SelectedOutOfOffice = (from o in context.OutOfOffices
                             where o.Owner == EventCreator.Value
                             select o).OrderByDescending(o => o.ID).FirstOrDefault();

            var startDate = GMGColorFormatData.GetFormattedDate(SelectedOutOfOffice.StartDate, dateFormat);
            var endDate = GMGColorFormatData.GetFormattedDate(SelectedOutOfOffice.EndDate, dateFormat);

            var timePeriod = startDate +" "+ "To" +" "+ endDate;
                  //var user = (from u in context.Users
                  //            where u.ID == SubstitutedUserID
                  //            select new
                  //            {
                  //                UserId = u.ID,
                  //                UserStatusID = u.Status,
                  //                FullName = u.GivenName + " " + u.FamilyName,
                  //                UserGroups = u.UserGroupUsers.Select(ug => ug.UserGroup1.Name).ToList()
                  //            }).FirstOrDefault();

                  //if (user != null)
                  //{
                  //    //string roles = "";
                  //userRoles.ForEach(
                  //    o =>
                  //    roles += string.Format(Resources.notificationUserAddedHeaderForEachModule, o.RoleName, o.ModuleName));

                  emailBody = SetHeader(emailBody, context);
                emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
                emailBody = string.Format(emailBody, userImageLocation, userName, timePeriod);
            //}

            return emailBody;
        }
        #endregion

        #region Protected Methods
        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationOutOfOffice);

            #endregion
        }
    }
}
