﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class UserWasAdded : AdministrationNotification
    {
        #region Properties

        public int CreatedUserID { get; set; }

        #endregion

        #region Ctors

        public UserWasAdded()
            : base(NotificationType.UserWasAdded)
        {

        }

        #endregion

        #region Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            var emailBody = template;
            string userImageLocation = string.Empty;
            string userName = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                               {
                                   FullName = u.GivenName + " " + u.FamilyName,
                                   UserID = u.ID
                               }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            var userRoles = (from ur in context.UserRoles
                             where ur.User == CreatedUserID
                             select new {RoleName = ur.Role1.Name, ModuleName = ur.Role1.AppModule1.Name}).ToList();

            var user = (from u in context.Users
                        where u.ID == CreatedUserID
                        select new
                            {
                                UserId = u.ID,
                                UserStatusID = u.Status,
                                FullName = u.GivenName + " " + u.FamilyName,
                                UserGroups = u.UserGroupUsers.Select(ug => ug.UserGroup1.Name).ToList()
                            }).FirstOrDefault();

            if (user != null)
            {
                string roles = "";
                userRoles.ForEach(
                    o =>
                    roles += string.Format(Resources.notificationUserAddedHeaderForEachModule, o.RoleName, o.ModuleName));

                string groups = "";
                user.UserGroups.ForEach(
                    g => groups += string.Format(Resources.notificationUserAddedHeaderForEachGroup, g));

                //TODO The content of this mail should be changed with the roles for all the modules that were defined into system
                emailBody = SetHeader(emailBody, context);
                emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
                emailBody = emailBody.Replace("<$is_visible_thumbnail$>", "none !important;");
                emailBody = string.Format(emailBody, userImageLocation, userName, user.FullName, roles, groups);
            }

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationUserAddedHeader);
        }

        #endregion
    }
}
