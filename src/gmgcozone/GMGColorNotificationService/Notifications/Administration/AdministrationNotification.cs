﻿using System;
using GMGColorDAL;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public abstract class AdministrationNotification : Notification
    {
        #region Properties

        public int Account { get; set; }

        #endregion

        #region Ctors

        public AdministrationNotification(NotificationType notificationType)
        {
            NotificationType = notificationType;
        }

        #endregion
    }
}
