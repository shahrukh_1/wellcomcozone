﻿using System;
using System.Linq;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorNotificationService
{
    public class PlanWasChanged : AdministrationNotification
    {
        #region Properties

        public int? OldBilingPlan { get; set; }

        public int? NewBilingPlan { get; set; }

        #endregion

        #region Ctors

        public PlanWasChanged() : base(NotificationType.PlanWasChanged)
        {
            
        }

        #endregion

        #region Methods

        public override string CreateBody(string template, GMGColorContext context, bool passAnnotationID)
        {
            string emailBody = template;
            string userImageLocation = string.Empty;
            string userName = string.Empty;
            string changedAccount = string.Empty;

            var creator = (from u in context.Users
                           where u.ID == EventCreator.Value
                           select new
                           {
                               FullName = u.GivenName + " " + u.FamilyName,
                               UserID = u.ID
                           }).FirstOrDefault();

            if (creator != null)
            {
                userName = creator.FullName;
                userImageLocation = User.GetImagePath(creator.UserID, true, context);
            }

            //TODO  handle no subscription case
            var account = (from a in context.Accounts
                           where a.ID == Account
                           select new { GPPDiscount = a.GPPDiscount, CurrencySymbol = a.Currency.Symbol, AccountName = a.Name, AccountDomain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain }).FirstOrDefault();

            string plan1Name = Resources.lblNoSubscription;
            if (OldBilingPlan.HasValue)
            {
                BillingPlan objBillingPlan1 = DALUtils.GetObject<BillingPlan>(OldBilingPlan.Value, context);
                plan1Name = objBillingPlan1.Name;
            }

            decimal planPrice = 0;
            string plan2Name = Resources.lblNoSubscription;
            if (NewBilingPlan.HasValue)
            {
                BillingPlan objBillingPlan2 = DALUtils.GetObject<BillingPlan>(NewBilingPlan.Value, context);
                plan2Name = objBillingPlan2.Name;
                planPrice = objBillingPlan2.Price;
            }


            if (account != null)
            {
                var accountUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + account.AccountDomain + "/";
                changedAccount = "<strong><a href=\"" + accountUrl + "\" style=\"color: #1997c4;text-decoration: none;\"> " + account.AccountName + "'s</a></strong>";
            }
           
            decimal discount = (account.GPPDiscount.HasValue && account.GPPDiscount.Value > 0) ? account.GPPDiscount.Value : 0;
            decimal price = planPrice - (planPrice * (discount / 100));
            string accountBillingPlanPrice = GMGColorFormatData.GetFormattedCurrency(price, account.CurrencySymbol) + " " + Resources.lblNotification_PerMonth;

            emailBody = SetHeader(emailBody, context);
            emailBody = emailBody.Replace("<$timestamp$>", GMGColorFormatData.GetTimeDifference(CreatedDate));
            emailBody = string.Format(emailBody, userImageLocation, userName, changedAccount, plan1Name, plan2Name, plan2Name, accountBillingPlanPrice);

            return emailBody;
        }

        #endregion

        #region Protected Methods

        protected override string SetHeader(string template, GMGColorContext context)
        {
            return template.Replace("<$notificationHeader$>", Resources.notificationPlanChangedHeader);
        }

        #endregion
    }
}
