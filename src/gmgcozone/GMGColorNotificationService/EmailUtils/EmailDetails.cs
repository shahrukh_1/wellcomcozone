﻿using GMGColorDAL;

namespace GMGColorNotificationService
{
    public class EmailDetails
    {
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ToName { get; set; }
        public string ToEmail { get; set; }
        public string AccountName { get; set; }
        public string AccountUrl { get; set; }
        public int? AccountId { get; set; }
        public string EmailLogoPath { get; set; }
        public string ManageNotificationAction { get; set; }
        public bool IsRecipientDeleted { get; set; }
        public bool IsRemoveAllGMGCollaborateBranding { get; set; }
        public int AccountAltId { get; set; }
    }
}
