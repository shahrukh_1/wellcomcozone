﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using GMGColor.Resources;
using GMGColorDAL;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using GMGColorDAL.Extensions;

namespace GMGColorNotificationService
{
    public class EmailUtils
    {
	
		/// <summary>
        /// Update email with the occurrence template
        /// </summary>
        /// <param name="email"></param>
        /// <param name="occurrenceTemplate"></param>
        /// <returns></returns>
        public static string SetEmailoccurrenceLabel(string email, string occurrenceTemplate)
        {
            return email.Replace("<$template_content$>", occurrenceTemplate);
        }
		
		/// <summary>
        /// Set email to and from details
        /// </summary>
        /// <param name="emailInfo"></param>
        /// <param name="email"></param>
        public static void SetEmailDetails(EmailInfo emailInfo, Email email)
        {
            email.fromEmail = emailInfo.EmailDetails.FromEmail;
            email.fromName = emailInfo.EmailDetails.FromName;
            email.toName = emailInfo.EmailDetails.ToName;
            email.toEmail = emailInfo.EmailDetails.ToEmail;
            email.Account = emailInfo.EmailDetails.AccountId;
        }

        /// <summary>
        /// Populates template with footer info
        /// </summary>
        /// <param name="outerTemplate">Template to populate</param>
        /// <param name="emailInfo">Info about the Email</param>
        /// <returns>template populated with footer info</returns>
        public static string PopulateFooter(string outerTemplate, EmailInfo emailInfo)
        {
            string s = emailInfo.EmailDetails.AccountName.Substring(emailInfo.EmailDetails.AccountName.Length - 2);
            if(s == "'s")
            {
                emailInfo.EmailDetails.AccountName = emailInfo.EmailDetails.AccountName.Remove(emailInfo.EmailDetails.AccountName.Length - 2);
            }
            outerTemplate = outerTemplate.Replace("<$accountlogo$>", emailInfo.EmailDetails.EmailLogoPath);
            outerTemplate = outerTemplate.Replace("<$accountname$>", emailInfo.EmailDetails.AccountName);
            outerTemplate = outerTemplate.Replace("<$pharagraphManageYourEmailNotification$>", Resources.pharagraphManageYourEmailNotification);
            outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter1$>", String.Format(Resources.pharagraphCommonFooter1, emailInfo.EmailDetails.AccountName));
            outerTemplate = outerTemplate.Replace("<$send_to$>", String.Format(Resources.lblSendTo, emailInfo.EmailDetails.ToEmail));
            if (!emailInfo.EmailDetails.IsRemoveAllGMGCollaborateBranding)
            {
                outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter2$>", Resources.pharagraphCommonFooter2);
            }
            outerTemplate = outerTemplate.Replace("<$manage_notification_action$>", emailInfo.EmailDetails.ManageNotificationAction);
            outerTemplate = outerTemplate.Replace("<$ServerProtocol$>", GMGColorConfiguration.AppConfiguration.ServerProtocol);
            outerTemplate = outerTemplate.Replace("<$UserAccountURL$>", emailInfo.EmailDetails.AccountUrl);

            return outerTemplate;
        }

        public static string PopulateFooterForWelcomeUser(string outerTemplate, EmailInfo emailInfo)
        {
            string s = emailInfo.EmailDetails.AccountName.Substring(emailInfo.EmailDetails.AccountName.Length - 2);
            if (s == "'s")
            {
                emailInfo.EmailDetails.AccountName = emailInfo.EmailDetails.AccountName.Remove(emailInfo.EmailDetails.AccountName.Length - 2);
            }
            outerTemplate = outerTemplate.Replace("<$accountlogo$>", emailInfo.EmailDetails.EmailLogoPath);
            outerTemplate = outerTemplate.Replace("<$accountname$>", emailInfo.EmailDetails.AccountName);
            outerTemplate = outerTemplate.Replace("<$pharagraphManageYourEmailNotification$>", String.Empty);
            outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter1$>", String.Format(Resources.pharagraphCommonFooter1, emailInfo.EmailDetails.AccountName));
            outerTemplate = outerTemplate.Replace("<$send_to$>", String.Format(Resources.lblSendTo, emailInfo.EmailDetails.ToEmail));
            if (!emailInfo.EmailDetails.IsRemoveAllGMGCollaborateBranding)
            {
                outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter2$>", Resources.pharagraphCommonFooter2);
            }
            outerTemplate = outerTemplate.Replace("<$manage_notification_action$>", String.Empty);
            outerTemplate = outerTemplate.Replace("<$ServerProtocol$>", GMGColorConfiguration.AppConfiguration.ServerProtocol);
            outerTemplate = outerTemplate.Replace("<$UserAccountURL$>", emailInfo.EmailDetails.AccountUrl);

            return outerTemplate;
        }

        /// <summary>
        /// Popualtes deliver timeout template footer
        /// </summary>
        /// <param name="outerTemplate">Email outer template</param>
        /// <param name="emailInfo">Info about the Email</param>
        /// <returns>template populated with footer info</returns>
        public static string PopulateDeliverTimeoutTemplate(string outerTemplate, EmailInfo emailInfo)
        {
            outerTemplate = outerTemplate.Replace("<$accountlogo$>", emailInfo.EmailDetails.EmailLogoPath);
            outerTemplate = outerTemplate.Replace("<$accountname$>", emailInfo.EmailDetails.AccountName);
            outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter2$>", Resources.pharagraphCommonFooter2);
            outerTemplate = outerTemplate.Replace("<$send_to$>", String.Format(Resources.lblSendTo, emailInfo.EmailDetails.ToEmail));

            return outerTemplate;
        }

        /// <summary>
        /// Populate new pairing code template email 
        /// </summary>
        /// <param name="outerTemplate"></param>
        /// <param name="emailInfo"></param>
        /// <returns></returns>
        public static string PopulateNewDeliverPairingCodeTemplate(string outerTemplate, EmailInfo emailInfo)
        {
            outerTemplate = outerTemplate.Replace("<$accountlogo$>", emailInfo.EmailDetails.EmailLogoPath);
            outerTemplate = outerTemplate.Replace("<$accountname$>", emailInfo.EmailDetails.AccountName);
            outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter2$>", Resources.pharagraphCommonFooter2);
            outerTemplate = outerTemplate.Replace("<$send_to$>", String.Empty);
            outerTemplate = outerTemplate.Replace("<$pharagraphCommonFooter1$>", String.Empty);
            outerTemplate = outerTemplate.Replace("<$send_to$>", String.Empty);
            outerTemplate = outerTemplate.Replace("<$manage_notification_action$>", String.Empty);
            outerTemplate = outerTemplate.Replace("<$pharagraphManageYourEmailNotification$>", string.Empty);
            outerTemplate = outerTemplate.Replace("<$ServerProtocol$>", GMGColorConfiguration.AppConfiguration.ServerProtocol);
            outerTemplate = outerTemplate.Replace("<$UserAccountURL$>", emailInfo.EmailDetails.AccountUrl);

            return outerTemplate;
        }

        /// <summary>
        /// Set bulk email title
        /// </summary>
        /// <param name="email"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        public static string GetEmailTitle(NotificationType notificationType)
        {
            string emailTitle = string.Empty;

            switch (notificationType)
            {
                case NotificationType.ChecklistItemEdited:
                    emailTitle = Resources.lblNotification_ChecklistItemEdited;
                    break;
                case NotificationType.ACommentIsMade:
                    emailTitle = Resources.lblNotification_CommentIsMadeTitle;
                    break;
                case NotificationType.RepliesToMyComments:
                    emailTitle = Resources.lblNotification_RepliedToCommentTitle;
                    break;
                case NotificationType.NewAnnotationAdded:
                    emailTitle = Resources.lblNotification_NewCommentIsMadeTitle;
                    break;
                case NotificationType.NewApprovalAdded:
                    emailTitle = Resources.lblNotification_NewApprovalAddedTitle;
                    break;
                case NotificationType.ApprovalWasDownloaded:
                    emailTitle = Resources.lblNotification_ApprovalDownloadedTitle;
                    break;
                case NotificationType.ApprovalWasUpdated:
                    emailTitle = Resources.lblNotification_ApprovalUpdatedTitle;
                    break;
                case NotificationType.ApprovalWasDeleted:
                    emailTitle = Resources.lblNotification_ApprovalDeletedTitle;
                    break;
                case NotificationType.ApprovalStatusChanged:
                    emailTitle = Resources.lblNotification_ApprovalStatusChangedTitle;
                    break;
                case NotificationType.ApprovalStatusChangedByPdm:
                    emailTitle = Resources.lblNotification_ApprovalStatusChangedByPdm;
                    break;
                case NotificationType.NewVersionWasCreated:
                    emailTitle = Resources.lblNotification_NewVersionCreatedTitle;
                    break;
                case NotificationType.ApprovalWasShared:
                    emailTitle = Resources.lblNotification_ApprovalSharedTitle;
                    break;
                case NotificationType.UserWasAdded:
                    emailTitle = Resources.lblNotification_UserWasAddedTitle;
                    break;
                case NotificationType.GroupWasCreated:
                    emailTitle = Resources.lblNotification_GroupCreatedTitle;
                    break;
                case NotificationType.PlanWasChanged:
                    emailTitle = Resources.lblNotification_PlanWasChangedTitle;
                    break;
                case NotificationType.AccountWasDeleted:
                    emailTitle = Resources.lblNotification_AccountDeletedTitle;
                    break;
                case NotificationType.AccountWasAdded:
                    emailTitle = Resources.lblNotification_AccountAddedTitle;
                    break;
                case NotificationType.DeliverFileTimeOut:
                    emailTitle = Resources.lblNotification_DeliverJobTimeoutTitle;
                    break;
                case NotificationType.WorkstationRegistered:
                    emailTitle = Resources.lblNotification_WorkstationRegistrationTitle;
                    break;
                case NotificationType.DeliverFileWasSubmitted:
                    emailTitle = Resources.lblNotification_NewDeliverTitle;
                    break;               
                case NotificationType.DeliverFileCompleted:
                    emailTitle = Resources.lblNotification_DeliverJobCompletedTitle;
                    break;
                case NotificationType.DeliverFileErrored:
                    emailTitle = Resources.lblNotification_DeliverJobFileErrorTitle;
                    break;               
                case NotificationType.SharedWorkflowAccessRevoked:
                    emailTitle = Resources.lblNotification_AccessRevokedTitle;
                    break;
                case NotificationType.InvitedUserDeleted:
                    emailTitle = Resources.lblNotification_UserDeletedTitle;
                    break;
                case NotificationType.InvitedUserFileSubmitted:
                    emailTitle = Resources.lblNotification_NewDeliverTitle;
                    break;
                case NotificationType.ShareRequestAccepted:
                    emailTitle = Resources.lblNotification_ShareAcceptedTitle;
                    break;
                case NotificationType.SharedWorkflowDeleted:
                    emailTitle = Resources.lblNotification_SharedWorkflowDeletedTitle;
                    break;
                case NotificationType.HostAdminUserDeleted:
                    emailTitle = Resources.lblNotification_UserDeletedTitle;
                    break;
                case NotificationType.HostAdminUserFileSubmitted:
                    emailTitle = Resources.lblNotification_NewDeliverTitle;
                    break;
                case NotificationType.LostPassword:
                    emailTitle = Resources.lblLostYourPassword;
                    break;
                case NotificationType.ResetPassword:
                    emailTitle = Resources.lblLostYourPassword;
                    break;
                case NotificationType.SharedWorkflowInvitation:
                    emailTitle = Resources.notificationSharedWorkflowHeader;
                    break;
                case NotificationType.DeliverNewPairingCode:
                    emailTitle = Resources.notificationNewCPInstanceHeader;
                    break;
                case NotificationType.ApprovalReminder:
                    emailTitle = Resources.lblApprovalReminder;
                    break;
                case NotificationType.DemoPlanExpired:
                    emailTitle = Resources.lblDemoPlanExpired;
                    break;               
                case NotificationType.PlanLimitReached:
                    emailTitle = Resources.lblPlanLimitReached;
                    break;
                case NotificationType.PlanLimitWarning:
                    emailTitle = Resources.lblPlanLimitWarning;
                    break;
                case NotificationType.ApprovalChangesCompleted:
                    emailTitle = Resources.lblNotification_ApprovalChangesComplete;
                    break;
                case NotificationType.ApprovalWasRejected:
                    emailTitle = Resources.lblNotification_ApprovalWasRejected;
                    break;
                case NotificationType.ApprovalOverdue:
                    emailTitle = Resources.lblNotification_ApprovalOverdue;
                    break;
                case NotificationType.PhaseWasDeactivated:
                    emailTitle = Resources.lblNotification_PhaseWasDeactivated;
                    break;
                case NotificationType.PhaseWasActivated:
                    emailTitle = Resources.lblNotification_PhaseWasActivated;
                    break;
                case NotificationType.WorkflowPhaseRerun:
                    emailTitle = Resources.lblNotification_WorkflowPhaseRerun;
                    break;
                case NotificationType.PhaseOverdue:
                    emailTitle = Resources.lblNotification_PhaseOverdue;
                    break;
                case NotificationType.NewFileTransferAdded:
                    emailTitle = Resources.lblNotification_NewFileTransferAdded;
                    break;
                case NotificationType.FileTransferDownloaded:
                    emailTitle = Resources.lblNotification_FileTransferDownloaded;
                    break;
                case NotificationType.OutOfOffice:
                    emailTitle = Resources.lblNotification_SubstituteUser;
                    break;
                case NotificationType.ApprovalJobCompleted:
                    emailTitle = Resources.lblNotification_All_Decisions_Completed;
                    break;
                case NotificationType.NewProjectFolder:
                    emailTitle = Resources.lblNotification_New_ProjectFolder;
                    break;
                case NotificationType.ProjectFolderWasShared:
                    emailTitle = Resources.lblNotification_ProjectFolder_was_shared;
                    break;
                case NotificationType.ProjectFolderWasUpdated:
                    emailTitle = Resources.lblNotification_ProjectFolder_was_updated;
                    break;
            }
            return emailTitle;
        }

        /// <summary>
        /// Cleanup any template data
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static string CleanupEmail(string email)
        {
            return Regex.Replace(email, Constants.EmailContentDataPattern, string.Empty, RegexOptions.Multiline);
        }


        /// <summary>
        /// Create daily and weekly emails
        /// </summary>      
        /// <param name="userNotificationsToSend"></param>
        /// <param name="frequency"></param>
        /// <param name="templatesDictionary"></param>
        /// <param name="emailTemplate"></param>
        /// <param name="context"></param>
        /// <param name="internalRecipientId"></param>
        public static Email ProcessEmailsByFrequencyType(List<NotificationGroup> userNotificationsToSend, NotificationFrequency.Frequency frequency, Dictionary<string, List<string>> templatesDictionary,
                                                                            int internalRecipientId, string emailTemplate, GMGColorContext context)
        {

            //Create email
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(internalRecipientId, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            //Get current internal recipient date and time info
            var recipientDateTimeInfo = (from acc in context.Accounts
                                            join u in context.Users on acc.ID equals u.Account
                                            where u.ID == internalRecipientId
                                            select new {
                                                acc.DateFormat1.Pattern,
                                                acc.TimeZone
                                            }).FirstOrDefault();

            var userCurrentDateTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, recipientDateTimeInfo.TimeZone);
            var emailGroups = 0;
            //Create digest email body, concatenating each event group body
            emailTemplate = Enum.GetNames(typeof(EventGroup.GroupName)).Select(name => (EventGroup.GroupName)Enum.Parse(typeof(EventGroup.GroupName), name))
                            .Aggregate(emailTemplate, (current, groupName) => GetEmailsBodyByNotificationtype(userNotificationsToSend, groupName, templatesDictionary, frequency, current, ref emailGroups, context));

            if (emailGroups == 0)
            {
                return null;
            }

            switch (frequency)
            {
                case NotificationFrequency.Frequency.Daily:
                    {
                        string date = GMGColorFormatData.GetFormattedDate(userCurrentDateTime, recipientDateTimeInfo.Pattern);

                        emailTemplate = emailTemplate.Replace("<$title$>", Resources.lblNotification_EmailTitleDaily);
                        emailTemplate = emailTemplate.Replace("<$date$>", date);
                        email.subject = string.Format(Resources.lblNotificationEmail_DailySubject, date);
                    }
                    break;
                case NotificationFrequency.Frequency.Weekly:
                    {
                        string dateRange = GMGColorFormatData.GetFormattedDate(userCurrentDateTime.AddDays(-7), recipientDateTimeInfo.Pattern) + " " + Resources.lblTo + " " + GMGColorFormatData.GetFormattedDate(userCurrentDateTime, recipientDateTimeInfo.Pattern);

                        emailTemplate = emailTemplate.Replace("<$title$>", Resources.lblNotification_EmailTitleWeekly);
                        emailTemplate = emailTemplate.Replace("<$daterange$>", dateRange);
                        email.subject = string.Format(Resources.lblNotificationEmail_WeeklySubject, dateRange);
                    }
                    break;
                case NotificationFrequency.Frequency.PerProofStudioSession:
                {
                    var date = GMGColorFormatData.GetFormattedDate(userCurrentDateTime, recipientDateTimeInfo.Pattern);
                    
                    emailTemplate = emailTemplate.Replace("<$title$>", Resources.lblNotification_EmailPerProofStudioSession);
                    emailTemplate = emailTemplate.Replace("<$date$>", date);
                    email.subject = string.Format(Resources.lblNotification_EmailPerProofStudioSession, date);
                }
                    break;
            }

            SetEmailDetails(emailInfo, email);
            emailTemplate = PopulateFooter(emailTemplate, emailInfo);
            emailTemplate = CleanupEmail(emailTemplate);
            email.bodyHtml = emailTemplate;

            return email;
        }

        /// <summary>
        /// Concatenate scheduled email that is to be received by user
        /// </summary>
        /// <param name="userNotificationsToSend"></param>
        /// <param name="templatesDictionary"></param>
        /// <param name="internalRecipientId"></param>
        /// <param name="emailTemplate"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Email CreateUserScheduleEmail(List<NotificationGroup> userNotificationsToSend, Dictionary<string, List<string>> templatesDictionary,
                                                                            int internalRecipientId, string emailTemplate, GMGColorContext context)
        {
            //Create email
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(internalRecipientId, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            //Get current internal recipient date and time info
            var recipientDateTimeInfo = (from acc in context.Accounts
                                         join u in context.Users on acc.ID equals u.Account
                                         where u.ID == internalRecipientId
                                         select new
                                         {
                                             acc.DateFormat1.Pattern,
                                             acc.TimeZone,
                                             acc.TimeFormat
                                         }).FirstOrDefault();

            var userCurrentDateTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, recipientDateTimeInfo.TimeZone);
            var emailGroups = 0;
            //Create digest email body, concatenating each event group body
            emailTemplate = Enum.GetNames(typeof(EventGroup.GroupName)).Select(name => (EventGroup.GroupName)Enum.Parse(typeof(EventGroup.GroupName), name))
                            .Aggregate(emailTemplate, (current, groupName) => GetEmailsBodyByNotificationtype(userNotificationsToSend, groupName, templatesDictionary, NotificationFrequency.Frequency.Weekly, current, ref emailGroups, context));

            if (emailGroups == 0)
            {
                return null;
            }

            string date = GMGColorFormatData.GetFormattedDate(userCurrentDateTime, recipientDateTimeInfo.Pattern);

            date = date + " " + GMGColorFormatData.GetFormattedTime(userCurrentDateTime, recipientDateTimeInfo.TimeFormat);

            emailTemplate = emailTemplate.Replace("<$title$>", Resources.lblNotification_EmailDigestTitle);
            emailTemplate = emailTemplate.Replace("<$daterange$>", date);
            email.subject = string.Format(Resources.lblNotification_EmailDigestSubject, date);

            SetEmailDetails(emailInfo, email);
            emailTemplate = PopulateFooter(emailTemplate, emailInfo);
            emailTemplate = CleanupEmail(emailTemplate);
            email.bodyHtml = emailTemplate;

            return email;
        }

        /// <summary>
        /// Creates approval reminders emails
        /// </summary>
        /// <param name="userRemindersToSend"></param>
        /// <param name="emailOuterTemplate"></param>
        /// <param name="internalRecipient"></param>
        /// <param name="emailBodyTemplate"></param>
        /// <param name="occurrenceTemplate"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Email CreateApprovalReminderEmail(List<ApprovalReminder> userRemindersToSend, string emailOuterTemplate, int internalRecipient, string emailBodyTemplate, string occurrenceTemplate, GMGColorContext context)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.Populate(internalRecipient, false, context);
            Email email = new Email();
            email.bodyText = string.Empty;
            email.toCC = string.Empty;

            string emailTitle = GetEmailTitle(NotificationType.ApprovalReminder);
            email.subject = emailTitle;

            SetEmailDetails(emailInfo, email);

            var aggregated = new StringBuilder();
            foreach (var reminder in userRemindersToSend)
            {
                var emailBody = reminder.CreateBody(emailBodyTemplate, context, false);

                if (!String.IsNullOrEmpty(emailBody))
                {
                    aggregated.Append(emailBody);
                }
            }

            emailOuterTemplate = SetEmailoccurrenceLabel(emailOuterTemplate, occurrenceTemplate);

            emailOuterTemplate = emailOuterTemplate.Replace("<$body$>", aggregated.ToString());
            emailOuterTemplate = emailOuterTemplate.Replace("<$title$>", emailTitle);

            emailOuterTemplate = PopulateFooter(emailOuterTemplate, emailInfo);
            emailOuterTemplate = CleanupEmail(emailOuterTemplate);
            email.bodyHtml = emailOuterTemplate;

            return email;
        }

        /// <summary>
        /// Get the emails body concatenated as single string and returns the total number of occurrences
        /// </summary>
        /// <param name="notificationsToSend"></param>
        /// <param name="eventGroup"></param>
        /// <param name="templatesDictionary"></param>
        /// <param name="emailTemplate"></param>
        /// <param name="context"></param>
        /// <param name="frequency"></param>
        /// <returns></returns>
        private static string GetEmailsBodyByNotificationtype(List<NotificationGroup> notificationsToSend, EventGroup.GroupName eventGroup, Dictionary<string, List<string>> templatesDictionary, 
                                                            NotificationFrequency.Frequency frequency, string emailTemplate, ref int emailGroups, GMGColorContext context)
        {
            var aggregated =  new StringBuilder();

            //Group notifications by event group key
            List<Notification> items = notificationsToSend.Where(n => n.EventGroupKey == EventGroup.GetKey(eventGroup)).Select(n => n.Notifications).ToList();

            int numberOfIgnoredEmails = 0;

            //Create body template for each notification from current group
            foreach (var notification in items)
            {
                string isVisibleBody = (frequency != NotificationFrequency.Frequency.Weekly) ? string.Empty/*"block;"*/ : "none !important;";
                string template = templatesDictionary.FirstOrDefault(x => x.Value.Contains(notification.NotificationType.ToString())).Key;

                template = template.Replace("<$is_visible_body$>", isVisibleBody);

                string emailBody = notification.CreateBody(template, context, false);

                if (!String.IsNullOrEmpty(emailBody))
                {
                    aggregated.Append(emailBody);
                }
                else
                {
                    //emails body is empty remove it from email(probably data missing from DB)
                    numberOfIgnoredEmails++;
                }
            }

            // The numebr of the notifications from current group (module)
            int count = items.Sum(t => GetNotificationEmailBodiesCount(t)) - numberOfIgnoredEmails;
            emailGroups += count > 0 ? 1 : 0;

            var eventGroupName = eventGroup.ToString().ToLower();
            string isGroupVisible = (count > 0) ? /*string.Empty*/"block !important;" : "none !important;";

            //Replace daily or weekly template, depending on frequency, with created email body for each module
            emailTemplate = emailTemplate.Replace("<$is_" + eventGroupName + "_visible$>", isGroupVisible);
            if (count > 0)
            {
                emailTemplate = emailTemplate.Replace("<$" + eventGroupName + "$>", EventGroup.GetLabelByNotificationEventGroupName(eventGroup));
                emailTemplate = emailTemplate.Replace("<$" + eventGroupName + "_count$>", count.ToString());
                emailTemplate = emailTemplate.Replace("<$" + eventGroupName + "_html$>", aggregated.ToString());
            }
            return emailTemplate;
        }

        /// <summary>
        /// Gets number of emails bodies contained by the current notification
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        private static int GetNotificationEmailBodiesCount(Notification notification)
        {
            int bodiesCount = 1;

            switch (notification.NotificationType)
            {
                case NotificationType.NewApprovalAdded:
                case NotificationType.ApprovalWasDeleted:
                case NotificationType.ApprovalWasDownloaded:
                case NotificationType.ApprovalWasShared:
                case NotificationType.NewVersionWasCreated:
                    ApprovalNotification appNotif = notification as ApprovalNotification;
                    bodiesCount = appNotif.ApprovalsIds.Length;
                    break;               
                case NotificationType.DeliverFileWasSubmitted:
                    DeliverNotification delNotif = notification as DeliverNotification;
                    bodiesCount = delNotif.JobsGuids.Length;
                    break;
            }

            return bodiesCount;
        }
    }
}
