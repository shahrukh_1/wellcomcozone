﻿using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Extensions;

namespace GMGColorNotificationService
{
    public class EmailInfo
    {
        private EmailDetails _emailDetails;

        public EmailDetails EmailDetails
        {
            get { return _emailDetails; }
            private set { }
        }

        public EmailInfo()
        {
            _emailDetails = new EmailDetails();
        }

        /// <summary>
        /// Populate email item details
        /// </summary>
        /// <param name="recipient">Recipient id</param>
        /// <param name="isExternalRecipient">External user flag</param>
        /// <param name="context">Database context</param>
        /// <param name="isDeliverExternal">Deliver external user flag</param>
        public void Populate(int recipient, bool isExternalRecipient, GMGColorContext context, bool isDeliverExternal = false)
        {
            if (isExternalRecipient)
            {
                var externalUser = GetEmailUserDetailsForExternalUser(recipient, context, isDeliverExternal);

                if (externalUser != null)
                {
                    BuildEmailDetails(externalUser, context);
                }
            }
            else
            {
                var user = GetEmailUserDetailsForInteralUser(recipient, context);

                if (user != null)
                {
                    BuildEmailDetails(user, context);
                    //check if recipient has PrimaryGroup with BrandingPreset and update email logo
                    var userBrandingPresetInfo = (from ugu in context.UserGroupUsers
                                                  join bprug in context.BrandingPresetUserGroups on ugu.UserGroup equals bprug.UserGroup
                                                  join bpr in context.BrandingPresets on bprug.BrandingPreset equals bpr.ID
                                                  where ugu.IsPrimary && ugu.User == recipient
                                                  select new
                                                  {
                                                      brandingPresetGuid = bpr.Guid,
                                                      emailLogo = bpr.EmailLogoPath
                                                  }).FirstOrDefault();

                    //set user email logo from branding preset if exist
                    if (userBrandingPresetInfo != null && !string.IsNullOrEmpty(userBrandingPresetInfo.emailLogo))
                    {
                        var emailLogoPath = userBrandingPresetInfo.brandingPresetGuid + "/" + userBrandingPresetInfo.emailLogo;
                        _emailDetails.EmailLogoPath = User.GetUserEmailLogoPath(user.Account, emailLogoPath, context);
                    }
                }
            }


            if (string.IsNullOrEmpty(_emailDetails.EmailLogoPath))
            {
                _emailDetails.EmailLogoPath = Account.GetLogoPath(_emailDetails.AccountAltId, Account.LogoType.EmailLogo, context); 
            }

            _emailDetails.ManageNotificationAction = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + _emailDetails.AccountUrl + "/PersonalSettings/Notifications";
        }

        /// <summary>
        /// Get email user details for external user
        /// </summary>
        /// <param name="recipient">Email recipient id</param>
        /// <param name="context">Database context</param>
        /// <param name="isDeliverExternal">Is user from Deliver</param>
        /// <returns></returns>
        private EmailUserDetails GetEmailUserDetailsForExternalUser(int recipient, GMGColorContext context, bool isDeliverExternal = false)
        {
            return isDeliverExternal ? (from dec in context.DeliverExternalCollaborators
                                        join ac in context.Accounts on dec.Account equals ac.ID
                                        where dec.ID == recipient
                                        select
                                            new EmailUserDetails
                                            {
                                                AccountName = ac.Name,
                                                AccountUrl = (ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain),
                                                AccountId = dec.Account,
                                                CustomFromName = ac.CustomFromName,
                                                CustomFromAddress = ac.CustomFromAddress,
                                                GivenName = dec.GivenName,
                                                FamilyName = dec.FamilyName,
                                                EmailAddress = dec.EmailAddress,
                                                IsDeleted = false,
                                                IsRemoveAllGMGCollaborateBranding = ac.IsRemoveAllGMGCollaborateBranding,
                                                IsExternalUser = true,
                                            }).FirstOrDefault()
                                        :
                                        (from ec in context.ExternalCollaborators
                                            join ac in context.Accounts on ec.Account equals ac.ID
                                            where ec.ID == recipient
                                            select
                                                new EmailUserDetails
                                                {
                                                    AccountName = ac.Name,
                                                    AccountUrl = (ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain),
                                                    AccountId = ec.Account,
                                                    CustomFromName = ac.CustomFromName,
                                                    CustomFromAddress = ac.CustomFromAddress,
                                                    GivenName = ec.GivenName,
                                                    FamilyName = ec.FamilyName,
                                                    EmailAddress = ec.EmailAddress,
                                                    IsDeleted = ec.IsDeleted,
                                                    IsRemoveAllGMGCollaborateBranding = ac.IsRemoveAllGMGCollaborateBranding,
                                                    IsExternalUser = true,
                                                }).FirstOrDefault();
        }

        /// <summary>
        /// Get email user details for interal user
        /// </summary>
        /// <param name="recipient">Email recipient id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        private EmailUserDetails GetEmailUserDetailsForInteralUser(int recipient, GMGColorContext context)
        {
            return (from u in context.Users
             join ac in context.Accounts on u.Account equals ac.ID
             where u.ID == recipient
             select new EmailUserDetails
             {
                 GivenName = u.GivenName,
                 FamilyName = u.FamilyName,
                 EmailAddress = u.EmailAddress,
                 Account = ac,
                 AccountId = ac.ID,
                 IsRemoveAllGMGCollaborateBranding = ac.IsRemoveAllGMGCollaborateBranding,
                 IsExternalUser = false
             }).FirstOrDefault();
        }

        /// <summary>
        /// Configure email details, fromEmail and accountId, for custom smtp
        /// </summary>
        /// <param name="emailUserDetails">The user details used to create a email item</param>
        /// <param name="context">Database context</param>
        private void ConfigureEmailDetailsForCustomSmtpServer(EmailUserDetails emailUserDetails, GMGColorContext context)
        {
            const string customSmptKey = "CustomSmtpServer";

            var customSmtpServer = context.AccountSettings.Where(ac => ac.Account == emailUserDetails.AccountId && ac.Name == customSmptKey).FirstOrDefault();
            if (customSmtpServer != null)
            {
                var settingsObj = customSmtpServer.Value.ToObject<GMGColorDAL.CustomModels.CustomSMTPServerViewModel>();
                if (settingsObj.IsEnabled)
                {
                    _emailDetails.FromEmail = settingsObj.EmailAddress;
                    _emailDetails.AccountId = emailUserDetails.AccountId;
                }
                else
                {
                    SetFromEmailToDefaultValue(emailUserDetails.AccountId, context);
                    _emailDetails.AccountId = null;
                }
            }
            else
            {
                SetFromEmailToDefaultValue(emailUserDetails.AccountId, context);
            }
        }

        /// <summary>
        /// Build the details for the email db item
        /// </summary>
        /// <param name="emailUserDetails">The user details used to create a email item</param>
        /// <param name="context">Database context</param>
        private void BuildEmailDetails(EmailUserDetails emailUserDetails, GMGColorContext context)
        {
            if (emailUserDetails.IsExternalUser)
            {
                _emailDetails.FromName = (!string.IsNullOrEmpty(emailUserDetails.CustomFromName)) ? emailUserDetails.CustomFromName : GMGColorConfiguration.AppConfiguration.EmailSystemFromName;

                ConfigureEmailDetailsForCustomSmtpServer(emailUserDetails, context);

                _emailDetails.ToName = emailUserDetails.GivenName + " " + emailUserDetails.FamilyName;
                _emailDetails.ToEmail = emailUserDetails.EmailAddress;
                _emailDetails.AccountName = emailUserDetails.AccountName;
                _emailDetails.AccountUrl = emailUserDetails.AccountUrl;
                _emailDetails.IsRecipientDeleted = emailUserDetails.IsDeleted;
                _emailDetails.IsRemoveAllGMGCollaborateBranding = emailUserDetails.IsRemoveAllGMGCollaborateBranding;
                _emailDetails.AccountAltId = emailUserDetails.AccountId;
            }
            else
            {
                _emailDetails.FromName = (!string.IsNullOrEmpty(emailUserDetails.Account.CustomFromName)) 
                                            ? emailUserDetails.Account.CustomFromName 
                                            : GMGColorConfiguration.AppConfiguration.EmailSystemFromName;

                ConfigureEmailDetailsForCustomSmtpServer(emailUserDetails, context);

                _emailDetails.ToName = emailUserDetails.GivenName + " " + emailUserDetails.FamilyName;
                _emailDetails.ToEmail = emailUserDetails.EmailAddress;
                _emailDetails.AccountName = emailUserDetails.Account.Name;
                _emailDetails.AccountUrl = (emailUserDetails.Account.IsCustomDomainActive ? emailUserDetails.Account.CustomDomain : emailUserDetails.Account.Domain);
                _emailDetails.IsRemoveAllGMGCollaborateBranding = emailUserDetails.IsRemoveAllGMGCollaborateBranding;
                _emailDetails.AccountAltId = emailUserDetails.AccountId;
            }
        }


        /// <summary>
        /// Set the fromEmail to Account default
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="context">Database context</param>
        private void SetFromEmailToDefaultValue(int accountId, GMGColorContext context)
        {
            var acc = context.Accounts.Where(ac => ac.ID == accountId).FirstOrDefault();
            if (acc != null)
            {
                _emailDetails.FromEmail = (!string.IsNullOrEmpty(acc.CustomFromAddress))
                                                ? acc.CustomFromAddress
                                                : GMGColorConfiguration.AppConfiguration.EmailSystemFromAddress;
            }
        }
    }

    public class NotificationGroup
    {
        public string EventGroupKey { get; set; }
        public Notification Notifications { get; set; }
        public NotificationItem NotificationItem { get; set; }
    }
}
