﻿using GMGColorDAL;

namespace GMGColorNotificationService
{
    public class EmailUserDetails
    {
        public bool IsExternalUser { get; set; }
        public string AccountName { get; set; }
        public string AccountUrl { get; set; }
        public int AccountId { get; set; }
        public string CustomFromName { get; set; }
        public string CustomFromAddress { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsRemoveAllGMGCollaborateBranding { get; set; }
        public Account Account { get; set; }
    }
}
