﻿using GMG.CoZone.Common;
using GMGColorNotificationService.CreateNotifications.Creators;
using System;

namespace GMGColorNotificationService.CreateNotifications
{
	public class NotificationsCreator: INotificationCreator
	{
		private static INotificationCreator _notificationsCreator = null;
		public static INotificationCreator getInstance()
		{
			if (_notificationsCreator == null)
			{
				_notificationsCreator = new NotificationsCreator();
			}
			return _notificationsCreator;
		}

		private INotificationCreator[] creators;
		private NotificationsCreator()
		{
			var notificationTypeValues = Enum.GetValues(typeof(NotificationType));
			creators = new INotificationCreator[notificationTypeValues.Length];
			foreach (NotificationType notificationType in notificationTypeValues)
			{
				creators[(int)notificationType] = new NotImplementedCreator();
			}

			creators[(int)NotificationType.ApprovalWasShared] = new ApprovalWasSharedCreator();
			creators[(int)NotificationType.NewVersionWasCreated] = new NewVersionWasCreatedCreator();
			creators[(int)NotificationType.NewApprovalAdded] = new NewApprovalAddedCreator();
            creators[(int)NotificationType.NewFileTransferAdded] = new NewFileTransferAddedCreator();
            creators[(int)NotificationType.FileTransferDownloaded] = new FileTransferDownloadedCreator();

        }

		public Notification Create(TempNotificationItem tempNotificationItem)
		{
			return creators[(int)tempNotificationItem.NotificationType].Create(tempNotificationItem);
		}
	}
}
