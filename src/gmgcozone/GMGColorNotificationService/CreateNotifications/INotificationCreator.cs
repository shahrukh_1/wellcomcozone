﻿using GMG.CoZone.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMGColorNotificationService.CreateNotifications
{
	public interface INotificationCreator
	{
		Notification Create(TempNotificationItem tempNotificationItem);
	}
}
