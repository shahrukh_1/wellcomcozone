﻿using GMG.CoZone.Common;


namespace GMGColorNotificationService.CreateNotifications.Creators
{
    class NewFileTransferAddedCreator:INotificationCreator
    {
        public Notification Create(TempNotificationItem tempNotificationItem)
        {
            return new NewFileTransfer()
            {
                EventCreator = tempNotificationItem.EventCreator,
                InternalRecipient = tempNotificationItem.InternalRecipient,
                ExternalRecipient = tempNotificationItem.ExternalRecipient,
                ApprovalsIds = tempNotificationItem.ApprovalsIds.ToArray(),
                OptionalMessage = tempNotificationItem.OptionalMessage
            };
        }
    }
}
