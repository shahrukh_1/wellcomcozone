﻿
using GMG.CoZone.Common;


namespace GMGColorNotificationService.CreateNotifications.Creators
{
    class FileTransferDownloadedCreator : INotificationCreator
    {
        public Notification Create(TempNotificationItem tempNotificationItem)
        {
            return new FileTransferDownloaded()
            {
                EventCreator = tempNotificationItem.EventCreator,
                InternalRecipient = tempNotificationItem.InternalRecipient,
                ExternalRecipient = tempNotificationItem.ExternalRecipient,
                ApprovalsIds = tempNotificationItem.ApprovalsIds.ToArray(),
                OptionalMessage = tempNotificationItem.OptionalMessage
            };
        }
    }
}
