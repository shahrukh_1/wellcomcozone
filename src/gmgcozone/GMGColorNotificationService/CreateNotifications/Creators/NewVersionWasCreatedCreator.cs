﻿using GMG.CoZone.Common;

namespace GMGColorNotificationService.CreateNotifications.Creators
{
	public class NewVersionWasCreatedCreator : INotificationCreator
	{
		public Notification Create(TempNotificationItem tempNotificationItem)
		{
			return new NewVersionWasCreated()
			{
				EventCreator = tempNotificationItem.EventCreator,
				InternalRecipient = tempNotificationItem.InternalRecipient,
				ExternalRecipient = tempNotificationItem.ExternalRecipient,
				ApprovalsIds = tempNotificationItem.ApprovalsIds.ToArray(),
				OptionalMessage = tempNotificationItem.OptionalMessage
			};
		}
	}
}
