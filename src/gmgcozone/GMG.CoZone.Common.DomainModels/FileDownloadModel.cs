﻿using System.Collections.Generic;

namespace GMG.CoZone.Common.DomainModels
{
    public class FileDownloadModel
    {
        public int AccountId { get; set; }
        public DownloadVersionTypes DownloadVersionTypes { get; set; }
        public DownloadFileTypes DownloadFileTypes { get; set; }
        public AppModule ApplicationModule { get; set; }
        public List<int> FileIds { get; set; }
        public List<int> FolderIds { get; set; }
        public bool HasAccessToFtp { get; set; }
        public int SelectedPresetId { get; set; } = 0;
        public List<KeyValuePair<int, string>> AccountPressets { get; set; }
        public bool ShowAllFilesToAdmins { get; set; }
    }
}
