﻿namespace GMG.CoZone.Common.DomainModels
{
    public class CustomSmtpTestConnectionModel
    {
        public string MessageGuid { get; set; }
        public int AccountId { get; set; }
        public CustomSmtpTestModel TestData { get; set; }

        public CustomSmtpTestConnectionModel()
        {
            TestData = new CustomSmtpTestModel();
        }
    }
}
