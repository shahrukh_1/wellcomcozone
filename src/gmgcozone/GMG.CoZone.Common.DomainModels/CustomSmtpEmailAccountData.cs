﻿namespace GMG.CoZone.Common.DomainModels
{
    public class CustomSmtpEmailAccountData
    {
        public string EmailLogoPath { get; set; }
        public string UserLogoPath { get; set; }
        public string AccountName { get; set; }
        public string Domain { get; set; }
        public bool IsSecureConnection { get; set; }
    }
}
