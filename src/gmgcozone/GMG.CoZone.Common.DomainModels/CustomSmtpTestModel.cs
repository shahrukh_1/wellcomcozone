﻿namespace GMG.CoZone.Common.DomainModels
{
    public class CustomSmtpTestModel
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnabledSsl { get; set; }
        public string SenderEmail { get; set; }
        public string RecipientEmail { get; set; }
        public string Password { get; set; }
        public CustomSmtpEmailAccountData EmailAccountData { get; set; }

        public CustomSmtpTestModel()
        {
            EmailAccountData = new CustomSmtpEmailAccountData();
        }
    }
}
