﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Common.DomainModels
{
    public class FtpPresetJobDownloadModel
    {
        public int ID { get; set; }
        public int Preset { get; set; }
        public int? ApprovalJob { get; set; }
        public int? DeliverJob { get; set; }
        public string GroupKey { get; set; }
        public bool? Status { get; set; }
        public string StatusDescription { get; set; }
    }
}
