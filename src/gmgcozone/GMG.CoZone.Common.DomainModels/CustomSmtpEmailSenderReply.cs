﻿namespace GMG.CoZone.Common.DomainModels
{
    public class CustomSmtpEmailSenderReply
    {
        public int AccountId { get; set; }
        public string MessageGuid { get; set; }
        public string Error { get; set; }
    }
}
