﻿namespace GMG.CoZone.Common.DomainModels
{
    public class ApprovalProcessingModel
    {
        public int ID { get; set; }
        public string ScreenshotUrl { get; set; }
        public int? WebPageSnapshotDelay { get; set; }
        public bool RestrictedZoom { get; set; }
        public string ProcessingInProgress { get; set; }
        public bool JobIsHighRes { get; set; } = false;
        public string SessionGuid { get; set; }
    }
}
