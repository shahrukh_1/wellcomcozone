﻿namespace GMG.CoZone.Common.DomainModels
{
    public class ActionConfirmationModel
    {
        public string ModalTitle { get; set; }
        public string ModalText { get; set; }
        public string LeaveText { get; set; }
        public string StayText { get; set; }
    }
}
