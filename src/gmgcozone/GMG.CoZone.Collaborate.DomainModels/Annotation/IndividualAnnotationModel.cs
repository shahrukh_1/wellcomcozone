﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Collaborate.DomainModels.Annotation
{
    public class IndividualAnnotationModel
    {
        public int ID { get; set; }
        public int? ImageWidth { get; set; }
        public int? ImageHeight { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public List<string> SVG { get; set; }
        public string Comment { get; set; }
        public List<string> comment { get; set; }
        public string CreatorDisplayName { get; set; }
        public string CommentType { get; set; }
        public string AnnotatedDate { get; set; }
        public DateTime UTCCreatedDate { get; set; }
        public int AnnotationIndex { get; set; }
        public List<AnnotationReplyModel> Replies { get; set; }
        public string PhaseName { get; set; }
        public int AnnotatedOnImageWidth { get; set; }
        public List<string> AnnotationAttachment { get; set; }
        public List<AnnotationChecklistModel> AnnotationChecklist { get; set; }
        public int? AnnotationTimeFrame { get; set; }
        public bool IsUserStatusInactive { get; set; }
    }
}
