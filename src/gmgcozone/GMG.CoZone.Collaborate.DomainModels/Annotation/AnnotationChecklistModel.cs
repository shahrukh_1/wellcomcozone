﻿using System;

namespace GMG.CoZone.Collaborate.DomainModels.Annotation
{
    public class AnnotationChecklistModel
    {
        public string Checklist { get; set; }
        public string ChecklistItem { get; set; }
        public string TextValue { get; set; }

    }
}
