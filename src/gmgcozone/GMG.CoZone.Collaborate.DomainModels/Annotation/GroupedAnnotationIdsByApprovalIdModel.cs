﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Collaborate.DomainModels.Annotation
{
    public class GroupedAnnotationIdsByApprovalIdModel : List<int>, IGrouping<int, int>
    {
        public int Key
        {
            get;
            set;
        }
        public IEnumerable<int> Annotations
        {
            set
            {
                this.AddRange(value);
            }
        }
    }
}
