﻿using System;
using System.Collections.Generic;

namespace GMG.CoZone.Collaborate.DomainModels.Annotation
{
    public class AnnotationsModel
    {
        public int Approval { get; set; }
        public IEnumerable<DateTime> NotViewed { get; set; }
    }
}
