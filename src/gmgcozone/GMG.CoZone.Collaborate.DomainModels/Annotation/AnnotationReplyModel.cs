﻿using System;
using System.Collections.Generic;

namespace GMG.CoZone.Collaborate.DomainModels.Annotation
{
    public class AnnotationReplyModel
    {
        public string Comment { get; set; }
        public List<string> comment { get; set; }
        public DateTime UTCReplyDate { get; set; }
        public string AnnotatedDate { get; set; }
        public string CreatorDisplayName { get; set; }
        public string PhaseName { get; set; }
        public bool IsUserStatusInactive { get; set; }
    }
}
