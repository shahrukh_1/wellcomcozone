﻿namespace GMG.CoZone.Collaborate.DomainModels
{
    public class CollaboratorSoadModel
    {
        public int Approval { get; set; }
        public int Collaborator { get; set; }
        public int? Phase { get; set; }
        public bool IsExternal { get; set; }
        public string ProgressStateClass { get; set; }
    }
}
