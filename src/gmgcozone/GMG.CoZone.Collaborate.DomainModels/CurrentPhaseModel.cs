﻿namespace GMG.CoZone.Collaborate.DomainModels
{
    public class CurrentPhaseModel
    {
        public int Position { get; set; }
        public int ApprovalJobWorkflow { get; set; }
    }
}
