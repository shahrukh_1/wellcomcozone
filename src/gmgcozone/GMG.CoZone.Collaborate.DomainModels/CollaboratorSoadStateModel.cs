﻿namespace GMG.CoZone.Collaborate.DomainModels
{
    public class CollaboratorSoadStateModel : ApprovalSoadStateModel
    {
        public int CollaboratorId { get; set; }
        public bool IsExternal { get; set; }
    }
}
