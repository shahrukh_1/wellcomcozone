﻿using System;
using System.Collections.Generic;

namespace GMG.CoZone.Collaborate.Models
{
    public class AnnotationsModel
    {
        public int Approval { get; set; }
        public IEnumerable<DateTime> NotViewed { get; set; }
    }
}
