﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalInfoModel
    {
        public string Guid { get; set; }
        public string Region { get; set; }
        public string Domain { get; set; }
    }
}
