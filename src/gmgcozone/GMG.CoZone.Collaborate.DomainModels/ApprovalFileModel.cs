﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalFileModel
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string FileName { get; set; }
    }
}
