﻿namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalSoadStateModel
    {
        public int Approval { get; set; }
        public int? Phase { get; set; }
        public int ProgressState { get; set; }
    }
}
