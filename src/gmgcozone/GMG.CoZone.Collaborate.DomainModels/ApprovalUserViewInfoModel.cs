﻿using System;

namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalUserViewInfoModel
    {
        public int Approval { get; set; }
        public int User { get; set; }
        public DateTime ViewedDate { get; set; }
        public int? phase { get; set; }
        public int? externalUser { get; set; }
    }
}
