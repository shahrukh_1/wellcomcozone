﻿using System;

namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalLastOpenedDate
    {
        public int Approval { get; set; }
        public DateTime ViewedDate { get; set; }
    }
}
