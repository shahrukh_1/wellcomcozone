﻿using System.Collections.Generic;

namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalCollaboratorsAndAnnotationsModel
    {
        public List<int> InternalCollaborators { get; set; }
        public List<int> ExternalCollaborators { get; set; }
        public List<int> InternalAnnotationsCreators { get; set; }
        public List<int> ExternalAnnotationsCreators { get; set; }

        public ApprovalCollaboratorsAndAnnotationsModel()
        {
            InternalAnnotationsCreators = new List<int>();
            ExternalAnnotationsCreators = new List<int>();
            InternalCollaborators = new List<int>();
            ExternalCollaborators = new List<int>();
        }
    }
}
