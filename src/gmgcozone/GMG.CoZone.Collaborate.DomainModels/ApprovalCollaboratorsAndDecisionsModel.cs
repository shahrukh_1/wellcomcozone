﻿using System.Collections.Generic;

namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalCollaboratorsAndDecisionsModel
    {
        public List<int> InternalCollaboratorsDecision { get; set; }
        public List<int> ExternalCollaboratorsDecision { get; set; }
        public List<int> InternalCollaborators { get; set; }
        public List<int> ExternalCollaborators { get; set; }

        public ApprovalCollaboratorsAndDecisionsModel()
        {
            InternalCollaboratorsDecision = new List<int>();
            ExternalCollaboratorsDecision = new List<int>();
            InternalCollaborators = new List<int>();
            ExternalCollaborators = new List<int>();
        }
    }
}
