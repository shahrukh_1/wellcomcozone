﻿namespace GMG.CoZone.Collaborate.DomainModels
{
    public class ApprovalSoadModel
    {
        public int Approval { get; set; }
        public int? Phase { get; set; }
        public string ProgressStateClass { get; set; }
    }
}
