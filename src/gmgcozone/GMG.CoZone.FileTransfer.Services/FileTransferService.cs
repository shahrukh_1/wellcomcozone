﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMG.CoZone.Collaborate.DomainModels;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.FileTransfer.DomainModels;
using GMG.CoZone.FileTransfer.Interfaces;
using GMG.CoZone.Repositories.FileTransfer;
using GMG.CoZone.Repositories.Interfaces;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace GMG.CoZone.FileTransfer.Services
{
    public class FileTransferService: IFileTransferService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FileTransferService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int SaveTransfer(FileTransferModel model)
        {
            int filleTransferId=_unitOfWork.FileTransferRepository.SaveTransfer(model);
            //_unitOfWork.Save();
            return filleTransferId;
        }

        public List<int> GetExternalUsersForTransfer(int fileTransferId)
        {
            List<int> ids= _unitOfWork.FileTransferRepository.GetExternalUsersForTransfer(fileTransferId);
            return ids;
        }

        public void DeleteFileTransfer(int id)
        {
            _unitOfWork.FileTransferRepository.SoftDeleteFileTransfer(id);
            _unitOfWork.Save();
        }

        public Dictionary<int, string> GetDownloadPathsForApprovalsFromFolders(List<int> folderIds, int loggedUserId ,bool adminCanViewAllApprovals)
        {
            var approvalFileModels = GetApprovalsToDownloadFromFolder(folderIds, loggedUserId,adminCanViewAllApprovals);

            //TODO this line must be uncommented after send notificcation functionality will be refatored. The return type of this method will be only List<string>. The logic bellow should be removed
            //return GetCollaborateJobPaths(approvalFileModels);

            return approvalFileModels.ToDictionary(approvalFileModel => approvalFileModel.Id, approvalFileModel => "approvals/" + approvalFileModel.Guid + "/" + approvalFileModel.FileName);
        }

        private List<ApprovalFileModel> GetApprovalsToDownloadFromFolder(List<int> folderIds, int loggedUserId,  bool adminCanViewAllApprovals)
        {
            List<ApprovalFileModel> approvalFileModels = _unitOfWork.FileTransferRepository.GetMaxApprovalVersionsByFolderIds(folderIds, loggedUserId, adminCanViewAllApprovals);
                                                                                            
            return approvalFileModels;
        }

        public void SetDownloadDateForExternal(int fileTransferExternalId)
        {
            _unitOfWork.FileTransferRepository.SetDownloadDateForExternal(fileTransferExternalId);
            _unitOfWork.Save();
        }

        public void SetDownloadDateForInternal(int fileTransferInternalId)
        {
            _unitOfWork.FileTransferRepository.SetDownloadDateForInternal(fileTransferInternalId);
            _unitOfWork.Save();
        }
        
       public void SendDownloadNotification(int fileTransferId, int internalrecipient, int eventCreatorUserId, DbContextBL context,bool isExternal)
        {
            string optionalMessage = String.Empty;
            var loggedUser = User.GetObject(internalrecipient, context);
            var loggedAccount = loggedUser.Account;
            ApprovalBL.SendFileTransferDownloadedNotifications(fileTransferId, internalrecipient, eventCreatorUserId, loggedAccount, loggedUser, optionalMessage, isExternal, context);
        }
    }
}
