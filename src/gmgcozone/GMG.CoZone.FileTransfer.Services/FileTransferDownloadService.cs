﻿
using System;
using System.Collections.Generic;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Repositories.Interfaces;
using GMGColorDAL;

namespace GMG.CoZone.FileTransfer.Services
{
    public class FileTransferDownloadService: IFileTransferDownloadService
    {
        private readonly IUnitOfWork _unitOfWork;
        

        public FileTransferDownloadService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            
        }
       

        public Dictionary<int, string> GetDownloadPaths(FileDownloadModel model, int loggedUserId)
        {
            // var relativePaths = new List<string>();
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            try
            {

                var transferFileModels = _unitOfWork.FileTransferRepository.GetFileTransfersByIds(model.FileIds, model.AccountId);

                if (transferFileModels.Count > 0) { 
                    foreach (var transferFileModel in transferFileModels)
                    {
                        dictionary.Add(transferFileModel.ID, "filetransfer/" + transferFileModel.Guid + "/" + transferFileModel.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, model.AccountId, "Logged User: {0}, Error in Base Control -> FileTransferService -> GetDownloadPaths Method. Exception : {1}", loggedUserId, ex.Message);
            }

            return dictionary;
        }

        public void SetFtpDownloadPaths(FileDownloadModel model, int loggedUserId)
        {
            string groupKey = null;
        }
    }
}
