﻿using System;
using System.Collections.Generic;
using GMG.CoZone.Common;

namespace GMG.CoZone.Collaborate.UI
{
    [Serializable]
    public class FileDownloadViewModel
    {
        public int AccountId { get; set; }
        public DownloadVersionTypes DownloadVersionTypes { get; set; }
        public DownloadFileTypes DownloadFileTypes { get; set; }
        public AppModule ApplicationModule { get; set; }
        public int Preset { get; set; }
        public string FileIds { get; set; }
        public string FolderIds { get; set; }
        public bool HasAccessToFtp { get; set; }
        public int SelectedPresetId { get; set; } = 0;
        public List<KeyValuePair<int, string>> AccountPressets { get; set; }
        public bool ShowAllFilesToAdmins { get; set; }
        public bool IsZipFileWhenDownloadingEnabled { get; set; }
    }
}
