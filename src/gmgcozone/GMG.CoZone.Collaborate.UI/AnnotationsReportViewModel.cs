﻿namespace GMG.CoZone.Collaborate.UI
{
    public class AnnotationsReportViewModel
    {
        public string ReportType { get; set; }
        public int[] GroupedAnnotations { get; set; }
        public int[] AllAnnotations { get; set; }
    }
}
