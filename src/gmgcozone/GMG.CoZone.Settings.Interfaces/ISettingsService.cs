﻿using GMG.CoZone.Settings.Interfaces.Settings;

namespace GMG.CoZone.Settings.Interfaces
{
    /// <summary>
    /// Interface used to set account level settings
    /// </summary>
    public interface ISettingsService
    {
        /// <summary>
        /// Handle Account Sso settings changes
        /// </summary>
        ISsoSettings SsoSettings { get; set; }

        /// <summary>
        /// Handle Accoun Smtp settings changes
        /// </summary>
        ISmtpSettings SmtpSettings { get; set; }
    }
}
