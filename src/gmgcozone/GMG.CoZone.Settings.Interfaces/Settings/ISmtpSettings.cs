﻿using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Settings.DomainModels;

namespace GMG.CoZone.Settings.Interfaces.Settings
{
    /// <summary>
    /// SMTP settings interface
    /// </summary>
    public interface ISmtpSettings
    {
        /// <summary>
        /// Get the SMTP view model settings
        /// </summary>
        /// <param name="loggedAccount">Account Id</param>
        /// <returns>SMTP settings view model</returns>
        SmtpSettingsModel Get(int loggedAccount);

        /// <summary>
        /// Get encrypted password
        /// </summary>
        /// <param name="loggedAccount">Account Id</param>
        /// <returns></returns>
        string GetPassword(int loggedAccount);

        /// <summary>
        /// Add or Update SMPT settings
        /// </summary>
        /// <param name="settings">View model settings</param>
        /// <param name="loggedAccount">Account Id</param>
        /// <param name="loggedAccountGuid">Account GUId</param>
        /// <param name="valueTypeKey">Settings Key</param>
        void AddUpdate(SmtpSettingsModel settings, int loggedAccount, string loggedAccountGuid, string valueTypeKey);

        /// <summary>
        /// Delete SMTP settings
        /// </summary>
        /// <param name="loggedAccount">Account Id</param>
        void Disable(int loggedAccount);

        /// <summary>
        /// Test SMPT settings using a MSMQ queue
        /// </summary>
        /// <param name="messageModel">The message body</param>
        /// <returns>Error string if connection is not successfull</returns>
        string TestConnection(CustomSmtpTestConnectionModel messageModel);
    }
}
