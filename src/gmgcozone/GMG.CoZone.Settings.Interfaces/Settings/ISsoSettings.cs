﻿using GMG.CoZone.Settings.DomainModels;
using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.Settings.Interfaces.Settings
{
    /// <summary>
    /// SSO settings interface
    /// </summary>
    public interface ISsoSettings
    {
        /// <summary>
        /// Get the account SSO setings
        /// </summary>
        /// <param name="loggedAccount">Account Id</param>
        /// <returns>SSO settings details</returns>
        SsoModel Get(int loggedAccount);

        /// <summary>
        /// Add or Update SSO account settings
        /// </summary>
        /// <param name="settings">View data</param>
        /// <param name="accountSettingsData">Account SSO data</param>
        void AddUpdate(SsoModel settings, AccountSettingsDataModel accountSettingsData);

        /// <summary>
        /// Delete the SSO account settings
        /// </summary>
        /// <param name="loggedAccount">Account Id</param>
        /// <param name="region">Account Region</param>
        /// <param name="accountGuid">Account Guid</param>
        void Disable(int loggedAccount, string region, string accountGuid);
    }
}
