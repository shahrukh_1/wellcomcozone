﻿namespace GMG.CoZone.Settings.Interfaces
{
    /// <summary>
    /// Interface for parsing SMTP connection queue messages
    /// </summary>
    public interface ISmtpQueueParser
    {
        string ParseConnectionReplyFromAWSQueue(string queueName, string messageGuid);
        string ParseConnectionReplyFromLocalQueue(string queueName, string messageGuid);
    }
}
