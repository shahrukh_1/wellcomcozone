$(document).ready(function () {
    var userColor = $('input#ProofStudioColor').attr('value') || $('input#objUsers_objEditUser_ProofStudioColor').attr('value')
        || $('input#objUsers_objNewUser_ProofStudioColor').attr('value') || $('input#objEditUser_ProofStudioColor').attr('value')
        || $('input#objNewUser_ProofStudioColor').attr('value');
    // create picker
    $('select[name="colorpicker-picker-list"]').simplecolorpicker({ picker: true, theme: 'regularfont' });
    // set user selected color
    $('select[name="colorpicker-picker-list"]').simplecolorpicker('selectColor', userColor);
    // attach event
    $('select[name="colorpicker-picker-list"]').on('change', function () {
        $('input#ProofStudioColor').attr('value', $('select[name="colorpicker-picker-list"]').val());
        $('input#objUsers_objEditUser_ProofStudioColor').attr('value', $('select[name="colorpicker-picker-list"]').val());
        $('input#objUsers_objNewUser_ProofStudioColor').attr('value', $('select[name="colorpicker-picker-list"]').val());
        $('input#objEditUser_ProofStudioColor').attr('value', $('select[name="colorpicker-picker-list"]').val());
        $('input#objNewUser_ProofStudioColor').attr('value', $('select[name="colorpicker-picker-list"]').val());
    });
});

