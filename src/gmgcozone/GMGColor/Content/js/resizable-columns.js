$(function () {
    var resizableTable = $("table.resizable");
    resizableTable.find('th:first').on('resize', function (event) {
        if (resizableTable.is(":visible")) {
            var th = $(this);
            $('.resizable-menu-separator').each(function (index, element) {
                element = $(element);
                element.css('margin-top', "1px");
                element.css('height', th.parents('tr:first').height());
            });
            if ($('th:first input').hasClass('DeliverAllCheckbox')) {
                $("table.resizable").find('tr th:first').css('width', '40px');
                $("table.resizable").find('tr th:last').css('width', '40px');
                $('.CRG:first .CRZ').css('left', '41px');
            }

            if ($('td:first').hasClass('jobThumbCol')) {
                $("table.resizable").find('tr th:first').css('width', '85px');
                $("table.resizable").find('tr th:last').css('width', '40px');
                $('.CRG:first .CRZ').css('left', '11px');
            }

            if ($('th:first input').hasClass('FileTransferCheckbox')) {
                $("table.resizable").find('tr th:first').css('width', '85px');
                $("table.resizable").find('tr th:last').css('width', '100%');
                $('.CRG:first .CRZ').css('left', '11px');
            }
        }
    });
    resizableTable.colResizable({
        liveDrag: true,
        postbackSafe: true,
        draggingClass: 'aaaa',
        minWidth: 50,
        gripInnerHtml: "<div class='resizable-menu-separator'></div>",
        onResize: function (e) {
            resizableTable.find('th:first').trigger('resize');
        }
    });

    $('.CRG:first .CRZ, .CRG:last .CRZ').mousedown(function (e) {
        if ($('th:first input').hasClass('DeliverAllCheckbox') || $('td:first').hasClass('jobThumbCol') || $('td:first').hasClass('FileTransferCheckbox') ||
            $('td:first').hasClass('allProfilesSelected')) {
            e.preventDefault();
            return false;
        }
        return true;
    });

    $('.CRG:first .CRZ, .CRG:last .CRZ').on("hover mousedown", function (e) {
        if (resizableTable.attr('id') != 'approvals-index') {
            $(this).css("cursor", "e-resize");
            //return false;
        }
        return true;
    });
    resizableTable.find('th:first').trigger('resize');
});

