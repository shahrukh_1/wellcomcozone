﻿$(document).ready(function () {
    //Because the element is created dynamically, the event handler fires each time it has been bound to the element.
    //So in order to avoid this first unbind the handler and then rebind again.
    $(".up, .down").off('click').on('click', function (e) {
        var row = $(this).parents("tr:first");
        var rowChanged = 0;

        var id = $(this).parents("ul:first").attr('phase'); //The id of the row from which click event is triggered
        var tableId = $(this).parents("table:first").attr('id');

        var isUp = $(this).is(".up");
        if (isUp) {
            rowChanged = row.prev().find("ul:first").attr('phase');
        } else {
            rowChanged = row.next().find("ul:first").attr('phase');
        }
        //save the new positions of both rows in database
        $.ajax({
            type: "POST",
            url: "/Settings/ChangeChecklistItemPosition",
            data: { sourceChecklistId: id, destinationChecklistId: rowChanged, __RequestVerificationToken: $("#formMain input").val() },
            dataType: "json",
            cache: false,
            success: function (canChangePosition) {
                if (canChangePosition) {
                    if (isUp) {
                        row.insertBefore(row.prev());
                    } else {
                        row.insertAfter(row.next());
                    }
                    hideFirstAndLastArrow(tableId);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
            }
        });
    });
});

function hideFirstAndLastArrow(tblItemId) {
    $('#' + tblItemId + ' .up:first, #' + tblItemId + ' .down:last').css('display', 'none');
    $('#' + tblItemId + ' .up, #' + tblItemId + ' .down').not('.up:first, .down:last').css('display', 'block');
}



