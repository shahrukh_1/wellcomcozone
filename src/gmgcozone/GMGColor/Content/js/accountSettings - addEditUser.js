﻿var addEditMode, objToAppend;
var userData = {};
var emailWarning = document.getElementById('welcomEmailWarning');

$(document).ready(function () {
    //check wich object to append the error message to 
    if ($(".well").length) {
        objToAppend = $(".well");
    }
    else {
        objToAppend = $(".alert");
    }

    //check if is sso user
    if ($("#objUsers_objEditUser_objUser_IsSsoUser:checked").length > 0) {
        $("input[id$='HasPasswordEnabled']").attr("disabled", true);
    }

    //check if it is edit or new
    if ($("#objUsers_objNewUser_ModulePermisions_0__SelectedRole").length || $("#objNewUser_ModulePermisions_0__SelectedRole").length) {
        addEditMode = "objNewUser";
        objToAppend.after("<div class=\"alert\" id=\"permisionsWarning\">" + $("#userApplicationPermissionsWarning").val() + "</div>");
    }

    // EditUser disable Administration Role if he is not collaborate Administrator
    if ($('#objUsers_objEditUser_ModulePermisions_0__SelectedRole').val() != 3 || $('#objUsers_objEditUser_ModulePermisions_0__SelectedRole').val() == 'undefined') {
        $("#objUsers_objEditUser_ModulePermisions_4__SelectedRole").attr("disabled", true);
    }

    // AddUser disable Administration Role if he is not collaborate Administrator
    if ($('#objUsers_objNewUser_ModulePermisions_0__SelectedRole').val() != 3 || $('#objUsers_objNewUser_ModulePermisions_0__SelectedRole').val() == 'undefined') {
        $("#objUsers_objNewUser_ModulePermisions_4__SelectedRole").attr("disabled", true);
    }

 

    // AddUser disable ProjectFolder Role if he is not collaborate Administrator
    //if (!($('#drpCollaborate').val() == 3 || $('#drpCollaborate').val() == 4) || $('#drpCollaborate').val() == 'undefined') {
    //    $("#drpProjectFolders").attr("disabled", true);
    //}

    $("#formUserInformation .btn-primary").click(function () {
        if (!checkUserHasAccessToAtLeastOneModule()) {
            if (!$("#formUserInformation .row-fluid").find("#permisionsWarning").length) {
                objToAppend.after("<div class=\"alert\" id=\"permisionsWarning\">" + $("#userApplicationPermissionsWarning").val() + "</div>");
            }
            return false;
        }
    });
});

//check if the user has at least one application persmissions
function checkUserHasAccessToAtLeastOneModule() {
    for (var i = 0; i < 3; i++) {
        if ($("select[id*=_" + i + "__SelectedRole" + "]" + " option:selected").text() !== "NoAccess") {
            return true;
        }
    }
    return false;
}

$("input[id$='IsSsoUser']").on("change", function () {
    if ($(this).is(":checked")) {
        $(emailWarning).detach();
        $("#permisionsWarning").before("<div class=\"alert\" id=\"ssoWarning\">" + $("input[id$='ssoNamesWarning']").val() + "</div>");
        $("#formUserInformation .btn.btn-primary").text($("#btnSave").val());
        $("#objUsers_objNewUser_objUser_HasPasswordEnabled").attr("checked", false);
        $("#objUsers_objNewUser_objUser_HasPasswordEnabled").attr("disabled", true);

        RefreshFormInputs(this,
                         [$("input[id$='FirstName']"),
                          $("input[id$='LastName']"),
                          $("input[id$='HasPasswordEnabled']"),
                          $("input[id$='Password']"),
                          $("input[id$='ConfirmPassword']")], true);
    }
    else {
        $("#permisionsWarning").before($(emailWarning));
        $("#ssoWarning").detach();
        $("#objUsers_objNewUser_objUser_HasPasswordEnabled").attr("disabled", false);
        $("#formUserInformation .btn.btn-primary").text($("#btnSendInvite").val());

        RefreshFormInputs(this,
                   [$("input[id$='FirstName']"),
                    $("input[id$='LastName']"),
                    $("input[id$='HasPasswordEnabled']")],
                   false);
    }
});

$("input[id$='HasPasswordEnabled']").on("change", function () {
    if ($(this).is(":checked")) {
        RefreshFormInputs(this, [$("input[id$='Password']"), $("input[id$='ConfirmPassword']")], false);
        $(emailWarning).detach();
        $("#btnAddProfile").prop("value", $("#btnSave").val());
        if ($("#formUserInformation .btn.btn-primary").text().trim() !== $("#btnSaveChanges").val()) {
            $("#formUserInformation .btn.btn-primary").text($("#btnSave").val());
        }
    } else {
        RefreshFormInputs(this, [$("input[id$='Password']"), $("input[id$='ConfirmPassword']")], true);
        $("#permisionsWarning").before($(emailWarning));
        if ($("#formUserInformation .btn.btn-primary").text().trim() !== $("#btnSaveChanges").val()) {
            $("#formUserInformation .btn.btn-primary").text($("#btnSendInvite").val());   
        }
    }
});

// EditUser UserRole validation
$('#objUsers_objEditUser_ModulePermisions_0__SelectedRole').on('change', function () {

    if ($('#objUsers_objEditUser_ModulePermisions_0__SelectedRole').val() != 3){ 
        $('#objUsers_objEditUser_ModulePermisions_4__SelectedRole option[value="20"]').attr("selected", "selected");
        $("#objUsers_objEditUser_ModulePermisions_4__SelectedRole").attr("disabled", true);
    }
    else{
        $("#objUsers_objEditUser_ModulePermisions_4__SelectedRole").attr("disabled", false);
    }      
});

// NewUser UserRole validation
$('#objUsers_objNewUser_ModulePermisions_0__SelectedRole').on('change', function () {

    if ($('#objUsers_objNewUser_ModulePermisions_0__SelectedRole').val() != 3) {
        $('#objUsers_objNewUser_ModulePermisions_4__SelectedRole option[value="20"]').attr("selected", "selected");
        $("#objUsers_objNewUser_ModulePermisions_4__SelectedRole").attr("disabled", true);
    }
    else {
        $("#objUsers_objNewUser_ModulePermisions_4__SelectedRole").attr("disabled", false);
    }
});


//$('#drpCollaborate').on('change', function () {

//    if (!($('#drpCollaborate').val() == 3 || $('#drpCollaborate').val() == 4))   {
//        $('#drpCollaborate option[value="36"]').attr("selected", "selected");
//        $("#drpProjectFolders").attr("disabled", true);
//    }
//    else {
//        $("#drpProjectFolders").attr("disabled", false);
//    }
//});

function RefreshFormInputs(callerObj, elements, enableDisable) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].valid();
        if (callerObj.id.indexOf("IsSsoUser") !== -1 && elements[i].selector === "input[id$='HasPasswordEnabled']") {
            elements[i].attr("checked", false);
            elements[i].attr("disabled", enableDisable);
        } else {
            if (elements[i].selector === "input[id$='FirstName']" || elements[i].selector === "input[id$='LastName']") {
                continue;
            }
            if (elements[i].is(":checkbox")) {
                elements[i].attr("checked", enableDisable);
            } else {
                elements[i].val("");
            }
            elements[i].attr("disabled", enableDisable);
        }
    }
}