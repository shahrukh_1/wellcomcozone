/*global $:false */
/* PBD 2012 */
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Integrating Bootstrap Error styling with MVC�s Unobtrusive Error Validation
** http://www.braindonor.net/blog/integrating-bootstrap-error-styling-with-mvcs-unobtrusive-error-validation/381/
** Bug Fixed in jquery.validate.unobtrusive.js add following lines
** line: 41 - container.parents('div.control-group').addClass("error");
** line: 73 - container.parents('div.control-group').removeClass("error");
*/
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* MVC�s Unobtrusive Error Validation
** http://forums.asp.net/t/1716181.aspx/1
** Bug Fixed in jquery.validate.unobtrusive.js replace following line
** line : 287 - element = $(options.form).find(":input[name='" + fullOtherName + "']")[0];
*/
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* > IE9 Browsers doesn't have the some functionalities it self.*/

//Add global namespace to add functions in it
window.COZ = window.COZ || {};

/* Add the following code to add trim functionality to the string.*/
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}
/* Add the following code to add indexOf functionality to the Array.*/
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
          this[from] === elt)
                return from;
        }
        return -1;
    };
}
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
// Reset Model
function ResetModel() {
    $('.modal').each(function () {
        $(this).find('input:not(:checkbox)').each(function () {
            $(this).val('');

        });
        $(this).find('input:checkbox').each(function () {
            $(this).removeAttr('checked');
        });

        $(this).find('.error').each(function () {
            $(this).removeClass('error');
            $(this).find('.input-validation-error').each(function () {
                $(this).removeClass('input-validation-error');
                $(this).addClass('valid');
            });
            $(this).find('.field-validation-error').each(function () {
                $(this).removeClass('field-validation-error');
                $(this).addClass('field-validation-valid');
            });
        });

        $(".collaboratorshare-blocks .alert-error").each(function (i, v) {
            if (!$(this).hasClass("hide")) {
                $(this).addClass("hide");
            }
        });
    });
}

function ResetModelCustom() {
    $('.modal').each(function () {

        $(this).find("input[type='text']").each(function () {
            $(this).val('');
        });
        $(this).find("input[type='password']").each(function () {
            $(this).val('');
        });
        $(this).find("input[type='checkbox']").each(function () {
            $(this).removeAttr("checked");
        });

        // select the default text selection
        $(this).find("select").each(function () {
            $(this).val('');
        });

        // move modal-body scrolldown on top
        $(".modal .modal-body").scrollTop(0);

        $(this).find('.error').each(function () {
            $(this).removeClass('error');
            $(this).find('.input-validation-error').each(function () {
                $(this).removeClass('input-validation-error');
                $(this).addClass('valid');
            });
            $(this).find('.field-validation-error').each(function () {
                $(this).removeClass('field-validation-error');
                $(this).addClass('field-validation-valid');
            });
        });
    });
}

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*Initialize diryForms checking plugin
*/
$(document).ready(function () {
    $('form:not(.dirtyignore)').dirtyForms();
    $('a[href="javascript:void(0);"]').addClass($.DirtyForms.ignoreClass);
    
    $.DirtyForms.dialog = {
        // Typical Dirty Forms Properties and Methods
        fire: function (message, title) {
            $dlg = $('#dirtyFormsDialog');
            $dlg.modal({ backdrop: 'static', keyboard: false });
        },
        bind: function () {
            $('.dirty-continue').click(function (e) {
                $.DirtyForms.choiceContinue = true;
            });
            $dlg.bind('hidden.bs.modal', function (e) {
                $.DirtyForms.choiceCommit(e);
                $dlg.modal('hide');
            });
        },

        // Stashing (needed only if you have a form inside a modal dialog
        // as Bootstrap modal does not support stacking modal dialogs).
        stash: function () {
            return false;
        },
        refire: function () {
            return false;
        },

        // Selector for stashing the content of another dialog.
        selector: 'no-op'
    };
});
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Theme Change */
(function ($) {
    $(document).ready(function (event) {
        $('.colorscheme li').on('click', function (event) {
            if (event.target.id.indexOf("theme") !== -1 || 
                event.target.className == "icon-pencil" || 
                event.target.className == "btn edit-delete-advs" ||
                event.target.className.trim() == "styleswitch" ||
                event.target.nodeName == "DL") {
                $('.colorscheme li').removeClass('active');
                $('.colorscheme li input[type=radio]').attr('checked', false);
                $(this).addClass('active');
                $(this).children('input[type=radio]').attr('checked', true);
                $('form').dirtyForms('setDirty');

                //removeCustomClasses
                $(".navBarTheme").removeAttr("style");
                $("a").removeAttr("style");
                $("buttons").removeAttr("style");
                $("#footer").removeAttr("style");
            }
        });

        $('.colorscheme.styleswitch li').on('click', function () {
            switchStylestyle($(this).children('input[type=radio]').attr("rel"));
            return false;
        });
    });

    function switchStylestyle(styleName) {
        if (styleName != undefined) {
            var $img = $('#header a.brand img[src*="nologo-122px-33px"]');
            if ($img) {
                var src = 'http://' + document.domain + $img.attr('default');
                if (styleName != 'default') {
                    src = src.replace("nologo-122px-33px", "nologo-122px-33px-" + styleName);
                }
                $img.attr('src', src);
            }
            $('head link[rel*=stylesheet][title]').each(function (e) {
                this.disabled = true;
                if (this.getAttribute('title') == styleName) this.disabled = false;
            });
        }  
    }
})(jQuery);

$(document).ready(function () {
    $('.colorscheme li input[type=radio][checked=checked]').parents('li:first').addClass('active');
    if ($('.colorpallet').length > 0) {
        $('.colorpallet').colorpicker().on('changeColor', function (ev) {
            $(this).children('input[type=text]').val(ev.color.toHex().toUpperCase());
        });
    }
});
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Retun false if input[type=text], input[type=password] fields enter keydown event */

$(document).ready(function () {
    $('input[type=text], input[type=password]').keypress(function (e) {
        return ($(this).hasClass('cancel') || (e.keyCode != 13));
    });
});
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Show Model on user request */

if (this.modelDialog != '') {
    $('#' + this.modelDialog).modal({ backdrop: 'static', keyboard: false });
}
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Bind Submit Buttons to Show Waiting text(Saving... etc..) */

$(function () {
    $('body').on('click.button.data-api', '[data-loading-text][type=submit]', function (e) {
        var $btn = $(e.target);
        if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn');

        if ($btn.closest("form").valid()) {

            //workaround for chrome (if a button is disabled, chrome prevents form submission
            //see details: http://stackoverflow.com/questions/10788205/this-code-prevents-my-form-from-submitting-only-in-chrome-works-in-ie-and-fire)
            setTimeout(function() {
                $btn.button('loading');
            }, 0);
            
            var $imgLoading = $btn.attr("data-loading-image");
            if (typeof $imgLoading != "undefined" && $imgLoading !== false) {
                $("#" + $imgLoading).show();
            }
        }
    });
});
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Intiger Input Box */

$('.iintiger').keydown(function (e) {
    // Allow: backspace, delete, tab and escape
    if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 ||
    // Allow: Ctrl+A
    (e.keyCode == 65 && e.ctrlKey === true) ||
    // Allow: home, end, left, right
    (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
});
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Double Input Box */

$('.idouble').keydown(function (e) {
    // Allow: backspace, delete, tab and escape
    if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 ||
    // Allow: Ctrl+A
    (e.keyCode == 65 && e.ctrlKey === true) ||
    // Allow: home, end, left, right
    (e.keyCode >= 35 && e.keyCode <= 39) ||
    // Allow: dot and numberpad dot
    (e.keyCode == 110 || e.keyCode == 190)) {
        // let it happen, don't do anything
        return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
});
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

function applyGpp(isNewAccount) {
    var gpp = parseFloat(($('#txtGpp').val()) ? $('#txtGpp').val() : '0.00');
    if (isNaN(gpp)) {
        gpp = '0';
    }

    $('.table').each(function () {
        var colName = $($(this).children('thead').children('tr').children('th')[3])[0].innerHTML;
        var preValue = $($(this).children('thead').children('tr').children('th')[3])[0].innerHTML.split('(')[1];
        var currencySymble = $(this).find('label').attr('distprice').split('-')[0];
        $($(this).children('thead').children('tr').children('th')[3])[0].innerHTML = colName.replace(preValue, gpp + '%)');

        $(this).find('td.applygpp-price').each(function () {
            var price = parseFloat($(this).find('label').attr('distprice').split('-')[1]);
            calculateDiscountCost($(this).find('label')[1], price, gpp, currencySymble);
        });

        $(this).find('td.applygpp-proofprice').each(function () {
            var price = parseFloat($(this).find('label').attr('distprice').split(' ')[1]);
            calculateDiscountCost($(this).find('label')[1], price, gpp, currencySymble);
        });

    });

    if (isNewAccount) {
        $('.billing-plan').each(function () {
            //apply gpp for billingPrice.(dropdown list)
            var symble = $(this).attr('price').split('-')[0];
            var n = parseFloat($(this).attr('price').split('-')[1]);
            var distPrice = (n - n * (gpp / 100)).toFixed(2);
            var preValue = $(this).text().split('(')[1];
            var newValue = $(this).text().replace(preValue, (accounting.formatMoney(distPrice, symble + ' ') + ')'));
            $(this).text(newValue);
        });
    }
}

function calculateDiscountCost(label, price, gpp, currencySymble) {
    var discountedPrice = (price - price * (gpp / 100)).toFixed(2);
    var discountedPriceString = accounting.formatMoney(discountedPrice, currencySymble + ' ');
    $(label).text(discountedPriceString);
}

function setCpp(selected) {
    if ($(selected).attr('value') != "") {
        var price = parseFloat($(selected).attr('price').split('-')[1]);
        var currency = $(selected).attr('price').split('-')[0];
        var cpp = parseFloat($(selected).attr('cpp'));
        if (price > 0 && cpp > 0) {
            $('#dvCpp').show();
            $('#cpp')[0].innerHTML = accounting.formatMoney(cpp.toFixed(2), currency + ' ');
        }
        else {
            $('#dvCpp').hide();
        }
    }
    else {
        $('#dvCpp').hide();
    }
}

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* submit GET requests from Forms using Url.Action links without submitting the form (needed for grid action links ) */
$(document).on('click', "a[data-form-method='post']", function(event) {
            event.preventDefault();
            var element = $(this);
            var action = element.attr("href");
            element.closest("form").each(function() {
                var form = $(this);
                form.attr("action", action);
                form.submit();
            });
        });

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* Utilitary function for rotating table headers with 90 degrees */
        function rotateHeadCell(tableId) {
            'use strict';

            var aRows = document.getElementById(tableId).rows, padding = 4;
            [ ].every.call(aRows, function (row) {
                if (row.cells[0].tagName !== 'TH') {
                    return false;
                }
                rotateCell(row);
                return true;
            });
            function rotateCell(row) {
                var maxw = -1;
                [ ].forEach.call(row.cells, function (cell) {
                    var w;
                    if (!cell.hasAttribute("data-rotate")) {
                        return;
                    }
                    cell.innerHTML = '<div class=hgs_rotate>' + cell.innerHTML + '</div>';
                    w = cell.firstChild.clientWidth;
                    if (w > maxw) {
                        maxw = w;
                        cell.style.height = maxw + padding + 'px';
                    }
                    cell.firstChild.style.width = cell.firstChild.clientHeight + 'px';
                });
                if (maxw === -1) {
                    return;
                }
                [ ].forEach.call(row.cells, function (cell) {
                    var dd;
                    if (!cell.hasAttribute("data-rotate")) {
                        return;
                    }
                    dd = cell.firstChild;
                    dd.style.top = (cell.clientHeight - dd.clientHeight - padding) / 2 + 'px';
                    dd.style.left = '0px';
                    dd.style.position = 'relative';

                });
            }
        }
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* Distribution Groups Ajax Event */
        function rowToogleDistributionGroup(itemId, urlAction) {
            if ($('#distributionGroup' + itemId).hasClass('hide')) {
                $('#trigger' + itemId + ' i.icon-caret-right').addClass('icon-caret-down');
                $('#trigger' + itemId + ' i.icon-caret-right').removeClass('icon-caret-right');
                $('#distributionGroup' + itemId).removeClass('hide');

                if ($('#distributionGroup' + itemId + 'Instances').length == 0) {
                    $.ajax({
                        type: "GET",
                        url: urlAction,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: onLoadInstancessSuccess,
                        error: onLoadInstancessError
                    });
                }
            } else {
                $('#trigger' + itemId + ' .icon-caret-down').addClass('icon-caret-right');
                $('#trigger' + itemId + ' .icon-caret-down').removeClass('icon-caret-down');
                $('#distributionGroup' + itemId).addClass('hide');
            }
        }

        function onLoadInstancessSuccess(groupDetails) {
            var parentGroupCellId = $('#distributionGroup' + groupDetails.GroupId);
            var cellText = $(parentGroupCellId).html();
            if (cellText.trim() != groupDetails.Content.trim()) {
                $(parentGroupCellId).append(groupDetails.Content);
            }
            $('#imgLoading' + groupDetails.GroupId).remove();
        }

        function onLoadInstancessError() {
            // TODO - handle error
        }
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* Add thousand seperator */
/*function formatNumber(nStr) {
nStr += '';
x = nStr.split('.');
x1 = x[0];
x2 = x.length > 1 ? '.' + x[1] : '';
var rgx = /(\d+)(\d{3})/;
while (rgx.test(x1)) {
x1 = x1.replace(rgx, '$1' + ',' + '$2');
}
return x1 + x2;
}*/
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Allow text box to enter given number of range
To apply this to text box use $( '#myTextbox' ).numeric({max: 100});

(function ($) {
$.fn.numeric = function (options) {
return this.each(function () {
var $this = $(this);
$this.keypress(options, function (e) {
// check max range 
var dest = e.which - 48;
var result = this.value + dest.toString();
if (result > e.data.max) {
return false;
}
});
});
};
})(jQuery);*/
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------ Pop-ups Asynchronous Loading & Handlers */

/* Project folder grid Approval Permissions Pop-up and collaborators */

        var isCancleEvent = 0; isFromAccessGrid = false; isWorkfFlowAccess = false; isUserCanAcess = true; selectedPFApprovalAccess = 0;
        var IsPageReload = false; var approvalSelectedNewUser = []; var ExcistingUsers = []; var tryCount = 0;

        function ProjectFolderMultiAccessApprovals() {
            
            var id_approval = $('#hdnSelectedApprovals')[0].value.split(',')[0];
            $('.ProjectFolderApprovalDefaultAceess-' + id_approval).click();
        }
        
        function ModifyProjectGridPermissionsSubmit() {
            var listOfSelectedApproval = $('#hdnSelectedApprovals')[0].value.split(',');
            var selectedApproval_ = $('#hdnSelectedApproval').val();
            tryCount = 0;
            listOfSelectedApproval.forEach(function (item) {
                $('.chkThumb' + item).removeClass('NotExecutedWorkflow');
                $('.chkThumb' + item).addClass('ExecutedWorkflow');
                $('#soadIndicatorBar-' + item).attr('class', 'sent-state');
                IsPageReload = true;
            });
            
        }

        function LoadProjectFolderGridPermissions(event, urlLoadPermissionsAction, urlLoadApprovalGollaboratorsAction, approvalId_ , element, IsFromGrid, Is) {
            selectedPFApprovalAccess = approvalId_;
            if ((!$('#hdnSelectedApprovals')[0].value.includes(approvalId_) && $('#hdnSelectedApprovals')[0].value != "" ))
            {
                $('.choose_item, .customCheckBox .checkbox').removeAttr('checked');
                $('#hdnSelectedApprovals')[0].value = selectedPFApprovalAccess;
                $('.chkThumb' + approvalId_).attr('checked', true);
            }
    //if selected approval is only one and its from grid change url
    if (IsFromGrid == -1) {
        urlLoadApprovalGollaboratorsAction += "?id=" + $('#hdnSelectedApprovals')[0].value;
    }
    if (isFromAccessGrid && (event.target.id == "")) {
        isFromAccessGrid = false;
        $(element).click();
    }
    if (IsFromGrid == undefined) {
        if ($(".chkApproval" + event.target.getAttribute("widget-id") + " .NotExecutedWorkflow").length > 0) {
            isWorkfFlowAccess = true;
        }
        else {
            isWorkfFlowAccess = false;
        }
    }
    else if ($('.NotExecutedWorkflow:checked').length > 0 && $('#btnPF_MultiAccess').attr('disabled') == undefined) {
        isWorkfFlowAccess = true;
    }
    
    // Get Collaborator details first (only if the collaborators weren't already loaded)  
    if ($(element).attr("widget-users") == '' && ( isWorkfFlowAccess || !$('#hdnSelectedApprovals')[0].value.includes(',') ) && isCancleEvent == 0) {
        event.stopPropagation();
        $.ajax({
            type: "GET",
            url: urlLoadApprovalGollaboratorsAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                // Set the collaborators to the current element
                var collaborators = response.Collaborators;

                if ($(element).attr("widget-users") != undefined) {
                    $(element).attr("widget-users", '[' + collaborators['CollaboratorsWithRole'] + ']');
                }

                if ($(element).attr("widget-groups") != undefined) {
                    $(element).attr("widget-groups", '[' + collaborators['Groups'] + ']');
                }

                if ($(element).attr("widget-external") != undefined) {
                    $(element).attr("widget-external", '[' + collaborators['ExternalCollaboratorsWithRole'] + ']');
                }

                if ($(element).attr("widget-disabled") != undefined) {
                    $(element).attr("widget-disabled", '[' + collaborators['CollaboratorsWithDecision'] + ']');
                }

                if ($(element).attr("widget-exdisabled") != undefined) {
                    $(element).attr("widget-exdisabled", '[' + collaborators['ExternalCollaboratorsWithDecision'] + ']');
                }

                // Make sure the Permissions popup is loaded
                if ($('#formPermissions div').length == 0) {
                   LoadPermissionsPopup(urlLoadPermissionsAction, element);
                } else {
                    $(element).click();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error Loading Collaboratros ' + textStatus, errorThrown);
            }
        });
    } else {
        // Clear selection of users if more than one approval selected
        if ($('#hdnSelectedApprovals')[0].value.includes(',') && !isWorkfFlowAccess ) {
            $(element).attr("widget-users", "");
            $(element).attr("widget-external", "");
            $(element).attr("widget-owner", "");
            $(element).attr("widget-id", "");
            $(element).attr("widget-external-pdm", "");
            $(element).attr("widget-decision", "");
            $(element).attr("widget-groups", "");
            urlLoadPermissionsAction = urlLoadPermissionsAction + "&hsa=" + $('#hdnSelectedApprovals')[0].value;
        }
        if (isCancleEvent == 1 && (event.target.id == "btnPF_MultiAccess")) {
            $(element).attr("widget-users", "");
            $(element).attr("widget-external", "");
            $(element).attr("widget-owner", "");
            $(element).attr("widget-id", "");
            $(element).attr("widget-external-pdm", "");
            $(element).attr("widget-decision", "");
            $(element).attr("widget-groups", "");
            $(element).attr("widget-phase", "");
            isCancleEvent = 0;
        }
        
        LoadPermissionsPopup(urlLoadPermissionsAction, element);
        // Make sure the Permissions popup is loaded
    }
}

function LoadPermissions(event, urlLoadPermissionsAction, urlLoadApprovalGollaboratorsAction, element, IsFromGrid) {
    //if selected approval is only one and its from grid change url
    if (IsFromGrid == -1) {
        urlLoadApprovalGollaboratorsAction += "?id=" + $('#hdnSelectedApprovals')[0].value;
    }
    if (isFromAccessGrid && (event.target.id == "")) {
        isFromAccessGrid = false;
        $(element).click();
    }

    // Get Collaborator details first (only if the collaborators weren't already loaded)  
    if ($(element).attr("widget-users") == '' && !$('#hdnSelectedApprovals')[0].value.includes(',') && isCancleEvent == 0) {
        event.stopPropagation();
        $.ajax({
            type: "GET",
            url: urlLoadApprovalGollaboratorsAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                // Set the collaborators to the current element
                var collaborators = response.Collaborators;

                if ($(element).attr("widget-users") != undefined) {
                    $(element).attr("widget-users", '[' + collaborators['CollaboratorsWithRole'] + ']');
                }

                if ($(element).attr("widget-groups") != undefined) {
                    $(element).attr("widget-groups", '[' + collaborators['Groups'] + ']');
                }

                if ($(element).attr("widget-external") != undefined) {
                    $(element).attr("widget-external", '[' + collaborators['ExternalCollaboratorsWithRole'] + ']');
                }

                if ($(element).attr("widget-disabled") != undefined) {
                    $(element).attr("widget-disabled", '[' + collaborators['CollaboratorsWithDecision'] + ']');
                }

                if ($(element).attr("widget-exdisabled") != undefined) {
                    $(element).attr("widget-exdisabled", '[' + collaborators['ExternalCollaboratorsWithDecision'] + ']');
                }

                // Make sure the Permissions popup is loaded
                if ($('#formPermissions div').length == 0) {
                    $.ajax({
                        type: "GET",
                        url: "/Approvals/CanUserAccess?id=" + $('#hdnSelectedApprovals')[0].value,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            isUserCanAcess = response;
                            LoadPermissionsPopup(urlLoadPermissionsAction, element);
                        }
                    });
                } else {
                    $(element).click();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error Loading Collaboratros ' + textStatus, errorThrown);
            }
        });
    } else {
        // Clear selection of users if more than one approval selected
        if ($('#hdnSelectedApprovals')[0].value.includes(',')) {
            $(element).attr("widget-users", "");
            $(element).attr("widget-external", "");
            $(element).attr("widget-owner", "");
            $(element).attr("widget-id", "");
            $(element).attr("widget-external-pdm", "");
            $(element).attr("widget-decision", "");
            $(element).attr("widget-groups", "");
            urlLoadPermissionsAction = urlLoadPermissionsAction +"&hsa="+$('#hdnSelectedApprovals')[0].value;
        }

        if (isCancleEvent == 1 && (event.target.id == "btnMultiAccess")) {
            $(element).attr("widget-users", "");
            $(element).attr("widget-external", "");
            $(element).attr("widget-owner", "");
            $(element).attr("widget-id", "");
            $(element).attr("widget-external-pdm", "");
            $(element).attr("widget-decision", "");
            $(element).attr("widget-groups", "");
            isCancleEvent = 0;
        }
        // Make sure the Permissions popup is loaded
        $.ajax({
            type: "GET",
            url: "/Approvals/CanUserAccess?id=" + $('#hdnSelectedApprovals')[0].value,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                isUserCanAcess = response;
                LoadPermissionsPopup(urlLoadPermissionsAction, element);
            }
        });
    }
}

/* Folder Permissions Pop-up and collaborators */

function LoadFolderPermissions(event, folderId, urlLoadFolderPermissionsAction, urlLoadFolderGollaboratorsAction, element) {
   
    // Get Folder Collaborator details first (only if the collaborators weren't already loaded)
    if ($(element).attr("widget-users") == '') {
        event.stopPropagation();
        $.ajax({
            type: "GET",
            url: urlLoadFolderGollaboratorsAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                // Set the collaborators to the current element
                var collaborators = response.FolderCollaborators;

                if ($(element).attr("widget-users") != undefined) {
                    $(element).attr("widget-users", '[' + collaborators['CollaboratorsWithRole'] + ']');
                }

                if ($(element).attr("widget-groups") != undefined) {
                    $(element).attr("widget-groups", '[' + collaborators['Groups'] + ']');
                }

                // Make sure the Permissions popup is loaded
                if ($('#formPermissions div').length == 0) {
                    $.ajax({
                        type: "GET",
                        url: "/Approvals/CanUserAccessFolder?id=" + folderId,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            isUserCanAcess = response;
                            LoadPermissionsPopup(urlLoadFolderPermissionsAction, element);
                        }
                    });
                } else {
                    $(element).click();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error Loading Collaboratros ' + textStatus, errorThrown);
            }
        });
    } else {
        $.ajax({
            type: "GET",
            url: "/Approvals/CanUserAccessFolder?id=" + folderId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                isUserCanAcess = response;
                LoadPermissionsPopup(urlLoadFolderPermissionsAction, element);
            }
        });
        // Make sure the Permissions popup is loaded
    }

}

function LoadPermissionsPopup(urlLoadPermissionsAction, element) {
    if( isUserCanAcess){
    if ($('#formPermissions div').length == 0) {
        $.ajax({
            type: "GET",
            url: urlLoadPermissionsAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(permissions) {
                $('#formPermissions').append(permissions.Content);
                $(element).click();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error Loading Permissions pop-up ' + textStatus, errorThrown);
            }
        });
    }
    }
    else {
        isUserCanAcess = true;
}
}

function LoadFoldersTreePopup(urlLoadFoldersTreeAction, element, isDisplayFolderAction) {
    var approvals, folders;
    if (!isDisplayFolderAction) {
        approvals = $('.choose_item:checked[approval]').map(function () { return $(this).attr('approval'); }).get().join(',');
        folders = $('.choose_item:checked[folder]').map(function () { return $(this).attr('folder'); }).get().join(',');

        if ($(element).attr("widget-approvals") == undefined) {
            $(element).attr("widget-approvals", '[' + approvals + ']');
        }

        if ($(element).attr("widget-folders") == undefined) {
            $(element).attr("widget-folders", '[' + folders + ']');
        }
    }
    
    if ($('#formMoveToFolder div').length == 0) {
        $.ajax({
            type: "GET",
            url: urlLoadFoldersTreeAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(folders) {
                $('#formMoveToFolder').append(folders.Content);
                $(element).click();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error Loading Folders Tree pop-up ' + textStatus, errorThrown);
            }
        });
    } 
}

/* Approval SendAndShare Pop-up */
function ApprovalExternalPermissions(urlExternalPermissionsAction, urlLoadApprovalExternalCollaboratorsAction, element) {
  
    // Get external collaborator details first (only if the external collaborators weren't already loaded)
    if ($(element).attr("widget-external") == '') {
        $.ajax({
            type: "GET",
            url: urlLoadApprovalExternalCollaboratorsAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
               
                // Set the collaborators to the current element
                var externalCollaborators = response.ExternalCollaborators;
               
                if ($(element).attr("widget-external") != undefined) {
                    $(element).attr("widget-external", '[' + externalCollaborators + ']');
                }
                $.ajax({
                    type: "GET",
                    url: "/Approvals/CanUserAccess?id=" + $('#hdnSelectedApprovals')[0].value,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        isUserCanAcess = response;
                        ApprovalSendAndShare(urlExternalPermissionsAction, element);
                    }
                });
                // Make sure the External Permissions popup is loaded
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error Loading External Collaboratros ' + textStatus, errorThrown);
            }
        });
    } else {
        $.ajax({
            type: "GET",
            url: "/Approvals/CanUserAccess?id=" + $('#hdnSelectedApprovals')[0].value,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                isUserCanAcess = response;
                ApprovalSendAndShare(urlExternalPermissionsAction, element);
            }
        });
        // Make sure the External Permissions popup is loaded
    }

}

function ApprovalSendAndShare(urlAction, element) {
    if (isUserCanAcess) {
    if ($('#formShare div').length == 0) {
        $.ajax({
            type: "GET",
            url: urlAction,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (shareView) {
                $('#formShare').append(shareView.Content);
                $(element).click();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error Loading Share pop-up ' + textStatus, errorThrown);
            }
        });
    }
} else {
        isUserCanAcess = true;
}
}

function clearSharePopupInputs() {
    $(this).find("input[name^='share_']").each(function () {
        $(this).val('');
    });
}

/* Profile Groups Ajax Event */
function rowToogleProfile(itemId, urlAction) {
    if ($('#profileGroup' + itemId).hasClass('hide')) {
        $('#trigger' + itemId + ' i.icon-caret-right').addClass('icon-caret-down');
        $('#trigger' + itemId + ' i.icon-caret-right').removeClass('icon-caret-right');
        $('#profileGroup' + itemId).removeClass('hide');

        if ($('#profileGroup' + itemId + 'Instances').length == 0) {
            $.ajax({
                type: "GET",
                url: urlAction,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onLoadProfilesSuccess,
                error: onLoadProfilesError
            });
        }
    } else {
        $('#trigger' + itemId + ' .icon-caret-down').addClass('icon-caret-right');
        $('#trigger' + itemId + ' .icon-caret-down').removeClass('icon-caret-down');
        $('#profileGroup' + itemId).addClass('hide');
    }
}

function onLoadProfilesSuccess(groupDetails) {
    var parentGroupCellId = $('#profileGroup' + groupDetails.GroupId);
    var cellText = $(parentGroupCellId).html();
    if (cellText.trim() != groupDetails.Content.trim()) {
        $(parentGroupCellId).append(groupDetails.Content);
    }
    $('#imgLoading' + groupDetails.GroupId).remove();
}

function onLoadProfilesError() {
    // TODO - handle error
}
/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/


/* Approval SendToDeliver Pop-up */
function loadSubmitToDeliverPopup() {
    $.ajax({
        type: "GET",
        url: '/Approvals/LoadSubmitToDeliverPopup',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache:false,
        success: function (popup) {
            $('#deliverPopUpLoading').remove();
            $('#modalDialogSubmitToDeliver div.modal-body').append(popup.Content);
            checkPageRange();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#deliverPopUpLoading').replaceWith('<label>Error Loading SubmitToDeliver Popup ' + textStatus + ' ' + errorThrown + '</label>'); ;
        }
    });
}

/* Reset Cliend Validation Errors */

function removeUnobstrusiveValidations() {
    //Removes validation from input-fields
    $('.input-validation-error').addClass('input-validation-valid valid');
    $('.input-validation-error').removeClass('input-validation-error');
    //Removes validation message after input-fields
    $('.field-validation-error').addClass('field-validation-valid');
    $('.field-validation-error').removeClass('field-validation-error');
}

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*In case user avatar picture was not found load default one */
function setUserDefaultPicture() {
    $(".userAvatar").on('error', function () {
        $(this).attr('src', '/content/img/nouser-48px-48px.png?' + Math.random());
    });
}
$(function () {
    setUserDefaultPicture();
});

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
function openReportFilterPopup(element, isFromFolderTree) {
    //get all checked jobs
    var selectedApprovals = $('#hdnSelectedApprovals').val() == undefined || $('#hdnSelectedApprovals').val() == 0 ? "" : $('#hdnSelectedApprovals').val();
    var selectedFolders = $('#hdnSelectedFolders').val() == undefined ? "" : $('#hdnSelectedFolders').val();

    if (selectedApprovals != "" && $(element).attr('approval')) {
        selectedApprovals = selectedApprovals + "," + $(element).attr('approval');
    }
    else if (selectedApprovals == "" && $(element).attr('approval')) {
        selectedApprovals = $(element).attr('approval');
    }

    if (selectedFolders == "" && $(element).attr('folder')) {
        selectedFolders = $(element).attr('folder');
        if (isFromFolderTree) {
            selectedApprovals = "";
        }
    }

    $.ajax({
        type: "GET",
        url: '/Approvals/PopulateAnnotationsReportListBox?selectedApprovals=' + selectedApprovals + '&selectedFolders=' + selectedFolders,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            window.LoadUsersAndGroupsListBox(data);
            $('#modalAnnotationsReportFilter').modal({ backdrop: 'static', keyboard: false });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('#dinamycLists').replaceWith('<label>Error Loading list ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
}

//openReportFilterPopupfromProjectFolder
function openReportFilterPopupfromProjectFolder(element, isFromFolderTree) {

    //get all checked jobs
    var selectedApprovals = $('#hdnSelectedApprovals').val() == undefined || $('#hdnSelectedApprovals').val() == 0 ? "" : $('#hdnSelectedApprovals').val();
    var selectedFolders = $('#hdnSelectedFolders').val() == undefined ? "" : $('#hdnSelectedFolders').val();

    if (selectedApprovals != "" && $(element).attr('approval')) {
        selectedApprovals = selectedApprovals + "," + $(element).attr('approval');
    }
    else if (selectedApprovals == "" && $(element).attr('approval')) {
        selectedApprovals = $(element).attr('approval');
    }

    if (selectedFolders == "" && $(element).attr('folder')) {
        selectedFolders = $(element).attr('folder');
        if (isFromFolderTree) {
            selectedApprovals = "";
        }
    }

    $.ajax({
        type: "GET",
        url: '/ProjectFolders/PopulateAnnotationsReportListBox?selectedApprovals=' + selectedApprovals + '&selectedFolders=' + selectedFolders,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            window.LoadUsersAndGroupsListBox(data);
            $('#modalAnnotationsReportFilter').modal({ backdrop: 'static', keyboard: false });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('#dinamycLists').replaceWith('<label>Error Loading list ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
}
function openTagwordsFilterPopup() {
    var selectedApprovalTypes = '';
    var selectedTagwords = '';
    $.ajax({
        type: "GET",
        url: '/Widget/GetTagwordsForViewAllPopup?selectedApprovalTypes=' + selectedApprovalTypes + '&selectedTagwords=' + selectedTagwords,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            $('.loopTagwords').remove();
            LoadAllTagwords(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('.filteredtagwordsdiv').replaceWith('<label>Error Loading list ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
    $('#modalTagwordsReportFilter').modal({ backdrop: 'static', keyboard: false });
}
function openNewProjectFolderPopup() {
    $('#modalNewProjectFolder').modal({ backdrop: 'static', keyboard: false });
}
function openProjectFolderEditMember() {
    var projectID = $('#hdnProjectFolderID')[0].value;
    $.ajax({
        type: "GET",
        url: '/ProjectFolders/GetProjectFoldersMembers?SelectedProjectFolderID=' + projectID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            LoadProjectFolderCollaboratorsToEditMemberPopup(data);
            $('#modalEditProjectFolderMember').modal({ backdrop: 'static', keyboard: false });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
        }
    });
}
function openEditProjectFolderPopup() {
    var projectID = $('#hdnProjectFolderID')[0].value;

    $.ajax({
        type: "GET",
        url: '/ProjectFolders/EditProject?SelectedProjectFolderID=' + projectID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            LoadProjectFolderDetailsToEditPopup(data);
            $('#modalNewProjectFolder').modal({ backdrop: 'static', keyboard: false });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
        }
    });
}

// Collaborate Approval Filter
function openApprovalFilterPopup(currentTab) {
    var selectedApprovalTypes = '';
    var selectedTagwords = '';
    $.ajax({
        type: "GET",
        url: '/Widget/GetTagwordsBasedOnSelectedApprovalType?selectedApprovalTypes=' + selectedApprovalTypes + '&selectedTagwords=' + selectedTagwords + '&currentTab=' + currentTab,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            $('.filteredtagwordsdiv').remove();
            LoadTagwords(data);
            LoadApprovalsCount(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('.filteredtagwordsdiv').replaceWith('<label>Error Loading list ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
    $('#modalApprovalReportFilter').modal({ backdrop: 'static', keyboard: false });
    
}

function openProjectFilterPopup() {
    var selectedApprovalTypes = '';
    var selectedTagwords = '';
    $.ajax({
        type: "GET",
        url: '/Widget/GetTagwordsForProjectFolderFilters?selectedApprovalTypes=' + selectedApprovalTypes + '&selectedTagwords=' + selectedTagwords,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            $('.filteredtagwordsdiv').remove();
            LoadTagwords(data);
            LoadApprovalsCount(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('.filteredtagwordsdiv').replaceWith('<label>Error Loading list ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
    $('#modalApprovalReportFilter').modal({ backdrop: 'static', keyboard: false });

}


//project Folder Approval Filter
function openProjectApprovalFilterPopup(currentTab) {
    var selectedApprovalTypes = '';
    var selectedTagwords = '';
    $.ajax({
        type: "GET",
        url: '/Widget/GetTagwordsBasedOnSelectedProjectApprovalType?selectedApprovalTypes=' + selectedApprovalTypes + '&selectedTagwords=' + selectedTagwords + '&currentTab=' + currentTab,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            $('.filteredtagwordsdiv').remove();
            LoadTagwords(data);
            LoadApprovalsCount(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('.filteredtagwordsdiv').replaceWith('<label>Error Loading list ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
    $('#modalApprovalReportFilter').modal({ backdrop: 'static', keyboard: false });

}

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
function applyFolderPermissionsConfirmation(selectedFolderId) {
    if ($('#modalDialogApplyFolderPermissionsConfirmation div.modal-body div').length == 0) {
        $('#selectedFolder').val(selectedFolderId);
        $('#modalDialogApplyFolderPermissionsConfirmation').modal('show');
    }
}

// validate key for numeric input
COZ.isNumber = function(evt, isInteger) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (isInteger || charCode != 46) {
            return false;
        }
    }
    return true;
};

// show error field
COZ.showError = function(show, errorFieldId, errorAttribute, additionalMessage) {
    var errorField = $('#' + errorFieldId);
    if (show) {
        var additionalError;
        if (typeof(additionalMessage) != 'undefined') {
            additionalError = additionalMessage;
        } else {
            additionalError = '';
        }
        errorField.text(errorField.attr(errorAttribute) + additionalError);
        errorField.show();
    } else {
        errorField.text('');
        errorField.hide();
    }
};

/*---------------------------------------------------------------------------- */
$('#formAddNewApproval, #formAddDeliverJob').on('submit', function () {
    if (($('div .block input[type=text].first-name').val() != undefined && $('div .block input[type=text].first-name').val() != "") || ($('div .block input[type=text].last-name').val() != undefined && $('div .block input[type=text].last-name').val() != "") || ($('div .block input[type=text].email').val() != undefined && $('div .block input[type=text].email').val() != "")) {
        $('#errSelectExternalUser').show();

        $("#imgSavingLoader").hide();

        setTimeout(function () {
            $('button[data-loading-text]').button('reset');
        }, 0);
        return false;
    }
    return true;
});

$('div .block input[type=text].first-name, div .block input[type=text].last-name, div .block input[type=text].email').on('keyup', function () {
    if ($('div .block input[type=text].first-name').val() == "" && $('div .block input[type=text].last-name').val() == "" && $('div .block input[type=text].email').val() == "") {
        $('#errSelectExternalUser').hide();
    }
});

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
//Detect changes on contenteditable div's and mark form as dirty
$('body').on('focus', '[contenteditable]', function() {
    var $this = $(this);
    $this.data('before', $this.html());
    return $this;
}).on('blur keyup paste input', '[contenteditable]', function() {
    var $this = $(this);
    if ($this.data('before') !== $this.html()) {
        $this.data('before', $this.html());
        $('form').dirtyForms('setDirty');
    }
    return $this;
});

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
//Detect changes on spinner div's and mark form as dirty
$('.spinner button').on('click', function() {
    $('form').dirtyForms('setDirty');
});

/*----------------------------------------------------------------------------------------------------------------------------------------------------------*/
//Detect changes on delete table row button and mark form as dirty
$('i.icon-trash').closest('button').on('click', function () {
    $('form').dirtyForms('setDirty');
});

function loadUserSOADIndicator(approval) {
    if (approval) {
        $.ajax({
            type: "GET",
            url: getUserSOADIndicatorData + '?approval=' + approval,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalsSOAD) {
                approvalsSOAD.Content.Data.forEach(function (item, index, array) {                    
                    if (item.IsExternal.toString() == 'true') {
                        $('.externalActivityTable .soadContainer #soadIndicatorBar_' + item.Approval + '_' + item.Collaborator + '_' + item.Phase).attr('class', item.ProgressStateClass);
                    } else {
                        $('#userActivityTable .soadContainer #soadIndicatorBar_' + item.Approval + '_' + item.Collaborator + '_' + item.Phase).attr('class', item.ProgressStateClass);
                    }
                });

                $('.approval-state[rel="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading User SOAD ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}
