﻿//SideBar's initial offset
var initialOffset = {};
var gridRes;

//SideBar's size
var boxWidth;
var sliderButtonWidth;
var isSideBarMenuBig = false;
var foldersCount = 0;
var sideBarIsClosed = false;

//close SideBar when clicking outside of the SideBar
$(document).mouseup(function (e) {
    var container = $(".box-container");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        closeSideBar();
    }
});

//when page is loaded render the SideBar and hide it
$(document).ready(function () {
    resizeGrid();
    showSideBarAfterGrid();
     
    if (localStorage.getItem("sidebarIsPinned") == "false") {
        closeSideBar();
    }
});

$(window).load(function () {
    setTimeout(function () {
        // Fix for Safari and IE as they do not have required data at document ready event
        adjustInitialOffset();
    }, 100);
});

var adjustInitialOffset = function () {
    if(initialOffset.TopPos == 0) {
        initialOffset.leftPos = $("#sliderButton").offset().left;
        initialOffset.TopPos = $("#sliderButton").offset().top;
        initialOffset.currentWindowWidth = $(window).width();
    }
}

$(window).on('resize', function () {
    //adapt the SideBar based on window resize
    initialOffset.leftPos = $(".widget").offset().left;
    if ($(window).scrollTop() > initialOffset.TopPos) {
        $('#sliderMenu').css('left', initialOffset.leftPos);
        $('#sliderButton').css('left', initialOffset.leftPos);
    }

    if (!localStorage.getItem("sidebarIsPinned")) {
        localStorage.setItem("sidebarIsPinned", true);
    }
    if (localStorage.getItem("sidebarIsPinned") == "true") {
        gridRes = $("#gridContainer").width() - 280;
        $("#summaryView").css("width", gridRes);
        $("#summaryView").css("float", "right");
        $("#statusView").css("width", gridRes);
        $("#statusView").css("float", "right");
    } else {
        gridRes = $("#gridContainer").width();
        $("#statusView").css("width", gridRes);
        $("#summaryView").css("width", gridRes);
    }

    smartColumns();
});


//display the SideBar only after the grid has finished loading
function showSideBarAfterGrid() {
    $("#sliderButton").show();
    initialOffset.leftPos = $("#sliderButton").offset().left;
    initialOffset.TopPos = $("#sliderButton").offset().top;
    initialOffset.currentWindowWidth = $(window).width();
}

//scroll the SideBar to the top of the page and make it stick
$(window).scroll(function () {
    adjustInitialOffset();
    var winTop = $(window).scrollTop();
    $('#sliderButton').toggleClass('sticky', $(window).scrollTop() > initialOffset.TopPos);
    $('#sliderMenu').toggleClass('sticky', $(window).scrollTop() > initialOffset.TopPos);
    
    var hasAnyApprovals = $('#formMain').find('div.row-fluid').length == 0;

    if(foldersCount > 0 && hasAnyApprovals) {
        dinamicSideBarMenuFoldersPanel(); 
    }

    var left = winTop > initialOffset.TopPos ? initialOffset.leftPos : 0;

    $('#sliderMenu').css('left', left);
    $('#sliderButton').css('left', left);

    $('#collaborateSidebar > div.open').removeClass('open');
});

// close/open the SideBar when clicking the button
$(".slide-toggle").on('click', function () {
    if ((localStorage.getItem("sidebarIsPinned") == "false")) {
        showHideSideBar();
    }
});

function showHideSideBar() {
    //var sideBarWidth = GetSidebarWidth();
    boxWidth = $(".box-container").width();
    sliderButtonWidth = $("sliderButton").width();
    if (boxWidth > 0) {
        closeSideBar();
    } else {
    $(".box-container").animate({
        width: 283,
    }, 100, function () {
        $(".box-container").addClass('overFlowClass');
        $("#icon").addClass('rotate');
        if (localStorage.getItem("sidebarIsPinned") == "true") {
            setTimeout(function () {
                $('#sidebarPin > i').removeClass('icon-rotate-180').addClass('icon-rotate-45');
            }, 30);
        }
        $("#sliderMenu").show();
        
        setTimeout(function () {
            baseSideBarMenuFoldersPanel();
            dinamicSideBarMenuFoldersPanel();
        }, 50);

        if (!window.location.href.includes("ProjectFolders"))
            scrollToActiveFolder();

        sideBarIsClosed = false;
    });

    $("#sliderButton").animate({
        height: parseInt($('.box-container').css('height')) - 100
    }, 0);

    }
}

//fix the position of the folder Dropdown
function dropDownFixPosition(element, id) {
    var element = $(element);
    var windowObject = $(window);
    var menuAboveButtonPadding = 34;
    var spaceBetweenMenuAndFooter = 50;
    var currentDropdown = $('#actionDropDown' + id);
    var dropDownTop = element.offset().top + element.outerHeight() - windowObject.scrollTop();

    // Do not apply fix for IE as IE has other issues
    if (!isIEBrowser()) {
        var availableSpace = windowObject.height() - element[0].getBoundingClientRect().top;
        var dropDownHeight = currentDropdown.height();

        if (availableSpace - dropDownHeight - spaceBetweenMenuAndFooter > 0) {
            // If there is enough space including padding, display dropdown below
            currentDropdown.css('top', dropDownTop + "px");
        } else {
            // Display dropdown above including padding 
            currentDropdown.css('top', dropDownTop - dropDownHeight - menuAboveButtonPadding + "px");
        }
    } else {
        currentDropdown.css('top', dropDownTop + "px");
    }
    
    currentDropdown.css('left', element.offset().left - currentDropdown.width() + "px");
}

// Check if IE browser
function isIEBrowser() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        return true;
    } else {
        return false;
    }
}

function closeSideBar() {
    if (localStorage.getItem("sidebarIsPinned") == "false") {
        sideBarIsClosed = true;
        $(".box-container").animate({
            width: 0
        }, 100, function () {
            $(".box-container").removeClass('overFlowClass');
            $("#icon").removeClass('rotate');
            $("#sliderMenu").hide();
        });
        $("#sliderButton").animate({
            height: 150
        }, 100);
    }
}

$('#collaborateSidebar-handle').on('click', function () {
    var sidebar = $('#collaborateSidebar');
    if (sidebar.hasClass('active')) {
        sidebar.removeClass('active');
        sidebar.css('left', '-500px');
    } else {
        var widget = $('div.widget');
        sidebar.addClass('active');
        sidebar.css('left', widget.offset().left);
        sidebar.css('height', widget.height());
    }
});

function renderSideBarFolders(viewAllFiles) {
    $.ajax({
        type: "GET",
        url: popupateFoldersUrl + "&viewAll=" + viewAllFiles ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (folders) {
            $('#loadingFoldersTree').remove();
            $('#folders').append(folders.Content);
            if (localStorage.getItem("sidebarIsPinned") == "true") {
                $('#sidebarPin > i').removeClass('icon-rotate-180').addClass('icon-rotate-45');
            }
            $("#folders div[rel='tooltip']").slice(-2).attr('data-placement', 'top');
            $("#folders div[rel='tooltip']").tooltip();

            foldersCount = (folders.Content.match(/block level/g) || []).length;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('#loadingFoldersTree').replaceWith('<label>Error Loading Folders Tree ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
}

setTimeout(function () {
    if ($(".icon-pushpin").hasClass("icon-rotate-180")) {
        $("#formFilters").addClass("selectAllChcekbox");
    }
},1000)

function changePinState() {
    if (localStorage.getItem("sidebarIsPinned") == undefined || localStorage.getItem("sidebarIsPinned") == "false") {
        localStorage.setItem("sidebarIsPinned", true);
        $('#sidebarPin > i').removeClass('icon-rotate-180').addClass('icon-rotate-45');
        gridRes = $("#gridContainer").width() - 280;
        $("#summaryView").css("width", gridRes);
        $("#summaryView").css("float", "right");
        $("#statusView").css("width", gridRes);
        $("#statusView").css("float", "right");
        $("#formFilters").removeClass("selectAllChcekbox");

    } else {
        localStorage.setItem("sidebarIsPinned", false);
        closeSideBar();
        $('#sidebarPin > i').removeClass('icon-rotate-45').addClass('icon-rotate-180');
        $("#summaryView").css("width", $("#gridContainer").width());
        $("#statusView").css("width", $("#gridContainer").width());
        $("#formFilters").addClass("selectAllChcekbox");
    }
    $(window).trigger('resize');
    $('#approvals-index').find('th:first').trigger('resize');
}

function resizeGrid() {
    $(window).trigger('resize');
    if (localStorage.getItem("sidebarIsPinned") == undefined || localStorage.getItem("sidebarIsPinned") == "false") {
        $('#sidebarPin > i').removeClass('icon-rotate-45').addClass('icon-rotate-180');
        gridRes = $("#gridContainer").width();
        $("#summaryView").css("width", gridRes);
        $("#statusView").css("width", gridRes);
    } else {
        $('#sidebarPin > i').removeClass('icon-rotate-180').addClass('icon-rotate-45');
        gridRes = $("#gridContainer").width() - 280;
        $("#summaryView").css("width", gridRes);
        $("#summaryView").css("float", "right");
        $("#statusView").css("width", gridRes);
        $("#statusView").css("float", "right");
    }
}

var scrollToActiveFolder = function() {
    var maxTime = 3000; // 3 seconds
    var time = 0;
        if ($('#formMain div').hasClass("blank-state")) {
            $("#formFilters .checkbox.pull-left").css("display", "none");
        }
    var isRecentlyArchivedActive = $("ul.nav.nav-pills.nav-stacked.nav-colored li.active.recentlyArchived")[0] != undefined;
    if(!isRecentlyArchivedActive) {
        var interval = setInterval(function () {
        
            if($('#approvalsFoldersSideBar').is(':visible')) {
                if ($('#approvalsFoldersSideBar').length > 0 && $('#folders div.active.open').length > 0) {
                    var offsetTop = ($('#folders div.active.open').offset().top - $('#approvalsFoldersSideBar').offset().top);
                    $('#approvalsFoldersSideBar').animate({
                        scrollTop: offsetTop
                    }, 'slow'); 
                }
                clearInterval(interval);
            } else {
                if (time > maxTime) {
                    // still hidden, after 3 seconds, stop checking
                    clearInterval(interval);
                    return;
                }
            
                // not visible yet
                scrollToActiveFolder();
                time += 100;
            }
        }, 200);
    }
};

var baseSideBarMenuFoldersPanel = function () {
    adjustInitialOffset();

    // Set min heights of container and folders panel
    var isRecentlyArchivedActive = $("ul.nav.nav-pills.nav-stacked.nav-colored li.active.recentlyArchived")[0] != undefined;
    var foldersPanelMinHeight = parseInt($('#approvalsFoldersSideBar').css('min-height'));
    var spaceForOtherElements = parseInt($('.collaborateSidebarTop').css('height')) + 50;
    var containerMinHeight = parseInt($('.box-container').css('min-height'));
    var searchItems = searchItemsInSideBarMenuCount();

    if(searchItems > 0 && !isSideBarMenuBig && !isRecentlyArchivedActive)
    {
        containerMinHeight = containerMinHeight + (searchItems * 35);
        isSideBarMenuBig = true;
    }

    $('#approvalsFoldersSideBar').css('min-height', foldersPanelMinHeight);
    $('.box-container').css('min-height', containerMinHeight);
    $('.box-container').css('max-height', containerMinHeight);

    if(isRecentlyArchivedActive) {
        $('#sliderButton').css('height', spaceForOtherElements - 100);
        $('.box-container').css('height', spaceForOtherElements);
        $('#sliderButton').css('min-height', spaceForOtherElements - 100);
        $('.box-container').css('min-height', spaceForOtherElements);
    }

    $('#sliderButton').css('height', parseInt($('.box-container').css('height')) - 100);

    setTimeout(function () {
        checkFoldersPanelSize();
    }, 100);
}

var checkFoldersPanelSize = function () {
    var foldersPanelHeight = parseInt($('#approvalsFoldersSideBar').css('height'));
    var spaceForOtherElements = parseInt($('.collaborateSidebarTop').css('height')) + 50;
    var containerHeight = parseInt($('.box-container').css('height'));

    // Make sure heights are set accordingly
    if(foldersPanelHeight + spaceForOtherElements + 50 > containerHeight) {
        $('#approvalsFoldersSideBar').css('height', containerHeight - spaceForOtherElements - 50);
    }
}

var dinamicSideBarMenuFoldersPanel = function () {
    adjustInitialOffset();

    // At first dinamic setting, reset max height
    $('.box-container').css('max-height', '');

    // Safari fix: initialOffset is set at document.ready, Safari does not have any details about element offset at that time
    var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                   navigator.userAgent && !navigator.userAgent.match('CriOS');

    if(initialOffset.TopPos == 0 && isSafari) {
        initialOffset.TopPos = $("#sliderButton").offset().top;
    }

    var isRecentlyArchivedActive = $("ul.nav.nav-pills.nav-stacked.nav-colored li.active.recentlyArchived")[0] != undefined;
    var foldersPanelHasOverflow = false;
    var sideBarMenuIsSticky = $("#sliderMenu").hasClass("sticky");

    if ($('#approvalsFoldersSideBar').prop('scrollHeight') > $('#approvalsFoldersSideBar').prop('clientHeight')) {
        foldersPanelHasOverflow = true;
    } 

    if(!isRecentlyArchivedActive && 
        (foldersPanelHasOverflow || !sideBarMenuIsSticky)) {
            var windowHeight = $(window).height();
            var sideBarMenuContainerSize = windowHeight - 50;
            var spaceForOtherElements = parseInt($('.collaborateSidebarTop').css('height')) + 100;
            var isHeaderAndMenuVisible = $(window).scrollTop() < initialOffset.TopPos;
            var headerAndMenuVisibleHeight = initialOffset.TopPos - $(window).scrollTop();
            var containerHeight = 0;
            
            if(isHeaderAndMenuVisible) {
                containerHeight = sideBarMenuContainerSize - headerAndMenuVisibleHeight;
            } else {
                containerHeight = sideBarMenuContainerSize;
            }
            var foldersPanelHeight = containerHeight - spaceForOtherElements; 

            // Take into consideration available folders
            if(foldersCount > 0) {
                var actualFoldersRequiredSpace = foldersCount * 30;
                if(actualFoldersRequiredSpace < foldersPanelHeight) {
                    containerHeight = containerHeight - (foldersPanelHeight - actualFoldersRequiredSpace);
                    foldersPanelHeight = actualFoldersRequiredSpace;
                }
            }

            var sidebarIsPinned = localStorage.getItem("sidebarIsPinned") == "true";
            if(sidebarIsPinned || !sideBarIsClosed) {
                $('#sliderButton').css('height', containerHeight - 100);
            }

            $('.box-container').css('height', containerHeight);
            $('#approvalsFoldersSideBar').css('height', foldersPanelHeight);
            $('#approvalsFoldersSideBar').css('max-height', foldersPanelHeight);

            setTimeout(function () {
                checkFoldersPanelSize();
            }, 50);
    }
}

var searchItemsInSideBarMenuCount = function () {
    if ($('.box-container').find('#customSearch').length) {
        if($('ul#customSearch li').length >= 1) {
            var count = $("ul#customSearch li").size();
            // Only 3 custom search items are displayed 
            if(count > 3)
                count = 3;

            return count;
        }
    }

    return 0;
};

