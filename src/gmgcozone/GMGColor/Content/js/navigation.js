﻿$(document).ready(function () {
    $("ul li a.navigation-collapse-trigger").click(function () {

        var level = $(this).parents("li:first").find("ul.nav-children:first").attr("level");
        var menuKey = $(this).parents("li:first").find("ul.nav-children:first").attr("key");

        $(".nav-children[level='" + level + "']").each(function (i, v) {
            if ($(v).attr("key") != menuKey) {
                $(v).slideUp();

                $(v).find("li ul.nav-children[level]").slideUp('normal', function () {
                    RemoveLastItemRoundCorners();
                });
            }
        });

        $(this).parents('li:first').find('ul.nav-children:first').slideToggle('normal', function () {
            RemoveLastItemRoundCorners();
        });

    });
});

function RemoveLastItemRoundCorners() {
    return;
    
    // add class to remove the bottom left and right corners when last menu is expand
    if ($('.nav-parent>li:last>ul.nav-children:first').is(':visible')) {
        $('.nav-parent>li:last').addClass('no-rounded-corners');
    } else {
        $('.nav-parent>li:last').removeClass('no-rounded-corners');
    }
}