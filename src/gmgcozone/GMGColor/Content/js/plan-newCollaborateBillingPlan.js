﻿$(document).ready(function () {
    HandleCurrentBPType();
    $('#objBillingPlanType_AppModule').val('1').change();
    $('#objBillingPlanType_AppModule').prop("disabled", true);
    $('#txtPricePerYear')[0].value = $("#currencySymbol").val() + ' ' + Number($('#Price').val()) * 12;

    if ($('#chkIsFixed').is(':checked')) {
        removeFreePlanValidation();
    }
});

function HideCollaborateSettings() {
    $("#objBillingPlan_SoftProofingWorkstations").val('0');
    $(".softproofing").addClass('hide');
}

function HideDeliverSettings() {
    $("#objBillingPlan_ColorProofConnections").val('0');
    $("#cpConnections").addClass('hide');
}
                    
function EnableFixedPricePlan() {
    $('#fixedPriceLabel').show();
}

function HandleCurrentBPType() {
    HideDeliverSettings();
    EnableFixedPricePlan();
}

var currencySymble = $("#currencySymbol").val() ;

$('#chkIsFixed').click(function () {
    if ($(this)[0].checked) {
        removeFreePlanValidation();
    }
    else {
        $('#dvMu').show();
        $('#dvMs').show();
        $('#MaxUsers').rules("add", "required");
        $('#MaxStorageInGb').rules("add", "required");
    }
});

//update price per year based on price per month
$('#Price').keyup(function () {
    $('#txtPricePerYear')[0].value = $("#currencySymbol").val() + ' ' + Number($('#Price').val()) * 12;
});

function removeFreePlanValidation() {
    $('#dvMu').hide();
    $('#dvMs').hide();
    $('#MaxUsers').rules("remove", "required");
    $('#MaxStorageInGb').rules("remove", "required");
    $('#MaxUsers').val('');
    $('#MaxStorageInGb').val('');
}
