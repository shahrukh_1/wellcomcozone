﻿$(document).ready(function () {
    $('.date-picker').datepicker({ startDate: new Date() }).on('changeDate', function (ev) { $('.date-picker').datepicker('hide'); $('form').dirtyForms('setDirty'); });

    $('#newProjectFolder').click(function () {
        BindProjectFolder2Tagwords();
        BindProjectFolder2Collaborators();
    });
    $('#btnEditProject').click(function () {
        BindProjectFolder2Tagwords();
        BindProjectFolder2Collaborators();
    });
    $('#btnEditMember').click(function () {
        BindEditProjectFolder2Collaborators();
    });
});

$('#projectFolderNameId').on('change', function () {
    if ($('#projectFolderNameId')[0].value != "" && $('.NewProjectSelectedCollaborators').length > 0) {
        $('#btnAddEditProjectFolder')[0].disabled = false;
    } else {
        $('#btnAddEditProjectFolder')[0].disabled = true;
    }
});

function closeNewProjectFolderpopup() {
    $('#closePopup').click();
    window.location.reload();
}

function RemoveSelectedTagword(tagwordID) {
    if ($('.select2-selection__choice.' + tagwordID).length > 0) {
        $('.select2-selection__choice.' + tagwordID)[0].remove();
    }
}

function appendingEditedCollaborators() {
    var collCount = $('.EditProjectSelectedCollaborators').length;
    var selectedUsersId = "";
    for (var i = 0; i < collCount; i++) {
        if (selectedUsersId == "") {
            selectedUsersId = $('.EditProjectSelectedCollaborators')[i].children[1].attributes[1].value + "," + $('.EditProjectSelectedCollaborators')[i].children[1].attributes[2].value + "," + $('.EditProjectSelectedCollaborators')[i].children[1].attributes[3].value;
        } else {
            selectedUsersId = selectedUsersId + ";" + $('.EditProjectSelectedCollaborators')[i].children[1].attributes[1].value + "," + $('.EditProjectSelectedCollaborators')[i].children[1].attributes[2].value + "," + $('.EditProjectSelectedCollaborators')[i].children[1].attributes[3].value;
        }
    }
    var id = $('#hdnProjectFolderID')[0].value;
    $('#hdnSelectedCollaborators').val(selectedUsersId);
    $('#hdnselectedProjectFolderID').val(id);
    //var projectID = $('#hdnProjectFolderID')[0].value;
}
function appendingSelectedCollaborators() {
    var collCount = $('.NewProjectSelectedCollaborators').length;
    var selectedUsersId = "";
    for (var i = 0; i < collCount; i++) {
        if (selectedUsersId == "") {
            selectedUsersId = $('.NewProjectSelectedCollaborators')[i].children[1].attributes[1].value + "," + $('.NewProjectSelectedCollaborators')[i].children[1].attributes[2].value + "," + $('.NewProjectSelectedCollaborators')[i].children[1].attributes[3].value;
        } else {
            selectedUsersId = selectedUsersId + ";" + $('.NewProjectSelectedCollaborators')[i].children[1].attributes[1].value + "," + $('.NewProjectSelectedCollaborators')[i].children[1].attributes[2].value + "," + $('.NewProjectSelectedCollaborators')[i].children[1].attributes[3].value;
        }
    }
    $('#hdnSelectedUsers').val(selectedUsersId);

    var tagCount = $('.select2-selection__choice').length;
    var selectedTagwords = "";
    for (var i = 0; i < tagCount; i++) {
        if (selectedTagwords == "") {
            selectedTagwords = $('.select2-selection__choice')[i].title
        } else {
            selectedTagwords = selectedTagwords + "," + $('.select2-selection__choice')[i].title;
        }
    }
    $('#hdnSelectedTagwordIds').val(selectedTagwords);
}

function RemoveSelectedCollaborator(collaboratorID) {
    if ($('.NewProjectSelectedCollaborators.span6.' + collaboratorID).length > 0) {
        $('.NewProjectSelectedCollaborators.span6.' + collaboratorID).remove();
    }
    if ($('.EditProjectSelectedCollaborators.span6.' + collaboratorID).length > 0) {
        $('.EditProjectSelectedCollaborators.span6.' + collaboratorID).remove();
    }
    if ($('#projectFolderNameId')[0].value != "" && $('.NewProjectSelectedCollaborators').length > 0) {
        $('#btnAddEditProjectFolder')[0].disabled = false;
    } else {
        $('#btnAddEditProjectFolder')[0].disabled = true;
    }
}
function BindEditProjectFolder2Collaborators() {
    var dataObject = {
        __RequestVerificationToken: $("#EditProjectFolderMember input").val(),
        term: ""
    };
    $("#teamForEditProjectFolder").select2({
        placeholder: "Enter name or email",
        //tags: true,
        ajax: {
            delay: 500,
            type: "POST",
            url: '/ProjectFolders/GetCollaboratorsOnSearch',
            dataType: 'json',
            data: function (params) {
                $("#select2-tags-results").html('')
                if (params.term == undefined) {
                    params.term = "";
                }
                dataObject.term = params.term;
                return dataObject;
            },
            processResults: function (data) {
                var arr = []
                $.each(data.Content, function (index, value) {
                    arr.push({
                        id: value.id + "," + value.emailAddress,
                        text: value.text,
                        emailAddress: value.emailAddress
                    })
                })
                return {
                    results: arr
                };
            }
        }
    }).on("select2:unselect", function (e) {
    });
}

function BindProjectFolder2Collaborators() {
    var dataObject = {
        __RequestVerificationToken: $("#EditProjectFolderMember input").val(),
        term: ""
    };
    $("#teamForNewProjectFolder").select2({
        placeholder: "Enter name or email",
        tags: true,
        ajax: {
            delay: 500,
            type: "POST",
            url: '/ProjectFolders/GetCollaboratorsOnSearch',
            dataType: 'json',
            data: function (params) {
                $("#select2-tags-results").html('')
                if (params.term == undefined) {
                    params.term = "";
                }
                dataObject.term = params.term;
                return dataObject;
            },
            processResults: function (data) {
                var arr = []
                $.each(data.Content, function (index, value) {
                    arr.push({
                        id: value.id + "," + value.emailAddress,
                        text: value.text,
                        emailAddress: value.emailAddress
                    })
                })
                return {
                    results: arr
                };
            }
        }
    }).on("select2:unselect", function (e) {
    });
}

var newProjectTagwordsIds = [];
var projectSelectedTagwords = [];

function BindProjectFolder2Tagwords() {
    var dataObject = {
        __RequestVerificationToken: $("#EditProjectFolderMember input").val(),
        term: "",
        selectid: ""
    };
    $("#tagsForNewProjectFolder").select2({
        placeholder: "Add Tag word",
        tags: true,
        templateSelection: function (data, container) {
            $(container).css("color", "")
            if (newProjectTagwordsIds.includes(parseInt(data.id))) {
                $(container).css("background-color", "#ffd892");
            }
            return data.text;
        },
        createTag: function (tag) {
            return {
                id: tag.term,
                text: tag.term,
                isNew: true
            };
        },
        ajax: {
            delay: 500,
            type: "POST",
            url: '/Approvals/GetApprovalsTagwordsOnSearch',
            dataType: 'json',
            data: function (params) {
                $("#select2-tags-results").html('')
                if (params.term == undefined) {
                    params.term = "";
                }
                var selectedIds = "";
                $.each($('.select2-selection__choice'), function (index, value) {
                    if (selectedIds == "") {
                        if (value.classList.length == 2) {
                            selectedIds = value.classList[1];
                        } else {
                            selectedIds = value.dataset.select2Id;
                        }
                    } else {
                        if (value.classList.length == 2) {
                            selectedIds = selectedIds + ',' + value.classList[1];
                        } else {
                            selectedIds = selectedIds + ',' + value.dataset.select2Id;
                        }
                    }
                })

                dataObject.term = params.term;
                dataObject.selectid = selectedIds;
                return dataObject;
            },
            processResults: function (data) {
                var arr = []
                $.each(data.Content, function (index, value) {
                    arr.push({
                        id: value.id,
                        text: value.text
                    })
                })
                return {
                    results: arr
                };
            }
        }
    }).on("select2:select", function (e) {
        if (e.params.data.isNew) {
            var selectedtag = $(this);
            $.ajax({
                url: '/Approvals/InsertApprovalsTagwords',
                type: 'POST',
                data: JSON.parse("{ \"tagword\" : \"" + e.params.data.text + "\", \"__RequestVerificationToken\" : \"" + $("#EditProjectFolderMember input").val() + "\"}"),
                success: function (result) {
                    if (result != null) {
                        selectedtag.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + result + '">' + e.params.data.text + '</option>');
                        projectSelectedTagwords.push(result);
                        $("#SelectedTagwords").val(projectSelectedTagwords);
                        newProjectTagwordsIds.push(result);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
        }
        else {

            $.each($(this).select2('val'), function (i, el) {
                if ($.inArray(el, projectSelectedTagwords) === -1) projectSelectedTagwords.push(el);
            });
            $("#SelectedTagwords").val(projectSelectedTagwords);
        }
    }).on("select2:unselect", function (e) {
        projectSelectedTagwords.splice(projectSelectedTagwords.indexOf(e.params.data.id), 1);
        $("#SelectedTagwords").val(projectSelectedTagwords);
    });
}