﻿/*
* Widget             : Folder Tree Widget v6.0
* Created By         : Prasad Dissanayake ©
* Last Modified Date : 2013-01-10
*/

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define(['jquery', 'jquery.ui.widget'], factory);
    }
    else {
        // Browser globals:
        factory(window.jQuery);
    }
}
(function ($) {
    'use strict';

    $.widget('codeFlex.folderTree', {
        options: {
            // The namespace used for event handler binding on the access widget.
            // If not set, the name of the widget ("codeFlex") is used.
            namespace: undefined,
            // The access widget DOM id
            domid: undefined,
            // The access widget
            widget: '#folderTree',
            // The access widget control div
            control: 'cfftControl',
            // The access widget base div
            base: 'cfftBase',
            // Next node index
            index: 1,
            // Current selected option
            activeoption: 0,
            // The user creator
            creator: 0,
            // Ajax request status 1=begin, 2=success, 3=process, 4=ok
            status: 4,
            // Selected Folder id
            folder: 0,
            // Selected Parent Folder id
            parentfolder: 0,
            // Trigger
            trigger: undefined,
            // Permissions Trigger
            externaltrigger: undefined,
            // Widget panel label span html
            folderhtml: undefined,
            //Selected approvals
            selectedapprovals: undefined,
            //Selected folders
            selectedfolders: undefined,
            // Show widget panel(is lie on the page)
            showpanel: false,
            // Refesh Page after modify permissions
            refresh: false,
            //Selected folder that should be moved,
            currentfolder: 0,
            //current tab from Collaborate dashboard
            currenttab: -1,
            // Widget header html
            headerhtml: undefined,
            //is popup triggered by display folder button
            isDisplayFolder: false
        },

        _onToggleFolder: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            var domid = options.domid;

            var blockid = $(this).parents('.block:first').attr('id');
            if ($('#' + domid + '_blocks #' + blockid + ' > .block-content').hasClass('open')) {
                $('#' + domid + '_blocks #' + blockid + ' > .block-content').removeClass('open');
            }
            else {
                $('#' + blockid + ' > .block-content').addClass('open');
            }
        },

        _onSubmit: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            var domid = options.domid;
            e.preventDefault();

            $('#' + domid + '_actions #allCollDec').hide();

            var active = $('#' + domid + '_blocks .block[widget-id=' + options.folder + '] > .block-content').hasClass('active');
            if (active) {
                $('#' + domid + '_events button[value=submit] .widget-apply').addClass('hide');
                $('#' + domid + '_events button[value=submit] .widget-select').removeClass('hide');
            }
            else {
                $('#' + domid + '_events button[value=submit] .widget-apply').removeClass('hide');
                $('#' + domid + '_events button[value=submit] .widget-select').addClass('hide');
            }

            $('#' + domid + '_data .widget-id input[type=hidden]').val(options.folder);
            $(that.options.widget + ' .foldertree-panel button.label span ').html((active ? $('#' + domid + '_blocks .block[widget-id=' + options.folder + '] > .block-content > .title .action').html().trim() : that.options.folderhtml));

            var action = $(this).attr('action');
            if ((that.options.status == 4) && (action != '') && !options.showpanel) {
                $('#' + that.options.control + ' [data-dismiss=modal]').hide();

                var location = window.location.href;
                var existingFolders = $('#' + that.options.domid + '_data .widget-existing input[type=hidden]').val().trim();

                var formatedUrl = '/Widget/' + action;
                var dataObject = {
                    __RequestVerificationToken: $("#" + $(this).parents('form').attr('id') + " input").val(),
                    Folder: options.folder,
                    SelectedApprovals: options.selectedapprovals.toString(),
                    SelectedFolders: options.selectedfolders.toString(),
                    ExistingFolders: existingFolders.toString(),
                    CurrentTab: options.currenttab
                };
                $.ajax({
                    type: 'POST',
                    url: formatedUrl,
                    dataType: 'json',
                    data: dataObject,
                    success: function (data) {
                        if (data != "HttpRequestValidationException") {
                            that.options.status = 3;
                            that._rePopulate(that, data);
                        }
                        that._showHideSubmmitButtons(options);
                    },
                    complete: function (data) {
                        if (data != "HttpRequestValidationException") {
                            that.options.status = 4;
                            that._disableActions(that, false);
                            $('#' + that.options.control + ' [data-dismiss=modal]').show();
                            if (options.refresh) {
                                window.location = location; //window.location.reload();
                            }
                        }
                        that._showHideSubmmitButtons(options);
                    }
                });
            }
        },

        _onChangeSelect: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            var domid = options.domid;
            e.preventDefault();

            //Hide all coll dec req checkbox, needed only when creating new folders
            $('#' + domid + '_actions #allCollDec').hide();

            var blockid = $(this).parents('.block:first').attr('id');
            var folderid = $('#' + domid + '_blocks #' + blockid).attr('widget-id');
            var active = $('#' + domid + '_blocks #' + blockid + ' > .block-content').hasClass('active');

            that._showHideSubmmitButtons(options);

            $('#' + domid + '_blocks .block .block-content, #' + domid + '_blocks .block .item-group').removeClass('active');
            $(options.trigger).attr('widget-id', (active ? 0 : folderid));

            $('#moveToFolder').attr('disabled', 'disabled');
            $('#displayFolder').attr('disabled', 'disabled');

            options.folder = active ? 0 : folderid;

            if (active) {
                //$('#' + domid + '_actions button[name=rename]').hide();
                $('#' + domid + '_actions button[name=delete]').hide();
                $('#' + domid + '_events button[value=submit] .widget-apply').removeClass('hide');
                $('#' + domid + '_events button[value=submit] .widget-select').addClass('hide');
            }
            else {
                $('#' + domid + '_blocks #' + blockid + ' > .block-content, #' + domid + '_blocks #' + blockid + ' > .block-content > .title .item-group').addClass('active');
                //$('#' + domid + '_actions button[name=rename]').show();
                if ($(this).hasClass('delete')) {
                    $('#' + domid + '_actions button[name=delete]').show();
                }
                else {
                    $('#' + domid + '_actions button[name=delete]').hide();
                }
                $('#moveToFolder').removeAttr('disabled');
                $('#displayFolder').removeAttr('disabled');
                $('#' + domid + '_events button[value=submit] .widget-apply').addClass('hide');
                $('#' + domid + '_events button[value=submit] .widget-select').removeClass('hide');
            }
        },

        _onClickCreate: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            var domid = options.domid;
            e.preventDefault();

            $('#' + domid + '_actions .empty-block-toolong').addClass('hide');
            var foldername = $('#' + domid + '_actions input[name=folder-name][type=text]').val().trim();
            if (foldername.toString().length > 64) {
                $('#' + domid + '_actions .empty-block-toolong').removeClass('hide');
                return 0;
            }

            $('#' + domid + '_actions input[name=folder-name][type=text]').val('');
            if ((that.options.status == 4) && (foldername != '')) {
                var allColDec = $('#' + domid + '_actions input[name=AllCollaboratorsDecisionRequired][type=checkbox]').is(':checked');
                that._sendRequest(that, 0, foldername, options.folder, false, allColDec);
            }
        },

        _onClickRename: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            var domid = options.domid;
            e.preventDefault();

            $('#' + domid + '_actions .empty-block-toolong').addClass('hide');
            var foldername = $('#' + domid + '_actions input[name=folder-name][type=text]').val().trim();
            if (foldername.toString().length > 64) {
                $('#' + domid + '_actions .empty-block-toolong').removeClass('hide');
                return 0;
            }
            $('#' + domid + '_actions input[name=folder-name][type=text]').val('');
            if ((that.options.status == 4) && (foldername != '')) {
                var allColDec = $('#' + domid + '_actions input[name=AllCollaboratorsDecisionRequired][type=checkbox]').is(':checked');
                that._sendRequest(that, options.folder, foldername, 0, false, allColDec);
            }
        },

        _onClickDelete: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            e.preventDefault();

            that._sendRequest(that, options.folder, '', 0, true, false);
            options.folder = 0; options.parentfolder = 0;
        },

        _sendRequest: function (that, folder, name, parent, isdelete, allColDecReq) {
            if (that.options.status == 4) {
                $('#' + that.options.control + ' [data-dismiss=modal]').hide();
                that.options.status = 1;
                that._disableActions(that, true);

                var existingFolders = $('#' + that.options.domid + '_data .widget-existing input[type=hidden]').val().trim();

                var formatedUrl = '/Widget/FolderCRD';
                var dataObject = {
                    __RequestVerificationToken: $("#" + $(this).parents('form').attr('id') + " input").val(),
                    Folder: folder,
                    Name: name,
                    IsPrivate: 0,
                    Parent: parent,
                    IsDelete: isdelete,
                    ExistingFolders: existingFolders,
                    AllCollaboratorsDecisionRequired: allColDecReq
                };

                $.ajax({
                    type: 'POST',
                    url: formatedUrl,
                    dataType: 'json',
                    data: dataObject,
                    success: function (data) {
                        if (data != "HttpRequestValidationException") {
                            that.options.status = 3;
                            that._rePopulate(that, data);
                        }
                        that._showHideSubmmitButtons(that.options);
                    },
                    complete: function (data) {
                        if (data != "HttpRequestValidationException") {
                            that.options.status = 4;
                            that._disableActions(that, false);
                            $('#' + that.options.control + ' [data-dismiss=modal]').show();
                        }
                        that._showHideSubmmitButtons(that.options);
                    }
                });
            }
        },

        _rePopulate: function (that, data) {
            var options = that.options;
            var domid = options.domid;
            var control = options.control;
            var ns = options.namespace;

            // Unbind Events
            $('#' + control + ' #' + domid + '_blocks .block .toggler')
                .unbind('click.' + ns, { folderTree: that }, that._onToggleFolder);

            $('#' + control + ' #' + domid + '_blocks .block .action')
                .unbind('click.' + ns, { folderTree: that }, that._onChangeSelect);

            //$('#' + domid + '_actions button[name=rename]').hide();
            $('#' + domid + '_actions button[name=delete]').hide();

            var activeid = undefined;
            $('#' + domid + '_blocks .foldertree.folder-shared .block').remove();
            $('#' + domid + '_blocks .foldertree.folder-shared').html(data);

            $('#' + domid + '_blocks .block').each(function () {
                var blockid = domid + '_b_' + that.options.index;
                $(this).attr('id', blockid);
                that.options.index++;

                if ($('#' + blockid + ' > .block-content').hasClass('active')) {
                    activeid = blockid;
                }
            });

            // Bind Events
            $('#' + control + ' #' + domid + '_blocks .block .toggler')
                .bind('click.' + ns, { folderTree: that }, that._onToggleFolder);

            $('#' + control + ' #' + domid + '_blocks .block .action')
                .bind('click.' + ns, { folderTree: that }, that._onChangeSelect);

            // Selecet Create/Edit folder
            if (activeid != undefined) {
                $('#' + activeid + ' > .block-content, #' + activeid + ' > .block-content > .title .item-group').removeClass('active');
                $('#' + activeid + ' .action').click();
            }
            $('.foldertree .block .block-content.active').parents('.block-content').addClass('open');
        },

        _disableActions: function (that, disable) {
            var domid = that.options.domid;
            if (disable) {
                $('#' + domid + '_actions button').attr('disabled', 'disabled');
            }
            else {
                $('#' + domid + '_actions button').removeAttr('disabled');
            }
        },

        _onTrigger: function (e) {
            var that = e.data.folderTree;
            var options = that.options;
            var domid = options.domid;

            //show all folder tree items
            $('#' + domid + '_blocks .block > .block-content').show();
            $('#' + domid + '_blocks .block').show();

            //hide the "NOTE:..." text from folder tree popup
            $('div[class="pull-left moveFolder"]').hide();
         
            //hide the informational message from popup tree when there is at least one folder
            options.isDisplayFolder = ($(this).attr('widget-display-folder') != undefined) ? $(this).attr('widget-display-folder') : false;
            that._showHideSubmmitButtons(options);

            if (!options.showpanel) {
                options.selectedfolders = ($(this).attr('widget-folders') != undefined) ? $(this).attr('widget-folders') : "";
                options.currentfolder = ($(this).attr('widget-folder') != undefined) ? $(this).attr('widget-folder') : 0;

                //check whether the event is triggered from action menu item or directly from the page
                if (options.currentfolder == 0) {
                    options.selectedapprovals = ($(this).attr('widget-approvals') != undefined) ? $(this).attr('widget-approvals') : "";
                } else {
                    options.selectedapprovals = "";
                }

                var triggerIsButton = e.currentTarget.className == "btn";

                var headerhtml = "";
                if (!options.isDisplayFolder) {
                    headerhtml = (triggerIsButton ? $('.widget-title-moveFile').text() : $('.widget-title-moveFolder').text());
                } else {
                    headerhtml = $('.widget-title-display').text();
                }
                $('#' + options.control + ' h3.widget-title').html(headerhtml);

                if (!triggerIsButton) {
                    options.selectedfolders = "";
                    $('div[class="pull-left moveFolder"]').show(); //show the "NOTE:..." text from folder tree popup when folder is selected from action menu
                }

                options.selectedfolders = (options.selectedfolders != "" ? options.selectedfolders : (options.currentfolder != 0 ? options.currentfolder : ""));

                //hide the folders, in folder tree popup, that were selected to be moved
                var parent, foldersCnt, isTopLevel, isFirstLevel;
                var folders = options.selectedfolders.split(',');
                for (var i = 0; i < folders.length; i++) {
                    parent = $('#' + domid + '_blocks .block[widget-id=' + folders[i] + ']').parents('li.block:first');
                    $('#' + domid + '_blocks .block[widget-id=' + folders[i] + '] > .block-content').hide();
                    foldersCnt = $('#' + domid + '_blocks .block[widget-id=' + folders[i] + ']').parent().children().length;
                    isTopLevel = $('#' + domid + '_blocks .block[widget-id=' + folders[i] + ']').hasClass('level0');
                    isFirstLevel = $('#' + domid + '_blocks .block[widget-id=' + folders[i] + ']').hasClass('level1');
                }

                //hide the parent of the selcted folders if all its folders where selected
                if (parent.find('ul:first').children().length == folders.length) {
                    parent.hide();
                }

                //check whether in folders tree there is only one folder or one folder with only on child
                //if so, then show the informational message in folders tree popup
                if ($('.foldertree.browse > li').length == 0 || (foldersCnt == 1 && isTopLevel) || (isFirstLevel && $('.level0').length <= 2 && $('.level1').length <= 2)) {
                    var noFolders = (options.isDisplayFolder || options.showpanel ? $('.widget-text-cannot-display').text() : $('.widget-text-cannot-move').text());
                    $('.foldertree-base .text-center').text(noFolders);
                    $('#moveToFolder').attr('disabled', 'disabled');
                    $('#displayFolder').attr('disabled', 'disabled');
                }
            }

            options.folder = $('#' + domid + '_data .widget-id input[type=hidden]').val();
            if (options.folder > 0) {
                $('#' + domid + '_blocks .block[widget-id=' + options.folder + '] > .block-content').addClass('active');
                $('#' + domid + '_blocks .block[widget-id=' + options.folder + '] > .block-content > .title .item-group').addClass('active');
            }
            else {
                $('#' + domid + '_blocks .block .block-content').removeClass('active');
                $('#' + domid + '_blocks .block .block-content > .title .item-group').removeClass('active');
            }

            $('.foldertree .block .block-content.active').parents('.block-content').addClass('open');

            $('#' + that.options.control).modal({ backdrop: 'static', keyboard: false });
        },

        _modifyPermissions: function (options, sender) {
            var domid = options.domid;
            if (options.externaltrigger != undefined) {
                var existingusers = ($(sender).attr('widget-users') != undefined) ? $(sender).attr('widget-users') : '[]';
                var existinggroups = ($(sender).attr('widget-groups') != undefined) ? $(sender).attr('widget-groups') : '[]';

                $(options.externaltrigger).attr('widget-folder-users', existingusers);
                $(options.externaltrigger).attr('widget-groups', existinggroups);

                var targetid = ($(this).attr('widget-id') != undefined) ? parseInt($(this).attr('widget-id')) : 0;
                $(options.externaltrigger).attr('widget-id', ((targetid == -1) ? -2 : -1)).click();
                $(options.externaltrigger).attr('widget-id', targetid);
            }
        },

        _resetBlocks: function (options) {
            var domid = options.domid;
            options.folder = 0; options.parentfolder = 0;

            //$('#' + domid + '_actions button[name=rename]').hide();
            $('#' + domid + '_actions button[name=delete]').hide();
            $('#' + domid + '_blocks .active').removeClass('active');
            $('#' + domid + '_blocks .block').removeClass('active').removeClass('open');

            var folder = parseInt($('#' + domid + '_data .widget-id input[type=hidden]').val());
            if ((folder > 0) && (options.externaltrigger != undefined)) {
                $('#' + domid + '_blocks .block[widget-id=' + folder + '] > .block-content > .title .action').click();
                $('#' + domid + '_events button[type=button][value=submit]').click();
            }
        },

        _initControls: function () {
            var options = this.options;
            var domid = options.domid;

            if (options.showpanel) {
                options.folderhtml = $(options.widget + ' .foldertree-panel .label span').html().trim();
            }

            var controlid = domid + '_control';
            $(options.widget + ' .foldertree-control').attr('id', controlid);
            options.control = controlid;

            options.headerhtml = $('#' + options.control + ' h3.widget-title').html();

            var baseid = domid + '_base';
            $('#' + options.control + ' .foldertree-base').attr('id', baseid);
            options.base = baseid;

            var actionsid = domid + '_actions';
            $('#' + options.base + ' .foldertree-actions').attr('id', actionsid);

            var blocksid = domid + '_blocks';
            $('#' + options.base + ' .foldertree-blocks').attr('id', blocksid);

            var dataid = domid + '_data';
            $('#' + options.base + ' .foldertree-data').attr('id', dataid).hide();

            var eventsid = domid + '_events';
            $('#' + options.control + ' .foldertree-events').attr('id', eventsid);

            $('#' + domid + '_blocks .block').each(function () {
                $(this).attr('id', domid + '_b_' + options.index);
                options.index++;
            });
        },

        _generateDOMID: function () {
            var chars, newchar, rand;
            chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            newchar = 'cfft';
            for (var i = 0; i < 3; i++) {
                rand = Math.floor(Math.random() * chars.length);
                newchar += chars.substring(rand, rand + 1);
            }
            return newchar;
        },

        _showHideSubmmitButtons: function (options) {
            $('#displayFolder, #moveToFolder, #submit-destination').css('display', 'none');
            $('.foldertree-base .text-center').text("");
            $('#moveToFolder').attr('disabled', 'disabled');
            $('#displayFolder').attr('disabled', 'disabled');

            if (options.showpanel) {
                $('#submit-destination').css('display', 'block');
            } else {
                if (!options.isDisplayFolder) {
                    $('#moveToFolder').css('display', 'block');
                } else {
                    $('#displayFolder').css('display', 'block');
                }
            }

        },

        _initEventHandlers: function (that) {
            var options = that.options;
            var domid = options.domid;
            var control = options.control;
            var ns = options.namespace;
            that._showHideSubmmitButtons(options);

            $('*[widget-toggle=foldertree]')
                .bind('click.' + ns, { folderTree: that }, that._onTrigger);

            $('#' + control + ' #' + domid + '_events button[type=button][value=submit]')
                .bind('click.' + ns, { folderTree: that }, that._onSubmit);

            $('#' + control + ' #' + domid + '_blocks .block .toggler')
                .bind('click.' + ns, { folderTree: that }, that._onToggleFolder);

            $('#' + control + ' #' + domid + '_blocks .block .action')
                .bind('click.' + ns, { folderTree: that }, that._onChangeSelect);

            $('#' + control + ' #' + domid + '_actions button[name=create]')
                .bind('click.' + ns, { folderTree: that }, that._onClickCreate);

            //$('#' + control + ' #' + domid + '_actions button[name=rename]')
            //    .bind('click.' + ns, { folderTree: that }, that._onClickRename);

            $('#' + control + ' #' + domid + '_actions button[name=delete]')
                .bind('click.' + ns, { folderTree: that }, that._onClickDelete);
        },

        _destroyEventHandlers: function (that) {
            var options = that.options;
            var domid = options.domid;
            var control = options.control;
            var ns = options.namespace;

            $('*[widget-toggle=foldertree]')
                .unbind('click.' + ns, { folderTree: that }, that._onTrigger);

            $('#' + control + ' #' + domid + '_events button[type=button][value=submit]')
                .unbind('click.' + ns, { folderTree: that }, that._onSubmit);

            $('#' + control + ' #' + domid + '_blocks .block .toggler')
                .unbind('click.' + ns, { folderTree: that }, that._onToggleFolder);

            $('#' + control + ' #' + domid + '_blocks .block .action')
                .unbind('click.' + ns, { folderTree: that }, that._onChangeSelect);

            $('#' + control + ' #' + domid + '_actions button[name=create]')
                .unbind('click.' + ns, { folderTree: that }, that._onClickCreate);

            //$('#' + control + ' #' + domid + '_actions button[name=rename]')
            //    .unbind('click.' + ns, { folderTree: that }, that._onClickRename);

            $('#' + control + ' #' + domid + '_actions button[name=delete]')
                .unbind('click.' + ns, { folderTree: that }, that._onClickDelete);
        },

        _create: function () {
            var widget = ('#' + this.element[0].id),
                options = this.options;

            if ($(widget + ' .foldertree-base').length > 0) {
                options.domid = this._generateDOMID();
                options.namespace = options.namespace || this.namespace;
                options.widget = widget;
                this._initControls();
                this._initEventHandlers(this);
                this._resetBlocks(options);
            }
        },

        destroy: function () {
            this._destroyEventHandlers();
            $.Widget.prototype.destroy.call(this);
        }
    });
}));