﻿/*
* Widget             : Group Controler Widget v2.4
* Created By         : Prasad Dissanayake ©
* Last Modified Date : 2012-10-22
*/

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define(['jquery', 'jquery.ui.widget'], factory);
    }
    else {
        // Browser globals:
        factory(window.jQuery);
    }
}
(function ($) {
    'use strict';

    $.widget('codeFlex.collaboratorGroup', {
        options: {
            // The namespace used for event handler binding on the group control.
            // If not set, the namespace of the widget ("codeFlex") is used.
            namespace: undefined,
            // The access control
            control: '#collaboratorGroup',
            // The access control input base div
            base: 'collaboratorGroupBase',
            // Set the following option to the location of a redirect url on the
            // origin server, for widget handler
            redirect: undefined,
            // Selected users count
            count: 0,
            // Reset user blocks when loaded
            resetblocks: true,
            // Widget data
            data: {
                // Text value for Members text
                memberstext: 'Members'
            }
        },

        _onChangeFilter: function (e) {
            e.preventDefault();
            var that = e.data.collaboratorGroup;
            var base = that.options.base;

            $(base + ' .collaboratorgroup-blocks').removeClass('a-active-only a-search-only');
            if (e.target.name == 'members') {
                $(base + ' .collaboratorgroup-blocks').addClass('a-active-only');
                $(base + ' .collaboratorgroup-filters input[type=text]').val('');
            }

            $(base + ' .collaboratorgroup-filters ul li').removeClass('active');
            $(this).addClass('active');
        },

        _onChangeUser: function (e) {
            e.preventDefault();
            var that = e.data.collaboratorGroup;
            var base = that.options.base;
            var status = $(this).hasClass('active');
            var id = 'liControlEvent';
            $(this).attr('id', id);
            if (status) {
                $(this).removeClass('active');
                $(base + ' li[id=' + id + '] dl dd.hide input[type=checkbox]').removeAttr('checked');
            }
            else {
                $(this).addClass('active');
                $(base + ' li[id=' + id + '] dl dd.hide input[type=checkbox]').attr('checked', 'checked');
            }
            $(this).removeAttr('id');

            $('#widgetGroups').dirtyForms('setDirty');
            
            that._setBlocks(that.options);
        },

        _onKeyUpSearch: function (e) {
            var that = e.data.collaboratorGroup;
            var base = that.options.base;
            var text = e.target.value.trim().toUpperCase().replace(/\s+/g, '^');
            
            $(base + ' .collaboratorgroup-filters a[name=all]').click();
            $(base + ' .collaboratorgroup-blocks li').removeClass('active-search');
            $(base + ' .collaboratorgroup-blocks .search-results').css('display', 'none');

            if (text != '') {
                $(base + ' .collaboratorgroup-blocks').addClass('a-search-only');

                $(base + ' .collaboratorgroup-blocks li').each(function () {
                    var content = $(this).text().trim().toUpperCase();
                    content = content.replace(/ /g, '^').replace(/\s+/g, '_');
                    if (content.indexOf(text) > -1) {
                        $(this).addClass('active-search');
                    }
                });

                if ($(base + ' .collaboratorgroup-blocks .active-search').length == 0) {
                    $(base + ' .collaboratorgroup-blocks .block-empty').css('display', 'block');
                }
            }
            else {
                $(base + ' .collaboratorgroup-blocks').removeClass('a-search-only');
            }
        },

        _onSave: function (e) {
            var that = e.data.collaboratorGroup;
            if (!that._validateForm(that.options, false)) {
                return false;
            }
        },

        _validateForm: function (options, remove) {
            var base = options.base;
            var valied = false;

            var value = $(base + ' div.collaboratorgroup-fields input[type=hidden]').val();
            valied = (value != '') && (Number(value) > -1);
            if (!remove) {
                valied = valied && (options.count > 0);
                if (!(options.count > 0)) {
                    $(base + ' div.empty-blocks').show();
                }
            }
            return valied;
        },

        _setBlocks: function (options) {
            var base = options.base;
            var count = $(base + ' div.collaboratorgroup-blocks li.active').length;
            options.count = count;
            $(options.control + ' div.collaboratorgroup-filters li a[name=members]').text(options.data.memberstext + ' (' + count + ')');
            $(base + ' div.empty-blocks').hide();
        },

        _setDefault: function () {
            var control = this.options.control;
            var base = this.options.base;

            $(base + ' div.collaboratorgroup-blocks li').removeClass('active');
            $(base + ' div.collaboratorgroup-blocks li').removeClass('active-search');
            $(base + ' div.collaboratorgroup-blocks').removeClass('a-active-only');

            $(base + ' div.collaboratorgroup-filters li a[name=all]').parents('li').addClass('active');
            if (this.options.resetblocks) {
                $(base + ' div.collaboratorgroup-fields input[type=text]').text('');
                $(base + ' div.collaboratorgroup-blocks li dd input[type=checkbox]').removeAttr('checked');
            }
            else {
                $(base + ' div.collaboratorgroup-blocks li dd input[type=checkbox][checked=checked]').parents('li').addClass('active');
            }

            this.options.data.memberstext = $(base + ' div.collaboratorgroup-data .widget-text-members').html().trim();

            this._setBlocks(this.options);
        },

        _initEventHandlers: function () {
            var control = this.options.control;
            var ns = this.options.namespace;
           
            $(control + ' div.collaboratorgroup-filters ul li')
                .bind('click.' + ns, { collaboratorGroup: this }, this._onChangeFilter);

            $(control + ' div.collaboratorgroup-filters input[name=search-text][type=text]')
                .bind('keyup.' + ns, { collaboratorGroup: this }, this._onKeyUpSearch);

            $(control + ' div.collaboratorgroup-blocks ul li')
                .bind('click.' + ns, { collaboratorGroup: this }, this._onChangeUser);

            $(control + ' div.collaboratorgroup-actions button[type=submit][value=save]')
                .bind('click.' + ns, { collaboratorGroup: this }, this._onSave);
        },

        _create: function () {
            var control = ('#' + this.element[0].id),
                options = this.options;

            options.control = control;
            options.namespace = options.namespace || this.namespace;
            options.redirect = options.redirect || location.href;

            var length = $('div.collaboratorgroup-base').length;
            if (length > 0) {
                options.base += length;
                this.element.children('div.collaboratorgroup-base').attr('id', options.base);
                options.base = '#' + options.base;
                $(control + ' div.collaboratorgroup-actions input[name=RedirectURL][type=hidden]').val(options.redirect);

                this._initEventHandlers();
                this._setDefault();
            }
        }
    });
}));