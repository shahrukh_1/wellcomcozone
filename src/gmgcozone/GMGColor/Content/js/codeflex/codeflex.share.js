﻿/*
* Widget             : Share Controller v4.0
* Created By         : Prasad Dissanayake ©
* Last Modified Date : 2012-12-01
*/

function getQueryStringValue(key) {
    return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define(['jquery', 'jquery.ui.widget'], factory);
    }
    else {
        // Browser globals:
        factory(window.jQuery);
    }
}
(function ($) {
    'use strict';

    $.widget('codeFlex.collaboratorShare', {
        options: {
            // The namespace used for event handler binding on the access widget. If not set, the name of the widget ("codeFlex") is used.
            namespace: undefined,
            // The access widget DOM id
            domid: undefined,
            // The access widget
            widget: '#collaboratorAccess',
            // The access widget control div
            control: 'cfshControl',
            // The access widget base div
            base: 'cfshBase',
            // Set the following option to the location of a redirect url on the origin server, for widget handler
            redirect: undefined,
            // Selected users count
            count: 0,
            // Show widget panel(is lie on the page)
            showpanel: false,
            // Ajax request status 1=begin, 2=success, 3=process, 4=ok
            status: 4,
            // The share type(all=share & download, share, download)
            task: undefined,
            // The widge controler type(approval, folder,...)
            type: undefined,
            // Permissions Trigger
            externaltrigger: undefined,
            // Arry of existing users IDs with out creator and modifier
            existingusers: undefined,
            // Arry of existing groups IDs
            existinggroups: undefined,
            // Widget header html
            headerhtml: undefined,
            // Is accessed from deliver page
            isFromDeliver: false,
            //Is add/edit approval phase page
            isPhaseScreen: false,
            // Email validation regx
            regxemail: /^(""[^""]+""@[a-zA-Z0-9\-_]+[\.][a-zA-Z0-9\.\-_]+)$|^([a-zA-Z0-9\\\/$!%=_\.+\-""]+@[a-zA-Z0-9\-_]+[\.]+[a-zA-Z0-9\.\-_]*)$/,
            // Widget data
            data: {
                // The approval
                approval: 0,
                // Text value for approval
                approvaltext: 'approval',
                // Text vale for folder
                foldertext: 'folder',
                // Target type of widget
                type: false,
                // Block Template
                blocktemplate: undefined,
                // The next id
                nextid: 1,
                // Emails with role
                emailswithoption: new Array(),
                // Users with role
                userswithoption: new Array(),
                // Data modification level. 0: None, 1: Insert, 2:update, 3:delete
                modifylevel: 0
            }
        },

        _onChangeShare: function (e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var domid = options.domid;

            if ($(this).hasClass('share')) {
                if (e.target.checked) {
                    $('#' + domid + '_blocks').removeClass('disable-roles');
                }
                else {
                    $('#' + domid + '_blocks').addClass('disable-roles');
                    $('#' + domid + '_blocks').find('.block.external.active').attr('block-role', '1');
                    var rolename = $('#' + domid + '_blocks .role-selector .role [block-role=' + '1' + ']').html().trim();
                    $('#' + domid + '_blocks .block-role').html(rolename);
                    options.data.modifylevel = 2;
                }
            }

            if ($('#' + domid + '_filters input[type=checkbox]:checked').length > 0) {
                $('#' + domid + '_fields .message').removeAttr('disabled');
            }
            else {
                $('#' + domid + '_fields .message').attr('disabled', 'disabled');
            }
            if ($('#' + domid + '_filters').hasClass('error')) {
                $('#' + domid + '_filters').removeClass('error');
                $('#' + domid + '_filters .validation').hide();
            }
        },

        //this function will be triggered only for deliver users and from new deliver job page
        _onChangeSelect: function (e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var $block = $(this).parents('.block:first');
            
            //select/deselect only existing users
            if (options.isFromDeliver && !$block.hasClass('disabled') && $block.attr('block-id') > 0) {
               
                var user = parseInt($block.attr('block-id')); //existing user id
                var curroption = parseInt($block.attr('block-preset')); // preset id

                var pstvalue = user + '|' + curroption;
                
                if ($block.hasClass('active')) {
                    $block.removeClass('active');
                    options.data.modifylevel = 3; //remove relation with this job for current existing user
                }
                else {
                    $block.addClass('active');
                    options.data.modifylevel = 1; //add relation with this job for current existing user
                }
         
                that._updateWidgetData(options, false, pstvalue);
                that._updateExternalTigger(options);
            }
            else {
                e.preventDefault();
            }
        },
        
        _onChangeOption: function (e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var domid = options.domid;

            //this variable will replace the name of the class or attribute, depending from where the action was triggered,
            //from deliver or collaborate dashboards
            //'preset' and 'role' refers to the options types from the dropdown related with the new user
            var option = options.isFromDeliver ? 'preset' : 'role';

            var blockid = $(this).parents('.block').attr('id');
            var user = parseInt($('#' + blockid).attr('block-id'));
            var curroption = parseInt($(this).attr('block-' + option));
            
            // Special Case
            var email = '', newUser = '', name = '', prevalue = '', pstvalue = '';
            var applychanges = (blockid != domid + '_b_0');
            if (applychanges) {
                newUser = (user == 0);
                name = $('#' + blockid + ' .block-name').html().trim();
                if (newUser) {
                    var language = $('#ext-language').val();
                    email = $('#' + blockid + ' .block-email').html().trim();
                    pstvalue = email + '|' + curroption + '|' + name + '|' + language;
                }
                else {
                    pstvalue = user + '|' + curroption;
                }
            }

            // Assign Option (option cand be Preset or Role)
            if (curroption > 0) {
                var preoption = $('#' + blockid).attr('block-' + option);
                var optionname = $('#' + blockid + ' .' + option + '-selector .' + option + ' [block-'+ option +'=' + curroption + ']').html().trim();
                $('#' + blockid + ' .'+ option +'-selector').attr('pre-'+ option, preoption);
                $('#' + blockid + ' .block-'+ option).html(optionname);
                $('#' + blockid).attr('block-'+ option, curroption);

                options.data.modifylevel = 2; //update
                if (applychanges) {
                    prevalue = email + '|' + preoption + '|' + name;
                }
                else {
                    prevalue = user + '|' + preoption;
                }
            }
            // Remove User
            else {
                pstvalue = email + '|' + parseInt($('#' + blockid).attr('block-'+ option));
                if (user == 0) {
                    pstvalue += '|' + name;
                }
                else {
                    pstvalue = user + pstvalue;
                }
                $('#' + blockid).removeClass('active').addClass('remove');

                options.data.modifylevel = 3; // delete
            }
            
            //if is add/edit phase page update decision maker dropdown 
            if (options.isPhaseScreen) {
                that._syncDecisionMakerList(domid, user, curroption);
            }

            if (applychanges) {
                that._updateWidgetData(options, newUser, pstvalue, prevalue);
                that._updateExternalTigger(options);
            }
        },

        //add only the users with approver and reviewer role, otherwise remove them from  Decision Maker dropdown (Phase page)
         _syncDecisionMakerList: function(domid, user, role) {
             if (role == 3) {
                 $('.errAccess').hide(); // hide the error message if at least one user is selected
                if ($('#primaryDecisionMaker option[value="' + user + '"][is-external="true"]').length == 0) {
                    var index = $('#primaryDecisionMaker option').length - 1;
                    var userName = $('#' + domid + '_blocks .block.external[block-id="' + user + '"] .block-name').html();
                        $('#primaryDecisionMaker').append($("<option></option>")
                                            .attr("value", user)
                                            .attr("is-external", true)
                                            .attr("index", index)
                                            .text(userName));

                    
                    $('#decisionMaker').append('<input type="hidden" name="DecisionMakers['+ index +'].ID" value="'+ user +'"/>' +
                        '<input type="hidden" name="DecisionMakers['+ index +'].IsExternal" value="true"/>' +
                        '<input type="hidden" name="DecisionMakers[' + index + '].IsSelected" value="false"/>' +
                        '<input type="hidden" name="DecisionMakers[' + index + '].Name" value="' + userName + '"/>');
                }
                   
            } else {
                if ($('#primaryDecisionMaker option[value="' + user +'"][is-external="true"]').length > 0) {
                    var optionIndex = $('#primaryDecisionMaker  option[value="' + user +'"][is-external="true"]').attr('index');
                    
                    $('#primaryDecisionMaker option[value="' + user +'"][is-external="true"]').remove();
                    $('#decisionMaker').find('input[name^="DecisionMakers[' + optionIndex +']"]').each(function() {
                        $(this).remove();
                    });
                }
            }
        },
        
        _onTrigger: function (e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var domid = options.domid;

            var widgettask = $(e.target).attr('widget-task');
            if (widgettask != undefined && widgettask != '') {
                options.task = widgettask;
                options.externaltrigger = this;

                var targetid = ($(e.target).attr('widget-id') != undefined) ? parseInt($(e.target).attr('widget-id')) : 0;
                var targettype = ($(e.target).attr('widget-type').toLowerCase() == 'folder') ? true : false;
                var disableoptions = options.isFromDeliver ? 'disable-presets' : 'disable-roles';

                var headerhtml = options.headerhtml.trim() + '&nbsp;' + (targettype ? options.data.foldertext : options.data.approvaltext);
                $('#' + options.control + ' .widget-title').html(headerhtml);

                if (!$('#' + domid + '_blocks').hasClass(disableoptions)) {
                    $('#' + domid + '_blocks').addClass(disableoptions);
                }
                that._resetBlocks(options);

                options.data.approval = targetid;
                $('#' + domid + '_data .widget-id-approval input[type=hidden]').val(options.data.approval);
                options.data.type = targettype;
                $('#' + domid + '_data .widget-type input[type=hidden]').val(options.data.type);

                options.existingusers = ($(e.target).attr('widget-users') != undefined) ? $(e.target).attr('widget-users') : '[]';
                options.existinggroups = ($(e.target).attr('widget-groups') != undefined) ? $(e.target).attr('widget-groups') : '[]';

                if (options.existingusers) {
                    options.existingusers = options.existingusers.substring(1, (options.existingusers.length - 1));
                    options.existingusers = ((options.existingusers != '') ? options.existingusers.split(',') : []);
                }
                var userids = options.existingusers;
                for (var i = 0; i < userids.length; i++) {
                    $('#' + domid + '_fields .chzn-results li[value=' + userids[i] + ']').removeClass('result-selected').addClass('active-result');
                }
               
                $('#' + options.control).modal({ backdrop: 'static', keyboard: false });
            }
        },

        _applyModel: function (options, active) {
            if (active) {
                $('#' + options.base).addClass('modal-body');
                $('#' + options.control).removeClass('modal-clear').addClass('modal');
            }
            else {
                $('#' + options.base).removeClass('modal-body');
                $('#' + options.control).removeClass('modal').addClass('modal-clear');
            }
        },

        _updateArray: function (arr, value) {
            var user = value.match("[^\|]+\\|");
            if (user != null) user = user[0];

            if (user != null) {
                var found = false;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].match("^" + user.replace("|", "\\|")) != null) {
                        arr[i] = value;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    arr.push(value);
                }
            }
        },

        _updateWidgetData: function (options, email, pstvalue, prevalue) {
            var index;
            if (email) {
                switch (options.data.modifylevel) {
                    case 1: //insert
                    case 2: //update
                        {
                            this._updateArray(options.data.emailswithoption, pstvalue);
                            break;
                        }
                    case 3: //delete
                        {
                            index = options.data.emailswithoption.indexOf(pstvalue);
                            if (index > -1) {
                                options.data.emailswithoption.splice(index, 1);
                            }
                            break;
                        }

                }
                $('#' + options.domid + '_data .widget-data-emails input[type=hidden]').val(JSON.stringify(options.data.emailswithoption));
            }
            else {               
                switch (options.data.modifylevel) {
                    case 1: //insert
                    case 2: //update
                        {
                            this._updateArray(options.data.userswithoption, pstvalue);
                            break;
                        }
                    case 3: //delete
                        {
                            index = options.data.userswithoption.indexOf(pstvalue);
                            if (index > -1) {
                                options.data.userswithoption.splice(index, 1);
                            }                           
                            break;
                            
                        }

                }
                
                $('#' + options.domid + '_data .widget-data-users input[type=hidden]').val(JSON.stringify(options.data.userswithoption));
            }
        },

        _updateExternalTigger: function (options) {
            if (options.externaltrigger != undefined) {
                //this variable will replace the name of the attribute, depending from where the action was triggered,
                //from deliver or collaborate dashboards
                //'block-preset' and 'block-role' refers to the options types from the dropdown related with the new user
                var blockoption = options.isFromDeliver ? 'block-preset' : 'block-role';
                var externaluserswithoption = [];
       
                $('#' + options.domid + '_blocks .block.external.active').each(function () { externaluserswithoption.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr(blockoption))); });
                $(options.externaltrigger).attr('widget-external', JSON.stringify(externaluserswithoption));
                
                if ($(options.externaltrigger) != undefined && $(options.externaltrigger).attr('widget-workflow-phases') != undefined && $(options.externaltrigger).attr('widget-workflow-phases') != "") {
                    if (!options.isPhaseScreen) {
                        //check if an approval workflow with phases is selected
                        if ($(options.externaltrigger).attr('widget-workflow-phases').length > 0) {
                            //in case is access popup for an approval workflow, the external users should not be displayed (except the new added ones)
                            $('#' + options.domid + '_blocks .block.external.active').not('[block-id="0"]').css('display','none');
                        } 
                    }
                }
            }
        },

        _onInsert: function (e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var ns = options.namespace;
            var domid = that.options.domid;
            e.preventDefault();
            $('#errSelectExternalUser').hide();
            
            //this variable will replace the name of the class or attribute, depending from where the action was triggered,
            //from deliver or collaborate dashboards
            //'block-preset' and 'block-role' refers to the options types from the dropdown related with the new user
            var option = options.isFromDeliver ? 'preset' : 'role';
            
            $('#' + domid + '_blocks [class^=empty-block-]').addClass('hide');

            var collection;
            var email = $('#' + domid + '_blocks input[type=text].email').val().trim();
            
            if (email != undefined && email != '') {
                if (options.regxemail.test(email)) {
                    var update = false;
                    var blockid = $(this).parents('.block').attr('id');
                    var curroption = parseInt($('#' + blockid).attr('block-' + option));
                    var optionname = $('#' + blockid + ' .' + option + '-selector .' + option + ' [block-' + option + '=' + curroption + ']').html().trim();
        
                    var value = email + '|' + curroption;
                    var preblckid = $('#' + domid + '_blocks .block-email:contains(' + email + ')').parents('.block:first').attr('id');
                    if (preblckid != undefined) {
                            if ($('#' + preblckid).removeClass('remove').hasClass('active')) {
                                $('#' + domid + '_blocks .empty-block-email-exists').removeClass('hide');
                            } else {
                                $('#' + domid + '_blocks input[type=text].first-name, #' + domid + '_blocks input[type=text].last-name, #' + domid + '_blocks input[type=text].email').val('');
                            }
                            $('#' + preblckid).removeClass('remove').addClass('active');
                            $('#' + preblckid + ' .' + option + '-selector [block-' + option + '=' + curroption + ']').click();

                            $('#' + domid + '_blocks input[type=text].email').val('');
                    }
                    else {
                        var firstname = $('#' + domid + '_blocks input[type=text].first-name').val().trim();
                        var lastname = $('#' + domid + '_blocks input[type=text].last-name').val().trim();
                        var language = $('#ext-language').val();
                        if (!update && (firstname != undefined && firstname != '') && (lastname != undefined && lastname != '')) {
                            value += '|' + firstname + ' ' + lastname + '|' + language;
                            collection = { blockid: 0, cssclass: 'block external active', domid: options.domid, index: options.data.nextid, name: (firstname + ' ' + lastname), email: email, option: curroption, optionname: optionname };
                            var li = options.data.blocktemplate.replace(/##([A-Z]+)##/g, replaceTemplate);
                            $('#' + domid + '_blocks .block-grid.external').append(li);
                            $('#' + domid + '_b_' + options.data.nextid + ' .'+ option +'-selector [block-' + option + ']').bind('click.' + ns, { collaboratorShare: that }, that._onChangeOption);

                            options.data.modifylevel = 1; // insert
                            options.data.nextid++;
                            that._updateWidgetData(options, true, value, '');
                            that._updateExternalTigger(options);

                            $('#' + domid + '_blocks input[type=text].first-name, #' + domid + '_blocks input[type=text].last-name, #' + domid + '_blocks input[type=text].email').val('');
                        }
                        else {
                            $('#' + domid + '_blocks .empty-block-user').removeClass('hide');
                        }
                    }
                }
                else {
                    $('#' + domid + '_blocks .empty-block-email').removeClass('hide');
                }
            }
            else {
                $('#' + domid + '_blocks .empty-block-user').removeClass('hide');
            }

            function replaceTemplate(s, m) {
                switch (m) {
                    case "ID":
                        return collection.blockid;
                        break;
                    case "CLASS":
                        return collection.cssclass;
                        break;
                    case "DOMID":
                        return collection.domid;
                        break;
                    case "EMAIL":
                        return collection.email;
                        break;
                    case "INDEX":
                        return collection.index;
                        break;
                    case "NAME":
                        return collection.name;
                        break;
                    case "ROLE":
                        return collection.option;
                        break;
                    case "ROLENAME":
                        return collection.optionname;
                        break;
                    case "PRESET":
                        return collection.option;
                        break;
                    case "PRESETNAME":
                        return collection.optionname;
                        break;
                }
            }
        },

        _onSubmit: function (e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var domid = that.options.domid;
            e.preventDefault();

            var action = $(this).attr('action');
            var shareURL = ($('#' + domid + '_filters .share:checked').length > 0);
            var downloadURL = ($('#' + domid + '_filters .download:checked').length > 0);
         
            if ((that.options.status == 4) && (action != '') && (options.data.userswithoption != '' || options.data.emailswithoption != '') && (shareURL || downloadURL)) {
                $('#' + domid + '_filters').removeClass('error');
                $('#' + domid + '_filters .validation').hide();
                $('#' + domid + '_blocks [class^=empty-block-]').addClass('hide');

                $('#' + that.options.control + ' [data-dismiss=modal]').hide();
                $(this).button('loading');

                that.options.status = 1;

                var id = options.data.approval;
                var currentPhase = $(options.externaltrigger).attr('widget-phase');
                var message = $('#' + domid + '_fields .message').val() || $('#' + domid + '_fields .message').text();

                var location = window.location;
                var currentApproval = getQueryStringValue("selectedApproval"); // needed to fix problem when modifying permissions from approval details and after refresh current version was reseted
                if (currentApproval != "") {
                    location = location.href.replace("selectedApproval=" + currentApproval, "selectedApproval=" + id);
                }
                var formatedUrl = '/Widget/' + action;
                var dataObject = {
                    __RequestVerificationToken: $("#" + $(this).parents('form').attr('id') + " input").val(),
                    Approval: id,
                    ExternalUsers: options.data.userswithoption.toString(),
                    ExternalEmails: options.data.emailswithoption.toString(),
                    IsShareURL: shareURL,
                    IsShareDownloadURL: downloadURL,
                    Message: message
                };
                if (currentPhase > 0) {
                    dataObject.CurrentPhase = currentPhase;
                }

                $.ajax({
                    type: 'POST',
                    url: formatedUrl,
                    dataType: 'json',
                    data: dataObject,
                    success: function (data) {
                        that.options.status = 3;
                    },
                    complete: function (data) {
                        that.options.status = 4;
                        $('#' + domid + '_events button[value=submit]').button('reset');
                        $('#' + that.options.control + ' [data-dismiss=modal]').show();
                        $('#' + domid + '_events [data-dismiss=modal]').click();
                        if (options.data.emailswithoption != undefined && options.data.emailswithoption.length > 0 || (options.data.userswithoption != undefined && options.data.userswithoption.length > 0)) {
                            window.location = location;
                        }
                    }
                });
            }
            else {
                window.setTimeout('$(\'#' + domid + '_events button[value=submit]\').button(\'reset\')', 100);
                if ((options.data.userswithoption == '') && (options.data.emailswithoption == '')) {
                    $('#' + domid + '_blocks .empty-block-share').removeClass('hide');
                }
                if (!shareURL && !downloadURL) {
                    $('#' + domid + '_filters').addClass('error');
                }
                $('#' + domid + '_fields .validation').show();
            }
        },

        _resetBlocks: function (options) {
            var domid = options.domid;
            options.count = 0;
           
            $('#' + domid + '_fields .share').removeAttr('checked').parents('.controls').hide();
            $('#' + domid + '_fields .download').removeAttr('checked').parents('.controls').hide();
            if (options.task == 'share') {
                $('#' + domid + '_fields .download').attr('checked', 'checked').parents('.controls').show();
            }
            else if (options.task == 'download') {
                $('#' + domid + '_fields .share').attr('checked', 'checked').parents('.controls').show();
            }
            else {
                $('#' + domid + '_fields .share').attr('checked', 'checked').parents('.controls').show();
                $('#' + domid + '_fields .download').parents('.controls').show();
            }

            $('#' + domid + '_fields .validation').hide();
            $('#' + domid + '_fields input[type=text], #' + domid + '_fields textarea').val('');
            $('#' + domid + '_events button[value=submit]').button('reset');

            $('#' + domid + '_filters input[type=checkbox]').removeAttr('checked');
            
            //display all existing users except new added ones
            $('#' + domid + '_blocks .block-grid .block [block-id="0"]').each(function() {
               $('#' + domid + '_blocks .block-grid .block').removeClass('active').addClass('remove');
            });
                       
            $('#' + domid + '_blocks input[type=text].first-name, #' + domid + '_blocks input[type=text].last-name, #' + domid + '_blocks input[type=text].email').val('');
            $('#' + domid + '_fields .message').attr('disabled', 'disabled').val('');

            options.data.userswithoption = new Array();
            options.data.emailswithoption = new Array();
        },

        //this function is triggered only for 'add new job popup' from collaborate dashboard (ApprovalGrid.cshtml)
        _onClose : function(e) {
            var that = e.data.collaboratorShare;
            var options = that.options;
            var domid = options.domid;
            
            //reset all blocks with existing external users
             $('#' + domid + '_blocks .block-grid .block').each(function() {
                 
                 var blockid = $(this).attr('block-id');
                 //if user is new, remove him. Otherwise, make him inactive
                 if (blockid == 0) {
                    $(this).removeClass('active').addClass('remove');
                 } else {
                    $(this).removeClass('active remove'); 
                 }
            });
            
            options.data.userswithoption = new Array();//clear selected user(s)
            
           //empty the message box
            $('#deliverMessage').val('');
            $('#errSelectExternalUser').hide();
        },
        
        _initEventHandlers: function () {
            var control = this.options.control;
            var ns = this.options.namespace;
            var domid = this.options.domid;

            if (!this.options.isFromDeliver) {
                $(document)
                    .on('click.' + ns, '*[widget-toggle=collaboratorshare]', { collaboratorShare: this }, this._onTrigger);
            } else {
                $(document)
                    .on('click.' + ns, '*[widget-toggle=closedeliverpopup]', { collaboratorShare: this }, this._onClose);
            }

            $('#' + control + ' button[type=button][value=submit]')
                .bind('click.' + ns, { collaboratorShare: this }, this._onSubmit);

            $('#' + control + ' button[type=button][value=insert]')
                .bind('click.' + ns, { collaboratorShare: this }, this._onInsert);

            $('#' + control + ' .collaboratorshare-filters input[type=checkbox]')
                .bind('click.' + ns, { collaboratorShare: this }, this._onChangeShare);
            
            $('#' + control + ' #' + domid + '_blocks .block .user-selector')
                .bind('click.' + ns, { collaboratorShare: this }, this._onChangeSelect);
            
            $('#' + control + ' #' + domid + '_blocks .block .role-selector [block-role]')
                .bind('click.' + ns, { collaboratorShare: this }, this._onChangeOption);
            
            $('#' + control + ' #' + domid + '_blocks .block .preset-selector [block-preset]')
                .bind('click.' + ns, { collaboratorShare: this }, this._onChangeOption);
        },

        _destroyEventHandlers: function () {
            var control = this.options.control;
            var ns = this.options.namespace;
            var domid = this.options.domid;

            $(document)
                .off('click.' + ns, '*[widget-toggle=collaboratorshare]', { collaboratorShare: this }, this._onTrigger);

            $('#' + control + ' button[type=button][value=submit]')
                .unbind('click.' + ns, { collaboratorShare: this }, this._onSubmit);

            $('#' + control + ' button[type=button][value=insert]')
                .unbind('click.' + ns, { collaboratorShare: this }, this._onInsert);

            $('#' + control + ' .collaboratorshare-filters input[type=checkbox]')
                .unbind('click.' + ns, { collaboratorShare: this }, this._onChangeShare);
            
            $('#' + control + ' #' + domid + '_blocks .block .user-selector')
                .unbind('click.' + ns, { collaboratorShare: this }, this._onChangeSelect);

            $('#' + control + ' #' + domid + '_blocks .block .role-selector [block-role]')
                .unbind('click.' + ns, { collaboratorShare: this }, this._onChangeOption);
            
            $('#' + control + ' #' + domid + '_blocks .block .preset-selector [block-preset]')
                .unbind('click.' + ns, { collaboratorShare: this }, this._onChangeOption);
            
            $(document)
                .off('click.' + ns, '*[widget-toggle=closedeliverpopup]', { collaboratorShare: this }, this._onClose);
        },

        _initControls: function () {
            var options = this.options;
            var domid = this.options.domid;

            var controlid = domid + '_control';
            $(options.widget + ' .collaboratorshare-control').attr('id', controlid);
            options.control = controlid;

            options.headerhtml = $('#' + options.control + ' .widget-title').html();

            var baseid = domid + '_base';
            $('#' + options.control + ' .collaboratorshare-base').attr('id', baseid);
            options.base = baseid;

            var filterid = domid + '_filters';
            $('#' + options.base + ' .collaboratorshare-filters').attr('id', filterid);

            var blocksid = domid + '_blocks';
            $('#' + options.base + ' .collaboratorshare-blocks').attr('id', blocksid);

            var fieldsid = domid + '_fields';
            $('#' + options.base + ' .collaboratorshare-fields').attr('id', fieldsid);

            var dataid = domid + '_data';
            $('#' + options.base + ' .collaboratorshare-data').attr('id', dataid);

            var eventsid = domid + '_events';
            $('#' + options.control + ' div.collaboratorshare-events').attr('id', eventsid);

            $('#' + domid + '_blocks .block').each(function () {
                var wid = $(this).attr('block-id');
                var user = $(this).hasClass('user');

                if (!user && wid == '0') {
                    $(this).attr('id', domid + '_b_0');
                }
                else {
                    $(this).attr('id', domid + '_b_' + options.data.nextid);
                    options.data.nextid++;
                }
            });

            options.data.approval = parseInt($('#' + domid + '_data .widget-id-approval input[type=hidden]').val());
            options.data.approvaltext = $('#' + domid + '_data .widget-text-approval').html().trim();
            options.data.foldertext = $('#' + domid + '_data .widget-text-folder').html().trim();
            options.data.type = $('#' + domid + '_data .widget-type input[type=hidden]').val().toLowerCase().trim();
            options.data.blocktemplate = $('#' + domid + '_blocks .block-template').html().trim();

            /* PrasaD 2012-12-01 Release v4.0 */
            this._applyModel(options, !options.showpanel);
        },

        _generateDOMID: function () {
            var chars, newchar, rand;
            chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            newchar = 'cfsh';
            for (var i = 0; i < 3; i++) {
                rand = Math.floor(Math.random() * chars.length);
                newchar += chars.substring(rand, rand + 1);
            }
            return newchar;
        },

        _create: function () {
            var widget = ('#' + this.element[0].id),
                options = this.options;

            if ($(widget + ' .collaboratorshare-base').length > 0) {
                options.domid = this._generateDOMID();
                options.namespace = options.namespace || this.namespace;
                options.widget = widget;
                this._initControls();
                this._initEventHandlers();
                this._resetBlocks(this.options);
            }
        },

        destroy: function () {
            this._destroyEventHandlers();
            $.Widget.prototype.destroy.call(this);
        }
    });
}));