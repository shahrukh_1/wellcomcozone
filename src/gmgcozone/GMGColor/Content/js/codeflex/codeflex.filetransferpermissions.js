﻿/*
* Widget             : Permissions v7.0
* Created By         : Prasad Dissanayake ©
* Last Modified Date : 2013-01-10
*/

function getQueryStringValue(key) {
    return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define(['jquery', 'jquery.ui.widget'], factory);
    }
    else {
        // Browser globals:
        factory(window.jQuery);
    }
}
(function ($) {
    'use strict';

    $.widget('codeFlex.fileTransferAccess', {
        options: {
            // The namespace used for event handler binding on the access widget.
            // If not set, the name of the widget ("codeFlex") is used.
            namespace: undefined,
            // The access widget DOM id
            domid: undefined,
            // The access widget
            widget: '#fileTransferAccess',
            // The access widget control div
            control: 'cfcaControl',
            // The access widget base div
            base: 'cfcaBase',
            // Selected users count
            count: 0,
            // The widge controler type(approval, folder,...)
            type: undefined,
            // Show widget panel(is lie on the page)
            showpanel: false,
            // The user creator
            creator: 0,
            // The user modifier
            modifier: 0,
            // Ajax request status 1=begin, 2=success, 3=process, 4=ok
            status: 4,
            // Trigger
            trigger: undefined,
            // Permissions Trigger
            externaltrigger: undefined,
            // Arry of locked users IDs
            lockedusers: undefined,
            // Arry of locked external users IDs
            exlockedusers: undefined,
            // Arry of existing users IDs
            existingusers: undefined,
            // Arry of existing groups IDs
            existinggroups: undefined,
            // Arry of external user IDs
            externalusers: undefined,
            // Widget header html
            headerhtml: undefined,
            // Widget members html
            membershtml: undefined,
            // Freeze events
            freeze: false,
            // Refesh Page after modify permissions
            refresh: false,
            //the current trigger
            currentTrigger: undefined,
            // Reset to initial values
            canReset: false,
            //check if is triggered for phase
            isPhaseScreen: false,
            //repopulates popup when workflow is changed
            canReload: false,
            //all phases from current workflow, if any
            phases: undefined,
            //is new approval page
            isNewApproval: false,
            //Apply parent access permission to the child folders and jobs
            applyParentPermissions: false,
            //decision type PDM or Group Approval
            decisionType: 0,
            //do page refresh
            doPageRefresh: false,
            // Widget data,
            data: {
                // The user owner
                owner: 0,
                // The user primary decition maker
                pdm: 0,
                // Text value for approval
                approvaltext: 'approval',
                // Text vale for folder
                foldertext: 'folder',
                // Text value for phase
                phasetext: 'phase',
                // Text value for modifier
                modifiertext: 'me',
                // Text value for owner
                ownertext: 'owner',
                // Text value for primary decition maker text
                pdmtext: 'pdm',
                //all internal users available on access popup
                defaultUsers: undefined,
                //is fo the first phase in workflow
                currentphase: 0,
                //previous workflow id
                prevworkflow: 0,
                //check for selected PDM user (external or not)
                pdmIsExternal: false
            }
        },

        _onChangeFilter: function (e) {
            e.preventDefault();
            var that = e.data.collaboratorAccess;
            var domid = that.options.domid;

            $('#' + domid + '_blocks').removeClass('a-active-only a-search-only');
            if ($(e.target).attr('data-filter') == 'members') {
                $('#' + domid + '_blocks .block-header.group, #' + domid + '_blocks .block.group, #' + domid + '_blocks [class=block-empty]').css('display', 'none');
                $('#' + domid + '_blocks').addClass('a-active-only');
                $('#' + domid + '_filters input[type=text]').val('');
            }
            else {
                $('#' + domid + '_blocks .block-header.group, #' + domid + '_blocks .block.group').removeAttr('style');
            }
            $('#' + domid + '_filters ul.nav-pills li').removeClass('active');
            $(this).parents('li:first').addClass('active');
        },

        _onKeyUpSearch: function (e) {
            var that = e.data.collaboratorAccess;
            var domid = that.options.domid;
            var text = e.target.value.trim().toUpperCase().replace(/\s+/g, '_');

            $('#' + domid + '_filters [data-filter=all]').click();
            $('#' + domid + '_blocks .block').removeClass('active-search');
            $('#' + domid + '_blocks .search-results').css('display', 'none');

            if (text != '') {
                $('#' + domid + '_blocks').addClass('a-search-only');

                $('#' + domid + '_blocks .block').each(function () {
                    var content = $(this).text().trim().toUpperCase();
                    content = content.replace(/ /g, '^').replace(/\s+/g, '_');
                    if (content.indexOf(text) > -1) {
                        $(this).addClass('active-search');
                    }
                });

                if ($('#' + domid + '_blocks .block.group.active-search').length == 0) {
                    $('#' + domid + '_blocks .block-empty.group').css('display', 'block');
                }
                if ($('#' + domid + '_blocks .block.user.active-search').length == 0) {
                    $('#' + domid + '_blocks .block-empty.user').css('display', 'block');
                }
            }
            else {
                $('#' + domid + '_blocks').removeClass('a-search-only');
            }
        },

        _onChangeSelect: function (e) {
            var that = e.data.collaboratorAccess;
            var options = that.options;
            var domid = that.options.domid;
            var $block = $(this).parents('.block:first');

            if (!$('#' + options.control).hasClass('modal-clear') && !$block.hasClass('disabled')) {
                var id = $block.attr('id');
                var role = parseInt($block.attr('block-role'));

                var checked = $('#' + id + ' .block-status input[type=checkbox]').attr('checked');
                if (checked) {
                    $block.removeClass('active');
                    $('#' + id + ' .block-status input[type=checkbox]').removeAttr('checked');
                }
                else {
                    $block.addClass('active');
                    $(options.externaltrigger).attr('update-data', true);
                    $('#' + id + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                }

                //if is not the "add new approval" page and if is "Group Approval" decision type, then refresh the Collaborate dashboard in order to update grid data
                if (role == 3 && options.isNewApproval == false && options.decisionType == 2) {
                    options.doPageRefresh = true;
                }

                var isgroup = $block.hasClass('group');
                var classes = $block.attr('class').replace('block', '').replace('active-search', '').replace('active', '').replace('group', '').replace('user', '').trim();
                if (isgroup && !checked) {
                    $('#' + domid + '_blocks .block.user.' + classes).addClass('active');
                    $('#' + domid + '_blocks .block.user.' + classes + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                }
                else {
                    if (isgroup) {
                        var groupkey = classes; classes = new Array(); classes.push(groupkey);

                        $('#' + domid + '_blocks .block.user.' + groupkey).each(function () {
                            if (!$block.hasClass('disabled')) {
                                var groupkeys = $block.attr('class').replace('block', '').replace('active-search', '').replace('active', '').replace('group', '').replace('user', '').trim().split(' ');
                                for (var i = 0; i < groupkeys.length; i++) {
                                    if (classes.indexOf(groupkeys[i]) == -1) {
                                        classes.push(groupkeys[i]);
                                    }
                                }
                            }
                        });

                        $('#' + domid + '_blocks .block.user.' + groupkey).removeClass('active');
                        $('#' + domid + '_blocks .block.user.' + groupkey + ' .block-status input[type=checkbox]').removeAttr('checked');

                        $('#' + domid + '_blocks .block.user.disabled').addClass('active');
                        $('#' + domid + '_blocks .block.user.disabled .block-status input[type=checkbox]').attr('checked', 'checked');
                    }
                    else {
                        classes = classes.split(' ');
                    }

                    for (var i = 0; i < classes.length; i++) {
                        if (classes[i] != '') {
                            var x = $('#' + domid + '_blocks .block.user.' + classes[i] + ' .block-status input[type=checkbox]').length;
                            var y = $('#' + domid + '_blocks .block.user.' + classes[i] + ' .block-status input[type=checkbox]:checked').length;
                            if (x != y) {
                                $('#' + domid + '_blocks .block.group.' + classes[i]).removeClass('active');
                                $('#' + domid + '_blocks .block.group.' + classes[i] + ' .block-status input[type=checkbox]').removeAttr('checked');
                            }
                        }
                    }
                }
                that._setCount(that.options);
            }
            else {
                e.preventDefault();
            }
        },

        _onChangeRole: function (e) {
            var that = e.data.collaboratorAccess;
            var options = that.options;
            var domid = options.domid;

            var blockid = $(this).parents('.block').attr('id');
            var user = parseInt($('#' + blockid).attr('block-id'));
            var role = parseInt($(this).attr('block-role'));

            if (role < 0) {
                var labelclass = 'label-info';
                var preuser = options.data.owner;
                var preblockid = $('#' + domid + '_blocks .block.user[block-id=' + preuser + ']').attr('id') || blockid;
                switch (role) {
                    case -1: // Owner
                        {
                            // Unhide all                            
                            $('#' + domid + '_blocks .block .role-selector, #' + domid + '_blocks .block .role-selector .settings').removeClass('hide');
                            $('#' + domid + '_blocks .block .block-name span.label:contains(' + options.data.ownertext + ')').remove();
                            options.data.owner = user;
                            $('#' + blockid).addClass('active disabled');
                            $('#' + blockid + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                            $('#' + blockid + ' .block-name').append('<span class="label ' + labelclass + '">' + options.data.ownertext + '</span>');
                            if (blockid != domid + '_b_1') {
                                $('#' + domid + '_b_1 .block-name').append('<span class="label label-success">' + options.data.modifiertext + '</span>');
                            }

                            // If previous owner may be creator or modifier then shoud not uncheck and not remove the disabled class
                            if ((preuser != options.creator) && (preuser != options.modifier)) {
                                $('#' + preblockid).removeClass('disabled');
                                $('#' + preblockid + ' .block-status input[type=checkbox]').removeAttr('checked');
                            }

                            $('#' + blockid + ' .role-selector .settings').addClass('hide');

                            if (options.isNewApproval === false ||
                                (options.isNewApproval === true && options.decisionType != 1)) {
                                $('#' + domid + '_blocks .block .role-selector .settings.pdm').addClass('hide');
                                $('#' + domid + '_blocks .block.external .role-selector ul.dropdown-menu ')
                                    .each(function (index, selector) {
                                        if ($(selector).find('li.settings').not('.hide').length <= 2) {
                                            $(selector).find('li.settings').addClass('hide');
                                        }
                                    });

                                $('#' + domid + '_blocks .block.user .role-selector ul.dropdown-menu ')
                                    .each(function (index, selector) {
                                        if ($(selector).find('li.settings').not('.hide').length <= 2) {
                                            $(selector).find('li.settings').addClass('hide');
                                        }
                                    });

                            } else {
                                if (options.isNewApproval === true && options.decisionType == 1) {
                                    $('#' + blockid + ' .role-selector .settings').removeClass('hide');
                                    $('#' + blockid + ' .role-selector .settings.owner').addClass('hide');
                                }
                            }

                            // Approval onwer must be in [Approver & Reviewer] role
                            role = parseInt($('#' + blockid + ' .role-selector .role.approver a').attr('block-role'));
                            // set the approval owner
                            $(".collaboratoraccess-data .widget-id-owner input[type=hidden]").val($('#' + blockid).attr("block-id"));
                            break;
                        }
                    case -2:
                        {
                            $('#' + domid + '_blocks .block .role-selector, #' + domid + '_blocks .block .role-selector .settings').removeClass('hide');
                            $('#' + domid + '_blocks .block .block-name span.label:contains(' + options.data.pdmtext + ')').remove();
                            $('#' + domid + '_blocks .block').removeClass("pdm disabled");

                            $('#' + domid + '_blocks .block.user[block-id=' + options.data.owner + '] .role-selector .settings.owner').addClass('hide');
                            options.data.pdm = user;
                            options.data.pdmIsExternal = $('#' + blockid).hasClass('external');
                            role = 3;//if pdm set Approver and Reviewer role automatically

                            $('#' + blockid).addClass('active pdm disabled');
                            $('#' + blockid + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                            $('#' + blockid + ' .block-name').append('<span class="label ' + labelclass + '">' + options.data.pdmtext + '</span>');

                            $('#' + blockid + ' .role-selector .settings.pdm').addClass('hide');

                            break;
                        }
                    case -3: // Remove
                        {
                            $('#' + options.control).removeClass('modal-clear');
                            $('#' + blockid + ' .user-selector').click();
                            $('#' + options.control).addClass('modal-clear');
                            break;
                        }
                }
            }

            $('#' + domid + '_blocks .block.external .role-selector ul.dropdown-menu ').each(function (index, selector) {
                if ($(selector).find('li.settings').not('.hide').length <= 2) {
                    $(selector).find('li.settings').addClass('hide');
                }
            });

            $('#' + domid + '_blocks .block.user .role-selector ul.dropdown-menu ').each(function (index, selector) {
                if ($(selector).find('li.settings').not('.hide').length <= 2) {
                    $(selector).find('li.settings').addClass('hide');
                }
            });

            //if is add/edit phase page update decision maker dropdown 
            if (options.isPhaseScreen) {
                $('#' + domid + '_blocks .block.group.active').each(function () { groups.push(parseInt($(this).attr('block-id'))); });
                var users = [];
                $('#' + domid + '_blocks .block.user.active').each(function () { users.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr('block-role'))); });
                var externalusers = [];
                $('#' + domid + '_blocks .block.external.active').each(function () { externalusers.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr('block-role'))); });

                that._syncApproversAndReviewersLists(domid, users, externalusers);
            }

            if (role > 0) {
                if (role == 3) {
                    options.doPageRefresh = (options.decisionType == 2);//if is Group Approval decision type and  added role is Approver and reviewer, then refresh the Collaborate dashboard in order to update grid data
                    $('.errAccess').hide();
                    $('#approverAndReviewerErr').hide();
                }

                that._assignRole(blockid, role);
            }
            if ($('#' + options.control).hasClass('modal-clear')) {
                $('#widgetPermissions').dirtyForms('setDirty');
                $(options.externaltrigger).attr('update-data', false);
                if (!options.showpanel) {
                    $('#' + domid + '_events button[type=button][value=submit]').click();
                }
            }
        },

       

        _onSubmit: function (e) {
            var that = e.data.collaboratorAccess;
            var options = that.options;
            var domid = that.options.domid;
            e.preventDefault();

            var groups = [];
            $('#' + domid + '_blocks .block.group.active').each(function () { groups.push(parseInt($(this).attr('block-id'))); });
            var users = [];
            $('#' + domid + '_blocks .block.user.active').each(function () { users.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr('block-role'))); });
            var externalusers = [];
            $('#' + domid + '_blocks .block.external.active').each(function () { externalusers.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr('block-role'))); });
            if (options.isPhaseScreen) {
                that._syncApproversAndReviewersLists(domid, users, externalusers);
            }

            //in case job is assigned to an approval workflow, check if at least one user has approver and reviewer role on current phase
            if ((options.decisionType != undefined && options.decisionType != 0) && that._checkForApproverAndReviewer(users, externalusers, options) == false) {
                return false;
            }

            if (options.data.currentphase != 0) {
                that._updateCurrentPhase(domid, options.data.currentphase, $.parseJSON($('#PhasesCollaborators').val()));
                $('#' + domid + '_data .widget-dirty-data').val($('#PhasesCollaborators').val());
                $(options.externaltrigger).attr('widget-workflow-phases', $('#PhasesCollaborators').val());
            }

            if ((options.externaltrigger != undefined) && !($(options.externaltrigger).attr('update-data') == 'false')) {
                $(options.externaltrigger).attr('update-data', true);
                $(options.externaltrigger).attr('widget-groups', JSON.stringify(groups));
                $(options.externaltrigger).attr('widget-users', JSON.stringify(users));
                $(options.externaltrigger).attr('widget-external', JSON.stringify(externalusers));
                $(options.externaltrigger).attr('widget-owner', options.data.owner);
                $(options.externaltrigger).attr('widget-decision', options.data.pdm);
                $(options.externaltrigger).attr('widget-is-external', options.data.pdmIsExternal);
            } else {
                $(options.externaltrigger).attr('update-data', true);
            }

            if (options.showpanel) {
                if (that.options.count > 0 && users.length != that.options.count) {
                    $('#widgetPermissions').dirtyForms('setDirty');
                } else {
                    $('#widgetPermissions').dirtyForms('setClean');
                }

                if (!options.freeze) {
                    options.freeze = true;
                    $('#' + domid + '_blocks .block.user.active-search').removeClass('active-search');
                    $('#' + domid + '_events [data-dismiss=modal]').click();
                    options.freeze = false;
                }
            }
            else {
                var action = $(this).attr('action');
                if ((that.options.status == 4) && (action != '')) {
                    $('#' + that.options.control + ' [data-dismiss=modal]').hide();
                    $(this).button('loading');

                    var id = $(options.trigger).attr('widget-id');
                    var phaseId = $(options.trigger).attr('widget-phase');
                    var isfolder = ($(options.trigger).attr('widget-type') == 'folder');
                    var sendemails = true;
                    var location = window.location;
                    var currentApproval = getQueryStringValue("selectedApproval"); // needed to fix problem when modifying permissions from approval details and after refresh current version was reseted
                    if (currentApproval != "") {
                        location = location.href.replace("selectedApproval=" + currentApproval, "selectedApproval=" + id);
                    }

                    var formatedUrl = '/Widget/' + action;
                    var dataObject = {
                        __RequestVerificationToken: $("#" + $(this).parents('form').attr('id') + " input").val(),
                        ID: id,
                        Groups: groups.toString(),
                        Users: users.toString(),
                        ExternalUsers: externalusers.toString(),
                        Creator: options.creator,
                        Modifier: options.modifier,
                        Owner: options.data.owner,
                        IsFolder: isfolder,
                        SendEmails: sendemails
                    };

                    if (phaseId > 0) {
                        dataObject.CurrentPhase = phaseId;
                    }

                    $.ajax({
                        type: 'POST',
                        url: formatedUrl,
                        dataType: 'json',
                        data: dataObject,
                        success: function (data) {
                            that.options.status = 3;
                            $(options.trigger).attr('widget-groups', JSON.stringify(groups));
                            $(options.trigger).attr('widget-users', JSON.stringify(users));
                            $(options.trigger).attr('widget-external', JSON.stringify(externalusers));
                            $(options.trigger).attr('widget-owner', options.data.owner);
                        },
                        complete: function (data) {
                            that.options.status = 4;
                            $('#' + domid + '_events button[value=submit]').button('reset');
                            $('#' + that.options.control + ' [data-dismiss=modal]').show();
                            $('#' + domid + '_events [data-dismiss=modal]').click();

                            if (options.refresh || (isfolder && options.applyParentPermissions) || options.doPageRefresh) {
                                window.location = location; //window.location.reload();
                            }
                        }
                    });
                }
            }
            options.count = users.length;
            that._setCount(options);
        },

       

        _onCancel: function (e) {
            var that = e.data.collaboratorAccess;
            var options = that.options;
            var domid = that.options.domid;

            //check whether the access popup is opened from new approval page or not
            if (options.showpanel) {
                $('#' + domid + '_blocks .search-results').css('display', 'none');
                e.preventDefault();

                if (!options.freeze) {
                    options.freeze = true;
                    $(options.externaltrigger).click();
                    options.freeze = false;
                }

                if (options.data.currentphase != 0) {
                    $('#PhasesCollaborators').val($('#' + domid + '_data .widget-dirty-data').val());
                    //display all existing users except new added ones
                    that._applyModel(options, true);
                } else {
                    that._applyModel(options, false);
                }

            } else {
                //set this option to true in order reset the values to their initial state
                options.canReset = true;

                // the refresh option is set to true only when the access pop up is opened from version details page
                // otherwise is set to false
                // the lines below (with click event) are needed in order to reset popup to initial state 
                if (options.refresh) {
                    $(options.trigger).click();
                } else {
                    $(options.currentTrigger).click();
                }
                options.canReset = false;
            }
        },

      

        _applyModel: function (options, active) {
            if (active) {
                $('#' + options.base).addClass('modal-body');
                $('#' + options.control).removeClass('modal-clear').addClass('modal');
                $('#' + options.domid + '_blocks .a-user-selector').removeClass('newApproval');
                $('#' + options.domid + '_base').animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#' + options.base).removeClass('modal-body');
                $('#' + options.control).removeClass('modal').addClass('modal-clear');
                $('#' + options.domid + '_blocks .a-user-selector').addClass('newApproval');
            }
        },

        _setCount: function (options) {
            var domid = options.domid;
            var count = $('#' + domid + '_blocks .block.user.active, #' + domid + '_blocks .block.external.active').length;
            $('#' + domid + '_filters [data-filter=members]').html(options.membershtml + ' (' + count + ')');
        },

       

        _alignActiveTabToCenter: function () {
            var activeTabLeftPos = $('ul.nav-tabs li.active').offset().left;
            var activeTabWidth = $('ul.nav-tabs li.active').width();

            var scrollingDivLeftPos = $('.scrolling-container').offset().left;
            var scrollingDivWidth = $('.scrolling-container').width();
            var scrollingDivRighttPos = scrollingDivLeftPos + scrollingDivWidth;

            var scrollingDivMidPos = scrollingDivLeftPos + (scrollingDivWidth / 2);
            var activeTabMid = (activeTabWidth / 2);

            var lastTabRightPos = ($('ul.nav-tabs li:last').offset().left + $('ul.nav-tabs li:last').width());
            var firstTabLeftPos = $('ul.nav-tabs li:first').offset().left;

            if (firstTabLeftPos > scrollingDivLeftPos && lastTabRightPos < scrollingDivRighttPos) {
                return false;
            }

            //if active tab is partial visible on the right side or the left side of the scrolling div
            //then move the active tab to center of the scrolling div
            if ((activeTabLeftPos + activeTabWidth) > scrollingDivRighttPos) {
                var newLeft = (activeTabLeftPos - scrollingDivMidPos) + activeTabMid;
                $('.scrolling-container').animate({ scrollLeft: '-=' + newLeft * (-1) }, 400);
            } else {
                var newRight = (scrollingDivMidPos - (activeTabLeftPos + activeTabWidth)) + activeTabMid;
                $('.scrolling-container').animate({ scrollLeft: '-=' + newRight * 1 }, 400);
            }

            window.setTimeout('showHideArrow()', 300);
        },

       

        _onTrigger: function (e) {
            var that = e.data.collaboratorAccess;
            var options = that.options;
            var domid = options.domid;
            var targetid = ($(this).attr('widget-id') != undefined) ? parseInt($(this).attr('widget-id')) : 0;
            var targettype = ($(this).attr('widget-type').toLowerCase() == 'folder') ? true : false;

            var widgetid = parseInt($(options.trigger).attr('widget-id') || 0);
            var widgettype = (($(options.trigger).attr('widget-type') || 'false').toLowerCase() === 'true');

            var headerhtml = options.headerhtml.trim() + '&nbsp;' + (options.isPhaseScreen ? options.data.phasetext : (targettype ? options.data.foldertext : options.data.approvaltext));
            $('#' + options.control + ' h3.widget-title').html(headerhtml);

            if (targetid > 0) {
                options.trigger = this;
                options.externaltrigger = $(this).parents('ul:first').find('[widget-toggle=collaboratorshare]') || undefined;
            }

            //the current trigger needs to be kept in order to be able to update or reset his values when the cancel event is triggered
            options.currentTrigger = $(this).parents('ul:first').find('[widget-toggle=collaboratoraccess]') || undefined;
            options.decisionType = options.data.currentphase == 0 && options.isNewApproval ? "" : $(this).attr("widget-decision-type");

            var folderusers = undefined;
            // (targettype == options.type) check was added in order to avoid the case when folder access popup is shown instead of the approval access popup
            // this is the case when access popup for a folder with id = x is opened and after that an approval, with same id as folder id, is opened imediatly after folder access popup is closed
            if (!(targettype == widgettype && targetid == widgetid && targettype == options.type) || options.canReset) {
                options.type = targettype;
                options.creator = $(this).attr('widget-creator') && parseInt($(this).attr('widget-creator'));
                options.lockedusers = ($(this).attr('widget-disabled') != undefined) ? $(this).attr('widget-disabled') : '[]';
                options.exlockedusers = ($(this).attr('widget-exdisabled') != undefined) ? $(this).attr('widget-exdisabled') : '[]';
                options.existinggroups = ($(this).attr('widget-groups') != undefined) ? $(this).attr('widget-groups') : '[]';
                options.existingusers = ($(this).attr('widget-users') != undefined) ? $(this).attr('widget-users').replace(/\"/g, '') : '[]';
                options.externalusers = ($(this).attr('widget-external') != undefined) ? $(this).attr('widget-external').replace(/\"/g, '') : '[]';
                options.data.owner = ($(this).attr('widget-owner') != undefined) ? parseInt($(this).attr('widget-owner')) : 0;
                options.phases = ($(this).attr('widget-phases') != undefined && $(this).attr('widget-phases') != "") ? JSON.parse($(this).attr('widget-phases')) : [];
                options.data.pdm = ($(this).attr('widget-decision') != undefined) ? parseInt($(this).attr('widget-decision')) : 0;
                options.data.pdmIsExternal = ($(this).attr('widget-is-external') != undefined) && $(this).attr('widget-is-external').toLowerCase() == "true";
                // If the PDM is external or if the PDM is already present - made for job details part
                if (options.data.pdmIsExternal && options.data.pdm == 0) {
                    options.data.pdm = ($(this).attr('widget-external-pdm') != undefined) ? parseInt($(this).attr('widget-external-pdm')) : 0;
                }

                folderusers = ($(this).attr('widget-folder-users') != undefined) ? $(this).attr('widget-folder-users') : undefined;
                if (folderusers != undefined) {
                    var existingusers = options.existingusers;
                    options.existingusers = $.merge(folderusers, existingusers);
                    $(this).removeAttr('widget-folder-users');
                }

                that._resetBlocks(options);
                if (options.lockedusers) {
                    options.lockedusers = options.lockedusers.substring(1, (options.lockedusers.length - 1));
                    options.lockedusers = ((options.lockedusers != '') ? options.lockedusers.split(',') : []);
                }
                if (options.exlockedusers) {
                    options.exlockedusers = options.exlockedusers.substring(1, (options.exlockedusers.length - 1));
                    options.exlockedusers = ((options.exlockedusers != '') ? options.exlockedusers.split(',') : []);
                }
                if (options.existinggroups) {
                    options.existinggroups = options.existinggroups.substring(1, (options.existinggroups.length - 1)).trim();
                    options.existinggroups = ((options.existinggroups != '') ? options.existinggroups.split(',') : []);
                }
                if (options.existingusers) {
                    options.existingusers = options.existingusers.substring(1, (options.existingusers.length - 1));
                    options.existingusers = ((options.existingusers != '') ? options.existingusers.split(',') : []);
                }
                if (options.externalusers) {
                    options.externalusers = options.externalusers.substring(1, (options.externalusers.length - 1));
                    options.externalusers = ((options.externalusers != '') ? options.externalusers.split(',') : []);
                }

                if ($(this).attr('widget-phase') == undefined || $(this).attr('widget-phase') == "") {
                    options.data.currentphase = 0;
                    options.data.prevworkflow = 0;
                }

                $('#' + domid + '_phases').removeClass('phase');

                options.canReload = false;
                if (options.phases.length > 0) {
                    var phases = options.phases;
                    $('#' + domid + '_phases ul.nav-tabs li').remove();

                    //loop through all phases from the current workflow, if any, and add a tab for each of them
                    for (var i = 0; i < phases.length; i++) {
                        var phase = phases[i];
                        if (options.data.prevworkflow != parseInt(phase.WorkflowId)) {
                            options.data.prevworkflow = parseInt(phase.WorkflowId);
                            options.data.currentphase = parseInt(phase.PhaseId);
                            $('#PhasesCollaborators').val($(this).attr('widget-workflow-phases'));
                            $('#' + domid + '_data .widget-dirty-data').val($(this).attr('widget-workflow-phases'));
                        }

                        if ($(this).attr('widget-phase') == undefined || $(this).attr('widget-phase') == "") {
                            $(this).attr('widget-phase', phase.PhaseId);
                            options.canReload = true;
                        }
                        var active = ($(this).attr('widget-phase') == phase.PhaseId) ? "active" : "";
                        var activeTab = ($(this).attr('widget-phase') == phase.PhaseId) ? "phase-tab" : "";

                        //add a tab with phase name   
                        $('#' + domid + '_phases ul.nav-tabs').append('<li class=' + active + ' ><a class=' + activeTab + ' href="#phase' + phase.PhaseId + '" data-phase="' + phase.PhaseId + '" data-toggle="tab">' + phase.PhaseName + '</a></li>');
                    }

                    //force (re)populating popup with colaborators from the first phase only when workflow is changed
                    if (options.canReload) {
                        $('#' + domid + '_phases ul.nav-tabs li:first a').click();

                        //calculate the phase "ul" width in order to avoid displaying of the tabs in two lines
                        var width = 0;
                        $('ul.nav-tabs li').each(function () {
                            width += $(this).width();
                        });

                        //change the scrolling div width depending on arrows (left, right) visibility
                        if ((width + 50) > 540) {
                            $('#left, #right').css('display', 'block');
                            $('.scrolling-container').width('489px');
                        } else {
                            $('#left, #right').css('display', 'none');
                            $('.scrolling-container').width('500px');
                            width = 500;
                        }

                        $('#ulDiv').css('min-width', width + 50);//set the min width for the scrolling div
                        that._alignActiveTabToCenter();
                    }

                    if (!$('#' + domid + '_phases').hasClass('phase')) {
                        $('#' + domid + '_phases').addClass('phase');
                    }
                }

                var groupids = options.existinggroups;
                for (var i = 0; i < groupids.length; i++) {
                    var id = $('#' + options.domid + '_blocks .block.group[block-id=' + groupids[i] + ']').attr('id');
                    $('#' + id + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                    $('#' + id).addClass('active');
                }

                var userids = options.existingusers;
                for (var i = 0; i < userids.length; i++) {
                    var userrole = userids[i].split('|');
                    var blockid = $('#' + options.domid + '_blocks .block.user[block-id=' + userrole[0] + ']').attr('id');
                    if (blockid != undefined) {
                        if (userrole[1] == undefined || userrole[1] == null) {
                            userrole[1] = $('#' + blockid + ' .role-selector').attr('pre-role');
                        }
                        $('#' + blockid + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                       
                        $('#' + blockid).addClass('active');

                       // that._assignRole(blockid, userrole[1]);
                    }
                }
                if (!options.type) {
                    var externalids = options.externalusers;
                    for (var i = 0; i < externalids.length; i++) {
                        var externaluserrole = externalids[i].split('|');
                        var externalblockid = $('#' + options.domid + '_blocks .block.external[block-id=' + externaluserrole[0] + ']').attr('id');
                        if (externaluserrole[1] == undefined || externaluserrole[1] == null) {
                            externaluserrole[1] = $('#' + externalblockid + ' .role-selector').attr('pre-role');
                        }
                        if (externalblockid != undefined) {
                            $('#' + externalblockid + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                            $('#' + externalblockid).addClass('active');

                            //that._assignRole(externalblockid, externaluserrole[1]);
                        }
                    }
                }
                options.count = userids.length;
                that._setCount(options);
                $('#pdmApproverAndReviewerErr').hide();
                if (targettype) { /* if folder */
                    $("#widgetPermissions div.a-user-selector ul.user li.user dd.block-role").hide();
                    $("#widgetPermissions div.a-user-selector ul.user li.user dt.block-name").addClass("permissionsFolderUserName");
                    $('#widgetPermissions div.a-user-selector .all-roles').parent().hide();
                }
                else {
                    $("#widgetPermissions div.a-user-selector ul.user li.user dd.block-role").show();
                    $("#widgetPermissions div.a-user-selector ul.user li.user dt.block-name").removeClass("permissionsFolderUserName");

                    if (!options.showpanel) {
                        $('#widgetPermissions div.a-user-selector .all-roles').parent().show();
                    }
                }
               // that._resetUserRoles(options, domid);//reset the roles to default for the users that are not collaborators for the current item
            }
            if ((!options.freeze) && (folderusers == undefined) || options.canReload) {
                that._applyModel(options, true);
                $('#' + options.control).modal({ backdrop: 'static', keyboard: false });
            }
        },

        _resetBlocks: function (options) {
            var domid = options.domid;
            $('#' + domid + '_filters [data-filter=all]').parents('li').addClass('active');
            $('#' + domid + '_filters [data-filter=members]').parents('li').removeClass('active');
            $('#' + domid + '_blocks .block .block-name span.label:contains(' + options.data.pdmtext + ')').remove();
            $('#' + domid + '_filters input[type=text]').val('');

            $('#' + domid + '_blocks .block .block-status input[type=checkbox]').removeAttr('checked');
            $('#' + domid + '_blocks .block').removeClass('active active-search disabled');
            $('#' + domid + '_blocks .block.user.active-search').removeClass('active-search');
            $('#' + domid + '_blocks .block.user .block-name span.label').remove();
            $('#' + domid + '_blocks .block .role-selector, #' + domid + '_blocks .block .role-selector .settings').removeClass('hide');
            $('#' + domid + '_blocks .block .role-selector .settings.pdm').addClass('hide');
            $('#' + domid + '_blocks').removeClass('a-active-only a-search-only ');
            $('#' + domid + '_blocks [class=block-empty]').css('display', 'none');
            $('#' + domid + '_blocks .block .role-selector .settings.pdm').addClass('hide');
            if (!options.showpanel) {
                $('#' + domid + '_blocks').addClass('freeze');
                $('#' + domid + '_b_1 .block-name').append('<span class="label label-success">' + options.data.modifiertext + '</span>');
            }

            // Apply properties to ctreator and modifier
            //Commented for CZ-344(do't display admin user as collaborator in access pop-up if he's not)
            //$('#' + domid + '_b_0, #' + domid + '_b_1').addClass('active disabled');
            
          
           

           

            $('#' + domid + '_events input[type=checkbox]').removeAttr('checked');
            $('#' + domid + '_events button[value=submit]').button('reset');

            if (options.showpanel && (options.trigger != undefined)) {
                var trigger = options.trigger;
                options.trigger = undefined;
                $(trigger).attr('widget-id', ((parseInt($(trigger).attr('widget-id')) == -1) ? -2 : -1)).click();
                $('#' + domid + '_events button[value=submit]').click();
            }

            $('#' + domid + '_blocks .block.external .role-selector ul.dropdown-menu ').each(function (index, selector) {
                if ($(selector).find('li.settings').not('.hide').length <= 2) {
                    $(selector).find('li.settings').addClass('hide');
                }
            });

            $('#' + domid + '_blocks .block.user .role-selector ul.dropdown-menu ').each(function (index, selector) {
                if ($(selector).find('li.settings').not('.hide').length <= 2) {
                    $(selector).find('li.settings').addClass('hide');
                }
            });

            if (options.type) {
                $('#' + domid + '_blocks .block.user .role-selector, #' + domid + '_blocks .block-header.external').addClass('hide');
                $('#' + domid + '_blocks .block-grid.external').css('display', 'none');
            }
            else {
                $('#' + domid + '_blocks .block-header.external').removeClass('hide');
                $('#' + domid + '_blocks .block-grid.external').css('display', '');
            }
        },

        _initEventHandlers: function () {
            var control = this.options.control;
            var ns = this.options.namespace;
            var domid = this.options.domid;

            $(document)
                .on('click.' + ns, '*[widget-toggle=collaboratoraccess]', { collaboratorAccess: this }, this._onTrigger);

            $('#' + control + ' [data-dismiss=modal]')
                .bind('click.' + ns, { collaboratorAccess: this }, this._onCancel);

            $('#' + control + ' button[type=button][value=submit]')
                .bind('click.' + ns, { collaboratorAccess: this }, this._onSubmit);

            $('#' + control + ' #' + domid + '_filters ul.nav-pills li a')
                .bind('click.' + ns, { collaboratorAccess: this }, this._onChangeFilter);

            $('#' + control + ' #' + domid + '_filters input[name=search-text][type=text]')
                .bind('keyup.' + ns, { collaboratorAccess: this }, this._onKeyUpSearch);

            $('#' + control + ' #' + domid + '_blocks .block .user-selector')
                .bind('click.' + ns, { collaboratorAccess: this }, this._onChangeSelect);

            $('#' + control + ' #' + domid + '_blocks .block .role-selector [block-role]')
                .bind('click.' + ns, { collaboratorAccess: this }, this._onChangeRole);

            $('#' + control + ' #submitRoles')
                .bind('click.' + ns, { collaboratorAccess: this }, this._onChangeAllRoles);

            $('#' + control + ' #' + domid + '_phases ul.nav-tabs')
                .on('click.' + ns, 'li a', { collaboratorAccess: this }, this._onChangePhase);
        },

        _destroyEventHandlers: function () {
            var control = this.options.control;
            var ns = this.options.namespace;
            var domid = this.options.domid;

            $(document)
               .off('click.' + ns, '*[widget-toggle=collaboratoraccess]', { collaboratorAccess: this }, this._onTrigger);

            $('#' + control + ' [data-dismiss=modal]')
                   .unbind('click.' + ns, { collaboratorAccess: this }, this._onCancel);

            $('#' + control + ' button[type=button][value=submit]')
                .unbind('click.' + ns, { collaboratorAccess: this }, this._onSubmit);

            $('#' + control + ' #' + domid + '_filters ul.nav-pills li a')
                .unbind('click.' + ns, { collaboratorAccess: this }, this._onChangeFilter);

            $('#' + control + ' #' + domid + '_filters input[name=search-text][type=text]')
                .unbind('keyup.' + ns, { collaboratorAccess: this }, this._onKeyUpSearch);

            $('#' + control + ' #' + domid + '_blocks .block .user-selector')
                .unbind('click.' + ns, { collaboratorAccess: this }, this._onChangeSelect);

            $('#' + control + ' #' + domid + '_blocks .block .role-selector [block-role]')
                .unbind('click.' + ns, { collaboratorAccess: this }, this._onChangeRole);

            $('#' + control + ' #submitRoles')
                .unbind('click.' + ns, { collaboratorAccess: this }, this._onChangeAllRoles);

            $('#' + control + ' #' + domid + '_phases .tabs-container ul.nav-tabs li a')
                .off('click.' + ns, 'li a', { collaboratorAccess: this }, this._onChangePhase);
        },

        _initControls: function () {
            var options = this.options;
            var domid = this.options.domid;

            var controlid = domid + '_control';
            $(options.widget + ' .collaboratoraccess-control').attr('id', controlid);
            options.control = controlid;

            options.headerhtml = $('#' + options.control + ' h3.widget-title').html();

            var baseid = domid + '_base';
            $('#' + options.control + ' .collaboratoraccess-base').attr('id', baseid);
            options.base = baseid;

            var filterid = domid + '_filters';
            $('#' + options.base + ' .collaboratoraccess-filters').attr('id', filterid);
            options.membershtml = $('#' + filterid + ' [data-filter=members]').html();

            var blocksid = domid + '_blocks';
            $('#' + options.base + ' .collaboratoraccess-blocks').attr('id', blocksid);

            var dataid = domid + '_data';
            $('#' + options.base + ' .collaboratoraccess-data').attr('id', dataid);

            var eventsid = domid + '_events';
            $('#' + options.control + ' .collaboratoraccess-events').attr('id', eventsid);

            var phasesid = domid + '_phases';
            $('#' + options.control + ' .collaboratoraccess-phases').attr('id', phasesid);

            var index = 2;
            var addgroup = false;
            $('#' + domid + '_blocks .block').each(function () {
                var wid = $(this).attr('block-id');
                var group = $(this).hasClass('group');
                var external = $(this).hasClass('external');

                if (!group && !external && (wid == options.creator)) {
                    $(this).attr('id', domid + '_b_0');
                }
                else if (!group && !external && (wid == options.modifier)) {
                    $(this).attr('id', domid + '_b_1');
                }
                else {
                    $(this).attr('id', domid + '_b_' + index);
                    index++;
                }
            });

          
            options.data.approvaltext = $('#' + domid + '_data .widget-text-approval').html().trim();
            options.data.foldertext = $('#' + domid + '_data .widget-text-folder').html().trim();
            
        },

        _generateDOMID: function () {
            var chars, newchar, rand;
            chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            newchar = 'cfca';
            for (var i = 0; i < 3; i++) {
                rand = Math.floor(Math.random() * chars.length);
                newchar += chars.substring(rand, rand + 1);
            }
            return newchar;
        },

        _create: function () {
            var widget = ('#' + this.element[0].id),
                options = this.options;

            if ($(widget + ' div.collaboratoraccess-base').length > 0) {
                options.domid = this._generateDOMID();
                options.namespace = options.namespace || this.namespace;
                options.widget = widget;
                this._initControls();
                this._initEventHandlers();
                this._resetBlocks(options);
                this._setCount(options);
            }
        },

        destroy: function () {
            this._destroyEventHandlers();
            $.Widget.prototype.destroy.call(this);
        }
    });
}));