﻿$(document).ready(function () {
    window.locale = {
        "fileupload": {
            "errors": {
                "maxFileSize": "File is too big",
                "minFileSize": "File is too small",
                "acceptFileTypes": "Filetype not allowed",
                "maxNumberOfFiles": "You can upload only one file",
                "uploadedBytes": "Uploaded bytes exceed file size",
                "emptyResult": "Empty file upload result"
            },
            "error": $('.resource-error:first').text(),
            "start": "Start",
            "cancel": $('.resource-cancel:first').text(),
            "destroy": $('.resource-destroy:first').text()
        }
    };

    $('.fileupload').each(function () {
        $(this).fileupload({
            acceptFileTypes: eval($(this).attr('acceptFileTypes')),
            maxFileSize: parseInt($(this).attr('max-file-size')),
            /*resizeMaxWidth: 1920,
            resizeMaxHeight: 1200,*/
            autoUpload: true,
            maxChunkSize: 1900000000, // 1.9 GB
            dropZone: ($(this).attr('use-dropzone') == 'true') ? $('#dropzone_' + $(this).attr('key')) : $(this),
            maxNumberOfFiles: (($('#btnUpload_' + $(this).attr('key')).attr('multiple') != null) ? eval($(this).attr('maxNumberOfFiles')) : 1)
        });
    });



});