﻿var measurementConditions, oldMConditionValues, oldSelectedMCondition, pageModel;

$(document).ready(function () {
    // convert hiden string (the list of measurement conditions object) to json object list
    oldSelectedMCondition = parseInt($("#SelectedMeasurementCondition").val());
    pageModel = jQuery.parseJSON($("#hdnPaperTint").val());

    // on edit populate Lab values with the first measurement condition values
    var oldMCondVal = getCurrentConditionValues(oldSelectedMCondition);
    if (typeof oldMCondVal != 'undefined') {
        $("#L").val(oldMCondVal.L);
        $("#a").val(oldMCondVal.a);
        $("#b").val(oldMCondVal.b);
    }
    else {
        $("#L").val('');
        $("#a").val('');
        $("#b").val('');
    }
});

// when measurement condition changes save the user added values
$("#SelectedMeasurementCondition").change(function () {

    // get the value for the new selected measurement condition
    var oldMeasurements = getCurrentConditionValues(oldSelectedMCondition);
    if (typeof oldMeasurements == 'undefined') {
        var MeasurementsObject = {
            L: $("#L").val(),
            a: $("#a").val(),
            b: $("#b").val(),
            MeasurementCondition: oldSelectedMCondition
        };
        removeEmptyMeasurementCondition(MeasurementsObject)
    }
    else {
        oldMeasurements.L = $("#L").val();
        oldMeasurements.a = $("#a").val();
        oldMeasurements.b = $("#b").val();
    }

    var newMeasurements = getCurrentConditionValues($(this).val());
    if (typeof newMeasurements != 'undefined') {
        $("#L").val(newMeasurements.L);
        $("#a").val(newMeasurements.a);
        $("#b").val(newMeasurements.b);
    }
    else {
        $("#L").val('');
        $("#a").val('');
        $("#b").val('');
    }

    oldSelectedMCondition = $(this).val();
});

// when user submits get the current values and convert the object to string
$("#formAddUpdatePaperTint").on('submit', function () {

    //get the current values
    var curentCondition = getCurrentConditionValues(parseInt([$("#SelectedMeasurementCondition").val()]));
    if (typeof curentCondition == 'undefined') {
        var curentConditionObj = {
            L: $("#L").val(),
            a: $("#a").val(),
            b: $("#b").val(),
            MeasurementCondition: $("#SelectedMeasurementCondition").val()
        };
        removeEmptyMeasurementCondition(curentConditionObj);
    }
    else {
        curentCondition.L = $("#L").val();
        curentCondition.a = $("#a").val();
        curentCondition.b = $("#b").val();
    }

    for (var i = 0; i < pageModel.MeasurementConditionValues.length; i++) {
        if (pageModel.MeasurementConditionValues[i].L == "" && pageModel.MeasurementConditionValues[i].a == "" && pageModel.MeasurementConditionValues[i].b == "") {
            pageModel.MeasurementConditionValues.splice(i, 1);
        }
    }
    pageModel.SelectedMediaCategory = $("#SelectedMediaCategory").val();
    //Check if the user has added no values for Lab
    if (pageModel.MeasurementConditionValues.length > 0) {
        for (var i = 0; i < pageModel.MeasurementConditionValues.length; i++) {
            if (pageModel.MeasurementConditionValues[i].L.length == 0 || pageModel.MeasurementConditionValues[i].a.length == 0 || pageModel.MeasurementConditionValues[i].b.length == 0) {
                $(".empty-block-user.alert.alert-error.alert-danger").removeClass('hide');
                return false;
            }
        }
        $("#hdnPaperTint").val(JSON.stringify(pageModel));
    }
    else {
        $(".empty-block-user.alert.alert-error.alert-danger").removeClass('hide');
        return false;
    }
});


function getCurrentConditionValues(id) {
    for (var i = 0; i < pageModel.MeasurementConditionValues.length; i++) {
        if (pageModel.MeasurementConditionValues[i].MeasurementCondition == id) {
            return pageModel.MeasurementConditionValues[i];
        }
    }
}

function removeEmptyMeasurementCondition(condition) {
    if (condition.L != "" || condition.a != "" || condition.b != "") {
        pageModel.MeasurementConditionValues.push(condition);
    }
}

//only allow numeric values to be added to Lab textboxes    
$(".numericValues").keydown(function (e) {
    // Allow: backspace, delete, tab and escape
    if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 ||
    // Allow: Ctrl+A
    (e.keyCode == 65 && e.ctrlKey === true) ||
    // Allow: home, end, left, right
    (e.keyCode >= 35 && e.keyCode <= 39) ||
    // Allow: dot and numberpad dot
    (e.keyCode == 110 || e.keyCode == 190) ||
    (e.keyCode == 189) || (e.keyCode == 173)) {
        // let it happen, don't do anything
        return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
});