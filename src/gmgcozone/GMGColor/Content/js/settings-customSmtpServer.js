﻿$(document).ready(function () {
    if ($("#IsEnabled").val() == 'False') {
        $(".smtpSettings-password").removeClass("hide");
        $("#IsPasswordEnabled").val(true);
    }
    else {
        $(".smtpSettings-password").addClass("hide");
        $("#IsPasswordEnabled").val(false);
        $("#passwordField").removeClass("hide");
        enableCheckDisableButtons();
    }
});


function enableCheckDisableButtons()
{
    $("#btnTestConnection").removeAttr("disabled");
    $("#btnDisableCustomSmtp").removeAttr("disabled");
}


$("#passwordField").on("click", function () {
    if ($("#IsPasswordEnabled").val() == "false") {
        this.text = $("#CancelPasswordText").val();
        $("#IsPasswordEnabled").val(true);
        $(".smtpSettings-password").removeClass("hide");
    }
    else {
        this.text = $("#ChangePasswordText").val();
        $("#IsPasswordEnabled").val(false);
        $(".smtpSettings-password").addClass("hide");
        $("#btnTestConnection").removeAttr("disabled");
    }
});

$("#btnTestConnection").on("click", function () {
    var obj = {
        ServerName: $("#ServerName").val(),
        Port: $("#Port").val(),
        EnableSsl: $("#EnableSsl").val(),
        EmailAddress: $("#EmailAddress").val(),
        Password: $("#Password").val(),
        ConfirmPassword: $("#ConfirmPassword").val(),
        IsPasswordEnabled: $("#IsPasswordEnabled").val(),
        IsEnabled: $("#IsEnabled").val()
    };

    $(".validation-message").remove();

    $("#testCustomSmptLoadingIcon").removeClass("hide");
    $.ajax({
        url: $("#testSmtpUrl").val(),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        data: { testSettings: JSON.stringify(obj) },
        cache: false,
        success: function (data) {
            $("#testCustomSmptLoadingIcon").addClass("hide");
            if (data.Success) {
                $("#btnTestConnection").after("<span class='validation-message help-inline success valid' style='color:#468847;' >" + $("#SmtpSuccessConnection").val() + "</span>");
            }
            else {
                $("#btnTestConnection").after("<span class='validation-message help-inline success valid' style='color:#b94a48;' >" + $("#SmtpFailedConnection").val() + "</span>");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#testCustomSmptLoadingIcon").addClass("hide");
        }
    });
});
