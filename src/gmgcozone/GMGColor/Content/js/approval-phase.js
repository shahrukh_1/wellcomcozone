﻿$(document).ready(function () {
    $('#ApprovalShouldBeViewedByAll').attr('disabled', true);

    $('#decisionType').trigger('change');
    $('.date-picker').datepicker({ startDate: new Date() }).on('changeDate', function (ev) { $('.date-picker').datepicker('hide'); $('form').dirtyForms('setDirty'); });

    window.setTimeout('chooseDecisionMakers()', 100);
    $('.collaboratoraccess-events button[value=submit]').click(function () {
        window.setTimeout('chooseDecisionMakers()', 100);
    });

    if ($('#phaseForm').length > 0) {
        validateInput();
    }

    //Because the element is created dynamically, the event handler fires each time it has been bound to the element.
    //So in order to avoid this first unbind the handler and then rebind again.
    $(".up, .down").off('click').on('click', function (e) {
        var row = $(this).parents("tr:first");
        var rowChanged = 0;

        var id = $(this).parents("ul:first").attr('phase'); //The id of the row from which click event is triggered
        var tableId = $(this).parents("table:first").attr('id');

        var isUp = $(this).is(".up");
        if (isUp) {
            rowChanged = row.prev().find("ul:first").attr('phase');
        } else {
            rowChanged = row.next().find("ul:first").attr('phase');
        }

        //save the new positions of both rows in database
        $.ajax({
            type: "POST",
            url: "/ApprovalWorkflow/ChangePhasePosition",
            data: { changingPhase: id, changedPhase: rowChanged, __RequestVerificationToken: $("#formMain input").val() },
            dataType: "json",
            cache: false,
            success: function (canChangePosition) {
                if (canChangePosition) {
                    if (isUp) {
                        row.insertBefore(row.prev());
                    } else {
                        row.insertAfter(row.next());
                    }
                    hideFirstAndLastArrow(tableId);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
            }
        });
    });
});

function hideFirstAndLastArrow(tblPhasesId) {
    $('#' + tblPhasesId + ' .up:first, #' + tblPhasesId + ' .down:last').css('display', 'none');
    $('#' + tblPhasesId + ' .up, #' + tblPhasesId + ' .down').not('.up:first, .down:last').css('display', 'block');
}

$('#SelectedApprovalWorkflow').on('change', function () {
    $("#PhaseName").removeData("previousValue"); //clear cache
    $("form").data('validator').element('#PhaseName'); //retrigger remote call
    $('#PhaseName').blur();
});

$("#SelectedDecisionType").on('change', function () {
    configurePhaseDecisionSection(this);
});

//Implement logic for the Decision section of a phase
function configurePhaseDecisionSection(obj) {
    if ($(obj).find(':selected').val() == 1) {
        $('#primaryDecisionMaker').removeAttr('disabled');
        $("#ApprovalShouldBeViewedByAll").removeAttr('disabled');

    } else {
        //select the default option from Decision Maker dropdown
        $('#primaryDecisionMaker option:first').attr('selected', 'selected');
        $('#primaryDecisionMaker').trigger('change'); //mark selected=false the previous selected option

        $('#ApprovalShouldBeViewedByAll').attr('checked', false);
        $('#primaryDecisionMaker').attr('disabled', 'disabled');
        $("#ApprovalShouldBeViewedByAll").attr('disabled', true);
    }
}

$('#decisionType').on('change', function () {
    configurePhaseDecisionSection(this);
});

$('#primaryDecisionMaker').on('change', function () {
    var index = $(this).find('option:selected').attr('index');
    $('#errPdmRequired').hide();

    $('#decisionMaker').find('input[name$=".IsSelected"]').each(function (idx, val) {
        if (idx == index) {
            $(this).val(true);
        } else {
            $(this).val(false);
        }
    });
});

function chooseDecisionMakers() {
    var selectedexternal = [];
    var externaluserswithrole = ($('#aApprovalTrigger').attr('widget-external') != undefined) ? $('#aApprovalTrigger').attr('widget-external').replace(/\"/g, "") : '[]';
    if (externaluserswithrole) {
        externaluserswithrole = externaluserswithrole.substring(1, (externaluserswithrole.length - 1));
        externaluserswithrole = ((externaluserswithrole != '') ? externaluserswithrole.split(',') : []);
        $(externaluserswithrole).each(function () { selectedexternal.push(this.split('|')[0]); });
    }

    var datablock = $('.collaboratorshare-control .collaboratorshare-blocks .block[block-id=0]');
    var dataemail = $('.collaboratorshare-control .collaboratorshare-blocks .role-selector input[type=text].email');
    var datarole = datablock.attr('block-role');
    var userblocks = $('.collaboratorshare-control .collaboratorshare-blocks .block-grid .block');

    // save the existing roles and clear all the existing users
    var roles = new Array();
    var activeExternalUsers = $(".block-email").parents('li.block.active');
    for (i = 0; i < activeExternalUsers.length; i++) {

        var currentExternalUser = $(activeExternalUsers[i]);

        var userId = currentExternalUser.attr("block-id");
        var roleId = currentExternalUser.attr("block-role");

        roles[roles.length] = userId + "|" + roleId;

        if (parseInt(userId, 10) > 0) {
            currentExternalUser.removeClass('active').addClass('remove');
        }
    }

    for (var i = 0; i < userblocks.length; i++) {
        var blockid = $(userblocks[i]).attr('id');
        if (parseInt($('#' + blockid).attr('block-id')) >= 0) {
            if ((selectedexternal.length > 0) && (selectedexternal.indexOf($(userblocks[i]).attr('block-id')) > -1)) {
                var email = $('#' + blockid + ' .block-email').html().trim();
                var role = $('.collaboratoraccess-control .collaboratoraccess-blocks .block.external[block-id=' + $('#' + blockid).attr('block-id') + ']').attr('block-role');
                dataemail.val(email);
                datablock.attr('block-role', role);
                $('.collaboratorshare-control .collaboratorshare-blocks button[type=button][value=insert]').click();
            } else if (parseInt($('#' + blockid).attr('block-id')) > 0) {
                $('#' + blockid + ' .role-selector .actions.remove [block-role=-3]').click();
            }
        }
    }

    dataemail.val('');
    datablock.attr('block-role', datarole);
}

//$('#dueDate').on('change', function () {
//    if ($(this).is(':checked')) {
//        $('#deadlineDate').css('display', 'block');
//    } else {
//        $('#deadlineDate').css('display', 'none');
//    }
//});

$('.spinner .btn:first-of-type').on('click', function () {
    $(this).blur();
    if (parseInt($('.spinner input').val(), 10) < 99) {
        $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 1);
    }
    validateInput();
    return false;
});
$('.spinner .btn:last-of-type').on('click', function () {
    $(this).blur();
    if (parseInt($('.spinner input').val(), 10) > 1) {
        $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1);
    }
    validateInput();
    return false;
});

$('.spinner input').on('keyup', function () {
    validateInput();
});

function validateInput() {
    var input = $('.spinner input').val().trim();
    if (input == "" || isNaN(input)) {
        $("#valueMustBeNumber").css({ 'display': 'block', "color": "#b94a48", "margin": "8px 0 0 21px" });
        $("#numberMustBeBetweenRange").css({ 'display': 'none' });
        $("#selectedUnit").css('border-color', '#b94a48');
        return false;
    }

    if (parseInt($('.spinner input').val(), 10) > 99 || parseInt($('.spinner input').val(), 10) < 1) {
        $("#numberMustBeBetweenRange").css({ 'display': 'block', "color": "#b94a48", "margin": "8px 0 0 21px" });
        $("#selectedUnit").css('border-color', '#b94a48');
        $("#valueMustBeNumber").css({ 'display': 'none' });
        return false;
    }

    $("#numberMustBeBetweenRange").css({ 'display': 'none' });
    $("#selectedUnit").css('border', '1px solid #ccc');
    $("#valueMustBeNumber").css({ 'display': 'none' });
}

$('#SelectedDeadlineType').on('change', function () {
    if ($('#SelectedDeadlineType').val() == "1") {
        validateInput();
    } else {
        $('.spinner input').val(1);
        $("#numberMustBeBetweenRange").css({ 'display': 'none' });
        $("#selectedUnit").css('border', '1px solid #ccc');
        $("#valueMustBeNumber").css({ 'display': 'none' });
    }
});

$('#phaseForm').on('submit', function (e) {
    if ($('#SelectedDeadlineType:checked').val() == "1") {
        var isValid = validateInput();
        if (isValid) {
            return true;
        }
    }
});


