﻿$(function () {
    // new instance of colorpicker
    $('.colorPicker').colorpicker({
        format: 'rgb'
    });

    //expand color list on edit button click
    $(".edit-delete-advs").click(function (e) {
        toggleColorList($(this));
        e.stopPropagation();
    });

    //update colorbars on colorpicker color change
    $(".color input").on("input blur", function () {
        var newColor = $(this).val();
        var colors = [];
        var colorIndex = $(this).parent().parent().index();

        $(this).closest("ul").find("li").each(function () {
            var current = $(this);
            if (current.hasClass("themeColorListItem")) {
                colors.push($(current.find(".colorPicker")[0]).val());

                if ($(current.find(".colorPicker")[1]).val()) {
                    colors.push($(current.find(".colorPicker")[1]).val());
                }
            }
        });

        //change the color bars colors based on selection
        $(this).next().children().css("background-color", newColor);
        $(this).closest("li[id^='theme']").find("dl").find("dd:nth-child(" + colorIndex + ")").css("background-color", newColor);

        //change elements colors
        updateColorsBasedOnSelection(colors)
    });

    //move to the next colorpicker on tab press
    $(".colorPicker").keydown(function (e) {
        var code = e.keyCode || e.which;

        if (code === 9) {
            e.preventDefault();
            $(this).colorpicker('hide');
            $(this).closest("li").next().find(".colorPicker").focus();
        }
    });

    $(".colorPicker").click(function (event) {
        event.stopPropagation();
    });

    //toggle the colorList
    function toggleColorList(obj) {
        var $colorScheme = obj.parent().parent("li");
        var $colorList = $colorScheme.find(".themeColorList");

        var isOpen = $colorList.hasClass("display");

        $(".themeColorList").addClass("hide");
        $(".themeColorList").removeClass("display");
        $("li[id^='theme']").removeClass("openList");
        $colorList.find(".colorListName").val($colorScheme.find(".styleswitch").text().trim());

        if (!isOpen) {
            $colorList.addClass("display");
            $colorList.removeClass("hide");
            $("li[id^='theme']").removeClass("active");
            $colorScheme.addClass("openList active");
        }
    }

    $("li[id^='theme']").on("click", function () {
        //set all theme statuses to false
        var themesStatuses = $("li[id^='theme']").find(".themeStatus");
        themesStatuses.each(function () { $(this).val(false) })

        //set the current selected theme status to true
        $(this).find(".themeStatus").val(true);

        //real time update theme colors on theme select
        var userSelectedSchema = $(this).find(".colorSchemeGroup input").map(function () { return $(this).val(); }).get();
        userSelectedSchema.push($(this).find(".styleswitch").first().text());
        updateColorsBasedOnSelection(userSelectedSchema);
        toggleColorList($(this));
    });


    function updateColorsBasedOnSelection(colors) {
        var sideBarTextColor;
        switch (colors[6]) {
            case "Default":
                sideBarTextColor = "#ed1556!Important";
                break;
            case "Pink":
                sideBarTextColor = "#666!Important";
                break;
            case "Juicy":
                sideBarTextColor = "#049cdb!Important";
                break;
            case "Fresh":
                sideBarTextColor = "#1368a4!Important";
                break;
            case "Clear":
                sideBarTextColor = "#ed1556!Important";
                break;
        };

        less.modifyVars({
            '@headerTopColor': colors[0],
            '@headerBottomColor': colors[1],
            '@footerColor': colors[2] + "!important",
            '@buttonTopColor': colors[3],
            '@buttonBottomColor': colors[4],
            '@linksColor': colors[5] + "!important",
            '@sideMenuBarText': sideBarTextColor
        });
    }
});