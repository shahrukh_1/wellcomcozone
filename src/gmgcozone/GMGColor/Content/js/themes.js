﻿$(function () {
    //check if theme is already in local storage
    if (!localStorage.getItem("themeSchema")) {
        //get the theme for current users/account and save it to localStorage
        var loggedUserThemeSchema = getLoggedUserThemeSchema();
        var activeUsertheme = $.grep(loggedUserThemeSchema, function (theme) { return theme.Active == true; });
        var themeToString = JSON.stringify(activeUsertheme);
        localStorage.setItem("themeSchema", themeToString);
    }

    //get the active theme from local storage and parse it
    var userThemeFromLocalStorage = $.parseJSON(localStorage.getItem("themeSchema"));

    // IE fix
    var colors;
    if (userThemeFromLocalStorage[0] != null) {
        colors = userThemeFromLocalStorage[0].Colors;
    }
    else {
        colors = userThemeFromLocalStorage.Colors;
    }

    updateColorsBasedOnAccountTheme(colors);
    destroyLessCache("/Content/less");

    function updateColorsBasedOnAccountTheme(colors) {
        var header = $.grep(colors, function (obj) { return obj.Name == "Header"; });
        var footer = $.grep(colors, function (obj) { return obj.Name == "Footer"; });
        var buttons = $.grep(colors, function (obj) { return obj.Name == "Buttons"; });
        var links = $.grep(colors, function (obj) { return obj.Name == "Links"; });

        less.modifyVars({
            '@headerTopColor': header[0].TopColor,
            '@headerBottomColor': header[0].BottomColor,
            '@footerColor': footer[0].TopColor + "!important",
            '@buttonTopColor': buttons[0].TopColor,
            '@buttonBottomColor': buttons[0].BottomColor,
            '@linksColor': links[0].TopColor + "!important"
        });

        less.refreshStyles();
    }
    setTimeout(function () {
        updateColorsBasedOnAccountTheme(colors);
    }, 300);  
    function getLoggedUserThemeSchema() {
        var colorsTheme;
        $.ajax({
            type: "GET",
            url: "/Themes/GetLoggedUserColorSchema",
            datatype: "json",
            async: false,
            success: function (result) {
                colorsTheme = result;
            }
        });
        return colorsTheme;
    }

    //clean up cached less files from local storage 
    function destroyLessCache(path) {
        var host = window.location.host;
        var protocol = window.location.protocol;
        var keyPrefix = protocol + '//' + host + path;

        for (var key in window.localStorage) {
            if (key.indexOf(keyPrefix) === 0) {
                delete window.localStorage[key];
            }
        }
    }

    $("#brandingPresetForm, #formUserInformation, #formGroup, #formMain, #login-form").on("submit", function () {
        localStorage.removeItem("themeSchema");
    });
});