﻿$(document).ready(function () {

    var profileExists = $("#hdnFileName_1").val();
    if (profileExists !== "") {
        $('#addEditSimulationProfile').prop('disabled', false);
    }
    else {
        $('#addEditSimulationProfile').prop('disabled', true);
    }

    //enable button when upload is done
    $('#fileUpload_1').on('fileuploaddone', function (e, data) {
        $('#addEditSimulationProfile').prop('disabled', false);
    });

    //disable button when the uploaded file is deleted
    $('#fileUpload_1').on('fileuploaddestroy', function (e, data) {
        $('#addEditSimulationProfile').prop('disabled', true);
    });

    $("#formAddUpdateSimulationProfile").submit(function (e) {
        var uploadedFileName = $("#hdnFileName_1").val();
        if (uploadedFileName.length > 50) {
            e.preventDefault();
            if ($('#uploadFile').length == 0) {
                $("#fileUpload_1").after("<span id=\"uploadFile\" generated=\"true\" class=\"field-validation-error\">The Upload Profile Name must be a string with a maximum length of 50.</span>");
                $("#uploadFile").css("padding-left", "180px");
            }
        }
    });
    $(document).on("click", ".btn.btn-danger", function () {
        if ($('#uploadFile').length != 0) {
            $("#uploadFile").remove();
        }
    });
})