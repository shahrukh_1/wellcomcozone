﻿$(document).on('click', '.folder-action', bindApprovalFolderEvents);
$(document).on('click', '.filter-action', bindSearchFilterEvents);

function LoadApprovalDecisions(approvals) {
    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: approvalDecisionsUrl + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalsDecisions) {
                approvalsDecisions.Content.Data.forEach(function (item, index, array) {
                    var toolTipBody = '';
                    item.Decisions.forEach(function (decision) {
                        if (decision.CollaboratorName != null) {
                            toolTipBody += decision.CollaboratorName + '&nbsp;-&nbsp;';
                        }
                        toolTipBody += decision.ApprovalDecision + '<br>';
                    });
                    // set tooltip body
                    $('.lblApproval' + item.ApprovalId).find('.approval-decision').attr('data-title', toolTipBody);

                    // set tooltip body for CollaborateStatusView grid
                    $('tr:has(label.lblApproval' + item.ApprovalId + ')').find('.approval-decision').attr('data-title', toolTipBody);

                    //build phase details tooltip
                    var phaseDetailsToolTipBody = '';
                    phaseDetailsToolTipBody += $("#lblWorkflow").val() + ": " + item.WorkflowName + '<br>' + $("#lblPhase").val() + ": " + item.PhaseName + '<br>' + $("#lblStatus").val() + ": " + item.PhaseStatus;
                    $('.lblApproval' + item.ApprovalId).find('.phase-icon').attr('data-original-title', phaseDetailsToolTipBody);
                });
              
                $('.approval-decision[rel="tooltip"]').tooltip();
                $('.phase-icon[rel="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval Decisions ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        }); 
    }
}

function LoadSOADIndicator(approvals) {
    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: getSOADIndicatorData + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalsSOAD) {
                approvalsSOAD.Content.Data.forEach(function (item, index, array) {
                    if ($('.chkThumb' + item.Approval + '.NotExecutedWorkflow').attr("approval") != undefined) {
                        $('#soadIndicatorBar-' + item.Approval).attr('class', 'no-state');
                    } else {
                        $('#soadIndicatorBar-' + item.Approval).attr('class', item.ProgressStateClass);
                    }
                });

                $('.approval-state[rel="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval SOAD ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

function LoadApprovalRetouchers(approvals) {
    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: getApprovalRetouchersUrl + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalsRetouchers) {
                    approvalsRetouchers.Content.Data.forEach(function (item, index, array) {
                        var toolTipBody = '';
                        item.Retouchers.forEach(function (retoucher) {
                            toolTipBody += retoucher + '<br>';
                        });
                        if (toolTipBody != '') {
                            // set tooltip body
                            $('.lblApproval' + item.ApprovalId).find('.backup_picture').attr('data-title', toolTipBody);
                        }
                    });

                    $('.backup_picture[rel="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval Retouchers ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

function LoadApprovalNotViewedAnnotations(approvals) {
    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: getApprovalAnnotationsNotViewedUrl + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalsNotViewedAnnotations) {
                approvalsNotViewedAnnotations.Content.Data.forEach(function (item, index, array) {
                    var notViewedAnnotations = $('.not-viewed-annotations-for-' + item.ApprovalId);
                    if (item.NotViewed > 0) {
                        notViewedAnnotations.text(item.NotViewed);
                        notViewedAnnotations.show();
                        $('.newComments' + item.ApprovalId).show();
                    } else {
                        notViewedAnnotations.hide();
                    }
                });
                
                $('.not-viewed-annotations[rel="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval Not Viewed Annotations ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

function LoadApprovalPhaseDeadline(approvals) {
    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: getApprovalPhaseDeadlineUrl + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (response) {
                response.Content.Data.forEach(function (item, index, array) {
                    var phaseDeadlineElem = $('.phasedeadline-col-' + item.Approval + ' span');
                    var deadline = '<span>' + item.DeadlineDate + '<br>' + '<small class="medium-gray-color">' + item.DeadlineTime + '</small>'
                    $(phaseDeadlineElem).replaceWith(deadline);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval Phase Deadline' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

function LoadApprovalVisualIndicators(approvals) {
    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: getVisualIndicatorData + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalCollab) {
                processVisualIndicatorData(approvalCollab.Content.Data);                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval Visual Indicator ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

function LoadApprovalDecisionMakers(approvals) {

    if (approvals.length > 0) {

        approvals.forEach(function (item) {

            var approval_info = $("#LoadApprovalDecisionMakers-" + item);
            var approval = approval_info.attr('data-item-approval');
            var currentPhase = approval_info.attr('data-item-currentPhase');
            var pdm = approval_info.attr('data-item-pdm');
            var IsExternalPdm = approval_info.attr('data-item-IsExternalPDM');
            var IsLoadAll = "False";

            $.ajax({
                type: "GET",
                url: $('#getDecisionMakers').val() + "?approval=" + approval + "&currentPhase=" + currentPhase + "&pdm=" + pdm + "&IsExtenalPDM=" + IsExternalPdm + "&IsLoadAllDecisionMakers=" + IsLoadAll ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (decisionMakers) {
                    if (decisionMakers.Status == 200) {
                        $('#LoadApprovalDecisionMakers-' + approval).append(decisionMakers.Content);
                        if (decisionMakers.DecisionMakersCount > 5)
                        {
                            $('#btnViewAllDecisionMakers-' + approval).removeClass('hide');
                        }
                    } else {
                        $('#errOccured').show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                        return;
                    } else {
                        $('#errOccured').show();
                    }
                }
            });
        });
        
    }
}


$(document).ready(function () {
    $("#sliderButton").hide();
    $("#sliderMenu").hide();

  

    $.ajax({
        type: "GET",
        url: "/Approvals/SetSelectedFilterType?filterID=" + localStorage.getItem('dashboardFilterType') + "&sortingID=" + localStorage.getItem('dashboardSortingOrderType') + "&topLinkID=" + localStorage.getItem('dashboardTopLinkID'),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) { }
    });

    $.ajax({
        type: "GET",
        url: propulateApprovalsGridUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (approvals) {
            // Add grid content to Dom
            $("#sliderButton").show();
            $('#loadingGrid').remove();
            $('#approvals').append(approvals.Content);
            var viewAllFiles = $("#hdnShowAllFilesAndFolder").val();

            if (!propulateApprovalsGridUrl.includes("ProjectFolder"))
                renderSideBarFolders(viewAllFiles);
                        
            if (localStorage.getItem("sidebarIsPinned") == "true") {
                showHideSideBar();
            }

            InitApprovalsGrid(); // initialize the approval grid (layout and events)
            setTimeout(function () {
                $('#approvals-index').find('th:first').trigger('resize');
                var viewType = localStorage.getItem('dashboardViewType');
                showDashboardView(viewType, approvals.Approvals);
            }, 100);

            // Load decision tooltips
            LoadApprovalDecisions(approvals.Approvals);
      
            if (approvals.DisableVisualIndicator == "false") {
                LoadApprovalVisualIndicators(approvals.Approvals);
            }

            // Load Decision makers
            LoadApprovalDecisionMakers(approvals.Approvals);
          
            if (approvals.DisableSOADIndicator == false) {
                LoadSOADIndicator(approvals.Approvals); //To do: Optimize and then un-comment 
            }

            if ($('#hdnFolderID').val() > 0) {
                $('#btnLinkForRetoucher').removeClass('hide');
                if ($('#hdnSelectedStatus')[0].value == '2') {
                    $('#btnLinkForRetoucher').removeAttr('disabled');
                }
            }

            //Fix for the webgrid header columns to generate the correct url (needed after loading async the grid)
            $('th a').on('click', function () {
                $(this).attr('href', $(this).attr('href').replace('PopulateApprovalsGrid', 'Populate'));
            });

            // Load retouchers if needed
            if (approvals.DisplayRetouchers) {
                LoadApprovalRetouchers(approvals.Approvals);
            }

            if (approvals.ShowBadges) {
                LoadApprovalNotViewedAnnotations(approvals.Approvals);
            }

            registerDashboardConnectionApprovalIds();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
            else {
                $('#loadingGrid').replaceWith('<label>Error Loading Approvals Grid ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });

    if (localStorage.getItem('dashboardTopLinkID') == -7 && $('#topLinkIdAllApprovals').length > 0) {
        if ($('#topLinkIdAllApprovals')[0].className == "active") {
            $('#topLinkIdAllApprovals')[0].className = "";
            $('#topLinkIdMyopenApprovals')[0].className = "active";
        }
    }
});

function bindSearchFilterEvents() {
    var action = $(this).attr('action');
    var filter = $(this).attr('filter');

    if ((action != '') && !($(this).hasClass('locked'))) {
        $('#hdnSelectedTopLinkID').val(filter);

        switch (action.toUpperCase()) {
            case 'ADVANCEDSEARCHDELETE':
            {
                $('#modelDialogDeleteConfirmation_11').modal({ backdrop: 'static', keyboard: false });
                break;
            }
        }
    }
}

function bindApprovalFolderEvents() {
    var action = $(this).attr('action');
    var folder = $(this).attr('folder') || 0;
   
    if ((action != '') && !($(this).hasClass('locked'))) {
        $('#hdnSelectedFolder').val(folder);

        switch (action.toUpperCase()) {
        case 'ARCHIVEFOLDER':
        {
            $('#modelDialogDeleteConfirmation_3').modal({ backdrop: 'static', keyboard: false });
            break;
        }
        case 'DELETEFOLDER':
        {
            $('#modelDialogDeleteConfirmation_4').modal({ backdrop: 'static', keyboard: false });
            break;
        }
        case 'DELETEFOLDERPERMANENT':
        {
            $('#modelDialogDeleteConfirmation_6').modal({ backdrop: 'static', keyboard: false });
            break;
        }
        case 'DOWNLOADFOLDER':
            {
                $("#FileIds").val("");
                $("#FolderIds").val(folder);
                $('#downloadFileType option[value="1"]').attr('selected', true);
                $('#downloadFileType option[value="0"]').attr('disabled', true);
                $("#modalFileDownload").modal({ backdrop: 'static', keyboard: false });
                break;
                }
            case 'TRANSFERFOLDER': {
                
                $('#btnTransferFolder').click();
                 
                break;
            }
        case 'CREATEFOLDER':
        case 'RENAMEFOLDER':
        {
            var create = (action.toUpperCase() == 'CREATEFOLDER');
            var private = $(this).attr('private');
            var foldername = '';

            if (create) {
                $('#modalDialogFolderCR .create').removeClass('hide');
                $('#modalDialogFolderCR .rename').addClass('hide');
                $('#modalDialogFolderCR .create:button').attr('value', 'CreateFolder');

                var isAllColDecReq = false;
                $.ajax({
                type: 'GET',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: collaboratorDecisionsUrl,
                success: function (result) {
                    isAllColDecReq = result;
                },
                complete: function() {
                    $('#modalDialogFolderCR input[type=checkbox]').prop('checked', isAllColDecReq);
                    showFolderPopup(folder, private);
                }
                });
            }
            else {
                $('#modalDialogFolderCR .create').addClass('hide');
                $('#modalDialogFolderCR .rename').removeClass('hide');
                $('#modalDialogFolderCR .create:button').attr('value', 'RenameFolder');
                private = private || 'True';

                var blockid = 'cdflx_PBD';
                $(this).parents('.block:first').attr('id', blockid);
                foldername = $('#' + blockid + ' .block-content > .title .action').first().text().trim();
                $(this).parents('.block:first').removeAttr('id');
                    
                var isChecked = $(this).attr('data-alldecisionrequired') == 'True';
                $('#modalDialogFolderCR input[type=checkbox]').prop('checked', isChecked);

                showFolderPopup(folder, private);
                $('#modalDialogFolderCR input[type=text]').focus().val(foldername);
            }
            break;
        }
        case 'RESTOREFOLDER':
        {
            $('#btnRestoreFolder').click();
            break;
        }
        case 'CREATELINK':
        {
            var folderLink = location.protocol + '//' + location.host + '/Approvals/Populate?topLinkID=' + folder;
            $('#folderUrl').val(folderLink);

            $('#modalCreateLink').modal({ backdrop: 'static', keyboard: false });

            break;
        }
        default:
        {
            $('#dynamicAction').attr('value', action).click();
            break;
        }
        }
    }
}

function showFolderPopup(folder, private) {
    $('#hdnFolderID').val(folder);
    $('#hdnFolderParent').val(folder);
    $('#hdnFolderIsPrivate').val(private);
    $('#modalDialogFolderCR').modal({ backdrop: 'static', keyboard: false });
}

function processVisualIndicatorData(collaborators) {
    collaborators.forEach(function (item, index, array) {
        var totalUsers = item.greenUsers.length + item.redUsers.length + item.greyUsers.length + item.whiteUsers.length + item.orangeUsers.length;
        if (totalUsers > 0) {
            //green

            var greentoolTipBody = '';
            item.greenUsers.forEach(function(greenUser) {
                greentoolTipBody += greenUser + '<br>';
            });
            if (greentoolTipBody != '') {
                // set tooltip body
                $('.indicatorBar-' + item.ApprovalId).find('.greenBarIndicator').attr('data-original-title', greentoolTipBody);
            }
            var greenPercent = item.greenUsers.length / totalUsers * 100;
            $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greenBarIndicator').css("height", greenPercent + "%");
            $('.indicatorBar-' + item.ApprovalId + '.horizontalIndicatorBar').find('.greenBarIndicator').css("width", greenPercent + "%");
            if (greenPercent == 0) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greenBarIndicator').addClass("displayNone");
            }
            else if (greenPercent == 100){
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greenBarIndicator').addClass("noBorder");
            }
            else {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greenBarIndicator').addClass("indicatorFirstChild");
            }
            //red
            var redtoolTipBody = '';
            item.redUsers.forEach(function(redUser) {
                redtoolTipBody += redUser + '<br>';
            });
            if (redtoolTipBody != '') {
                // set tooltip body
                $('.indicatorBar-' + item.ApprovalId).find('.redBarIndicator').attr('data-original-title', redtoolTipBody);
            }
            var redPercent = item.redUsers.length / totalUsers * 100;
            $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.redBarIndicator').css("height", redPercent + "%");
            $('.indicatorBar-' + item.ApprovalId + '.horizontalIndicatorBar').find('.redBarIndicator').css("width", redPercent + "%");
            if (redPercent == 0) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.redBarIndicator').addClass("displayNone");
            }
            else if (redPercent == 100) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.redBarIndicator').addClass("noBorder");
            }
            else {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.redBarIndicator').addClass("indicatorFirstChild");
            }
            //grey
            var greytoolTipBody = '';
            item.greyUsers.forEach(function(greyUser) {
                greytoolTipBody += greyUser + '<br>';
            });
            if (greytoolTipBody != '') {
                // set tooltip body
                $('.indicatorBar-' + item.ApprovalId).find('.greyBarIndicator').attr('data-original-title', greytoolTipBody);
            }
            var greyPercent = item.greyUsers.length / totalUsers * 100;
            $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greyBarIndicator').css("height", greyPercent + "%");
            $('.indicatorBar-' + item.ApprovalId + '.horizontalIndicatorBar').find('.greyBarIndicator').css("width", greyPercent + "%");
            if (greyPercent == 0) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greyBarIndicator').addClass("displayNone");
            }
            else if (greyPercent == 100) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greyBarIndicator').addClass("noBorder");
            }
            else {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.greyBarIndicator').addClass("indicatorFirstChild");
            }
            //orange
            var orangetoolTipBody = '';
            item.orangeUsers.forEach(function (orangeUser) {
                orangetoolTipBody += orangeUser + '<br>';
            });
            if (orangetoolTipBody != '') {
                // set tooltip body
                $('.indicatorBar-' + item.ApprovalId).find('.orangeBarIndicator').attr('data-original-title', orangetoolTipBody);
            }
            var orangePercent = item.orangeUsers.length / totalUsers * 100;
            $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.orangeBarIndicator').css("height", orangePercent + "%");
            $('.indicatorBar-' + item.ApprovalId + '.horizontalIndicatorBar').find('.orangeBarIndicator').css("width", orangePercent + "%");
            if (orangePercent == 0) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.orangeBarIndicator').addClass("displayNone");
            }
            else if (orangePercent == 100) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.orangeBarIndicator').addClass("noBorder");
            }
            else {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.orangeBarIndicator').addClass("indicatorFirstChild");
            }
            //white
            var whitetoolTipBody = '';
            item.whiteUsers.forEach(function(whiteUser) {
                whitetoolTipBody += whiteUser + '<br>';
            });
            if (whitetoolTipBody != '') {
                // set tooltip body
                $('.indicatorBar-' + item.ApprovalId).find('.whiteBarIndicator').attr('data-original-title', whitetoolTipBody);
            }
            var whitePercent = item.whiteUsers.length / totalUsers * 100;
            $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.whiteBarIndicator').css("height", whitePercent + "%");
            $('.indicatorBar-' + item.ApprovalId + '.horizontalIndicatorBar').find('.whiteBarIndicator').css("width", whitePercent + "%");
            if (whitePercent == 0) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.whiteBarIndicator').addClass("displayNone");
            }
            else if (whitePercent == 100) {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.whiteBarIndicator').addClass("noBorder");
            }
            else {
                $('.indicatorBar-' + item.ApprovalId + '.verticalIndicatorBar').find('.whiteBarIndicator').addClass("indicatorFirstChild");
            }
            //set users header
            $('.indicatorBar-' + item.ApprovalId).find("div[class^='usersBarIndicator']").html((item.greenUsers.length + item.redUsers.length) + "/" + totalUsers);
        }
    });

    $('.indicatorBar[rel="tooltip"]').tooltip();
}

$(window).resize(function () {
    var SeperatorLineHeight = $("#approvals-index thead tr").height();
    $(".CRC .CRG .resizable-menu-separator").css("height", SeperatorLineHeight);
});