﻿var selectedApprovalId = 0;

$(document).ready(function () {
    // show the side bar before pressing the menu button to avoid side bar flickering
    $("#approvals-sidebar-wrapper").removeClass("hide");
    $("#menu-sidebar-wrapper").removeClass("hide");

    initHandlers();
    var ListIDs = typeof (inProgressApprovalIDs) != "undefined" ? inProgressApprovalIDs : [];
    var viewType = localStorage.getItem('dashboardViewType');

    smartColumns();
    showDashboardView(viewType);

    // Smart Columns resize
    $(window).resize(function () {
        smartColumns();
    });
    
    // fix for elements that are positioned as fixed when keyboard is shown
    if (Modernizr.touch) {
        /* cache dom references */
        var body = jQuery('body');

        $("input").focus(function () {
            $(body).addClass('keyboardOn');
        }).blur(function () {
            $(body).removeClass('keyboardOn');
        });
    }

    window.onpopstate = function (event) {
        $("#tablet-loading").addClass('tablet-icon-hide');
    };
});

function LoadApprovalPhaseDeadline() {
   var seen = {};
   var approvals =  $('a[title="Studio"]').map(function () {
                                                var value = $(this).attr('approval');

                                                //check if this value has already been added
                                                if(seen.hasOwnProperty(value)) return null;

                                                seen[value] = true;
                                                return value;
                                             }).get().join(',');

    if (approvals.length > 0) {
        $.ajax({
            type: "GET",
            url: getApprovalPhaseDeadlineUrl + '?approvals=' + approvals,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (response) {
                response.Content.Data.forEach(function (item, index, array) {
                    var phaseDeadlineElem = $('.phasedeadline-col-' + item.Approval + ' span');
                    var deadline = '<span>' + item.DeadlineDate + '<br>' + '<small class="medium-gray-color">' + item.DeadlineTime + '</small>'
                    $(phaseDeadlineElem).replaceWith(deadline);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading Approval Phase Deadline' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

//function getProgress(IDs) {
//    var progress = '';
//    if (IDs.length > 0) {
//        $.ajax({
//            type: "POST",
//            url: getProgressURL,
//            data: "{'jobIdList':[" + IDs + "]}",
//            dataType: "json",
//            contentType: "application/json; charset=utf-8",
//            error: function () {
//                setTimeout(function () { getProgress(IDs); }, 2000);
//            },
//            success: function (result) {
//                for (var i = 0; i < result.statuses.length; i++) {
//                    if (result.statuses[i] != false) {

//                        var id = result.statuses[i].ID;
//                        var value = result.statuses[i].value;
//                        var progressText = result.statuses[i].progressText;
//                        var path = result.statuses[i].path;

//                        switch (value) {
//                            case 'Queued':
//                            case 'Processing':
//                            default:
//                                {
//                                    if (value == 'Processing') {
//                                        $('#dvThumb' + id).addClass('hide');
//                                    }
//                                    $('#h3Processing' + id).addClass('processing').removeClass('label label-important');
//                                    break;
//                                }
//                            case 'Error':
//                                {
//                                    $('#dvProgress' + id).parent().remove();
//                                    $('#h3Processing' + id).text(progressText);
//                                    $('#imgThumb' + id).attr('src', path);
//                                    $('.lblApproval' + id).parents('li.block:first').removeClass('render');
//                                    $('#dvThumb' + id).removeClass('hide');
//                                    $('#dvThumb' + id + ' .dropdown-menu li').addClass('hide');
//                                    if ($('#dvThumb' + id + ' .approval-action[action=DeleteVersion]')) {
//                                        $('#dvThumb' + id + ' .approval-action[action=DeleteVersion]').parents('li:first').removeClass('hide');
//                                    }
//                                    if ($('#dvThumb' + id + ' .approval-action[action=DeleteApproval]')) {
//                                        $('#dvThumb' + id + ' .approval-action[action=DeleteApproval]').parents('li:first').removeClass('hide');
//                                    }
//                                    $('#h3Processing' + id).removeClass('processing').addClass('label label-important');

//                                    var arr = IDs.split(',');
//                                    arr.splice(arr.indexOf('' + id), 1);
//                                    IDs = arr.join(',');

//                                    break;
//                                }
//                            case 'Success':
//                                {
//                                    if (typeof (ShowOrHideIcons) == 'function') {
//                                        ShowOrHideIcons(i);
//                                    }

//                                    $('#h3Processing' + id).remove();
//                                    $('#imgThumb' + id).attr('src', path);
//                                    $('#aThumb' + id).removeClass('locked');
//                                    $('#chkThumb' + id).addClass('choose_item');
//                                    $('.lblApproval' + id + ' .hide').removeClass('hide');
//                                    $('.lblApproval' + id).parents('li.block:first').removeClass('render');
//                                    $('#chkThumb' + id).click(function () {
//                                        if ($(this).is("[approval]")) {
//                                            var id = $(this).attr('approval');
//                                            $('.lblApproval' + id).toggleClass('selected');
//                                        }
//                                        else if ($(this).is("[folder]")) {
//                                            var id = $(this).attr('folder');
//                                            $('#lblFolder' + id).toggleClass('selected');
//                                        }
//                                        getSelectedApprovals();
//                                    });

//                                    var arr = IDs.split(',');
//                                    arr.splice(arr.indexOf('' + id), 1);
//                                    IDs = arr.join(',');

//                                    break;
//                                }
//                        }
//                    }
//                }

//                setTimeout(function () { getProgress(IDs); }, 2000);
//            }
//        });
//    }
//}

// Smart Columns
function smartColumns() {
    $(".approval-grid").css({ 'width': "100%" });

    var colWrap = $(".approval-grid").width();
    var colNum = Math.floor(colWrap / 227);
    var colFixed = Math.floor(colWrap / colNum);

    $(".approval-grid").css({ 'width': colWrap });
    $(".approval-grid li.block").css({ 'width': colFixed });
    $(".approval-grid").css('display', 'inline!important');
}

function ShowOrHideIcons(approvalId) {
   
    var appObj = $(".lblApproval" + approvalId);

    var inProgress = $(appObj).parents("li.block:first").hasClass('render') && $(appObj).find(".progress .bar:visible").length > 0;
    var cannotDelete = $(appObj).parents("li.block:first").hasClass('no-delete');
    var cannotChangeAccess = $(appObj).parents("li.block:first").hasClass('no-access');
    var isError = $(appObj).find(".label-important.error").length > 0;
    var nrProcent = $(appObj).find(".progress .bar:visible").width();

    $("#tablet-proof-studio").addClass('tablet-icon-disabled');
    $("#tablet-details").addClass('tablet-icon-disabled');
    $("#tablet-access").addClass('tablet-icon-disabled');
    $("#tablet-share").addClass('tablet-icon-disabled');
    
    if (cannotDelete) {
        $("#tablet-delete").addClass('tablet-icon-disabled');
    } else {
        $("#tablet-delete").removeClass('tablet-icon-disabled');
    }

    if (!isNaN(approvalId)) {
        if ($(".lblApp.selected").length >= 1) {
            if (!inProgress && !isError) {
                $("#tablet-proof-studio").removeClass('tablet-icon-disabled');
                $("#tablet-details").removeClass('tablet-icon-disabled');
                if (cannotChangeAccess) {
                    $("#tablet-access").addClass('tablet-icon-disabled');
                    $("#tablet-share").addClass('tablet-icon-disabled');
                } else {
                    $("#tablet-access").removeClass('tablet-icon-disabled');
                    $("#tablet-share").removeClass('tablet-icon-disabled');
                }
            }
            if (isError || (inProgress && nrProcent == 0) || !inProgress && !cannotDelete) {
                $("#tablet-delete").removeClass('tablet-icon-disabled');
            }
        }
    }
}

function initHandlers() {
    $(".lblApp").tap(function () {
        var selectedElementId = $(this).attr("id").replace("lblApproval", "");
        selectedElementId = selectedElementId.replace("lblApproval", "");
        var currentApprovalId = parseInt(selectedElementId, 10);

        if (!isNaN(currentApprovalId)) {
            // mark current item as selected
            $('.lblApproval' + selectedElementId).toggleClass('selected');
        }

        if (!isNaN(currentApprovalId) && selectedApprovalId != currentApprovalId) {

            $(".lblApproval" + selectedApprovalId).removeClass("selected");
            selectedApprovalId = currentApprovalId;
        }

        ShowOrHideIcons(currentApprovalId);
    });

    $('input:checkbox[name^=ListAccessUsers]').click(function () {
        var isChecked = $(this).attr('checked');
        var selectedType = $(this).attr('SelectionType');
        if (isChecked && $('input:checkbox[SelectionType$=' + selectedType + ']').length && $('input:checkbox[SelectionType$=' + selectedType + '][checked==checked]').length) {
            $('#SelAll' + selectedType).text('@(Resources.lblDeselectAll) ' + selectedType);
        }
        else {
            $('#SelAll' + selectedType).text('@(Resources.lblSelectAll) ' + selectedType);
        }
    });

    $("#tablet-proof-studio").click(function () {
        if ($(this).hasClass("tablet-icon-disabled")) {
            return;
        }
        $("#tablet-loading").removeClass('tablet-icon-hide');
        var approvalId = $(".lblApp.selected").attr('id').replace("lblApproval", "");
        if (approvalId != "") {
            CallAction(approvalId, "Studio");
        }
    });
    $("#tablet-details").tap(function () {
        if ($(this).hasClass("tablet-icon-disabled")) {
            return;
        }
        $("#tablet-loading").removeClass('tablet-icon-hide');
        var approvalId = $(".lblApp.selected").attr('id').replace("lblApproval", "");
        if (approvalId != "") {
            CallAction(approvalId, "ViewApproval");
        }
    });
    $("#tablet-delete").tap(function () {
        if ($(this).hasClass("tablet-icon-disabled")) {
            return;
        }
        var approvalId = $(".lblApp.selected").attr('id').replace("lblApproval", "");
        if (approvalId != "") {
            CallAction(approvalId, "DeleteVersion");
        }
    });
    $("#tablet-access").tap(function () {
        if ($(this).hasClass("tablet-icon-disabled")) {
            return;
        }
        var approvalId = $(".lblApp.selected").attr('id').replace("lblApproval", "");
        if (approvalId != "") {
            CallAction(approvalId, "access");
        }
    });
    $("#tablet-share").tap(function () {
        if ($(this).hasClass("tablet-icon-disabled")) {
            return;
        }
        var approvalId = $(".lblApp.selected").attr('id').replace("lblApproval", "");
        if (approvalId != "") {
            CallAction(approvalId, "share");
        }
    });
    $("#tablet-approvaldetails-back").tap(function () {
        var links = $("p.breadcrumb").find("a[action]");
        for (i = 0; i < links.length; i++) {
            links[i].click();
        }
    });

    $("#tablet-view-type").unbind("tap");
    $("#tablet-view-type").tap(function (e) {
        e.preventDefault();
        var selectedOption = parseInt(localStorage.getItem('dashboardViewType'));
       
        if (parseInt(selectedOption) == parseInt($('#statusView').attr('data-view-type'))) {
            localStorage.setItem('dashboardViewType', parseInt($('#summaryView').attr('data-view-type')));
            selectedOption = parseInt($('#summaryView').attr('data-view-type'));
        } else {
            localStorage.setItem('dashboardViewType', parseInt($('#statusView').attr('data-view-type')));
            selectedOption = parseInt($('#statusView').attr('data-view-type'));
        }
        showDashboardView(selectedOption);
    });
    //fix for the case where the handler was already registered
    $("#tablet-approvals-sidebar").unbind("tap");
    $("#tablet-approvals-sidebar").tap(function (e) {
        e.preventDefault();
        if ($("#approvals-sidebar-wrapper").hasClass("active")) {
            $("#approvals-sidebar-wrapper").removeClass("active");
        } else {
            $("#menu-sidebar-wrapper").removeClass("active");
            $("#approvals-sidebar-wrapper").addClass("active");
        }
    });

    //fix for the case where the handler was already registered
    $("#tablet-settings").unbind("tap");
    $("#tablet-settings").tap(function (e) {
        e.preventDefault();
        if ($("#menu-sidebar-wrapper").hasClass("active")) {
            $("#menu-sidebar-wrapper").removeClass("active");
        } else {
            $("#approvals-sidebar-wrapper").removeClass("active");
            $("#menu-sidebar-wrapper").addClass("active");
        }
    });

    // Bind Approval Actions Events
    $('.approval-action').click(function () {
        var action = $(this).attr('action');
        var approval = $(this).attr('approval') || 0;

        if ((action != '') && (approval > 0) && !($(this).hasClass('locked'))) {
            $('#hdnSelectedApproval').val(approval);

            switch (action.toUpperCase()) {
                case 'ARCHIVEAPPROVAL':
                    {
                        $('#modelDialogDeleteConfirmation_1').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEAPPROVAL':
                    {
                        $('#modelDialogDeleteConfirmation_9').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEVERSION':
                    {
                        $('#modelDialogDeleteConfirmation_2').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEAPPROVALPERMANENT':
                    {
                        $('#modelDialogDeleteConfirmation_5').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DOWNLOADAPPROVAL':
                case 'STUDIO':
                    {
                        var download = (action.toUpperCase() == 'DOWNLOADAPPROVAL');
                        var requesturl = (download ? ('/Base/DownloadFile?aIDs=' + approval) : '/Approvals/Studio?isHtml=true&Ids=') + approval + '&Url=' + window.location;
                        $.ajax({
                            url: requesturl,
                            type: "POST",
                            success: function (content) {
                                if (download) {
                                    addDownloadNode(content);
                                }
                                else {
                                    window.location = content;
                                }
                            }
                        });
                        break;
                    }
                default:
                    {
                        $('#dynamicAction').attr('value', action).click();
                        break;
                    }
            }
        }
    });

    // Bind Folder Actions Events
    $('.folder-action').click(function () {
        var action = $(this).attr('action');
        var folder = $(this).attr('folder') || 0;

        if ((action != '') && !($(this).hasClass('locked'))) {
            $('#hdnSelectedFolder').val(folder);

            switch (action.toUpperCase()) {
                case 'ARCHIVEFOLDER':
                    {
                        $('#modelDialogDeleteConfirmation_3').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEFOLDER':
                    {
                        $('#modelDialogDeleteConfirmation_4').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEFOLDERPERMANENT':
                    {
                        $('#modelDialogDeleteConfirmation_6').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DOWNLOADFOLDER':
                    {
                        $.ajax({
                            url: '/Base/DownloadFile?fIDs=' + folder,
                            type: "POST",
                            success: function (content) {
                                addDownloadNode(content);
                            }
                        });
                        break;
                    }
                case 'CREATEFOLDER':
                case 'RENAMEFOLDER':
                    {
                        var create = (action.toUpperCase() == 'CREATEFOLDER');
                        var private = $(this).attr('private');
                        var foldername = '';
                        if (create) {
                            $('#modalDialogFolderCR .create').removeClass('hide');
                            $('#modalDialogFolderCR .rename').addClass('hide');
                        }
                        else {
                            $('#modalDialogFolderCR .create').addClass('hide');
                            $('#modalDialogFolderCR .rename').removeClass('hide');
                            private = private || 'True';

                            var blockid = 'cdflx_PBD';
                            $(this).parents('.block:first').attr('id', blockid);
                            foldername = $('#' + blockid + ' .block-content > .title .action').text().trim();
                            $(this).parents('.block:first').removeAttr('id');
                        }
                        $('#hdnFolderID').val(folder);
                        $('#hdnFolderParent').val(folder);
                        $('#hdnFolderIsPrivate').val(private);
                        $('#modalDialogFolderCR').modal({ backdrop: 'static', keyboard: false });
                        $('#modalDialogFolderCR input[type=text]').focus().val(foldername);
                        break;
                    }
                default:
                    {
                        $('#dynamicAction').attr('value', action).click();
                        break;
                    }
            }
        }
    });

    // Folder Tree Naviagiotn
    var cookiename = 'codeflex.foldertree';
    var cookievalue = $.cookie(cookiename) ? $.cookie(cookiename).split(',') : new Array();
    $('.foldertree .block .block-content.active').parents('.block').each(function () {
        var folder = $(this).attr('widget-id');
        if (cookievalue.indexOf(folder) == -1) {
            cookievalue.push(folder);
        }
        $.cookie(cookiename, cookievalue);
    });
    $('.foldertree .block').each(function () {
        var folder = $(this).attr('widget-id');
        if (cookievalue.indexOf(folder) > -1) {
            $('.foldertree .block[widget-id=' + folder + '] > .block-content').addClass('open');
        }
    });
    $('.foldertree .block .toggler').click(function (e) {
        var blockid = 'cfftBCT';
        $(this).parents('.block-content:first').attr('id', blockid);
        var folder = $(this).parents('.block:first').attr('widget-id');

        if ($('#' + blockid).hasClass('open')) {
            $('#' + blockid).removeClass('open');
            var index = cookievalue.indexOf(folder);
            cookievalue.splice(index, 1);
        }
        else {
            $('#' + blockid).addClass('open');
            if (cookievalue.indexOf(folder) == -1) {
                cookievalue.push(folder);
            }
        }
        $('#' + blockid).removeAttr('id');
        $.cookie(cookiename, cookievalue);
    });

    $("div.pagination-tablet li").tap(function () {
        window.location = $(this).find("a[url]:first").attr("url");
    });
}


function CallAction(approvalId, action) {
    var i;
    var links = $(".lblApproval" + approvalId).find("a[action]");
    for(i=0; i < links.length;i++) {
        if ($(links[i]).attr("action").toLowerCase() == action.toLowerCase()) {
            links[i].click();
            break;
        }
    }
    links = $("a[widget-toggle][widget-id]");
    for (i = 0; i < links.length; i++) {
        if ($(links[i]).attr("widget-toggle").toLowerCase() == "collaborator" + action.toLowerCase() && $(links[i]).attr("widget-id") == approvalId) {
            links[i].click();
        }
    }
}

$(".backup_picture").on('error', function () {
    $(this).attr('src', '/Content/img/noimage-240px-135px.png');
});

function showDashboardView(selectedOption) {
    if (selectedOption == null || isNaN(selectedOption)) {
        selectedOption = (isNaN(selectedOption) || selectedOption == null) ? $('#summaryView').attr('data-view-type') : selectedOption;
    }

    if (parseInt(selectedOption) == parseInt($('#summaryView').attr('data-view-type'))) {
        $('#summaryView').css('display', 'block');
        $('#statusView').css('display', 'none');
    } else {
        $('#summaryView').css('display', 'none');
        $('#statusView').css('display', 'block');
    }
    LoadApprovalPhaseDeadline();
}