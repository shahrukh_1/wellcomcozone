﻿var approvalId = 0;
var isFromProofStudio = true;

//show the Send Job to Deliver Popup
function showSendToDeliverPopup(selectedApproval) {
    approvalId = selectedApproval;
    $(".czn-modal-overlay").show();

    $.ajax({
        type: "GET",
        url: $("#hdnGetApprovalsPageCount").val(),
        data: { 'jobIdList': selectedApproval },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            //set pages count to session when hidden field is undefined
            if ($("#SelectedApprovalsPageCounts").val() === undefined) {
                window.localStorage.setItem("SelectedApprovalsPageCounts", result);
            }
            $("#SelectedApprovalsPageCounts").val(result);
            if ($('#modalDialogSubmitToDeliverFromPS div.modal-body div').length == 0) {
                $('#deliverPopUpLoading').removeClass("hidden");
                loadSubmitToDeliverPopup();
            } else {
                checkPageRange();
            }
        }
    });
}

// populate the Send Job to Deliver Popup async
function loadSubmitToDeliverPopup() {
    $.ajax({
        type: "GET",
        url: $("#hdnLoadSubmitToDeliverPopup").val(),
        data: { 'isFromProofStudio': isFromProofStudio },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (popup) {
            $('#deliverPopUpLoading').remove();
            $('#modalDialogSubmitToDeliverFromPS div.modal-body').append(popup.Content);
            checkPageRange();
            replaceOldBootStrapClases()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#deliverPopUpLoading').replaceWith('<label>Error Loading SubmitToDeliver Popup ' + textStatus + ' ' + errorThrown + '</label>'); ;
        }
    });
}

$(".close").on('click', function () {
    $('#modalDialogSubmitToDeliverFromPS').hide();
    $(".czn-modal-overlay").hide();
});

function AddDeliverJobCompleted(data) {
    var json_data = eval('(' + eval('(' + data.responseText + ')') + ')');
    if (json_data.Success) {
        ResetDeliverModel();
        $('#modalDialogSubmitToDeliverFromPS').hide();
        $(".czn-modal-overlay").hide();
    }

    $('#AddDeliverJob').removeClass('disabled');
    $('#AddDeliverJob').removeAttr('disabled');
    $(".selected").removeClass('selected');

    var errors = new Array;
    for (var key in json_data.Errors) {
        if (key == '') {
            for (var i = 0; i < json_data.Errors[key].length; i++) {
                errors[errors.length] = "<li>" + json_data.Errors[key][i] + "</li>";
            }
        }
    }
    $("#modalDialogSubmitToDeliverFromPS .modal-body .validation-summary-errors").remove();
    $("#modalDialogSubmitToDeliverFromPS .modal-body .control-group:nth-child(5)").after("<div class=\"validation-summary-errors\" data-valmsg-summary=\"false\"><ul style=\"list-style-type: none; font-size: 13px; font-weight: 400; padding-left: 11%\">" + errors.join("\n") + "</ul></div>");
    $('button[data-loading-text]').button('reset');
}

function replaceOldBootStrapClases() {
    $(".block.active input").addClass("form-control");
    $(".block.active input").addClass("externalInputs");
    $(".input-append").addClass("inlineControls");
    $("#ext-language").addClass("czn-select");
    $("#deliverMessage").addClass("form-control");
    $("#deliverMessage").removeClass("span9");
    $(".icon-cog").addClass("fa fa-cog").removeClass(".icon-cog");
    $(".icon-remove").addClass("fa fa-times").removeClass(".icon-remove");

}

$("#AddDeliverJob").on('click', function () {
    $("#AddDeliverJob").button('loading');
});
$('#formAddDeliverJob').on('submit', function () {
    if (($('div .block input[type=text].first-name').val() != undefined && $('div .block input[type=text].first-name').val() != "") || ($('div .block input[type=text].last-name').val() != undefined && $('div .block input[type=text].last-name').val() != "") || ($('div .block input[type=text].email').val() != undefined && $('div .block input[type=text].email').val() != "")) {
        $('#errSelectExternalUser').show();

        $("#imgSavingLoader").hide();

        setTimeout(function () {
            $('button[data-loading-text]').button('reset');
        }, 2000);
        return false;
    }
    else {
        //send ajax
        $.ajax({
            type: 'POST',
            url: this.action,
            type: this.method,
            data: $(this).serialize() + '&__RequestVerificationToken' + $("#formAddDeliverJob input").val(),
            complete: function (result) {
                AddDeliverJobCompleted(result);
            }
        });
    }
    return false;
});

$('div .block input[type=text].first-name, div .block input[type=text].last-name, div .block input[type=text].email').on('keyup', function () {
    if ($('div .block input[type=text].first-name').val() == "" && $('div .block input[type=text].last-name').val() == "" && $('div .block input[type=text].email').val() == "") {
        $('#errSelectExternalUser').hide();
    }
});

function removeUnobstrusiveValidations() {
    //Removes validation from input-fields
    $('.input-validation-error').addClass('input-validation-valid valid');
    $('.input-validation-error').removeClass('input-validation-error');
    //Removes validation message after input-fields
    $('.field-validation-error').addClass('field-validation-valid');
    $('.field-validation-error').removeClass('field-validation-error');
}

function ResetModelCustom() {
    $('.modal').each(function () {

        $(this).find("input[type='text']").each(function () {
            $(this).val('');
        });
        $(this).find("input[type='password']").each(function () {
            $(this).val('');
        });
        $(this).find("input[type='checkbox']").each(function () {
            $(this).removeAttr("checked");
        });

        $("#SelectedCPServerId").val('');
        $("#SelectedCPServerId").val('');

        // move modal-body scrolldown on top
        $(".modal .modal-body").scrollTop(0);

        $(this).find('.error').each(function () {
            $(this).removeClass('error');
            $(this).find('.input-validation-error').each(function () {
                $(this).removeClass('input-validation-error');
                $(this).addClass('valid');
            });
            $(this).find('.field-validation-error').each(function () {
                $(this).removeClass('field-validation-error');
                $(this).addClass('field-validation-valid');
            });
        });
    });
}

//replace old icons with new icons and replace old classes
$("#modalDialogSubmitToDeliverFromPS").on("click", ".btn.btn-success", function () {
    $(".icon-cog").addClass("fa fa-cog").removeClass(".icon-cog");
    $(".icon-remove").addClass("fa fa-times").removeClass(".icon-remove");
    $('.empty-block-user.alert.alert-error').addClass('alert alert-danger');
    $('.empty-block-email.alert.alert-error').addClass('alert alert-danger');
    $('.empty-block-email-exists.alert.alert-error').addClass('alert alert-danger');
    
});