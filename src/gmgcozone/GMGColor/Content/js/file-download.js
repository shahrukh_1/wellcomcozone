﻿function downloadSuccess(content) {
    if (content != "ftp") {
        addDownloadNode(content);
    }
    $('#modalFileDownload').modal('hide');
}
function downloadSuccessPF(content) {
    if (content != "ftp") {
        addDownloadNode(content);
        $('#btnMultiDownload')[0].innerHTML = "<i class='icon-download-alt'></i>";
    }
}
function downloadFailed(e) {
    ResetFileDownloadModel();
    $('#errorsList').append('<li>' + e.responseText + '</li>');
    $('#ModelErrors').show();
}
function downloadFailedPF(e) {
    ResetFileDownloadModel();
    $('#errorsList').append('<li>' + e.responseText + '</li>');
    $('#ModelErrors').show();
}

function downloadComplete(e) {
    $('#DownloadFile').button('reset');
    $('form').dirtyForms('setClean');
    resetControlsToDefaultValues();
}
function downloadCompletePF(e) {
    $('#btnMultiDownload').button('reset');
    $('form').dirtyForms('setClean');
}

function resetControlsToDefaultValues()
{
    $('#downloadFileType option[value="0"]').attr('selected', true);
    $('#downloadVersionType option[value="0"]').attr('selected', true);
    $('#downloadDestination option[value="0"]').attr('selected', true);
    $('#downloadFileType').attr('disabled', false);
    $('#fileTypeInfo').hide();
}

function ResetFileDownloadModel() {
    resetControlsToDefaultValues();
    $('#ModelErrors').hide();
    $('#errorsList').empty();
}

$(document).ready(function () {
    console.log('file download document ready');
    if ($('#ApplicationModule').val() === "Deliver") {
        $('#downloadVersionType').attr("disabled", true);
        console.log('file download deliver');
    } else {
        $('#downloadVersionType').attr("disabled", false);
        console.log('file download collaborate');
    }

    $(document).on('change', '#downloadDestination', function () {
        var self = $(this);
        console.log('file download on change destination');
        if (self.attr("value") !== "0") {
            $('#downloadFileType').attr('disabled', true);
            $('#fileTypeInfo[rel="tooltip"]').tooltip();
            $('#fileTypeInfo').show();
            if ($('#IsZipFileWhenDownloadingEnabled').val() == "True") {
                $('#downloadFileType option[value="1"]').attr('selected', true);
            } else {
                $('#downloadFileType option[value="0"]').attr('selected', true);
            }
        } else {
            $('#downloadFileType').attr('disabled', false);
            $('#downloadFileType option[value="0"]').attr('selected', true);
            $('#fileTypeInfo').hide();
        }
    });
});



