﻿var pageModel, oldSelectedLanguage;

$(document).ready(function () {
    pageModel = jQuery.parseJSON($("#StatusesModel").val());
    oldSelectedLanguage = pageModel.LoadedUserLanguage;
});

function clearDecision(decId) {
    $('#DecisionTxtBox' + decId).val('');

    for (var i = 0; i < pageModel.CustomDecisions.length; i++) {
        if (decId == pageModel.CustomDecisions[i].DecisionId) {
            var currentDecission = pageModel.CustomDecisions[i];
            for (var j = 0; j < currentDecission.CustomDecisionLanguages.length; j++) {
                if (currentDecission.CustomDecisionLanguages[j].Locale == $("#LoadedUserLanguage").val()) {
                    currentDecission.CustomDecisionLanguages[j].Name = "";
                    break;
                }
            }   
            break;
        }
    }
}

$("#LoadedUserLanguage").change(function () {
    var newLanguage = parseInt($(this).val());
    for (var i = 0; i < pageModel.CustomDecisions.length; i++) {
        var currentDecission = pageModel.CustomDecisions[i];
        var oldLanguageFound = false;
        var newLanguageFound = false;

        //save old language values
        for (var j = 0; j < currentDecission.CustomDecisionLanguages.length; j++) {
            if (currentDecission.CustomDecisionLanguages[j].Locale == oldSelectedLanguage) {
                currentDecission.CustomDecisionLanguages[j].Name = $("#DecisionTxtBox" + currentDecission.DecisionId).val();
                oldLanguageFound = true;
                break;
            }
        }
        if (!oldLanguageFound) {
            var newLanguageObj = {};
            newLanguageObj.Locale = oldSelectedLanguage;
            newLanguageObj.Name = $("#DecisionTxtBox" + currentDecission.DecisionId).val();
            currentDecission.CustomDecisionLanguages.push(newLanguageObj);
        }

        //load new language values
        for (var j = 0; j < currentDecission.CustomDecisionLanguages.length; j++) {
            if (currentDecission.CustomDecisionLanguages[j].Locale == newLanguage) {
                $("#DecisionTxtBox" + currentDecission.DecisionId).val(currentDecission.CustomDecisionLanguages[j].Name);
                newLanguageFound = true;
                break;
            }

        }
        if (!newLanguageFound) {
            $("#DecisionTxtBox" + currentDecission.DecisionId).val("");
        }
    }
    oldSelectedLanguage = newLanguage;
});


$("#statusForm").submit(function () {
    var currentSelectedLanguage = parseInt($("#LoadedUserLanguage").val());
    for (var i = 0; i < pageModel.CustomDecisions.length; i++) {
        var languageExists = false;
        var currentDecission = pageModel.CustomDecisions[i];
        for (var j = 0; j < currentDecission.CustomDecisionLanguages.length; j++) {
            if (currentDecission.CustomDecisionLanguages[j].Locale == currentSelectedLanguage) {
                currentDecission.CustomDecisionLanguages[j].Name = $("#DecisionTxtBox" + currentDecission.DecisionId).val();
                languageExists = true;
                break;
            }
        }
        if (!languageExists) {
            var newLanguageObj = {};
            newLanguageObj.Locale = currentSelectedLanguage;
            newLanguageObj.Name = $("#DecisionTxtBox" + currentDecission.DecisionId).val().trim();
            currentDecission.CustomDecisionLanguages.push(newLanguageObj);
        }
    }

    $("#StatusesModel").val(JSON.stringify(pageModel));
});