﻿function renameChecklist(name, checklistId, startFromFirstItem) {
    showHideControls(false);
    $('#hdnChecklistId').val(checklistId);
    $('#modalNewChecklist input[type=text]').focus().val(name);
    $('#modalNewChecklist input[type=checkbox]').focus().attr("checked", startFromFirstItem);;
    $('#modalNewChecklist').modal({ backdrop: 'static', keyboard: false });
}

$('#modalNewChecklist').on('show', function () {
    if (parseInt($('#hdnChecklistId').val()) > 0) {
        showHideControls(false);
    } else {
        showHideControls(true);
    }
});

//show/hide specific controls from page depending on action (Create/Rename)
function showHideControls(isCreateAction) {
    if (isCreateAction) {
        $('#modalNewChecklist .create').removeClass('hide');
        $('#modalNewChecklist .rename').addClass('hide');

        $('button[class="create"]').show();
        $('button[class="rename"]').hide();
    } else {
        $('#modalNewChecklist .create').addClass('hide');
        $('#modalNewChecklist .rename').removeClass('hide');

        $('button[class="create"]').hide();
        $('button[class="rename"]').show();
    }
}

function rowToogleChecklist(checklistId, urlAction) {
    if ($('#checklist' + checklistId).hasClass('hide')) {
        $('#trigger' + checklistId + ' i.icon-caret-right').addClass('icon-caret-down');
        $('#trigger' + checklistId + ' i.icon-caret-right').removeClass('icon-caret-right');
        $('#checklist' + checklistId).removeClass('hide');

        if ($('#checklist' + checklistId + '_Item').length == 0) {
            $.ajax({
                type: "GET",
                url: urlAction,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (phasesDetails) {
                    if (phasesDetails.Status == 200) {
                        var parentGroupCellId = $('#checklist' + checklistId);
                        var cellText = $(parentGroupCellId).html();
                        if (cellText.trim() != phasesDetails.Content.trim()) {
                            $(parentGroupCellId).append(phasesDetails.Content);
                        }
                        $('#checklist' + checklistId + '_Item .up:first, #checklist' + checklistId + '_Item  .down:last').css('display', 'none');
                        $('#checklist' + checklistId + '_Item .up, #checklist' + checklistId + '_Item .down').not('.up:first, .down:last').css('display', 'block');
                        $('#checklist' + checklistId + '_Item td').css('vertical-align', 'middle');

                        $('#imgChecklistLoading' + checklistId).remove();
                    } else {
                        $('#errOccured').show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                        return;
                    } else {
                        $('#errOccured').show();
                    }
                }
            });
        }
    } else {
        $('#trigger' + checklistId + ' .icon-caret-down').addClass('icon-caret-right');
        $('#trigger' + checklistId + ' .icon-caret-down').removeClass('icon-caret-down');

        $('#checklist' + checklistId).addClass('hide');
    }
}