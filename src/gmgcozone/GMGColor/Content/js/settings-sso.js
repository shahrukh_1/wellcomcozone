﻿$(document).ready(function () {
    if ($("#ActiveConfiguration").val() === "Saml") {
        $("#samlSettings").show();
        ConfigureSamlForm();
    }
    else {
        $("#auth0Settings").show();
        ConfigureAuth0Form();
    }
});

function ConfigureAuth0Form() {
    $("#SwitcTohAuth0").attr("disabled", true);
    $("#SwitchToSaml").attr("disabled", false);

    document.querySelector("input[id$=ClientSecret]").setAttribute("type", "password");
    if ($("input[id$=ClientSecret]").val()) {
        $("#showSecretChk").prop("disabled", false);
        $("#btnDisableSSO").prop("disabled", false);
    }
}

function ConfigureSamlForm() {
    $("#SwitcTohAuth0").attr("disabled", false);
    $("#SwitchToSaml").attr("disabled", true);

    var certificate = $("#Saml_CertificateName").val();
    if (certificate) {
        $("#addUpdateSoo").prop('disabled', false);
        $("#btnDisableSSO").prop('disabled', false);
    }
    else {
        $("#addUpdateSoo").prop('disabled', true);
    }
    $(document).on("click", ".btn.btn-danger", function () {
        if ($("*[id^=hdnFileName]").length != 0) {
            $("#addUpdateSoo").prop('disabled', true);
        }
    });
}

$("input[id$=ClientSecret]").on("blur", function () {
    if ($("input[id$=ClientSecret]").val()) {
        $("#showSecretChk").prop("disabled", false);
    }
});

$("#showSecretChk").on("click", function () {
    if ($("input[id$=ClientSecret]").attr("type") == "text") {
        document.querySelector("input[id$=ClientSecret]").setAttribute("type", "password");
    }
    else {
        document.querySelector("input[id$=ClientSecret]").setAttribute("type", "text");
    }
});

$("#btnDisableSSO").on("click", function () {
    $('#modelDialogDeleteConfirmation_1').modal({ backdrop: 'static', keyboard: false });
    return false;
});

$("#SwitcTohAuth0").on("click", function () {
    switchSettings(true, false);
});

$("#SwitchToSaml").on("click", function () {
    switchSettings(false, true);
});

function switchSettings(auth, saml) {
    if (checkSettingHasValue() != true ) {
        $("#actionConfirmation").modal("show");
    } else {
        $("#SwitcTohAuth0").attr("disabled", auth);
        $("#SwitchToSaml").attr("disabled", saml);

        if (auth) {
            $("#auth0Settings").show();
            $("#samlSettings").hide();
            $("#ActiveConfiguration").val("Auth0");
        }
        else {
            $("#auth0Settings").hide();
            $("#samlSettings").show();
            $("#ActiveConfiguration").val("Saml");
            if ($("*[id^=hdnFileName]").length != 0) {
                $("#addUpdateSoo").prop('disabled', true);
            }
        }
    }
}

$("#fileUpload_1").bind('fileuploadstop', function () {
    $("#addUpdateSoo").prop('disabled', false);
});

$(document).on("click", "button.btn.btn-danger", function () {
    $("#addUpdateSoo").prop('disabled', true);
    $("#btnDisableSSO").prop('disabled', true);
});

function checkSettingHasValue() {
    var result = true;
    var settings = $(".ssoSettings :input");
    $.each(settings, function (i, ival) {
        if (ival.value !== "" && ival.value != "False" && ival.value != "0" && ival.name != "accountRegion" && ival.name != "pathToTempFolder")
            return result = false;
    });;
    return result;
}

$(document).on("click", "#confirmAction", function (event) {
    if ($("#ActiveConfiguration").val() == "Auth0") {
        $("#Auth0_Domain, #Auth0_ClientId, #Auth0_ClientSecret").val("");
        $("#showSecretChk").attr('checked', false);
        $('.modal-backdrop').remove();
        $('#actionConfirmation').modal('toggle');
        switchSettings(false, true);
    } else {
        $("button.btn.btn-danger").trigger("click");
        $("#hdnFileName_1, #Saml_EntityId, #Saml_IdpIssuerUrl, #Saml_CertificateName, .approvalTitle, input[name^='ApprovalFileName'], input[name^='ApprovalSize'], input[name^='PdfPagesCount_0']").val("");
        $('.modal-backdrop').remove();
        $('#actionConfirmation').modal("toggle");
        switchSettings(true, false);
    }
});

$("#Auth0_Domain, #Auth0_ClientId, #Auth0_ClientSecret").on("blur", function () {
    $("#addUpdateSoo").attr("disabled", false);
});