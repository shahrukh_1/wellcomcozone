var previousIconSize;
var approvalIds;

function AddDeliverJobCompleted(data) {
    var location = window.location;
    var json_data = eval('(' + eval('(' + data.responseText + ')') + ')');
    if (json_data.Success) {
        ResetDeliverModel();
        $('#modalDialogSubmitToDeliver').modal('hide');
        window.location = location;
    }

    $('#AddDeliverJob').removeClass('disabled');
    $('#AddDeliverJob').removeAttr('disabled');
    $('#AddDeliverJob')[0].innerHTML = trAddColorProofInstance;
    $(".choose_item:checked").parents(".lblApp:first").removeClass('selected');
    $(".choose_item:checked").removeAttr("checked");
    $("#btnSubmitToDeliver").attr("disabled", "disabled");
    $("#btnMultiDownload").attr("disabled", "disabled");  
    $(".selected").removeClass('selected');
    $(".choose_item_all").removeAttr("checked");
       
    var errors = new Array;
    for (var key in json_data.Errors) {
        if (key == '') {
            for (var i = 0; i < json_data.Errors[key].length; i++) {
                errors[errors.length] = "<li>" + json_data.Errors[key][i] + "</li>";
            }
        }
    }
   
    $("#modalDialogSubmitToDeliver .modal-body .validation-summary-errors").remove();
    $("#modalDialogSubmitToDeliver .modal-body .control-group:nth-child(8)").after("<div class=\"validation-summary-errors\" data-valmsg-summary=\"false\"><ul style=\"list-style-type: none; font-size: 13px; font-weight: 400;\">" + errors.join("\n") + "</ul></div>");
}

function ResetModel() {
    $(this).find('input').each(function () {
        $(this).val('');
    });
}


function getSelectedApprovals() {
    $('#hdnSelectedFolders').val($('.choose_item:checked[folder]').map(function () { return $(this).attr('folder'); }).get().join(','));
    var selectedApprovals = $('.choose_item:checked[approval]').map(function () { return $(this).attr('approval'); }).get().join(',');
    var uniqueValues = JSON.parse("[" + selectedApprovals + "]").filter(function (item, i, ar) { return ar.indexOf(item) === i; }); //removes duplicated values
    $('#hdnSelectedApprovals').val(uniqueValues);

    $('*[widget-toggle=foldertree]').attr("widget-approvals", $('#hdnSelectedApprovals').val());
    $('*[widget-toggle=foldertree]').attr("widget-folders", $('#hdnSelectedFolders').val());

    var checkedApprovals = $('.choose_item:checked').length;
    $('.choose_item_all').attr('checked', ($('.choose_item').length == checkedApprovals));

    $("#SelectedApprovalJobs").val(uniqueValues);   

    var hasErrorElement = false;
    $(".selected").each(function () {
        if ($(this).find(".caption").text().indexOf("Error") != -1) {
            hasErrorElement = true;
        }
    });
    
    if (hasErrorElement || ($('.inProgress:checked').length > 0)) {
        $('#btnMultiDelete').removeAttr('disabled');
        $('#btnMultiDownload').attr('disabled', 'disabled');
        $('#btnSubmitToDeliver').attr('disabled', 'disabled');        
        $('#btnMoveFilesToFolder').attr('disabled', 'disabled');
        $('#btnMultiDocumentReport').attr('disabled', 'disabled');
        $('#btnMultiFileCompare').attr('disabled', 'disabled');
        $('#btnMultipleArchive').attr('disabled', 'disabled');
        $('#btnMarkMultiPhaseAsCompleted').attr('disabled', 'disabled');
        $(".approval-grid").find("[title = 'Archived']").hide();
        $("#approvals-index tbody tr").find("[title = 'Archived']").hide();
        $(".approval-grid").find("[title = 'Phase Complete']").hide();
        $("#approvals-index tbody tr").find("[title = 'Phase Complete']").hide();
         $('#btnMarkApprovalAsApproved').attr('disabled', 'disabled');
         $('#btnMarkApprovalAsRejected').attr('disabled', 'disabled');
         $('#btnMultiAccess').attr('disabled', 'disabled');
         $('#btnPF_MultiAccess').attr('disabled', 'disabled');
        $(".approval-grid").find("[title = 'Access']").hide();
        $("#approvals-index tbody tr").find("[title = 'Access']").hide();
    } else {
        if ($('#hdnSelectedApprovals').val() == "" && $('#hdnSelectedFolders').val() == "") {
            $('#btnMultiDownload').attr('disabled', 'disabled');
            $('#btnMultiDelete').attr('disabled', 'disabled');
            $('#btnSubmitToDeliver').attr('disabled', 'disabled');          
            $('#btnMoveFilesToFolder').attr('disabled', 'disabled');
            $('#btnMultiDocumentReport').attr('disabled', 'disabled');
            $('#btnMultiFileCompare').attr('disabled', 'disabled');
            $('#btnMultipleArchive').attr('disabled', 'disabled');
            $('#btnMultiAccess').attr('disabled', 'disabled');
            $('#btnPF_MultiAccess').attr('disabled', 'disabled');
            $('#btnMarkMultiPhaseAsCompleted').attr('disabled', 'disabled');
            $(".approval-grid").find("[title = 'Archived']").show();
            $("#approvals-index tbody tr").find("[title = 'Archived']").show();
            $(".approval-grid").find("[title = 'Phase Complete']").show();
            $("#approvals-index tbody tr").find("[title = 'Phase Complete']").show();
             $('#btnMarkApprovalAsApproved').attr('disabled', 'disabled');
             $('#btnMarkApprovalAsRejected').attr('disabled', 'disabled');
            $(".approval-grid").find("[title = 'Access']").show();
            $("#approvals-index tbody tr").find("[title = 'Access']").show();
        }
        else {
            $('#btnMultiDownload').removeAttr('disabled');
            $('#btnMultiDelete').removeAttr('disabled');
            $('#btnSubmitToDeliver').removeAttr('disabled');         
            $('#btnMoveFilesToFolder').removeAttr('disabled');
            $('#btnMultiDocumentReport').removeAttr('disabled');
            $('#btnMultipleArchive').removeAttr('disabled');
            $('#btnMultiAccess').removeAttr('disabled');
            $('#btnPF_MultiAccess').removeAttr('disabled');
            $('#btnMarkMultiPhaseAsCompleted').removeAttr('disabled');
            $(".approval-grid").find("[title = 'Archived']").show();
            $("#approvals-index tbody").find("[title = 'Archived']").show();
            $(".approval-grid").find("[title = 'Phase Complete']").show();

            $("#approvals-index tbody tr").find("[title = 'Phase Complete']").show();
            $(".approval-grid").find("[title = 'Access']").show();
            $("#approvals-index tbody").find("[title = 'Access']").show();

            $.ajax({
                url: "/Approvals/GetApprovalsDecision",
                type: "GET",
                data: { approvalIds: $('#hdnSelectedApprovals').val() },
                dataType: "json",
                success: function (data) {
                    if (data.Content.Data) {
                        $('#btnMarkApprovalAsApproved').removeAttr('disabled');
                        $('#btnMarkApprovalAsRejected').removeAttr('disabled');
                    } else {
                        $('#btnMarkApprovalAsApproved').attr('disabled', 'disabled');
                        $('#btnMarkApprovalAsRejected').attr('disabled', 'disabled');
                    }
                }
            });
            
            if ($('.no-phase-complete:checked').length > 0) {
                $('#btnMarkMultiPhaseAsCompleted').attr('disabled', 'disabled');
                $(".approval-grid").find("[title = 'Phase Complete']").hide();
                $("#approvals-index tbody tr").find("[title = 'Phase Complete']").hide();
            }

            if ($('.no-archive:checked').length > 0) {
                $('#btnMultipleArchive').attr('disabled', 'disabled');
                $(".approval-grid").find("[title = 'Archived']").hide();
                $("#approvals-index tbody tr").find("[title = 'Archived']").hide();
            }

            if ($('.no-access:checked').length > 0) {
                $('#btnMultiAccess').attr('disabled', 'disabled');
                $(".approval-grid").find("[title = 'Access']").hide();
                $("#approvals-index tbody tr").find("[title = 'Access']").hide();
            }

            if ($('.no-access:checked').length > 0) {
                $('#btnPF_MultiAccess').attr('disabled', 'disabled');
                $(".approval-grid").find("[title = 'Access']").hide();
                $("#approvals-index tbody tr").find("[title = 'Access']").hide();
            }

            if ($('.no-download:checked').length > 0) {
                $('#btnMultiDownload').attr('disabled', 'disabled');
            }

            if ($('.no-delete:checked').length > 0) {
                $('#btnMultiDelete').attr('disabled', 'disabled');
            }

            if ($('.no-moveToFolder:checked').length > 0) {
                $('#btnMoveFilesToFolder').attr('disabled', 'disabled');
            }

            if ($('.no-report:checked').length > 0) {
                $('#btnMultiDocumentReport').attr('disabled', 'disabled');
            }

            if ((($('.NotExecutedWorkflow:checked').length > 0) && ($('.ExecutedWorkflow:checked').length > 0)) ||
                ($('.NotExecutedWorkflow:checked').length == 0) && ($('.ExecutedWorkflow:checked').length == 0) ){
                $('#btnPF_MultiAccess').attr('disabled', 'disabled');
                if ($('#hdnSelectedApprovals').val() != "") {
                $("#approvals-index tbody tr").find("[title = 'Access']").hide();
                }
            }

            if ((($('.NotExecutedWorkflow:checked').length > 0) && ($('.ExecutedWorkflow:checked').length == 0)) ||
                ($('.NotExecutedWorkflow:checked').length == 0) && ($('.ExecutedWorkflow:checked').length > 0)) {
                $('#btnPF_MultiAccess').removeAttr('disabled');
                $("#approvals-index tbody tr").find("[title = 'Access']").show();
            }
            if ($('.NotExecutedWorkflow:checked').length > 0)
            {
                isWorkfFlowAccess = true;
            }
            else {
                isWorkfFlowAccess = false;
            }

            if ($('.other-type:checked').length != $('.choose_item:checked[approval]').length) {
                $('#btnMultiFileCompare').attr('disabled', 'disabled');
            }

            if (uniqueValues.length >= 2 && ($('.other-type:checked').length == $('.choose_item:checked[approval]').length)) {
                $('#btnMultiFileCompare').removeAttr('disabled');
            } else {
                $('#btnMultiFileCompare').attr('disabled', 'disabled');
            }
        }
    }

    if ($('#hdnSelectedFolders').val() != "") {
        $('#btnSubmitToDeliver').attr('disabled', 'disabled');
    }
}

// Smart Columns
function smartColumns() {
    $(".approval-grid").css({ 'width': "100%" });

    var colWrap = $(".approval-grid").width();
    var colNum = Math.floor(colWrap / 282);
    var colFixed = Math.floor(colWrap / colNum);

    $(".approval-grid").css({ 'width': colWrap });
    $(".approval-grid li.block").css({ 'width': colFixed });

    var fileCountWidth = ($(".well.clearboth").width() - ($("#headerRightSection").width() + $("#headerLeftSection").width() + 22));
    $("#fileCount").css("width", fileCountWidth);

    var ImageResizeWidth = ($("#approvals-index thead tr th:first-child").width() - 38);

    $(".approval-thumbnail-cell .approval-status-view").css({ "width": ImageResizeWidth, "height": ImageResizeWidth });
    $(".approval-thumbnail-cell .approval-status-view a.approval-action").css({ "width": ImageResizeWidth, "height": ImageResizeWidth });
    $(".approval-thumbnail-cell .approval-status-view .img-frame").css({ "width": ImageResizeWidth, "height": ImageResizeWidth });
    $(".approval-thumbnail-cell .approval-status-view .img-frame img").css({ "max-width": ImageResizeWidth, "max-height": ImageResizeWidth });
}
         
function InitApprovalsGrid() {
    var ListIDs = typeof (inProgressApprovalIDs) != "undefined" ? inProgressApprovalIDs : [];
 
    smartColumns();
    
    $('.choose_item_all').on('click', function () {
        if ($(this).is(':checked')) {
            // Select approvals except processing
            $('.lblApp').addClass('selected');
            $('.choose_item, .customCheckBox .checkbox').attr('checked', 'checked');
        }
        else {
            $('.lblApp').removeClass('selected');
            $('.choose_item, .customCheckBox .checkbox').removeAttr('checked');
        }
        getSelectedApprovals();
    });

    $('.choose_item').on('click', function () {
        if ($(this).is("[approval]")) {
            var id = $(this).attr('approval');
            $('.lblApproval' + id).toggleClass('selected');
            if ($(this).is(':checked')) {
                $('.chkThumb' + id).attr('checked', 'checked');
            } else {
                $('.chkThumb' + id).removeAttr('checked');
            }

        }
        else if ($(this).is("[folder]")) {
            var id = $(this).attr('folder');
            $('#lblFolder' + id).toggleClass('selected');
        }
        getSelectedApprovals();
    });

    // Bind Approval Actions Events
    $('.approval-action').on('click', function () {
        var action = $(this).attr('action');
        var approval = $(this).attr('approval') || 0;
        var approvals = $('#hdnSelectedApprovals').val();
        
        var ids = [];
        if (approvals.length > 0) {
            var items = approvals.split(',');
            items.forEach(function (item) {
                ids.push(parseInt(item, 10));
            });
        } else {
            ids.push(parseInt(approval, 10));
        }

        if ((action != '') && ((approval > 0) || ids.length > 0) && !($(this).hasClass('locked'))) {
            $('#hdnSelectedApproval').val(approval);

            switch (action.toUpperCase()) {
                case 'ARCHIVEAPPROVAL':
                    {
                        $('#modelDialogDeleteConfirmation_1').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEAPPROVAL':
                    {
                        $('#modelDialogDeleteConfirmation_9').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEVERSION':
                    {
                        $('#modelDialogDeleteConfirmation_2').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DELETEAPPROVALPERMANENT':
                    {
                        $('#modelDialogDeleteConfirmation_5').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'RESTOREARCHIVEDAPPROVAL':
                    {
                        $('#modelDialogDeleteConfirmation_3').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'DOWNLOADAPPROVAL':
                    {
                        $("#FolderIds").val("");
                        $("#FileIds").val(approval);
                        resetFileTypeState(approval);
                        $("#modalFileDownload").modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                case 'STUDIO':
                    {
                        var requesturl = '/Approvals/Studio?Ids=' + ids.join(',') + '&url=' + window.location;
                        $.ajax({
                            url: requesturl,
                            type: "POST",
                            data: { __RequestVerificationToken: $("#formMain input").val() },
                            success: function (url) {
                                window.location = url;
                            }
                        });
                        break;
                    }
                case 'STUDIOHTML':
                    {
                        var requesturl = '/Approvals/Studio?Ids=' + ($(this).val() == "" ? approval : ids.join(',')) + '&url=' + window.location;

                        $.ajax({
                            url: requesturl,
                            type: "POST",
                            data: { __RequestVerificationToken: $("#formMain input").val() },
                            success: function (url) {
                                window.location = url;
                            }
                        });
                        break;
                    }
                case 'MARKPHASEASCOMPLETED':
                    {
                        $('#modelDialogDeleteConfirmation_10').modal({ backdrop: 'static', keyboard: false });
                        break;
                    }
                default:
                    {
                        $('#dynamicAction').attr('value', action).click();
                        break;
                    }
            }
        }
    });

    var width = $('.approval-status-view').first().width();
    if (width <= 40) {
        previousIconSize = 'S';
        $('.smallIcon i').show();
    } else if (width <= 100) {
        previousIconSize = 'M';
        $('.mediumIcon i').show();
    } else {
        previousIconSize = 'L';
        $('.largeIcon i').show();
    }

    $('.iconSize').on('click', function () {
        $(this).children('i').show();
        var scaleFactor = 1;

        if ($(this).hasClass('smallIcon')) {
            $('.mediumIcon i').hide();
            $('.largeIcon i').hide();
            if (previousIconSize == 'M') {
                scaleFactor = 1 / 2;
            } else if (previousIconSize == 'L') {
                scaleFactor = 1 / 4;
            }
            previousIconSize = 'S';
        } else if ($(this).hasClass('mediumIcon')) {
            $('.smallIcon i').hide();
            $('.largeIcon i').hide();
            if (previousIconSize == 'S') {
                scaleFactor = 2;
            } else if (previousIconSize == 'L') {
                scaleFactor = 1 / 2;
            }
            previousIconSize = 'M';
        } else {
            $('.mediumIcon i').hide();
            $('.smallIcon i').hide();
            if (previousIconSize == 'M') {
                scaleFactor = 2;
            } else if (previousIconSize == 'S') {
                scaleFactor = 4;
            }
            previousIconSize = 'L';
        }

        $.each($('.approval-status-view').find('.img-frame'), function (idx, item) {
            $(item).width($(item).width() * scaleFactor);
            $(item).height($(item).height() * scaleFactor);
            //$(item).parent().height($(item).height() * scaleFactor);
            //$(item).parent().width($(item).width() * scaleFactor);
            $(item).width($(item).children('img').width());
            $(item).height($(item).children('img').height());
        });
    });

    $("#modalDialogSubmitToDeliver").on('shown', function () {
        //get selected approvals page count
        $.ajax({
            type: "GET",
            url: getApprovalsPageCountUrl,
            data: { 'jobIdList': $('#hdnSelectedApprovals').val() },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                //set pages count to session when hidden field is undefined
                if ($("#SelectedApprovalsPageCounts").val() === undefined) {
                    window.localStorage.setItem("SelectedApprovalsPageCounts", result);
                }

                $("#SelectedApprovalsPageCounts").val(result);

                if ($('#modalDialogSubmitToDeliver div.modal-body div').length == 0) {
                    $('#deliverPopUpLoading').show();
                    loadSubmitToDeliverPopup();
                } else {
                    checkPageRange();
                }
            }
        });
    });  

    function resetFileTypeState(approvals) {
        if (approvals.split(',').length > 1) {
            $('#downloadFileType option[value="1"]').attr('selected', true);
            $('#downloadFileType option[value="0"]').attr('disabled', true);
        }
        else {
            $('#downloadFileType option[value="0"]').attr('selected', true);
            $('#downloadFileType option[value="0"]').attr('disabled', false);
        }
    };

    $('#btnFilter').on('click', function () {
        var selectedFilterId = $('#hdnSelectedFilter')[0].value;
        localStorage.setItem('dashboardFilterType', parseInt(selectedFilterId));
    });

    $('#btnSortingOrder').on('click', function () {
        var selectedSortingId = $('#hdnSortingOrder')[0].value;
        localStorage.setItem('dashboardSortingOrderType', parseInt(selectedSortingId));
    });

    $('#btnMultiDownload').on('click', function () {
        $("#FileIds").val($('#hdnSelectedApprovals').val());
        resetFileTypeState($('#hdnSelectedApprovals').val());
        $("#FolderIds").val($('#hdnSelectedFolders').val());
        $("#modalFileDownload").modal({ backdrop: 'static', keyboard: false });
    });

    $(".backup_picture").on('error', function () {
        $(this).attr('src', '/Content/img/noimage-240px-135px.png');
    });

    $('i[rel="tooltip"]').tooltip();
    $('button[rel="tooltip"]').tooltip();

    $('.expandableItem').on('click', function () {
        var approval = $(this).attr('data-item');
        var row = $(this).parents('tr:first');
        var disableSOADIndicator = $(this).attr('data-item-show-status');
        var isDetailsLoaded = row.next().hasClass('userActivityDetails');
        if (isDetailsLoaded) {
            row.next().toggleClass('hide');
        } else {
            var newRowHtml = $('#detailsRowTemplate').html();
            var newRow = $(newRowHtml);
            row.after(newRow);
            $.ajax({
                type: "GET",
                url: $('#loadActivityUrl').val() + "?approval=" + approval,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (userActivity) {
                    if (userActivity.Status == 200) {
                        var cellText = $(newRow).children().eq(1).html();
                        if (cellText.trim() != userActivity.Content.trim()) {
                            $(newRow).children().eq(1).append(userActivity.Content);
                        }
                        $(newRow).find('img').remove();
                       
                        if (disableSOADIndicator.toLowerCase() == "false") {
                            loadUserSOADIndicator(parseInt(approval));
                        }                        
                    } else {
                        $('#errOccured').show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                        return;
                    } else {
                        $('#errOccured').show();
                    }
                }
            });
        }

        //change arrow icon based on current state
        var icon = $(this).children('i');
        if (icon.hasClass('icon-angle-down')) {
            icon.removeClass('icon-angle-down').addClass('icon-angle-up');
        } else {
            icon.removeClass('icon-angle-up').addClass('icon-angle-down');
        }
    });

    $('.ViewAllDecisionMakers').on('click', function () {
        var approval = $(this).attr('data-item-approval');
        var currentPhase = $(this).attr('data-item-currentPhase');
        var pdm = $(this).attr('data-item-pdm');
        var IsExternalPdm = $(this).attr('data-item-IsExternalPDM');
        var isLoadAll = "True";
        $.ajax({
                type: "GET",
                url: $('#getDecisionMakers').val() + "?approval=" + approval + "&currentPhase=" + currentPhase + "&pdm=" + pdm + "&IsExtenalPDM=" + IsExternalPdm + "&IsLoadAllDecisionMakers=" + isLoadAll,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (decisionMakers) {
                    if (decisionMakers.Status == 200) {
                        $('#DecisionMakersOnLoad-' + approval).addClass('hide');
                        $('#btnViewAllDecisionMakers-' + approval).addClass('hide');
                        $('#LoadApprovalDecisionMakers-' + approval).append(decisionMakers.Content);
                    } else {
                        $('#errOccured').show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                        return;
                    } else {
                        $('#errOccured').show();
                    }
                }
            });
    });
}

function openCreateLinkForRetoucherPopup() {
    var folderId = $('.foldertree .block-content .active').parents('li:first').attr('widget-id');
    var linkForRetoucher = location.protocol + '//' + location.host + '/Approvals/Populate?topLinkID=' + folderId + '&status=crq';
    $('#currentFolderUrl').val(linkForRetoucher);
    $('#modalCreateLinkForRetoucher').modal({ backdrop: 'static', keyboard: false });
}

//$(document).on('click', '#formEvents .btn', function () {
//    $("#formEvents .btn").removeClass("btn-primary");
//    $(this).addClass("btn-primary");
//});

//$(document).on('click', '#ListViewThumbViewbtnGroup .btn', function () {
//    $("#formEvents .btn").removeClass("btn-primary");
//    $(this).addClass("btn-primary");
//});


$(document).on('click', '#btn-SummaryView', function () {
    $("#formEvents .btn").removeClass("btn-primary");
    $(this).addClass("btn-primary");
});

$(document).on('click', '#btn-StatusView', function () {
    $("#formEvents .btn").removeClass("btn-primary");
    $(this).addClass("btn-primary");
});

function showDashboardView(selectedOption, approvals) {
    $('i[name^="viewType"]').css('display', 'none'); //make all options unselected   

    var summaryView = $('#summaryView');
    var statusView = $('#statusView');

    if (selectedOption ==  null || selectedOption == 0 || isNaN(selectedOption)) {
        selectedOption = (isNaN(selectedOption) || selectedOption == null) ? 0 : selectedOption;
        summaryView.css('display', 'block');
        statusView.css('display', 'none');
        $('[data-original-title="Thumbnail View"]').addClass("btn-primary");
        $("#formFilters .checkbox.pull-left").removeClass("checkboxPosition");
        //$('#iconSizeFilter').hide();
    } else {
        summaryView.css('display', 'none');
        statusView.css('display', 'block');
        $('[data-original-title="List View"]').addClass("btn-primary");
        if ($('#formMain div').hasClass("blank-state")) {
            $(".checkboxPosition").removeClass('checkboxPosition');
        } else {
            $("#formFilters .checkbox.pull-left").addClass("checkboxPosition");
        }
        
        //$('#iconSizeFilter').show();
    }

    $('#summaryView, #statusView').removeClass('visibilityHidden');
    $(".approval-grid").css({ 'display': 'inline!important', "width": "auto" });
  
    // select option based on given value
    $('i[name^="viewType' + selectedOption + '"]').css('display', 'inline-block');
    localStorage.setItem('dashboardViewType', parseInt(selectedOption));
    
    if ($('ul#viewTypeFilter').length > 0) {
        // display text of the selected option
        $('ul#viewTypeFilter li a:first').contents().first()[0].textContent = $('i[name^="viewType' + selectedOption + '"]').parent("a").text() + " ";
    }

    if (approvals !== undefined) {
        approvalIds = approvals;
    }
    
    if (!statusView || !statusView.attr("style") || !summaryView || !summaryView.attr("style")) {
        return;
    }

    if (statusView.attr("style").indexOf("none") === -1 && summaryView.attr("style").indexOf("none") !== -1) {
        LoadApprovalPhaseDeadline(approvalIds);
    }
}

/* Advanced search */
var advsData;

function populateAdvancedSearch(topLinkId) {
    $('#modalAdvancedSearch').modal({ backdrop: 'static', keyboard: false });
    loadAdvancedSearchPopup(topLinkId);
}

function loadAdvancedSearchPopup(topLinkId) {
    topLinkId = topLinkId || 0;
    $('#modalAdvancedSearch div.modal-body').empty();
    $.ajax({
        type: "GET",
        url: '/Approvals/LoadAdvancedSearchPopup?TopLinkID=' + topLinkId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (popup) {
            
            $('#modalAdvancedSearch div.modal-body').append(popup.Content);
            
            //remove the form's validation and re validate it 
            var form = $("#formAdvancedSearch")
                        .removeData("validator")
                        .removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse(form);

            dispalyAdvsModal();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#advancedSearchPopUpLoading').replaceWith('<label>Error loading advanced search popup ' + textStatus + ' ' + errorThrown + '</label>');
        }
    });
}


function loadCustomSearches(topLinkId, isManager) {
    advsData = advsData || {};
    advsData.TopLink = advsData.TopLink || topLinkId;
    advsData.IsManager = advsData.IsManager || isManager;
    
    // load shearchs from server. they have name and id of the search
    $.ajax({
        cache: false,
        url: '/Approvals/GetAdvancedSearchNames',
        type: "GET",
        success: function (items) {
            cleanCustomSearches();
            populateCustomSearches(items.Searches);
        },
        complete: function () {
        }
    });
}


function cleanCustomSearches() {
    $("#customSearch").empty();
}

function populateCustomSearches(items) {
    items.forEach(function(item) {
        $("#customSearch").append(createSearchListElement(item));
    });
    
  
    if (items.length > 3) {
        $(".box-container").css('height', 593);
        $("#customSearch").css('height', 100);

        $("#customSearch li").css('width', 230);
    } else {
        $(".box-container").css('height', 493 + items.length * 33);
        $("#customSearch").css('height', items.length * 33);

        $("#customSearch li").css('width', 250);
    }
}

function createSearchListElement(item) {
    var strClass = "";
    if (advsData.TopLink == item.ID) {
        strClass = " class='active'";
    }
    var strEditDelete = "";
    if (advsData.IsManager) {
        strEditDelete = "<button class=\"btn edit-delete-advs\" onclick=\"populateAdvancedSearch(" + item.ID + ")\" type='button'><i class='icon-pencil'></i></button>" +
            "<button class=\"btn edit-delete-advs filter-action\" action=\"AdvancedSearchDelete\" filter=\"" + item.ID + "\" type='button'><i class='icon-trash'></i></button>";
    }
    return "<li" + strClass + "><span class='edit-delete-advs-span pull-right'>" +
                strEditDelete +
                "</span>" +
                "<a href=\"Populate?topLinkID=" + item.ID + "\">" +
                    "<i class='icon-search'></i>" + item.Name +
                "</a>" +
        "</li>";
}

function deleteClickedAdvancedSearch(topLinkId) {
    $.ajax({
        type: "POST",
        url: "/Approvals/AdvancedSearchDelete?TopLinkID=" + topLinkId,
        dataType: "json",
        data: { __RequestVerificationToken: $("#formAdvancedSearch input").val() },
        success: function (result) {
            loadCustomSearches();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            } else {
                alert("Error on delete search " + textStatus + ' ' + errorThrown);
            }
        }
    });
}

function registerDashboardConnectionApprovalIds() {
    var hub = $.connection.instantNotificationsHub;
    var ids = [];

    if (hub) {
        $("label.lblApp").each(function () {
            if ($.inArray($(this).attr("approval"), ids) == -1) {
                ids.push($(this).attr("approval"));
            }
        });
        $.connection.hub.start().done(function () {
            hub.server.registerDashboardConnectionApprovalIds(ids.join('|')).done(function () {
                console.log("SIGNALR registerDashboardConnectionApprovalIds succeeded.");
            }).fail(function (error) {
                console.log("SIGNALR FAIL calling registerDashboardConnectionApprovalIds: " + error);
            });
        });
    }
}

