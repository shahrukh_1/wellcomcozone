﻿$(document).ready(function () {
    $('.action-menu').each(function () {
        if ($('.action-menu li').length > 0) {
            $('.groupBtn').show();
        } else {
            $('.groupBtn').hide();
        }
    });
    //override bootstrap default style
    $('.modal-label').width(90);
    $('.form-horizontal:not(#formViewingCond) .controls').css({ 'margin-left': '10px', 'margin-right': '30px' });
    $(".tab").click(function (e) {
        $(".tab").removeClass("active");
        $(this).addClass("active");
        $('.box').hide();
        $('div[data-item=box_' + $(this).index() + '_' + $(this).attr('data-item') + '][class="box"]').show();
    });

    var appId = $(".tabs-container").find("ul > li.active").attr("approval");

    if ($('#workflowTab' + appId).length > 0) {
        $('#userActivity' + appId).css('display',' none');
        changeToWorkflow(appId);
    }
    //setup the layout for the existing DeadLine
    $(".existingDeadLine" + appId).css({ "display": "inline-block", "float": "left" });
    $('.input-deadline' + appId).css('display', 'none');
    $(".date-picker" + appId).css('display', 'none');
    $("#editDeadLine" + appId).css({ "display": "inline-block", "padding-left": "5px" });
    $("#saveNewDeadLine" + appId).css('display', 'none');

    $('.date-picker' + appId).datepicker({ startDate: new Date() }).on('changeDate', function (ev) { $('.date-picker').datepicker('hide'); });
    $('div[class^=date-picker' + appId + '_]').datepicker({ startDate: new Date() }).on('changeDate', function (ev) { $('div[class^=date-picker' + appId + '_]').datepicker('hide'); });

    if ($('#disableSoadIndicatorBar').val() == "false") {
        loadUserSOADIndicator(appId);
        LoadPhaseSOADIndicator(appId);
    }
   
    $('.workflow-actions[rel="tooltip"]').tooltip();    
   
    //hide existing Deadline end display edit controls
    $(".editBtn").on('click', function () {
        if ($("#saveNewDeadLine" + appId).css('display') == 'none') {
            $("#saveNewDeadLine" + appId).css('display', 'inline-block');
            $(".existingDeadLine" + appId).hide();
            $("#newDeadLine" + appId).css('display', 'inline-block');
            $("#editDeadLine" + appId).hide();
            $(".date-picker" + appId).show();
            $('.input-deadline' + appId).show();
        }
    });

    hideAllEditablePhaseDeadlines(appId);
});

function hideAllEditablePhaseDeadlines(appId) {
    //setup the layout for the existing phase DeadLine
    $('div[class^=existingPhaseDeadline' + appId + '_]').css({ "display": "inline-block", "float": "left" });
    $('[class^=input-deadline' + appId + '_]').css('display', 'none');
    $('div[class^=date-picker' + appId + '_]').css('display', 'none');
    $('div[id^=editPhaseDeadline' + appId + '_]').css({ "display": "inline-block", "padding-left": "5px" });
    $('div[id^=savePhaseDeadline' + appId + '_]').css('display', 'none');
}

$(".editPhaseBtn").on('click', function () {
    var currentTh = $(this).parent().closest('th');
    var phaseIdentifier = $(this).attr('approval-phase');
    if ($("#savePhaseDeadline" + phaseIdentifier).css('display') == 'none') {
        $(currentTh).removeClass("span3").addClass("span12");
        $("#savePhaseDeadline" + phaseIdentifier).css('display', 'inline-block');
        $("#newPhaseDeadline" + phaseIdentifier).css('display', 'inline-block');
        $(".existingPhaseDeadline" + phaseIdentifier).hide();
        $("#editPhaseDeadline" + phaseIdentifier).hide();
        $(".date-picker" + phaseIdentifier).show();
        $('.input-deadline' + phaseIdentifier).show();
    }
});

function updateWorkflowDetailsDeadLine(approvalID) {
   
    //ignore changes to bypass the Leave Page modal dialog
    $('.input-deadline' + approvalID).dirtyForms('setClean');
    $('.date-picker' + approvalID).find('input').dirtyForms('setClean');
    
    //get the new date/time
    var date, time, meridian;
    date = $(".date-picker" + approvalID + " input").val();
    time = $("#DeadlineTime" + approvalID).val();
    meridian = $("#DeadlineTimeMeridiem" + approvalID).val();
   
    var timeFormat = $("#timeFormat" + approvalID).val();
    var dateFormat = $("#dateFormat" + approvalID).val();
    var dateTime = date + ", " + time;
    if (timeFormat == 1) {
        dateTime += " " + meridian;
    }
    
    //update the deadline via ajax
    $.ajax({
        type: "POST",
        url: "/Approvals/UpdateApprovalsDeadLine",
        data: { approvalID: approvalID, deadlineDate: date, deadlineTime: time, deadlineTimeMeridiem: meridian, dateFormat: dateFormat, timeFormat: timeFormat, __RequestVerificationToken: $("#formStatus input").val() },
        dataType: "json",
        success:
            function () {
                $('.input-deadline' + approvalID).css('display', 'none');
                $(".date-picker" + approvalID).css('display', 'none');
                $("#saveNewDeadLine" + approvalID).css('display', 'none');
                $(".existingDeadLine" + approvalID).text(dateTime);
                $(".existingDeadLine" + approvalID).css('display', 'inline-block');
                $("#editDeadLine" + approvalID).css('display', 'inline-block');
            },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            } else {
                $('#newDeadLine' + approvalID).replaceWith('<label>Error Updating Approval Deadline ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
}

function getWorkflowDetails(versionId) {
    if ($('#approvalWorkflow' + versionId).find('#workflowPartial' + versionId).length == 0) {
        $('#loadingDetailsGrid' + versionId).removeClass('hide');
        $.ajax({
            type: "GET",
            url: "/Approvals/GetApprovalWorkflowDetails?versionId=" + versionId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:
            function (workflowDetails) {
                $('#approvalWorkflow' + versionId).append(workflowDetails.Content);
                $('#loadingDetailsGrid' + versionId).addClass('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                } else {
                    $('#approvalWorkflow').replaceWith('<label>Error Loading Approvals Grid ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}


function getAnnotationsTodos(versionId) {
    if ($('#approvalAnnotationsTodos' + versionId).find('#toDosPartial').length == 0) {
        $('#loadingDetailsGrid' + versionId).removeClass('hide');
        $.ajax({
            type: "GET",
            url: "/Approvals/GetApprovalAnnotationsToDosDetails?versionId=" + versionId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: true,
            success:
                function (annotationsToDosDetails) {
                    $('#approvalAnnotationsTodos' + versionId).append(annotationsToDosDetails.Content);
                    $('#loadingDetailsGrid' + versionId).addClass('hide');
                },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                } else {
                    $('#approvalAnnotationsTodos').replaceWith('<label>Error Loading Approvals Grid ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}

function changeToAnnotation(versionId) {
    getAnnotationsTodos(versionId);
}

function changeToWorkflow(versionId) {
    getWorkflowDetails(versionId);
}

function updatePhaseDeadLine(elem, approvalID, phaseID) {
    var currentTh = $(elem).parent().closest('th');
    var phaseIdentifier = approvalID + "_" + phaseID;
    //ignore changes to bypass the Leave Page modal dialog
    $('.input-deadline' + phaseIdentifier).dirtyForms('setClean');
    $('.date-picker' + phaseIdentifier).find('input').dirtyForms('setClean');

    //get the new date/time
    var date, time, meridian;
    date = $(".date-picker" + phaseIdentifier + " input").val();
    time = $("#PhaseDeadlineTime" + phaseIdentifier).val();
    meridian = $("#PhaseDeadlineTimeMeridiem" + phaseIdentifier).val();

    var timeFormat = $("#timeFormat" + phaseIdentifier).val();
    var dateFormat = $("#dateFormat" + phaseIdentifier).val();
    var dateTime = date + ", " + time;
    if (timeFormat == 1) {
        dateTime += " " + meridian;
    }

    
    //update the deadline via ajax
    $.ajax({
        type: "POST",
        url: "/Approvals/UpdatePhaseDeadline",
        data: { phaseID: phaseID, deadlineDate: date, deadlineTime: time, deadlineTimeMeridiem: meridian, dateFormat: dateFormat, timeFormat: timeFormat, approvalId: approvalID, __RequestVerificationToken: $("#formStatus input").val() },
        dataType: "json",
        success:
            function () {
                $('.input-deadline' + phaseIdentifier).css('display', 'none');
                $(".date-picker" + phaseIdentifier).css('display', 'none');
                $("#savePhaseDeadline" + phaseIdentifier).css('display', 'none');
                $(currentTh).removeClass("span12").addClass("span3");
                $(".existingPhaseDeadline" + phaseIdentifier).text(dateTime);
                $(".existingPhaseDeadline" + phaseIdentifier).css('display', 'inline-block');
                $("#editPhaseDeadline" + phaseIdentifier).css('display', 'inline-block');
            },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            } else {
                $('#newDeadLine' + approvalID).replaceWith('<label>Error Updating Approval Deadline ' + textStatus + ' ' + errorThrown + '</label>');
            }
        }
    });
}

function activateOrDeactivatePhase(phase, activatePhase, currentPhasePosition, selectedPhasePosition, isMaxPhaseCompleted) {
    var selectedApproval = $('#hdnSelectedApproval').val();  
    $.ajax({
        type: "POST",
        url: activateOrDeactivateApprovalPhaseUrl,
        data: { phase: phase, approvalId: selectedApproval, activatePhase: activatePhase, __RequestVerificationToken: $("#formStatus input").val() },
        dataType: "json",
        success:
            function (response) {            
                if (response.isRedirect) {
                    window.location.href = response.redirectUrl;
                } else {
                    var status;
                    if (activatePhase) {
                        if (isMaxPhaseCompleted && currentPhasePosition < selectedPhasePosition) {
                            status = "<small>" + $('#inProressPhase').val() + "</small>";
                            window.location.href = window.location.href;                       
                        } else {
                            status = "<small>" + $('#pendingPhase').val() + "</small>";
                        }                       
                    } else {
                        status = '<small class=\"inactiveStatusColor\">' + $('#inactivePhase').val() + '</small>';
                    }
                    $('#deactivatePhase-' + phase).toggleClass("hide");
                    $('#activatePhase-' + phase).toggleClass("hide");

                    $('#workflowPartiaTable tbody > tr > th > label#phase-' + phase).empty().append(status);
                }
            },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
        }
    });
}

var _url, _phase, _phaseIsActive, _approvalId;
$('ul.dropdown-menu > li.workflow-rerun').on('click', function () {
    _phase = $(this).find('a').attr('data-item-id');
    _phaseIsActive = $(this).find('a').attr('data-item-phaseIsActive') === 'true';
    $('#modelDialogConfirmation_phase').modal({ backdrop: 'static', keyboard: false });
});

function workflowRerun(phaseId, phaseIsActive, approvalId) {
    _phase = phaseId;
    _phaseIsActive = phaseIsActive === 'true';
    _approvalId = approvalId;
    $('#modelDialogConfirmation_phase').modal({ backdrop: 'static', keyboard: false });
};


function executeAction(url) {
    var selectedApproval = $('#hdnSelectedApproval').val();
    if (selectedApproval == 0) selectedApproval = _approvalId;
    $.ajax({
        type: "POST",
        url: url ,
        data: { phase: _phase, approvalId: selectedApproval, phaseIsInactive: !_phaseIsActive, __RequestVerificationToken: $("#formStatus input").val() },
        dataType: "json",
        success:
            function (response) {
                if (response.isRedirect) {
                    window.location.href = response.redirectUrl;
                } else {
                    window.location.href = window.location.href;
                }
            },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
        }
    });
}

function LoadPhaseSOADIndicator(approval) {
    if (approval) {
        $.ajax({
            type: "GET",
            url: getPhaseOADIndicatorData + '?approval=' + approval,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (approvalsSOAD) {
                approvalsSOAD.Content.Data.forEach(function (item, index, array) {
                        $('#soadIndicatorBar_' + item.Approval + '_' + item.Phase).attr('class', item.ProgressStateClass);
                });

                $('.approval-state[rel="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#loadingGrid').replaceWith('<label>Error Loading User SOAD ' + textStatus + ' ' + errorThrown + '</label>');
                }
            }
        });
    }
}