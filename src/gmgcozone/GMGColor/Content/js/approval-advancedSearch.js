﻿$(function () {
    $(document).on("click", "#SaveAdvSearch", function (evt) {
        evt.preventDefault();
        if ($("#formAdvancedSearch").valid()) {
            var strData = $("#formAdvancedSearch").serialize();
            $.ajax({
                type: "POST",
                url: "/Approvals/AdvancedSearchSave",
                data: strData,
                dataType: "json",
                success: function (result) {
                    if (result.Content && result.Content != "") {
                        putAdvSNameErrors(result.Content);
                    } else {
                        loadCustomSearches();
                        $('#modalAdvancedSearch').modal('hide');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                        return;
                    } else {
                        alert("Error on save search " + textStatus + ' ' + errorThrown);
                    }
                }
            });
        }
    });
});

function putAdvSNameErrors(mess) {
    $('#advSNameAlert').html(mess);
    $('#advSNameAlert').show();
    $('#advSNameAlert').parent().addClass("error");
}

function dispalyAdvsModal() {
    $(".advsoverdue input:checkbox").change(function () {
        if ($(this).attr("checked")) {
            $(".advsoverdue input:checkbox").attr("checked", false);
            $(this).attr("checked", true);
        }
    });
    $(".chzn-select").chosen();
    $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
    removeDisabledButtons();

    $('#dtFromPicker').datepicker()
                                .on('changeDate', function (ev) {
                                    var endDate = $('#dtToPicker').datepicker('getDate');
                                    if (ev.date.valueOf() > endDate.valueOf()) {
                                        $('#dtFromPickerAlert').show();
                                        $('#dtFromPicker').parent().parent().addClass("error");
                                        $('#ApplyAdvSearch').attr('disabled', 'disabled');
                                        $('#SaveAdvSearch').attr('disabled', 'disabled');

                                    } else {
                                        removeStartDateError();
                                        removeEndDateError();
                                        removeDisabledButtons();
                                    }
                                    $('#dtFromPicker').datepicker('hide');
                                    activateDeadlineInTimeframes();
                                });

    $('#dtToPicker').datepicker()
                                .on('changeDate', function (ev) {
                                    var startDate = $('#dtFromPicker').datepicker('getDate');
                                    if (ev.date.valueOf() < startDate.valueOf()) {
                                        $('#dtToPickerAlert').show();
                                        $('#dtToPicker').parent().parent().addClass("error");
                                        $('#ApplyAdvSearch').attr('disabled', 'disabled');
                                        $('#SaveAdvSearch').attr('disabled', 'disabled');
                                    } else {
                                        removeStartDateError();
                                        removeEndDateError();
                                        removeDisabledButtons();
                                    }
                                    $('#dtToPicker').datepicker('hide');
                                    activateDeadlineInTimeframes();
                                });
    function removeStartDateError() {
        $('#dtFromPickerAlert').hide();
        $('#dtFromPicker').parent().parent().removeClass("error");
    }

    function removeEndDateError() {
        $('#dtToPickerAlert').hide();
        $('#dtToPicker').parent().parent().removeClass("error");
    }
    function removeDisabledButtons() {
        $('#ApplyAdvSearch').removeAttr('disabled');
        $('#SaveAdvSearch').removeAttr('disabled');
    }

    function activateDeadlineInTimeframes() {
        $('#AdvSWithinATimeframe').attr('checked', true);
    }
}