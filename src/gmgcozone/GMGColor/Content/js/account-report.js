﻿$("#btnMultiDelete").on("click", function () {
    $('#modelDialogDeleteConfirmation_2').modal({ backdrop: 'static', keyboard: false });
})

$(document).ready(function () {
    $('.chzn-select').chosen();
    $('.chzn-select-deselect').chosen({ allow_single_deselect: true });
});

$('.AccountSingleCheckbox').click(function () {
    if ($(this).is(':checked')) {
        $('#btnMultiDelete').removeAttr('disabled');
        $('#btnMultiSuspend').removeAttr('disabled');
        $('#btnMultiRestore').removeAttr('disabled');
        if ($('.AccountSingleCheckbox').length == $('.AccountSingleCheckbox:checked').length) {
            $('.AccountsAllCheckbox').attr('checked', 'checked');
        }
    } else {
        if ($('.AccountSingleCheckbox:checked').length == 0) {
            $('#btnMultiDelete').attr('disabled', 'disabled');
            $('#btnMultiSuspend').attr('disabled', 'disabled');
            $('#btnMultiRestore').attr('disabled', 'disabled');
        }
        $('.AccountsAllCheckbox').removeAttr('checked');
    }
});

$('.AccountsAllCheckbox').click(function () {
    if ($(this).is(':checked')) {
        $('#btnMultiDelete').removeAttr('disabled');
        $('#btnMultiSuspend').removeAttr('disabled');
        $('#btnMultiRestore').removeAttr('disabled');
        $('.AccountSingleCheckbox').attr('checked', 'checked');
    } else {
        $('#btnMultiDelete').attr('disabled', 'disabled');
        $('#btnMultiSuspend').attr('disabled', 'disabled');
        $('#btnMultiRestore').attr('disabled', 'disabled');
        $('.AccountSingleCheckbox').removeAttr('checked');
    }
});

$('.delete-yes').click(function () {
    $('#mainForm').attr('data-ajax', 'false');
});