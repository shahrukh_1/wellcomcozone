(function (A) {
    A.extend(A.fn, {
        pstrength: function (B) {
            var B = A.extend({
                verdects: ["Very weak", "Weak", "Medium", "Strong", "Very strong"],
                colors: ["#ED1B24", "#F1592A", "#F8931F", "#21B24B", "#09B3CD"],
                scores: [10, 15, 30, 40],
                common: ["password", "sex", "god", "123456", "123", "liverpool", "letmein", "qwerty", "monkey"],
                minchar: 8,
                txtminchar: "Your password must be between 8 and 20 characters",
                txttooshort: 'Too short',
                txtunsafe: "Unsafe password word!"
            }, B);
            return this.each(function () {
                var C = A(this).attr("id");
                A(this).after("<div class=\"pstrength-info\" id=\"" + C + "_text\"></div>");
                A(this).after("<div class=\"pstrength-bar\" id=\"" + C + "_bar\" style=\"border: 1px solid white; font-size: 1px; height: 5px; width: 0px;\"></div>");
                A(this).after("<p class=\"help-block pstrength-minchar\" id=\"" + C + "_minchar\">" + B.txtminchar + " </p>");
                A(this).keyup(function () {
                    A.fn.runPassword(A(this).val(), C, B)
                })
            })
        },
        runPassword: function (D, F, C) {
            nPerc = A.fn.checkPassword(D, C);
            var B = "#" + F + "_bar";
            var E = "#" + F + "_text";
            if (nPerc == -200) {
                strColor = "#ED1B24";
                strText = C.txtunsafe;
                A(B).css({
                    width: "0%"
                })
            } else {
                if (nPerc < 0 && nPerc > -199) {
                    strColor = "#ccc";
                    strText = C.txttooshort;
                    A(B).css({
                        width: "1.5%"
                    })
                } else {
                    if (nPerc <= C.scores[0]) {
                        strColor = C.colors[0];
                        strText = C.verdects[0];
                        A(B).css({
                            width: "5%"
                        })
                    } else {
                        if (nPerc > C.scores[0] && nPerc <= C.scores[1]) {
                            strColor = C.colors[1];
                            strText = C.verdects[1];
                            A(B).css({
                                width: "7%"
                            })
                        } else {
                            if (nPerc > C.scores[1] && nPerc <= C.scores[2]) {
                                strColor = C.colors[2];
                                strText = C.verdects[2];
                                A(B).css({
                                    width: "10%"
                                })
                            } else {
                                if (nPerc > C.scores[2] && nPerc <= C.scores[3]) {
                                    strColor = C.colors[3];
                                    strText = C.verdects[3];
                                    A(B).css({
                                        width: "12%"
                                    })
                                } else {
                                    strColor = C.colors[4];
                                    strText = C.verdects[4];
                                    A(B).css({
                                        width: "15%"
                                    })
                                }
                            }
                        }
                    }
                }
            }
            A(B).css({
                backgroundColor: strColor
            });
            A(E).html("<span style='color: " + strColor + ";'>" + strText + "</span>")
        },
        checkPassword: function (C, B) {
            var F = 0;
            var E = B.verdects[0];
            if (C.length < B.minchar) {
                F = (F - 100)
            } else {
                if (C.length >= B.minchar && C.length <= (B.minchar + 2)) {
                    F = (F + 6)
                } else {
                    if (C.length >= (B.minchar + 3) && C.length <= (B.minchar + 4)) {
                        F = (F + 12)
                    } else {
                        if (C.length >= (B.minchar + 5)) {
                            F = (F + 18)
                        }
                    }
                }
            }
            if (C.match(/[a-z]/)) {
                F = (F + 1)
            }
            if (C.match(/[A-Z]/)) {
                F = (F + 5)
            }
            if (C.match(/\d+/)) {
                F = (F + 5)
            }
            if (C.match(/(.*[0-9].*[0-9].*[0-9])/)) {
                F = (F + 7)
            }
            if (C.match(/.[!,@,#,$,%,^,&,*,?,_,~]/)) {
                F = (F + 5)
            }
            if (C.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/)) {
                F = (F + 7)
            }
            if (C.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                F = (F + 2)
            }
            if (C.match(/([a-zA-Z])/) && C.match(/([0-9])/)) {
                F = (F + 3)
            }
            if (C.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)) {
                F = (F + 3)
            }
            for (var D = 0; D < B.common.length; D++) {
                if (C.toLowerCase() == B.common[D]) {
                    F = -200
                }
            }
            return F
        }
    })
})(jQuery)