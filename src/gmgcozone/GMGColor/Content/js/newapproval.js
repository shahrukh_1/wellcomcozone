﻿$(document).ready(function () {
    $('.date-picker').datepicker({ startDate: new Date() }).on('changeDate', function (ev) { $('.date-picker').datepicker('hide'); $('form').dirtyForms('setDirty'); });
    showDecisionMaker();
    window.setTimeout('chooseDecisionMakers()', 100);
    $('.collaboratoraccess-events button[value=submit]').click(function () {
        window.setTimeout('chooseDecisionMakers()', 100);
    });
    $('#fileUpload_1').on('fileuploaddone', function (e, data) {
        setTimeout(function () {
            if ($("#isNewVersionRequest").val() == "False") {
                $("#tblUpload1 ").find("tr[file-name='" + data.files[0].name + "'].displaysuccess ").addClass("noWidth");
            }
            var fileGuid = $("#tblUpload1 ").find("tr[file-name='" + data.files[0].name + "'].displaysuccess ").attr("file-guid");
            $("#ApprovalType_" + fileGuid).val($("#ApprovalType_" + fileGuid).attr("data-selected"));
            BindSelect2Tagwords(fileGuid);
            fileUploaded();

    }, 500); //---> this line causes problems when window focus is lost. Need to test if the removal of this line will not cause other issues
        //fileUploaded();
    });
    $('#fileUpload_1').on('fileuploaddestroy', function (e, data) {
        // remove the sibling hidden row of the deleted row: each upload item has two rows (one for error and one for success) and only one is visible
        if ($(data.context).hasClass('displayerror')) {
            $(data.context).next().remove();
        } else if ($(data.context).hasClass('displaysuccess')) {
            var theRowId = $(data.context).attr("file-guid");
            $('#tblUpload1 tr[file-guid="' + theRowId + '"]').remove();

            $(data.context).prev().remove();
        }

        setTimeout(function () { fileDeleted(); }, 500);
    });

    $("#btnAddApproval").mouseenter(function () {
        ValidateFileTitles();
    });
    $("#btnAddApproval").focus(function () {
        ValidateFileTitles();
    });

    $("#btnAddApproval").click(function () {
        if ($("#AppprovalHasAdHocWorkflow").length) {
            $("#HasAdHocWorkflow").val(true);
            $("#ApprovalAdHocWorkflowName").val($("#AdHocWorkflowName").val());
        }
    });

    //if is "Add new version" page and job is asigned to an workflow,
    //the wrokflow cannot be changed and should be disabled
    var jobId = parseInt($('#Job').val(), 10);
    if (jobId > 0) {
        $('#selectedApprovalWorkflow').attr('readonly', 'true').attr('disabled', 'disabled');
        showHidePageControls(parseInt($('#ApprovalWorkflowId').val(), 10), true, jobId);
    } else {
        //the template field must be reset to "Not Required" value, specialy on page refresh,
        //because Firefox doesn't do this by default
        if (!$('span.errAccess').is(":visible")) {
            $('#selectedApprovalWorkflow').val(0);
        }
        showHidePageControls($('#ApprovalWorkflowId').val(), false, 0);
    }

    if (jobId > 0) {
        $('#selectedChecklist').attr('readonly', 'true').attr('disabled', 'disabled');
        showHidePageControls(parseInt($('#ChecklistId').val(), 10), true, jobId);
    } else {
        //the template field must be reset to "Not Required" value, specialy on page refresh,
        //because Firefox doesn't do this by default
        if (!$('span.errAccess').is(":visible")) {
            $('#selectedChecklist').val(0);
        }
        showHidePageControls($('#ChecklistId').val(), false, 0);
    }

});

function removeSelectedExternalUsers() {
    if ($('#chkPrivateAnnotations').is(':checked')) {
        $('.collaboratorshare-blocks').css('display', 'none');
    } else {
        $('.collaboratorshare-blocks').css('display', 'block');
    }
}

function showDecisionMaker() {
    if ($('#chkOnlyOneDecision').is(':checked')) {
        $('#chkLockProof').attr('checked', 'checked');
        $('#dvPrimaryDecisionMaker').css('display', 'inline-flex');
    } else {
        $('#dvPrimaryDecisionMaker').hide();
        $("#PrimaryDecisionMakerHiddenField").val("no required");
    }
}

function toggleButton(id) {
    if (id == 0) {
        $('#btnChooseFile').addClass('active');
        $('#btnCaptureFromWeb').removeClass('active');
        $('#dvUploader').show();
        $('#dvCapture').hide();
        $('#chkUpload').prop('checked', true);
        if ($('#tblUpload1 tr').length == 0) {
            $('#btnAddApproval').attr('disabled', 'disabled');
        }
    } else if (id == 1) {
        $('#btnCaptureFromWeb').addClass('active');
        $('#btnChooseFile').removeClass('active');
        $('#dvUploader').hide();
        $('#dvCapture').show();
        $('#chkUpload').prop('checked', false);
        if ($('#btnAddApproval').attr('disabled')) {
            $('#btnAddApproval').removeAttr('disabled');
        }
    }
}

//event to check new files status for current selected approval folder
$('#submit-destination').on('click', function (e, data) {
    var folder = $('#aFolderTrigger').attr('widget-id');
    var phase = ($('#aApprovalTrigger').attr('widget-phase') != undefined && $('#aApprovalTrigger').attr('widget-phase') != "") ? $('#aApprovalTrigger').attr('widget-phase') : 0;

    if ($('#ApplyBatchUploadSettings').val().toLowerCase() == "true") {
        $('#approvalFolderId').val(folder);

        if (folder > 0) {
            var files = new Array();
            $(".table-upload tr.displaysuccess").each(function () {
                var fileName = $(this).attr('file-name');
                if (fileName != '') {
                    files[files.length] = fileName;
                }
            });

            if (files.length > 0) {
                $.ajax({
                    type: "GET",
                    traditional: true,
                    url: urlGetApprovalFileStatuses,
                    data: { 'fileNames': files, 'folderId': folder },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (fileStatuses) {
                        for (var i = 0; i < fileStatuses.length; i++) {
                            if (fileStatuses[i].FileStatus == 'DuplicatedFile' || fileStatuses[i].FileStatus == 'IgnoredFile') {
                                $("tr.displaysuccess[file-name='" + fileStatuses[i].FileName + "']").hide();
                                var errorRow = $("tr.displayerror[file-name='" + fileStatuses[i].FileName + "']");
                                $(errorRow).show();
                                $(errorRow).find(".error span").text(fileStatuses[i].FileStatusLabel);
                            } else {
                                $("tr.displayerror[file-name='" + fileStatuses[i].FileName + "']").hide();
                                var successRow = $("tr.displaysuccess[file-name='" + fileStatuses[i].FileName + "']");
                                $(successRow).show();
                                $(successRow).find("input[name^='ApprovalFileStatus_']").val(fileStatuses[i].FileStatus);
                                $(successRow).find(".statusLabel span").text(fileStatuses[i].FileStatusLabel);
                            }
                        }
                        HandleApprovalFileStatus();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        }
    }
 
    //when folder is changed set access list to the one of the dest folder
    if (folder > 0 && phase < 1) {
        //mark form as dirty
        $('form').dirtyForms('setDirty');
        $.ajax({
            type: "GET",
            url: urlGetFolderCollaborators + '?id=' + folder,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                // Set the collaborators to the current element
                var collaborators = response.FolderCollaborators;

                if ($('#aApprovalTrigger').attr("widget-users") != undefined) {
                    $('#aApprovalTrigger').attr("widget-users", '[' + collaborators['CollaboratorsWithRole'] + ']');
                }

                if ($('#aApprovalTrigger').attr("widget-groups") != undefined) {
                    $('#aApprovalTrigger').attr("widget-groups", '[' + collaborators['Groups'] + ']');
                }

                $('#aApprovalTrigger').click();
                $(".collaboratoraccess-events button[value='submit']").click();
            }
        });
    }
});


$('#KeepOriginalFileName').click(function () {
    changeJobName();
});

//Used mouseover and not click because the dropdown was not refreshed properly and the user that should be removed was still shown 
$("#PrimaryDecisionMaker").mouseover(function() {
    $("#aApprovalTrigger").mouseover();
    populateDecisionMakersDropDown();
});

function fileUploaded() {
    HandleApprovalFileStatus();
    HideShowRenderSettings();
}

function fileDeleted() {
    HandleApprovalFileStatus();
    HideShowRenderSettings();
}

function changeJobName() {
    if ($('#KeepOriginalFileName').is(':checked')) {
        $('.initialName').text($('.approvalTitle').val());
    } else {
        $('.initialName').text("");
    }
}

function HandleApprovalFileStatus() {
    var anyJob = false;
    var filesCount = $(".table-upload tr.displaysuccess").length;
    var newJobCount = $("input[name^='ApprovalFileStatus_'][value='NewJob']").length;

    if ($('#ApplyBatchUploadSettings').val().toLowerCase() == "true" && $('#approvalFolderId').val() > 0) {
        if (newJobCount != filesCount) {
            $('#selectedApprovalWorkflow option:first-child').attr("selected", "selected");
            $('#selectedApprovalWorkflow').attr('readonly', 'true').attr('disabled', 'disabled');
            showHidePageControls($('#ApprovalWorkflowId').val(), false, 0);
        } else {
            $('#selectedApprovalWorkflow').removeAttr('readonly').removeAttr('disabled');
        }
    }

   $("input[name^='ApprovalFileStatus_']").each(function (i, v) {
        if ($(this).val() == 'NewJob') {
            anyJob = true;
        }
    });

    if (!anyJob && filesCount > 0 && $('#ModelVersion').val() == 0) {
        $('#updateFileName').css('display', 'none');
    } else {
        $('#updateFileName').css('display', 'block');
    }
 
    var anyFile = filesCount > 0;
    if (anyFile && $('.myclass.3').length > 1 && $('.myclass.3').length == $('.myclass').length && ($('#hdnFileName_1')[0].value.match(/.pdf/g) || []).length == $('.myclass.3').length) {
        $('#merge-btn').removeAttr('disabled');
        $('.merging-pdf').show();
    } else {
        $('.merging-pdf').hide();
    }
    if (!anyFile) {
        //hide submit button in case their is no file left
        $('#btnAddApproval').attr('disabled', 'disabled');
        $('.merging-pdf').hide();
    }
    if ($('#ModelVersion').val() != 0 && filesCount > 1) {
        $('#btnAddApproval').attr('disabled', 'disabled');
        $('#error-adding-multipleversions').removeClass('hide');
    }
    if ($('#ModelVersion').val() != 0 && filesCount == 1) {
        $('#btnAddApproval').removeAttr('disabled');
        $('#error-adding-multipleversions').addClass('hide');
    }
    changeJobName();
    
    if ($('#UsePreviousSettings').val().toLowerCase() == "true") {

        var overrideSettings = anyFile;
        $("input[name^='ApprovalFileStatus_']").each(function(i, v) {
            if ($(this).val() != 'NewVersion') {
                overrideSettings = false;
            }
        });
        if (overrideSettings) {
            $('#overrideSettings :input').attr('disabled', 'disabled');
            $('#overrideSettings a').addClass('disableClick');
            $('#overrideSettings a').on('click', DisableClick()); // for IE
            $('#settingsOverrideMsg').show();
        } else {
            $('#overrideSettings :input').removeAttr('disabled');
            $('#overrideSettings a').removeClass('disableClick');
            $('#overrideSettings a').off('click', DisableClick()); // for IE
            $('#settingsOverrideMsg').hide();
        }
    }
}

function DisableClick(e) {
    return false;
}

function populateDecisionMakersDropDown() {

    var currentSelectedPDM = $('#PrimaryDecisionMaker').find(':selected').val();

    var decisionroles = approvalCollaboratorRoleId;
    var selectedusers = [];
    var userswithrole = ($('#aApprovalTrigger').attr('widget-users') != undefined) ? $('#aApprovalTrigger').attr('widget-users').replace(/\"/g, '') : '[]';
    if (userswithrole) {
        userswithrole = userswithrole.substring(1, (userswithrole.length - 1));
        userswithrole = ((userswithrole != '') ? userswithrole.split(',') : []);
        $(userswithrole).each(function() {
            if (decisionroles.indexOf(parseInt(this.split('|')[1])) > -1)
                selectedusers.push(this.split('|')[0]);
        });
    }
    $('select[name=PrimaryDecisionMaker] option').remove();
    $('#dummyPrimaryDecisionMaker option').each(function() {
        if (selectedusers.indexOf($(this).val()) > -1) {
            var option = $('<option>').val($(this).val()).text($(this).text());
            $('select[name=PrimaryDecisionMaker]').append(option);
        }
    });
    $('select[name=PrimaryDecisionMaker] option[value=' + $('#aApprovalTrigger').attr('widget-creator') + ']').attr('selected', 'selected');

    var externaluserblocks = $('.collaboratorshare-base .collaboratorshare-blocks .block.external.active');

    for (var i = 0; i < externaluserblocks.length; i++) {
        var blockid = $(externaluserblocks[i]).attr('id');
        var role = $(externaluserblocks[i]).attr('block-role');
        if (decisionroles.indexOf(parseInt(role)) > -1) {
            var name = $('#' + blockid + ' .block-name').html().trim();
            var email = $('#' + blockid + ' .block-email').html().trim();
            var option = $('<option>').val(email).text(name);
            $('select[name=PrimaryDecisionMaker]').append(option);
        }
    }

    var old = $('#PrimaryDecisionMaker').find(" option[value=" + "'" + currentSelectedPDM + "']");
    if (old.length > 0)
        old.attr('selected', 'selected');
}

function chooseDecisionMakers() {
    populateDecisionMakersDropDown();

    var selectedexternal = [];
    var externaluserswithrole = ($('#aApprovalTrigger').attr('widget-external') != undefined) ? $('#aApprovalTrigger').attr('widget-external').replace(/\"/g, "") : '[]';
    if (externaluserswithrole) {
        externaluserswithrole = externaluserswithrole.substring(1, (externaluserswithrole.length - 1));
        externaluserswithrole = ((externaluserswithrole != '') ? externaluserswithrole.split(',') : []);
        $(externaluserswithrole).each(function() { selectedexternal.push(this.split('|')[0]); });
    }

    var datablock = $('.collaboratorshare-control .collaboratorshare-blocks .block[block-id=0]');
    var dataemail = $('.collaboratorshare-control .collaboratorshare-blocks .role-selector input[type=text].email');
    var datarole = datablock.attr('block-role');
    var userblocks = $('.collaboratorshare-control .collaboratorshare-blocks .block-grid .block');

    // save the existing roles and clear all the existing users
    var roles = new Array();
    var activeExternalUsers = $(".block-email").parents('li.block.active');
    for (i = 0; i < activeExternalUsers.length; i++) {

        var currentExternalUser = $(activeExternalUsers[i]);

        var userId = currentExternalUser.attr("block-id");
        var roleId = currentExternalUser.attr("block-role");

        roles[roles.length] = userId + "|" + roleId;

        if (parseInt(userId, 10) > 0) {
            currentExternalUser.removeClass('active').addClass('remove');
        }
    }

    for (var i = 0; i < userblocks.length; i++) {
        var blockid = $(userblocks[i]).attr('id');
        if (parseInt($('#' + blockid).attr('block-id')) >= 0) {
            if ((selectedexternal.length > 0) && (selectedexternal.indexOf($(userblocks[i]).attr('block-id')) > -1)) {
                var email = $('#' + blockid + ' .block-email').html().trim();
                var role = $('.collaboratoraccess-control .collaboratoraccess-blocks .block.external[block-id=' + $('#' + blockid).attr('block-id') + ']').attr('block-role');
                dataemail.val(email);
                datablock.attr('block-role', role);
                $('.collaboratorshare-control .collaboratorshare-blocks button[type=button][value=insert]').click();
            } else if (parseInt($('#' + blockid).attr('block-id')) > 0) {
                $('#' + blockid + ' .role-selector .actions.remove [block-role=-3]').click();
            }
        }
    }

    dataemail.val('');
    datablock.attr('block-role', datarole);

    if ($('#chkOnlyOneDecision').is(':checked')) {
        $("#PrimaryDecisionMakerHiddenField").val($("#PrimaryDecisionMaker").val());
    }

    if ($('#PrimaryDecisionMaker option').length == 0) {
        if (!$('#chkOnlyOneDecision').is(':disabled')) {
            $('#chkOnlyOneDecision').attr('disabled', 'disabled');
        }
        if ($('#dvPrimaryDecisionMaker').is(':visible')) {
            $('#dvPrimaryDecisionMaker').hide();
        }
        if ($('#chkOnlyOneDecision').is(':checked')) {
            $('#chkOnlyOneDecision').removeAttr('checked');
        }
        $('#PrimaryDecisionMakerHiddenField').val("no required");
    } else {
        if ($('#chkOnlyOneDecision').is(':disabled')) {
            $('#chkOnlyOneDecision').removeAttr('disabled');
        }
    }
}

/*Keep session alive if approval is uploading*/
$(function() {
    setInterval('pingSession()', 15000); // keep session alive by pinging every 15 sec
});

function pingSession() {
    if ($('#tblUpload1').children('tbody').children().length != 0) {
        $.ajax({
            type: "POST",
            url: urlPingSession,
            data: { __RequestVerificationToken: $("#formAddNewApproval input").val() },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function() {
            }
        });
    }
}

$('#dueDate').on('change', function() {
    if ($(this).is(':checked')) {
        $('#approvalDeadlineDate').css('display', 'block');
    } else {
        $('#approvalDeadlineDate').css('display', 'none');
    } 
});
    /*--------------------------------------------*/

$('#selectedApprovalWorkflow').on('change', function () {
    var url = "";
    var workflowId = parseInt($(this).val());
    changeAccessPartial();

    showHidePageControls(workflowId, false, parseInt($('#Job').val(), 10));
    $('fieldset:last').find('div[class="control-group"]').not(':first').not(':last').css('display', 'block');

    if (workflowId > 0) {
        url = "/Approvals/GetApprovalWorkflowCollaborators?workflowId=" + workflowId;
    } else {
        url = "/Approvals/GetNewApprovalCollaborators?versionId=" + parseInt($('#Job').val());
    }

    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (workflowId > 0) {
                if ($('#aApprovalTrigger').attr("widget-workflow-phases") != undefined) {
                    $('#aApprovalTrigger').attr("widget-workflow-phases", response.Collaborators);
                }

                if ($('#aApprovalTrigger').attr("widget-phases") != undefined) {
                    $('#aApprovalTrigger').attr("widget-phases", response.Phases);
                }
            } else {
                if ($('#aApprovalTrigger').attr("widget-users") != undefined) {
                    $('#aApprovalTrigger').attr("widget-users", '[' + response.Collaborators['CollaboratorsWithRole'] + ']');
                }

                if ($('#aApprovalTrigger').attr("widget-groups") != undefined) {
                    $('#aApprovalTrigger').attr("widget-groups", '[' + response.Collaborators['Groups'] + ']');
                }

                if ($('#aApprovalTrigger').attr("widget-external") != undefined) {
                    $('#aApprovalTrigger').attr("widget-external", '[]');
                }

                $('#aApprovalTrigger').attr("widget-workflow-phases", '');
                $('#aApprovalTrigger').attr("widget-phases", '');
                $('#aApprovalTrigger').attr("widget-decision", 0);

            }
            $('#aApprovalTrigger').attr("widget-phase", "");
            $('#aApprovalTrigger').click();
            $(".collaboratoraccess-events button[value='submit']").click();
            $('span.errAccess').hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error Loading Permissions pop-up ' + textStatus, errorThrown);
        }
    });
});

function HideShowRenderSettings() {
    var anyDocumentFile = false;
    $(".table-upload tr.displaysuccess").each(function () {
        var fileName = $(this).attr('file-name');
        if (fileName != '') {
            var arr = fileName.split(".");
            if (arr.length > 0) {
                var extension = arr[arr.length - 1];
                switch (extension.toLowerCase()) {
                    case "tiff":
                    case "tif":
                    case "jpg":
                    case "jpeg":
                    case "png":
                    case "pdf":
                    case "eps":
                        {
                            anyDocumentFile = true;
                            return;
                        }
                }
            }
        }
    });

    if (anyDocumentFile) {
        $('#render-settings').find('input,select').removeAttr('disabled');
    } else {
        $('#render-settings').find('input,select').attr('disabled', 'disabled');
    }
}

function showHidePageControls(workflowId, isNewVersion, jobId) {

    if (workflowId > 0) {
        setTimeout(function () {
            $('#chkLockProof').attr("checked", false).parents('.control-group').css('display', 'none');
            $('#chkOnlyOneDecision').attr("checked", false).parents('.control-group').css('display', 'none');

            $('#LockProofWhenFirstDecisionsMade').closest("div.control-group").css('display', 'none');

            //if is "Add new version" page the "Allow users to be added to the phase once the file is uploaded" option should be hidden
            if (!isNewVersion && jobId == 0) {
                $('#allowOtherUsersToBeAdded').parents('.control-group').css('display', 'block');
            } else {
                $('#allowOtherUsersToBeAdded').parents('.control-group').css('display', 'none');
            }
        }, 100);        
    } else {
        $('#chkLockProof').parents('.control-group').css('display', 'block');
        $('#chkOnlyOneDecision').parents('.control-group').css('display', 'block');
        $('#allowOtherUsersToBeAdded').attr("checked", false).parents('.control-group').css('display', 'none');
        $('#LockProofWhenFirstDecisionsMade').closest("div.control-group").css('display', 'block');
    }
}

var newTagwordsIds = [];
var approvalSelectedTagwords = [];

function BindSelect2Tagwords(fileGuid) {
    $("#tags_" + fileGuid).select2({
        placeholder: "Add Tag word",
        tags: true,
        templateSelection: function (data, container) {
            $(container).css("color", "")
            if (newTagwordsIds.includes(data.id)) {
                $(container).css("background-color", "#ffd892");
            }
            return data.text;
        },
        createTag: function (tag) {
            return {
                id: tag.term,
                text: tag.term,
                isNew: true
            };
        },
        ajax: {
            delay: 500,
            type: "POST",
            url: '/Approvals/GetApprovalsTagwordsOnSearch',
            dataType: 'json',
            data: function (params) {
                $("#select2-tags_" + fileGuid + "-results").html('')
                if (params.term == undefined) {
                    params.term = "";
                }
                var selectedIds = "";
                return JSON.parse("{ \"term\" : \"" + params.term + "\" , \"selectid\" : \"" + selectedIds + "\", \"_type\" : \"query\", \"__RequestVerificationToken\" : \"" + $("#formAddNewApproval input").val() + "\" }");
            },
            processResults: function (data) {
                var arr = []
                $.each(data.Content, function (index, value) {
                    arr.push({
                        id: value.id + "_" + fileGuid,
                        text: value.text
                    })
                })
                return {
                    results: arr
                };
            }
        }
    }).on("select2:select", function (e) {
        if (e.params.data.isNew) {
            var selectedtag = $(this);
            $.ajax({
                url: '/Approvals/InsertApprovalsTagwords',
                type: 'POST',
                data: JSON.parse("{ \"tagword\" : \"" + e.params.data.text + "\", \"__RequestVerificationToken\" : \"" + $("#formAddNewApproval input").val() + "\"}"),
                success: function (result) {
                    if (result != null) {
                        selectedtag.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + result + '_' + fileGuid + '">' + e.params.data.text + '</option>');
                        approvalSelectedTagwords.push(result + '_' + fileGuid);
                        $("#SelectedTagwords").val(approvalSelectedTagwords);
                        newTagwordsIds.push(result + '_' + fileGuid);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
        }
        else {

            $.each($(this).select2('val'), function (i, el) {
                if ($.inArray(el, approvalSelectedTagwords) === -1) approvalSelectedTagwords.push(el);
            });
            $("#SelectedTagwords").val(approvalSelectedTagwords);
        }
    }).on("select2:unselect", function (e) {
        approvalSelectedTagwords.splice(approvalSelectedTagwords.indexOf(e.params.data.id), 1);
        $("#SelectedTagwords").val(approvalSelectedTagwords);
    });
}

function select2CopyClasses(data, container) {
    if (data.element) {
        $(container).addClass($(data.element).attr("class"));
    }
    return data.text;
}