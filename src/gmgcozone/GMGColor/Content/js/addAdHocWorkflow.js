﻿//collaborator Role enum
var collaboratorRole = {
    ReadOnly: 1,
    Reviewer: 2,
    ApprovalAndReviewer: 3
};
var adHocApprovalID;
var defaultAccessElement;
var defaultWidget;

var adHocPhasesList = [];

//get the default access selection (Not Requred)
$(document).ready(function () {
    if ($("#isNewVersionRequest").val() == "True") {
        $("#addAdHocWorkflow").remove();
    }
    defaultAccessElement = document.getElementById('templateWorkflowAccess');
    defaultWidget = document.getElementById("widgetPermissions");    
});

//Add ad-hoc workflow partial to New Approval page
$("#addAdHocWorkflow").click(function () {
    $('#allowOtherUsersToBeAdded').parents('.control-group').css('display', 'block');

    $("#addAdHocWorkflow").attr("disabled", true);
    //remove the default Access to prevent recursive call

    if (defaultAccessElement !== null) {
        $(defaultAccessElement).detach();
        $(defaultWidget).detach();
    }

    $("#LoadingImage").show();
  
    $.ajax({
        url: $("#adHocWorkflowUrl").val(),
        type: "GET",
        dataType: 'html',
        success: function (data) {
            $("#LoadingImage").hide();
            //Attach the partial to the New Approval page
            $("#adHocWorkflowContainer").html($.parseJSON(data).Content);

            $('fieldset:last').find('div[class="control-group"]').not(':first').not(':last').css('display', 'none');

            $('.rerun-info-message[rel="tooltip"]').tooltip();

            //Remove phase details legend
            $("#adhocPhaseContainer fieldset:first-child legend").detach();

            $('#adHocPhases').on('click', 'li > label', function (e) {
                var $anchor = $(this).closest('li');
                var phaseId = $anchor.attr('phase');

                if (phaseId > 0) {
                    var phaseDetails = {};
                    $.each(adHocPhasesList, function (index, phase) {
                        if (phase.ID == phaseId) {
                            phaseDetails = phase;
                            return true;
                        }
                    });
                    getPhaseDetailsById(phaseDetails);
                }
                movePhasePanelToTheBottomOfPhasesList($anchor);
                configurePhaseDecisionSection($('#SelectedDecisionType'));
            });
        }
    }); 
});

function getPhaseDetailsById(selectedPhaseDetails) {
    $("#ShowAnnotationsToUsersOfOtherPhases").attr("checked", selectedPhaseDetails.ShowAnnotationsToUsersOfOtherPhases);
    $("#ShowAnnotationsOfOtherPhasesToExternalUsers").attr("checked", selectedPhaseDetails.ShowAnnotationsOfOtherPhasesToExternalUsers);
    $("#PhaseName").val(selectedPhaseDetails.PhaseName);

    $('#SelectedDeadlineType').val(selectedPhaseDetails.SelectedDeadlineType);
    if (selectedPhaseDetails.SelectedDeadlineType == 0) {
        //set deadline date
        $('#DeadlineDate').val(selectedPhaseDetails.DeadlineDate);

        //set deadline time
        $('#DeadlineTime option:selected').attr('selected', false);
        $('#DeadlineTime option[value="' + selectedPhaseDetails.DeadlineTime + '"]').attr('selected', true);

        //set deadline meridium
        $('#DeadlineTimeMeridiem option:selected').attr('selected', false);
        $('#DeadlineTimeMeridiem option[value="' + selectedPhaseDetails.DeadlineTimeMeridiem + '"]').attr('selected', true);

        $("#selectedUnit").val(1);

        //set deadline time
        $('#SelectedUnit option:first-child').attr('selected', true);

        //set deadline meridium
        $('#SelectedTrigger option:first-child').attr('selected', true);
    } else {
        $("#selectedUnit").val(selectedPhaseDetails.selectedUnit);

        //set deadline time
        $('#SelectedUnit option:selected').attr('selected', false);
        $('#SelectedUnit option[value="' + selectedPhaseDetails.SelectedUnit + '"]').attr('selected', true);

        //set deadline meridium
        $('#SelectedTrigger option:selected').attr('selected', false);
        $('#SelectedTrigger option[value="' + selectedPhaseDetails.SelectedTrigger + '"]').attr('selected', true);

        //set deadline time
        $('#DeadlineTime option:first-child').attr('selected', true);

        //set deadline meridium
        $('#DeadlineTimeMeridiem option:first-child').attr('selected', true);
    }

    //set decision type
    $('#SelectedDecisionType option:selected').attr('selected', false);
    $('#SelectedDecisionType option[value="' + selectedPhaseDetails.SelectedDecisionType + '"]').attr('selected', true);

    //set approval trigger
    $('#SelectedApprovalPhaseTrigger option:selected').attr('selected', false);
    $('#SelectedApprovalPhaseTrigger option[value="' + selectedPhaseDetails.SelectedApprovalPhaseTrigger + '"]').attr('selected', true);

    $("#ApprovalShouldBeViewedByAll").attr("checked", selectedPhaseDetails.ApprovalShouldBeViewedByAll);

    //display selected users and groups
    setPhaseCollaborators(selectedPhaseDetails.SelectedUsersAndGroups);

    if (selectedPhaseDetails.SelectedDecisionType == 1) {
        $('#primaryDecisionMaker option').not(':first-child').remove();
        var selectedPDM, isExternal;
        $.each(selectedPhaseDetails.DecisionMakers,
            function(index, item) {
                if (item.IsSelected) {
                    selectedPDM = item.ID;
                    isExternal = item.IsExternal;
                }
            });
        syncApproversAndReviewersLists(selectedPDM, isExternal);

        $('#primaryDecisionMaker option[value="' + selectedPDM + '"][is-external="' + isExternal + '"]')
            .attr('selected', true);
    }

    //populate trigger with phase's users and groups 
    $('#aApprovalTrigger').attr("widget-users", selectedPhaseDetails.widgetUsers);
    $('#aApprovalTrigger').attr("widget-groups", selectedPhaseDetails.widgetGroups);
    $('#aApprovalTrigger').attr("widget-external", selectedPhaseDetails.widgetExternal);
}

function setPhaseCollaborators(phaseCollaborators) {
    var domid = $('.collaboratoraccess-blocks').attr('id');

    resetUserAndGroups(domid);

    //loop through phase's selcted users or groups
    $.each(phaseCollaborators, function (index, item) {
        if (item.IsChecked) {
            //add selected groups
            if (item.IsGroup) {
                var id = $('#' + domid + ' .block.group[block-id=' + item.ID + ']').attr('id');
                $('#' + id + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                $('#' + id).addClass('active');
                return true;
            }

            //add selected external collaborators
            if (item.IsExternal) {
                var externalblockid = $('#' + domid + ' .block.external[block-id=' + item.ID + ']').attr('id');
                if (item.ApprovalRole == null) {
                    item.ApprovalRole = $('#' + externalblockid + ' .role-selector').attr('pre-role');
                }
          
                if (externalblockid != undefined) {
                    $('#' + externalblockid + ' .block-status input[type=checkbox]').attr('checked', 'checked');

                    var extDomid = $('.collaboratorshare-blocks').attr('id');
                    $('#' + extDomid + ' .block.external[block-id=' + item.ID + ']').removeClass('remove').addClass('active');

                    assignRole(domid, item.ApprovalRole);
                }
                return true;
            }

            //add selected internal collaborators
            var blockid = $('#' + domid + ' .block.user[block-id=' + item.ID + ']').attr('id');
            if (blockid != undefined) {
                if (item.ApprovalRole == null) {
                    item.ApprovalRole = $('#' + blockid + ' .role-selector').attr('pre-role');
                }
                $('#' + blockid + ' .block-status input[type=checkbox]').attr('checked', 'checked');
                $('#' + blockid).addClass('active');

                assignRole(blockid, item.ApprovalRole);
            }
            return true;
        }
    });
}

//reset all users and groups
function resetUserAndGroups(domid) {
    var extDomid = $('.collaboratorshare-blocks').attr('id');
    
    $('#aApprovalTrigger').attr("widget-users", "[]");
    $('#aApprovalTrigger').attr("widget-groups", "[]");
    $('#aApprovalTrigger').attr("widget-external", "[]");

    $('#' + domid + ' .block.group').removeClass("active");
    $('#' + extDomid + ' .block.external').addClass("remove").removeClass("active");
    $('#' + domid + ' .block.user').removeClass("active");
}

//update users roles
function assignRole(blockid, role) {
    $('#' + blockid + ' .role-selector').attr('pre-role', $('#' + blockid).attr('block-role'));

    var rolename = $('#' + blockid + ' .role-selector .role [block-role=' + role + ']').html().trim();
    $('#' + blockid).attr('block-role', role);
    $('#' + blockid + ' .block-role').html(rolename);
    $('#' + blockid + ' input[type=hidden][id$=ApprovalRole]').val(role);
}

//add only the users with approver and reviewer role, otherwise remove them from  Decision Maker dropdown (Phase page)
function syncDecisionMakerList(domid, user, role, isExternal, isSelected) {
    if (role == 3) {
        $('#approverAndReviewerErr').hide();
        $('.errAccess').hide(); // hide the error message if at least one user is selected
        if ($('#primaryDecisionMaker option[value="' + user + '"][is-external="' + isExternal + '"]').length == 0) {
            var index = $('#primaryDecisionMaker option').length - 1;
            var userName = isExternal ? $('#' + domid + ' .block.external[block-id="' + user + '"] .block-name').html()
                                                    : $('#' + domid + ' .block.user[block-id="' + user + '"] span.lblUsername').html()
            $('#primaryDecisionMaker').append($("<option></option>")
                                .attr("value", user)
                                .attr("is-external", isExternal)
                                .attr("index", index)
                                .text(userName));


            $('#decisionMaker').append('<input type="hidden" name="DecisionMakers[' + index + '].ID" value="' + user + '"/>' +
                                        '<input type="hidden" name="DecisionMakers[' + index + '].IsExternal" value="' + isExternal + '"/>' +
                                        '<input type="hidden" name="DecisionMakers[' + index + '].IsSelected" value="' + isSelected + '"/>' +
                                        '<input type="hidden" name="DecisionMakers[' + index + '].Name" value="' + userName + '"/>');
        }

    }
}

function syncApproversAndReviewersLists(selectedPDM, isExternal) {
    var domid = $('.collaboratoraccess-blocks').attr('id');
    var extDomid = $('.collaboratorshare-blocks').attr('id');

    var users = [];
    $('#' + domid + ' .block.user.active').each(function () { users.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr('block-role'))); });
    var externalusers = [];
    $('#' + extDomid + ' .block.external.active').each(function () { externalusers.push(parseInt($(this).attr('block-id')) + '|' + parseInt($(this).attr('block-role'))); });
   
    //remove all users from dropdown in order to recreate it based on active approvers and reviewers
    $('#decisionMaker').find('input[name^="DecisionMakers"]').remove();
 
    $.each(users, function () {
        var userRole = this.split('|')[1];
        var userId = this.split('|')[0];
        syncDecisionMakerList(domid, userId, userRole, false, (!isExternal && userId == selectedPDM));
    });

    $.each(externalusers, function () {
        var userRole = this.split('|')[1];
        var userId = this.split('|')[0];
        syncDecisionMakerList(extDomid, userId, userRole, true, (isExternal && userId == selectedPDM));
    });
}

//On change selected approval switch between ad-hoc and template workflow
function changeAccessPartial() {
    if ($("#adHocWorkflow").length > 0) {
        //remove new adhox elements
        $("#addAdHocWorkflow").attr("disabled", false);
        $("#adHocWorkflow").remove();
        $("#widgetPermissions").remove();

        //append previously existing elements
        $("#adHocWorkflowContainer").append(defaultAccessElement);
        $("#adHocWorkflowContainer").append(defaultWidget);

        //reorder elements correctly
        $(defaultAccessElement).insertAfter(defaultWidget);
    }
}

//Save new ad-hoc phase
$("#adHocWorkflowContainer").on('click', '#saveAdHocPhase, #saveAndNewAdHocPhase', function () {
    var saveAndNew = false;
    if ($(this).attr("id") == "saveAndNewAdHocPhase") {
        saveAndNew = true;
    }
    //Get the ad-hoc phase details
    var adHocPhaseDetails = new AdHocPhaseDetails();
    adHocPhaseDetails.__RequestVerificationToken = $("#formAddNewApproval input").val();
    $.ajax({
        type: "POST",
        url: $("#saveAdHocWorkflowUrl").val(),
        data: adHocPhaseDetails,
        success: function (result) {
            processWorkflowDetailsRespons(result, saveAndNew);
        },
        error: function (err) {
        }
    });

    //scroll to top of phase details template
    $('html, body').animate({
        scrollTop: $("#adHocPhases > li:first-child").offset().top - 40
    }, 700);
});

//Create ad-hoc phase details 
function AdHocPhaseDetails() {
    this.ID = $("#adHocPhases > li.current").attr('phase');
    this.ApprovalWorkflow = $('#AdHocWorkflowId').val();
    this.AdHocWorkflowName = $("#AdHocWorkflowName").val();
    this.PhaseName = $("#PhaseName").val();
    this.ShowAnnotationsToUsersOfOtherPhases = $("#ShowAnnotationsToUsersOfOtherPhases").is(':checked');
    this.ShowAnnotationsOfOtherPhasesToExternalUsers = $("#ShowAnnotationsOfOtherPhasesToExternalUsers").is(':checked');
    this.DeadlineDate = $("#DeadlineDate").val();
    this.DeadlineTime = $("#DeadlineTime").val();
    this.DeadlineTimeMeridiem = $("#DeadlineTimeMeridiem").val();
    this.RerunWorkflow = $("#RerunWorkflow").val();

    if (!$("#SelectedDeadlineType:checked").val() == 0) {
        this.SelectedUnit = $("#SelectedUnit").val();
        this.SelectedTrigger = $("#SelectedTrigger").val();
    }
    this.SelectedUsersAndGroups = getPhaseCollaborators();
    this.SelectedDecisionType = $("#SelectedDecisionType").val();
    this.SelectedPrimaryDecisionMaker = $("#primaryDecisionMaker").val();
    this.SelectedApprovalPhaseTrigger = $("#SelectedApprovalPhaseTrigger").val();
    this.ApprovalShouldBeViewedByAll = $("#ApprovalShouldBeViewedByAll").is(":checked");
    this.ExternalUsers = $('#aApprovalTrigger').attr("widget-external");
    this.DecisionMakers = getDecisionMakers();
}

//Get the selected Phase Collaborators
function getPhaseCollaborators() {
    var phaseCollaborators = [];
    $('li[id^="cfca"]').each(function () {
        if ($(this).hasClass("active")) {
            phaseCollaborators.push({
                ID: $(this).attr("block-id"),
                Role: $(this).attr("block-role"),
                IsChecked: true,
                IsExternal: $(this).hasClass("external"),
                IsGroup: $(this).find("input[id$='IsGroup']").val(),
                ApprovalRole: $(this).find("input[id$='ApprovalRole']").val()
            })
        }
    });
    return phaseCollaborators;
}

//Get the phase decision makers
function getDecisionMakers() {
    var selectedDecisionMakers = [];
    for (var i = 0; i < ($('#primaryDecisionMaker option').length - 1); i++) {
        selectedDecisionMakers.push({
            ID: $('#decisionMaker input[name="DecisionMakers[' + i + '].ID"]').val(),
            IsExternal: $('#decisionMaker input[name="DecisionMakers[' + i + '].IsExternal"]').val(),
            IsSelected: $('#decisionMaker input[name="DecisionMakers[' + i + '].IsSelected"]').val(),
            Name: $('#decisionMaker input[name="DecisionMakers[' + i + '].Name"]').val()
        });
    }
    return selectedDecisionMakers;
}

//Display or remove error messages from page
function processWorkflowDetailsRespons(objList, saveAndNew) {
    var idList = ["AdHocWorkflowName", "PhaseName", "aApprovalTrigger", "SelectedDecisionType"];
    if (!objList.Success) {
        $.each(objList.Errors, function (index, value) {
            var element = document.getElementById(index);
            if (element !== null && element.parentElement.children.length == 1) {
                $("#" + index).after("<p class=\"field-validation-error custom-validation-error-message\">" + value + "</p>");
                $("#" + index).closest(".control-group").addClass("error");
            }
            if (index == "aApprovalTrigger" && $("#aApprovalTrigger").parent().children().length == 2) {
                $("#" + index).after("<p class=\"field-validation-error custom-validation-error-message\">" + value + "</p>");
                $("#" + index).closest(".control-group").addClass("error");
            }
            if (idList.indexOf(index) != -1) {
                idList.splice(idList.indexOf(index), 1);
            }
        });
        if (idList.length > 0) {
            for (var i = 0; i < idList.length; i++) {
                if (idList[i] != "aApprovalTrigger") {
                    var id = "#" + idList[i]
                    $(id).parent().find("p").remove();
                    $(id).parent().parent().removeClass("error");
                } else {
                    $("#aApprovalTrigger").next().remove();
                }
            }
        }
    } else {
        $("#AdHocWorkflowName").prop('disabled', true);
        //Add new workflow name to dropdow
        $("#selectedApprovalWorkflow").append("<option value=" + objList.Content.ApprovalWorkflow + " selected=\"true\">" + objList.Content.AdHocWorkflowName + "</option>");
        adHocApprovalID = objList.Content.ApprovalWorkflow;
        $("#AdHocWorkflowId").val(objList.Content.ApprovalWorkflow);

        //Remove errors from the details
        if ($(".control-group").hasClass("error")) {
            $(".control-group").removeClass("error");
        }
        $("p.field-validation-error").remove();
        
        //if phase was updated
        if ($("#adHocPhases > li.current").attr('phase') > 0) {
            $.each(adHocPhasesList,
                    function (i) {
                        if (adHocPhasesList[i].ID == objList.Content.ID) {
                            adHocPhasesList.splice(i, 1);
                            return false;
                        }
                    });
                    $('#adHocPhases').find(' > li[phase="' + objList.Content.ID + '"] > label').text(objList.Content.PhaseName);
        } else {
            //if save succeeded add new created phase to the phases list
            $('#adHocPhases').find(' > li:last-child').before('<li class="adHoc-phase" phase="' + objList.Content.ID + '"><label>' + objList.Content.PhaseName + '</label></li>');
        }

        //save the state of the aApprovalTrigger
        objList.Content["widgetUsers"] = $('#aApprovalTrigger').attr("widget-users");
        objList.Content["widgetGroups"] = $('#aApprovalTrigger').attr("widget-groups");
        objList.Content["widgetExternal"] = $('#aApprovalTrigger').attr("widget-external");
        adHocPhasesList.push(objList.Content);

        var activeElem = $("#adHocPhases > li.current");
        movePhasePanelToTheBottomOfPhasesList(activeElem, saveAndNew);
    }

    //Add up/down arrow to each phase
    appendUpDownArrows();

    //Add action menu to each phase
    appendActionMenu();
}

function movePhasePanelToTheBottomOfPhasesList(self, saveAndNew) {
    var $anchor = self;
    var $snip = $anchor.parent().find('#adhocPhaseContainer'); // get the clicked tab
    var $newPhaseTab = $("#adHocPhases > li").last(); // get the 'Add new phase' tab
    var domid = $('.collaboratoraccess-blocks').attr('id');
 
    //check if clicked tab is 'Add new phase' and show/hide the phase details panel
    if ($anchor.attr('phase') == $newPhaseTab.attr('phase')) {
        restAllControls();
        resetUserAndGroups(domid);

        var container = $("#adHocPhases > li:last-child").find('#adhocPhaseContainer');
    
        if ($snip.hasClass('hide') || saveAndNew == true) {
            $snip.removeClass('hide');
        } else {
            $snip.addClass('hide');
        }

        if (container.length == 0 && !$newPhaseTab.hasClass('current')) {
            $("#adHocPhases > li").removeClass('current');
            $anchor.parent().find('#adhocPhaseContainer').detach();
            $newPhaseTab.append($snip);
            $newPhaseTab.addClass('current');
            $snip.removeClass('hide');
        }
    } else if ($anchor.attr('phase') == $snip.closest('li').attr('phase')) {//if clicked tab is the active one, then hide the phase details panel and show the 'Add new phase' tab
        restAllControls();
        resetUserAndGroups(domid);

        if (saveAndNew) {
            $snip.removeClass('hide');
        } else {
            $snip.addClass('hide');
        }
        $anchor.removeClass('current');
        $anchor.parent().find('#adhocPhaseContainer').detach();
        $newPhaseTab.append($snip);
        $newPhaseTab.addClass('current');
    } else {
        // append the phase details panel to the clicked tab
        $anchor.append($snip);
        $snip.removeClass('hide');

        $('#adHocPhases > li.current #adhocPhaseContainer').detach();
        // bubble up to duplicate anchor's ul tag and remove it entirely
        $("#adHocPhases > li").removeClass('current');
        $anchor.addClass('current');
    }
}

function restAllControls() {
    $("#ShowAnnotationsToUsersOfOtherPhases").attr("checked", true);
    $("#ShowAnnotationsOfOtherPhasesToExternalUsers").attr("checked", false);
    $("#PhaseName").val("");
    $('#SelectedDecisionType option:selected').attr('selected', false);
    $('#SelectedDecisionType option:first-child').attr('selected', true);

    $("#adhocPhaseDetailsContainer option:first-child").attr("selected", true);
    $('#SelectedDeadlineType').val(0);
    $("#ApprovalShouldBeViewedByAll").attr("checked", false);
}

//Attach action menu to each phase header except for the first
function appendActionMenu() {
    var phaseItemList = $(".adhoc-nav #adHocPhases .adHoc-phase");
    for (var i = 0; i < phaseItemList.length; i++) {
        if ($(phaseItemList[i]).attr("phase") !== "0" && $("[phase=" + $(phaseItemList[i]).attr("phase") + "]").find("div").length == 0) {
            $("#phaseTemplate").clone(true).attr('id', 'phaseAction_' + $(phaseItemList[i]).attr("phase")).appendTo($("[phase=" + $(phaseItemList[i]).attr("phase") + "]"));
            $("#phaseAction_" + $(phaseItemList[i]).attr("phase")).css('display', 'inline-block');
        }
    }
}

//Call confirmation delete modal
$("#adHocWorkflowContainer").on('click', ".btnDeletePhase", function () {
    var phaseFullID = $(this).closest("div").attr("id");
    var selectedPhase = parseInt(phaseFullID.substring(phaseFullID.indexOf("_") + 1, phaseFullID.length));
    $("#hdnSelectedPhase").val(selectedPhase);
    $('#modelDialogDeleteConfirmation_1').modal({ backdrop: 'static', keyboard: false });
});

//Fire delete action when clicking Yes button from Delete confirmation modal
$("#adHocWorkflowContainer").on('click', '.btn.btn-primary.delete-yes', function () {
    $.ajax({
        type: "POST",
        url: $("#deleteAdHocPhaseUrl").val(),
        data: { selectedPhase: $("#hdnSelectedPhase").val(), __RequestVerificationToken: $("#formAddNewApproval input").val() },
        success: function (result) {
            if (result.success) {
                $('#modelDialogDeleteConfirmation_1').modal('hide');
                $("li[phase=" + result.phaseId + "]").remove();
                adHocPhasesList.splice(adHocPhasesList.indexOf(result.phaseId), 1);
                appendUpDownArrows();
            }
        },
        error: function (err) {
        }
    });
});

//Add change phase position icons
function appendUpDownArrows() {
    //remove previously existing buttons
    if ($(".upAndDownButtons").length > 0) {
        $(".upAndDownButtons").remove();
    }

    //get the phase items
    var phaseList = $(".adHoc-phase");
    var phases = phaseList.filter(function () {
        return $(this).attr("phase") > 0;
    });

    //add the corespoding arrows
    if (phases.length >= 2) {
        for (var i = 0; i < phases.length; i++) {
            if ($("li[phase=" + $(phases[i]).attr("phase") + "]").is(':first-child') && $("li[phase=" + $(phases[i]).attr("phase") + "] ul.upAndDownButtons").length == 0) {
                $("li[phase=" + $(phases[i]).attr("phase") + "]").append("<ul class=\"pull-left upAndDownButtons\"><a href=\"#\" class=\"ignoredirty down arrow\" style=\"padding-top:11px\" rel=\"nofollow\"><i class=\"icon-chevron-down\"></i></a></ul>");
            } else if (i == phases.length - 1 && $("li[phase=" + $(phases[i]).attr("phase") + "] ul.upAndDownButtons").length == 0) {
                $("li[phase=" + $(phases[i]).attr("phase") + "]").append("<ul class=\"pull-left upAndDownButtons\"><a href=\"#\" class=\"ignoredirty up arrow\"  style=\"padding-top:11px\" rel=\"nofollow\"><i class=\"icon-chevron-up\"></i></a></ul>")
            } else if (!$("li[phase=" + $(phases[i]).attr("phase") + "]").is(':first-child') && i != phases.length - 1 && $("li[phase=" + $(phases[i]).attr("phase") + "] ul.upAndDownButtons").length == 0) {
                $("li[phase=" + $(phases[i]).attr("phase") + "]").append("<ul class=\"pull-left upAndDownButtons\"></ul>")
                $("li[phase=" + $(phases[i]).attr("phase") + "] ul.upAndDownButtons").append("<a href=\"#\" class=\"ignoredirty up arrow\" rel=\"nofollow\"><i class=\"icon-chevron-up\"></i></a>");
                $("li[phase=" + $(phases[i]).attr("phase") + "] ul.upAndDownButtons").append("<a href=\"#\" class=\"ignoredirty down arrow\" rel=\"nofollow\"><i class=\"icon-chevron-down\"></i></a>");
            }
        }
        $("#adHocPhases li:not(:last-child) label").css('padding-left', '12px');
    }
}

//Change phase position based on user input
$("#adHocWorkflowContainer").on('click', '.up, .down', function (e) {
    //Get the trigger row and check witch direction it should be moved
    var isUp = $(this).is(".up");
    var triggerRow = $(this).parent().parent("li");
    var triggerRowId = $(this).parent().parent("li").attr("phase");
    var switchRowID;
    var switchRow;

    //Select the next or previous row
    if (isUp) {
        switchRowID = $(this).parent().parent("li").prev("li").attr("phase");
        switchRow = $(this).parent().parent("li").prev("li").first();
    } else {
        switchRowID = $(this).parent().parent("li").next("li").attr("phase");
        switchRow = $(this).parent().parent("li").next("li");
    }

    //save the new positions of both rows in database
    $.ajax({
        type: "POST",
        url: $("#changeAdHocPhasePositionUrl").val(),
        data: { changingPhase: triggerRowId, changedPhase: switchRowID, __RequestVerificationToken: $("#formAddNewApproval input").val() },
        dataType: "json",
        cache: false,
        success: function (canChangePosition) {
            if (canChangePosition) {
                if (isUp) {
                    triggerRow.insertBefore(switchRow);
                } else {
                    triggerRow.insertAfter(switchRow);
                }
                appendUpDownArrows();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                return;
            }
        }
    });
    e.preventDefault();
});

$("#adHocWorkflowContainer").on('change', '#decisionType', function () {
    configurePhaseDecisionSection(this);
});
