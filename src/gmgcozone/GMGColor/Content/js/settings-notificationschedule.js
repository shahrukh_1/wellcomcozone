﻿var AsEventOccur = 0;

$("#SelectedFrecvency").change(function () {
    if ($(this).children('option:selected').attr('data-key') == AsEventOccur) {
        $('#CollaborateReminders select').attr('disabled', 'disabled');
        $('#NotificationPerPSSession').attr('disabled', false);
    } else {
        $('#CollaborateReminders select').removeAttr('disabled');
        $("#SelectedUnitValue").trigger("change");
        $('#NotificationPerPSSession').attr('disabled', true);
        $('#NotificationPerPSSession').prop('checked', false);
    }
});

$('#SelectedUnitValue').change(function () {
    if (!$(this).val()) {
        $('#SelectedUnit').attr('disabled', 'disabled');
        $('#SelectedTrigger').attr('disabled', 'disabled');
    } else {
        $('#SelectedUnit').removeAttr('disabled');
        $('#SelectedTrigger').removeAttr('disabled');
    }
});

$(document).ready(function () {
    if ($("#userError").val() == 'True') {
        $('#userReq').show();
    }
    if ($('#SelectedFrecvency').children('option:selected').attr('data-key') == AsEventOccur) {
        $('#CollaborateReminders select').attr('disabled', 'disabled');
    } else {
        $('#NotificationPerPSSession').attr('disabled', true);
    }
    $("#SelectedUnitValue").trigger("change");
    $('form').dirtyForms('setClean');
});