﻿function ValidateFileTitles() {
    var validTitles = true;
    $("input[name^='ApprovalTitle_']").each(function(i, v) {
        if ($(v).val().replace(/\s+/g, "") == "") {
            validTitles = false;
        }
    });
    if (!validTitles) {
        $("#FileTitleValidator").val("");
    } else {
        $("#FileTitleValidator").val("no required");
    }
}