﻿function renameApprovalWorkflow(name, workflowId, startFromFirstPhase) {
    showHideControls(false);
    $('#hdnApprovalWorkflowId').val(workflowId);
    $('#modalNewApprovalWorkflow input[type=text]').focus().val(name);
    $('#modalNewApprovalWorkflow input[type=checkbox]').focus().attr("checked", startFromFirstPhase);;
    $('#modalNewApprovalWorkflow').modal({ backdrop: 'static', keyboard: false });
}

$('#modalNewApprovalWorkflow').on('show', function () {
    if (parseInt($('#hdnApprovalWorkflowId').val()) > 0) {
        showHideControls(false);
    } else {
        showHideControls(true);
    }
});

//show/hide specific controls from page depending on action (Create/Rename)
function showHideControls(isCreateAction) {
    if (isCreateAction) {
        $('#modalNewApprovalWorkflow .create').removeClass('hide');
        $('#modalNewApprovalWorkflow .rename').addClass('hide');
    
        $('button[class="create"]').show();
        $('button[class="rename"]').hide();
    } else {
        $('#modalNewApprovalWorkflow .create').addClass('hide');
        $('#modalNewApprovalWorkflow .rename').removeClass('hide');
    
        $('button[class="create"]').hide();
        $('button[class="rename"]').show();
    }
}

function rowToogleWorkflow(workflow, urlAction) {
    if ($('#workflow' + workflow).hasClass('hide')) {
        $('#trigger' + workflow + ' i.icon-caret-right').addClass('icon-caret-down');
        $('#trigger' + workflow + ' i.icon-caret-right').removeClass('icon-caret-right');
        $('#workflow' + workflow).removeClass('hide');
    
        if ($('#approvalWorkflow' + workflow + '_Phases').length == 0) {
            $.ajax({
                type: "GET",
                url: urlAction,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (phasesDetails) {
                    if (phasesDetails.Status == 200) {
                        var parentGroupCellId = $('#workflow' + workflow);
                        var cellText = $(parentGroupCellId).html();
                        if (cellText.trim() != phasesDetails.Content.trim()) {
                            $(parentGroupCellId).append(phasesDetails.Content);
                        }
                        $('#approvalWorkflow' + workflow + '_Phases .up:first, #approvalWorkflow' + workflow + '_Phases  .down:last').css('display', 'none');
                        $('#approvalWorkflow' + workflow + '_Phases .up, #approvalWorkflow' + workflow + '_Phases .down').not('.up:first, .down:last').css('display', 'block');
                        $('#approvalWorkflow' + workflow + '_Phases td').css('vertical-align', 'middle');

                        $('#imgWorkflowLoading' + workflow).remove();
                    } else {
                        $('#errOccured').show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                        return;
                    } else {
                        $('#errOccured').show();
                    }
                }
            });
        }
    } else {
        $('#trigger' + workflow + ' .icon-caret-down').addClass('icon-caret-right');
        $('#trigger' + workflow + ' .icon-caret-down').removeClass('icon-caret-down');
        
        $('#workflow' + workflow).addClass('hide');
    }
}

// performe search by pressing the Enter key
$("#txtSearch").keyup(function (event) {
    if (event.keyCode == 13) {
        $("#btnSearchText").click();
    }
});