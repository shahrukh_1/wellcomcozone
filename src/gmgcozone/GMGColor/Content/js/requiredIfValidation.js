﻿$.validator.unobtrusive.adapters.add('requiredif', ['dependentproperty', 'desiredvalue'], function (options) {
    options.rules['requiredif'] = options.params;
    options.messages['requiredif'] = options.message;
});

jQuery.validator.addMethod('requiredif', function (value, element, parameters) {
    var desiredvalue = parameters.desiredvalue;
    desiredvalue = (desiredvalue == null ? '' : desiredvalue).toString();
    var controlType = $("input[id$='" + breakDownComplexId(parameters.dependentproperty) + "']").attr("type");
    var actualvalue = {}
    if (controlType == "checkbox" || controlType == "radio") {
        //for IsSsoUser detection issue
        if (parameters.dependentproperty.indexOf("IsSsoUser") != -1) {
            actualvalue = (typeof($("input[id*='IsSsoUser']").attr("checked")) === "undefined") ? false : true;
        } else {
            var control = $("input[id$='" + breakDownComplexId(parameters.dependentproperty) + "']:checked");
            actualvalue = control.val();
        }        
    } else {
        actualvalue = $("#" + parameters.dependentproperty).val();
    }
    if ($.trim(desiredvalue).toLowerCase() === $.trim(actualvalue).toLocaleLowerCase()) {
        var isValid = $.validator.methods.required.call(this, value, element, parameters);
        return isValid;
    }
    return true;
});

// If and ID is completex formed with multipele parts cocatenated with "_" this methods take the last part of the ID
function breakDownComplexId(idName) {
    //check if the id is complex
    if (idName.indexOf("_") !== -1) {
        return idName.substring(idName.lastIndexOf("_") + 1, idName.length);
    } else {
        return idName;
    }
}