﻿var lock;
var accountSsoSettings;

$(document).ready(function () {
    if ($("#AccountSsoSettings").val() !== "null") {
        accountSsoSettings = jQuery.parseJSON($("#AccountSsoSettings").val());
        if (accountSsoSettings.Type === "Auth0") {
            lock = new Auth0Lock(accountSsoSettings.ClientId, accountSsoSettings.Domain, {
                auth: {
                    redirectUrl: window.location.protocol + "//" + window.location.host + "/" + "LoginCallback.ashx",
                    responseType: 'code',
                    params: {
                        scope: 'openid email'
                    }
                }
            });
        }
    }
});

$("#objUser_Username").on("blur", function () {
    var minLength = 3;
    if ($("#objUser_Username").val().length >= minLength && $("#AccountSsoSettings").val() !== "null") {
        if (isValidEmailAddress($("#objUser_Username").val())) {
            $.ajax({
                type: "GET",
                url: $("#checkIFUserIsSso").val(),
                data: { email: $("#objUser_Username").val() },
                success: function(response) {
                    if (response.Status === 200 && response.Content === true) {
                        $("#objUser_Password").parent().parent().hide();
                        $("#objUser_IsSsoUser").val(response.Content);

                        if (accountSsoSettings.Type === "Auth0") {
                            $("#logIn").prop("type", "button");
                            $("#logIn").append(function () { lock.show(); });
                            $("#logIn").attr("onclick", "lock.show();");
                        }
                    }
                }
            });
        }
    }
});

function isValidEmailAddress(userNameOrEmail)
{
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(userNameOrEmail);
}