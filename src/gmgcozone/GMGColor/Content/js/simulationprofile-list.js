﻿$(function () {
    var simProfIds, args;

    // Lab tool tip
    $(".customToolTip").tooltip({
        selector: "a[rel=tooltip]"
    })

    $(document).ready(function () {
        //get the list of ids of the simulation profiles that do not have calculated Lab values
        simProfIds = $("#hdnSimProfIdListWithNoLab").val();

        //check if there are no simp profiles without Lab values
        if (simProfIds != "") {

            // convert the string array into a int array
            args = simProfIds.split(",").map(function (item) {
                return parseInt(item, 10);
            });

            updateSimulationProfilesLabValues(args);
        }

        //add table header for Paper tint Lab
        //var th = $("<tr id=\"paperTintHeader\"><th scope=\"col\" colspan=\"9\" style=\"text-align:right;padding-right:90px\">Paper Tint</th></tr>")
        $("#simulationProfile-table tr:first").before('<tr class=\'header-row-roles\'><th colspan=\'5\'></th><th class=\'header-cell-role\' colspan=\'3\'><a href=\'#\'>Paper Tint</a></th><th></th></tr>');
        //$("thead").prepend(th);

    });

    function updateSimulationProfilesLabValues(args) {
        // check if there are simulation profiles for wich the status needs to be updated
        if (args.length > 0) {
            $.ajax({
                type: 'GET',
                traditional: true,
                url: $("#hdnGetSimProfLabValues").val(),
                dataType: "json",
                data: { 'simProfileIdsList': args },
                cache: false,
                contentType: "application/json; charset=utf-8",
                error: function (jqXHR, textStatus, errorThrown) {
                },
                success: function (result) {
                    if (result != null && result.simProfValues != null) {
                        for (var i = 0; i < result.simProfValues.length; i++) {
                            if (result.simProfValues[i].IsValid != null) {
                                createSimulationProfileLabColumn(result.simProfValues[i]);
                                args.splice(args.indexOf(result.simProfValues[i].ID), 1);
                            }
                        }
                        setTimeout(function () { updateSimulationProfilesLabValues(args); }, 5000);
                    }
                }
            });
        }
    };

    function createSimulationProfileLabColumn(result) {

        var row = $("div[data-profile='" + result.ID + "']").closest('tr');
        if (result.IsValid == true) {
            row.find('.validationIcon').html('V');
            row.find('.validationIcon').addClass("badge label-success validationIcon");
            row.find('.validationIcon').tooltip('hide').attr('data-original-title', 'The Profile has been validated by the system');

            var lValue = decimalPlaces(result.L) > 2 ? displayCorrectLabValues(result.L) : result.L;
            var aValue = decimalPlaces(result.a) > 2 ? displayCorrectLabValues(result.a) : result.a;
            var bValue = decimalPlaces(result.b) > 2 ? displayCorrectLabValues(result.b) : result.b;

            // add the lab values
            $(row.find("#L p")).append("<a title=" + result.L + " data-html=\"true\" rel=\"tooltip\" href=\"#\">" + lValue + "</a>");
            $(row.find("#a p")).append("<a title=" + result.a + " data-html=\"true\" rel=\"tooltip\" href=\"#\">" + aValue + "</a>");
            $(row.find("#b p")).append("<a title=" + result.b + " data-html=\"true\" rel=\"tooltip\" href=\"#\">" + bValue + "</a>");
        }
        else {
            row.find('.validationIcon').html('I');
            row.find('.validationIcon').removeClass().addClass("badge label-warning validationIcon");
            row.find('.validationIcon').tooltip('hide').attr('data-original-title', 'The Profile has failed the system validation');
        }
    }

    function displayCorrectLabValues(num) {
        var numToString = num.toString();
        var numDecimals = numToString.substring(numToString.indexOf(".") + 1);
        if (numDecimals.length >= 2) {
            var retVal = numToString.substring(0, numToString.indexOf(".") + 2);
            return retVal;
        }
        else {
            return Math.floor(num * 100) / 100
        }
    }

    function decimalPlaces(num) {
        var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if (!match) { return 0; }
        return Math.max(
        0,
        // Number of digits right of decimal point.
        (match[1] ? match[1].length : 0)

        // Adjust for scientific notation.
        - (match[2] ? +match[2] : 0));
    }
})