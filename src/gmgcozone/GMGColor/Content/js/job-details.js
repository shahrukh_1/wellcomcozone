﻿//
//Default Viewing Conditions
//

var paperTints;

$(document).ready(function () {
    var selectedProfile = $('#DefaultViewingConditions_SelectedSimulationProfile option:selected');
    if (selectedProfile.val() > 0) {
        reorderPaperTints(selectedProfile);
    }

    paperTints = JSON.parse($('#SerializedPaperTints').val());

    var selectedPaperTint = $('#DefaultViewingConditions_SelectedPaperTint option:selected').val();
    if (selectedProfile.val() > 0 && selectedPaperTint > 0) {
        var currentPaperTint = getCurrentPaperTint(selectedPaperTint);
        var profileMeasurementCond = selectedProfile.attr('data-measurementCondition');
        var matchingPaperTintCondition = getPaperTintMeasurementBasedOnProfile(profileMeasurementCond, currentPaperTint);

        if (matchingPaperTintCondition == null) {
            matchingPaperTintCondition = currentPaperTint.Measurements[0];
        }
        calculateDelta2000(selectedProfile, matchingPaperTintCondition);
    }
});

$('#DefaultViewingConditions_SelectedPaperTint').on('change', function () {
    viewingConditionsChanged($('#DefaultViewingConditions_SelectedSimulationProfile option:selected'), $(this).children('option:selected'));
});

$('#DefaultViewingConditions_SelectedSimulationProfile').on('change', function () {
    var selectedProfileOption = $(this).children('option:selected');
    viewingConditionsChanged(selectedProfileOption, $('#DefaultViewingConditions_SelectedPaperTint option:selected')    );
    if (selectedProfileOption.val() > 0) {
        reorderPaperTints(selectedProfileOption);
    }
});

function viewingConditionsChanged($selectedProfile, $selectedPaperTint) {
    $('#DefaultViewingConditions_Delta2000').val('');
    $('#deltaWarning').hide();

    if ($selectedProfile.val() > 0 && $selectedPaperTint.val() > 0) {
        var currentPaperTint = getCurrentPaperTint($selectedPaperTint.val());
        var profileMediaCategory = $selectedProfile.attr('data-mediaCategory');
        var paperTintMediaCategory = currentPaperTint.MediaCategory;
        if (profileMediaCategory != null && profileMediaCategory !== paperTintMediaCategory) {
            showMismatchWarning($("#mediaCategoryMismatchAlert"), $('#mediaCategoryMismatch').val(), profileMediaCategory, paperTintMediaCategory);
        } else {
            hideMismatchWarning($("#mediaCategoryMismatchAlert"));
        }

        var profileMeasurementCond = $selectedProfile.attr('data-measurementCondition');

        var matchingPaperTintCondition = getPaperTintMeasurementBasedOnProfile(profileMeasurementCond, currentPaperTint);
        
        if (matchingPaperTintCondition == null) {
            showMismatchWarning($("#measurementCondMismatchAlert"), $('#measurementConditionMismatch').val(), profileMeasurementCond, currentPaperTint.Measurements.map(function(elem) {
                                                                                                                                                        return elem.MeasurementCondition;
                                                                                                                                                    }).join(","));
        } else {
            hideMismatchWarning($("#measurementCondMismatchAlert"));
        }

        if (matchingPaperTintCondition == null) {
            matchingPaperTintCondition = currentPaperTint.Measurements[0];
        }

        calculateDelta2000($selectedProfile, matchingPaperTintCondition);
    } else {
        hideMismatchWarning($("#mediaCategoryMismatchAlert"));
        hideMismatchWarning($("#measurementCondMismatchAlert"));
        $('#delta2000').val('');
    }
}

function showMismatchWarning($element, text, param1, param2) {
    $element.html(text.replace("{0}", param1).replace("{1}", param2));
    $element.show();
}

function hideMismatchWarning($element) {
    $element.hide();
}

function saveCompleted() {
    $("#btnSaveViewingConditions").removeClass('disabled').removeAttr('disabled');
    $("#btnSaveViewingConditions").text($("#btnSaveViewingConditions").attr('data-text'));
    $('#formViewingCond').dirtyForms('setClean');
}

function reorderPaperTints($selectedProfile) {
    var profileMediaCategory = $selectedProfile.attr('data-mediaCategory');
    //remove breaking line if found
    $('#DefaultViewingConditions_SelectedPaperTint option[value="-1"]').remove();
    //remove options except No Paper Tint
    var paperTints = $('#DefaultViewingConditions_SelectedPaperTint option[value!="' + '"]');

    var orderedPaperTints = [];
    var breakLineOption = $('<option/>');
        breakLineOption.text('------------------');
        breakLineOption.val(-1);

    //push breakline element into the array
    orderedPaperTints.push(breakLineOption);
    for (var i = 0; i < paperTints.length; i++) {
        if ($(paperTints[i]).attr("data-mediaCategory") == profileMediaCategory) {
            //for the same add before neutral
            orderedPaperTints.unshift(paperTints[i]);
        } else {
            //for different add after
            orderedPaperTints.push(paperTints[i]);
        }
    }
    //determine the index of breakline after the sorting
    var breakLineIdx = orderedPaperTints.indexOf(breakLineOption);

    //if it is the first than all papaer tints are of different media category, if it is the last than all are of the same media category
    //if there are no multiple media cateogories just remove the breakline
    if (breakLineIdx == 0 || breakLineIdx == orderedPaperTints.length - 1) {
        orderedPaperTints.splice(breakLineIdx, 1);
    }

    //recreate select
    $('#DefaultViewingConditions_SelectedPaperTint').find('option[value!="' + '"]').remove().end().append(orderedPaperTints);
}

function getCurrentPaperTint(paperTintId) {
    for (var i = 0; i < paperTints.length; i++) {
        if (paperTints[i].ID == paperTintId)
            return paperTints[i];
    }
    return null;
}

function calculateDelta2000(selectedProfile, paperTintMeasurement) {
    var profileLab = { L: parseFloat(selectedProfile.attr('data-L')), A: parseFloat(selectedProfile.attr('data-a')), B: parseFloat(selectedProfile.attr('data-b')) };
    var paperTintLab = { L: paperTintMeasurement.L, A: paperTintMeasurement.a, B: paperTintMeasurement.b };

    var deltaE = DeltaE.getDeltaE00(profileLab, paperTintLab);
    $('#delta2000').val(deltaE);
    if (deltaE > 10) {
        $('#deltaWarning').show();
    } else {
        $('#deltaWarning').hide();
    }
}

function getPaperTintMeasurementBasedOnProfile(selectedProfileMeasurement, currentPaperTint) {
    for (var i = 0; i < currentPaperTint.Measurements.length; i++) {
        if (selectedProfileMeasurement == currentPaperTint.Measurements[i].MeasurementCondition) {
            return currentPaperTint.Measurements[i];
        }
    }

    return null;
}