﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMGColor.Controllers
{
    public class CMSController : BaseController
    {

        public CMSController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        [HttpGet]
        public ActionResult Announcements()
        {
            AccountSettings.CMSAnnouncements model = new AccountSettings.CMSAnnouncements();
            if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
            {
                model.Announcements = DALUtils.SearchObjects<GMGColorDAL.CMSAnnouncement>(o => o.ID > 0, Context)
                                     .Where(o => o.Account == LoggedAccount.ID)
                                     .Select(o => new CMSAnnouncementModel() { ID = o.ID, Content = o.Content }).ToList();
            }
            else
            {
                model.Announcements = DALUtils.SearchObjects<GMGColorDAL.CMSAnnouncement>(o => o.ID > 0, Context)
                                      .Where(o => o.Account == null)
                                      .Select(o => new CMSAnnouncementModel() { ID = o.ID, Content = o.Content }).ToList();
            }

            if (TempData["EditAnnouncement"] != null)
            {
                int announcementId = TempData["EditAnnouncement"].ToString().ToInt().GetValueOrDefault();
                GMGColorDAL.CMSAnnouncement obj = DALUtils.GetObject<GMGColorDAL.CMSAnnouncement>(announcementId, Context);
                model.EditAnnouncement = new CMSAnnouncementModel() { ID = obj.ID, Content = obj.Content };
                TempData["EditAnnouncement"] = null;
                ViewBag.ModelDialog = "modalDialogEditAnnouncement";
            }
            else
            {
                ViewBag.ModelDialog = null;
            }
            model.Announcements = model.Announcements.OrderByDescending(o => o.ID).ToList();
            model.isSysAdmin = CMSBL.isSysAdmin(LoggedAccount.AccountType);
            return View("CMSAnnouncements", model);
        }

        [HttpGet]
        public ActionResult Videos()
        {
            AccountSettings.CMSHomeVideos model = new AccountSettings.CMSHomeVideos();
            if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
            {
                model.TrainingVideos.Videos = DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning, Context)
                    .Where(o => o.Account == LoggedAccount.ID)
                    .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = true, Title = o.VideoTitle, Description = o.VideoDescription }).ToList();
                model.FeatureVideos.Videos = DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => !o.IsTrainning, Context)
                    .Where(o => o.Account == LoggedAccount.ID)
                    .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = false, Title = o.VideoTitle, Description = o.VideoDescription }).ToList();
            }
            else
            {
                model.TrainingVideos.Videos = DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning, Context)
                    .Where(o => o.Account == null)
                    .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = true, Title = o.VideoTitle, Description = o.VideoDescription }).ToList();
                model.FeatureVideos.Videos = DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => !o.IsTrainning, Context)
                    .Where(o => o.Account == null)
                    .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = false, Title = o.VideoTitle, Description = o.VideoDescription }).ToList();
            }

            model.NewVideo.IsVideoTraining = true;

            model.isSysAdmin = CMSBL.isSysAdmin(LoggedAccount.AccountType);
            return View("CMSVideos", model);
        }

        [HttpGet]
        public ActionResult Manuals()
        {
            AccountSettings.CMSItems model = new AccountSettings.CMSItems();

            if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
            {
                model.Items.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Manuals).Where(o => o.Account == LoggedAccount.ID));
            }
            else
            {
                model.Items.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Manuals).Where(o => o.Account == null));
            }
            model.UserPathToTempFolder = LoggedUserTempFolderPath;
            model.Region = (!GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                               ? string.Empty
                               : GMGColorConfiguration.AppConfiguration.AWSRegion;
            model.Type = Enums.CMSItemsType.Manuals;

            model.isSysAdmin = CMSBL.isSysAdmin(LoggedAccount.AccountType);
            return View("CMSItems", model);
        }

        [HttpGet]
        public ActionResult Downloads()
        {
            AccountSettings.CMSItems model = new AccountSettings.CMSItems();

            if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
            {
                model.Items.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Downloads).Where(o => o.Account == LoggedAccount.ID));
            }
            else
            {
                model.Items.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Downloads).Where(o => o.Account == null));
            }
            model.UserPathToTempFolder = LoggedUserTempFolderPath;
            model.Region = (!GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                               ? string.Empty
                               : GMGColorConfiguration.AppConfiguration.AWSRegion;
            model.Type = Enums.CMSItemsType.Downloads;

            model.isSysAdmin = CMSBL.isSysAdmin(LoggedAccount.AccountType);
            return View("CMSItems", model);
        }


        [HttpPost]
        public ActionResult SaveAnnouncement(CMSAnnouncement NewAnnouncement)
        {
            try
            {
                if (!CheckIsValidHtml(NewAnnouncement.Content))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                if (!string.IsNullOrEmpty(NewAnnouncement.Content))
                {
                    try
                    {
                        using (TransactionScope ts = new TransactionScope())
                        {
                            GMGColorDAL.CMSAnnouncement obj = new GMGColorDAL.CMSAnnouncement() {ID = 0};
                            obj.Content = HttpUtility.HtmlDecode(NewAnnouncement.Content);
                            if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
                            {
                                obj.Account = LoggedAccount.ID;
                            }
                            Context.CMSAnnouncements.Add(obj);
                            Context.SaveChanges();
                            ts.Complete();
                        }
                    }
                    catch (DbEntityValidationException ex)
                    {
                        DALUtils.LogDbEntityValidationException(ex);
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.ErrorFormat(ex, "Error when saving an announcement, message: {0}", ex.Message);
                    }
                }
            }
            finally
            {
                
            }
            return RedirectToAction("Announcements", "CMS");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteAnnouncement")]
        public ActionResult DeleteAnnouncement(int? announcementId)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.CMSAnnouncement obj =
                        DALUtils.GetObject<GMGColorDAL.CMSAnnouncement>(announcementId.GetValueOrDefault(), Context);
                    DALUtils.Delete<GMGColorDAL.CMSAnnouncement>(Context, obj);

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error when deleting an announcement, message: {0}", ex.Message);
            }
            return RedirectToAction("Announcements", "CMS");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditAnnouncement")]
        public ActionResult EditAnnouncement(AccountSettings.CMSAnnouncements model, int? announcementId)
        {
            try
            {
                TempData["EditAnnouncement"] = announcementId.Value;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error when editing an announcement, message: {0}", ex.Message);
            }
            return RedirectToAction("Announcements", "CMS");
        }

        [HttpPost]
        public ActionResult UpdateAnnouncement(CMSAnnouncement model)
        {
            try
            {
                if (!CheckIsValidHtml(model.Content))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                GMGColorDAL.CMSAnnouncement obj = DALUtils.GetObject<GMGColorDAL.CMSAnnouncement>(model.ID, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    obj.Content = HttpUtility.HtmlDecode(model.Content);
                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error when updating an announcement, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("Announcements", "CMS");
        }

        [HttpPost]
        public ActionResult SaveVideo(CMSVideoModel NewVideo)
        {
            try
            {
                if (!(CheckIsValidHtml(NewVideo.Title) || CheckIsValidHtml(NewVideo.Description) ))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                int videoItemsCount;
                if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
                {
                    videoItemsCount = DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning == NewVideo.IsVideoTraining, Context).Where(o => o.Account == LoggedAccount.ID).Count();
                }
                else 
                {
                    videoItemsCount = DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning == NewVideo.IsVideoTraining, Context).Where(o => o.Account == null).Count();
                }
                if (!string.IsNullOrEmpty(NewVideo.Url) && videoItemsCount < AccountSettings.CMSHomeVideos.VideoMaxCount)
                {
                    try
                    {
                        using (TransactionScope ts = new TransactionScope())
                        {
                            GMGColorDAL.CMSVideo obj = new GMGColorDAL.CMSVideo() { ID = 0 };

                            obj.VideoUrl = NewVideo.Url;
                            obj.VideoTitle = NewVideo.Title ?? string.Empty;
                            obj.VideoDescription = NewVideo.Description ?? string.Empty;
                            obj.IsTrainning = NewVideo.IsVideoTraining;
                            Context.CMSVideos.Add(obj);
                            if (!CMSBL.isSysAdmin(LoggedAccount.AccountType))
                            {
                                obj.Account = LoggedAccount.ID;
                            }

                            Context.SaveChanges();
                            ts.Complete();
                        }
                    }
                    catch (DbEntityValidationException ex)
                    {
                        DALUtils.LogDbEntityValidationException(ex);
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.ErrorFormat(ex, "Error when saving an video, message: {0}", ex.Message);
                    }
                }
            }
            finally
            {
            }
            return RedirectToAction("Videos", "CMS");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteVideo")]
        public ActionResult DeleteVideo(int? videoId)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.CMSVideo obj = DALUtils.GetObject<GMGColorDAL.CMSVideo>(videoId.GetValueOrDefault(), Context);
                    DALUtils.Delete<GMGColorDAL.CMSVideo>(Context, obj);

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error when deleting an video, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("Videos", "CMS");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveCMSItems")]
        public JsonResult SaveCMSItems(AccountSettings.CMSItems model)
        {
            bool success = true;
            try
            {
                

                GMGColorDAL.Account loggedAccount = this.LoggedAccount;
                // Remove files that were deleted in the ui and doesn't already exist in db
                model.Items.RemoveAll(t => t.FileGuid == null && t.IsDeleted == true);

                List<CMSItemModel> newItems = model.Items.Where(t => t.FileGuid == null).ToList();
                List<CMSItemModel> updatedItems = model.Items.Where(t => t.FileGuid != null).ToList();


                CMSBL.RepairFileName(newItems);

                #region Validation

                foreach (CMSItemModel itemModel in model.Items)
                {
                    if (itemModel.IsDeleted)
                    {
                        continue;
                    }

                    int index = model.Items.IndexOf(itemModel);

                    if (string.IsNullOrEmpty(itemModel.FileName))
                    {
                        ModelState.AddModelError(string.Format("Items[{0}].FileName", index), Resources.Resources.errCMSItemsFileNameIsEmpty);
                        success = false;
                    }

                    if (string.IsNullOrEmpty(itemModel.Name))
                    {
                        ModelState.AddModelError(string.Format("Items[{0}].Name", index), Resources.Resources.reqCMSItemName);
                        success = false;
                    }

                    if (!string.IsNullOrEmpty(itemModel.Name) && itemModel.Name.Length >= 120)
                    {
                        ModelState.AddModelError(string.Format("Items[{0}].Name", index), Resources.Resources.errTooManyCharactors);
                        success = false;
                    }

                    if (!string.IsNullOrEmpty(itemModel.FileName) && itemModel.FileName.Length >= 256)
                    {
                        ModelState.AddModelError(string.Format("Items[{0}].FileName", index), Resources.Resources.errTooManyCharactors);
                        success = false;
                    }
                }

                #endregion

                #region Upload files on S3 and Save to Db

                if (ModelState.IsValid && success)
                {
                    if (newItems.Count > 0)
                    {
                        if (!CMSBL.AddNewFiles(model, newItems, Context, loggedAccount))
                        {
                            ModelState.AddModelError("", Resources.Resources.errCMSItemSaving);
                        }
                    }

                    if (updatedItems.Count > 0)
                    {
                        switch (model.Type)
                        {
                            case Enums.CMSItemsType.Manuals:
                                CMSBL.UpdateExistingManualsFiles(model, updatedItems, Context);
                                break;
                            case Enums.CMSItemsType.Downloads:
                                CMSBL.UpdateExistingDownloadsFiles(model, updatedItems, Context);
                                break;
                        }
                    }
                }

                #endregion
            }
            catch (ExceptionBL ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "Error when saving CMSItems, message: {0}", ex.Message);
                ModelState.AddModelError("", Resources.Resources.errCMSItemSaving);
                success = false;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "Error when saving CMSItems, message: {0}", ex.Message);
                ModelState.AddModelError("", Resources.Resources.errManualSaving);
                success = false;
            }

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var errors = new Dictionary<string, List<string>>();
            foreach (string key in ModelState.Keys)
            {
                if (!ModelState[key].Errors.Any())
                    continue;

                errors.Add(key, ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
            }

            return Json(serializer.Serialize(new
            {
                Success = success,
                Errors = errors
            }));
        }
       
    }
}