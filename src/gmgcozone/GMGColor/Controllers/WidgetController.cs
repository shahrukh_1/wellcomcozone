﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using AutoMapper;
using GMG.CoZone.Common;
using GMG.CoZone.Common.Interfaces;
using GMGColor.ActionFilters;
using GMGColor.Common;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;
using Newtonsoft.Json;
using User = GMGColorDAL.User;
using GMGColorBusinessLogic;
using GMGColor.Hubs;
using Microsoft.AspNet.SignalR;

namespace GMGColor.Controllers
{
    
    public class WidgetController : BaseController
    {

        public WidgetController(IDownloadService downlaodService, IMapper mapper):base(downlaodService, mapper){ }

        #region Groups


        [HttpPost]
        public ActionResult Groups(GroupsModel model)
        {
            if (!CheckIsValidHtml(model.Name))
            {
                return RedirectToAction("XSSRequestValidation", "Error");
            }

            if (model.Group > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.UserGroup>(model.Group, LoggedAccount.ID, Context))
            {
                // if trying to save changes on a group from another account
                Response.Redirect(Url.Action("Unauthorised", "Error"));
            }
            bool isNewGroupCreated = false;
            try
            {
                int groupId = 0;
                bool unique = GMGColorDAL.UserGroup.IsGroupNameUnique(model.Name, model.Group, LoggedAccount.ID, Context);
                if (unique)
                {
                    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                    {
                        UserGroup objGroup;
                        if (model.Group > 0)
                        {
                            objGroup = (from g in Context.UserGroups where g.ID == model.Group select g).FirstOrDefault();
                        }
                        else
                        {
                            objGroup = new UserGroup();
                            objGroup.Creator = this.LoggedUser.ID;
                            objGroup.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                            objGroup.Guid = Guid.NewGuid().ToString();

                            objGroup.Account = this.LoggedAccount.ID;
                            Context.UserGroups.Add(objGroup);
                            isNewGroupCreated = true;
                        }

                        // Delete Collaborator Group
                        if (model.IsDelete)
                        {
                            Context.UserGroups.Remove(objGroup);
                        }
                        // Add / Edit Collaborator Group
                        else
                        {
                            objGroup.Modifier = this.LoggedUser.ID;
                            objGroup.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                            objGroup.Name = model.Name;

                            //add, update and delete operations related with group collaborators
                            PermissionsBL.CUDGroupCollaborators(model.Group, model.CollaboratorsWhereThisIsCollaboratorGroup, Context);
                        }

                        Context.SaveChanges();
                        groupId = objGroup.ID;
                        ts.Complete();
                    }
                }
                else
                {
                    ModelState.AddModelError("Name", Resources.Resources.remGroupNameIsNotAvailable);

                    AccountSettings.UserGroups objModel = new AccountSettings.UserGroups(this.LoggedAccount, Context);
                    objModel.UserGroupID = model.Group;
                    ViewBag.SelectedUsers = model.CollaboratorsWhereThisIsCollaboratorGroup;

                    return View("AddEditUserGroup", objModel);
                }

                if (isNewGroupCreated)
                {
                    NotificationServiceBL.CreateNotification(new GroupWasCreated()
                    {
                        EventCreator = LoggedUser.ID,
                        UserGroupID = groupId,
                        Account = LoggedAccount.ID
                    },
                                                                LoggedUser,
                                                                Context
                                                            );
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "WidgetController.CollaboratorGroup : Updating Collaborator Group Failed. {0}", ex.Message);

            }

            return RedirectToAction("UserGroups", "Settings");
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult IsNameUnique(string name, int? group)
        {
            bool unique = GMGColorDAL.UserGroup.IsGroupNameUnique(name, group.GetValueOrDefault(), LoggedAccount.ID, Context);
            return Json(unique, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Folders

        /// <summary>
        /// Return PartialViewResult of FolderTree
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult Folders(int? Folder, bool? showPanel, bool moveFiles)
        {
            FoldersModel model = null;
            try
            {
                model = PopulateFoldersModel(Folder, showPanel, moveFiles, 0, Context);
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("WidgetController.Folders -> " + ex.Message, ex);
            }

            return PartialView("Folders", model);
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [Compress]
        public ActionResult LoadFolderTree(int? Folder, bool? showPanel, bool moveFiles, int currentTab = -1)
        {
            try
            {
                var model = PopulateFoldersModel(Folder, showPanel, moveFiles, currentTab, Context);

                return Json(
                    new
                        {
                            Status = 400,
                            Content = RenderViewToString(this, "Folders", LoggedUser.ID, model)
                        },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex,
                                                "Error occued while loading folders tree popup, Method: MoveToFolder, Exception: {0}",
                                                ex.Message);

                return Json(
                    new
                        {
                            Status = 300,
                            Content = "Failed loading Move To Folder Popup"
                        },
                    JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetTagwordsForViewAllPopup(string selectedApprovalTypes, string selectedTagwords)
        {
            try
            {
                int ProjectFolderID = 0;
                if (Session[Constants.SelectedProjectId] != null)
                {
                    ProjectFolderID = int.Parse(Session[Constants.SelectedProjectId].ToString()) > 0 ? int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                }
                var model = new ApprovalsFilterModel();
                var ApprovalTypes = selectedApprovalTypes.Split(',').ToList();
                var Tagwords = selectedTagwords.Split(',').ToList();
                if (ApprovalTypes.Count == 1 && ApprovalTypes[0] == "")
                {
                    ApprovalTypes.RemoveAt(0);
                }

                if(ProjectFolderID > 0)
                { 
                    model.tagwords = PopulateProjectApprovalTypeTagwords(ProjectFolderID, ApprovalTypes, true);
                }
                else
                {
                    model.tagwords = PopulateApprovalTypeTagwords(ApprovalTypes, true);
                }
                return Json(
                 new
                 {
                     Status = 400,
                     tagwords = model.tagwords
                 },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "WidgetController.GetTagwordsBasedOnSelectedApprovalType Ajax Request failed. {0}", ex.Message);
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested tagwords details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetTagwordsBasedOnSelectedApprovalType(string selectedApprovalTypes, string selectedTagwords, int currentTab)
        {
            try
            {
                if (Session[Constants.SelectedProjectId] != null)
                {
                    return GetTagwordsBasedOnSelectedProjectApprovalType(selectedApprovalTypes, selectedTagwords, currentTab);
                }
                var model = new ApprovalsFilterModel();
                bool viewAll = false;
                if (Session[Constants.ViewAllApprovals] != null)
                {
                    viewAll = Convert.ToBoolean(Session[Constants.ViewAllApprovals]);
                }
                var ApprovalTypes = selectedApprovalTypes.Split(',').ToList();
                var Tagwords = selectedTagwords.Split(',').ToList();
                if(Tagwords.Count == 1 && Tagwords[0] == "")
                {
                    Tagwords.RemoveAt(0);
                }
                if (Session[Constants.ApprovalPopulateId] != null ? int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 : false)
                {
                    if (int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                    {
                        this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    }
                    else
                    {
                        return null;
                    }
                }

                var count = PopulateApprovalsCount(Tagwords, currentTab, viewAll);
                model.NoOfImageApprovals = count.NoOfImageApprovals;
                model.NoOfVideoApprovals = count.NoOfVideoApprovals;
                model.NoOfDocumentApprovals = count.NoOfDocumentApprovals;
                model.NoOfHTMLApprovals = count.NoOfHTMLApprovals;

                if (ApprovalTypes.Count == 1 && ApprovalTypes[0] == "")
                {
                    ApprovalTypes.RemoveAt(0);
                }
                if(currentTab != 0)
                {
                    model.tagwords = PopulateApprovalTypeTagwords(ApprovalTypes, false);
                }
                else
                {
                    model.tagwords = PopulateProjectFolderTagwords();
                }
                return Json(
                 new
                 {
                     Status = 400,
                     NoOfImageApprovals = model.NoOfImageApprovals,
                     NoOfVideoApprovals = model.NoOfVideoApprovals,
                     NoOfDocumentApprovals = model.NoOfDocumentApprovals,
                     NoOfHTMLApprovals = model.NoOfHTMLApprovals,
                     tagwords = model.tagwords
                 },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "WidgetController.GetTagwordsBasedOnSelectedApprovalType Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested tagwords details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetTagwordsForProjectFolderFilters(string selectedApprovalTypes, string selectedTagwords)
        {
            try
            {
                int currentTab = 0;
                if (Session[Constants.SelectedProjectId] != null)
                {
                    return GetTagwordsBasedOnSelectedProjectApprovalType(selectedApprovalTypes, selectedTagwords, currentTab);
                }
                var model = new ApprovalsFilterModel();
                bool viewAll = false;
                if (Session[Constants.ViewAllApprovals] != null)
                {
                    viewAll = Convert.ToBoolean(Session[Constants.ViewAllApprovals]);
                }
                var ApprovalTypes = selectedApprovalTypes.Split(',').ToList();
                var Tagwords = selectedTagwords.Split(',').ToList();
                if (Tagwords.Count == 1 && Tagwords[0] == "")
                {
                    Tagwords.RemoveAt(0);
                }
                if (Session[Constants.ApprovalPopulateId] != null ? int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 : false)
                {
                    if (int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                    {
                        this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    }
                    else
                    {
                        return null;
                    }
                }
                //var count = new ApprovalsTypeCount();
                //var count = PopulateApprovalsCount(Tagwords, currentTab, viewAll);
                //model.NoOfImageApprovals = count.NoOfImageApprovals;
                //model.NoOfVideoApprovals = count.NoOfVideoApprovals;
                //model.NoOfDocumentApprovals = count.NoOfDocumentApprovals;
                //model.NoOfHTMLApprovals = count.NoOfHTMLApprovals;

                if (ApprovalTypes.Count == 1 && ApprovalTypes[0] == "")
                {
                    ApprovalTypes.RemoveAt(0);
                }
               model.tagwords = PopulateProjectFolderTagwords();
                return Json(
                 new
                 {
                     Status = 400,
                     NoOfImageApprovals = model.NoOfImageApprovals,
                     NoOfVideoApprovals = model.NoOfVideoApprovals,
                     NoOfDocumentApprovals = model.NoOfDocumentApprovals,
                     NoOfHTMLApprovals = model.NoOfHTMLApprovals,
                     tagwords = model.tagwords
                 },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "WidgetController.GetTagwordsBasedOnSelectedApprovalType Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested tagwords details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult GetTagwordsBasedOnSelectedProjectApprovalType(string selectedApprovalTypes, string selectedTagwords, int currentTab)
        {
            try
            {
                int ProjectFolderID = 0;
                if (Session[Constants.SelectedProjectId] != null)
                {
                    ProjectFolderID = int.Parse(Session[Constants.SelectedProjectId].ToString()) > 0 ? int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                }

                var model = new ApprovalsFilterModel();
                bool viewAll = false;
                if (Session[Constants.ViewAllApprovals] != null)
                {
                    viewAll = Convert.ToBoolean(Session[Constants.ViewAllApprovals]);
                }
                var ApprovalTypes = selectedApprovalTypes.Split(',').ToList();
                var Tagwords = selectedTagwords.Split(',').ToList();
                if(Tagwords.Count == 1 && Tagwords[0] == "")
                {
                    Tagwords.RemoveAt(0);
                }
                if (Session[Constants.ApprovalPopulateId] != null ? int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 : false)
                {
                    if (int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                    {
                        this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    }
                    else
                    {
                        return null;
                    }
                }

                var count = PopulateProjectApprovalsCount(ProjectFolderID, Tagwords, currentTab, viewAll);
                model.NoOfImageApprovals = count.NoOfImageApprovals;
                model.NoOfVideoApprovals = count.NoOfVideoApprovals;
                model.NoOfDocumentApprovals = count.NoOfDocumentApprovals;
                model.NoOfHTMLApprovals = count.NoOfHTMLApprovals;

                if (ApprovalTypes.Count == 1 && ApprovalTypes[0] == "")
                {
                    ApprovalTypes.RemoveAt(0);
                }
                if(currentTab != 0)
                { 
                    model.tagwords = PopulateProjectApprovalTypeTagwords(ProjectFolderID, ApprovalTypes, false);
                }
                return Json(
                 new
                 {
                     Status = 400,
                     NoOfImageApprovals = model.NoOfImageApprovals,
                     NoOfVideoApprovals = model.NoOfVideoApprovals,
                     NoOfDocumentApprovals = model.NoOfDocumentApprovals,
                     NoOfHTMLApprovals = model.NoOfHTMLApprovals,
                     tagwords = model.tagwords
                 },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "WidgetController.GetTagwordsBasedOnSelectedApprovalType Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested tagwords details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetSearchRelatedTagwords(string searchText)
        {
            try
            {
                var model = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
                model = PredefinedtagwordsBL.GetTagwordsForselectedApprovalTypes(LoggedAccount.ID, new List<string>(), searchText, Context);
                return Json(
                new
                {
                    Status = 400,
                    tagwords = model
                },
                JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "WidgetController.GetSearchRelatedTagwords Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested tagwords details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }
        private ApprovalsTypeCount PopulateApprovalsCount(List<string> selectedTagwords, int currentTab, bool viewAll)
        {
            var model = new ApprovalsTypeCount();
            model = PredefinedtagwordsBL.GetApprovalsCountForSelectedTagwords(LoggedAccount.ID, LoggedUser.ID, selectedTagwords, currentTab, viewAll, Context);
            return model;
        }

        private ApprovalsTypeCount PopulateProjectApprovalsCount(int ProjectFolderID , List<string> selectedTagwords, int currentTab, bool viewAll)
        {
            var model = new ApprovalsTypeCount();
            model = PredefinedtagwordsBL.GetProjectFolderApprovalsCountForSelectedTagwords(ProjectFolderID, LoggedAccount.ID, LoggedUser.ID, selectedTagwords, currentTab, viewAll, Context);
            return model;
        }
        private List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount> PopulateApprovalTypeTagwords(List<string> selectedApprovalTypes, bool showAllTagwords)
        {
            var model = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
            model = PredefinedtagwordsBL.GetTagwordsForselectedApprovalTypes(LoggedAccount.ID, selectedApprovalTypes, string.Empty, Context);
            if(model.Count > 20 && !showAllTagwords)
            {
                model = model.GetRange(0, 20);
            }
            return model;
        }

        private List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount> PopulateProjectApprovalTypeTagwords(List<string> selectedApprovalTypes, bool showAllTagwords)
        {
            var model = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
            model = PredefinedtagwordsBL.GetTagwordsForselectedApprovalTypes(LoggedAccount.ID, selectedApprovalTypes, string.Empty, Context);
            if (model.Count > 20 && !showAllTagwords)
            {
                model = model.GetRange(0, 20);
            }
            return model;
        }

        private List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount> PopulateProjectFolderTagwords()
        {
            var model = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
            model = PredefinedtagwordsBL.GetTagwordsForProjectFolder(LoggedAccount.ID, Context);
            if (model.Count > 20)
            {
                model = model.GetRange(0, 20);
            }
            return model;
        }
        private List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount> PopulateProjectApprovalTypeTagwords(int ProjectFolderID, List<string> selectedApprovalTypes, bool showAllTagwords)
        {
            var model = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
            model = PredefinedtagwordsBL.GetTagwordsForselectedProjectApprovalTypes(ProjectFolderID, LoggedAccount.ID, selectedApprovalTypes, string.Empty, Context);
            if (model.Count > 20 && !showAllTagwords)
            {
                model = model.GetRange(0, 20);
            }
            return model;
        }


        private FoldersModel PopulateFoldersModel(int? folder, bool? showPanel, bool moveFiles, int currentTab, DbContextBL context)
        {
            var model = new FoldersModel();
            model.ShowPanel = ((showPanel != null) ? (bool)showPanel : true);
            model.Account = this.LoggedAccount.ID;
            model.Refresh = model.ShowPanel == false; // Refresh the page if is not NewApproval page
            model.MoveFiles = moveFiles;
            model.CurrentTab = currentTab;

            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, context);
            model.FolderPermissionsAutoInheritanceEnabled = collaborateSettings.InheritParentFolderPermissions;

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            if (Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] != null)
            {
                model.ExistingFolders =
                    serializer.Serialize(
                        (int[])(Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()]));
            }
            else
            {
                model.ExistingFolders = serializer.Serialize(new int[] { this.LoggedUser.ID });
            }

            int folderID = ((folder != null) ? (int)folder : 0);
            if (folderID > 0)
            {
                Folder objFolder = DALUtils.GetObject<Folder>(folderID, context);

                model.Creator = objFolder.Creator;
                model.Folder = objFolder.ID;
                model.Parent = objFolder.Parent.GetValueOrDefault();
                model.IsPrivate = objFolder.IsPrivate;
                model.Name = objFolder.Name;
            }
            else
            {
                model.Creator = this.LoggedUser.ID;
                model.Folder = 0;
                model.Parent = 0;
                model.IsPrivate = true;
                model.Name = string.Empty;
            }

            model.LoggedUserId = LoggedUser.ID;

            model.AllCollaboratorsDecisionRequired = SettingsBL.GetAccountGlobalSetting(LoggedAccount.ID, GMGColorConstants.AllColDecisionRequired, context);

            return model;
        }

        [HttpPost]
        public string MoveToFolder(FoldersModel model)
        {
            try
            {
                if (model.SelectedApprovals != null && model.SelectedApprovals != "[]")
                {
                    int[] approvalsIds = model.SelectedApprovals.Split(',').ToList().Select(o => int.Parse(o)).ToArray();

                    if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalsIds, LoggedAccount.ID, Context))
                    {
                        throw new Exception("Unauthorised");
                    }
                    List<int> approvalIDS = model.SelectedApprovals.Split(',').Select(Int32.Parse).ToList();
                    if (!ApprovalBL.CheckUserCanAccess(approvalIDS, GMGColorDAL.Approval.CollaborateValidation.MoveApprovals, LoggedUser.ID, Context))
                    {
                        throw new Exception("Unauthorised");
                    }
                    ApprovalBL.MoveApprovalsToFolder(approvalsIds, model.Folder, LoggedUser.ID, LoggedUserCollaborateRole, model.CurrentTab, Context);
                }

                if (model.SelectedFolders != null && model.SelectedFolders != "[]")
                {
                    int[] foldersIds = model.SelectedFolders.Split(',').ToList().Select(o => int.Parse(o)).ToArray();

                    if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>(foldersIds, LoggedAccount.ID, Context))
                    {
                        throw new Exception("Unauthorised");
                    }
                    List<int> folderId = model.SelectedFolders.Split(',').Select(Int32.Parse).ToList();
                    if (LoggedUserCollaborateRole != Role.RoleName.AccountAdministrator && LoggedUserCollaborateRole != Role.RoleName.AccountManager && !ApprovalBL.CheckUserCanAccess(folderId, GMGColorDAL.Approval.CollaborateValidation.MoveFolder, LoggedUser.ID, Context))
                    {
                        throw new Exception("Unauthorised");
                    }
                    ApprovalBL.MoveFoldersToFolder(foldersIds, model.Folder, LoggedUser.ID, LoggedUserCollaborateRole, Context);
                }

                model.Account = this.LoggedAccount.ID;
                model.Creator = this.LoggedUser.ID;
                model.Status = (model.Folder > 0 && model.IsDelete) ? FoldersModel.FolderStatus.Delete : FoldersModel.FolderStatus.Default;

                int[] existingFolders = model.ExistingFolders.Replace("[", string.Empty).Replace("]", string.Empty).Split(',').ToList().Select(o => int.Parse(o)).ToArray();

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
              
                return FolderBL.PopulateFolders(model.Folder, this.LoggedUser.ID, existingFolders, collaborateSettings.InheritParentFolderPermissions, Context);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "WidgetController.MoveToFolder : Move selected items to folder failed. {0}", ex.Message);
                return string.Empty;
            }
            finally
            {
                
            }
        }

        [HttpPost]
        public ActionResult DisplayFolder(FoldersModel model)
        {
            return RedirectToAction("Populate", "Approvals", new { topLinkID = model.Folder });
        }

        
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "CreateFolder")]
        public ActionResult CreateFolder(ApprovalsModel model)
        {
            try
            {
                if (model.objFolderCR.Parent > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>(model.objFolderCR.Parent, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                if (!CheckIsValidHtml(model.objFolderCR.Name))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }
                AccountSettings.CollaborateGlobalSettings collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
                FoldersModel objModel = new FoldersModel()
                                            {
                                                Account = this.LoggedAccount.ID,
                                                Creator = this.LoggedUser.ID,
                                                Name = model.objFolderCR.Name,
                                                IsPrivate = model.objFolderCR.IsPrivate,
                                                Parent = model.objFolderCR.Parent,
                                                AllCollaboratorsDecisionRequired = model.objFolderCR.AllCollaboratorsDecisionRequired,
                                                FolderPermissionsAutoInheritanceEnabled = collaborateSettings.InheritParentFolderPermissions
                                            };
                FolderBL.FolderCRD(Context, ref objModel);

                Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] = objModel.Folder;
                Session[Constants.ApprovalsResetParameters] = false;
            }
            catch( Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "GMGColorDAL.Folder.CreateFolder : Folder action failed. ex: {0}", ex.Message);
            }

            return RedirectToAction("Index", "Approvals");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RenameFolder")]
        public ActionResult RenameFolder(ApprovalsModel model)
        {
            try
            {
                model.BreadCrum = Server.HtmlEncode(model.BreadCrum);

                if (model.objFolderCR.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>(model.objFolderCR.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                List<int> folderId = new List<int>();
                folderId.Add(model.objFolderCR.ID);
                if(!ApprovalBL.CheckUserCanAccess(folderId, GMGColorDAL.Approval.CollaborateValidation.EditFolder, this.LoggedUser.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                FoldersModel objModel = new FoldersModel
                                            {
                                                Account = this.LoggedAccount.ID,
                                                Creator = this.LoggedUser.ID,
                                                Folder = model.objFolderCR.ID,
                                                Name = model.objFolderCR.Name,
                                                AllCollaboratorsDecisionRequired = model.objFolderCR.AllCollaboratorsDecisionRequired
                                            };
                FolderBL.FolderCRD(Context, ref objModel);

                Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] = model.objFolderCR.ID;
                Session[Constants.ApprovalsResetParameters] = false;
            }
            finally
            {
                
            }
            return RedirectToAction("Index", "Approvals");
        }

        [HttpPost]
        public string FolderCRD(FoldersModel model)
        {
            try
            {

                model.Account = this.LoggedAccount.ID;
                model.Creator = this.LoggedUser.ID;
                model.Status = (model.Folder > 0 && model.IsDelete) ? FoldersModel.FolderStatus.Delete : FoldersModel.FolderStatus.Default;
                
                bool viewAllChecked = Convert.ToBoolean(Session[Constants.AdminHasFullRights + LoggedUser.ID]);
                bool adminHasRights = (viewAllChecked && LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator);

                FolderBL.FolderCRD(Context, ref model, adminHasRights);

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
                int[] existingFolders = model.ExistingFolders.Replace("[", string.Empty).Replace("]", string.Empty).Split(',').ToList().Select(o => int.Parse(o)).ToArray();
                return FolderBL.PopulateFolders(((model.Status == FoldersModel.FolderStatus.Delete) ? model.Parent : model.Folder), this.LoggedUser.ID, existingFolders, collaborateSettings.InheritParentFolderPermissions, Context);
            }
            finally
            {
            }
        }

        #endregion

        #region Permissions

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [Compress]
        public MaxJsonResult ApprovalPermissions(bool refresh, bool showPanel, int? approvalid,  string hsa, bool? isFromNewApprovalScreen,  int workflowId = 0 )
        {
            try
            {
                if (approvalid == 0) { approvalid = null; } 
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                PermissionsModel model = BuildPermissionsModel(refresh, showPanel, isFromNewApprovalScreen, false, approvalid);
                if(hsa !=  null)
                {
                    model.SelectedApprovals = hsa;
                }
                if (approvalid == null && (hsa == null || hsa == string.Empty)) { model.IsFolder = true; }

                return new MaxJsonResult(new
                {
                    Status = 400,
                    Content = RenderViewToString(this, "Permissions", LoggedUser.ID, model)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while loading approval permissions popup, Method: ApprovalPermissions, Exception: {0}", ex.Message);

                return new MaxJsonResult(new
                {
                    Status = 300,
                    Content = "Failed loading Approval Permissions Popup"
                },
                       JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Return Partial View Result of Permissions
        /// </summary>        
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult Permissions(bool refresh, bool showPanel, bool? isFromNewApprovalScreen, bool? isPhaseScreen, int? approvalFolderId)
        {
            if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
            {
                LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
            }
            PermissionsModel model = BuildPermissionsModel(refresh, showPanel, isFromNewApprovalScreen, isPhaseScreen, null);
            if (approvalFolderId.GetValueOrDefault() > 0)
            {
                ViewBag.FolderCollaboratorsInfo = ApprovalBL.GetApprovalFolderCollaboratorInfo(approvalFolderId.GetValueOrDefault(), LoggedUser.ID, LoggedUserCollaborateRole, Context);
            }

             return PartialView("Permissions", model);
        }

        [ChildActionOnly]
        public PartialViewResult ProjectApprovalPermissions(bool refresh, bool showPanel, bool? isFromNewApprovalScreen, bool? isPhaseScreen, int? approvalFolderId)
        {
            if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
            {
                LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
            }
            PermissionsModel model = BuildPermissionsModel(refresh, showPanel, isFromNewApprovalScreen, isPhaseScreen, null);
            if (approvalFolderId.GetValueOrDefault() > 0)
            {
                ViewBag.FolderCollaboratorsInfo = ApprovalBL.GetApprovalFolderCollaboratorInfo(approvalFolderId.GetValueOrDefault(), LoggedUser.ID, LoggedUserCollaborateRole, Context);
            }

            return PartialView("ProjectApprovalPermissions", model);
        }

        /// <summary>
        /// Return Partial View Result of Permissions
        /// </summary>        
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult FileTransferPermissions(bool refresh, bool showPanel, bool? isFromNewApprovalScreen, bool? isPhaseScreen, int? approvalFolderId)
        {
            PermissionsModel model = BuildPermissionsModel(refresh, showPanel, isFromNewApprovalScreen, isPhaseScreen, null);

            if (approvalFolderId.GetValueOrDefault() > 0)
            {
                ViewBag.FolderCollaboratorsInfo = ApprovalBL.GetApprovalFolderCollaboratorInfo(approvalFolderId.GetValueOrDefault(), LoggedUser.ID, LoggedUserCollaborateRole, Context);
            }

            // return PartialView("Permissions", model);
            return PartialView("FileTransferPermissions", model);
        }

        /// <summary>
        /// Creates Permission Model for Permissions Partial View
        /// </summary>
        /// <param name="refresh"></param>
        /// <param name="showPanel"></param>
        /// <param name="isFromNewApprovalScreen"></param>
        /// <returns></returns>
        private PermissionsModel BuildPermissionsModel(bool refresh, bool showPanel, bool? isFromNewApprovalScreen, bool? isPhaseScreen, int? approvalId)
        {
            return PermissionsBL.BuildPermissionsModel(LoggedAccount, LoggedUser, LoggedUserCollaborateRole, refresh, showPanel, isFromNewApprovalScreen, isPhaseScreen, approvalId, Context);
        }

        [HttpPost]
        public void ModifyPermissions(PermissionsModel model)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (model.SelectedApprovals != null && model.SelectedApprovals != "0")
                {
                    lstApprovalIds = model.SelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }
                if(!model.IncludeAnOptionalMessage)
                {
                    model.OptionalMessage = string.Empty;
                }
                DashboardInstantNotification notification;
                if (lstApprovalIds.Count <= 1)
                {
                    if(lstApprovalIds.Count() == 1)
                    {
                        model.ID = lstApprovalIds[0];
                        model.CurrentPhase = (from a in Context.Approvals where a.ID == model.ID select a.CurrentPhase).FirstOrDefault();
                    }
                    notification = PermissionsBL.GrantDenyPermissions(model, Context, LoggedUser, LoggedAccount.ID, LoggedUserCollaborateRole, true);
                    Session[Constants.SelectedApprovalId] = Convert.ToString(model.ID);
                    AddInstantDashboardNotification(notification);
                }
                else
                {
                    foreach (int approvalID in lstApprovalIds)
                    {
                        model.ID = approvalID;
                        var approvalCurrentPhase = (from a in Context.Approvals where a.ID == approvalID select a.CurrentPhase).FirstOrDefault();
                        model.CurrentPhase = approvalCurrentPhase;
                        notification = PermissionsBL.GrantDenyPermissionsForMultiAccess(model, Context, LoggedUser, LoggedAccount.ID, LoggedUserCollaborateRole, true);
                        Session[Constants.SelectedApprovalId] = Convert.ToString(model.ID);
                        AddInstantDashboardNotification(notification);
                    }
                }
                
            }
            catch(ExceptionBL exbl)
            {
                GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);
            }
        }

        
        public JsonResult GetPermissionUserInfo(PermissionsModel model)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (model.SelectedApprovals != null && model.SelectedApprovals != "0")
                {
                    lstApprovalIds = model.SelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }

                var permissions = new PermissionsUsersAndRolesModel();
                if (lstApprovalIds.Count <= 1)
                {
                    if (lstApprovalIds.Count() == 1)
                    {
                        model.ID = lstApprovalIds[0];
                        model.CurrentPhase = (from a in Context.Approvals where a.ID == model.ID select a.CurrentPhase).FirstOrDefault();
                    }
                    if (model.Groups != null)
                        permissions.Groups = model.Groups.Split(',').ToList()
                            .Select(m => new GroupPermissions() { Group = Int32.Parse(m), Phase = (int?)null })
                            .ToList();
                    if (model.Users != null)
                    {
                        var selectedUsersAndRole = model.Users.Split(',').ToList();
                        permissions.Users = PermissionsBL.GetUsersAndRoles(selectedUsersAndRole, model.CurrentPhase);
                    }
                    Dictionary<int, int> existingUsers = PermissionsBL.GetExistingUsers(model.ID, model.CurrentPhase, Context);
                    int[] permissionsSelectedUser = permissions.Users.Select(u => u.ID).ToArray();
                    int[] existingCollaborator = existingUsers.Select(x => x.Key).ToArray();
                    var fin = permissionsSelectedUser.Where(x => !existingCollaborator.Contains(x)).ToList();
                    List<int> deleteUsers = existingUsers.Where(eu => !permissions.Users.Select(u => u.ID).ToList().Contains(eu.Key)).Select(u => u.Key).ToList();

                     return Json(
                                new
                                {
                                    addedUserCount = fin.Count(),
                                    removedUserCount = deleteUsers.Count(),
                                },
                                JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(
                                new
                                {
                                    addedUserCount = model.Users.Split(',').ToList().Count(),
                                    removedUserCount = 0,
                                },
                                JsonRequestBehavior.AllowGet);
                }

            }
            
            catch (Exception ex)
            {
                return Json(
                 new
                 {
                     error = true
                 },
                  JsonRequestBehavior.AllowGet);
            }
        }
        public MaxJsonResult ProjectFolderGridApprovalPermissions(bool refresh, bool showPanel, int? approvalid, string hsa, bool? isFromNewApprovalScreen, int workflowId = 0)
        {
            try
            {
                if (approvalid == 0) { approvalid = null; }
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                PermissionsModel model = BuildPermissionsModel(refresh, showPanel, isFromNewApprovalScreen, false, approvalid);
                if (hsa != null)
                {
                    model.SelectedApprovals = hsa;
                }
                return new MaxJsonResult(new
                {
                    Status = 400,
                    Content = RenderViewToString(this, "ProjectApprovalGridPermissions", LoggedUser.ID, model)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while loading approval permissions popup, Method: ApprovalPermissions, Exception: {0}", ex.Message);

                return new MaxJsonResult(new
                {
                    Status = 300,
                    Content = "Failed loading Approval Permissions Popup"
                },
                       JsonRequestBehavior.AllowGet);
            }
        }

        public void ModifyProjectGridPermissions(PermissionsModel model)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (model.SelectedApprovals != null && model.SelectedApprovals != "0")
                {
                    lstApprovalIds = model.SelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }
                if (!model.IncludeAnOptionalMessage)
                {
                    model.OptionalMessage = string.Empty;
                }
                DashboardInstantNotification notification;
                if (lstApprovalIds.Count <= 1)
                {
                    var AllowExecteWorkflowPermission = (from ac in Context.ApprovalCollaborators where ac.Approval == model.ID select ac.ID).Any();

                    if (model.ApprovalWorkflow != null ? ( model.ApprovalWorkflow > 0 && !AllowExecteWorkflowPermission) : false)
                    {
                        ApprovalBL.UpdateExecteWorkflowPermission(model, lstApprovalIds.Count() == 1 ? lstApprovalIds[0] : model.ID , LoggedUser, Context);
                        AddInstantDashboardNotification(PermissionsBL.UpdateDashboardInstantNotification(lstApprovalIds.Count() == 1 ? lstApprovalIds[0] : model.ID,  LoggedUser.ID,  Context));

                    }
                    else
                    {
                        if (lstApprovalIds.Count() == 1)
                        {
                            model.ID = lstApprovalIds[0];
                            model.CurrentPhase = (from a in Context.Approvals where a.ID == model.ID select a.CurrentPhase).FirstOrDefault();
                        }
                        if (model.ApprovalWorkflow > 0)
                        {
                            model.CurrentPhase = PermissionsBL.UpdateProjectFolderApprovalWorkflow(Context, LoggedAccount.ID, model.ID, (int)model.ApprovalWorkflow, model.PhasesCollaborators, LoggedUser.ID);

                        }

                        //This is to update creator of project folder approval if executework flow selected
                        if (model.Creator == -1 || model.ApprovalWorkflow > 0)
                        {
                            PermissionsBL.UpdateProjectFolderApprovalOwner(Context, model.ID, model.Owner, model.CurrentPhase, model.Checklist);
                        }
                        if (model.Checklist != null && model.ApprovalWorkflow == null)
                        {
                            PermissionsBL.UpdateProjectFolderApprovalChecklist(Context, lstApprovalIds.Count() == 1 ? lstApprovalIds[0] : model.ID, model.Checklist.Value);
                        }
                        notification = PermissionsBL.GrantDenyPermissions(model, Context, LoggedUser, LoggedAccount.ID, LoggedUserCollaborateRole, true);
                        Session[Constants.SelectedApprovalId] = Convert.ToString(model.ID);
                        AddInstantDashboardNotification(notification);
                    }
                }
                else
                {
                    foreach (int approvalID in lstApprovalIds)
                    {
                        var AllowExecteWorkflowPermission = (from ac in Context.ApprovalCollaborators where ac.Approval == approvalID select ac.ID).Any();

                        if (model.ApprovalWorkflow != null ? (model.ApprovalWorkflow > 0 && !AllowExecteWorkflowPermission) : false)
                        {
                            ApprovalBL.UpdateExecteWorkflowPermission(model, approvalID , LoggedUser, Context);
                            AddInstantDashboardNotification(PermissionsBL.UpdateDashboardInstantNotification(lstApprovalIds.Count() == 1 ? lstApprovalIds[0] : model.ID, LoggedUser.ID, Context));
                        }
                        else
                        {

                            if (model.Creator == -1)
                            {
                                PermissionsBL.UpdateProjectFolderApprovalOwner(Context, approvalID, model.Owner, model.CurrentPhase, model.Checklist);
                            }
                            if (model.Checklist != null && model.ApprovalWorkflow == null)
                            {
                                PermissionsBL.UpdateProjectFolderApprovalChecklist(Context, lstApprovalIds.Count() == 1 ? lstApprovalIds[0] : model.ID, model.Checklist.Value);
                            }

                            model.ID = approvalID;
                            var approvalCurrentPhase = (from a in Context.Approvals where a.ID == approvalID select a.CurrentPhase).FirstOrDefault();
                            model.CurrentPhase = approvalCurrentPhase;
                            notification = PermissionsBL.GrantDenyPermissionsForMultiAccess(model, Context, LoggedUser, LoggedAccount.ID, LoggedUserCollaborateRole, true);
                            Session[Constants.SelectedApprovalId] = Convert.ToString(model.ID);
                            AddInstantDashboardNotification(notification);
                        }
                    }
                }

            }
            catch (ExceptionBL exbl)
            {
                GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);
            }

        }

        #endregion

        #region Share


        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [Compress]
        public MaxJsonResult ApprovalShare(bool ShowPanel)
        {
            try
            {
                ShareModel model = PopulateShare(LoggedAccount, Context);
                model.ShowPanel = ShowPanel;
                model.Domain = Domain;
                model.DefaultImagePath = GMGColorDAL.User.GetDefaultImagePath(model.Domain);

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, 0, false, Context);
                model.ShowRetoucherApprovalRole = collaborateSettings.EnableRetoucherWorkflow;

				return new MaxJsonResult(new
				{
					Status = 400,
					Content = RenderViewToString(this, "Share", LoggedUser.ID, model)
				}, JsonRequestBehavior.AllowGet);
			}
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while loading approval share popup, Method: ApprovalShare, Exception: {0}", ex.Message);

				return new MaxJsonResult(new
						{
							Status = 300,
							Content = "Failed loading Approval Share Popup"
						},  JsonRequestBehavior.AllowGet);
			}
        }

        /// <summary>
        /// Return PartialViewResult of CollaboratorShare
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult Share(bool ShowPanel, bool isPhaseScreen = false, bool isFromDeliver = false, bool showPopup = false)
        {
            ShareModel model = PopulateShare(LoggedAccount, Context, LoggedUser.ID, isFromDeliver);
            model.ShowPanel = ShowPanel;
            model.Domain = Domain;
            model.DefaultImagePath = GMGColorDAL.User.GetDefaultImagePath(model.Domain);
            model.IsPopupShown = showPopup;
            model.IsPhaseScreen = isPhaseScreen;
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, 0, false, Context);
            model.ShowRetoucherApprovalRole = collaborateSettings.EnableRetoucherWorkflow;

            return PartialView(isFromDeliver ? "DeliverShare" : "Share", model);
        }

        [ChildActionOnly]
        public PartialViewResult ProjectApprovalShare(bool ShowPanel, bool isPhaseScreen = false, bool isFromDeliver = false, bool showPopup = false)
        {
            ShareModel model = PopulateShare(LoggedAccount, Context, LoggedUser.ID, isFromDeliver);
            model.ShowPanel = ShowPanel;
            model.Domain = Domain;
            model.DefaultImagePath = GMGColorDAL.User.GetDefaultImagePath(model.Domain);
            model.IsPopupShown = showPopup;
            model.IsPhaseScreen = isPhaseScreen;
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, 0, false, Context);
            model.ShowRetoucherApprovalRole = collaborateSettings.EnableRetoucherWorkflow;

            return PartialView(isFromDeliver ? "DeliverShare" : "ProjectApprovalShare", model);
        }


        /// <summary>
        /// Return PartialViewResult of CollaboratorShare
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult FileTransferShare(bool ShowPanel, bool isPhaseScreen = false, bool isFromDeliver = false, bool showPopup = false)
        {
            ShareModel model = PopulateShare(LoggedAccount, Context, LoggedUser.ID, isFromDeliver);
            model.ShowPanel = ShowPanel;
            model.Domain = Domain;
            model.DefaultImagePath = GMGColorDAL.User.GetDefaultImagePath(model.Domain);
            model.IsPopupShown = showPopup;
            model.IsPhaseScreen = isPhaseScreen;
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, 0, false, Context);
            model.ShowRetoucherApprovalRole = collaborateSettings.EnableRetoucherWorkflow;

            return PartialView(isFromDeliver ? "DeliverShare" : "FileTransferShare", model);
        }

        /// <summary>
        /// Return ShareModel
        /// </summary>
        /// <param name="objAccount">Account Id</param>
        /// <param name="isFromDeliver"></param>
        /// <returns></returns>
        private static ShareModel PopulateShare(Account objAccount, DbContextBL context, int loggedUserId = 0, bool isFromDeliver = false)
        {
            ShareModel model = new ShareModel();
            model.IsFolder = false;
            if (isFromDeliver)
            {
                model.NotificationsPresets = DeliverBL.GetDeliverNotificationPresets(objAccount.ID, context);
            }
            else
            {
                model.ListRoles = DALUtils.SearchObjects<GMGColorDAL.ApprovalCollaboratorRole>(o => o.ID > 0, context).ToDictionary(m => m.ID, m => m.Name);
            }
            model.DefaultLanguage = objAccount.Locale;
            model.ExistingExternalUsers = ExternalCollaborator.GetActiveUsers(objAccount.ID, loggedUserId, isFromDeliver, context);

            model.ReadOnlyCollaboratorRole = GMGColorDAL.ApprovalCollaboratorRole.GetReadOnlyCollaboratorRoleId(context);
            model.ReviewerCollaboratorRole = GMGColorDAL.ApprovalCollaboratorRole.GetReviewerCollaboratorRoleId(context);
            model.ApproverAndReviewerCollaboratorRole = GMGColorDAL.ApprovalCollaboratorRole.GetApproverAndReviewerCollaboratorRoleId(context);
            model.RetoucherCollaboratorRole = GMGColorDAL.ApprovalCollaboratorRole.GetRetoucherCollaboratorRoleId(context);
            
            model.LocalizeLocaleNames = GMGColorDAL.Locale.GetLocalizeLocaleNames(context);
            
            return model;
        }

        [HttpPost]
        public void SendAndShare(ShareModel model)
        {
            try
            {
                if (CheckIsValidHtml(model.ExternalEmails))
                { 
                    int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                }
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(model.Approval, LoggedAccount.ID, Context))
                {
                    RedirectToAction("Unauthorised", "Error");
                    return;
                }
                Session[Constants.SelectedApprovalId] = Convert.ToString(model.Approval);
                
                string strUsersAndEmails = PermissionsBL.GetFormatedExternalUsersDetails(model.ExternalEmails);

                if (!String.IsNullOrEmpty(model.ExternalUsers) || !String.IsNullOrEmpty(strUsersAndEmails))
                {
                    model.ExternalUsers = model.ExternalUsers;
                    model.ExternalEmails = strUsersAndEmails;
                    model.SendEmails = true;
                    model.ShowPanel = false; // Send from Permissions
                    PermissionsBL.ShareApproval(model, Context, OutOfOfficeUserID == 0 ? this.LoggedUser : OutOfOfficeUser, this.LoggedAccount.ID, null);
                }
            }
            }
            catch (ExceptionBL exbl)
            {
                    GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);
            }
        }

        #endregion

        #region CMS
        
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [ValidateInput(false)]
        [HttpGet]
        public ActionResult IsValidHtml(string content)
        {
            bool isValid = CheckIsValidHtml(content);

            return Json(isValid, JsonRequestBehavior.AllowGet);
        }

        #endregion

        private void AddInstantDashboardNotification(DashboardInstantNotification notification)
        {
            var clients = InstantNotificationsHub.GetDashboardClients(notification.Version);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();

            foreach (string client in clients)
            {
                context.Clients.Client(client).addDashboardNotification(notification);
            }
        }
    }
}
