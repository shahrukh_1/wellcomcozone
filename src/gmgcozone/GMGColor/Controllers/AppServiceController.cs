﻿using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Common.DomainModels;
using GMGColor.ActionFilters;
using GMGColor.Common;
using GMGColor.ViewModels;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;

namespace GMGColor.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class AppServiceController : BaseController
    {

        private readonly IApprovalService _approvalService;

        public AppServiceController(IApprovalService approvalService, IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {
            _approvalService = approvalService;
        }

        #region POST

        [HttpPost]
        public void SNSVideoTranscodingNotificationEvent()
        {
            try
            {
                GMGColorLogging.log.InfoFormat("AppServiceController.SetVideoStatus : Set video Status request arrived");
                Stream req = Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string jsonString = new StreamReader(req).ReadToEnd();

                JToken token = JObject.Parse(jsonString);

                string messageType = Request.Headers["x-amz-sns-message-type"];

                switch (messageType)
                {
                    case "SubscriptionConfirmation":
                        string topicARN = (string)token.SelectToken("TopicArn");
                        string messageToken = (string)token.SelectToken("Token");

                        GMGColor.AWS.AWSSimpleNotificationService.ConfirmSubscription(GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                         GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                         GMGColorConfiguration.AppConfiguration.VideoTranscoderAWSRegion,
                                                                         topicARN,
                                                                         messageToken);
                        break;
                    case "Notification":
                        string messageBody = (string)token.SelectToken("Message");
                        JToken notificationToken = JObject.Parse(messageBody);
                        string jobState = (string)notificationToken.SelectToken("state");
                        string jobId = (string)notificationToken.SelectToken("jobId");
                        string width = String.Empty;
                        string height = String.Empty;
                        if (jobState == "COMPLETED")
                        {
                            width = (string)notificationToken.SelectToken("outputs").First.SelectToken("width");
                            height = (string)notificationToken.SelectToken("outputs").First.SelectToken("height");
                        }

                        GMGColorLogging.log.InfoFormat("AppServiceController.SetVideoStatus : Set video Status request arrived for job with transcoding id {0} and jobstate {1}, width {2}, height {3} :", jobId, jobState, width, height);

                        if (jobState == "COMPLETED" || jobState == "ERROR")
                        {
                            ProcessState(jobId, jobState, width, height);                            
                        }
                        
                        break;
                }

                GMGColorLogging.log.InfoFormat("AppServiceController.SetVideoStatus: End");
                Response.StatusCode = (int)HttpStatusCode.Accepted;
                return; 
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "AppServiceController.SetVideoStatus : Set video Status request failed " + ex.Message);
            }
        }

        private void SendVideoCompletedNotificationToBrowser(int approvalId)
        {
            var approvalStatus = new MediaDefineApprovalProgressStatusModel();

            approvalStatus.ApprovalId = approvalId;
            approvalStatus.Status = "Success";
            approvalStatus.Thumbnail = _approvalService.GetVideoThumbnailPath(approvalId);

            SignalRService signalRService = new SignalRService();
            signalRService.UpdateDashboardApprovalProcessStatus(approvalStatus);
        }

        private void SendErrorNotificationToBrowser(int approvalId)
        {
            var approvalStatus = new MediaDefineApprovalProgressStatusModel();

            approvalStatus.ApprovalId = approvalId;
            approvalStatus.Status = "Error";
            approvalStatus.Thumbnail = "";

            SignalRService signalRService = new SignalRService();
            signalRService.UpdateDashboardApprovalProcessStatus(approvalStatus);
        }
        #endregion

        #region GET

        /// <summary>
        /// Return Folder Tree Html
        /// </summary>
        /// <param name="ActiveFolder">Active folder id.</param>
        /// <param name="IsOwnFolder">If "true" populater Own folders or otherwise shared folders tree.</param>
        /// <param name="UserID">Id of owner, folders to be populated.</param>
        /// <param name="Parent">Parent folder id.</param>
        /// <returns></returns>
        [HttpGet]
        [Compress]
        public JsonResult PopulateFoldersAsync(int ActiveFolder, int UserID, int Parent, Role.RoleName userRole, bool folderPermissionsAutoInheritanceEnabled,bool tabletVersion = false, bool viewAll = false)
        {
            try
            {
                ApprovalFolder model = new ApprovalFolder
                {
                    FolderID = ActiveFolder,
                    LoggedUserID = UserID,
                    Parent = Parent,
                    ViewAllFilesAndFolders = viewAll
                };

                ViewBag.FolderPermissionsAutoInheritanceEnabled = folderPermissionsAutoInheritanceEnabled;
                ViewBag.LoggedUserID = UserID;
                ViewBag.LoggedUserCollaborateRole = userRole;

                var JsonResult = Json(
                new
                {
                    Status = 400,
                    Content = RenderViewToString(this, "ApprovalFolders", UserID, model)
                },
                JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;

                return JsonResult;

            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while pupulating folder tree, Method: PopulateFolders, Exception: {0}", ex.Message);


                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error occured while populating folder tree"
                           },
                           JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Gets the data required for the visual indicator bar
        /// </summary>
        /// <param name="approvals"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalsVisualBarData(string approvals)
        {
            try
            {
                List<int> ids = approvals.Split(',').Select(int.Parse).ToList();

                List<Approval.ApprovalsVisualIndicatorData> collaborators = null;
                if (ids.Count > 0)
                {
                    collaborators = ApprovalBL.GetApprovalsVisualIndicatorData(ids, Context);
                }

                return Json(
                    new
                    {
                        Status = 400,
                        Content = Json(collaborators, JsonRequestBehavior.AllowGet)

                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AppServiceController.GetApprovalsVisualBarData Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested data for visual indicator"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the data required for the SOAD indicator
        /// </summary>
        /// <param name="approvals"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalsSoadData(string approvals)
        {
            try
            {
                List<int> ids = approvals.Split(',').Select(int.Parse).ToList();

                List<ApprovalSoadViewModel> approvalSoadViewModels = null;
                if (ids.Count > 0)
                {
                    var approvalSoadDetails = _approvalService.GetApprovalSoadDetails(ids);

                    approvalSoadViewModels = new List<ApprovalSoadViewModel>();

                    foreach (var approvalSoadDetail in approvalSoadDetails)
                    {
                        approvalSoadViewModels.Add(new ApprovalSoadViewModel()
                        {
                            Approval = approvalSoadDetail.Approval,
                            Phase = approvalSoadDetail.Phase,
                            ProgressStateClass = approvalSoadDetail.ProgressStateClass
                        });
                    }
                }

                return Json( new { Status = 400, Content = Json(approvalSoadViewModels, JsonRequestBehavior.AllowGet)}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AppServiceController.GetApprovalsSOADData Ajax Request failed. {0}", ex.Message);

                return Json(new { Status = 300, Content = "Error retrieving the requested data for SOAD indicator" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetUserSoadData(int approval)
        {
            try
            {
                var approvalSoadDetails = _approvalService.GetUserSoadDetialsForSpecifiedApproval(approval);
                var userSoadViewModels = new List<UserSoadViewModel>();

                foreach (var userSoadDetail in approvalSoadDetails)
                {
                    userSoadViewModels.Add(new UserSoadViewModel()
                    {
                        Approval = userSoadDetail.Approval,
                        Phase = userSoadDetail.Phase.GetValueOrDefault(),
                        ProgressStateClass = userSoadDetail.ProgressStateClass,
                        IsExternal = userSoadDetail.IsExternal,
                        Collaborator = userSoadDetail.Collaborator
                    });
                }

                return Json(new { Status = 400, Content = Json(userSoadViewModels, JsonRequestBehavior.AllowGet) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AppServiceController.GetUserSoadData Ajax Request failed. {0}", ex.Message);

                return Json(new { Status = 300, Content = "Error retrieving the requested data for SOAD indicator" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetPhaseSoadData(int approval)
        {
            try
            {
                var approvalSoadDetails = _approvalService.GetPhasesSoadDetialsForSpecifiedApproval(approval);
                var userSoadViewModels = new List<ApprovalSoadViewModel>();

                foreach (var userSoadDetail in approvalSoadDetails)
                {
                    userSoadViewModels.Add(new ApprovalSoadViewModel()
                    {
                        Approval = userSoadDetail.Approval,
                        Phase = userSoadDetail.Phase.GetValueOrDefault(),
                        ProgressStateClass = userSoadDetail.ProgressStateClass
                    });
                }

                return Json(new { Status = 400, Content = Json(userSoadViewModels, JsonRequestBehavior.AllowGet) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AppServiceController.GetPhaseSoadData Ajax Request failed. {0}", ex.Message);

                return Json(new { Status = 300, Content = "Error retrieving the requested data for SOAD indicator" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Process State for the Transcoding Video
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="jobState"></param>
        public void ProcessState(string jobId, string jobState, string width, string height)
        {
            try
            {

                GMGColorLogging.log.InfoFormat("ProcessState 1.ENTER");

                var approvalVideoId = _approvalService.GetApprovalIdForTranscoding(jobId);

                if (approvalVideoId == null)
                {
                    GMGColorLogging.log.ErrorFormat(null, "ProcessState-Video job ID {0}, Error occurred while processing state of transcoded video , approval with specified transcoded job id cannot be found", jobId);
                    return;
                }
                // For SWF there will be 2 ApprovalPages and we need to update the second one (TODO - check why there are 2 pages generated for an SWF approval)
                GMGColorDAL.ApprovalPage approvalVideoPage = (from apPage in Context.ApprovalPages
                                                              where apPage.Approval == approvalVideoId && apPage.Number == 1
                                                              select apPage).FirstOrDefault();

                if (approvalVideoPage == null)
                {
                    GMGColorLogging.log.ErrorFormat(null, "ProcessState-Video job ID {0}, Error occurred while processing state of transcoded video , there is no approval page for the specified approval", jobId);
                    return;
                }

                GMGColorLogging.log.InfoFormat("ProcessState- 2.Video job ID {0}, ProcessState approvalVideoPage progress {1}", jobId, approvalVideoPage.Progress);

                switch (jobState)
                {
                    case "PROGRESSING":
                        //TODO: temporarily remove progressing updates to video as it generated numerous waiting tasks on MSSQL server
                        if (approvalVideoPage.Progress + 10 < 100)
                            approvalVideoPage.Progress += 10;
                        break;

                    case "COMPLETED":
                        if (approvalVideoPage.Progress == 100)
                        {
                            return;
                        }
                        else
                        {
                            approvalVideoPage.Progress = 100;

                            if (!String.IsNullOrEmpty(width) && !String.IsNullOrEmpty(height))
                            {
                                approvalVideoPage.PageLargeThumbnailHeight = Int32.Parse(height);
                                approvalVideoPage.PageLargeThumbnailWidth = Int32.Parse(width);
                                GMGColorLogging.log.InfoFormat("ProcessState- 3.Video job ID {0}, ProcessState approvalVideoPage width {1} height {2}", jobId, approvalVideoPage.PageLargeThumbnailWidth, approvalVideoPage.PageLargeThumbnailHeight);

                            }
                            else
                            {
                                GMGColorLogging.log.ErrorFormat(null, "Video job error: ID {0} error appying width {1} and height {2}", jobId, width, height);
                            }

                            Context.SaveChanges();

                            SendVideoCompletedNotificationToBrowser(approvalVideoId.GetValueOrDefault());
                        }
                        break;

                    case "ERROR":
                        var approvalVideo = (from ap in Context.Approvals where ap.ID == approvalVideoId select ap).FirstOrDefault();
                        approvalVideo.IsError = true;
                        approvalVideo.ErrorMessage = "Video Transcoding failed";
                        Context.SaveChanges();
                        SendErrorNotificationToBrowser(approvalVideoId.GetValueOrDefault());
                        break;
                }

            }
            finally
            {
            }
        }

        #endregion
     }
}
