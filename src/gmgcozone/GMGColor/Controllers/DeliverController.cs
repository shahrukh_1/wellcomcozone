﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using GMG.CoZone.Common;
using GMGColor.ActionFilters;
using GMGColor.Common;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;
using Utils = GMG.CoZone.Common.Utils;
using GMGColorBusinessLogic;
using System.Web.UI;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using AppModule = GMG.CoZone.Common.AppModule;


namespace GMGColor.Controllers
{
    public class DeliverController : BaseController
    {

        private const string CPSearchDeliverJobs = "CPSearchDeliverJobs";
        public const string TitleColumn = "Title";
        public const string UserColumn = "User";
        public const string PagesColumn = "Pages";
        public const string CpNameColumn = "CPName";
        public const string WorkflowColumn = "Workflow";
        public const string CreatedDateColumn = "CreatedDate";
        public const string StatusKeyColumn = "StatusKey";

        public DeliverController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        #region GET Actions

        [HttpGet]
        public ActionResult Index(int? page, string search, string sort, string sortdir, int? rowsPerPage)
        {
            if (rowsPerPage.HasValue && !WebGridNavigationModel.PagesCount.Contains(rowsPerPage.Value))
            {
                return RedirectToAction("Index", "Deliver");
            }

            ViewBag.FileDownload = BaseBL.PopulateFileDownloadModel(LoggedAccount.ID, LoggedUser.ID, AppModule.Deliver, Context);
            ViewBag.DeliverRole = LoggedUserDeliverRole;
            ViewBag.Page = page;
            ViewBag.Search = search;
            ViewBag.Sort = sort;
            ViewBag.SortDir = sortdir;

            if (Session["PageSize_" + LoggedUser.ID] != null && rowsPerPage.GetValueOrDefault() == Convert.ToInt32(Session["PageSize_" + LoggedUser.ID]))
            {
                ViewBag.RowsPerPage = Convert.ToInt32(Session["PageSize_" + LoggedUser.ID]);
            }
            else
            {
                ViewBag.RowsPerPage = JobBL.GetNumberOfRowsPerPage(LoggedUser.ID, rowsPerPage, GMGColorConstants.RememberNumberOfFilesShown, GMGColorConstants.DefaultNumberOfFilesShown, Context);
                Session["PageSize_" + LoggedUser.ID] = ViewBag.RowsPerPage;
            }

            ViewBag.AnyServerAvailable = ColorProofInstance.AnyAvailableValidInstances(LoggedUser.ID, Context);

            return View("Index");
        }

        [HttpGet]
        [Compress]
        public ActionResult GetDeliverJobs(int? page, string search, string sort, string sortdir, int? rowsPerPage, GMGColorDAL.Role.RoleName deliverRole, bool validServerAvailable)
        {
            try
            {
                int sortField = 0;
                switch (sort)
                {
                    case TitleColumn: sortField = 1; break;
                    case UserColumn: sortField = 2; break;
                    case PagesColumn: sortField = 3; break;
                    case CpNameColumn: sortField = 4; break;
                    case WorkflowColumn: sortField = 5; break;
                    case CreatedDateColumn: sortField = 6; break;
                    case StatusKeyColumn: sortField = 7; break;
                    default: sortField = 0; break;
                }
                bool sortAsc = (sortdir ?? string.Empty).ToLower() == "asc";

                if (!rowsPerPage.HasValue || rowsPerPage.GetValueOrDefault() <= 0)
                {
                    rowsPerPage = (int?)10;
                }

                if (page.HasValue && page.Value <= 0)
                    page = 1;


                Deliver.DeliverModel model = new Deliver.DeliverModel(this.LoggedAccount, this.LoggedUser.ID,
                                                                      this.LoggedUserTempFolderPath,
                                                                      page.HasValue ? page.Value : 1, sortField, sortAsc, Context, rowsPerPage.GetValueOrDefault(), search ?? String.Empty) { SearchText = search ?? string.Empty };

                int actualNrOfPages = model.TotalRows / model.RowsPerPage +
                                   (model.TotalRows % model.RowsPerPage == 0 ? 0 : 1);

                if (actualNrOfPages <= 0)
                    actualNrOfPages = 1;

                if (page > actualNrOfPages || page < 1)
                {
                    page = actualNrOfPages;
                    model = new Deliver.DeliverModel(this.LoggedAccount, this.LoggedUser.ID,
                                                                      this.LoggedUserTempFolderPath,
                                                                      page.HasValue ? page.Value : 1, sortField, sortAsc, Context, rowsPerPage.GetValueOrDefault(), search ?? String.Empty) { SearchText = search ?? string.Empty };
                }

                model.DeliverRole = deliverRole;
                model.ValidServerAvailable = validServerAvailable;

                ViewBag.PageSize = model.RowsPerPage;

                return Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, "DeliverJobs", LoggedUser.ID, model)
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in DeliverController.DeliverJobs, Asynchronous Loading deliver Jobs grid method: {0}", ex.Message);

                return Json(
                       new
                       {
                           Status = 300,
                           Content = "Failed to Load Deliver Jobs Grid"
                       },
                       JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult NewDeliver(int? page, int? rowsPerPage)
        {
            Deliver.DeliverModel model = new Deliver.DeliverModel(this.LoggedAccount, this.LoggedUser.ID, this.LoggedUserTempFolderPath, Context);
            ViewBag.DeliverRole = LoggedUserDeliverRole;

            string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser, GMG.CoZone.Common.AppModule.Deliver, 0, Context, LoggedAccountBillingPlans.DeliverBillingPlan);
            if (!string.IsNullOrEmpty(warningMessage))
            {
                model.PopupModel.ErrorMessage = warningMessage;
            }

            if (!rowsPerPage.HasValue || rowsPerPage.GetValueOrDefault() <= 0)
            {
                rowsPerPage = 10;
            }

            if (page.GetValueOrDefault() <= 0)
            {
                page = 1;
            }

            model.PopupModel.RowsPerPage = rowsPerPage.GetValueOrDefault();
            model.PopupModel.PageNr = page.GetValueOrDefault();

            return View("NewDeliver", model);
        }

        [HttpGet]
        public ActionResult ExternalUserJobDetailLink(string userId, int? job)
        {
            if (String.IsNullOrEmpty(userId) || job == null ||
                !DeliverBL.ExtUserHasAccessToJob(userId, job.Value, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            Deliver.JobDetailsInstance jobDetails = GetJobDetails(job);

            return View("ExternalUserJobDetails", jobDetails);

        }

        #endregion

        #region POST Actions
        
        public JsonResult GetWorkflows(int? selectedCPServerId)
        {
            try
            {
                List<ColorProofCoZoneWorkflow> securedList = DeliverBL.GetWfSecuredList(selectedCPServerId.Value,
                    LoggedUser.ID,
                    Context);

                return
                    Json(
                        new
                        {
                            workflows =
                                securedList.Select(
                                    t =>
                                        new
                                        {
                                            ID = t.ID,
                                            Name = t.Name,
                                            ProofStandard =
                                                string.IsNullOrEmpty(t.ColorProofWorkflow1.ProofStandard)
                                                    ? string.Empty
                                                    : t.ColorProofWorkflow1.ProofStandard,
                                            SpotColors =
                                                HttpUtility.HtmlEncode(t.ColorProofWorkflow1.SupportedSpotColorsForWeb),
                                            MaxPaperWidth =
                                                t.ColorProofWorkflow1.MaximumUsablePaperWidth.HasValue
                                                    ? t.ColorProofWorkflow1.MaximumUsablePaperWidth.ToString()
                                                    : string.Empty
                                        }).OrderBy(o => o.Name)
                        });
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch( Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                                                            "DeliverController.GetWorkflows : error occurred while getting workflows. " +
                                                            ex.Message);
            }
            return Json(new {workflows = ""});
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetJobsStatuses(int[] jobIdList)
        {
            if (jobIdList == null) 
                return Json(new {statuses = new DeliverJobStatusFromCP[] {}}, JsonRequestBehavior.AllowGet);
            try
            {
                // set thread UI cluture based on account culture
                SetUICulture(LoggedUser.ID);

                List<KeyValuePair<int, string>> translatedStatuses = GMGColor.Common.Utils.GetDeliverTranslationsFromStatuses();
                List<DeliverJobStatusFromCP> listOfStatuses = GMGColorDAL.DeliverJob.GetJobStatuses(jobIdList, translatedStatuses, Context);
                return Json(new { statuses = listOfStatuses }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "DeliverController.GetJobsStatuses : error occured while geting job statuses for some jobs. {0}", ex.Message);
            }
            return Json(new { statuses = new DeliverJobStatusFromCP[] { }.ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult JobCancel(int jobId)
        {
            GMGColorDAL.DeliverJob job = GMGColorDAL.DeliverJob.GetJobByIdAndAccountId(jobId, LoggedAccount.ID, Context);
            if (job != null)
            {
                try
                {
                    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                    {
                        GMGColorDAL.DeliverJob jobBo = DALUtils.GetObject<GMGColorDAL.DeliverJob>(jobId, Context);
                        Dictionary<string, string> dict = Utils.CreateCommandQueueMessage(jobBo.Guid, jobBo.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.UserName, jobBo.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.Password, jobBo.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.Guid, CommandType.Cancel);
                        GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.DeliverJob);

                        jobBo.IsCancelRequested = true;

                        Context.SaveChanges();
                        ts.Complete();
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "DeliverController.CancelJob : error occured while canceling a Deliver Job. {0}", ex.Message);
                    return Json(new { status = false });
                }
            }
            return Json(new { status = true});
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteJob")]
        public ActionResult DeleteJob(string SelectedInstance, int? PageNr, int? RowsPerPage, string SearchText)
        {
            try
            {
                int? selectedDeliverJobId = SelectedInstance.ToInt();
                if (!selectedDeliverJobId.HasValue)
                {
                    GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "GetJobDetails method, invalid selectedInstance parameter");
                    // handle invalid argument error
                    Response.Redirect(Url.Action("Cannotcontinued", "Error"));
                }
                else
                {
                    DeliverBL.DeleteDeliverJob(selectedDeliverJobId.Value, Context);
                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "Deliver: DeleteJob method, error: {0}", ex.Message);
            }

            return RedirectToAction("Index", GMGColorDAL.CustomModels.Common.GetCurrentQueryStrings(Request.QueryString,
                    new string[] { "page", "RowsPerPage", "search" },
                    new object[] { PageNr, RowsPerPage, SearchText }
                    ));
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "JobDetail")]
        public ActionResult JobDetail(string SelectedInstance, int? PageNr, int? RowsPerPage, string SearchText, bool? isSharedJob)
        {
            // redirect to the existing action above .. instead 2 actions that make the same thing
            // the url of the page must to be changed for the deliver menu to be hightlighted
            return RedirectToAction("JobDetailLink", GMGColorDAL.CustomModels.Common.GetCurrentQueryStrings(Request.QueryString,
                    new string[] { "selectedInstance", "page", "RowsPerPage", "search", "isSharedJob" },
                    new object[] { SelectedInstance, PageNr, RowsPerPage, SearchText, isSharedJob }
                    ));
        }

        public ActionResult JobDetailLink(string SelectedInstance, int? page, int? RowsPerPage, string search, bool? isSharedJob)
        {
            if (LoggedUserDeliverRole == Role.RoleName.AccountModerator || LoggedUserDeliverRole == Role.RoleName.AccountContributor)
            {
                return RedirectToAction("Index", new { rowsPerPage = RowsPerPage, page = page });
            }
            
            if (LoggedUserDeliverRole == Role.RoleName.AccountNone)
            {
                return RedirectToAction("Home", "Home");
            }

            Deliver.JobDetailsInstance jobDetails = GetJobDetails(SelectedInstance.ToInt(), isSharedJob.GetValueOrDefault());
            jobDetails.PageNr = page;
            jobDetails.RowsPerPage = RowsPerPage;
            jobDetails.SearchText = search;
            jobDetails.IsSharedJob = isSharedJob.GetValueOrDefault();

            return View("InternalUserJobDetails", jobDetails);

        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddDeliverJob")]
        public ActionResult AddDeliverJob(Deliver.AddInstance model, FormCollection collection, int? page)
        {
            bool success = true;

            if (ModelState.IsValid)
            {
                List<int> lstAttachedFilesTmp = collection.AllKeys.Where(o => o.Contains("ApprovalTitle_")).Select(o => int.Parse(o.Split('_').Last())).ToList();

                #region Validator for no files
                if (lstAttachedFilesTmp.Count == 0)
                {
                    ModelState.AddModelError("fileValidator", Resources.Resources.reqDeliverJobFile);
                    model.ShowModalPopup = true;
                    success = false;
                }
                #endregion

                if (success)
                {
                    string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser, GMG.CoZone.Common.AppModule.Deliver, lstAttachedFilesTmp.Count, Context, LoggedAccountBillingPlans.DeliverBillingPlan);
                    if (!string.IsNullOrEmpty(warningMessage))
                    {
                        model.ErrorMessage = warningMessage;
                        success = false;
                    }
                }

                List<DeliverJob> files = new List<DeliverJob>();
                foreach (int key in lstAttachedFilesTmp)
                {
                    if (!success)
                         break;

                    var jobTitles = Request.Form.GetValues("ApprovalTitle_" + key);
                    if (jobTitles.Any())
                    {
                        var jobFileNames = Request.Form.GetValues("ApprovalFileName_" + key);
                        var jobSizes = Request.Form.GetValues("ApprovalSize_" + key);
                        var jobPagesCount = Request.Form.GetValues("PdfPagesCount_" + key);

                        if (jobTitles.Count() == 1)
                        {
                            int?[] pages = (model.PagesRangeValue ?? string.Empty).Split(new string[] { ",", "-" }, StringSplitOptions.RemoveEmptyEntries).ToArray().ToIntArray();
                            int? pageCount = GMG.CoZone.Common.Utils.Decrypt(collection["PdfPagesCount_" + key] ?? string.Empty).ToInt();
                            bool valid = true;
                            foreach (int? currentPage in pages)
                            {
                                if (currentPage.HasValue && currentPage.Value > pageCount.GetValueOrDefault())
                                {
                                    valid = false;
                                }
                            }
                            if (!valid)
                            {
                                ModelState.AddModelError("PagesRangeMaxPageCountHiddenField", Resources.Resources.reqRangeValueMaxPageCount);
                                model.ShowModalPopup = true;
                                success = false;
                            }
                        }

                        for (int i = 0; i < jobTitles.Count(); i++)
                        {
                            if (string.IsNullOrEmpty(jobTitles[i]))
                            {
                                #region Validator for empty file title

                                ModelState.AddModelError("fileTitleValidator", Resources.Resources.reqDeliverJobTtile);
                                model.ShowModalPopup = true;
                                success = false;

                                #endregion
                            }
                            else
                            {
                                files.Add(new DeliverJob()
                                {
                                    Guid = Guid.NewGuid().ToString(),
                                    Name = jobFileNames[i] ?? string.Empty,
                                    SourcePath = this.LoggedUserTempFolderPath,
                                    Size = decimal.Parse(jobSizes[i] ?? string.Empty),
                                    Title = jobTitles[i] ?? string.Empty,
                                    PagesRangeValue = model.PagesRangeValue,
                                    PagesRadioButton = model.PagesRadioButton,
                                    IncludeProofMetaInformation = model.IncludeProofMetaInformation,
                                    SelectedCpWorkflowId = model.SelectedCPWorkflowId.GetValueOrDefault(),
                                    LogProofControlResults = model.LogProofControlResults,
                                    TotalNrOfPages = GMG.CoZone.Common.Utils.Decrypt(jobPagesCount[i] ?? string.Empty).ToInt() ?? 1
                                }); 
                            }
                        }
                    }
                }

                if (success && !CreateDeliverJobs(files.ToArray(), true, model.ExternalUsers, model.ExternalEmails, model.OptionalMessage))
                {
                    success = false;
                }

                if (success)
                {
                    var jobGuid = files.Select(t => t.Guid).ToArray();
                    
                    NotificationServiceBL.CreateNotification(new NewDeliverJob
                                                            {
                                                                EventCreator = LoggedUser.ID,
                                                                InternalRecipient = LoggedUser.ID,
                                                                CreatedDate = DateTime.UtcNow,
                                                                JobsGuids = jobGuid
                                                            },
                                                            LoggedUser,
                                                            Context
                                                            );

                    var deliverExternalUsers = DeliverExternalCollaborator.GetDeliverCollaboratorsByJobGuids(jobGuid, EventType.GetDeliverEventTypeKey(EventType.TypeEnum.Deliver_File_Submitted), Context);

                    //send deliver file submited emails for deliver external users
                    foreach (var user in deliverExternalUsers)
                    {
                        NotificationServiceBL.CreateNotification(new NewDeliverJob()
                                                                {
                                                                    EventCreator = LoggedUser.ID,
                                                                    ExternalRecipient = user,
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    JobsGuids = jobGuid
                                                                },
                                                                LoggedUser,
                                                                Context
                                                                );
                    }

                    if (DeliverBL.IsSharedWorkflow(model.SelectedCPWorkflowId, Context))
                    {
                        var user = DeliverBL.GetUserByWorkflowID(model.SelectedCPWorkflowId, Context);
                        if (user.Account != LoggedAccount.ID)
                        {

                            NotificationServiceBL.CreateNotification(new HostAdminUserFileSubmitted()
                                                                    {
                                                                        EventCreator = LoggedUser.ID,
                                                                        InternalRecipient = user.ID,
                                                                        JobsGuids = jobGuid,
                                                                        IsSharedJob = LoggedUser.Account != user.Account
                                                                    },
                                                                        LoggedUser,
                                                                        Context
                                                                        );

                            NotificationServiceBL.CreateNotification(new InvitedUserFileSubmitted()
                                                                    {
                                                                        EventCreator = LoggedUser.ID,
                                                                        InternalRecipient = LoggedUser.ID,
                                                                        JobsGuids = jobGuid
                                                                    },
                                                                        LoggedUser,
                                                                        Context
                                                                        );
                        }
                    }
                }
            }
            else
            {
                success = false;
            }

            return !success ? OpenPopupAndShowErrors(model, page) : RedirectToAction("Index", new { rowsPerPage = model.RowsPerPage, page = model.PageNr });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchDeliverJobs")]
        public ActionResult Search(Deliver.DeliverModel model, string SearchText)
        {
            string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
            TempData[CPSearchDeliverJobs] = s;

            return RedirectToAction("Index", new {search = HttpUtility.HtmlEncode(s)});
        }

        [HttpPost]
        public ActionResult DeleteMultipleJobs(string deliverJobIds, int? PageNr, int? RowsPerPage, string SearchText)
        {
            try
            {
                if (!string.IsNullOrEmpty(deliverJobIds))
                {
                    var deliverJobs = new JavaScriptSerializer().Deserialize<List<int>>(deliverJobIds);

                    foreach (var deliverJobId in deliverJobs)
                    {
                        DeliverBL.DeleteDeliverJob(deliverJobId, Context);
                    }
                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "DeliverController.DeleteMultipleJobs : error occured while deleting multiple jobs. {0}", ex.Message);
            }

            return RedirectToAction("Index", GMGColorDAL.CustomModels.Common.GetCurrentQueryStrings(Request.QueryString,
                    new string[] { "page", "RowsPerPage", "search" },
                    new object[] { PageNr, RowsPerPage, SearchText }
                    ));
        }
        #endregion

        #region Methods

        private ActionResult OpenPopupAndShowErrors(Deliver.AddInstance model, int? pageNr)
        {
            #region Display the popup with the errors
            Deliver.DeliverModel cpModel = new Deliver.DeliverModel(this.LoggedAccount, this.LoggedUser.ID, this.LoggedUserTempFolderPath, pageNr.HasValue ? pageNr.Value : 1, 0, false, Context);
            cpModel.PopupModel = new Deliver.AddInstance(this.LoggedAccount, this.LoggedUser.ID, this.LoggedUserTempFolderPath, Context)
                                     {
                                         LogProofControlResults = model.LogProofControlResults,
                                         SelectedCPServerId = model.SelectedCPServerId,
                                         SelectedCPWorkflowId = model.SelectedCPWorkflowId,
                                         IncludeProofMetaInformation = model.IncludeProofMetaInformation,
                                         PagesRadioButton = model.PagesRadioButton,
                                         PagesRangeValue = model.PagesRangeValue,
                                         ErrorMessage = model.ErrorMessage,
                                         ShowModalPopup = true,
                                         ShowUploader = true
                                     };

            if (model.SelectedCPServerId > 0)
            {
                var selectedCPServerIdValue = model.SelectedCPServerId.GetValueOrDefault();
                List<GMGColorDAL.ColorProofWorkflow> lst = DALUtils.SearchObjects<GMGColorDAL.ColorProofWorkflow>(
                        o => o.ColorProofInstance1.ID == selectedCPServerIdValue, Context).ToList();
                cpModel.PopupModel.ListWorkflows = lst;
            }

            return View("NewDeliver", cpModel);
            #endregion
        }

        /// <summary>
        /// Get Jod Details Method
        /// </summary>
        /// <param name="SelectedInstance"></param>
        /// <returns></returns>
        private Deliver.JobDetailsInstance GetJobDetails(int? selectedDeliverJobId, bool isSharedJob = false)
        {
            Deliver.JobDetailsInstance jobDetails = new Deliver.JobDetailsInstance(this.LoggedAccount, Context);
            try
            {
                if (!selectedDeliverJobId.HasValue)
                {
                    GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "GetJobDetails method, invalid selectedInstance parameter");
                    // handle invalid argument error
                    Response.Redirect(Url.Action("Cannotcontinued", "Error"));
                }
                else
                {
                    var success = false;
                    if (isSharedJob)
                    {
                        var sharedJob = SharedJobDetails.GetSharedJobDetails(selectedDeliverJobId, Context);
                        if (sharedJob != null)
                        {
                            success = true;
                            jobDetails.Id = Convert.ToString(selectedDeliverJobId);
                            jobDetails.AccountUrl = sharedJob.AccountUrl;
                            jobDetails.Username = sharedJob.Username;
                            jobDetails.Email = sharedJob.Email;
                        }
                    }
                    else
                    {
                        GMGColorDAL.DeliverJob objDeliverJob = GMGColorDAL.DeliverJob.GetFullJobItem(selectedDeliverJobId.Value, LoggedAccount.ID, Context);

                        // make sure to redirect the user to 404 page if the jobId does not exist or the job was added from another account
                        if (objDeliverJob != null)
                        {
                            success = true;
                            jobDetails.Id = Convert.ToString(selectedDeliverJobId);
                            jobDetails.JobName = objDeliverJob.Job1.Title;
                            jobDetails.JobStatus = Common.Utils.GetJobStatusLabel((DeliverJobStatus) objDeliverJob.DeliverJobStatu.Key);
                            jobDetails.Location = objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.Address;
                            jobDetails.ProofControlInline =
                                objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.IsInlineProofVerificationSupported != null &&
                                objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.IsInlineProofVerificationSupported.Value
                                    ? Resources.Resources.lblColorProofWorkflowInlineProofEnabled
                                    : Resources.Resources.lblColorProofWorkflowInlineProofDisabled;
                            jobDetails.Filename = objDeliverJob.FileName;
                            jobDetails.ColorProofId =
                                Convert.ToString(objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance);
                            jobDetails.Owner = objDeliverJob.User.GivenName;
                            jobDetails.Pages = objDeliverJob.Pages;

                            jobDetails.ColorProofServerName =
                                objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.SystemName;
                            jobDetails.WorkflowName = objDeliverJob.ColorProofCoZoneWorkflow.Name;
                            jobDetails.ProofStandard = objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ProofStandard;
                            jobDetails.SpotColors =
                                Utils.GetSupportedSpotColorsForWeb(
                                    objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.SupportedSpotColors);
                            jobDetails.MaximumUsablePaperWidth =
                                Convert.ToString(objDeliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.MaximumUsablePaperWidth);
                            jobDetails.DeliverExternalUsers = DeliverBL.GetExternalUsersByJobId(selectedDeliverJobId.Value, Context);

                            foreach (
                                GMGColorDAL.DeliverJobPage objPage in objDeliverJob.DeliverJobPages)
                            {
                                Deliver.JobDetailsPage page = new Deliver.JobDetailsPage()
                                                                  {
                                                                      PageName = objPage.UserFriendlyName,
                                                                      PageResultMessage = objPage.ErrorMessage,
                                                                      Status = (DeliverPageStatus) objPage.DeliverJobPageStatu.Key
                                                                  };
                                page.Results.AddRange(objPage.DeliverJobPageVerificationResults.
                                                          Select(
                                                              o =>
                                                              new Deliver.JobDetailVerificationResult()
                                                                  {
                                                                      StripName = o.StripName,
                                                                      AvgDelta = o.AvgDelta,
                                                                      MaxDelta = o.MaxDelta,
                                                                      VerificationResult = o.ProofResult,
                                                                      AllowedAvgDelta = o.AllowedTolleranceAvg,
                                                                      AllowedMaxDelta = o.AllowedTolleranceMax,
                                                                      IsSpotColorVerification = o.IsSpotColorVerification
                                                                  }));
                                jobDetails.ResultsPages.Add(page);
                            }
                        }
                    }
                    
                    if(!success)
                    {
                        // redirect to 404 page error in case that the deliver job id is invalid
                        Response.Redirect(Url.Action("Unauthorised", "Error"));
                    }
                }
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "GetJobDetails method, error: {0}", ex.Message);
            }
            return jobDetails;
        }

        #endregion
    }
}