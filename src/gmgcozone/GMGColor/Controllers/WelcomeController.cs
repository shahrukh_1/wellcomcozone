﻿using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorDAL.CustomModels;

namespace GMGColor.Controllers
{
    public class WelcomeController : BaseController
    {
        public WelcomeController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        public ActionResult Index()
        {
            WelcomeModel model = new WelcomeModel(this.LoggedAccount, this.LoggedAccountType, this.LoggedUser);

            return View(model);
        }
    }
}
