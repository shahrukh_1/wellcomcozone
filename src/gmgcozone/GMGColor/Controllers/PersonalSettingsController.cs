﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using Microsoft.Web.Administration;


namespace GMGColor.Controllers
{
    public class PersonalSettingsController : BaseController
    {
        public PersonalSettingsController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        #region Fields

        private const string NotificationsKey = "Notifications";
        private const string PresetKey = "preset";
        private const string NotificationFrequencyKey = "notificationFrequency";
        private const string IncludeMyOwnActivityKey = "includeMyOwnActivity";
        #endregion

        #region Properties

        protected bool HasNotifications
        {
            get
            {
                bool n = false;
                try
                {
                    n = Session["HasNotifications_" + LoggedUser.ID] != null
                                ? Convert.ToBoolean(Session["HasNotifications_" + LoggedUser.ID])
                                : NotificationServiceBL.HasPersonalNotifications(LoggedUser.ID, Context);
                }
                catch (Exception)
                {
                    Session["HasNotifications_" + LoggedUser.ID] = n;
                }
                Session["HasNotifications_" + LoggedUser.ID] = n;
                return n;
            }
        }

        #endregion

        #region GET Actions

        [HttpGet]
        public ActionResult Index()
        {
            var objUserModel = new OwnerUser(this.LoggedUser, Context)
            {
                HasNotifications = HasNotifications
            };
            return View("Index", objUserModel);
        }

        [HttpGet]
        public ActionResult Photo()
        {
            var photoModel = new ChangePhoto(LoggedUser, LoggedUserTempFolderPath)
            {
                HasNotifications = HasNotifications
            };

            if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(photoModel.UserID, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            //Show color if logged account is not sys admin
            if (LoggedAccountType != AccountType.Type.GMGColor)
            {
                ViewBag.AvailableColors = UserBL.GetAvailableUserColors(LoggedUser.ID, LoggedAccount.ID, Context);
            }

            return View("Photo", photoModel);
        }

        [HttpGet]
        public ActionResult Password()
        {
            var passwordModel = new ChangePassword(LoggedUser.ID)
            {
                HasNotifications = HasNotifications
            };

            if (TempData["modelErrors"] != null)
            {
                Dictionary<string, string> modelErrors = TempData["modelErrors"] as Dictionary<string, string>;
                foreach (KeyValuePair<string, string> error in modelErrors)
                {
                    ModelState.AddModelError(error.Key, error.Value);
                }
            }

            return View("Password", passwordModel);
        }

        [HttpGet]
        public ActionResult Notifications()
        {
            NotificationsModel objNotificationsModel;
            if (TempData[NotificationsKey] == null)
            {
                objNotificationsModel = new NotificationsModel(this.LoggedUser, Context);
                objNotificationsModel.HasNotifications = HasNotifications;
            }
            else
            {
                Dictionary<string, int> dicNitificationsPara = (Dictionary<string, int>)TempData[NotificationsKey];

                objNotificationsModel = new NotificationsModel(this.LoggedUser.ID, dicNitificationsPara[PresetKey], Context);
                objNotificationsModel.objUser.Preset = dicNitificationsPara[PresetKey];
                objNotificationsModel.objUser.NotificationFrequency = dicNitificationsPara[NotificationFrequencyKey];
                objNotificationsModel.objUser.IncludeMyOwnActivity = dicNitificationsPara[IncludeMyOwnActivityKey] == 1;
            }

            if (TempData["modelErrors"] != null)
            {
                Dictionary<string, string> modelErrors = TempData["modelErrors"] as Dictionary<string, string>;
                foreach (KeyValuePair<string, string> error in modelErrors)
                {
                    ModelState.AddModelError(error.Key, error.Value);
                }
            }

            return View("Notifications", objNotificationsModel);

        }

        [HttpGet]
        public ActionResult OutOfOffice()
        {
            var outofofficemodel = new OutOfOfficeModel(LoggedUser.ID);

            outofofficemodel.HasNotifications = HasNotifications;
            outofofficemodel.OutOfOfficeData = UserBL.GetDataFromOutOfOffice(LoggedUser.ID, LoggedAccount.DateFormat1.Pattern, Context);
            return View("OutOfOffice", outofofficemodel);
        }

        [HttpGet]
        public ActionResult AddNewOutOfOffice(int? outOfOfficeId)
        {
            if (outOfOfficeId.HasValue)
            {
                var model = new AddNewOutOfOffice();
                var outOfOfficeDetails = UserBL.GetOutOfOfficeDetails((int)outOfOfficeId, LoggedAccount.DateFormat1.Pattern,Context);
                model.StartDate = GMGColorFormatData.GetFormattedDate(outOfOfficeDetails.FromDate, LoggedAccount.DateFormat1.Pattern);
                model.EndDate = GMGColorFormatData.GetFormattedDate(outOfOfficeDetails.ToDate, LoggedAccount.DateFormat1.Pattern);
                model.SubstituteUser = UserBL.GetUsersByAccount(LoggedUser, LoggedAccount, LoggedUserCollaborateRole, Context);
                model.Description = outOfOfficeDetails.Description;
                model.UserId = outOfOfficeDetails.SubstituteUserId;
                model.ID = (int)outOfOfficeId;

                DateTime toDate = DateTime.Now;
                DateTime fromDate = DateTime.Now;
                ViewBag.startDateYear = fromDate.Year;
                ViewBag.startDateMonth = fromDate.Month;
                ViewBag.startDateDay = fromDate.Day;
                ViewBag.endDateYear = toDate.Year;
                ViewBag.endDateMonth = toDate.Month;
                ViewBag.endDateDay = toDate.Day;
                ViewBag.MinStartDate = DateTime.Now;

                return View("AddNewOutOfOffice", model);
            }
            else {
                var model = new AddNewOutOfOffice();

                model.StartDate = GMGColorFormatData.GetFormattedDate(DateTime.UtcNow, LoggedAccount.DateFormat1.Pattern);
                model.EndDate = GMGColorFormatData.GetFormattedDate(DateTime.UtcNow, LoggedAccount.DateFormat1.Pattern);

                model.SubstituteUser = UserBL.GetUsersByAccount(LoggedUser, LoggedAccount, LoggedUserCollaborateRole, Context);

                DateTime toDate = DateTime.Now;
                DateTime fromDate = DateTime.Now;
                ViewBag.startDateYear = fromDate.Year;
                ViewBag.startDateMonth = fromDate.Month;
                ViewBag.startDateDay = fromDate.Day;
                ViewBag.endDateYear = toDate.Year;
                ViewBag.endDateMonth = toDate.Month;
                ViewBag.endDateDay = toDate.Day;
                ViewBag.MinStartDate = DateTime.Now;

                return View("AddNewOutOfOffice", model);
            }
        }


      
        #endregion

        #region POST Actions

        [HttpPost]
        public ActionResult Information(OwnerUser model)
        {
            try
            {
                string Input_String = model.FirstName + model.LastName + model.Username + model.MobileTelephoneNumber + model.HomeTelephoneNumber;

                if (!CheckIsValidHtml(Input_String) )
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }
                if (!ValidateEmail(LoggedAccount, model.EmailAddress,LoggedUser.EmailAddress))
                {
                    //email already in use
                    ModelState.AddModelError("EmailAddress", Resources.Resources.reqEmailExist);
                    model.HasNotifications = HasNotifications;
                    model.InitializePermissions(LoggedUser.ID, LoggedAccount.ID, Context);
                    model.Languages = GMGColorDAL.Locale.GetLocalizeLocaleNames(Context);
                    return View("Index", model);
                }
                else if (!ValidateUserName(model.Username, LoggedUser.Username))
                {
                    //username already in use
                    ModelState.AddModelError("Username", Resources.Resources.reqUsernameExist);
                    model.HasNotifications = HasNotifications;
                    model.InitializePermissions(LoggedUser.ID, LoggedAccount.ID, Context);
                    model.Languages = GMGColorDAL.Locale.GetLocalizeLocaleNames(Context);
                    return View("Index", model);
                }
                else
                {
                    GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(LoggedUser.ID, Context);

                    using (TransactionScope ts = new TransactionScope())
                    {
                        objUser.GivenName = model.FirstName;
                        objUser.FamilyName = model.LastName;
                        objUser.HomeTelephoneNumber = model.HomeTelephoneNumber;
                        objUser.MobileTelephoneNumber = model.MobileTelephoneNumber;
                        objUser.Username = model.Username;
                        objUser.EmailAddress = model.EmailAddress;
                        objUser.Locale = model.SelectedLanguage;
                        objUser.Modifier = this.LoggedUser.ID;

                        Context.SaveChanges();
                        ts.Complete();
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ChangePhoto(ChangePhoto model, string hdnFileName_1)
        {
            try
            {
                GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(LoggedUser.ID, Context);

                string relativeFolderPath = "users/" + objUser.Guid + "/";
                string uploadFileName = (!String.IsNullOrEmpty(hdnFileName_1.Trim())) ? hdnFileName_1.Replace("|", string.Empty) : string.Empty;
                if (!String.IsNullOrEmpty(uploadFileName))
                {
                    objUser.PhotoPath = "profilepicture-64px-64px" + Path.GetExtension(uploadFileName);
                    string relativeSourceFilePath = this.LoggedUserTempFolderPath + uploadFileName;
                    if (GMGColorIO.FolderExists(relativeFolderPath, this.LoggedAccount.Region, true) &&
                        GMGColorIO.FileExists(relativeSourceFilePath, this.LoggedAccount.Region))
                    {
                        GMGColorCommon.ResizeImageBasedOnWidthAndHeight(relativeSourceFilePath,
                                                                        relativeFolderPath + objUser.PhotoPath, 64, 64,
                                                                        this.LoggedAccount.Region);
                    }
                }
                else //File has been deleted
                {
                    string relativeDestinationFilePath = relativeFolderPath + objUser.PhotoPath;
                    try
                    {
                        if (GMGColorIO.DeleteFileIfExists(relativeDestinationFilePath, LoggedAccount.Region))
                        {
                            GMGColorLogging.log.Info("File was deleted from the location of :" + relativeDestinationFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error("GMGColorIO.DeleteFileIfExists : Exception details : " + ex.Message, ex);
                    }

                    objUser.PhotoPath = null;
                }

                // Update user color
                objUser.ProofStudioUserColor = UserBL.ValidateProofStudioUserColor(LoggedAccount.ID, LoggedUser.ID, model.ProofStudioColor, Context);
                if (objUser.ProofStudioUserColor == null)
                {
                    throw new Exception("Invalid Proof Studio User Color");
                }

                Context.SaveChanges();

            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error occured while saving the user photo", ex);
            }
            return RedirectToAction("Photo");
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePassword model)
        {
            try
            {
                GMGColorDAL.User objUser = UserBL.GetUserById(LoggedUser.ID, Context);
                var modelErrors = new Dictionary<string, string>();

                if (objUser.Password != GMGColorDAL.User.GetNewEncryptedPassword(model.OldPassword, Context))
                {
                    modelErrors.Add("objChangePassword.OldPassword", Resources.Resources.reqInvalidOldPassword);
                    TempData["modelErrors"] = modelErrors;
                }
                else if (model.NewPassword != model.ConfirmPassword)
                {
                    modelErrors.Add("", Resources.Resources.reqPasswordDoesNotMatch);
                    TempData["modelErrors"] = modelErrors;
                }
                else if (model.OldPassword == model.ConfirmPassword)
                {
                    modelErrors.Add("", Resources.Resources.reqPasswordShouldNotBeOldPassword);
                    TempData["modelErrors"] = modelErrors;
                }
                else
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(model.NewPassword, Context);
                        objUser.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));

                        Context.SaveChanges();
                        ts.Complete();

                        return RedirectToAction("Logout", "Auth");
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Password");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveNotifications")]
        public ActionResult Notifications(NotificationsModel model)
        {
            try
            {
                int customPresetId = GMGColorDAL.Preset.GetPreset(GMGColorDAL.Preset.PresetType.Custom, Context).ID;

                GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(LoggedUser.ID, Context);

                List<NotificationsModel.EventGroup.EventType> lst = new List<NotificationsModel.EventGroup.EventType>();

                model.EventGroups.ForEach(o => lst.AddRange(o.EventTypes));

                List<UserCustomPresetEventTypeValue> alreadyCheckedOptions =
                    (from ucpetv in Context.UserCustomPresetEventTypeValues
                     where ucpetv.User == this.LoggedUser.ID
                     select ucpetv).ToList();

                using (TransactionScope ts = new TransactionScope())
                {
                    objUser.Modifier = this.LoggedUser.ID;
                    objUser.IncludeMyOwnActivity = model.objUser.IncludeMyOwnActivity;
                    objUser.NotificationFrequency = model.objUser.NotificationFrequency;
                    objUser.Preset = model.objUser.Preset;

                    if (GMGColorDAL.Preset.GetPreset(model.objUser.Preset, Context) != GMGColorDAL.Preset.PresetType.Custom)
                    {
                        foreach (var item in alreadyCheckedOptions)
                        {
                            DALUtils.Delete<UserCustomPresetEventTypeValue>(Context, item);
                        }
                    }
                    else
                    {
                        if (!lst.Any(t => t.IsChecked == true))
                        {
                            Dictionary<string, string> modelErrors = new Dictionary<string, string>();
                            modelErrors.Add("", Resources.Resources.lblAtleastOneEvent);
                            TempData["modelErrors"] = modelErrors;

                            Dictionary<string, int> dicNotificationPara = new Dictionary<string, int>();
                            dicNotificationPara.Add(PresetKey, model.objUser.Preset);
                            dicNotificationPara.Add(NotificationFrequencyKey, model.objUser.NotificationFrequency);
                            dicNotificationPara.Add(IncludeMyOwnActivityKey, model.objUser.IncludeMyOwnActivity ? 1 : 0);

                            TempData[NotificationsKey] = dicNotificationPara;

                            return RedirectToAction("Notifications", "PersonalSettings");
                        }

                        foreach (NotificationsModel.EventGroup.EventType eventType in lst)
                        {
                            int rolePresetEventTypeId =
                                (from rpet in Context.RolePresetEventTypes
                                 where rpet.Preset == model.objUser.Preset && rpet.EventType == eventType.ID
                                 select rpet.ID).FirstOrDefault();

                            bool existsInDatabase = (from ucpetv in Context.UserCustomPresetEventTypeValues
                                                     where
                                                         ucpetv.RolePresetEventType == rolePresetEventTypeId &&
                                                         ucpetv.User == this.LoggedUser.ID
                                                     select ucpetv.ID).Any();

                            if (eventType.IsChecked && !existsInDatabase)
                            {
                                UserCustomPresetEventTypeValue item = new UserCustomPresetEventTypeValue();
                                item.User = this.LoggedUser.ID;
                                item.RolePresetEventType = rolePresetEventTypeId;
                                item.Value = eventType.IsChecked;

                                item.CreatedBy = item.ModifiedBy = LoggedUser.ID;
                                item.CreatedDate = item.ModifiedDate = DateTime.UtcNow;

                                Context.UserCustomPresetEventTypeValues.Add(item);
                            }
                            else if (!eventType.IsChecked && existsInDatabase)
                            {
                                UserCustomPresetEventTypeValue item = (from ucpetv in Context.UserCustomPresetEventTypeValues
                                                                       where
                                                                           ucpetv.User == this.LoggedUser.ID &&
                                                                           ucpetv.RolePresetEventType == rolePresetEventTypeId
                                                                       select ucpetv).FirstOrDefault();
                                if (item != null)
                                {
                                    DALUtils.Delete<UserCustomPresetEventTypeValue>(Context, item);
                                }
                            }
                        }
                    }

                    SettingsBL.MarkNotificationAsDeleted(LoggedAccount.ID, Context, userPersSetting: LoggedUser.ID);
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "PersonalSettings --> Notification", ex);
            }

            return RedirectToAction("Notifications", "PersonalSettings");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "GetEvents")]
        public ActionResult GetEventTypes(NotificationsModel model)
        {
            Dictionary<string, int> dicNotificationPara = new Dictionary<string, int>();
            dicNotificationPara.Add(PresetKey, model.objUser.Preset);
            dicNotificationPara.Add(NotificationFrequencyKey, model.objUser.NotificationFrequency);
            dicNotificationPara.Add(IncludeMyOwnActivityKey, model.objUser.IncludeMyOwnActivity ? 1 : 0);

            TempData[NotificationsKey] = dicNotificationPara;

            return RedirectToAction("Notifications", "PersonalSettings");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddNewOutOfOffice")]
        public ActionResult AddOrEditOutOfOffice(AddNewOutOfOffice model)
        {
            try
            {
                if (!CheckIsValidHtml(model.Description))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                if (model.ID > 0)
                {
                    string dateFormat = LoggedAccount.DateFormat1.Pattern;
                    var startDate = DateTime.ParseExact(model.StartDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                    var endDate = DateTime.ParseExact(model.EndDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);

                    
                    var utcStartDate = GMGColorFormatData.SetUserTimeToUTC(startDate, LoggedAccount.TimeZone);
                    endDate = endDate.AddSeconds(86399);
                    var utcEndDate = GMGColorFormatData.SetUserTimeToUTC(endDate, LoggedAccount.TimeZone);

                     
                    var existingDates = (from o in Context.OutOfOffices
                             where o.Owner == LoggedUser.ID && ((o.StartDate == startDate) || (o.StartDate == endDate)
                                                                 || (o.EndDate == startDate) || (o.EndDate == endDate)) && !(o.ID == model.ID)
                             select true).FirstOrDefault();

                    if (existingDates == false)
                    {

                        var obj = (from o in Context.OutOfOffices where o.ID == model.ID select o).FirstOrDefault();
                        obj.StartDate = utcStartDate;
                        obj.EndDate = utcEndDate;
                        obj.Description = model.Description;
                        obj.SubstituteUser = model.UserId;
                        Context.SaveChanges();
                    }
                    else
                    {
                        var AddNewOutOfOffice = new AddNewOutOfOffice();
                      
                        AddNewOutOfOffice.StartDate = model.StartDate;
                        AddNewOutOfOffice.EndDate = model.EndDate;
                        AddNewOutOfOffice.SubstituteUser = UserBL.GetUsersByAccount(LoggedUser, LoggedAccount, LoggedUserCollaborateRole, Context);
                        AddNewOutOfOffice.Description = model.Description;
                        AddNewOutOfOffice.UserId = model.UserId;
                        AddNewOutOfOffice.ID = model.ID;

                        DateTime toDate = DateTime.Now;
                        DateTime fromDate = DateTime.Now;
                        ViewBag.startDateYear = fromDate.Year;
                        ViewBag.startDateMonth = fromDate.Month;
                        ViewBag.startDateDay = fromDate.Day;
                        ViewBag.endDateYear = toDate.Year;
                        ViewBag.endDateMonth = toDate.Month;
                        ViewBag.endDateDay = toDate.Day;
                        ViewBag.MinStartDate = DateTime.Now;
                        ViewBag.error = Resources.Resources.lblCannotAddOnTheseDays;

                        return View("AddNewOutOfOffice", AddNewOutOfOffice);
                    }
                }
                else
                {
                    string dateFormat = LoggedAccount.DateFormat1.Pattern;
                    var startDate = DateTime.ParseExact(model.StartDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                    var endDate = DateTime.ParseExact(model.EndDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);

                    var utcStartDate = GMGColorFormatData.SetUserTimeToUTC(startDate, LoggedAccount.TimeZone);
                    endDate = endDate.AddSeconds(86399);
                    var utcEndDate = GMGColorFormatData.SetUserTimeToUTC(endDate, LoggedAccount.TimeZone);

                    var existingDates = (from o in Context.OutOfOffices
                             where o.Owner == LoggedUser.ID && ((o.StartDate == startDate) || (o.StartDate == endDate)
                                                                 || (o.EndDate == startDate) || (o.EndDate == endDate))
                             select true).FirstOrDefault();


                    if (existingDates == false)
                    {
                        var obj = new OutOfOffice();
                        obj.Owner = LoggedUser.ID;
                        obj.SubstituteUser = model.UserId;
                        obj.StartDate = utcStartDate;
                        obj.EndDate = utcEndDate;
                        obj.Description = model.Description;

                        Context.OutOfOffices.Add(obj);
                        Context.SaveChanges();
                        UserBL.OutOfOfficeNotification(model, LoggedUser, Context);
                    }
                    else
                    {

                        var AddNewOutOfOffice = new AddNewOutOfOffice();
                     
                        AddNewOutOfOffice.StartDate = model.StartDate;
                        AddNewOutOfOffice.EndDate = model.EndDate;
                        AddNewOutOfOffice.SubstituteUser = UserBL.GetUsersByAccount(LoggedUser, LoggedAccount, LoggedUserCollaborateRole, Context);
                        AddNewOutOfOffice.Description = model.Description;
                        AddNewOutOfOffice.UserId = model.UserId;


                        DateTime toDate = DateTime.Now;
                        DateTime fromDate = DateTime.Now;
                        ViewBag.startDateYear = fromDate.Year;
                        ViewBag.startDateMonth = fromDate.Month;
                        ViewBag.startDateDay = fromDate.Day;
                        ViewBag.endDateYear = toDate.Year;
                        ViewBag.endDateMonth = toDate.Month;
                        ViewBag.endDateDay = toDate.Day;
                        ViewBag.MinStartDate = DateTime.Now;
                        ViewBag.error = Resources.Resources.lblCannotAddOnTheseDays;

                        return View("AddNewOutOfOffice", AddNewOutOfOffice);
                    }
                }
            
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("OutOfOffice");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteOutOfOffice")]
        public ActionResult DeleteOutOfOffice(int? selectedOutOfOfficeId)
        {
            try
            {
                if (!selectedOutOfOfficeId.HasValue)
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                var outOfOffice = Context.OutOfOffices.FirstOrDefault(o => o.ID == selectedOutOfOfficeId.Value);
                if (outOfOffice != null)
                {
                    Context.OutOfOffices.Remove(outOfOffice);
                }
                Context.SaveChanges();


            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "PersonalSettingsController.DeleteOutOfOffice : Deletet OutOfOffice failed. {0}", ex.Message);
            }
            return RedirectToAction("OutOfOffice");
        }

        //[HttpPost]
        //[MultiButton(MatchFormKey = "action", MatchFormValue = "EditOutOfOffice")]
        //public ActionResult EditOutOfOffice(int? outOfOfficeId)
        //{
        //        var model = new AddNewOutOfOffice();
        //        var outOfOfficeDetails = UserBL.GetOutOfOfficeDetails((int)outOfOfficeId, Context);
        //        model.StartDate = outOfOfficeDetails.FromDate;
        //        model.EndDate = outOfOfficeDetails.ToDate;
        //        model.SubstituteUser = UserBL.GetUsersByAccount(LoggedUser.ID, Context);
        //        model.Description = outOfOfficeDetails.Description;
        //        model.UserId = outOfOfficeDetails.SubstituteUserId;
        //        model.ID = (int)outOfOfficeId;
        //        DateTime toDate = DateTime.Now;
        //        DateTime fromDate = DateTime.Now;
        //        ViewBag.startDateYear = fromDate.Year;
        //        ViewBag.startDateMonth = fromDate.Month;
        //        ViewBag.startDateDay = fromDate.Day;
        //        ViewBag.endDateYear = toDate.Year;
        //        ViewBag.endDateMonth = toDate.Month;
        //        ViewBag.endDateDay = toDate.Day;
        //        string datePattern = LoggedAccount.DateFormat1.Pattern;

        //        return View("AddNewOutOfOffice", model);
        //    }

        #endregion
    }
}
