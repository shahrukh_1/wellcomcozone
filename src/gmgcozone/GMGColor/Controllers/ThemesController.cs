﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorDAL.CustomModels;
using GMGColorBusinessLogic;
using GMGColorDAL;


namespace GMGColor.Controllers
{
    public class ThemesController : BaseController
    {
        public ThemesController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        /// <summary>
        /// Gets the color schema for the logged account
        /// </summary>
        /// <returns>ThemeSchema</returns>
        [HttpGet]
        public ActionResult GetLoggedUserColorSchema()
        {
            try
            {
                var loggedUserColorSchema = new List<ThemeColorSchema>();
                if (LoggedUser.ID == 0)
                {
                    loggedUserColorSchema = ThemeBL.GetAccountGlobalColorScheme(LoggedAccount.ID, Context);
                }
                else
                {
                    loggedUserColorSchema = ThemeBL.GetLoggedUserThemeColorSchemeToDisplay(LoggedUser.ID, Context);
                }
                return Json(loggedUserColorSchema, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error in GetLoggedAccountColorSchema action", ex.Message);
                return RedirectToAction("Index", "Error");
            }
        }

    }
}
