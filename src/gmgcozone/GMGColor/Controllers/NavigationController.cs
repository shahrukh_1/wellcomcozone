﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Windows;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using System.Web.Caching;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;

namespace GMGColor.Controllers
{
    public class NavigationController : BaseController
    {
        public NavigationController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        #region Fields

        private string _parentMenuKey;

        #endregion

        #region Enums

        public enum NavigationType
        {
            /// Main Navigaion
            Primary,
            /// Page Navigaion
            Secondary
        }

        #endregion

        #region Properties

        private int ParentMenuItem
        {
            get
            {
                int id = 0;
                if (TempData["Navigaionn_PM_ID"] != null && !String.IsNullOrEmpty(TempData["Navigaionn_PM_ID"].ToString()))
                {
                    id = int.Parse(TempData["Navigaionn_PM_ID"].ToString().Trim());
                }
                return id;
            }
        }

        private string ParentMenuKey
        {
            get
            {
                if (_parentMenuKey == null)
                {
                    object key = TempData["Nav_PM_Key"];
                    _parentMenuKey =  key != null ? key.ToString() : String.Empty;
                }

                return _parentMenuKey;
            }
        }

        private int ChildMenuItemID
        {
            get
            {
                int id = 0;
                if (TempData["Navigaionn_CM_ID"] != null && !String.IsNullOrEmpty(TempData["Navigaionn_CM_ID"].ToString()))
                {
                    id = int.Parse(TempData["Navigaionn_CM_ID"].ToString().Trim());
                }
                return id;
            }
        }

        #endregion

        #region Methods

        [ChildActionOnly]
        public PartialViewResult Navigation()
        {
            return PopulateNavigation(NavigationType.Primary);
        }

        [ChildActionOnly]
        public PartialViewResult SecondaryNavigation(SecondaryNavigationTypeEnum secondaryNavType = SecondaryNavigationTypeEnum.AccountSettings)
        {
            return PopulateNavigation(NavigationType.Secondary, secondaryNavType);
        }

        private PartialViewResult PopulateNavigation(NavigationType navType, SecondaryNavigationTypeEnum secondaryNavType = SecondaryNavigationTypeEnum.AccountSettings)
        {
            /// [IsTopNav] = true & [IsSubNav] = false - If menu item is a top one 
            /// [IsTopNav] = true & [IsSubNav] = true - If menu item is a top one and popualte under account name 
            /// [IsTopNav] = false & [IsSubNav] = false - If menu item is a top one and popualte under user name
            /// [IsTopNav] = false & [IsSubNav] = false - If menu item is a lie on the page like edit account

            switch (navType)
            {
                default:
                case NavigationType.Primary:
                {
                    NavigationModel model = new NavigationModel(LoggedAccount, LoggedAccountType);
                    model.LoggedUser = this.LoggedUser;
                    model.ParentMenuItemID = this.ParentMenuItem;
                    model.ParentMenuKey = ParentMenuKey;
                    model.UpdateFlags();
                    model.ChildMenuItemID = this.ChildMenuItemID;
                    model.HomePageUrl = UrlLandingPage();

                    if (!model.IsPersonalSettingsApp)
                    {
                            /// In here [IsAlignedLeft]
                            /// [IsAlignedLeft] = true - Is align to left
                            /// [IsAlignedLeft] = false - Is align to right

                            List<GMGColorDAL.UserMenuItemRoleView> AuthorizedTopMenuItems;

                            if (Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex] != null ?
                                Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex].ToString() == "true" : false)
                            {
                                    AuthorizedTopMenuItems =
                                (from o in this.AuthorizedControllerActions
                                 where
                                     o.IsVisible == true && o.Parent == 0 && o.IsSubNav == false && o.IsTopNav == true &&
                                     o.IsAdminAppOwned == false
                                 orderby o.Position
                                 select o).ToList();
                            }
                            else
                            {
                                AuthorizedTopMenuItems =
                            (from o in this.AuthorizedControllerActions
                             where
                                 o.IsVisible == true && o.Parent == 0 && o.IsSubNav == false && o.IsTopNav == true &&
                                 o.IsAdminAppOwned == model.IsAdminApp
                             orderby o.Position
                             select o).ToList();
                            }
                            // [IsTopNav] = true & [IsSubNav] = false - If menu item is a top one
                            model.TopLeftMenuItems = AuthorizedTopMenuItems.Where(m => m.IsAlignedLeft == true).ToList();

                        model.SettingsMenuItems = AuthorizedTopMenuItems.Where(m => m.IsAlignedLeft == false).ToList();
                    }
                    else
                    {
                        model.TopLeftMenuItems = model.SettingsMenuItems = new List<UserMenuItemRoleView>();
                    }

                    bool hasAccessToHomePage = !LoggedAccount.DisableLandingPage;

                    if (!hasAccessToHomePage)
                    {
                        model.TopLeftMenuItems.RemoveAll(o => o.Key == "HOME");
                    }
                    
                    List<GMGColorDAL.UserMenuItemRoleView> AuthorizedSubMenuItems =
                        (from o in this.AuthorizedControllerActions
                            where o.IsVisible == true && o.IsSubNav == true &&
                                    (o.IsAdminAppOwned == null || o.IsAdminAppOwned == false)
                            orderby o.IsAlignedLeft descending, o.Position
                            select o).ToList();

                    model.MenuItemsUnderAccount = AuthorizedSubMenuItems.Where(m => m.IsTopNav == true).ToList();

                    // for non Admin Administrator remove setting button in Dashbord        
                    if (LoggedUserAdminRole != Role.RoleName.AccountAdministrator)
                    {
                        model.MenuItemsUnderAccount.RemoveAll(t => t.IsTopNav == true);
                    }
                    
                    // for non Sys Admin account
                    if (LoggedAccount.Parent.GetValueOrDefault() > 0 &&
                        LoggedAccount.NeedSubscriptionPlan &&
                        (LoggedAccountSubscriptionDetails.HasCollaborateSubscriptionPlan ||
                        LoggedAccountSubscriptionDetails.HasDeliverSubscriptionPlan || 
                        LoggedAccount.IsFileTransferEnabled
                        ))
                    {
                        // add Collaborate and Deliver pages if the user is not on the home page or on account settings page or on a application page
                        if (!GMGColorDAL.MenuItem.MenusForHome.Union(GMGColorDAL.MenuItem.MenusForSiteSettings)
                                    .Union(GMGColorDAL.MenuItem.MenusForApplication)
                                    .ToArray().Contains(ParentMenuKey))
                        {
                            // if any items exists in the collection then add divider and then the menus
                            if (model.MenuItemsUnderAccount.Any())
                            {
                                model.MenuItemsUnderAccount.Add(new GMGColorDAL.UserMenuItemRoleView()
                                                                {
                                                                    Key =
                                                                        GMGColorDAL
                                                                        .MenuItem
                                                                        .Divider
                                                                });
                            }

                            model.MenuItemsUnderAccount.AddRange((from o in this.AuthorizedControllerActions
                                where
                                    ((o.Key == MenuItem.MenuForCollaborateApplication &&
                                        LoggedAccountSubscriptionDetails.HasCollaborateSubscriptionPlan) ||
                                        (o.Key == MenuItem.MenuForDeliverApplication &&
                                        LoggedAccountSubscriptionDetails.HasDeliverSubscriptionPlan) || (o.Key == MenuItem.MenuForFileTransferApplication &&
                                        LoggedAccount.IsFileTransferEnabled))
                                                                  orderby o.Position
                                select o));
                        }
                    }
                    // if user is on administration site then remove the 'Administration' menu
                    if (GMGColorDAL.MenuItem.MenusForAdmin.Contains(ParentMenuKey))
                    {
                        model.MenuItemsUnderAccount.RemoveAll(
                            o => GMGColorDAL.MenuItem.MenusForAdmin.Contains(o.Key));
                    }
                    // if user is on site settings then relive the menu
                    if (GMGColorDAL.MenuItem.MenusForSiteSettings.Contains(ParentMenuKey))
                    {
                        model.MenuItemsUnderAccount.RemoveAll(
                            o => GMGColorDAL.MenuItem.MenusForSiteSettings.Contains(o.Key));
                    }

                
                    model.MenuItemsUnderUser =
                        AuthorizedSubMenuItems.Where(m => m.Parent == 0 && m.IsTopNav == false)
                            .Distinct(new GMGColorDAL.UserMenuItemRoleView.UserMenuItemRoleViewEqualityComparer())
                            .ToList();

                    ViewBag.LoggedUserBrandingHeaderLogo = LoggedUserBrandingHeaderLogo;

                    return PartialView( ViewNameByBrowserAgent("Navigation"), model );
                }
                case NavigationType.Secondary:
                    {
                        SecondaryNavigationModel model = new SecondaryNavigationModel() {Type = secondaryNavType};
                        try
                        {
                            model.Menus = GetUserSecondaryNavigation(model.Type);
                            SetActiveMenu(model.Menus, ChildMenuItemID);
                        }
                        catch(ExceptionBL exbl)
                        {
                            GMGColorLogging.log.Error(exbl.Message, exbl);
                        }
                        catch(Exception ex)
                        {
                            GMGColorLogging.log.Error(ex.Message, ex);
                        }

                        return PartialView("SecondaryNavigation", model);
                    }
            }
        }

        #endregion
    }
}