﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using AppModule = GMGColorDAL.AppModule;

namespace GMGColor.Controllers
{
    public class ApplicationSettingsController : BaseController
    {
        public ApplicationSettingsController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        // GET: ApplicationSettings
        public ActionResult Index()
        {
            try
            { 
                var model =  ThemeBL.GetApprovalTypeColorScheme(Context);
                return View("Index", model);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UpdateTheme(List<ApprovalTypeModel> model)
        {
            try
            {
                ThemeBL.UpdateApprovalTypeColorScheme(model, Context);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("ApplicationSettings.UpdateTheme : Exception details : " + ex.Message, ex);
            }

            return RedirectToAction("Index");
        }
    }
}