﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using AppModule = GMGColorDAL.AppModule;

namespace GMGColor.Controllers
{
    public class PlansController : BaseController
    {
        public PlansController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        #region GET Actions

        [HttpGet]
        public ActionResult Index(PlanViewType? viewType)
        {
            TempData["selectedBillingPlan"] = null;

            var isOwnedView = LoggedAccountType == AccountType.Type.GMGColor || (LoggedAccountType == AccountType.Type.Client &&
                              viewType.GetValueOrDefault() == PlanViewType.Owned);

            var plansModel = new Plans(LoggedAccountType, LoggedAccount, Context, !isOwnedView);

            if (Session["BPID"] == null)
            {
                ViewBag.ModelDialog = null;
            }
            else
            {
                int planType = int.Parse(Session["BPID"].ToString());
                Session["BPID"] = null;

                ModelState.Clear();
                ViewBag.ModelDialog = "modalDialogAddEditBillingPlanType";

                if (planType > 0)
                {
                    plansModel.objBillingPlanType = DALUtils.GetObject<GMGColorDAL.BillingPlanType>(planType, Context);
                }
            }

            if (LoggedAccountType != AccountType.Type.GMGColor)
            {
                ViewBag.ParentAccountType = GMGColorDAL.AccountType.GetAccountType(plansModel.objLoggedAccount.Parent.GetValueOrDefault(),
                        Context);
            }

            if (isOwnedView)
            {
                ViewBag.OwnedBillingPlans = PlansBL.GetAccountOwnedBillingPlans(LoggedAccount, LoggedAccountType, Context);
                ViewBag.AvailableModules = PlansBL.GetAccountAvailableModules(LoggedAccountType, LoggedAccount.ID, Context);

            }
            else
            {
                ViewBag.ChangePlanMessages = GMGColorDAL.DALUtils.SearchObjects<GMGColorDAL.AccountPricePlanChangedMessage>(o => o.ChildAccount == LoggedAccount.ID, Context);
            }
           
            return View(isOwnedView ? "OwnedPlans" : "AttachedPlans", plansModel);
        }

        [HttpGet]
        public ActionResult AddEditBillingPlan()
        {
            Session["BPID"] = null;

            var plansModel = PlansBL.PopulateBillingPlan(TempData["selectedBillingPlan"], LoggedAccount, LoggedAccountType, Context);

            ViewBag.CollaborateModuleId = AppModule.GetModuleId(GMG.CoZone.Common.AppModule.Collaborate, Context);
            ViewBag.DeliverModuleId = AppModule.GetModuleId(GMG.CoZone.Common.AppModule.Deliver, Context);           
            
            return View("AddEditBillingPlan", plansModel);
        }

        [HttpGet]
        public JsonResult IsPlanTypeNameUnique(Plans objPlan)
        {
            if (PlansBL.CheckIfPlanTypeNameIsUnique(objPlan.objBillingPlanType.Name, Context) && objPlan.objBillingPlanType.ID == 0)
            {
                return Json(Resources.Resources.lblPlanTypeNameWarning, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        /*[HttpGet]
        public ActionResult CurrencyRates()
        {
            Session["BPID"] = null;
            Plans plansModel = new Plans(this.LoggedAccount.ID, this.LoggedAccount);
            return View("CurrencyRates", plansModel);
        }*/

        [HttpGet]
        public ActionResult AddEditNewCollaborateBillingPlan() {
            Session["BPID"] = null;
            var plansModel = PlansBL.PopulateNewCollaborateBillingPlan(TempData["selectedBillingPlan"], LoggedAccount, LoggedAccountType, Context);
            return View("AddEditNewCollaborateBillingPlan", plansModel);
        }
        #endregion

        #region POST Actions

        [HttpPost]
        public ActionResult SaveNewCollaborateBillingPlan(NewCollaborateBillingPlanViewModel planModel)
        {
            if (LoggedAccountType == AccountType.Type.Client)
            {
                if (!ValidateNewCollaborateBillingPlan(planModel))
                {
                    planModel.BillingPlanTypes = (from bpt in Context.BillingPlanTypes
                                                  join apm in Context.AppModules on bpt.AppModule equals apm.ID
                                                  where bpt.Account == LoggedAccount.ID &&
                                                        apm.Key == (int)GMG.CoZone.Common.AppModule.Collaborate
                                                  select new BillingPlanTypeViewModel()
                                                  {
                                                      ID = bpt.ID,
                                                      Name = bpt.Name,
                                                      MediaService = bpt.MediaService,
                                                      EnableMediaTools = bpt.EnableMediaTools,
                                                      EnablePrePressTools = bpt.EnablePrePressTools
                                                  }).ToList();


                    planModel.ProductType = Resources.Resources.lblCollaborateModuleName;
                    planModel.Currency = LoggedAccount.Currency.Symbol;

                    return View("AddEditNewCollaborateBillingPlan", planModel);
                }
            }

            if (planModel.ID > 0)
            {
                return UpdateNewCollaborateBillingPlan(planModel);
            }
            else
            {
                if (PlansBL.ProcessNewCollaborateBillingPlanForm(planModel, LoggedAccount, LoggedAccountType, Context))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Error");
                }
            }
        }

        [HttpPost]
        public ActionResult UpdateNewCollaborateBillingPlan(NewCollaborateBillingPlanViewModel planModel)
        {
            PlansBL.UpdateNewCollaborateBillingPlan(planModel, LoggedAccount, Context);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditNewCollaborateBillingPlan")]
        public ActionResult AddEditNewCollaborateBillingPlan(Plans plansModel, string SelectedID) 
        {
            TempData["selectedBillingPlan"] = SelectedID;
            return RedirectToAction("AddEditNewCollaborateBillingPlan");
        }
        
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RemoveNotification")]
        public ActionResult RemoveNotification()
        {
            try
            {
                using (var ts = new TransactionScope())
                {
                    List<GMGColorDAL.AccountPricePlanChangedMessage> lstAccountPricePlanChangedMessage =
                        DALUtils.SearchObjects<GMGColorDAL.AccountPricePlanChangedMessage>(
                            o => o.ChildAccount == this.LoggedAccount.ID, Context).ToList();

                    if (lstAccountPricePlanChangedMessage.Any())
                    {
                        Context.AccountPricePlanChangedMessages.RemoveRange(lstAccountPricePlanChangedMessage);

                        if ((DALUtils.SearchObjects<GMGColorDAL.AccountPricePlanChangedMessage>(o => o.ID > 0, Context).Count - lstAccountPricePlanChangedMessage.Count) == 0)
                        {
                            List<GMGColorDAL.ChangedBillingPlan> lstChangedBillingPlan = DALUtils.SearchObjects<GMGColorDAL.ChangedBillingPlan>(o => o.Account == this.LoggedAccount.Parent, Context);
                            if (lstChangedBillingPlan != null && lstChangedBillingPlan.Count > 0)
                            {
                                Context.ChangedBillingPlans.RemoveRange(lstChangedBillingPlan);
                            }
                        }
                        Context.SaveChanges();
                    }
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID,"Error occured while deleting notification", ex);
            }

            return RedirectToAction("Index", new { viewType = PlanViewType.Attached });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditPlan")]
        public ActionResult AddEditBillingPlan(Plans plansModel, string SelectedID)
        {
            TempData["selectedBillingPlan"] = SelectedID;
            return RedirectToAction("AddEditBillingPlan");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditPlanType")]
        public ActionResult AddEditPlanType(Plans plansModel, string SelectedID)
        {
            Session["BPID"] = int.Parse(SelectedID);
            return RedirectToAction("Index");
        }

        /*[HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddEditCurrencyRate")]
        public ActionResult AddEditCurrencyRate(Plans plansModel)
        {
            int currencyRate = plansModel.objCurrencyRate.ID;
            ModelState.Clear();
            ViewBag.ModelDialog = "modalDialogAddEditCurrencyRate";

            plansModel = new Plans(this.LoggedAccount.ID, this.LoggedAccount);
            if (currencyRate > 0)
            {
                plansModel.objCurrencyRate = new CurrencyRateBO();
                plansModel.objCurrencyRate.ID = currencyRate;
                plansModel.objCurrencyRate.GetObject();
            }

            return View("Index", plansModel);
        }*/

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveBillingPlan")]
        public ActionResult SaveBillingPlan(Plans plansModel)
        {
            bool succes = ProcessBillingPlanForm(plansModel, LoggedAccountType);
            if (succes)
            {
                return RedirectToAction("Index");
            }
            else
            {
                Plans model = new Plans(LoggedAccountType, LoggedAccount, Context, false)
                {
                    objBillingPlan =
                    {
                        ID = plansModel.objBillingPlan.ID,
                        IsFixed = plansModel.objBillingPlan.IsFixed,
                        Name = plansModel.objBillingPlan.Name,
                        Price = plansModel.objBillingPlan.Price,
                        Proofs = plansModel.objBillingPlan.Proofs,
                        ColorProofConnections = plansModel.objBillingPlan.ColorProofConnections,
                        IsDemo = plansModel.objBillingPlan.IsDemo,
                        Type = plansModel.objBillingPlan.Type
                    },
                    objBillingPlanType =
                    {
                        ID = plansModel.objBillingPlanType.ID,
                        AppModule = plansModel.objBillingPlanType.AppModule
                    }
                };
                ViewBag.CollaborateModuleId = AppModule.GetModuleId(GMG.CoZone.Common.AppModule.Collaborate, Context);
                ViewBag.DeliverModuleId = AppModule.GetModuleId(GMG.CoZone.Common.AppModule.Deliver, Context);               

                return View("AddEditBillingPlan", model);
            }
        }

        [HttpPost]
        public ActionResult SaveBillingPlanType(Plans plansModel)
        {
            if (PlansBL.CheckIfPlanTypeNameIsUnique(plansModel.objBillingPlanType.Name, Context) && plansModel.objBillingPlanType.ID == 0)
            {
                ViewBag.ModelDialog = "#modalDialogAddEditBillingPlanType";
                return RedirectToAction("Index");
            }
            this.ProcessBillingPlanTypeForm(plansModel, Context);
            return RedirectToAction("Index");
            //return View("Index", (new Plans(this.LoggedAccount.ID, this.LoggedAccount)));
        }

        /*[HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveCurrencyRate")]
        public ActionResult SaveCurrencyRate(Plans plansModel)
        {
            this.ProcessCurrencyRateForm(plansModel);
            return View("Index", (new Plans(this.LoggedAccount.ID, this.LoggedAccount)));
        }*/

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Price")]
        public ActionResult EditBillingPrice(Plans model)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.AttachedAccountBillingPlan objAttachedAccountBillingPlan =
                        this.LoggedAccount.AttachedAccountBillingPlans.Single(
                            o => o.BillingPlan == model.objEditPlanPrice.SelectedPrice);

                    if (objAttachedAccountBillingPlan.NewPrice != model.objEditPlanPrice.NewPrice)
                    {
                        GMGColorDAL.BillingPlan objBillingPlan =
                            DALUtils.GetObject<GMGColorDAL.BillingPlan>(objAttachedAccountBillingPlan.BillingPlan, Context);

                        objAttachedAccountBillingPlan.IsAppliedCurrentRate = false;
                        objAttachedAccountBillingPlan.NewPrice = model.objEditPlanPrice.NewPrice;

                        if (!objAttachedAccountBillingPlan.IsModifiedProofPrice)
                        {
                            objAttachedAccountBillingPlan.NewProofPrice = (objBillingPlan.Proofs != null && objBillingPlan.Proofs.Value > 0) ? (objAttachedAccountBillingPlan.NewPrice / objBillingPlan.Proofs.Value) : 0;
                        }

                        this.UpdateBillingPlanChangedMessage(objBillingPlan.ID, objAttachedAccountBillingPlan.ID);

                        Context.SaveChanges();
                        ts.Complete();
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID,"Error occured while saving billing plan price, PlansController --> EditBillingPrice(Plans model)", ex);
            }

            return RedirectToAction("Index", "Plans", new { viewType = PlanViewType.Attached });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "CostPerProof")]
        public ActionResult EditBillingProofPrice(Plans model)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.AttachedAccountBillingPlan objAttachedAccountBillingPlan =
                        this.LoggedAccount.AttachedAccountBillingPlans.Single(
                            o => o.BillingPlan == model.objEditPlanPrice.SelectedProof);

                    objAttachedAccountBillingPlan.IsModifiedProofPrice = true;
                    objAttachedAccountBillingPlan.NewProofPrice = model.objEditPlanPrice.NewProof;

                    this.UpdateBillingPlanChangedMessage(objAttachedAccountBillingPlan.BillingPlan, objAttachedAccountBillingPlan.ID);

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("Index", "Plans", new { viewType = PlanViewType.Attached });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeletePlanType")]
        public ActionResult DeletePlanType(string SelectedID)
        {
            int planType = int.Parse(SelectedID);

            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.BillingPlanType objBillingPlanType =
                        DALUtils.GetObject<GMGColorDAL.BillingPlanType>(planType, Context);

                    foreach (GMGColorDAL.BillingPlan item in objBillingPlanType.BillingPlans.ToList())
                    {
                        PlansBL.DeleteBillingPlan(item.ID, Context);
                    }

                    DALUtils.Delete<GMGColorDAL.BillingPlanType>(Context, objBillingPlanType);

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID,"Plans --> DeletePlanType", ex);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeletePlan")]
        public ActionResult DeletePlan(string SelectedID)
        {
            int plan = int.Parse(SelectedID);

            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                   PlansBL.DeleteBillingPlan(plan, Context);

                   Context.SaveChanges();
                   ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID,"Plans --> DeletePlan", ex);
            }

            return RedirectToAction("Index");
        }

        public JsonResult GetProductTypes(int? AppModule)
        {
            List<GMGColorDAL.BillingPlanType> planList =
                PlansBL.GetBillingPlanTypesByModule(
                    LoggedAccountType == AccountType.Type.GMGColor ? null : (int?) LoggedAccount.ID,
                    AppModule.GetValueOrDefault(), Context);

            return
                Json(
                    new
                    {
                        planTypes =
                        planList.Select(
                        t => new { ID = t.ID, Name = t.Name })
                    });
        }
 
        #endregion

        #region Methods

        private bool ProcessBillingPlanForm(Plans model, AccountType.Type accType)
        {
            bool succes = true;
            if (model.objBillingPlan.Price > GMGColorDAL.BillingPlan.MaxPrice)
            {
                succes = false;
                ModelState.AddModelError("objBillingPlan.Price", Resources.Resources.revBillingPlanPriceValidationMsg);
            }
            if (model.objBillingPlan.Proofs > GMGColorDAL.BillingPlan.MaxProofs)
            {
                succes = false;
                ModelState.AddModelError("objBillingPlan.Proofs", Resources.Resources.revBillingPlanProofsValidationMsg);
            }
            if (accType == AccountType.Type.Client)
            {
                var appModuleKey = (from appModule in Context.AppModules
                                    join bpt in Context.BillingPlanTypes on appModule.ID equals bpt.AppModule
                                    where bpt.ID == model.objBillingPlan.Type
                                    select appModule.Key).FirstOrDefault();

                var currentUsedProofs = (from bp in Context.BillingPlans
                                        join bpt in Context.BillingPlanTypes on bp.Type equals bpt.ID
                                        join apm in Context.AppModules on bpt.AppModule equals apm.ID
                                        where apm.Key == appModuleKey && bp.Account == LoggedAccount.ID && bp.ID != model.objBillingPlan.ID
                                        select bp.Proofs).FirstOrDefault();
              
                int? currentUsedWorkstations = 0;

                BillingPlan currentSubscriptionPlan = null;
                if (appModuleKey == (int)GMG.CoZone.Common.AppModule.Collaborate)
                {
                    currentSubscriptionPlan = LoggedAccount.CollaborateBillingPlan(Context);
                  
                }
                else if (appModuleKey == (int) GMG.CoZone.Common.AppModule.Deliver)
                {
                    currentSubscriptionPlan = LoggedAccount.DeliverBillingPlan(Context);
                }               

                if (currentSubscriptionPlan != null && !currentSubscriptionPlan.IsFixed)
                {
                    if (model.objBillingPlan.IsFixed)
                    {
                        succes = false;
                        ModelState.AddModelError("", Resources.Resources.errPlanCantBeFixed);
                    }
                    else if (model.objBillingPlan.Proofs + currentUsedProofs > currentSubscriptionPlan.Proofs)
                    {
                        succes = false;
                        ModelState.AddModelError("objBillingPlan.Proofs", String.Format(Resources.Resources.errPlanLimitReach, currentSubscriptionPlan.Proofs - currentUsedProofs));
                    }
                }
            }

            if (succes)
            {
                try
                {
                    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                    {
                        GMGColorDAL.BillingPlan objBillingPlan;
                        if (model.objBillingPlan.ID > 0)
                        {
                            objBillingPlan = DALUtils.GetObject<GMGColorDAL.BillingPlan>(model.objBillingPlan.ID, Context);

                            bool isPriceChanged = (objBillingPlan.Price != model.objBillingPlan.Price);
                            bool isProofChanged = (objBillingPlan.Proofs != model.objBillingPlan.Proofs);
                            bool isCPConnectionsChanged = (objBillingPlan.ColorProofConnections != model.objBillingPlan.ColorProofConnections);

                            if (isPriceChanged || isProofChanged || isCPConnectionsChanged)
                            {
                                this.UpdateBillingPlanChangedMessage(objBillingPlan.ID, null, true);
                            }
                        }
                        else
                        {
                            objBillingPlan = new BillingPlan
                            {
                                Account = LoggedAccountType != AccountType.Type.GMGColor
                                    ? (int?) LoggedAccount.ID
                                    : null
                            };
                        }

                        objBillingPlan.Type = model.objBillingPlan.Type;
                        objBillingPlan.Name = model.objBillingPlan.Name;
                        objBillingPlan.Price = model.objBillingPlan.Price;
                        objBillingPlan.IsFixed = model.objBillingPlan.IsFixed;
                        objBillingPlan.IsDemo = model.objBillingPlan.IsDemo;
                        

                        if (model.objBillingPlan.ColorProofConnections != null)
                        {
                            objBillingPlan.ColorProofConnections = model.objBillingPlan.ColorProofConnections;
                        }

                        if (!model.objBillingPlan.IsFixed && model.objBillingPlan.Proofs != null)
                        {
                            objBillingPlan.Proofs = model.objBillingPlan.Proofs;
                        }
                        else
                        {
                            objBillingPlan.Proofs = 0;
                        }

                        if (objBillingPlan.ID == 0)
                        {
                            Context.BillingPlans.Add(objBillingPlan);
                        }

                        Context.SaveChanges();
                        ts.Complete();
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                                                    "PlansController.SaveChanges.ProcessBillingPlanForm : Saving Billing Plan Failed. {0}",
                                                    ex.Message);
                    throw ex;
                }
            }
            return succes;
        }

        private bool ValidateNewCollaborateBillingPlan(NewCollaborateBillingPlanViewModel planModel)
        {
            var success = true;
            BillingPlan currentSubscriptionPlan =  LoggedAccount.CollaborateBillingPlan(Context);

            if (currentSubscriptionPlan != null && !currentSubscriptionPlan.IsFixed && PlansBL.IsNewCollaborateBillingPlan(currentSubscriptionPlan))
            {
                if (planModel.isFixedPlan)
                {
                    success = false;
                    ModelState.AddModelError("", Resources.Resources.errPlanCantBeFixed);
                }
                else
                {
                    var currentLimits = (from bp in Context.BillingPlans
                                         join bpt in Context.BillingPlanTypes on bp.Type equals bpt.ID
                                         join apm in Context.AppModules on bpt.AppModule equals apm.ID
                                         where apm.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && bp.Account == LoggedAccount.ID && bp.ID != planModel.ID
                                         select new
                                         {
                                             currentUsers = bp.MaxUsers,
                                             currentStorage = bp.MaxStorageInGb
                                         });

                    var totalUsers = currentLimits.Sum(t => t.currentUsers) ?? 0;
                    var totalStorage = currentLimits.Sum(t => t.currentStorage) ?? 0;

                    if (totalUsers + planModel.MaxUsers > currentSubscriptionPlan.MaxUsers)
                    {
                        success = false;
                        ModelState.AddModelError("MaxUsers", String.Format(Resources.Resources.errPlanLimitReach, currentSubscriptionPlan.MaxUsers - totalUsers));
                    }
                    if (totalStorage + planModel.MaxStorageInGb > currentSubscriptionPlan.MaxStorageInGb)
                    {
                        success = false;
                        ModelState.AddModelError("MaxStorageInGb", String.Format(Resources.Resources.errPlanLimitReach, currentSubscriptionPlan.MaxStorageInGb - totalStorage));
                    }
                }
            }

            return success;
        }

        private void ProcessBillingPlanTypeForm(Plans model, DbContextBL context)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.BillingPlanType objBillingPlanType;
                    if (model.objBillingPlanType.ID > 0)
                    {
                        objBillingPlanType = DALUtils.GetObject<GMGColorDAL.BillingPlanType>(model.objBillingPlanType.ID, Context);
                    }
                    else
                    {
                        objBillingPlanType = new BillingPlanType
                        {
                            AppModule = model.objBillingPlanType.AppModule,
                            Account = LoggedAccountType != AccountType.Type.GMGColor
                                ? (int?) LoggedAccount.ID
                                : null
                        };
                        context.BillingPlanTypes.Add(objBillingPlanType);
                    }
                    objBillingPlanType.Name = model.objBillingPlanType.Name;
                    objBillingPlanType.Key = objBillingPlanType.Name.Replace(" ", string.Empty).Trim();
                    objBillingPlanType.MediaService = model.objBillingPlanType.MediaService;

                    objBillingPlanType.EnablePrePressTools = model.objBillingPlanType.EnablePrePressTools;
                    objBillingPlanType.EnableMediaTools = model.objBillingPlanType.EnableMediaTools;

                    //Permissions for softproofing were changed so users subscribed to this plan type must relogin
                    if (objBillingPlanType.ID > 0)
                    {
                        List<GMGColorDAL.User> attachedUsersToPlanType = UserBL.GetUserForPlanType(objBillingPlanType.ID, Context);
                        foreach (var user in attachedUsersToPlanType)
                        {
                            user.NeedReLogin = true;
                        }
                    }
                    context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.InfoFormat(this.LoggedAccount.ID,"PlansController.SaveChanges.ProcessBillingPlanTypeForm : Saving Billing Plan Type Failed. {0}", ex.Message);
            }
            finally
            {
            }
        }

        private void UpdateBillingPlanChangedMessage(int? billingPlan, int? attachedAccountBillingPlan, bool isChangedByOwner = false)
        {
            var billingPlanValue = billingPlan.GetValueOrDefault();
            var attachedAccountBillingPlanValue = attachedAccountBillingPlan.GetValueOrDefault();
            GMGColorDAL.ChangedBillingPlan objCBP =
                DALUtils.SearchObjects<GMGColorDAL.ChangedBillingPlan>(
                    o =>
                    o.Account == this.LoggedAccount.ID &&
                    o.BillingPlan == billingPlanValue &&
                    attachedAccountBillingPlan == attachedAccountBillingPlanValue, Context).SingleOrDefault();

            // TODO - check if switch is needed and refactor
            if (objCBP == null)
            {
                GMGColorDAL.ChangedBillingPlan objChangedBillingPlan = new GMGColorDAL.ChangedBillingPlan();

                var childAccounts = LoggedAccount.GetChilds(Context);

                if (childAccounts.Count > 0)
                {
                    bool isHQ = LoggedAccountType == GMGColorDAL.AccountType.Type.GMGColor;

                    objChangedBillingPlan.Account = this.LoggedAccount.ID;
                    objChangedBillingPlan.BillingPlan = isHQ || isChangedByOwner ? billingPlan.Value : (int?) null;
                    objChangedBillingPlan.AttachedAccountBillingPlan = !isHQ && attachedAccountBillingPlan != null
                        ? attachedAccountBillingPlan.Value
                        : (int?) null;
                    Context.ChangedBillingPlans.Add(objChangedBillingPlan);

                    if (billingPlan.HasValue)
                    {
                        foreach (var childAccount in childAccounts)
                        {
                            // add messages only for child accounts that has the changed billing plan attached
                            if (childAccount.AttachedAccountBillingPlans.Any(a => a.BillingPlan1.ID == billingPlan.Value))
                            {
                                var objAPPCM = new AccountPricePlanChangedMessage
                                {
                                    ChildAccount = childAccount.ID
                                };
                                objChangedBillingPlan.AccountPricePlanChangedMessages.Add(objAPPCM);
                                Context.AccountPricePlanChangedMessages.Add(objAPPCM);
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (GMGColorDAL.Account childAccount in objCBP.Account1.GetChilds(Context).Where(o => o.AccountType != GMGColorDAL.AccountType.GetAccountType(GMGColorDAL.AccountType.Type.Subscriber, Context).ID).ToList())
                {
                    // add messages only for child accounts that has the changed billing plan attached
                    if (childAccount.AttachedAccountBillingPlans.Any(a => a.BillingPlan == objCBP.BillingPlan))
                    {
                        var objAPPCM = new GMGColorDAL.AccountPricePlanChangedMessage
                        {
                            ChildAccount = childAccount.ID
                        };
                        objCBP.AccountPricePlanChangedMessages.Add(objAPPCM);
                        Context.AccountPricePlanChangedMessages.Add(objAPPCM);
                    }
                }
            }
        }

        /*private void ProcessCurrencyRateForm(Plans model)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    CurrencyRateBO objCurrencyRate = new CurrencyRateBO();
                    if (model.objCurrencyRate.ID > 0)
                    {
                        objCurrencyRate.ID = model.objCurrencyRate.ID;
                        objCurrencyRate.GetObject();
                    }
                    else
                    {
                        objCurrencyRate.Creator = this.LoggedUser.ID;
                        objCurrencyRate.CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("g"));
                    }
                    objCurrencyRate.Modifier = this.LoggedUser.ID;
                    objCurrencyRate.ModifiedDate = Convert.ToDateTime(DateTime.Now.ToString("g"));

                    objCurrencyRate.Currency = model.objCurrencyRate.Currency;
                    objCurrencyRate.Rate = model.objCurrencyRate.Rate;

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.InfoFormat(this.LoggedAccount.ID,"PlansController.SaveChanges.ProcessCurrencyRateForm : Saving Currency Rate Failed. " + ex.Message);
            }
        }*/

        #endregion
    }
}
