﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.UI;
using GMG.CoZone.Common;
using GMGColor.Common;
using System.Xml;
using GMGColor.Hubs;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorBusinessLogic;
using GMGColorDAL.CustomModels.Api;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using GMGColorNotificationService;
using GMGColorNotificationService.Notifications.Approvals;
using Microsoft.AspNet.SignalR;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Newtonsoft.Json;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Utils = GMGColor.Common.Utils;
using System.IO;
using System.Net;
using GMGColor.ActionFilters;
using ApprovalAnnotationStatusChangeLog = GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog;
using PageSize = PdfSharp.PageSize;
using PdfDocument = PdfSharp.Pdf.PdfDocument;
using PdfPage = PdfSharp.Pdf.PdfPage;
using GMG.WebHelpers.Job;
using AppModule = GMG.CoZone.Common.AppModule;
using Approval = GMGColorDAL.Approval;
using Document = MigraDoc.DocumentObjectModel.Document;
using Role = GMGColorDAL.Role;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Collaborate.Interfaces.Annotation;
using GMG.CoZone.Collaborate.UI;
using GMG.CoZone.Common.Interfaces;
using System.Web;
using AutoMapper;
using GMG.CoZone.FileTransfer.DomainModels;
using GMG.CoZone.FileTransfer.Interfaces;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace GMGColor.Controllers
{
    public class FileTransferController : BaseController
    {
        private readonly IFileTransferService _fileTransferService;

        private readonly IApprovalService _approvalService;

        private readonly IAnnotationsInReportService _annotationsInReportService;
        private readonly IAWSSNSService _snsService;
        private readonly IGMGConfiguration _gmgConfiguration;

        public FileTransferController(IFileTransferService fileTransferService, IApprovalService approvalService,
                                   IAWSSNSService snsService,
                                   IAnnotationsInReportService annotationsInReportService,
                                   IGMGConfiguration gmgConfiguration,
                                   IDownloadService downlaodService,
                                   IMapper mapper) : base(downlaodService, mapper)
        {
            _fileTransferService = fileTransferService;
            _approvalService = approvalService;
            _annotationsInReportService = annotationsInReportService;
            _snsService = snsService;
            _gmgConfiguration = gmgConfiguration;
        }

        #region Constants
        public const string FileNameColumn = "FileName";
        public const string CreatorColumn = "Creator";
        public const string RecipientsColumn = "Recipients";
        public const string OptionalMessageColumn = "OptionalMessage";
        public const string SentDateColumn = "SentDate";
        private const string SearchFileTransfers = "SearchFileTransfers";

        #endregion

        #region Properties

        protected int FilterType
        {
            get
            {
                int c = 1;
                try
                {
                    if (Session["FilterType"] != null)
                    {
                        c = Convert.ToInt32(Session["FilterType"]);
                    }
                }
                catch (Exception)
                {
                    Session["FilterType"] = c;
                }
                Session["FilterType"] = c;
                return c;
            }
            set
            {
                Session["FilterType"] = value;
            }
        }

        protected int SortColumn
        {
            get
            {
                int c = 1;
                try
                {
                    if (Session["SortColumn"] != null)
                    {
                        c = Convert.ToInt32(Session["SortColumn"]);
                    }
                }
                catch (Exception)
                {
                    Session["SortColumn"] = c;
                }
                Session["SortColumn"] = c;
                return c;
            }
            set
            {
                Session["SortColumn"] = value;
            }
        }

        protected int PageSize
        {
            get
            {
                if (UserDeviceInfo.DeviceType == GMGColorEnums.BrowserType.Tablet)
                {
                    return 12;
                }
                else
                {
                    int c = 10;
                    try
                    {
                        if (Session["PageSize_" + LoggedUser.ID] != null)
                        {
                            c = Convert.ToInt32(Session["PageSize_" + LoggedUser.ID]);
                        }
                        else
                        {
                            c = JobBL.GetNumberOfRowsPerPage(LoggedUser.ID, null, GMGColorConstants.RememberNumberOfFilesShown, GMGColorConstants.DefaultNumberOfFilesShown, Context);
                        }
                    }
                    catch (Exception)
                    {
                        Session["PageSize_" + LoggedUser.ID] = c;
                    }
                    Session["PageSize_" + LoggedUser.ID] = c;
                    return c;
                }
            }
            set
            {
                Session["PageSize"] = value;
            }
        }

        protected int CurrentTabCount
        {
            get
            {
                int c = 0;
                try
                {
                    if (Session["CurrentTabCount"] != null)
                    {
                        c = Convert.ToInt32(Session["CurrentTabCount"]);
                    }
                }
                catch (Exception)
                {
                    Session["CurrentTabCount"] = c;
                }
                Session["CurrentTabCount"] = c;
                return c;
            }
            set
            {
                Session["CurrentTabCount"] = value;
            }
        }

        protected int PopulateID
        {
            get
            {
                int c = -1;
                try
                {
                    if (Session[Constants.ApprovalPopulateId] != null)
                    {
                        c = Convert.ToInt32(Session[Constants.ApprovalPopulateId]);
                    }
                }
                catch (Exception)
                {
                    Session[Constants.ApprovalPopulateId] = c;
                }
                Session[Constants.ApprovalPopulateId] = c;
                return c;
            }
            set
            {
                Session[Constants.ApprovalPopulateId] = value;
            }
        }

        protected bool FiltersSorting
        {
            get
            {
                bool c = true;
                try
                {
                    if (Session["FiltersSorting"] != null)
                    {
                        c = (bool)(Session["FiltersSorting"]);
                    }
                }
                catch (Exception)
                {
                    Session["FiltersSorting"] = c;
                }
                Session["FiltersSorting"] = c;
                return c;
            }
            set
            {
                Session["FiltersSorting"] = value;
            }
        }

        protected bool SortingOrder
        {
            get
            {
                bool c = true;
                try
                {
                    if (Session["SortingOrder"] != null)
                    {
                        c = (bool)(Session["SortingOrder"]);
                    }
                }
                catch (Exception)
                {
                    Session["SortingOrder"] = c;
                }
                Session["SortingOrder"] = c;
                return c;
            }
            set
            {
                Session["SortingOrder"] = value;
            }
        }

        protected bool SortingDirection
        {
            get
            {
                bool c = true;
                try
                {
                    if (Session["SortingDirection"] != null)
                    {
                        c = (bool)(Session["SortingDirection"]);
                    }
                }
                catch (Exception)
                {
                    Session["SortingDirection"] = c;
                }
                Session["SortingDirection"] = c;
                return c;
            }
            set
            {
                Session["SortingDirection"] = value;
            }
        }

        protected int Status
        {
            get
            {
                int status = 0;
                try
                {
                    if (Session[Constants.ApprovalStatus] != null)
                    {
                        status = (int)(Session[Constants.ApprovalStatus]);
                    }
                }
                catch (Exception)
                {
                    Session[Constants.ApprovalStatus] = status;
                }
                Session[Constants.ApprovalStatus] = status;
                return status;
            }
            set { Session[Constants.ApprovalStatus] = value; }
        }

        protected string SearchText
        {
            get
            {
                string s = string.Empty;
                try
                {
                    if (TempData["SearchText"] != null)
                    {
                        s = TempData["SearchText"].ToString();
                    }
                }
                catch (Exception)
                {
                    TempData["SearchText"] = s;
                }
                TempData["SearchText"] = s;
                return s;
            }
            set
            {
                TempData["SearchText"] = value;
            }
        }

        protected bool ViewAll
        {
            get
            {
                bool v = false;
                try
                {
                    if (Session[Constants.ViewAllApprovals] != null)
                    {
                        v = Convert.ToBoolean(Session[Constants.ViewAllApprovals]);
                    }

                }
                catch (Exception)
                {
                    Session[Constants.ViewAllApprovals] = v;
                }

                return v;
            }
            set
            {
                Session[Constants.ViewAllApprovals] = value;
            }
        }

        #endregion

        // GET: FileTransfer
        [HttpGet]
        public ActionResult NewFileTransfer(string guid, int? jobId)
        {
            NewApprovalModel model;

            if (LoggedUserFileTransferRole != Role.RoleName.AccountManager && LoggedUserFileTransferRole != Role.RoleName.AccountAdministrator)
            {
                return RedirectToAction("Index");
            }

            AccountFileTypeFilter fileTypeFilter;
            if (TempData["NewApprovalModel"] != null)
            {
                model = (NewApprovalModel)TempData["NewApprovalModel"];
                model.objLoggedAccount = LoggedAccount;
                fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(LoggedUser.ID, Context);
                TempData["NewApprovalModel"] = null;

                if (TempData["ModelErrors"] != null)
                {
                    var dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                    TempData["ModelErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }
            }
            else
            {
                model = new NewApprovalModel(this.LoggedAccount, LoggedUser.ID, this.LoggedUserTempFolderPath, Context, out fileTypeFilter);
            }

            if (!string.IsNullOrEmpty(guid))
            {
                jobId = JobBL.GetJobIdByAppGuid(guid, Context);
            }

            model.Folder = (Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] == null) ? 0 : int.Parse(Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()].ToString().Trim());
            model.Job = model.Job > 0 ? model.Job : (jobId == null) ? 0 : jobId.Value;

            ApprovalBL.PopulateNewApprovalModel(model, LoggedUser.ID, Context);

            if (model.PhaseCollaboratorsModel != null)
            {
                model.PhasesCollaborators = JsonConvert.SerializeObject(model.PhaseCollaboratorsModel);
            }

            // Get existing foldes before created new ones
            if (Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] == null)
            {
                var aFolders = DALUtils.SearchObjects<GMGColorDAL.Folder>(o => o.Creator == this.LoggedUser.ID, Context).Select(o => o.ID).ToArray();
                if (aFolders.Length == 0)
                    aFolders = new int[] { this.LoggedUser.ID };

                Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] = aFolders;
            }

            

            bool applyBatchUploadSettings = fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval;
            ViewBag.UseSettingsFromPreviousVerion = model.Job == 0 &&
                                                    applyBatchUploadSettings &&
                                                    fileTypeFilter.UploadSettingsFilter.UseSettingsFromPreviousVersion;

            //ViewBag.FileTypesAllowed = ApprovalBL.GetAllowedUploadFileTypes(fileTypeFilter);
            ViewBag.AppyApprovalBatchSettings = applyBatchUploadSettings;

            return View("NewFileTransfer", model);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "NewFileTransfer")]
        public ActionResult NewFileTransfer(NewApprovalModel model, string hdnFileName_1)
        {

            List<FileTransferIndividualItem> items = new List<FileTransferIndividualItem>();
           
            List<int> lstTmp = Request.Form.AllKeys.Where(o => o.Contains("ApprovalTitle_")).Select(o => int.Parse(o.Split('_').Last())).ToList();
            var phasesCollaborators = new ApprovalWorkflowPhasesInfo();
            var fileGuid="";
            var fileName = "";
            var names = "";

            foreach (int key in lstTmp)
            {
                try
                {
                    if (model.IsUploadApproval)
                    {
                        var approvaltitles = Request.Form.GetValues("ApprovalTitle_" + key);
                        if (approvaltitles.Any())
                        {
                            // Split in case files are concatenated in formcollection because delete was used in file uploader and another file was uploaded causing more files to be in the same key
                            var approvalFileNames = Request.Form.GetValues("ApprovalFileName_" + key);
                            var approvalSizes = Request.Form.GetValues("ApprovalSize_" + key);
                            var approvalGuids = Request.Form.GetValues("FileGuid_" + key);


                            //fileGuid = approvalGuids[0] ?? string.Empty;
                            //fileName= approvalFileNames[0] ?? string.Empty;

                            for (int i = 0; i < approvaltitles.Length; i++)
                            {
                                //if (string.IsNullOrEmpty(approvaltitles[i]))
                                //{
                                //    model.IsUploadApproval = true;

                                //    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                                //    dicModelErrors.Add("fileTitleValidator", Resources.Resources.reqDeliverJobTtile);

                                //    return RedirectWithModelAndErrors(model, dicModelErrors);
                                //}

                                items.Add(new FileTransferIndividualItem()
                                {
                                    FileName = approvalFileNames[i] ?? string.Empty,
                                    FileTitle = approvaltitles[i] ?? string.Empty,
                                    FileGuid = approvalGuids[i] ?? string.Empty
                                });

                                GMGColorLogging.log.Info($" DONE: items contains now  {items.Count}");
                            }


                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "FileATransferController.Index : Save New File Transfer Failed. {0}", ex.Message);
                    model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                    TempData["NewApprovalModel"] = model;

                    return RedirectToAction("NewApproval", "FileTransfer");
                }
            }

            #region Custom Validation

            if (!CheckIsValidHtml(model.OptionalMessage))
            {
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                return RedirectWithModelAndErrors(model, dicModelErrors);
            }

            if (String.IsNullOrEmpty(hdnFileName_1.Trim()))
            {
                model.IsUploadApproval = true;

                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                dicModelErrors.Add("fileValidator", Resources.Resources.reqApproval);

                return RedirectWithModelAndErrors(model, dicModelErrors);
            }


            #endregion

            //get internal users
            List<int> externalUsers = new List<int>();
            List<int> internalUsers = new List<int>();
            foreach (var item in model.SelectedUsersAndGroups)
            {
                if (item.IsChecked)
                {
                    if (!item.IsGroup)
                    {
                        if (item.IsExternal)
                        {
                            externalUsers.Add(item.ID);
                        }
                        else
                        {
                            internalUsers.Add(item.ID);
                        }
                    }
                }
            }

            //get approval title
            var approvalTitles = Request.Form.GetValues("ApprovalTitle_0");
            string title = approvalTitles[0] ?? string.Empty;


            //zip the individual items 
            if (items.Count > 1)
            {
                foreach(var item in items)
                {
                    names=String.Concat(names, item.FileName, ",");
                }

                fileGuid = Guid.NewGuid().ToString();
                fileName =Resources.Resources.lblFileTransfer + ".zip";
               
                    FileTransferBL.ZipTheFileTransfers(items, fileGuid, fileName, LoggedAccount, LoggedUser);
                
            }
            else
            {
                fileName = items[0].FileName;
                fileGuid = items[0].FileGuid;

            }

            GMGColorLogging.log.Info($" start saving the file transfer into the database");

            //Test SaveTransfer function:
            FileTransferModel fileTransferModel = new FileTransferModel()
            {
                AccountID = LoggedAccount.ID,
                FileName = fileName,
                FolderPath = "",
                Guid = fileGuid,
                Creator = LoggedUser.ID,
                CreatedDate = DateTime.UtcNow,
                Modifier = LoggedUser.ID,
                ModifiedDate = DateTime.UtcNow,
                Message = model.OptionalMessage,
                ExpirationDate = DateTime.UtcNow.AddDays(14),
                InternalUsers = internalUsers,
                ExternalUsers = externalUsers,
                ExternalEmails = model.ExternalEmails,
                IndividualFileNames = names
            };

            int fileTransferId = _fileTransferService.SaveTransfer(fileTransferModel);

            GMGColorLogging.log.Info($" save the file transfer into the database with the id : {fileTransferId}");

            var externalUsersIds = _fileTransferService.GetExternalUsersForTransfer(fileTransferId);


            ////----------

            ////create notifications


            FileTransfer fileTransfer = new FileTransfer()
            {
                ID = fileTransferId,
                AccountID = LoggedAccount.ID,
                FileName = fileName,
                FolderPath = "",
                Guid = fileTransferModel.Guid,
                Creator = LoggedUser.ID,
                CreatedDate = DateTime.UtcNow,
                Modifier = LoggedUser.ID,
                ModifiedDate = DateTime.UtcNow,
                Message = model.OptionalMessage,
                ExpirationDate = DateTime.UtcNow.AddDays(14)
            };


            //GMGColorLogging.log.Info($" start processing the transfer file: move it from temp to file transfer");
            if(items.Count==1)FileTransferBL.ProcessTheFileTransfer(fileTransfer, fileName, LoggedUser);

            FileTransferBL.SendNewFileTransferNotifications(fileTransfer, internalUsers, externalUsersIds, LoggedAccount, LoggedUser, model.OptionalMessage, Context);

            return RedirectToAction("Index", "FileTransfer");
        }





        private ActionResult RedirectWithModelAndErrors(NewApprovalModel model, Dictionary<string, string> dicModelErrors)
        {
            TempData["NewApprovalModel"] = model;
            TempData["ModelErrors"] = dicModelErrors;

            return RedirectToAction("Index", "FileTransfer");
        }

        // file transfer history


        [HttpGet]
        public ActionResult Index(int? page, string search,string searchSender, string sort, string sortdir, int? rowsPerPage)
        {
            if (rowsPerPage.HasValue && !WebGridNavigationModel.PagesCount.Contains(rowsPerPage.Value))
            {
                return RedirectToAction("Index", "FileTransfer");
            }

            ViewBag.FileDownload = BaseBL.PopulateFileDownloadModel(LoggedAccount.ID, LoggedUser.ID, AppModule.FileTransfer, Context);
            ViewBag.Page = page;
            ViewBag.Search = search;
            ViewBag.SearchSender = searchSender;
            ViewBag.Sort = sort;
            ViewBag.SortDir = sortdir;

            if (Session["PageSize_" + LoggedUser.ID] != null && rowsPerPage.GetValueOrDefault() == Convert.ToInt32(Session["PageSize_" + LoggedUser.ID]))
            {
                ViewBag.RowsPerPage = Convert.ToInt32(Session["PageSize_" + LoggedUser.ID]);
            }
            else
            {
                ViewBag.RowsPerPage = JobBL.GetNumberOfRowsPerPage(LoggedUser.ID, rowsPerPage, GMGColorConstants.RememberNumberOfFilesShown, GMGColorConstants.DefaultNumberOfFilesShown, Context);
                Session["PageSize_" + LoggedUser.ID] = ViewBag.RowsPerPage;
            }


            return View("Index");
        }

        [HttpGet]
        [Compress]
        public ActionResult GetFileTransfersHistory(int? page, string search,string searchSender, string sort, string sortdir, int? rowsPerPage)
        {
            try
            {
                int sortField = 0;
                switch (sort)
                {
                    case FileNameColumn: sortField = 1; break;
                    case CreatorColumn: sortField = 2; break;
                    case RecipientsColumn: sortField = 3; break;
                    case OptionalMessageColumn: sortField = 4; break;
                    default: sortField = 0; break;
                }
                bool sortAsc = (sortdir ?? string.Empty).ToLower() == "asc";

                if (!rowsPerPage.HasValue || rowsPerPage.GetValueOrDefault() <= 0)
                {
                    rowsPerPage = (int?)10;
                }

                if (page.HasValue && page.Value <= 0)
                    page = 1;



                FileTransferHistory.FileTransferHistoryModel model = new FileTransferHistory.FileTransferHistoryModel(this.LoggedAccount, this.LoggedUser.ID,
                                                                      this.LoggedUserTempFolderPath,
                                                                      page.HasValue ? page.Value : 1, sortField, sortAsc, Context, rowsPerPage.GetValueOrDefault(), search ?? String.Empty, searchSender ?? String.Empty)
                { SearchText = search ?? string.Empty , SearchSenderText = searchSender ?? String.Empty };

                int actualNrOfPages = model.TotalRows / model.RowsPerPage +
                                   (model.TotalRows % model.RowsPerPage == 0 ? 0 : 1);

                if (actualNrOfPages <= 0)
                    actualNrOfPages = 1;

                if (page > actualNrOfPages || page < 1)
                {
                    page = actualNrOfPages;
                    model = new FileTransferHistory.FileTransferHistoryModel(this.LoggedAccount, this.LoggedUser.ID,
                                                                      this.LoggedUserTempFolderPath,
                                                                      page.HasValue ? page.Value : 1, sortField, sortAsc, Context, rowsPerPage.GetValueOrDefault(), search ?? String.Empty, searchSender ?? String.Empty)
                    { SearchText = search ?? string.Empty, SearchSenderText = searchSender ?? String.Empty };
                }




                ViewBag.PageSize = model.RowsPerPage;
                var isAdministrator = (LoggedUserFileTransferRole == Role.RoleName.AccountAdministrator);
                ViewBag.Role = isAdministrator;

                return Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, "FileTransferHistory", LoggedUser.ID, model)
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in FileTransferController.GetFileTransfersHistory, Asynchronous Loading file transfer history grid method: {0}", ex.Message);

                return Json(
                       new
                       {
                           Status = 300,
                           Content = "Failed to Load File Transfer History Grid"
                       },
                       JsonRequestBehavior.AllowGet);
            }
        }


        //search in file transfer history
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchFileTransfers")]
        public ActionResult Search(FileTransferHistory.FileTransferHistoryModel model, string SearchText)
        {
           
            string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
            string searchSender = string.Empty;

            var str= s.Replace(" ", string.Empty);
            var isSender = Regex.IsMatch(str.ToLower(), "^(sender):");

            if (isSender)
            {
                string[] words = s.Split(':');
                searchSender = words[1].Trim();
                s = string.Empty;
            }
            
            TempData[SearchFileTransfers] = s;

            return RedirectToAction("Index", new { search = HttpUtility.HtmlEncode(s) , searchSender = HttpUtility.HtmlEncode(searchSender) });
        }


        [HttpPost]
        public ActionResult DeleteMultipleJobs(string deliverJobIds, int? PageNr, int? RowsPerPage, string SearchText)
        {

            try
            {
                if (!string.IsNullOrEmpty(deliverJobIds))
                {
                    var deliverJobs = new JavaScriptSerializer().Deserialize<List<int>>(deliverJobIds);
                    if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.FileTransfer>(deliverJobs, LoggedAccount.ID, Context))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }
                    foreach (var deliverJobId in deliverJobs)
                    {
                        _fileTransferService.DeleteFileTransfer(deliverJobId);
                    }
                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "FileController.DeleteMultipleJobs : error occured while deleting multiple file transfers. {0}", ex.Message);
            }


            return RedirectToAction("Index", GMGColorDAL.CustomModels.Common.GetCurrentQueryStrings(Request.QueryString,
                    new string[] { "page", "RowsPerPage", "search" },
                    new object[] { PageNr, RowsPerPage, SearchText }
                    ));
        }

        //folder transfer
        [HttpGet]
        public ActionResult NewFolderTransfer(string guid, int? folderId)
        {
            NewFileTransferFolderModel model;
            var currentfolderId = (folderId == null) ? 0 : folderId.Value;

            if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>(currentfolderId, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            if (LoggedUserFileTransferRole != Role.RoleName.AccountAdministrator && LoggedUserFileTransferRole != Role.RoleName.AccountManager )
            {
                return RedirectToAction("Index");
            }

            AccountFileTypeFilter fileTypeFilter;
            if (TempData["NewFileTransferFolderModel"] != null)
            {
                model = (NewFileTransferFolderModel)TempData["NewFileTransferFolderModel"];
                model.objLoggedAccount = LoggedAccount;
                fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(LoggedUser.ID, Context);
                TempData["NewFileTransferFolderModel"] = null;

                if (TempData["ModelErrors"] != null)
                {
                    var dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                    TempData["ModelErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }
            }
            else
            {
                model = new NewFileTransferFolderModel(currentfolderId,this.LoggedAccount, LoggedUser.ID, this.LoggedUserTempFolderPath, Context, out fileTypeFilter);
            }


            FileTransferBL.PopulateFileTransferFolderModel(model, LoggedUser.ID, Context);


            

            bool applyBatchUploadSettings = fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval;


            ViewBag.FileTypesAllowed = ApprovalBL.GetAllowedUploadFileTypes(fileTypeFilter);
            ViewBag.AppyApprovalBatchSettings = applyBatchUploadSettings;

            return View("NewFolderTransfer", model);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "NewFolderTransfer")]
        public ActionResult NewFolderTransfer(NewFileTransferFolderModel model)
        {

            #region Custom Validation

            if (!CheckIsValidHtml(model.OptionalMessage))
            {
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                TempData["NewFileTransferApprovalModel"] = model;
                TempData["ModelErrors"] = dicModelErrors;

                return RedirectToAction("Index", "FileTransfer");


            }

            #endregion

            //get internal users
            List<int> externalUsers = new List<int>();
            List<int> internalUsers = new List<int>();
            foreach (var item in model.SelectedUsersAndGroups)
            {
                if (item.IsChecked)
                {
                    if (!item.IsGroup)
                    {
                        if (item.IsExternal)
                        {
                            externalUsers.Add(item.ID);
                        }
                        else
                        {
                            internalUsers.Add(item.ID);
                        }
                    }
                }
            }



            var folderIds = new List<int>
            {
                model.Folder
            };
            var subfolderIds = FolderBL.GetSubfolderIdsForFolder(model.Folder,Context);
            if (subfolderIds != null)
            {
                folderIds.AddRange(subfolderIds);
            }
            var approvalsDetails = _fileTransferService.GetDownloadPathsForApprovalsFromFolders(folderIds, LoggedUser.ID, true);
            if (approvalsDetails.Count < 1)
            {
                return RedirectToAction("Index", "FileTransfer");
            }
            
            //get folder
            var objFolder = FolderBL.GetFolderByID(model.Folder, LoggedUser.ID, Context);

            //get folder name
            var fileName = string.Concat(objFolder.Name,".zip");

            model.OriginalFileName = fileName;

            //set a new file transfer Guid
            var fileGuid = Guid.NewGuid().ToString();

            GMGColorLogging.log.Info($" start saving the file transfer into the database");

            //Test SaveTransfer function:
            FileTransferModel fileTransferModel = new FileTransferModel()
            {
                AccountID = LoggedAccount.ID,
                FileName = fileName,
                FolderPath = "",
                Guid = fileGuid,
                Creator = LoggedUser.ID,
                CreatedDate = DateTime.UtcNow,
                Modifier = LoggedUser.ID,
                ModifiedDate = DateTime.UtcNow,
                Message = model.OptionalMessage,
                ExpirationDate = DateTime.UtcNow.AddDays(14),
                InternalUsers = internalUsers,
                ExternalUsers = externalUsers,
                ExternalEmails = model.ExternalEmails

            };

            int fileTransferId = _fileTransferService.SaveTransfer(fileTransferModel);

            GMGColorLogging.log.Info($" save the file transfer into the database with the id : {fileTransferId}");

            var externalUsersIds = _fileTransferService.GetExternalUsersForTransfer(fileTransferId);


            ////----------

            ////create notifications


            FileTransfer fileTransfer = new FileTransfer()
            {
                ID = fileTransferId,
                AccountID = LoggedAccount.ID,
                FileName = fileName,
                FolderPath = "",
                Guid = fileTransferModel.Guid,
                Creator = LoggedUser.ID,
                CreatedDate = DateTime.UtcNow,
                Modifier = LoggedUser.ID,
                ModifiedDate = DateTime.UtcNow,
                Message = model.OptionalMessage,
                ExpirationDate = DateTime.UtcNow.AddDays(14)
            };

            //save the folder transfer to S3 

            GMGColorLogging.log.Info($" start processing the transfer folder: copy it from approvals to file transfers");

            FileTransferBL.ProcessTheFolderTransfer(fileTransfer, approvalsDetails, LoggedUser,LoggedAccount);

           FileTransferBL.SendNewFileTransferNotifications(fileTransfer, internalUsers, externalUsersIds, LoggedAccount, LoggedUser, model.OptionalMessage, Context);

            return RedirectToAction("Index", "FileTransfer");
        }


        //approval file transfer
        // GET: FileTransfer
        [HttpGet]
        public ActionResult NewApprovalTransfer(string guid, int? approvalId)
        {
            NewFileTransferApprovalModel model;
            if (approvalId != null)
            {
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalId.Value, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
            }
            if (LoggedUserFileTransferRole != Role.RoleName.AccountAdministrator && LoggedUserFileTransferRole != Role.RoleName.AccountManager)
            {
                return RedirectToAction("Index");
            }

            AccountFileTypeFilter fileTypeFilter;
            if (TempData["NewApprovalModel"] != null)
            {
                model = (NewFileTransferApprovalModel)TempData["NewApprovalModel"];
                model.objLoggedAccount = LoggedAccount;
                fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(LoggedUser.ID, Context);
                TempData["NewApprovalModel"] = null;

                if (TempData["ModelErrors"] != null)
                {
                    var dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                    TempData["ModelErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }
            }
            else
            {
                model = new NewFileTransferApprovalModel(this.LoggedAccount, LoggedUser.ID, this.LoggedUserTempFolderPath, Context, out fileTypeFilter);
            }

           

            model.Folder = (Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] == null) ? 0 : int.Parse(Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()].ToString().Trim());
            model.Approval = model.Approval > 0 ? model.Approval : (approvalId == null) ? 0 : approvalId.Value;

            FileTransferBL.PopulateFileTransferApprovalModel(model, LoggedUser.ID, Context);

          

            // Get existing foldes before created new ones
            if (Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] == null)
            {
                var aFolders = DALUtils.SearchObjects<GMGColorDAL.Folder>(o => o.Creator == this.LoggedUser.ID, Context).Select(o => o.ID).ToArray();
                if (aFolders.Length == 0)
                    aFolders = new int[] { this.LoggedUser.ID };

                Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] = aFolders;
            }

           

            bool applyBatchUploadSettings = fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval;
           

            ViewBag.FileTypesAllowed = ApprovalBL.GetAllowedUploadFileTypes(fileTypeFilter);
            ViewBag.AppyApprovalBatchSettings = applyBatchUploadSettings;

            return View("NewApprovalTransfer", model);
        }


        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "NewApprovalTransfer")]
        public ActionResult NewApprovalTransfer(NewFileTransferApprovalModel model)
        {

            #region Custom Validation

            if (!CheckIsValidHtml(model.OptionalMessage))
            {
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                TempData["NewFileTransferApprovalModel"] = model;
                TempData["ModelErrors"] = dicModelErrors;

                return RedirectToAction("Index", "FileTransfer");

                
            }
            
            #endregion

            //get internal users
            List<int> externalUsers = new List<int>();
            List<int> internalUsers = new List<int>();
            foreach (var item in model.SelectedUsersAndGroups)
            {
                if (item.IsChecked)
                {
                    if (!item.IsGroup)
                    {
                        if (item.IsExternal)
                        {
                            externalUsers.Add(item.ID);
                        }
                        else
                        {
                            internalUsers.Add(item.ID);
                        }
                    }
                }
            }


            //get approval
            var approval = ApprovalBL.GetApproval(model.Approval, Context);
            
            //get approval title
            var fileName = approval.FileName;

            //set a new file transfer Guid
            var fileGuid = Guid.NewGuid().ToString();

            GMGColorLogging.log.Info($" start saving the file transfer into the database");

            //Test SaveTransfer function:
            FileTransferModel fileTransferModel = new FileTransferModel()
            {
                AccountID = LoggedAccount.ID,
                FileName = fileName,
                FolderPath = "",
                Guid = fileGuid,
                Creator = LoggedUser.ID,
                CreatedDate = DateTime.UtcNow,
                Modifier = LoggedUser.ID,
                ModifiedDate = DateTime.UtcNow,
                Message = model.OptionalMessage,
                ExpirationDate = DateTime.UtcNow.AddDays(14),
                InternalUsers = internalUsers,
                ExternalUsers = externalUsers,
                ExternalEmails = model.ExternalEmails
                
            };

            int fileTransferId = _fileTransferService.SaveTransfer(fileTransferModel);

            GMGColorLogging.log.Info($" save the file transfer into the database with the id : {fileTransferId}");

            var externalUsersIds = _fileTransferService.GetExternalUsersForTransfer(fileTransferId);


            ////----------

            ////create notifications


            FileTransfer fileTransfer = new FileTransfer()
            {
                ID = fileTransferId,
                AccountID = LoggedAccount.ID,
                FileName = fileName,
                FolderPath = "",
                Guid = fileTransferModel.Guid,
                Creator = LoggedUser.ID,
                CreatedDate = DateTime.UtcNow,
                Modifier = LoggedUser.ID,
                ModifiedDate = DateTime.UtcNow,
                Message = model.OptionalMessage,
                ExpirationDate = DateTime.UtcNow.AddDays(14)
            };

            //save the new file transfer to S3 

            //GMGColorLogging.log.Info($" start processing the transfer file: move it from temp to file transfer");
          
            FileTransferBL.ProcessTheApprovalTransfer(fileTransfer, approval.Guid, LoggedUser);

            FileTransferBL.SendNewFileTransferNotifications(fileTransfer, internalUsers, externalUsersIds, LoggedAccount, LoggedUser, model.OptionalMessage, Context);

            return RedirectToAction("Index", "FileTransfer");
        }



        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult LoadUserActivity(int approval)
        {
            try
            {
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.FileTransfer>(approval, LoggedAccount.ID, Context))
                {
                    return Json(
                           new
                           {
                               Status = 403
                           },
                           JsonRequestBehavior.AllowGet);
                }
                SetUICulture(LoggedUser.ID);

                   FileTransferHistory.FileTransferDetailsModel versionModel =
                        FileTransferBL.PopulateFileTransferDetailsModel(approval, LoggedAccount, LoggedUser.Locale, Context);
                    versionModel.LoggedUserID = LoggedUser.ID;
                

                    return Json(
                        new
                        {
                            Status = 200,
                            Content =
                                RenderViewToString(this, ViewNameByBrowserAgent("UserActivity"), LoggedUser.ID,
                                    versionModel)
                        },
                        JsonRequestBehavior.AllowGet);
               
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "FileTransferController. LoadUserActivity Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested file transfer details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

    }
}