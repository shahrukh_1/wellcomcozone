﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.UI;
using GMG.CoZone.Common;
using GMGColor.Common;
using System.Xml;
using GMGColor.Hubs;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorBusinessLogic;
using Utils = GMGColor.Common.Utils;
using GMGColor.ActionFilters;
using GMGColorDAL.CustomModels.Api;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using GMGColorNotificationService;
using GMGColorNotificationService.Notifications.Approvals;
using Microsoft.AspNet.SignalR;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Newtonsoft.Json;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.IO;
using System.Net;
using ApprovalAnnotationStatusChangeLog = GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog;
using PageSize = PdfSharp.PageSize;
using PdfDocument = PdfSharp.Pdf.PdfDocument;
using PdfPage = PdfSharp.Pdf.PdfPage;
using GMG.WebHelpers.Job;
using AppModule = GMG.CoZone.Common.AppModule;
using Approval = GMGColorDAL.Approval;
using Document = MigraDoc.DocumentObjectModel.Document;
using Role = GMGColorDAL.Role;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Collaborate.Interfaces.Annotation;
using GMG.CoZone.Collaborate.UI;
using GMG.CoZone.Common.Interfaces;
using System.Web;
using AutoMapper;
using GMG.CoZone.FileTransfer.DomainModels;
using GMG.CoZone.FileTransfer.Interfaces;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace GMGColor.Controllers
{
    public class ProjectFoldersController : BaseController
    {
        private readonly IApprovalService _approvalService;
        private readonly IAnnotationsInReportService _annotationsInReportService;
        private readonly IAWSSNSService _snsService;
        private readonly IGMGConfiguration _gmgConfiguration;

        public ProjectFoldersController(IApprovalService approvalService,
                                   IAWSSNSService snsService,
                                   IAnnotationsInReportService annotationsInReportService,
                                   IGMGConfiguration gmgConfiguration,
                                   IDownloadService downlaodService,
                                   IMapper mapper) : base(downlaodService, mapper)
		{
            _approvalService = approvalService;
            _annotationsInReportService = annotationsInReportService;
            _snsService = snsService;
            _gmgConfiguration = gmgConfiguration;
        }

        #region Constants
        public const string ProjectFolderNameColumn = "Project Name";
        public const string CreatedDate = "Created Date";
        public const string ProjectOwner = "Project Owner";
        public const string TeamSize = "Team Size";
        public const string TotalJobs = "Total Jobs";
        public const string DueDate = "Due Date";
        public const string Status_ = "Status";
        public const string JobOverView = "Job Overview";

        private const string DateCreated = "DateCreated";
        private const string DateModified = "DateModified";
        private const string DueByDate = "DueDate";
        private const string ProjectStatus = "Status";
        private const string Name = "ProjectName";

        private const string All = "All";
        private const string NotAssigned = "NotAssigned";
        private const string Ongoing = "Ongoing";
        private const string Completed = "Completed";
        private const string Archived = "Archived";

        #endregion

        //public ProjectFoldersController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }


        #region Properties

        protected int FilterType
        {
            get
            {
                int c = 1;
                try
                {
                    if (Session["FilterType"] != null)
                    {
                        c = Convert.ToInt32(Session["FilterType"]);
                    }
                }
                catch (Exception)
                {
                    Session["FilterType"] = c;
                }
                Session["FilterType"] = c;
                return c;
            }
            set
            {
                Session["FilterType"] = value;
            }
        }

        protected int SortColumn
        {
            get
            {
                int c = 1;
                try
                {
                    if (Session["SortColumn"] != null)
                    {
                        c = Convert.ToInt32(Session["SortColumn"]);
                    }
                }
                catch (Exception)
                {
                    Session["SortColumn"] = c;
                }
                Session["SortColumn"] = c;
                return c;
            }
            set
            {
                Session["SortColumn"] = value;
            }
        }

        protected int PageSize
        {
            get
            {
                if (UserDeviceInfo.DeviceType == GMGColorEnums.BrowserType.Tablet)
                {
                    return 12;
                }
                else
                {
                    int c = 10;
                    try
                    {
                        if (Session["PageSize_" + LoggedUser.ID] != null)
                        {
                            c = Convert.ToInt32(Session["PageSize_" + LoggedUser.ID]);
                        }
                        else
                        {
                            c = JobBL.GetNumberOfRowsPerPage(LoggedUser.ID, null, GMGColorConstants.RememberNumberOfFilesShown, GMGColorConstants.DefaultNumberOfFilesShown, Context);
                        }
                    }
                    catch (Exception)
                    {
                        Session["PageSize_" + LoggedUser.ID] = c;
                    }
                    Session["PageSize_" + LoggedUser.ID] = c;
                    return c;
                }
            }
            set
            {
                Session["PageSize"] = value;
            }
        }

        protected int CurrentTabCount
        {
            get
            {
                int c = 0;
                try
                {
                    if (Session["CurrentTabCount"] != null)
                    {
                        c = Convert.ToInt32(Session["CurrentTabCount"]);
                    }
                }
                catch (Exception)
                {
                    Session["CurrentTabCount"] = c;
                }
                Session["CurrentTabCount"] = c;
                return c;
            }
            set
            {
                Session["CurrentTabCount"] = value;
            }
        }

        protected int PopulateID
        {
            get
            {
                int c = -1;
                try
                {
                    if (Session[Constants.ApprovalPopulateId] != null)
                    {
                        c = Convert.ToInt32(Session[Constants.ApprovalPopulateId]);
                    }
                }
                catch (Exception)
                {
                    Session[Constants.ApprovalPopulateId] = c;
                }
                Session[Constants.ApprovalPopulateId] = c;
                return c;
            }
            set
            {
                Session[Constants.ApprovalPopulateId] = value;
            }
        }

        protected bool FiltersSorting
        {
            get
            {
                bool c = true;
                try
                {
                    if (Session["FiltersSorting"] != null)
                    {
                        c = (bool)(Session["FiltersSorting"]);
                    }
                }
                catch (Exception)
                {
                    Session["FiltersSorting"] = c;
                }
                Session["FiltersSorting"] = c;
                return c;
            }
            set
            {
                Session["FiltersSorting"] = value;
            }
        }

        protected bool SortingOrder
        {
            get
            {
                bool c = true;
                try
                {
                    if (Session["SortingOrder"] != null)
                    {
                        c = (bool)(Session["SortingOrder"]);
                    }
                }
                catch (Exception)
                {
                    Session["SortingOrder"] = c;
                }
                Session["SortingOrder"] = c;
                return c;
            }
            set
            {
                Session["SortingOrder"] = value;
            }
        }

        protected bool SortingDirection
        {
            get
            {
                bool c = true;
                try
                {
                    if (Session["SortingDirection"] != null)
                    {
                        c = (bool)(Session["SortingDirection"]);
                    }
                }
                catch (Exception)
                {
                    Session["SortingDirection"] = c;
                }
                Session["SortingDirection"] = c;
                return c;
            }
            set
            {
                Session["SortingDirection"] = value;
            }
        }

        protected int Status
        {
            get
            {
                int status = 0;
                try
                {
                    if (Session[Constants.ApprovalStatus] != null)
                    {
                        status = (int)(Session[Constants.ApprovalStatus]);
                    }
                }
                catch (Exception)
                {
                    Session[Constants.ApprovalStatus] = status;
                }
                Session[Constants.ApprovalStatus] = status;
                return status;
            }
            set { Session[Constants.ApprovalStatus] = value; }
        }

        protected string SearchText
        {
            get
            {
                string s = string.Empty;
                try
                {
                    if (TempData["SearchText"] != null)
                    {
                        s = TempData["SearchText"].ToString();
                    }
                }
                catch (Exception)
                {
                    TempData["SearchText"] = s;
                }
                TempData["SearchText"] = s;
                return s;
            }
            set
            {
                TempData["SearchText"] = value;
            }
        }

        protected bool ViewAll
        {
            get
            {
                bool v = false;
                try
                {
                    if (Session[Constants.ViewAllApprovals] != null)
                    {
                        v = Convert.ToBoolean(Session[Constants.ViewAllApprovals]);
                    }

                }
                catch (Exception)
                {
                    Session[Constants.ViewAllApprovals] = v;
                }

                return v;
            }
            set
            {
                Session[Constants.ViewAllApprovals] = value;
            }
        }

        #endregion



        #region GET Actions

        // GET: ProjectFolders
        [HttpGet]
        public ActionResult Index(int? page,  string search, string sort, string sortdir, int? rowsPerPage , string status, string TagwordIds)
        {

            try
            {
                Session[Constants.SelectedProjectId] = null;
                Session[Constants.IsFromProjectFolderIndex] = true;
                Session["SelectedTagwordIDsToFilter"] = string.Empty;
                Session["SelectedApprovaltypeIDsToFilter"] = string.Empty;
                sort = sort == null ? Session[Constants.ProjectFilterField] == null ? "" : Session[Constants.ProjectFilterField].ToString() : sort;
                sortdir = sortdir == null ? Session[Constants.ProjectFilterDir] == null ? "" : Session[Constants.ProjectFilterDir].ToString() : sort;
                status = status == null ? Session[Constants.ProjectStatusFilter] == null ? "" : Session[Constants.ProjectStatusFilter].ToString() : status;
                int sortField = 1;
                int statusFilter = 0;
                switch (sort)
                {
                    case DateCreated: sortField = 1; break;
                    case DateModified: sortField = 2; break;
                    case DueByDate: sortField = 3; break;
                    case ProjectStatus: sortField = 4; break;
                    case Name: sortField = 5; break;
                    default: sortField = 1; break;
                }

                switch (status)
                {
                    case All: statusFilter = 0; break;
                    case NotAssigned: statusFilter = 1; break;
                    case Ongoing: statusFilter = 2; break;
                    case Completed: statusFilter = 3; break;
                    case Archived: statusFilter = 4; break;
                    default: statusFilter = 5; break;
                }

                bool sortAsc = (sortdir ?? string.Empty).ToLower() == "asc";

                if (!rowsPerPage.HasValue || rowsPerPage.GetValueOrDefault() <= 0)
                {
                    rowsPerPage = (int?)30;
                }

                if ((page.HasValue && page.Value <= 0) || page == null)
                    page = 1;

                ProjectFolderModel.ProjectFoldersModel model = new ProjectFolderModel.ProjectFoldersModel(this.LoggedAccount, this.LoggedUser.ID,
                                                                      page.HasValue ? page.Value : 1, statusFilter, sortField, sortAsc, Context, rowsPerPage.GetValueOrDefault(), search ?? String.Empty)
                { SearchText = search ?? string.Empty };

                model.projectDetails = new GMGColorDAL.ProjectFolder();
                model.projectCollaboratorRole = ProjectFolderBL.GetProjectFolderCollaboratorRoles();

                model.ApprovalsFilter = new ApprovalsFilterModel();
                model.ApprovalsFilter.tagwords = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
                model.ApprovalsFilter.CurrentTab = 0;

                AccountSettings.CollaborateGlobalSettings collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);

                ViewBag.DisableTagwordsInDashbord = collaborateSettings.DisableTagwordsInDashbord;
                ViewBag.LoggedAccount = LoggedAccount;
                ViewBag.LoggedUser = LoggedUser;
                ViewBag.LastProjectCurrentPage = model.ListProjectFolders != null ? model.ListProjectFolders.Count() <= page * rowsPerPage ? model.ListProjectFolders.Count() : page * rowsPerPage : 0;
                ViewBag.PageSize = model.RowsPerPage;
                model.OrderField = sortField;
                model.OrderDesc = !sortAsc;
                ViewBag.Tab = 0;
                ViewBag.PF_SearchText = search;
                return View(ViewNameByBrowserAgent("Index"), model);

            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, " grid method: {0}", ex.Message);
                return RedirectToAction("Cannotcontinued", "Error");
            }

        }


        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditMemberProjectFolder")]
        public ActionResult EditMemberProjectFolder(string SelectedCollaborators, int SelectedProjectID)
        {
            List<int> projectId = new List<int>();
            projectId.Add(SelectedProjectID);
            if (ApprovalBL.CheckUserCanAccess(projectId, Approval.CollaborateValidation.EditMemberProjectFolder, LoggedUser.ID, Context))
            {
                var selectedUsers = SelectedCollaborators.Split(';').ToList();
                List<ProjectFolderSelectedCollaboratorsModel> collaborators = new List<ProjectFolderSelectedCollaboratorsModel>();
                foreach (var userdetail in selectedUsers)
                {
                    var details = userdetail.Split(',').ToList();
                    ProjectFolderSelectedCollaboratorsModel collborator = new ProjectFolderSelectedCollaboratorsModel();
                    collborator.collaboratorID = Int32.Parse(details[0]);
                    collborator.isPDM = Convert.ToBoolean(details[1]);
                    collborator.roleID = Int32.Parse(details[2]);
                    collaborators.Add(collborator);
                }
                ProjectFolderBL.EditProjectFolderCollaborators(collaborators, SelectedProjectID, LoggedUser.ID, LoggedAccount.ID);
                //ProjectFolderBL.SendNotificationProjectFolderWasUpdated(LoggedUser, SelectedProjectID, Context);
            }
                return RedirectToAction("Index");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddEditProjectFolder")]
        public ActionResult AddEditProjectFolder(ProjectFolderModel.ProjectFoldersModel model, string SelectedUsers, string SelectedTagwords)
        {
            try
            {
                if (!CheckIsValidHtml(model.projectDetails.Name ))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }


                DateTime deadlineDate = default(DateTime);
                deadlineDate = ProjectFolderBL.GetFormatedProjectFolderDeadline(model.projectDetails.DeadlineDate, LoggedAccount.DateFormat1.Pattern);
                var selectedUsers = SelectedUsers.Split(';').ToList();
                List<ProjectFolderSelectedCollaboratorsModel> collaborators = new List<ProjectFolderSelectedCollaboratorsModel>();
                foreach (var userdetail in selectedUsers)
                {
                    var details = userdetail.Split(',').ToList();
                    ProjectFolderSelectedCollaboratorsModel collborator = new ProjectFolderSelectedCollaboratorsModel();
                    collborator.collaboratorID = Int32.Parse(details[0]);
                    collborator.isPDM = Convert.ToBoolean(details[1]);
                    collborator.roleID = Int32.Parse(details[2]);
                    collaborators.Add(collborator);
                }
                var selectedTagwords = SelectedTagwords.Split(',').ToList();
                model.projectDetails.Deadline = deadlineDate;
                if (model.projectDetails.ID == 0)
                {

                    model.projectDetails.Account = LoggedAccount.ID;
                    model.projectDetails.Creator = LoggedUser.ID;
                    int projectFolderID = ProjectFolderBL.AddProjectFolder(model.projectDetails);
                    ProjectFolderBL.AddProjectFolderCollaboratorsandTagwords(LoggedUser.ID, projectFolderID, collaborators, selectedTagwords, LoggedAccount.ID, true);
                    ProjectFolderBL.SendNotificationAddNewProjectFolder(LoggedUser, projectFolderID, Context);

                }
                else
                {
                    List<int> projectId = new List<int>();
                    projectId.Add(model.projectDetails.ID);
                    if (ApprovalBL.CheckUserCanAccess(projectId, Approval.CollaborateValidation.EditProjectFolder, LoggedUser.ID, Context))
                    {
                        ProjectFolderBL.EditProjectFolderDetails(model.projectDetails, LoggedUser.ID);
                        ProjectFolderBL.EditProjectFolderTagwordsandCollaborators(model.projectDetails.ID, selectedTagwords, collaborators, model.projectDetails.Creator, LoggedUser.ID, LoggedAccount.ID);
                        //ProjectFolderBL.SendNotificationProjectFolderWasUpdated(LoggedUser, model.projectDetails.ID, Context);
                    }
                }
                return RedirectToAction("Index");
            }

            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, " AddEditProjectFolder: {0}", ex.Message);
                return RedirectToAction("Cannotcontinued", "Error");
            }
        }
        



        //search in  Project Folder
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchProjectFolders")]
        public ActionResult Search(ProjectFolderModel.ProjectFoldersModel model, string SearchText)
        {

            string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
       
            return RedirectToAction("Index", new { search = HttpUtility.HtmlEncode(s) });
        }

        

        [HttpGet]
        public ActionResult GetProjectFoldersMembers(int SelectedProjectFolderID)
        {
            List<int> projectId = new List<int>();
            projectId.Add(SelectedProjectFolderID);
            if (!ApprovalBL.CheckUserCanAccess(projectId, Approval.CollaborateValidation.EditMemberProjectFolder, LoggedUser.ID, Context))
            {
                return Json(
                new
                {
                    Status = 300,
                    Content = "Error retrieving the projectfolder members"
                }, JsonRequestBehavior.AllowGet);
            }
            int projectCreator = ProjectFolderBL.GetProjectFolderCreatorID(SelectedProjectFolderID);
            var collaborators = ProjectFolderBL.GetProjectFolderCollaboratorDetails(SelectedProjectFolderID, projectCreator);
            return Json(
                new
                {
                    Status = 400,
                    ProjectCollaborators = collaborators,
                },
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditProject(int SelectedProjectFolderID)
        {
            List<int> projectId = new List<int>();
            projectId.Add(SelectedProjectFolderID);
            if (!ApprovalBL.CheckUserCanAccess(projectId, Approval.CollaborateValidation.EditProjectFolder, LoggedUser.ID, Context))
            {
                return Json(
                new
                {
                    Status = 300,
                    Content = "Error retrieving the projectfolder details"
                }, JsonRequestBehavior.AllowGet);
            }

            ProjectFolderModel.ProjectFoldersModel model = new ProjectFolderModel.ProjectFoldersModel();
            model.projectDetails = ProjectFolderBL.GetProjectFolderDetails(SelectedProjectFolderID);
            var tagwords = ProjectFolderBL.GetProjectFolderTagwords(SelectedProjectFolderID, LoggedAccount.ID);
            var collaborators = ProjectFolderBL.GetProjectFolderCollaboratorDetails(SelectedProjectFolderID, model.projectDetails.Creator);
            var Project_Deadline = GMGColorFormatData.GetFormattedDate(model.projectDetails.Deadline , LoggedAccount, Context);

            return Json(
                 new
                 {
                     Status = 400,
                     ProjectFolderID = model.projectDetails.ID,
                     ProjectName = model.projectDetails.Name,
                     ProjectDeadline = Project_Deadline,
                     projectDescription = model.projectDetails.Description != null ? model.projectDetails.Description : string.Empty,
                     NoOfPageGrids = model.projectDetails.PageGrid,
                     ProjectTagwords = tagwords,
                     ProjectCollaborators = collaborators,
                 },
                 JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Compress]
        public ActionResult ProjectFolderApprovalsIndex(int? SelectedProjectFolderID, int? topLinkID, string sort, string sortdir)
        {
            try
            { 
            Session[Constants.IsFromProjectFolderIndex] = null;
            if (SelectedProjectFolderID == null)
            {
                SelectedProjectFolderID = int.Parse(Session[Constants.SelectedProjectId].ToString()) > 0 ? int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                if(SelectedProjectFolderID== 0)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                Session[Constants.SelectedProjectId] = SelectedProjectFolderID;
            }
                List<int> projectFolderId = new List<int>();
                projectFolderId.Add(SelectedProjectFolderID.Value);
                if (LoggedUserCollaborateRole != Role.RoleName.AccountAdministrator && !ApprovalBL.CheckUserCanAccess(projectFolderId, Approval.CollaborateValidation.ViewProjectFolder, LoggedUser.ID, Context))
                {
                    return RedirectToAction("Cannotcontinued", "Error");
                }
            return LoadProjectFolderApprovalsIndex(SelectedProjectFolderID.Value, sort, sortdir);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, " ProjectFolderApprovalsIndex: {0}", ex.Message);
                return RedirectToAction("Cannotcontinued", "Error");
            }


        }

        [HttpGet]
        public ActionResult Populate(string ApprovalTypeIds, string TagwordIds, int? topLinkID, int? page, string sort, string sortDir, string status = null)
        {
            if (topLinkID != null && topLinkID.Value == (int)Utils.ApprovalTab.RecycleBin && LoggedUserCollaborateRole == Role.RoleName.AccountViewer)
            {
                topLinkID = (int)Utils.ApprovalTab.AllApprovals;
            }

            if (!string.IsNullOrEmpty(status) && !ApprovalBL.ExistsFolderCollborator(topLinkID.GetValueOrDefault(), LoggedUser.ID, Context))
            {
                return RedirectToAction("ProjectFolderApprovalsIndex");
            }

            if (ApprovalTypeIds != null && ApprovalTypeIds != "")
            {
                Session["SelectedApprovaltypeIDsToFilter"] = ApprovalTypeIds;
            }
            else
            {
                Session["SelectedApprovaltypeIDsToFilter"] = null;
            }
            if (TagwordIds != null && TagwordIds != "")
            {
                Session["SelectedTagwordIDsToFilter"] = TagwordIds;
            }
            else
            {
                Session["SelectedTagwordIDsToFilter"] = null;
            }

            if (topLinkID == -8 && Session[Constants.OutOfOfficeUser] == null)
            {
                Session[Constants.OutOfOfficeUser] = "0";
                topLinkID = -1;
            }

            Session[Constants.ApprovalsCurrentPage + this.LoggedUser.ID] = (page != null) ? int.Parse(page.Value.ToString()) : 1;
            Session[Constants.ApprovalsFolderId + this.LoggedUser.ID] = (topLinkID != null && int.Parse(topLinkID.Value.ToString()) > 0) ? int.Parse(topLinkID.Value.ToString()) : 0;
            Session[Constants.ApprovalsTopLink + this.LoggedUser.ID] = (topLinkID != null && int.Parse(topLinkID.Value.ToString()) < 1) ? int.Parse(topLinkID.Value.ToString()) : -1;
            Session[Constants.ApprovalsResetParameters] = false;

            // reset filter criteria when switch the tabs
            var populateID = (Convert.ToString(Session[Constants.ApprovalPopulateId]) ?? string.Empty).ToInt();
            if (populateID != null && populateID != topLinkID)
            {
                if (topLinkID < 0)
                {
                    SearchText = string.Empty;
                }

                // reset the statuses
                Session[Constants.ApprovalStatus] = 0;
            }

            if (!string.IsNullOrEmpty(status))
            {
                //set the "Changes Required" status for the current folder when retoucher access the link sent by a producer 
                Session[Constants.ApprovalStatus] = JobStatu.GetJobStatusId(
                    JobStatu.GetJobStatus(status.ToUpper()), Context);
            }

            Session[Constants.ApprovalPopulateId] = topLinkID;
            Session[GMG.CoZone.Common.Constants.SelectedTopLinkId] = -8;
            int SelectedProjectFolderID = int.Parse(Session[Constants.SelectedProjectId].ToString()) > 0 ?
                int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;

            ViewBag.ViewAll = ViewAll;

            return ProjectFolderApprovalsIndex(SelectedProjectFolderID, topLinkID, sort, sortDir);
            //return RedirectToAction("ProjectFoderApprovalsIndex", new { TopLinkID = topLinkID, sort = sort, sortdir = sortDir });
        }

        [HttpGet]
        public ActionResult PopulateProjectFolder( string ProjectFolderTagwordIds)
        {
            
            if (ProjectFolderTagwordIds != null && ProjectFolderTagwordIds != "")
            {
                Session["SelectedTagwordIDsToFilter"] = ProjectFolderTagwordIds;
            }
            else
            {
                Session["SelectedTagwordIDsToFilter"] = null;
            }
            
            ViewBag.ViewAll = ViewAll;
            return RedirectToAction("Index", new { TagwordIds = ProjectFolderTagwordIds });
        }
    

        private ActionResult PopulateApprovals(int ProjectFolderID ,int TopLinkID, int Page, string sort, string sortdir)
        {
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
            ApprovalsModel model = new ApprovalsModel
            {
                LoggedUserID = LoggedUser.ID
            };
            if (TopLinkID == -8)
            {
                model.LoggedUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0 ?
                int.Parse(Session[Constants.OutOfOfficeUser].ToString()) : 0;
            }

            TempData["SelectedTab"] = TopLinkID;
            TempData["AccountName"] = this.LoggedAccount.Name;

            SetApprovalsCount(ProjectFolderID, TopLinkID, collaborateSettings);
            

            switch (TopLinkID)
            {
                case (int)Utils.ApprovalTab.AllApprovals:
                    ViewBag.CurrentTabCount = ViewBag.AllAppCount;
                    break;
                case (int)Utils.ApprovalTab.OwnedByMe:
                    ViewBag.CurrentTabCount = ViewBag.MyAppCount;
                    break;
                case (int)Utils.ApprovalTab.SharedWithMe:
                    ViewBag.CurrentTabCount = ViewBag.SharedAppCount;
                    break;
                case (int)Utils.ApprovalTab.RecentlyViewed:
                    ViewBag.CurrentTabCount = ViewBag.RecentlyViewedCount;
                    break;
                case (int)Utils.ApprovalTab.Archive_PastTense:
                    ViewBag.CurrentTabCount = ViewBag.RecentlyArchivedCount;
                    break;
                case (int)Utils.ApprovalTab.RecycleBin:
                    ViewBag.CurrentTabCount = ViewBag.RecycleBinCount;
                    break;
                case (int)Utils.ApprovalTab.MyOpenApprovals:
                    ViewBag.CurrentTabCount = ViewBag.MyOpenApprovalsCount;
                    break;
                case (int)Utils.ApprovalTab.SubstitutedApprovals:
                    ViewBag.CurrentTabCount = ViewBag.SubstitutedApprovalsCount;
                    break;
                default:
                    ViewBag.CurrentTabCount = ViewBag.AdvanceSearchCount;
                    break;
            }

            switch (TopLinkID)
            {
                case (int)Utils.ApprovalTab.AllApprovals:
                    TempData["SelectedTabName"] = Resources.Resources.lblAllApprovels;
                    break;
                case (int)Utils.ApprovalTab.OwnedByMe:
                    TempData["SelectedTabName"] = Resources.Resources.lblOwnedByMe;
                    break;
                case (int)Utils.ApprovalTab.SharedWithMe:
                    TempData["SelectedTabName"] = Resources.Resources.lblSharedWithMe;
                    break;
                case (int)Utils.ApprovalTab.RecentlyViewed:
                    TempData["SelectedTabName"] = Resources.Resources.lblRecentlyViewed;
                    break;
                case (int)Utils.ApprovalTab.Archive_PastTense:
                    TempData["SelectedTabName"] = Resources.Resources.lblArchive_PastTense;
                    break;
                case (int)Utils.ApprovalTab.RecycleBin:
                    TempData["SelectedTabName"] = Resources.Resources.lblRecycleBin;
                    break;
                case (int)Utils.ApprovalTab.MyOpenApprovals:
                    TempData["SelectedTabName"] = Resources.Resources.lblMyOpenApprovals;
                    break;
                case (int)Utils.ApprovalTab.SubstitutedApprovals:
                    TempData["SelectedTabName"] = Resources.Resources.lblOutOfOfficeApprovals;
                    break;
                default:
                    var advS = (GMGColorDAL.CustomModels.AdvancedSearch)Session["AdvancedSearchModel"];

                    if (advS == null && TopLinkID <= -Constants.TopLinkOffset)
                    {
                        int advsId = (-TopLinkID) - Constants.TopLinkOffset;

                        advS = ApprovalBL.GetAdvancedSearch(advsId, Context, LoggedAccount.DateFormat1.Pattern);

                        if (advS != null)
                        {
                            TempData["SelectedTabName"] = advS.AdvSName;
                            Session["AdvancedSearchModel"] = advS;
                        }
                    }
                    break;
            }           

            ViewBag.SearchText = SearchText;
            ViewBag.LoggedUserRole = LoggedUserCollaborateRole;
            ViewBag.LoggedUserId = LoggedUser.ID;
            ViewBag.TopLinkID = TopLinkID;
            ViewBag.Page = Page;
            ViewBag.Sort = sort;
            ViewBag.SortDir = sortdir;
            ViewBag.ViewAll = ViewAll;
            ViewBag.LoggedUserFileTransferRole = LoggedUserFileTransferRole;
            ViewBag.FolderID = 0;

            return View(ViewNameByBrowserAgent("ProjectFolderApprovalsIndex"), model);
        }
    
        [HttpGet]
        [Compress]
        public ActionResult PopulateApprovalsGrid(int topLinkID, int page, int folderID, Role.RoleName loggedUserRole, bool hasAccessToFtp, int archivedCount, int recycleBinCount, int? currentTabCount, string sort, string sortdir)
        {
            try
            {
                int projectFolderId = int.Parse(Session[Constants.SelectedProjectId].ToString()) > 0 ?
                int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                List<ReturnProjectFolderApprovalsPageView> lstApprovalsPageView = new List<ReturnProjectFolderApprovalsPageView>();

                if (sort != null)
                {
                    var sortColumn = (GMGColorCommon.ApprovalGrigdSortColumn)Enum.Parse(typeof(GMGColorCommon.ApprovalGrigdSortColumn), sort, true);
                    switch (sortColumn)
                    {
                        case GMGColorCommon.ApprovalGrigdSortColumn.Deadline: SortColumn = (int)GMGColorCommon.ApprovalGrigdSortColumn.Deadline; break;
                        case GMGColorCommon.ApprovalGrigdSortColumn.Title: SortColumn = (int)GMGColorCommon.ApprovalGrigdSortColumn.Title; break;
                        case GMGColorCommon.ApprovalGrigdSortColumn.PrimaryDecisionMaker: SortColumn = (int)GMGColorCommon.ApprovalGrigdSortColumn.PrimaryDecisionMaker; break;
                        case GMGColorCommon.ApprovalGrigdSortColumn.PhaseName: SortColumn = (int)GMGColorCommon.ApprovalGrigdSortColumn.PhaseName; break;
                        case GMGColorCommon.ApprovalGrigdSortColumn.NextPhase: SortColumn = (int)GMGColorCommon.ApprovalGrigdSortColumn.NextPhase; break;
                        case GMGColorCommon.ApprovalGrigdSortColumn.DecisionMakers: SortColumn = (int)GMGColorCommon.ApprovalGrigdSortColumn.DecisionMakers; break;
                    }
                }

                if (sortdir != null)
                {
                    var sortDirection = (sortdir ?? string.Empty).ToLower() == "desc";
                    SortingDirection = sortDirection;
                    FiltersSorting = false;
                }

                int filter = FiltersSorting ? FilterType : SortColumn;
                bool direction = FiltersSorting ? SortingOrder : SortingDirection;

                OutOfOfficeUsers OutOfOfficeUsers = new OutOfOfficeUsers();
                OutOfOfficeUsers.List_OutOfOfficeUsers = UserBL.GetOutOfOfficeUsers(LoggedUser.ID, Context);
                if (topLinkID == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    int substituteUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    loggedUserRole = Role.GetRole(UserBL.GetRoleKey(GMG.CoZone.Common.AppModule.Collaborate, substituteUserID, Context));
                    GMGColorDAL.User substituteUser = Context.Users.FirstOrDefault(u => u.ID == substituteUserID);
                    this.LoggedUser = substituteUser;
                }

                ProjectFolderApprovalsGrid model = new ProjectFolderApprovalsGrid(this.LoggedAccount, Context)
                {
                    LoggedUser = LoggedUser,
                    LoggedAccount = LoggedAccount,
                    LoggedUserRole = loggedUserRole,
                    AnnotationsReportFilters = new AnnotationsReportFilters(),
                    OutOfOfficeUsers = OutOfOfficeUsers
                };

                if (model.LoggedUserRole == Role.RoleName.AccountManager)
                {
                    string value = ApiBL.GetValueOfShowAllFilesToManagers(model.LoggedAccount.ID, GMGColorConstants.ShowAllFilesToManagers, Context);
                    model.ShowAllFilesToManagers = value == "True" ? true : false;
                }
                else
                {
                    model.ShowAllFilesToManagers = false;
                }

                int recCount = 0;
                ViewBag.Tab = (folderID == 0) ? topLinkID : folderID;
                ViewBag.FolderID = folderID;
                ViewBag.DeliverRole = LoggedUserDeliverRole;
                ViewBag.HasDeliverSubscriptionPlan = LoggedAccountSubscriptionDetails.HasDeliverSubscriptionPlan;

                // TODO: see if this is needed
                ViewBag.CustomDecisions = ApprovalCustomDecision.GetCustomDecisionsByAccountId(LoggedAccount.ID, LoggedUser.Locale, Context);

                // for retourcher make sure only changes required and changes completed are processed
                var modifiedFilters = false;
                if (LoggedUserCollaborateRole == Role.RoleName.AccountRetoucher && Status == 0)
                {
                    Status = (int)JobStatu.Status.ChangesRequired + (int)JobStatu.Status.ChangesComplete;
                    modifiedFilters = true;
                }

                AccountSettings.CollaborateGlobalSettings collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
                if (collaborateSettings.ShowAllFilesToAdmins == false)
                {
                    Session[Constants.ViewAllApprovals] = null;
                }
                ViewBag.ShowAllFilter = collaborateSettings.ShowAllFilesToAdmins;
                ViewBag.RetoucherWrflwEnabled = collaborateSettings.EnableRetoucherWorkflow;
                ViewBag.FolderPermissionsAutoInheritanceEnabled = collaborateSettings.InheritParentFolderPermissions;

                if (topLinkID == (int)Utils.ApprovalTab.RecentlyViewed)
                {
                    string selectedApprovalTypeIds = string.Empty;
                    string selectedTagwordIds = string.Empty;
                    if (Session["SelectedApprovaltypeIDsToFilter"] != null)
                    {
                        selectedApprovalTypeIds = (string)Session["SelectedApprovaltypeIDsToFilter"];

                    }
                    if (Session["SelectedTagwordIDsToFilter"] != null)
                    {
                        selectedTagwordIds = (string)Session["SelectedTagwordIDsToFilter"];
                    }
                    lstApprovalsPageView = GMGColorDAL.ProjectFolder.GetRecentViwedApprovals(collaborateSettings.HideCompletedApprovalsInDashboard, LoggedAccount.ID, projectFolderId , LoggedUser.ID , PageSize, page, filter, direction, SearchText, Status, out recCount, Context, selectedApprovalTypeIds, selectedTagwordIds);
                }
                else if (topLinkID == (int)Utils.ApprovalTab.Archive_PastTense && LoggedUserCollaborateRole != Role.RoleName.AccountRetoucher)
                {
                    //Check permissions to see All Archive within account for the current user
                    if (topLinkID != (int)Utils.ApprovalTab.Archive_PastTense || (model.LoggedUserRole != Role.RoleName.AccountAdministrator && model.LoggedUserRole != Role.RoleName.AccountManager))
                    {
                        ViewAll = false;
                    }
                    lstApprovalsPageView = GMGColorDAL.ProjectFolder.GetArchived(LoggedAccount.ID, projectFolderId , LoggedUser.ID, PageSize, page, filter, direction, ViewAll, SearchText, out recCount, Context);
                }
                else if (topLinkID == (int)Utils.ApprovalTab.RecycleBin)
                {
                    //Check permissions to see All RecycleBin within account for the current user
                    if (topLinkID != (int)Utils.ApprovalTab.RecycleBin || (model.LoggedUserRole != Role.RoleName.AccountAdministrator && model.LoggedUserRole != Role.RoleName.AccountManager))
                    {
                        ViewAll = false;
                    }
                    lstApprovalsPageView = GMGColorDAL.ProjectFolder.GetRecycleBin(LoggedAccount.ID, projectFolderId , LoggedUser.ID , PageSize, page, filter, direction, SearchText, ViewAll, out recCount, Context);
                }
                else
                {
                    //All approvals Tab
                    string selectedApprovalTypeIds = string.Empty;
                    string selectedTagwordIds = string.Empty;
                    if (Session["SelectedApprovaltypeIDsToFilter"] != null)
                    {
                        selectedApprovalTypeIds = (string)Session["SelectedApprovaltypeIDsToFilter"];

                    }
                    if (Session["SelectedTagwordIDsToFilter"] != null)
                    {
                        selectedTagwordIds = (string)Session["SelectedTagwordIDsToFilter"];
                    }
                    //Check permissions to see All Approvals within account for the current user
                    if (topLinkID != (int)Utils.ApprovalTab.AllApprovals || (model.LoggedUserRole != Role.RoleName.AccountAdministrator && model.LoggedUserRole != Role.RoleName.AccountManager))
                    {
                        ViewAll = false;
                    }


                    var advS = (GMGColorDAL.CustomModels.AdvancedSearch)Session["AdvancedSearchModel"];

                    if (advS != null)
                    {
                        var sessionSearchObject = (GMGColorDAL.CustomModels.AdvancedSearch)Session["AdvancedSearchModel"];
                        SearchText = sessionSearchObject.AdvSSearchByTitle;
                        lstApprovalsPageView = GMGColorDAL.ProjectFolder.GetProjectFolderApprovals(collaborateSettings.HideCompletedApprovalsInDashboard, LoggedAccount.ID, (int)projectFolderId, LoggedUser.ID, Math.Abs((int)Utils.ApprovalTab.AllApprovals), PageSize, page, filter, direction, SearchText, (int)Status, ViewAll, out recCount, Context, advS, LoggedAccount.TimeZone, LoggedAccount.DateFormat1.Pattern, selectedApprovalTypeIds, selectedTagwordIds);
                        Session["AdvancedSearchModel"] = null;
                    }
                    else
                    {
                        if (topLinkID != -8)
                        {
                            Session[Constants.OutOfOfficeUser] = "0";
                            lstApprovalsPageView = GMGColorDAL.ProjectFolder.GetProjectFolderApprovals(collaborateSettings.HideCompletedApprovalsInDashboard, LoggedAccount.ID, (int)projectFolderId, LoggedUser.ID, Math.Abs(topLinkID), PageSize, page, filter, direction, SearchText, (int)Status, ViewAll, out recCount, Context, advS, LoggedAccount.TimeZone, LoggedAccount.DateFormat1.Pattern, selectedApprovalTypeIds, selectedTagwordIds);
                        }
                        else
                        {
                            lstApprovalsPageView = GMGColorDAL.ProjectFolder.GetProjectFolderApprovals(collaborateSettings.HideCompletedApprovalsInDashboard, LoggedAccount.ID, (int)projectFolderId, int.Parse(Session[Constants.OutOfOfficeUser].ToString()), Math.Abs(topLinkID), PageSize, page, filter, direction, SearchText, Status, ViewAll, out recCount, Context, advS, LoggedAccount.TimeZone, LoggedAccount.DateFormat1.Pattern, selectedApprovalTypeIds, selectedTagwordIds);
                        }
                    }


                    ViewBag.ViewAll = ViewAll;
                }

                //Get Total from ApprovalCountsProcedure, avoid doing this again in populate procedures
                recCount = currentTabCount.GetValueOrDefault();

                if (page == 1)
                {
                    Session["CurrentTabCount"] = recCount;
                }

                Session["RecordCount"] = recCount;

                ViewBag.FileDownload = BaseBL.PopulateFileDownloadModel(LoggedAccount.ID, LoggedUser.ID, GMG.CoZone.Common.AppModule.Collaborate, Context, collaborateSettings.ShowAllFilesToAdmins);

                if (modifiedFilters)
                {
                    Status = 0;
                }

                ViewBag.CurrentPage = page;
                ViewBag.FilterType = FilterType;
                ViewBag.FilterStatus = JobStatu.GetJobStatus(Status, Context);
                ViewBag.SearchText = SearchText;
                ViewBag.SortingOrder = SortingOrder;
                ViewBag.PageSize = PageSize;
                ViewBag.CurrentTabApprovalCount = CurrentTabCount;
                ViewBag.AccountDateFormat = LoggedAccount.DateFormat1.Pattern;
                ViewBag.RecentlyArchivedCount = archivedCount;
                ViewBag.RecycleBinCount = recycleBinCount;
                ViewBag.Sort = sort;
                ViewBag.SortDir = sortdir;
                ViewBag.ViewAll = ViewAll;

                ViewBag.DisableSOADIndicator = collaborateSettings.DisableSOADIndicator;
                ViewBag.DisableTagwordsInDashbord = collaborateSettings.DisableTagwordsInDashbord;
                ViewBag.ShowBadges = collaborateSettings.ShowBadges;
                ViewBag.DisableVisualIndicator = collaborateSettings.DisableVisualIndicator;
                ViewBag.LoggedUserFileTransferRole = LoggedUserFileTransferRole;
                if (topLinkID == -8)
                {
                    ViewBag.OutofOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    if (ViewBag.OutofOfficeUserID == 0)
                    {
                        ViewBag.CurrentTabApprovalCount = 0;
                    }
                    else
                    {
                        ProjectFolderApprovalCountsView objOooUserApprovalCount = GMGColorDAL.ProjectFolder.GetApprovalCounts(folderID ,collaborateSettings.HideCompletedApprovalsInDashboard, this.LoggedAccount.ID, int.Parse(Session[Constants.OutOfOfficeUser].ToString()), ViewAll, false, Role.GetRoleKey(LoggedUserCollaborateRole), Context, LoggedAccount.TimeZone, LoggedAccount.DateFormat1.Pattern, null).SingleOrDefault();
                        ViewBag.CurrentTabApprovalCount = objOooUserApprovalCount.AllCount;
                    }
                }

                Session["SelectedTagwordIDsToFilter"] = string.Empty;
                Session["SelectedApprovaltypeIDsToFilter"] = string.Empty;
                model.ProjectFolderApprovalsPageView = lstApprovalsPageView;
                model.ApprovalsFilter = new ApprovalsFilterModel();
                model.ApprovalsFilter.tagwords = new List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount>();
                model.ApprovalsFilter.CurrentTab = ViewBag.Tab;
                model.AnyAvailableValidInstances = GMGColorDAL.ColorProofInstance.AnyAvailableValidInstances(this.LoggedUser.ID, Context);

                ViewBag.projectID = projectFolderId;
                ViewBag.LoggedUser = this.LoggedUser.ID;


                //------------> New Project Folder Approval

                NewApprovalModel projectfolderApproval = NewProjectFolderApproval("", 0, 0);
                projectfolderApproval.ProjectFolder = (int)projectFolderId;
                model.NewProjectFolderApproval = projectfolderApproval;

                var jsonResult = Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, ViewNameByBrowserAgent("ProjectFolderApprovalsGrid"), LoggedUser.ID, model),
                        Approvals = (from item in lstApprovalsPageView select item.Approval).ToArray(),
                        collaborateSettings.DisplayRetouchers,
                        collaborateSettings.ShowBadges,
                        collaborateSettings.DisableSOADIndicator,
                        collaborateSettings.DisableTagwordsInDashbord,
                        DisableVisualIndicator = collaborateSettings.DisableVisualIndicator.ToString().ToLower(),
                    },
                    JsonRequestBehavior.AllowGet);

                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;

            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController. Populate Approvals Grid Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested approvals"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetApprovalsDecisions(string approvals)
        {
            try
            {
                List<int> ids = approvals.Split(',').Select(int.Parse).ToList();

                List<Approval.ApprovalDecisions> decisions = null;
                if (ids.Count > 0)
                {
                    SetUICulture(LoggedUser.ID);
                    decisions = ApprovalBL.GetApprovalsDecisions(ids, LoggedAccount.ID, LoggedUser.Locale, Context);
                }

                return Json(
                    new
                    {
                        Status = 400,
                        Content = Json(decisions, JsonRequestBehavior.AllowGet)

                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetApprovalsDecisions Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested approvals decisions"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetApprovalsRetouchers(string approvals)
        {
            try
            {
                List<int> ids = approvals.Split(',').Select(int.Parse).ToList();

                List<Approval.ApprovalRetoucher> retouchers = null;
                if (ids.Count > 0)
                {
                    retouchers = ApprovalBL.GetApprovalRetouchers(ids, Context);
                }

                return Json(
                    new
                    {
                        Status = 400,
                        Content = Json(retouchers, JsonRequestBehavior.AllowGet)

                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetApprovalsRetouchers Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested approvals retouchers"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpGet]
        [Compress]
        public JsonResult GetProjectFolderTagwords(int projectFolder)
        {
            try
            {
                var tagwords = ProjectFolderBL.GetProjectFolderTagwords(projectFolder, LoggedAccount.ID);
                return Json(
                    new
                    {
                        Status = 400,
                        ProjectTagwords = (from itm in tagwords select new { id = itm.ID, text = itm.Tagword }).ToList()
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFolderController.GetProjectFolderTagwords Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested project folder tagwords"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetApprovalsNotViewedAnnotations(string approvals)
        {
            try
            {
                List<int> ids = approvals.Split(',').Select(int.Parse).ToList();
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                List<Approval.ApprovalNotViewedAnnotations> notViewedAnnotations = new List<Approval.ApprovalNotViewedAnnotations>();
                if (ids.Count > 0)
                {
                    notViewedAnnotations = ApprovalBL.GetApprovalsNotViewedAnnotations(ids, this.LoggedUser.ID, Context);
                }

                return Json(
                    new
                    {
                        Status = 400,
                        Content = Json(notViewedAnnotations, JsonRequestBehavior.AllowGet)

                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetApprovalsRetouchers Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested approvals retouchers"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetAllCollDecisionSetting()
        {
            bool settingVal = SettingsBL.GetAccountGlobalSetting(LoggedAccount.ID,
                GMGColorConstants.AllColDecisionRequired, Context);

            return Json(settingVal, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ApprovalDetails(string guid, int? selectedApproval, int? topLinkID)
        {
            Session[Constants.ApprovalsResetParameters] = false;
            int TopLinkId = 0;

            int substituteOwnerId = 0;
            Role.RoleName substituteOwnerCollaborateRole = Role.RoleName.AccountViewer;
            if (topLinkID == null && Session[Constants.SelectedTopLinkId] != null)
            {
                TopLinkId = int.Parse(Session[Constants.SelectedTopLinkId].ToString());
            }
            else if (topLinkID != null)
            {
                TopLinkId = (int)topLinkID;
            }
            if (TopLinkId == -8)
            {
                substituteOwnerId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                substituteOwnerCollaborateRole = Role.GetRole(UserBL.GetRoleKey(GMG.CoZone.Common.AppModule.Collaborate, substituteOwnerId, Context));
            }

            // Nulable the existing folder value
            Session[Constants.ApprovalsExistingFolders + (substituteOwnerId == 0 ? LoggedUser.ID : substituteOwnerId)] = null;

            if (selectedApproval.HasValue)
            {
                Session[Constants.SelectedApprovalId] = selectedApproval.Value;
            }

            if (topLinkID.HasValue)
            {
                Session[Constants.SelectedTopLinkId] = topLinkID.Value;
            }

            int ApprovalID = 0;
            if (!string.IsNullOrEmpty(guid))
            {
                //from PS
                ApprovalID = ApprovalBL.GetApprovalIdByGuid(guid, Context);
            }
            else if (Session[Constants.SelectedApprovalId] != null)
            {
                ApprovalID = int.Parse(Session[Constants.SelectedApprovalId].ToString());
            }
            var ViewAllApprovals = Session[Constants.ViewAllApprovals] != null ? Session[Constants.ViewAllApprovals].ToString() : "";
            bool selectAll = false;
            selectAll = ViewAllApprovals != null ? (ViewAllApprovals == "True" ? selectAll = true : selectAll = false) : selectAll = false;
            if (!selectAll)
            {
                if (!CanAccessApproval(ApprovalID, GMGColorDAL.ProjectFolder.ApprovalOperation.ViewApproval))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
            }



            if (substituteOwnerId == 0 ? (!CanAccess(ApprovalID, GMGColorDAL.ProjectFolder.ApprovalOperation.ViewApproval)) : !CanSubstituteUserAccess(ApprovalID, substituteOwnerId, GMGColorDAL.ProjectFolder.ApprovalOperation.ViewApproval))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
            var model = new ApprovalDetailsModel(LoggedAccount, (substituteOwnerId == 0 ? LoggedUser.ID : substituteOwnerId), ApprovalID, substituteOwnerId == 0 ? LoggedUserCollaborateRole : substituteOwnerCollaborateRole, collaborateSettings, Context);

            if (model.DefaultViewingConditions != null)
            {
                ViewBag.PaperTintsJSON = (new System.Web.Script.Serialization.JavaScriptSerializer()).Serialize(model.DefaultViewingConditions.PaperTints);
            }

            ViewBag.FileDownload = BaseBL.PopulateFileDownloadModel(LoggedAccount.ID, (substituteOwnerId == 0 ? LoggedUser.ID : substituteOwnerId), AppModule.Collaborate, Context, collaborateSettings.ShowAllFilesToAdmins);

            ViewBag.LoggedUserRole = LoggedUserCollaborateRole;
            ViewBag.CanRenameApproval = model.JobStatus != JobStatu.Status.Completed && GMGColorDAL.ProjectFolder.CanAccess(model.LoggedUserID, new int[] { model.ID }, GMGColorDAL.ProjectFolder.ApprovalOperation.RenameApproval, Context);
            ViewBag.ShowAllFilter = collaborateSettings.ShowAllFilesToAdmins;
            ViewBag.TopLinkID = TopLinkId;
            return View(ViewNameByBrowserAgent("ApprovalDetails"), model);
        }

        [HttpGet]
        public ActionResult UserActivityReport(int selectedApproval)
        {
             return RedirectToAction("UserActivityReport", "Approvals", new { selectedApproval = selectedApproval });
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalWorkflowDetails(int versionId)
        {
            try
            {
                SetUICulture(LoggedUser.ID);

                int substituteOwnerId = 0;
                if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                {
                    substituteOwnerId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }


                ApprovalVersionWorkflowDetails approvalVersionWorkflowDetails = new ApprovalVersionWorkflowDetails();
                approvalVersionWorkflowDetails = ApprovalBL.GetApprovalCurrentVersion(versionId, LoggedAccount, (substituteOwnerId == 0 ? LoggedUser.ID : substituteOwnerId), LoggedUser.Locale, LoggedUserCollaborateRole, Context);

                return Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, "ApprovalWorkflow", LoggedUser.ID, approvalVersionWorkflowDetails)
                    },
                        JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController. GetApprovalVersionDetails Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalAnnotationsToDosDetails(int versionId)
        {
            try
            {
                SetUICulture(LoggedUser.ID);

                List<ApprovalAnnotationStatusChangeLog> approvalVersionAnnotationDetails = ApprovalBL.GetApprovalWorkflowAnnotationToDosDetails(versionId, LoggedAccount, LoggedUser, LoggedUserCollaborateRole, Context);
                return Json(
                   new
                   {
                       Status = 400,
                       Content = RenderViewToString(this, "AnnotationsTodos", LoggedUser.ID, approvalVersionAnnotationDetails)
                   },
                       JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                  "ProjectFoldersController. GetApprovalAnnotationsToDosDetails Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalVersionDetails(int versionId, Role.RoleName loggedUserRole, JobStatu.Status jobStatus, bool? showAllFilter, int topLinkID)
        {
            try
            {
                if (topLinkID == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    this.LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    loggedUserRole = Role.GetRole(UserBL.GetRoleKey(GMG.CoZone.Common.AppModule.Collaborate, LoggedUser.ID, Context));
                }

                SetUICulture(LoggedUser.ID);

                ApprovalDetailsModel.VersionDetailsModel versionModel = ApprovalBL.PopulateVersionDetailsModel(versionId, LoggedUser, LoggedAccount, LoggedUser.Locale, Context);
                versionModel.JobStatus = jobStatus;
                versionModel.LoggedUserRole = loggedUserRole;
                versionModel.LoggedUserID = LoggedUser.ID;
                versionModel.ShowAllFilter = showAllFilter.GetValueOrDefault();
                ViewBag.IsLoggedUserVersionOwner = LoggedUser.ID == versionModel.objApprovalVersion.Owner;
                ViewBag.TopLinkID = topLinkID;

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
                versionModel.DisableSOADIndicator = collaborateSettings.DisableSOADIndicator;
                versionModel.DisableTagwordsInDashbord = collaborateSettings.DisableTagwordsInDashbord;

                return Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, ViewNameByBrowserAgent("ApprovalVersionDetails"), LoggedUser.ID, versionModel)
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController. GetApprovalVersionDetails Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalReminderSentDetails(int? versionId)
        {
            try
            {
                if (versionId != null)
                {
                    var approvalCollaborators = ApprovalBL.GetApprovalCollaborators(versionId.Value, LoggedAccount, Context);

                    return Json(
                        new
                        {
                            Status = 400,
                            Content = Json(approvalCollaborators, JsonRequestBehavior.AllowGet)
                        },
                        JsonRequestBehavior.AllowGet);
                }
                return Json(new { Status = 400, Content = "Error retrieving the requested GetApprovalReminderSentDetails" });
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFolderController. GetApprovalReminderSentDetails Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested GetApprovalReminderSentDetails"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult LoadUserActivity(int approval)
        {
            try
            {
                if (DALUtils.IsFromCurrentAccount<Approval>(approval, LoggedAccount.ID, Context))
                {
                    SetUICulture(LoggedUser.ID);

                    ApprovalDetailsModel.VersionDetailsModel versionModel =
                        ApprovalBL.PopulateVersionDetailsModel(approval, LoggedUser, LoggedAccount, LoggedUser.Locale, Context);
                    versionModel.LoggedUserID = LoggedUser.ID;
                    ViewBag.IsLoggedUserVersionOwner = LoggedUser.ID == versionModel.objApprovalVersion.Owner;
                    versionModel.DisableActions = true;

                    var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
                    versionModel.DisableSOADIndicator = collaborateSettings.DisableSOADIndicator;
                    versionModel.DisableTagwordsInDashbord = collaborateSettings.DisableTagwordsInDashbord;

                    return Json(
                        new
                        {
                            Status = 200,
                            Content =
                                RenderViewToString(this, ViewNameByBrowserAgent("ProjectFolderUserActivity"), LoggedUser.ID,
                                    versionModel)
                        },
                        JsonRequestBehavior.AllowGet);
                }

                return Json(
                       new
                       {
                           Status = 403
                       },
                       JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController. LoadUserActivity Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalFileStatuses(string[] fileNames, int folderId)
        {
            int duplicateFileKey = LoggedAccount.UploadEngineAccountSetting != null ? LoggedAccount.UploadEngineAccountSetting.UploadEngineDuplicateFileName.Key : -1;

            List<Approval.ApprovalFileStatusInfo> fileStatuses = new List<Approval.ApprovalFileStatusInfo>();

            foreach (string fileName in fileNames)
            {
                Job job;
                ApprovalFileStatus approvalFileStatus = ApprovalBL.NewFileStatus(LoggedAccount.ID, fileName, folderId, (FileUploadDuplicateName)duplicateFileKey, out job, Context);

                string approvalFileStatusLabel = null;

                switch (approvalFileStatus)
                {
                    case ApprovalFileStatus.NewJob:
                        approvalFileStatusLabel = Resources.Resources.lblApprovalNewJob;
                        break;
                    case ApprovalFileStatus.NewVersion:
                        approvalFileStatusLabel = Resources.Resources.lblApprovalNewVersion;
                        break;
                    case ApprovalFileStatus.DuplicatedFile:
                    case ApprovalFileStatus.IgnoredFile:
                        approvalFileStatusLabel = Resources.Resources.lblApprovalIgnoreFile;
                        break;
                }

                fileStatuses.Add(new Approval.ApprovalFileStatusInfo { FileName = fileName, FileStatus = approvalFileStatus.ToString(), FileStatusLabel = approvalFileStatusLabel });
            }

            return Json(fileStatuses, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetApprovalsPageCount(string jobIdList)
        {
            if (!String.IsNullOrEmpty(jobIdList))
            {
                IEnumerable<int> approvalsIds =
                    jobIdList.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);

                List<Approval.ApprovalPageCountInfo> approvalPages = ApprovalBL.GetApprovalPagesCount(approvalsIds, Context);


                return Json(string.Join(",", approvalPages.Select(t => t.PageCount)), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving approval page counts"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetWorkflowCheckListDetails()
        {
            var workflowList = ApprovalWorkflowBL.GetApprovalWorkflows(LoggedAccount.ID, Context);
            var checklist = ChecklistBL.GetCheckLists(LoggedAccount.ID, Context);

            try
            {
                return Json(
                  new
                  {
                      Status = 400,
                      workflowList = workflowList,
                      checklist = checklist
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetCollaborators Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the collaborators"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }


        #endregion


        #region POST Actions

        [HttpPost]
        public JsonResult DownloadSelectedProjects(string fileIDs)
        {
            List<int> selectedProjectIDs = (fileIDs.Split(',').ToList()).ConvertAll(int.Parse);
            FileDownloadViewModel model = new FileDownloadViewModel();
            model.AccountId = 0;
            model.AccountPressets = null;
            model.ApplicationModule = AppModule.ProjectFolders;
            model.DownloadFileTypes = DownloadFileTypes.Zip;
            model.DownloadVersionTypes = DownloadVersionTypes.CurrentVersion;
            model.FolderIds = null;
            model.HasAccessToFtp = false;
            model.IsZipFileWhenDownloadingEnabled = true;
            model.Preset = 0;
            model.SelectedPresetId = 0;
            model.ShowAllFilesToAdmins = true;
            model.FileIds = ProjectFolderBL.GetProjectFolderApprovalIDs(selectedProjectIDs, LoggedAccount.ID);

            return GetDownloadFilePath(model);
        }
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "NewProjectFolderApproval")]
        public ActionResult NewProjectFolderApproval(NewApprovalModel model, string hdnFileName_1)
        {
            if (!CheckIsValidHtml(model.ExternalEmails))
            {
                return RedirectToAction("XSSRequestValidation", "Error");
            }

            int substituteOwnerId = 0;
            int border = 5;
            if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
            {
                substituteOwnerId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                GMGColorDAL.User substituteUser = Context.Users.FirstOrDefault(u => u.ID == substituteOwnerId);
                this.LoggedUser = substituteUser;
                model.Owner = substituteOwnerId;
            }

            if (model.PrivateAnnotations && model.OnlyOneDecisionRequired)
            {
                model.ExternalUsers = "[]";
                model.ExternalEmails = null;
            }
            List<ApprovalJob> files = new List<ApprovalJob>();
            List<int> lstTmp = Request.Form.AllKeys.Where(o => o.Contains("ApprovalTitle_")).Select(o => int.Parse(o.Split('_').Last())).ToList();
            var phasesCollaborators = new ApprovalWorkflowPhasesInfo();
            List<GMGColorDAL.ApprovalPage> approvalHtmlPageList = new List<GMGColorDAL.ApprovalPage>();

            #region Custom Validation

            if (!CheckIsValidHtml(model.OptionalMessage))
            {
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                return RedirectWithModelAndErrors(model, dicModelErrors);
            }

            if (model.IsUploadApproval)
            {
                if (String.IsNullOrEmpty(hdnFileName_1.Trim()))
                {
                    model.IsUploadApproval = true;

                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                    dicModelErrors.Add("fileValidator", Resources.Resources.reqApproval);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }
            }
            else
            {
                lstTmp.Clear();

                Uri myUri;
                if (Uri.TryCreate(GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + model.Url, UriKind.Absolute, out myUri))
                {
                    lstTmp.Add(1);
                }
                else
                {
                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                    dicModelErrors.Add("urlValidator", Resources.Resources.valUrl);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }
            }
            if (model.HasAdHocWorkflow && string.IsNullOrEmpty(model.ApprovalAdHocWorkflowName))
            {

                model.ErrorMessage = Resources.Resources.errMissingWorkflowInfo;
                TempData["NewApprovalModel"] = model;

                return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
            }

            if (model.ApprovalWorkflowId > 0)
            {
                var dicModelErrors = new Dictionary<string, string>();
                if (model.HasAdHocWorkflow)
                {
                    //using (profiler.Step("ApprovalsController.GetApprovalWorkflowCollaborators"))
                    //{
                    phasesCollaborators = ApprovalBL.GetApprovalWorkflowCollaborators(model.ApprovalWorkflowId, Context);
                    //}
                }
                else
                {
                    phasesCollaborators = JsonConvert.DeserializeObject<ApprovalWorkflowPhasesInfo>(model.PhasesCollaborators);
                }

                var phases = ApprovalBL.PhasesWithPdmDecisionHasPdm(phasesCollaborators, Context);
                if (!string.IsNullOrEmpty(phases))
                {
                    dicModelErrors.Add("collaboratorsAccess", Resources.Resources.errPdmMustBeSelectedForPhase + phases);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }

                if (!ApprovalBL.AllPhasesHaveApproverAndReviewer(phasesCollaborators))
                {
                    dicModelErrors.Add("collaboratorsAccess", Resources.Resources.SelectAtLeastOneApproverAndReviewerForEachPhase);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }
            }

            #endregion

            //using (profiler.Step("ApprovalsController.foreach Create files"))
            //{
            foreach (int key in lstTmp)
            {
                try
                {
                    if (model.IsUploadApproval)
                    {
                        var lsthtmlfileInfo = Request.Form.AllKeys.Where(o => o.Contains("fileWidth_" + key)).Select(o => int.Parse(o.Split('_').Last())).ToList();

                        foreach (var obj in lsthtmlfileInfo)
                        {
                            GMGColorDAL.ApprovalPage objApprovalPage = new GMGColorDAL.ApprovalPage();
                            objApprovalPage.Number = 0;
                            objApprovalPage.Approval = 0;
                            objApprovalPage.DPI = 0;
                            objApprovalPage.PageSmallThumbnailHeight = 0;
                            objApprovalPage.PageSmallThumbnailWidth = 0;
                            objApprovalPage.PageLargeThumbnailHeight = 0;
                            objApprovalPage.PageLargeThumbnailWidth = 0;
                            objApprovalPage.Progress = 100;
                            objApprovalPage.HTMLFilePath = Request.Form.GetValues("HtmlFileDimensionlist_" + key + "_" + obj)[0].ToString();
                            objApprovalPage.OriginalImageWidth = Convert.ToInt32(Request.Form.GetValues("fileWidth_" + key + "_" + obj)[0]) + border;
                            objApprovalPage.OriginalImageHeight = Convert.ToInt32(Request.Form.GetValues("fileHeight_" + key + "_" + obj)[0]) + border;
                            objApprovalPage.OutputRenderWidth = Convert.ToInt32(Request.Form.GetValues("fileWidth_" + key + "_" + obj)[0]) + border;
                            objApprovalPage.OutputRenderHeight = Convert.ToInt32(Request.Form.GetValues("fileHeight_" + key + "_" + obj)[0]) + border;

                            if (objApprovalPage.OriginalImageHeight == border || objApprovalPage.OriginalImageWidth == border)
                            {
                                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                                ////dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                                return RedirectWithModelAndErrors(model, dicModelErrors);
                            }

                            approvalHtmlPageList.Add(objApprovalPage);
                        }

                        var approvalTitles = Request.Form.GetValues("ApprovalTitle_" + key);
                        var appprovalType = Convert.ToInt32(Request.Form.GetValues("ApprovalType" + key)[0].ToString());
                        if (approvalTitles.Any())
                        {
                            // Split in case files are concatenated in formcollection because delete was used in file uploader and another file was uploaded causing more files to be in the same key
                            var approvalFileNames = Request.Form.GetValues("ApprovalFileName_" + key);
                            var approvalSizes = Request.Form.GetValues("ApprovalSize_" + key);
                            var approvalGuids = Request.Form.GetValues("FileGuid_" + key);

                            for (int i = 0; i < approvalTitles.Length; i++)
                            {
                                if (string.IsNullOrEmpty(approvalTitles[i]))
                                {
                                    model.IsUploadApproval = true;

                                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                                    dicModelErrors.Add("fileTitleValidator", Resources.Resources.reqDeliverJobTtile);

                                    return RedirectWithModelAndErrors(model, dicModelErrors);
                                }

                                files.Add(new ApprovalJob()
                                {
                                    FileName = approvalFileNames[i] ?? string.Empty,
                                    FileTitle = approvalTitles[i] ?? string.Empty,
                                    FileSize = decimal.Parse(approvalSizes[i] ?? string.Empty),
                                    FileGuid = approvalGuids[i] ?? string.Empty,
                                    ApprovalType = appprovalType == 0 ? 1 : appprovalType
                                    
                                });
                            }
                        }
                    }
                    else
                    {
                        files.Add(new ApprovalJob()
                        {
                            FileName = Guid.NewGuid().ToString() + ".png",
                            FileTitle = model.Url.Contains("?")
                                ? model.Url.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0]
                                : model.Url,
                            FileSize = 0,
                            FileGuid = Guid.NewGuid().ToString()
                        });
                    }
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "ProjectFoldersController.NewProjectFolderApproval : Save New Project Folder Approval Failed. {0}", ex.Message);
                    model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                    TempData["NewApprovalModel"] = model;

                    return LoadProjectFolderApprovalsIndex(model.ProjectFolder, "", "");
                }
            }
            //}

            try
            {
                string warningMessage;


                //using (profiler.Step("ApprovalsController.CheckIfJobSubmissionAllowed"))
                //{
                warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser,
                   GMG.CoZone.Common.AppModule.Collaborate, lstTmp.Count, Context,
                   LoggedAccountBillingPlans.CollaborateBillingPlan, files.Select(t => t.FileSize).Sum());
                //}
                if (!string.IsNullOrEmpty(warningMessage))
                {
                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                    dicModelErrors.Add("warningMessage", warningMessage);

                    model.HasWarnings = true;

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }

                NewApprovalResponse approvalResponse;
                int folderId;
                //using (profiler.Step("ApprovalsController.AddNewApproval"))
                //{
                folderId = ApprovalBL.AddNewProjectFolderApproval(LoggedAccountBillingPlans.CollaborateBillingPlan, LoggedAccount,
                    LoggedUser, model, files, model.WebPageSnapshotDelay.ToInt(), phasesCollaborators, Context, out approvalResponse, false, approvalHtmlPageList);
                //}
                if (folderId > 0)
                {
                    Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] = folderId;
                    Session[Constants.ApprovalsResetParameters] = false;
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorLogging.log.Error(exbl.Message, exbl);

                model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                TempData["NewApprovalModel"] = model;
                return LoadProjectFolderApprovalsIndex(model.ProjectFolder, "", "");
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);

                model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                TempData["NewApprovalModel"] = model;
                return LoadProjectFolderApprovalsIndex(model.ProjectFolder, "", "");
            }

            if (!string.IsNullOrEmpty(model.ErrorMessage))
            {
                TempData["NewApprovalModel"] = model;
                return LoadProjectFolderApprovalsIndex(model.ProjectFolder, "", "");
            }

            // Nulable the existing folder value
            Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] = null;
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "VersionProjectFolderApproval")]
        public ActionResult VersionProjectFolderApproval(int SelectedApproval, int? SelectedProjectFolderID)
        {
            try
            {
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
                {
                    LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                if (SelectedProjectFolderID == null && Session[Constants.SelectedProjectId] != null)
                {
                    SelectedProjectFolderID = int.Parse(Session[Constants.SelectedProjectId].ToString()) > 0 ? int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                }

                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Job>(SelectedApproval, LoggedAccount.ID, Context) ||
                    !GMGColorDAL.Job.CanAccess(LoggedUser.ID, new int[] { SelectedApproval }, GMGColorDAL.Approval.ApprovalOperation.AddNewVersion, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Version Approval Failed. {0}", ex.Message);
            }
            var model = NewProjectFolderApproval("", SelectedApproval, SelectedProjectFolderID.Value);
            model.ExecuteWorkflow = true;
            //ViewBag.LoggedUser = this.LoggedUser.ID;
            return View("NewVersionProjectFolderApproval", model);
        }


        #endregion


        #region Methods

        public ActionResult LoadProjectFolderApprovalsIndex(int SelectedProjectFolderID, string sort, string sortdir)
        {
            Session[Constants.SelectedProjectId] = SelectedProjectFolderID;
            if ((Session[Constants.ApprovalsResetParameters] == null) || ((Session[Constants.ApprovalsResetParameters] != null) && bool.Parse(Session[Constants.ApprovalsResetParameters].ToString())))
            {
                Session[Constants.ApprovalsCurrentPage + this.LoggedUser.ID.ToString()] = null;
                Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] = null;
                Session[Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] = null;
                Session[Constants.SelectedProjectId + this.LoggedUser.ID.ToString()] = null;
            }
            Session[Constants.SelectedProjectId + this.LoggedUser.ID.ToString()] = SelectedProjectFolderID;
            int page = (Session[Constants.ApprovalsCurrentPage + this.LoggedUser.ID.ToString()] == null) ? 1 : int.Parse(Session[Constants.ApprovalsCurrentPage + this.LoggedUser.ID.ToString()].ToString().Trim());
            int folderID = (Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] == null) ? 0 : int.Parse(Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()].ToString().Trim());
            int tabID = (Session[Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] == null) ? -1 : int.Parse(Session[Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()].ToString().Trim());

            ViewBag.ShowCanntDelete = (Session[Constants.ApprovalsCannotDelete] != null);

            // Nulable the existing folder value
            Session[Constants.ApprovalsResetParameters] = null;
            Session[Constants.ApprovalsCannotDelete] = null;
            Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] = null;

            Session[Constants.LoggedUserId] = LoggedUser.ID;
            if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
            {
                tabID = -8;
                Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] = tabID;
            }

            return PopulateApprovals(SelectedProjectFolderID ,tabID, page, sort, sortdir);

        }


        private void SetApprovalsCount(int FolderID, int topLinkID, AccountSettings.CollaborateGlobalSettings collaborateSettings)
        {
            //Check permissions to see All Approvals within account for the current user
            if ((topLinkID != (int)Utils.ApprovalTab.AllApprovals && topLinkID != (int)Utils.ApprovalTab.Archive_PastTense && topLinkID != (int)Utils.ApprovalTab.RecycleBin) || (LoggedUserCollaborateRole != Role.RoleName.AccountAdministrator && LoggedUserCollaborateRole != Role.RoleName.AccountManager))
            {
                ViewAll = false;
            }

            var viewAllFromRecycle = collaborateSettings.ShowAllFilesToAdmins && LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator;

            var advS = (GMGColorDAL.CustomModels.AdvancedSearch)Session["AdvancedSearchModel"];
            if (advS == null && topLinkID <= -Constants.TopLinkOffset)
            {
                int advsId = (-topLinkID) - Constants.TopLinkOffset;

                advS = ApprovalBL.GetAdvancedSearch(advsId, Context, LoggedAccount.DateFormat1.Pattern);
            }

            ProjectFolderApprovalCountsView objApprovalCount = GMGColorDAL.ProjectFolder.GetApprovalCounts(FolderID, collaborateSettings.HideCompletedApprovalsInDashboard, this.LoggedAccount.ID, this.LoggedUser.ID, ViewAll, viewAllFromRecycle, Role.GetRoleKey(LoggedUserCollaborateRole), Context, LoggedAccount.TimeZone, LoggedAccount.DateFormat1.Pattern, advS).SingleOrDefault();

            ViewBag.MyAppCount = objApprovalCount.OwnedByMeCount;
            ViewBag.SharedAppCount = objApprovalCount.SharedCount;
            ViewBag.RecentlyViewedCount = objApprovalCount.RecentlyViewedCount;
            ViewBag.AllAppCount = objApprovalCount.AllCount;
            ViewBag.RecycleBinCount = objApprovalCount.RecycleCount;
            ViewBag.RecentlyArchivedCount = objApprovalCount.ArchivedCount;
            ViewBag.MyOpenApprovalsCount = objApprovalCount.MyOpenApprovalsCount;
            ViewBag.SubstitutedApprovalsCount = objApprovalCount.SubstitutedApprovalCount;

        }

        public NewApprovalModel NewProjectFolderApproval(string guid, int? jobId, int? SelectedProjectFolderID)
        {

            NewApprovalModel model;

            int substituteOwnerId = 0;
            if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
            {
                substituteOwnerId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                GMGColorDAL.User substituteUser = Context.Users.FirstOrDefault(u => u.ID == substituteOwnerId);
                this.LoggedUser = substituteUser;
            }

            if (LoggedUserCollaborateRole == Role.RoleName.AccountViewer || LoggedUserCollaborateRole == Role.RoleName.AccountContributor || LoggedUserCollaborateRole == Role.RoleName.AccountRetoucher)
            {
                return null;
            }

            AccountFileTypeFilter fileTypeFilter;
            if (TempData["NewApprovalModel"] != null)
            {
                model = (NewApprovalModel)TempData["NewApprovalModel"];
                model.objLoggedAccount = LoggedAccount;
                fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(LoggedUser.ID, Context);
                TempData["NewApprovalModel"] = null;

                if (TempData["ModelErrors"] != null)
                {
                    var dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                    TempData["ModelErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }
            }
            else
            {
                model = new NewApprovalModel(this.LoggedAccount, LoggedUser.ID, this.LoggedUserTempFolderPath, Context, out fileTypeFilter);
            }

            if (!string.IsNullOrEmpty(guid))
            {
                jobId = JobBL.GetJobIdByAppGuid(guid, Context);
            }

            model.Folder = (Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] == null) ? 0 : int.Parse(Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()].ToString().Trim());
            model.Job = model.Job > 0 ? model.Job : (jobId == null) ? 0 : jobId.Value;
            model.ProjectFolder = SelectedProjectFolderID.Value;
            ApprovalBL.PopulateNewApprovalModel(model, LoggedUser.ID, Context);

            if (model.PhaseCollaboratorsModel != null)
            {
                model.PhasesCollaborators = JsonConvert.SerializeObject(model.PhaseCollaboratorsModel);
            }

            // Get existing foldes before created new ones
            if (Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] == null)
            {
                var aFolders = DALUtils.SearchObjects<GMGColorDAL.Folder>(o => o.Creator == this.LoggedUser.ID, Context).Select(o => o.ID).ToArray();
                if (aFolders.Length == 0)
                    aFolders = new int[] { this.LoggedUser.ID };

                Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] = aFolders;
            }

            string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser, GMG.CoZone.Common.AppModule.Collaborate, 0, Context, LoggedAccountBillingPlans.CollaborateBillingPlan);
            if (!string.IsNullOrEmpty(warningMessage))
            {
                model.HasWarnings = true;
                ModelState.AddModelError("warningMessage", warningMessage);
            }
            bool applyBatchUploadSettings = fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval;
            ViewBag.UseSettingsFromPreviousVerion = model.Job == 0 &&
                                        applyBatchUploadSettings &&
                                        fileTypeFilter.UploadSettingsFilter.UseSettingsFromPreviousVersion;

            ViewBag.FileTypesAllowed = ApprovalBL.GetAllowedUploadFileTypes(fileTypeFilter);
            ViewBag.IsDisableTagwordsInDashbord = ApprovalBL.IsDisableTagwordsInDashbord(LoggedUser.Account, Context);
            ViewBag.AppyApprovalBatchSettings = applyBatchUploadSettings;

            return model;
        }

        private ActionResult RedirectWithModelAndErrors(NewApprovalModel model, Dictionary<string, string> dicModelErrors)
        {
            TempData["NewApprovalModel"] = model;
            TempData["ModelErrors"] = dicModelErrors;

            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }
        

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "UploadFiles")]
        public ActionResult UploadFiles(int SelectedProjectFolderID)
        {

            return null;
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SelectedStatus")]
        public ActionResult SelectedStatus(string SelectedStatus)
        {
            Session[Constants.ProjectStatusFilter] = SelectedStatus;

            return RedirectToAction("Index", new { status = HttpUtility.HtmlEncode(SelectedStatus) });

        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SelectedFilter")]
        public ActionResult SelectedFilter(string SelectedFilter)
        {
            Session[Constants.ProjectFilterField] = SelectedFilter;

            return RedirectToAction("Index", new { sort = HttpUtility.HtmlEncode(SelectedFilter) });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SelectedSortingOrder")]
        public ActionResult SelectedSortingOrder(string SortingOrder)
        {
            Session[Constants.ProjectFilterDir] = SortingOrder;

            return RedirectToAction("Index", new { sortAsc = HttpUtility.HtmlEncode(SortingOrder) });
        }


        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteProjectFolder")]
        public ActionResult DeleteProjectFolder(string ProjectFolderIds)
        {
            try
            {
                List<int> projectIds = ProjectFolderIds.Split(',').Select(Int32.Parse).ToList<int>();
                if(projectIds.Count > 0 && !ApprovalBL.CheckUserCanAccess(projectIds, GMGColorDAL.Approval.CollaborateValidation.DeleteProjectFolder, LoggedUser.ID, Context))
                {
                    return RedirectToAction("Index");
                }
                int?[] lstFolderIdsIds = ProjectFolderIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();
                foreach (int? folderId in lstFolderIdsIds)
                {
                    if (folderId != null)
                    {
                        DeleteProjectFolder(folderId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("ProjectFolders.DeleteProjectFolder --> " + ex.Message, ex);
            }
            return RedirectToAction("Index");
        }

        private void DeleteProjectFolder(int folderId)
        {

            try
            {
                var objProjectFolder = (from a in Context.ProjectFolders where a.ID == folderId select a).FirstOrDefault();

                if (objProjectFolder != null)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        objProjectFolder.IsDeleted = true;
                        objProjectFolder.Modifier = this.LoggedUser.ID;
                        objProjectFolder.ModifiedDate = DateTime.UtcNow;
                        Context.SaveChanges();
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("ProjectFolderController.DeleteProjectFolder --> " + ex.Message, ex);
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "MultiDownload")]
        public ActionResult MultiDownload(string ProjectFolderIds)
        {
            return null;
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ArchiveProjectFolder")]
        public ActionResult ArchiveProjectFolder(string ProjectFolderIds)
        {
            try
            {
                List<int> projectIds = ProjectFolderIds.Split(',').Select(Int32.Parse).ToList<int>();
                if (projectIds.Count > 0 && !ApprovalBL.CheckUserCanAccess(projectIds, GMGColorDAL.Approval.CollaborateValidation.ArchiveProjectFolder, LoggedUser.ID, Context))
                {
                    return RedirectToAction("Index");
                }
                    int?[] lstFolderIdsIds = ProjectFolderIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();

                foreach (int? folderId in lstFolderIdsIds)
                {
                    if (folderId != null)
                    {
                        ProjectFolderBL.ArchiveProjectFolder(folderId.Value, LoggedUser.ID, Context);
                        Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("ProjectFolders.ArchiveFolder --> " + ex.Message, ex);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "UnarchiveProjectFolder")]
        public ActionResult UnarchiveProjectFolder(string ProjectFolderIds)
        {
            try
            {
                List<int> projectIds = ProjectFolderIds.Split(',').Select(Int32.Parse).ToList<int>();
                if (projectIds.Count > 0 && !ApprovalBL.CheckUserCanAccess(projectIds, GMGColorDAL.Approval.CollaborateValidation.UnArchiveProjectFolder, LoggedUser.ID, Context))
                {
                    return RedirectToAction("Index");
                }
                int?[] lstFolderIdsIds = ProjectFolderIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();

                foreach (int? folderId in lstFolderIdsIds)
                {
                    if (folderId != null)
                    {
                        RestoreProjectFolder(folderId.Value);
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                GMGColorLogging.log.Error("ProjectFolders.MultiUnarchiveProjectFolder --> " + ex.Message, ex);

            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RestoreProjectFolder")]
        public ActionResult RestoreProjectFolder(int SelectedProjectFolderID)
        {
            try
            {
                List<int> projectIds = new List<int>();
                projectIds.Add(SelectedProjectFolderID);
                if (projectIds.Count > 0 && !ApprovalBL.CheckUserCanAccess(projectIds, GMGColorDAL.Approval.CollaborateValidation.UnArchiveProjectFolder, LoggedUser.ID, Context))
                {
                    return RedirectToAction("Index");
                }
                ProjectFolderBL.RestoreProjectFolder(SelectedProjectFolderID, LoggedUser.ID, Context);
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                GMGColorLogging.log.Error("ProjectFolders.RestoreProjectFolder --> " + ex.Message, ex);

            }
            return RedirectToAction("Index");
        }

        #region Approvals controller POST Actions
        [HttpPost]
        [Compress]
        public int SaveImagePreviews(List<string> imagesToBase64, List<int> approvalsIDs, int imageSerialNumber)
        {
            for (int imageToBase64 = 0; imageToBase64 < imagesToBase64.Count; imageToBase64++)
            {
                var currentId = approvalsIDs[imageToBase64];
                ApprovalAnnotationReport report = new ApprovalAnnotationReport();

                report.Approval = currentId;
                report.SerialNumber = imageSerialNumber;
                report.Base64String = imagesToBase64[imageToBase64];
                report.User = LoggedUser.ID;

                Context.ApprovalAnnotationReports.Add(report);
                imageSerialNumber++;
            }

            Context.SaveChanges();
            return imageSerialNumber;
        }

        [HttpPost]
        [Compress]
        public async System.Threading.Tasks.Task<string> GenerateAnnotationPDFReport(List<string> imagesToBase64, List<int> approvalsIDs, bool showSummaryPage, List<string> pageNumbers, List<string> annotationIds, List<string> approvalPageIds)
        {

            // Create a new PDF document
            PdfDocument document = new PdfDocument();

            PageSize pageSize = PdfSharp.PageSize.A4;


            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, LoggedAccount.TimeZone);
            DateTime createdUserTime = GMGColorFormatData.GetUserTimeFromUTC(userCurTime, LoggedAccount.TimeZone);

            var approvalId = 0;
            bool isFirstPage = false;
            int annotationOrderNumber = 1;
            var approvalInfo = new ApprovalAnnotationsModel();

            int[] approvalPageId1 = approvalPageIds.Select(int.Parse).ToArray();

            List<int> lstAnnotationIds = new List<int>();

            foreach (var obj in annotationIds)
            {
                lstAnnotationIds.AddRange(obj.Split(',').Select(int.Parse).ToList());
            }

            var ApprovalAnnotationsInfo1 = AnnotationBL.GetApprovalAnnotationsInfo1(approvalPageId1, lstAnnotationIds, Context);
            List<System.Threading.Tasks.Task> AsyncList = new List<System.Threading.Tasks.Task>();

            for (int imageToBase64 = 0; imageToBase64 < imagesToBase64.Count; imageToBase64++)
            {
                var image = ConvertBase64ToImage(imagesToBase64[imageToBase64]);
                XImage xImage = XImage.FromGdiPlusImage(image);

                if (xImage.PixelWidth >= 2 * xImage.PixelHeight)
                {
                    pageSize = PdfSharp.PageSize.A3;
                }
                else
                {
                    pageSize = PdfSharp.PageSize.A4;
                }

                var currentId = approvalsIDs[imageToBase64];

                if (approvalId != currentId)
                {
                    annotationOrderNumber = 1;
                    isFirstPage = true;
                    approvalId = currentId;
                    approvalInfo = ApprovalBL.GetApprovalInfo(currentId, LoggedUser, LoggedAccount, Context);

                    if (approvalInfo.ApprovalName.Length > 40)
                    {
                        approvalInfo.ApprovalName = approvalInfo.ApprovalName.Insert(40, " ");
                    }

                    if (approvalInfo.JobTitle.Length > 40)
                    {
                        approvalInfo.JobTitle = approvalInfo.JobTitle.Insert(40, " ");
                    }
                }


                if (pageSize == PdfSharp.PageSize.Undefined)
                    continue;


                //set page orientation to landscape if width > height, otherwise the orientation will be portrait (default)

                var orientation = xImage.PixelWidth > xImage.PixelHeight
                                          ? PageOrientation.Landscape
                                          : PageOrientation.Portrait;

                if (isFirstPage && showSummaryPage)
                {
                    isFirstPage = false;
                    //CreateApprovalDetailsPageForPDF(document, approvalInfo, orientation, pageSize);
                }

                PdfPage page = document.AddPage();
                if (orientation == PageOrientation.Landscape)
                {
                    XSize size = PageSizeConverter.ToSize(pageSize);
                    page.MediaBox = new PdfSharp.Pdf.PdfRectangle(new XPoint(0, 0), new XPoint(size.Height, size.Width)); // Magic: swap width and height

                }
                else
                {
                    page.Orientation = orientation;
                    page.Size = pageSize;
                }
                page.TrimMargins.Top = 10;
                page.TrimMargins.Right = 10;
                page.TrimMargins.Bottom = 10;
                page.TrimMargins.Left = 10;

                XGraphics gfx = XGraphics.FromPdfPage(page);
                XFont font = new XFont("Calibri", 11, XFontStyle.Regular);
                gfx.MUH = PdfFontEncoding.Unicode;
                gfx.MFEH = PdfFontEmbedding.Default;
                int[] approvalPageId = approvalPageIds[imageToBase64].Split(',').Select(int.Parse).ToArray();
                int[] annotations = annotationIds[imageToBase64].Split(',').Select(int.Parse).ToArray();
                var ApprovalAnnotationsInfo = (from itm in ApprovalAnnotationsInfo1 where itm.ApprovalPageId == approvalPageId[0] && annotations.Contains(itm.ID) select itm).ToArray();

                //Create MigraDoc document
                var doc = new Document();
                Section section = doc.AddSection();
                var table = section.AddTable();
                table.Borders.Width = 0.75;

                var emptyRow = " ";
                var columnTitleColor = Colors.Gray;
                const bool columnTitleBold = true;
                const int columnTitleFontSize = 8;

                //Calculate the width of each column base on page width
                var pageWidth = Math.Round(page.Width * ApprovalBL.OnePointInCm, 1); //Convert points to cm
                var firstColumnWidth = Math.Round((13 * pageWidth) / 100, 1);
                var secondColumnWidth = Math.Round((36 * pageWidth) / 100, 1);
                var fourthColumnWidth = Math.Round((34 * pageWidth) / 100, 1);

                //First column
                Column column = table.AddColumn(Unit.FromCentimeter(firstColumnWidth));
                column.Format.Font.Name = "Arial";
                column.Format.Font.Color = columnTitleColor;
                column.Format.Font.Bold = columnTitleBold;
                column.Format.Font.Size = columnTitleFontSize;

                //Second column
                table.AddColumn(Unit.FromCentimeter(secondColumnWidth));

                //Third column
                column = table.AddColumn(Unit.FromCentimeter(firstColumnWidth));
                column.Format.Font.Name = "Arial";
                column.Format.Font.Color = columnTitleColor;
                column.Format.Font.Bold = columnTitleBold;
                column.Format.Font.Size = columnTitleFontSize;


                //Fourth coulmn
                table.AddColumn(Unit.FromCentimeter(fourthColumnWidth));

                //First row and its cells
                Row row = table.AddRow();
                row.Borders.Color = Colors.Gray;
                row.Borders.Width = 0.75;

                Cell cell = row.Cells[0];
                cell.AddParagraph(Resources.Resources.lblApprovalNamepdf.ToUpper());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[1];
                cell.AddParagraph(approvalInfo.JobTitle);
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[2];
                cell.AddParagraph(Resources.Resources.lblFileNamepdf.ToUpper());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[3];
                cell.AddParagraph(approvalInfo.ApprovalName);
                cell.VerticalAlignment = VerticalAlignment.Center;

                //Second row
                row = table.AddRow();
                row.Borders.Color = Colors.Gray;
                row.Borders.Width = 0.75;

                cell = row.Cells[0];
                cell.AddParagraph(Resources.Resources.lblUploadDate.ToUpper());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[1];
                cell.AddParagraph(approvalInfo.CreatedDate);
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[2];
                cell.AddParagraph(Resources.Resources.lblReportGenerationDate.ToUpper());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[3];
                cell.AddParagraph(String.Format("{0} {1}", GMGColorFormatData.GetFormattedDate(userCurTime, LoggedAccount.DateFormat1.Pattern), createdUserTime.ToString("h:mm tt")));
                cell.VerticalAlignment = VerticalAlignment.Center;

                //Third row
                row = table.AddRow();
                row.Borders.Color = Colors.Gray;
                row.Borders.Width = 0.75;

                cell = row.Cells[0];
                cell.AddParagraph(Resources.Resources.lblVersion.ToUpper());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[1];
                cell.AddParagraph(approvalInfo.Version.ToString());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[2];
                cell.AddParagraph(Resources.Resources.lblApprovalOwner.ToUpper());
                cell.VerticalAlignment = VerticalAlignment.Center;

                cell = row.Cells[3];
                cell.AddParagraph(approvalInfo.Creator);
                cell.VerticalAlignment = VerticalAlignment.Center;


                //Fourth row
                if (approvalInfo.IsApprovalTypeVideo)
                {
                    row = table.AddRow();
                    row.Borders.Color = Colors.Gray;
                    row.Borders.Width = 0.75;

                    cell = row.Cells[0];
                    cell.AddParagraph(Resources.Resources.lblTimeFrame.ToUpper());
                    cell.VerticalAlignment = VerticalAlignment.Center;

                    var longtime = Math.Round((double)ApprovalAnnotationsInfo[0].AnnotationInfo.AnnotationTimeFrame / 1000000);
                    var mins = longtime >= 60 ? Math.Round(longtime / 60) : 0;

                    cell = row.Cells[1];
                    cell.AddParagraph(string.Format("{0:00}", mins) + ":" + string.Format("{0:00}", Math.Round(longtime) % 60));
                    cell.VerticalAlignment = VerticalAlignment.Center;
                    cell.Borders.Right.Color = Colors.Transparent;

                    cell = row.Cells[2];
                    cell.Borders.Right.Color = Colors.Transparent;
                    cell.Borders.Left.Color = Colors.Transparent;

                    cell = row.Cells[3];
                    cell.Borders.Left.Color = Colors.Transparent;
                }

                table.Rows.Height = 25;
                table.SetEdge(0, 0, 4, 3, Edge.Box, BorderStyle.Single, 0.75, Colors.Gray);

                //Create titles
                Paragraph title = section.AddParagraph();
                FormattedText formattedText = title.AddFormattedText(approvalInfo.JobTitle, TextFormat.Bold);
                formattedText.Font.Size = 12;
                formattedText.Font.Color = Colors.Black;
                formattedText.AddSpace(2);

                var documentRenderer = new DocumentRenderer(doc);
                documentRenderer.PrepareDocument();

                documentRenderer.RenderObject(gfx, XUnit.FromCentimeter(0.7), XUnit.FromCentimeter(1), "6cm", table);

                double imgWidth;
                double imgHeight;
                if (xImage.PixelWidth > 1200)
                {
                    double ratio = (double)xImage.PixelWidth / (double)xImage.PixelHeight;
                    var portionOfImageOnPageWidth = page.Width - page.Width * (0.20);
                    imgWidth = portionOfImageOnPageWidth;
                    imgHeight = imgWidth / ratio;

                }
                else if (xImage.PixelHeight > 1600)
                {
                    double ratio = (double)xImage.PixelWidth / (double)xImage.PixelHeight;
                    var portionOfImageOnPageHeight = page.Height - page.Height * (0.20);
                    imgHeight = portionOfImageOnPageHeight;
                    imgWidth = imgHeight * ratio;
                }
                else
                {
                    var width = xImage.PixelWidth * 72 / xImage.HorizontalResolution;
                    var height = xImage.PixelHeight * 72 / xImage.HorizontalResolution;

                    //scale the image
                    var ratioX = page.Width / width;
                    var ratioY = page.Height / height;
                    var ratio = Math.Min(ratioX, ratioY);

                    imgWidth = (int)(width * ratio);
                    imgHeight = (int)(height * ratio);
                }
                //check remaning space between right margin of the page and iamge, if is bigger than 100 points then change the left position of the image
                //otherwise the left position is 0
                //this check was added for CZ-988

                var doc1 = new Document();
                Section section1 = doc.AddSection();
                var table1 = section1.AddTable();
                var leftIndent = 0;

                table1.Borders.Width = 0.75;
                if (orientation == PageOrientation.Portrait)
                {
                    table1.Format.Font.Size = 8;
                    leftIndent = 15;
                }
                else
                {
                    table1.Format.Font.Size = 11;
                    leftIndent = 18;
                }

                //Calculate the width of each column base on page width
                var pageWidth1 = Math.Round(page.Width * ApprovalBL.OnePointInCm, 1); //Convert points to cm
                var firstColumnWidth1 = Math.Round((33 * pageWidth) / 100, 1);

                table1.AddColumn(Unit.FromCentimeter(firstColumnWidth1));
                Row row1 = null;
                Cell cell1 = null;
                double top = 5;

                for (var annotation = 0; annotation < annotations.Count(); annotation++)
                {
                    ApprovalAnnotationInfo annotationInfo = new ApprovalAnnotationInfo();
                    foreach (var ann in ApprovalAnnotationsInfo)
                    {
                        if (ann.ID == annotations[annotation])
                        {
                            annotationInfo = ann.AnnotationInfo;
                            annotationInfo.AnnotationReplies = ann.AnnotationReplies;
                        }
                    }

                    annotationInfo.AnnotationOrderNumber = annotationOrderNumber;
                    annotationOrderNumber++;

                    row1 = table1.AddRow();
                    row1.Borders.Color = Colors.Gainsboro;
                    row1.Borders.Width = 0.75;
                    row1.Shading.Color = Colors.Gainsboro;

                    var date = GMGColorFormatData.GetFormattedDate(annotationInfo.AnnotatedDate, LoggedAccount.DateFormat1.Pattern);
                    var time = GMGColorFormatData.GetUserTimeFromUTC(annotationInfo.AnnotatedDate, LoggedAccount.TimeZone).ToString("h:mm tt");
                    var annotatedDate = date + " " + time;
                    var annotationNumber = "";

                    cell1 = row1.Cells[0];
                    if (annotationInfo.AnnotationOrderNumber < 10)
                    {
                        annotationNumber = "0" + annotationInfo.AnnotationOrderNumber + " " + annotationInfo.AnnotationOwner;
                    }
                    else
                    {
                        annotationNumber = annotationInfo.AnnotationOrderNumber + " " + annotationInfo.AnnotationOwner;
                    }
                    if (annotationInfo.UserStatus == (int)UserStatus.Inactive)
                    {
                        annotationNumber = annotationNumber + " " + "(INACTIVE)";
                    }
                    cell1.AddParagraph(annotationNumber);
                    if (annotationInfo.AnnotationPhaseName != null)
                    {
                        var phaseNameWithTime = annotationInfo.AnnotationPhaseName + "-" + annotatedDate;
                        cell1.AddParagraph(phaseNameWithTime).Format.LeftIndent = leftIndent;
                    }
                    else
                    {
                        cell1.AddParagraph(annotatedDate).Format.LeftIndent = leftIndent;
                    }
                    cell1.VerticalAlignment = VerticalAlignment.Center;
                    cell1.Format.Font.Bold = true;

                    row1 = table1.AddRow();
                    row1.Borders.Color = Colors.Gainsboro;
                    row1.Borders.Width = 0.75;
                    cell1 = row1.Cells[0];
                    if (!annotationInfo.AnnotationComment.Contains(" ") && annotationInfo.AnnotationComment.Length > 30)
                    {
                        var noOfLines = annotationInfo.AnnotationComment.Length / 30;
                        for (int i = 1; i <= noOfLines; i++)
                        {
                            annotationInfo.AnnotationComment = annotationInfo.AnnotationComment.Insert(30 * i, "\n");
                        }
                    }
                    cell1.AddParagraph(annotationInfo.AnnotationComment.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("\n", " ").Replace("<p>", "").Replace("</p>", "").Replace("<strong>", "").Replace("</strong>", "").Replace("<em>", "").Replace("</em>", "").Replace("<u>", "").Replace("</u>", ""));
                    cell1.AddParagraph(emptyRow);
                    if (annotationInfo.AnnotationAttachments.Count > 0)
                    {
                        cell1.AddParagraph(Resources.Resources.lblAttachments.ToUpper()).Format.Font.Bold = true;
                        foreach (var attachment in annotationInfo.AnnotationAttachments)
                        {
                            cell1.AddParagraph(attachment);
                        }
                        cell1.AddParagraph(emptyRow);
                    }
                    if (annotationInfo.AnnotationChecklist != null)
                    {
                        var checklist = Resources.Resources.lblChecklist.ToUpper() + ":" + annotationInfo.AnnotationChecklist;
                        cell1.AddParagraph(checklist).Format.Font.Bold = true;
                        foreach (var checklistItem in annotationInfo.AnnotationChecklistItems)
                        {
                            var checklistName = checklistItem.ChecklistItemName + ":" + checklistItem.ChecklistItemValue;
                            cell1.AddParagraph(checklistName);
                        }
                        cell1.AddParagraph(emptyRow);
                    }
                    cell1.VerticalAlignment = VerticalAlignment.Center;
                    row1.Format.LeftIndent = leftIndent;

                    foreach (var reply in annotationInfo.AnnotationReplies)
                    {
                        row1 = table1.AddRow();
                        row1.Borders.Color = Colors.Gainsboro;
                        row1.Borders.Width = 0.75;
                        cell1 = row1.Cells[0];
                        var date1 = GMGColorFormatData.GetFormattedDate(reply.AnnotatedDate, LoggedAccount.DateFormat1.Pattern);
                        var time1 = GMGColorFormatData.GetUserTimeFromUTC(reply.AnnotatedDate, LoggedAccount.TimeZone).ToString("h:mm tt");
                        var annotatedDate1 = date1 + " " + time1;

                        if (reply.UserStatus == (int)UserStatus.Inactive)
                        {
                            cell1.AddParagraph(reply.AnnotationOwner + " " + "(INACTIVE)").Format.Font.Bold = true;
                        }
                        else
                        {
                            cell1.AddParagraph(reply.AnnotationOwner).Format.Font.Bold = true;
                        }
                        if (reply.AnnotationPhaseName != null)
                        {
                            var phaseNameWithTime = reply.AnnotationPhaseName + "-" + annotatedDate1;
                            cell1.AddParagraph(phaseNameWithTime).Format.Font.Bold = true;
                        }
                        else
                        {
                            cell1.AddParagraph(annotatedDate1).Format.Font.Bold = true;
                        }
                        cell1.AddParagraph(reply.AnnotationComment.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("</p>", "").Replace("<strong>", "").Replace("</strong>", "").Replace("<em>", "").Replace("</em>", "").Replace("<u>", "").Replace("</u>", ""));
                        cell1.AddParagraph(emptyRow);
                        row1.Format.LeftIndent = leftIndent;
                    }

                    row1 = table1.AddRow();
                    row1.Borders.Color = Colors.Gainsboro;
                    row1.Borders.Width = 0.75;
                    cell1 = row1.Cells[0];
                    cell1.AddParagraph(emptyRow);
                    cell1.AddParagraph(emptyRow);
                    cell1.Borders.Color = Colors.White;
                    cell1.Shading.Color = Colors.White;
                }


                table1.Rows.Height = 25;
                table1.SetEdge(0, 0, 1, 2, Edge.Box, BorderStyle.Single, 0.75, Colors.Gainsboro);

                AsyncList.Add(System.Threading.Tasks.Task.Factory.StartNew(() => RenderPDFReportAsync(gfx, xImage, imgWidth, imgHeight, doc, pageSize, orientation, top, table1)));

                //removing annotationids from array so as to make it null for next loop
                annotations = null;
                //when img is almost square the page number should be positioned based on the image position in order to avoid the overlapping
                bool isSquareImg = (page.Height - imgHeight) < 100;

                //add Pdf page number in the footer
                gfx.DrawString(String.Format("{0}", imageToBase64 + 1), font, XBrushes.Black,
                                isSquareImg
                                    ? new XRect((page.Width / 2), (page.Height + 55), 0, 0)
                                    : new XRect(0, 0, page.Width, page.Height), XStringFormats.BottomCenter);

            }

            await System.Threading.Tasks.Task.WhenAll(AsyncList);

            // Save the document to stream.
            var ms = new MemoryStream();
            document.Save(ms);

            return Convert.ToBase64String(ms.ToArray());
        }

        private void RenderPDFReportAsync(XGraphics gfx, XImage xImage, double imgWidth, double imgHeight, Document doc, PageSize pageSize, PageOrientation orientation, double top, Table table1)
        {
            gfx.DrawImage(xImage, 0, 120, imgWidth, imgHeight);

            var documentRenderer1 = new DocumentRenderer(doc);
            documentRenderer1.PrepareDocument();

            if (pageSize == PdfSharp.PageSize.A3)
            {
                documentRenderer1.RenderObject(gfx, XUnit.FromCentimeter(27.9), XUnit.FromCentimeter(top), "6cm", table1);
            }
            else if (orientation == PageOrientation.Landscape)
            {
                documentRenderer1.RenderObject(gfx, XUnit.FromCentimeter(19.8), XUnit.FromCentimeter(top), "6cm", table1);
            }
            else
            {
                documentRenderer1.RenderObject(gfx, XUnit.FromCentimeter(14), XUnit.FromCentimeter(top), "6cm", table1);
            }
        }

        [HttpPost]
        [Compress]
        public async System.Threading.Tasks.Task<string> GeneratePDF(List<string> imagesToBase64, List<int> approvalsIDs, bool showSummaryPage, List<string> pageNumbers)
        {
            // Create a new PDF document
            PdfDocument document = new PdfDocument();
            PageSize pageSize = PdfSharp.PageSize.A4;
            List<System.Threading.Tasks.Task> AsyncList = new List<System.Threading.Tasks.Task>();

            var approvalId = 0;
            bool isFirstPage = false;
            var approvalInfo = new ApprovalAnnotationsModel();

            for (int i = 0; i < imagesToBase64.Count; i++)
            {
                var currentId = approvalsIDs[i];

                if (approvalId != currentId)
                {
                    isFirstPage = true;
                    approvalId = currentId;
                    approvalInfo = ApprovalBL.GetApprovalInfo(currentId, LoggedUser, LoggedAccount, Context);
                }

                if (pageSize == PdfSharp.PageSize.Undefined)
                    continue;

                var image = ConvertBase64ToImage(imagesToBase64[i]);
                XImage xImage = XImage.FromGdiPlusImage(image);

                //set page orientation to landscape if width > height, otherwise the orientation will be portrait (default)
                var orientation = xImage.PixelWidth > xImage.PixelHeight
                                        ? PageOrientation.Landscape
                                        : PageOrientation.Portrait;

                if (isFirstPage && showSummaryPage)
                {
                    isFirstPage = false;
                    CreateApprovalDetailsPage(document, approvalInfo, orientation, pageSize);
                }

                PdfPage page = document.AddPage();
                if (orientation == PageOrientation.Landscape)
                {
                    XSize size = PageSizeConverter.ToSize(pageSize);
                    page.MediaBox = new PdfSharp.Pdf.PdfRectangle(new XPoint(0, 0), new XPoint(size.Height, size.Width)); // Magic: swap width and height

                }
                else
                {
                    page.Orientation = orientation;
                    page.Size = pageSize;
                }
                page.TrimMargins.Top = 10;
                page.TrimMargins.Right = 10;
                page.TrimMargins.Bottom = 80;
                page.TrimMargins.Left = 10;

                XGraphics gfx = XGraphics.FromPdfPage(page);
                XFont font = new XFont("Calibri", 11, XFontStyle.Regular);

                if (isFirstPage || !showSummaryPage)
                {
                    isFirstPage = false;

                    var title = approvalInfo.JobTitle.Length > 60
                                    ? approvalInfo.JobTitle.Substring(0, 60) + "..."
                                    : approvalInfo.JobTitle;

                    // add the job title and upload date
                    var jobTitleFont = new XFont("Arial", 12, XFontStyle.Bold);
                    gfx.DrawString(title, jobTitleFont, XBrushes.Black, new XRect(2, 20, 0, 0));

                    //Get the length, in points, of the string created above
                    XSize size = gfx.MeasureString(approvalInfo.JobTitle, jobTitleFont);

                    //add the date to the end of the job title
                    var dateFont = new XFont("Arial", 9, XFontStyle.Regular);
                    gfx.DrawString(approvalInfo.CreatedDate, dateFont, XBrushes.Gray, new XRect(size.Width + 7, 20, 0, 0));
                }

                //Show the document page number if is multipage document
                if (approvalInfo.NrOfPages > 1)
                {
                    // add the document page number
                    gfx.DrawString(Resources.Resources.lblPage + " " + pageNumbers[i], font, XBrushes.Black, new XRect(page.Width - 50, 20, 0, 0));
                }

                //transform image width/height into points because the page size is represented in points
                double width = xImage.PixelWidth * 72 / xImage.HorizontalResolution;
                double height = xImage.PixelHeight * 72 / xImage.HorizontalResolution;

                //scale the image
                var ratioX = page.Width / width;
                var ratioY = page.Height / height;
                var ratio = Math.Min(ratioX, ratioY);

                var imgWidth = (int)(width * ratio);
                var imgHeight = (int)(height * ratio);

                //check remaning space between right margin of the page and iamge, if is bigger than 100 points then change the left position of the image
                //otherwise the left position is 0
                //this check was added for CZ-988
                var imageLeftPosition = (page.Width - imgWidth) > 100 ? (page.Width - imgWidth) / 2 : 0;

                /*RenderViewDetailsReport(gfx, imageLeftPosition, xImage, imgWidth, imgHeight, page, font, i)*/
                ;

                AsyncList.Add(System.Threading.Tasks.Task.Factory.StartNew(() => RenderViewDetailsReportAsync(gfx, imageLeftPosition, xImage, imgWidth, imgHeight, page, font, i)));

            }
            await System.Threading.Tasks.Task.WhenAll(AsyncList);

            // Save the document to stream.
            var ms = new MemoryStream();
            document.Save(ms);

            return Convert.ToBase64String(ms.ToArray());
        }

        private void RenderViewDetailsReportAsync(XGraphics gfx, double imageLeftPosition, XImage xImage, int imgWidth, int imgHeight, PdfPage page, XFont font, int i)
        {
            gfx.DrawImage(xImage, imageLeftPosition, 30, imgWidth, imgHeight);

            //when img is almost square the page number should be positioned based on the image position in order to avoid the overlapping
            bool isSquareImg = (page.Height - imgHeight) < 100;

            //add Pdf page number in the footer
            gfx.DrawString(String.Format("{0}", i + 1), font, XBrushes.Black,
                            isSquareImg
                                ? new XRect((page.Width / 2), (page.Height + 55), 0, 0)
                                : new XRect(0, 0, page.Width, page.Height), XStringFormats.BottomCenter);
        }


        private void CreateApprovalDetailsPage(PdfDocument document, ApprovalAnnotationsModel approvalInfo, PageOrientation pageOrientation, PageSize pageSize)
        {
            //Create details page
            PdfPage page = document.AddPage();

            //PDFSharp margins are truncated in right side for landscape mode, instead swap width and height when creating the page.
            // Portrait orientation works well with margin
            if (pageOrientation == PageOrientation.Landscape)
            {
                XSize size = PageSizeConverter.ToSize(pageSize);
                page.MediaBox = new PdfSharp.Pdf.PdfRectangle(new XPoint(0, 0), new XPoint(size.Height, size.Width)); // Magic: swap width and height
            }
            else
            {
                page.Orientation = pageOrientation;
                page.Size = pageSize;
            }
            page.TrimMargins.Top = 10;
            page.TrimMargins.Right = 10;
            page.TrimMargins.Bottom = 10;
            page.TrimMargins.Left = 10;

            XGraphics gfx = XGraphics.FromPdfPage(page);
            gfx.MUH = PdfFontEncoding.Unicode;
            gfx.MFEH = PdfFontEmbedding.Default;

            //Create MigraDoc document
            var doc = new Document();
            Section section = doc.AddSection();
            var table = section.AddTable();
            table.Borders.Width = 0.75;

            var columnTitleColor = Colors.Gray;
            const bool columnTitleBold = true;
            const int columnTitleFontSize = 8;

            //Calculate the width of each column base on page width
            var pageWidth = Math.Round(page.Width * ApprovalBL.OnePointInCm, 1); //Convert points to cm
            var firstColumnWidth = Math.Round((13 * pageWidth) / 100, 1);
            var secondColumnWidth = Math.Round((52.8 * pageWidth) / 100, 1);
            var fourthColumnWidth = Math.Round((15 * pageWidth) / 100, 1);

            //First column
            Column column = table.AddColumn(Unit.FromCentimeter(firstColumnWidth));
            column.Format.Font.Name = "Arial";
            column.Format.Font.Color = columnTitleColor;
            column.Format.Font.Bold = columnTitleBold;
            column.Format.Font.Size = columnTitleFontSize;

            //Second column
            table.AddColumn(Unit.FromCentimeter(secondColumnWidth));

            //Third column
            column = table.AddColumn(Unit.FromCentimeter(firstColumnWidth));
            column.Format.Font.Name = "Arial";
            column.Format.Font.Color = columnTitleColor;
            column.Format.Font.Bold = columnTitleBold;
            column.Format.Font.Size = columnTitleFontSize;

            //Fourth coulmn
            table.AddColumn(Unit.FromCentimeter(fourthColumnWidth));

            //First row and its cells
            Row row = table.AddRow();
            row.Borders.Color = Colors.Gray;
            row.Borders.Width = 0.75;

            Cell cell = row.Cells[0];
            cell.AddParagraph(Resources.Resources.lblName.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[1];
            cell.AddParagraph(approvalInfo.ApprovalName);
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[2];
            cell.AddParagraph(Resources.Resources.lblPages.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[3];
            cell.AddParagraph(approvalInfo.NrOfPages.ToString());
            cell.VerticalAlignment = VerticalAlignment.Center;

            //Second row
            row = table.AddRow();
            row.Borders.Color = Colors.Gray;
            row.Borders.Width = 0.75;

            cell = row.Cells[0];
            cell.AddParagraph(Resources.Resources.lblUploaded.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[1];
            cell.AddParagraph(String.Format("{0} {1} {2}", approvalInfo.CreatedDate, Resources.Resources.lblBy, approvalInfo.Creator));
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[2];
            cell.AddParagraph(Resources.Resources.lblComments.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[3];
            cell.AddParagraph(approvalInfo.Comments.ToString());
            cell.VerticalAlignment = VerticalAlignment.Center;

            //Third row
            row = table.AddRow();
            row.Borders.Color = Colors.Gray;
            row.Borders.Width = 0.75;

            cell = row.Cells[0];
            cell.AddParagraph(Resources.Resources.lblVersion.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[1];
            cell.AddParagraph(approvalInfo.Version.ToString());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[2];
            cell.AddParagraph(Resources.Resources.lblStatus.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[3];
            cell.AddParagraph(approvalInfo.JobStatus);
            cell.VerticalAlignment = VerticalAlignment.Center;

            //Fourth row
            row = table.AddRow();
            row.Borders.Color = Colors.Gray;
            row.Borders.Width = 0.75;

            cell = row.Cells[0];
            cell.AddParagraph(Resources.Resources.lblOwner.ToUpper());
            cell.VerticalAlignment = VerticalAlignment.Center;

            cell = row.Cells[1];
            cell.AddParagraph(approvalInfo.Creator);
            cell.VerticalAlignment = VerticalAlignment.Center;
            cell.MergeRight = 2;

            table.Rows.Height = 25;
            table.SetEdge(0, 0, 4, 4, Edge.Box, BorderStyle.Single, 0.75, Colors.Gray);

            //Create titles
            Paragraph title = section.AddParagraph();
            FormattedText formattedText = title.AddFormattedText(approvalInfo.JobTitle, TextFormat.Bold);
            formattedText.Font.Size = 12;
            formattedText.Font.Color = Colors.Black;
            formattedText.AddSpace(2);

            title.AddFormattedText(approvalInfo.CreatedDate);
            title.Format.Font.Size = 9;
            title.Format.Font.Color = Colors.Gray;
            Paragraph paragraph = section.AddParagraph();
            FormattedText ft = paragraph.AddFormattedText(Resources.Resources.navApprovalsDetails, TextFormat.Bold);
            ft.Font.Size = 12;

            var documentRenderer = new DocumentRenderer(doc);
            documentRenderer.PrepareDocument();

            //second parameter is the left margin of the element related to page, the third is the top margin, the fourth is the length of the line
            documentRenderer.RenderObject(gfx, XUnit.FromCentimeter(0.7), XUnit.FromCentimeter(3), (pageWidth - 1) + "cm", title);
            documentRenderer.RenderObject(gfx, XUnit.FromCentimeter(0.7), XUnit.FromCentimeter(4.1), "12cm", paragraph);
            documentRenderer.RenderObject(gfx, XUnit.FromCentimeter(0.7), XUnit.FromCentimeter(5), "6cm", table);
        }

        public System.Drawing.Image ConvertBase64ToImage(string base64Image)
        {
            byte[] bytes = Convert.FromBase64String(base64Image);

            System.Drawing.Image image;
            using (var ms = new MemoryStream(bytes))
            {
                image = System.Drawing.Image.FromStream(ms);
            }

            return image;
        }

        [HttpGet]
        public ActionResult PopulateAnnotationsReportListBox(string selectedApprovals, string selectedFolders)
        {
            try
            {
                var approvalsIds = new int[] { };
                var infoMsg = string.Empty;

                if (!string.IsNullOrEmpty(selectedApprovals))
                {
                    approvalsIds = (Array.ConvertAll(selectedApprovals.Split(','), int.Parse)).Distinct().ToArray();
                    if (!ApprovalBL.CheckUserCanAccess(approvalsIds.ToList(), GMGColorDAL.Approval.CollaborateValidation.ApprovalAnnotationReport, LoggedUser.ID, Context))
                    {
                        return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
                    }
                }

                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalsIds, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }


                //if there are selected folders then retieve all approvals ids, that has annotations, from these folders
                if (!string.IsNullOrEmpty(selectedFolders))
                {
                    int[] foldersIds = Array.ConvertAll(selectedFolders.Split(','), int.Parse);

                    var ids = new List<int>();
                    ApprovalBL.GetAllApprovalsIdsFromFolder(foldersIds, ref ids, LoggedUser.ID, Context, true);
                    if (!ApprovalBL.CheckUserCanAccess(foldersIds.ToList(), GMGColorDAL.Approval.CollaborateValidation.FolderAnnotationReport, LoggedUser.ID, Context))
                    {
                        return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
                    }
                    //Merge approvals ids arrays
                    if (approvalsIds.Length > 0)
                    {
                        approvalsIds = approvalsIds.Union(ids.ToArray()).ToArray();
                    }
                    else if (ids.Count > 0)
                    {
                        approvalsIds = ids.ToArray();
                    }
                    else
                    {
                        infoMsg = Resources.Resources.errNoJobsWithAnnotations;
                    }
                }

                int jobIdFromApprovalId;
                List<AnnotationsReportUser> users = new List<AnnotationsReportUser>();
                List<AnnotationsReportUser> internalUsersList = new List<AnnotationsReportUser>();

                foreach (int approvalID in approvalsIds)
                {
                    //---> Need to change to lamba query
                    jobIdFromApprovalId = (from a in Context.Approvals where a.ID == approvalID select a.Job).FirstOrDefault();
                    var approvalUsers = UserBL.GetAllAnnotationReportUsersByJobID(jobIdFromApprovalId, Context);
                    foreach (var obj in approvalUsers)
                    {
                        if (!users.Where(x => x.ID == obj.ID && x.IsExternal == obj.IsExternal).Any())
                        {
                            users.Add(obj);
                        }
                    }
                }
                var internalUsers = users.Where(t => !t.IsExternal);

                List<AnnotationReportPhase> reportPhases = approvalsIds.Count() == 1
                    ? ApprovalBL.GetApprovalPhasesForAnnotationReport(approvalsIds[0], Context)
                    : null;

                int? lastVersion = approvalsIds.Count() == 1 ? ApprovalBL.GetApprovalLastVersion(approvalsIds[0], Context) : null;

                int? selectedApprovalVersion = approvalsIds.Count() == 1 ? ApprovalBL.GetApprovalSelectedVersion(approvalsIds[0], Context) : null;

                return Json(
                  new
                  {
                      Status = 400,
                      InfoMessage = infoMsg,
                      SelectedApprovals = approvalsIds,
                      Users = users.Where(t => !t.IsExternal),
                      ExternalUsers = users.Where(t => t.IsExternal),
                      Groups = UserBL.GetGroupsByUsersId(internalUsers.Select(t => t.ID), Context),
                      Phases = reportPhases,
                      LastVersion = lastVersion,
                      SelectedApprovalVersion = selectedApprovalVersion
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.PopulateAnnotationsReportListBox Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the requested version details"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetApprovalCollaborators(string id)
        {
            List<int> lstApprovalIds = new List<int>();
            if (id != "")
            {
                lstApprovalIds = id.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            }

            var approvalVersionCollaboratorInfo = ApprovalBL.GetApprovalVersionCollaboratorInfo(lstApprovalIds[0], Context);

            try
            {
                return Json(
                  new
                  {
                      Status = 400,
                      Collaborators = approvalVersionCollaboratorInfo
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetCollaborators Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the collaborators"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetExternalCollaborators(int id)
        {
            var approvalVersionExternalCollaboratorInfo = ApprovalBL.GetApprovalVersionExternalCollaboratorInfo(id, Context);

            try
            {
                return Json(
                  new
                  {
                      Status = 400,
                      ExternalCollaborators = approvalVersionExternalCollaboratorInfo
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetExternalCollaborators Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the external collaborators"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetFolderCollaborators(int id)
        {
            var approvalFolderCollaboratorInfo = ApprovalBL.GetApprovalFolderCollaboratorInfo(id, LoggedUser.ID, LoggedUserCollaborateRole, Context);


            try
            {
                return Json(
                  new
                  {
                      Status = 400,
                      FolderCollaborators = approvalFolderCollaboratorInfo
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetFolderCollaborators Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the collaborators of the current folder"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetApprovalWorkflowCollaborators(int workflowId, int? phase)
        {
            var approvalWorkflowCollaborators = ApprovalBL.GetApprovalWorkflowCollaborators(workflowId, Context);

            var workflowPhases = ApprovalBL.GetApprovalWorkflowPhases(workflowId, Context);
            try
            {
                return Json(
                  new
                  {
                      Status = 400,
                      Collaborators = JsonConvert.SerializeObject(approvalWorkflowCollaborators),
                      Phases = JsonConvert.SerializeObject(workflowPhases)
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetApprovalWorkflowCollaborators Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the collaborators"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetNewApprovalCollaborators(int? versionId)
        {
            ApprovalVersionCollaboratorInfo approvalVersionCollaboratorInfo = new ApprovalVersionCollaboratorInfo();

            if (versionId.GetValueOrDefault() > 0)
            {
                approvalVersionCollaboratorInfo.LoadCollaborats(versionId.GetValueOrDefault(), Context);
            }
            else
            {
                var approvalCollaborators = PermissionsBL.GetAccountUsers(LoggedAccount, LoggedUser.ID, Context);
                approvalVersionCollaboratorInfo.CollaboratorsWithRole = approvalCollaborators.Where(u => u.IsChecked && !u.IsExternal && !u.IsGroup).Select(u => u.ID + "|" + u.Role).ToList();
            }

            try
            {
                return Json(
                  new
                  {
                      Status = 400,
                      Collaborators = approvalVersionCollaboratorInfo
                  },
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetNewApprovalCollaborators Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving the collaborators"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetAdvancedSearchNames()
        {
            try
            {
                var searchedNames = ApprovalBL.GetAdvancedSearchNames(this.LoggedAccount.ID, Context);
                foreach (var advancedSearchName in searchedNames)
                {
                    advancedSearchName.ID = -advancedSearchName.ID - Constants.TopLinkOffset;
                }
                return Json(
                    new
                    {
                        Status = 400,
                        Searches = searchedNames
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.GetAdvancedSearchNames Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving search names"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult IsAdvsName_Available(string AdvSName, int ID)
        {
            if (ApprovalBL.isAdvsNameUnique(AdvSName, LoggedAccount.ID, ID, Context))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            var infoMsg = String.Format(Resources.Resources.lblNameIsNotAvailable, AdvSName);

            return Json(infoMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetApprovalsPhaseDeadline(string approvals)
        {
            try
            {
                List<int> ids = approvals.Split(',').Select(int.Parse).ToList();

                List<ApprovalPhaseDeadline> phasesDeadline = null;
                if (ids.Count > 0)
                {
                    phasesDeadline = ApprovalBL.GetApprovalPhasesDeadline(ids, Context, LoggedAccount);
                }

                return Json(
                    new
                    {
                        Status = 400,
                        Content = Json(phasesDeadline, JsonRequestBehavior.AllowGet)

                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetApprovalsPhaseDeadline Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested approvals phase deadline"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetApprovalsDecision(string approvalIds)
        {
            try
            {
                List<int> ids = approvalIds.Split(',').Select(int.Parse).ToList();
                bool result = false;
                foreach (var id in ids)
                {
                    result = ApprovalBL.CheckApprovalDecisionsMade(id, LoggedUser.ID, Context);
                    if (!result)
                    {
                        break;
                    }
                }
                return Json(
                    new
                    {
                        Status = 400,
                        Content = Json(result, JsonRequestBehavior.AllowGet)

                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetApprovalsDecision Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested approvals decisions"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetCollaboratorsOnSearch(string term, string _type)
        {
            try
            {
                List<GMGColorDAL.User> listCollaborators = new List<GMGColorDAL.User>();
                if (term != "")
                {
                    listCollaborators = UserBL.GetUserOnSearch(term, LoggedAccount.ID, Context);
                }
                return Json(
                    new
                    {
                        Content =
                        listCollaborators.Select(
                        t => new { id = t.ID, text = t.GivenName + " " + t.FamilyName, emailAddress = t.EmailAddress })
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetCollaboratorsOnSearch Ajax Request failed. {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error retrieving the requested collaborators"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }




        #endregion

        #region POST Actions

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ApplySearch")]
        public ActionResult AdvancedSearch(GMGColorDAL.CustomModels.AdvancedSearch model)
        {
            Session["AdvancedSearchModel"] = model;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AdvancedSearchSave(GMGColorDAL.CustomModels.AdvancedSearch model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    #region Business rules for save
                    if (string.IsNullOrWhiteSpace(model.AdvSName))
                    {
                        return Json(
                            new
                            {
                                Status = 400,
                                Content = Resources.Resources.lblAdvSsideBarNameRequired
                            },
                            JsonRequestBehavior.AllowGet);
                    }
                    if (!ApprovalBL.isAdvsNameUnique(model.AdvSName, LoggedAccount.ID, model.ID, Context))
                    {
                        return Json(
                            new
                            {
                                Status = 400,
                                Content = String.Format(Resources.Resources.lblNameIsNotAvailable, model.AdvSName)
                            },
                            JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    ApprovalBL.AddOrUpdateAdvancedSearch(model, LoggedAccount.ID, Context, LoggedAccount.DateFormat1.Pattern);
                    return Json(
                        new
                        {
                            Status = 400,
                            Content = string.Empty
                        },
                        JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string errorList = "";
                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            errorList += "\\" + error.ErrorMessage;
                        }
                    }
                    GMGColorLogging.log.Error(errorList);

                    return Json(
                        new
                        {
                            Status = 300,
                            Content = Resources.Resources.errSavingSearch
                        },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "ProjectFoldersController.AdvancedSearchSave Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = Resources.Resources.errSavingSearch
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AdvancedSearchDelete")]
        public ActionResult AdvancedSearchDelete(int TopLinkID)
        {
            var selectedTab = (Session[Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] == null) ? -1 : int.Parse(Session[Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()].ToString().Trim());
            try
            {
                ApprovalBL.DeleteAdvancedSearch(-TopLinkID - Constants.TopLinkOffset, Context);

                if (selectedTab == TopLinkID)
                {
                    selectedTab = -1;
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.AdvancedSearchDelete : Delete search failed. {0}", ex.Message);
            }
            return RedirectToAction("Populate", new { topLinkID = selectedTab });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SetPageSize")]
        public ActionResult SetPageSize(string SelectedPageSize, string SelectedPage)
        {
            var pageSize = Int32.Parse(SelectedPageSize);
            Session["PageSize_" + LoggedUser.ID] = JobBL.GetNumberOfRowsPerPage(LoggedUser.ID, pageSize, GMGColorConstants.RememberNumberOfFilesShown, GMGColorConstants.DefaultNumberOfFilesShown, Context).ToString();

            string sortColumn = null;
            string sortDirection = SortingDirection ? "DESC" : "ASC";
            switch (SortColumn)
            {
                case (int)GMGColorCommon.ApprovalGrigdSortColumn.Deadline: sortColumn = GMGColorCommon.ApprovalGrigdSortColumn.Deadline.ToString(); break;
                case (int)GMGColorCommon.ApprovalGrigdSortColumn.Title: sortColumn = GMGColorCommon.ApprovalGrigdSortColumn.Title.ToString(); break;
                case (int)GMGColorCommon.ApprovalGrigdSortColumn.PrimaryDecisionMaker: sortColumn = GMGColorCommon.ApprovalGrigdSortColumn.PrimaryDecisionMaker.ToString(); break;
                case (int)GMGColorCommon.ApprovalGrigdSortColumn.PhaseName: sortColumn = GMGColorCommon.ApprovalGrigdSortColumn.PhaseName.ToString(); break;
                case (int)GMGColorCommon.ApprovalGrigdSortColumn.NextPhase: sortColumn = GMGColorCommon.ApprovalGrigdSortColumn.NextPhase.ToString(); break;
                case (int)GMGColorCommon.ApprovalGrigdSortColumn.DecisionMakers: sortColumn = GMGColorCommon.ApprovalGrigdSortColumn.DecisionMakers.ToString(); break;
            }

            return Populate(string.Empty, string.Empty, int.Parse(SelectedPage), 1, sortColumn, sortDirection);
        }


        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SetFilter")]
        public ActionResult SetFilter(string SelectedFilter, string SelectedPage)
        {
            Session["FiltersSorting"] = true;
            Session["FilterType"] = SelectedFilter.ToString();
            return Populate(string.Empty, string.Empty, int.Parse(SelectedPage), 1, null, null);
        }

        [HttpPost]
        public ActionResult ViewAllApprovals(bool ViewAll, string SelectedPage)
        {
            Session[Constants.ApprovalStatus] = 0;
            Session[Constants.ViewAllApprovals] = ViewAll;
            return Populate(string.Empty, string.Empty, int.Parse(SelectedPage), 1, null, null);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SetStatus")]
        public ActionResult SetStatus(string SelectedStatus, string SelectedPage)
        {
            int selStatus = SelectedStatus.ToInt().GetValueOrDefault();
            int selPage = SelectedPage.ToInt().GetValueOrDefault();
            if (selStatus == (int)Session[Constants.ApprovalStatus])
            {
                selStatus = 0;
            }

            Session["FiltersSorting"] = true;
            Session[Constants.ViewAllApprovals] = false;
            Session[Constants.ApprovalStatus] = selStatus;

            return Populate(string.Empty, string.Empty, selPage, 1, null, null);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SetSortingOrder")]
        public ActionResult SetSortingOrder(string SortingOrder, string SelectedPage)
        {
            Session["FiltersSorting"] = true;
            Session["SortingOrder"] = (SortingOrder.ToString() == "1") ? true : false;
            return Populate(string.Empty, string.Empty, int.Parse(SelectedPage), 1, null, null);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SetOutOfOfficeUser")]
        public ActionResult SetOutOfOfficeUser(string OutOfOfficeUser, string SelectedPage)
        {
            Session[Constants.OutOfOfficeUser] = OutOfOfficeUser;
            return Populate(string.Empty, string.Empty, int.Parse(SelectedPage), 1, null, null);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Search")]
        public ActionResult Search(string txtSearch, string SelectedPage)
        {
            SearchText = txtSearch;
            return Populate(string.Empty, string.Empty, int.Parse(SelectedPage), 1, null, null);
        }
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "TagwordsSearch")]
        public ActionResult TagwordsSearch(string SelectedPage, string SelectedApprovalType, string SelectedTagword)
        {
            bool IsFromProjectFolder = Session[Constants.SelectedProjectId] != null;
            List<string> ApprovalTypes = SelectedApprovalType.Split(',').ToList();
            string ApprovalTypeIds = string.Empty;
            string TagwordIds = string.Empty;
            foreach (var type in ApprovalTypes)
            {
                switch (type)
                {
                    case "Image":
                        if (ApprovalTypeIds == "")
                        {
                            ApprovalTypeIds = "1";
                        }
                        else
                        {
                            ApprovalTypeIds = ApprovalTypeIds + "," + "1";
                        }
                        break;
                    case "Video":
                        if (ApprovalTypeIds == "")
                        {
                            ApprovalTypeIds = "2";
                        }
                        else
                        {
                            ApprovalTypeIds = ApprovalTypeIds + "," + "2";
                        }
                        break;
                    case "Document":
                        if (ApprovalTypeIds == "")
                        {
                            ApprovalTypeIds = "3";
                        }
                        else
                        {
                            ApprovalTypeIds = ApprovalTypeIds + "," + "3";
                        }
                        break;
                    case "HTML":
                        if (ApprovalTypeIds == "")
                        {
                            ApprovalTypeIds = "5";
                        }
                        else
                        {
                            ApprovalTypeIds = ApprovalTypeIds + "," + "5";
                        }
                        break;
                    default:
                        var s = 10;
                        break;
                }
            }
            if (SelectedTagword != "")
            {
                TagwordIds = SelectedTagword;
            }
            if (!IsFromProjectFolder)
            {
                return Populate(ApprovalTypeIds, TagwordIds, int.Parse(SelectedPage), 1, null, null);
            }
            else
            {
                return RedirectToAction("Populate", "ProjectFolders", new { ApprovalTypeIds = ApprovalTypeIds, TagwordIds = TagwordIds });
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "VersionApproval")]
        public ActionResult VersionApproval(int SelectedApproval)
        {
            try
            {
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
                {
                    LoggedUser.ID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Job>(SelectedApproval, LoggedAccount.ID, Context) ||
                    !GMGColorDAL.Job.CanAccess(LoggedUser.ID, new int[] { SelectedApproval }, GMGColorDAL.Approval.ApprovalOperation.AddNewVersion, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Version Approval Failed. {0}", ex.Message);
            }
            return RedirectToAction("NewApproval", new { jobId = SelectedApproval });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ArchiveApproval")]
        public ActionResult ArchiveApproval(int SelectedApproval, string HiddenSelectedApprovals)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (HiddenSelectedApprovals != "")
                {
                    lstApprovalIds = HiddenSelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }

                if (SelectedApproval > 0)
                {
                    lstApprovalIds.Add(SelectedApproval);
                }

                foreach (int Approval in lstApprovalIds)
                {
                    if (!CanAccess(Approval, GMGColorDAL.ProjectFolder.ApprovalOperation.Archive))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }
                    this.ArchivedSelectedApproval(Approval, Context);
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Archiving Approval Failed. {0}", ex.Message);
            }

            return RedirectToAction("ProjectFolderApprovalsIndex");
        }

        [HttpPost]
        public ActionResult SendApprovalReminder(int collaboratorID, bool isExternalCollaborator, int approval)
        {
            try
            {
                //Check if the request values are authorized
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approval, LoggedAccount.ID, Context) ||
                    isExternalCollaborator
                    ? !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(collaboratorID, LoggedAccount.ID,
                        Context)
                    : !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(collaboratorID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                ApprovalReminder notification = new ApprovalReminder();

                if (isExternalCollaborator)
                {
                    notification.ExternalRecipient = collaboratorID;

                    var objSharedApproval = (from sa in Context.SharedApprovals
                                             join a in Context.Approvals on sa.Approval equals a.ID
                                             where sa.Approval == approval
                                             && sa.ExternalCollaborator == collaboratorID
                                             && sa.Phase == a.CurrentPhase
                                             select sa).FirstOrDefault();

                    if (objSharedApproval != null)
                    {
                        objSharedApproval.IsBlockedURL = false;
                        objSharedApproval.ExpireDate = DateTime.UtcNow.AddDays(1);
                        objSharedApproval.IsReminderMailSent = false;
                        objSharedApproval.MailSentDate = DateTime.UtcNow;
                        Context.SaveChanges();
                    }
                }
                else
                {
                    notification.InternalRecipient = collaboratorID;
                    var objApprovalCollaborator = (from ac in Context.ApprovalCollaborators
                                                   join a in Context.Approvals on ac.Approval equals a.ID
                                                   where ac.Approval == approval && ac.Collaborator == collaboratorID
                                                   && ac.Phase == a.CurrentPhase
                                                   select ac).FirstOrDefault();
                    if (objApprovalCollaborator != null)
                    {
                        objApprovalCollaborator.IsReminderMailSent = false;
                        objApprovalCollaborator.MailSentDate = DateTime.UtcNow;
                        Context.SaveChanges();
                    }
                }

                notification.EventCreator = LoggedUser.ID;
                notification.CreatedDate = DateTime.UtcNow;
                notification.ApprovalId = approval;
                NotificationServiceBL.CreateNotification(notification, null, Context);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.SendApprovalReminder : SendApprovalReminder Failed for approval: {0} Exception: {1}", approval, ex.Message);
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteApproval")]
        public ActionResult DeleteApproval(int SelectedApproval)
        {
            try
            {
                if (!CanAccess(SelectedApproval, GMGColorDAL.ProjectFolder.ApprovalOperation.Delete))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);

                GMGColorDAL.Approval.ApprovalProcessingStatus objApprovalStatus = GMGColorDAL.Approval.GetApprovalProcessingStatus(SelectedApproval, Context);
                if (objApprovalStatus.Status == GMGColorDAL.Approval.ProcessingStatus.Processing)
                {
                    if (DateTime.Compare(ApprovalBL.GetApprovalCreatedDate(SelectedApproval, Context).AddHours(1), DateTime.UtcNow) != -1)
                    {
                        Session[Constants.ApprovalsCannotDelete] = true;
                    }
                    else
                    {
                        this.DeleteSelectedApproval(SelectedApproval, true, collaborateSettings.ShowAllFilesToAdmins, Context, objApprovalStatus.Status);
                    }
                }
                else
                {
                    this.DeleteSelectedApproval(SelectedApproval, true, collaborateSettings.ShowAllFilesToAdmins, Context, objApprovalStatus.Status);
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Delete Approval Failed. {0}", ex.Message);
            }

            return RedirectToAction("Populate", new { topLinkID = this.PopulateID });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteVersion")]
        public ActionResult DeleteVersion(int SelectedApproval)
        {
            try
            {
                if (!CanAccess(SelectedApproval, GMGColorDAL.ProjectFolder.ApprovalOperation.DeleteVersion))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);

                GMGColorDAL.Approval.ApprovalProcessingStatus objApprovalStatus = GMGColorDAL.Approval.GetApprovalProcessingStatus(SelectedApproval, Context);
                if (objApprovalStatus.Status == GMGColorDAL.Approval.ProcessingStatus.Processing)
                {
                    Session[Constants.ApprovalsCannotDelete] = true;
                }
                else
                {
                    this.DeleteSelectedApproval(SelectedApproval, false, collaborateSettings.ShowAllFilesToAdmins, Context, objApprovalStatus.Status);
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Delete Version Failed. {0}", ex.Message);
            }

            return RedirectToAction("Populate", new { topLinkID = this.PopulateID });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteVersionPermanent")]
        public ActionResult DeleteVersionPermanently(int selectedApproval)
        {
            try
            {
                if (!CanAccess(selectedApproval, GMGColorDAL.ProjectFolder.ApprovalOperation.Delete))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                Approval approval = DALUtils.GetObject<GMGColorDAL.Approval>(selectedApproval, Context);
                Approval.ApprovalProcessingStatus approvalStatus = Approval.GetApprovalProcessingStatus(selectedApproval, Context);
                Job job = approval.Job1;

                if (approvalStatus.Status == Approval.ProcessingStatus.Processing &&
                        DateTime.Compare(ApprovalBL.GetApprovalCreatedDate(selectedApproval, Context).AddHours(1), DateTime.UtcNow) != -1)
                {
                    Session[Constants.ApprovalsCannotDelete] = true;
                }
                else
                {
                    if (GMGColorDAL.JobStatu.GetJobStatus(job.JobStatu.Key) == GMGColorDAL.JobStatu.Status.Archived)
                    {
                        var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID,
                               GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);

                        foreach (var app in job.Approvals)
                        {
                            if ((LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator && collaborateSettings.ShowAllFilesToAdmins) ||
                                    approval.Owner == LoggedUser.ID)
                            {
                                Approval.MarkApprovalForPermanentDeletion(approval, LoggedUser.ID, Context);
                            }
                        }
                    }
                    else
                    {
                        Approval.MarkApprovalForPermanentDeletion(approval, LoggedUser.ID, Context);
                    }

                    Context.SaveChanges();
                }

                // If call is made from Details view and job has any available (not deleted) approvals, redirect accordingly
                if (job.Approvals.Any(a => a.DeletePermanently == false) && Request["HTTP_REFERER"].Contains("ApprovalDetails"))
                {
                    // Get last available approval ID
                    var lastAvailableApproval = job.Approvals.OrderByDescending(a => a.ID).
                        FirstOrDefault(a => (a.DeletePermanently == false && a.IsDeleted == false));

                    int lastApprovalId = lastAvailableApproval != null ? lastAvailableApproval.ID : -1;

                    // If valid ID is available and access is granted, redirect to approval details view
                    if (lastApprovalId != -1)
                    {
                        bool canAccessLastApproval = CanAccess(lastApprovalId, GMGColorDAL.ProjectFolder.ApprovalOperation.ViewDetails);

                        if (canAccessLastApproval)
                        {
                            return RedirectToAction("ApprovalDetails", "ProjectFolders", new { selectedApproval = lastApprovalId });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Delete Version Permanently Failed. {0}", ex.Message);
            }

            return RedirectToAction("Populate", "ProjectFolders", new { TopLinkID = this.PopulateID });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteApprovalPermanent")]
        public ActionResult DeleteApprovalPermanently(int selectedApproval)
        {
            int nextTab = (int)GMGColor.Common.Utils.ApprovalTab.RecycleBin;
            try
            {
                if (!CanAccess(selectedApproval, GMGColorDAL.ProjectFolder.ApprovalOperation.DeletePermanent))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                GMGColorDAL.Approval objApproval = DALUtils.GetObject<GMGColorDAL.Approval>(selectedApproval, Context);

                if (GMGColorDAL.JobStatu.GetJobStatus(objApproval.Job1.Status, Context) == GMGColorDAL.JobStatu.Status.Archived)
                {
                    nextTab = (int)GMGColor.Common.Utils.ApprovalTab.Archive_PastTense;

                    var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);

                    foreach (var approval in objApproval.Job1.Approvals)
                    {
                        if ((LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator && collaborateSettings.ShowAllFilesToAdmins) || approval.Owner == LoggedUser.ID)
                        {
                            //delete from database
                            Approval.MarkApprovalForPermanentDeletion(approval, LoggedUser.ID, Context);
                        }
                    }
                }
                else
                {
                    //delete from database
                    Approval.MarkApprovalForPermanentDeletion(objApproval, LoggedUser.ID, Context);
                }


                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Delete Approval Permanently Failed. {0}", ex.Message);
            }
            return RedirectToAction("Populate", new { topLinkID = nextTab });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "FileTransferApproval")]
        public ActionResult FileTransferApproval(int selectedApproval)
        {
            //send the approval to the file transfer
            return RedirectToAction("NewApprovalTransfer", "FileTransfer", new { approvalId = selectedApproval });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "TransferFolder")]
        public ActionResult TransferFolder(ApprovalsModel model)
        {
            try
            {
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>(model.objFolderCR.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                int currentFolderId = model.objFolderCR.ID;
                //send the approval to the file transfer
                return RedirectToAction("NewFolderTransfer", "FileTransfer", new { folderId = currentFolderId });

            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.TransferFolder : Restore folder failed, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RestoreArchivedApproval")]
        public ActionResult RestoreArchivedApproval(int selectedApproval)
        {
            try
            {
                GMGColorDAL.Approval objApproval = DALUtils.GetObject<GMGColorDAL.Approval>(selectedApproval, Context);

                string jobStatusCompleted = JobStatu.Status.Completed.ToString();
                objApproval.Job1.Status = Context.JobStatus.First(js => js.Name == jobStatusCompleted).ID;
                objApproval.Job1.ModifiedDate = DateTime.UtcNow;

                NotificationServiceBL.CreateNotification(new ApprovalWasUpdated
                {
                    EventCreator = LoggedUser.ID,
                    InternalRecipient = LoggedUser.ID,
                    CreatedDate = DateTime.UtcNow,
                    ApprovalsIds = new[] { objApproval.ID }
                },
                LoggedUser,
                Context,
                false);

                JobBL.LogJobStatusChange(objApproval.Job1.ID, objApproval.Job1.Status, LoggedUser.ID, Context);

                Context.SaveChanges();

                var jobDetails = new JobStatusDetails()
                {
                    jobGuid = objApproval.Job1.Guid,
                    jobStatus = objApproval.Job1.JobStatu.Name,
                    jobStatusKey = objApproval.Job1.JobStatu.Key,
                    username = LoggedUser.Username,
                    email = LoggedUser.EmailAddress
                };

                ApprovalBL.PushJobStatus(jobDetails, LoggedAccount.ID, Context);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.RestoreArchievedApproval : Restore Archieved Approval Failed. {0}", ex.Message);
            }

            return RedirectToAction("ProjectFolderApprovalsIndex");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EmptyRecycleBin")]
        public ActionResult EmptyRecycleBin()
        {
            try
            {
                FoldersModel objModel = new FoldersModel()
                {
                    Account = this.LoggedAccount.ID,
                    Creator = this.LoggedUser.ID,
                    Folder = 0,
                    Status = FoldersModel.FolderStatus.EmptyRecycleBin
                };
                FolderBL.FolderCRD(Context, ref objModel);

                var lstApprovals =
                    (from a in Context.Approvals
                     join j in Context.Jobs on a.Job equals j.ID
                     join acc in Context.Accounts on j.Account equals acc.ID
                     where a.IsDeleted && !a.DeletePermanently && (a.Owner == LoggedUser.ID || (from aph in Context.ApprovalUserRecycleBinHistories
                                                                                                where aph.Approval == a.ID && aph.User == LoggedUser.ID
                                                                                                select aph.ID).Any())

                     select new
                     {
                         a.ID,
                         a.Owner
                     }
                     ).ToList();

                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);
                foreach (var approval in lstApprovals)
                {
                    if ((LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator && collaborateSettings.ShowAllFilesToAdmins) || approval.Owner == LoggedUser.ID)
                    {
                        // approvals that has errors or are in progress cannot be located into RecycleBin!
                        Approval.MarkApprovalForPermanentDeletion(approval.ID, Context);
                    }
                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Empty RecycleBin Failed. {0}", ex.Message);
            }
            return RedirectToAction("Populate", new { topLinkID = (int)Utils.ApprovalTab.RecycleBin });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "MultipleDelete")]
        public ActionResult MultipleDelete(string HiddenSelectedApprovals, string HiddenSelectedFolders)
        {
            try
            {
                int?[] lstApprovalIds = HiddenSelectedApprovals.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();
                //int?[] lstFoldersIds = HiddenSelectedFolders.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();
                var lstApprovalsNotifIds = new List<int>();
                var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, false, Context);

                if (ApprovalBL.CheckUserCanAccess(lstApprovalIds.Where(x => x != null).Cast<int>().ToList(), Approval.CollaborateValidation.DeleteApproval, LoggedUser.ID, Context))
                {
                    //delete approvals
                    foreach (int? approvalId in lstApprovalIds.Where(approvalId => CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.Delete)))
                    {
                        //create a list for notifications with approvals ids that will be deleted
                        lstApprovalsNotifIds.Add((int)approvalId);

                        GMGColorDAL.Approval.ApprovalProcessingStatus objApprovalStatus = GMGColorDAL.Approval.GetApprovalProcessingStatus(approvalId.Value, Context);
                        if (objApprovalStatus.Status == GMGColorDAL.Approval.ProcessingStatus.Processing)
                        {
                            Session[Constants.ApprovalsCannotDelete] = true;
                        }
                        else
                        {
                            this.DeleteSelectedApproval(approvalId.Value, true, collaborateSettings.ShowAllFilesToAdmins, Context, objApprovalStatus.Status, false);
                        }
                    }
                }

                //delete folders
                //foreach (var folderId in lstFoldersIds)
                //{
                //    if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>((int)folderId, LoggedAccount.ID, Context))
                //    {
                //        continue;
                //    }

                //    DeleteFolder((int)folderId);
                //}

                NotificationServiceBL.CreateNotification(new ApprovalWasDeleted
                {
                    EventCreator = LoggedUser.ID,
                    InternalRecipient = LoggedUser.ID,
                    CreatedDate = DateTime.UtcNow,
                    ApprovalsIds = lstApprovalsNotifIds.ToArray()
                },
                                                            LoggedUser,
                                                            Context
                                                        );
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Multiple Delete Failed. {0}", ex.Message);
            }

            return RedirectToAction("Populate", new { topLinkID = PopulateID });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RestoreApproval")]
        public ActionResult RestoreApproval(int SelectedApproval)
        {
            if (!CanAccess(SelectedApproval, GMGColorDAL.ProjectFolder.ApprovalOperation.Restore))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            try
            {
                ApprovalBL.RestoreApproval(SelectedApproval, LoggedUser.ID, Context);
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Restore Approval Failed. {0}", ex.Message);
            }

            return RedirectToAction("ProjectFolderApprovalsIndex");
        }

        [HttpPost]
        public string Studio(string Ids, string url)
        {
            string sessionKey = (new Random()).Next(1000, 9999).ToString();

            Session["SessionKey"] = sessionKey;
            Session["StudioApproval_" + sessionKey] = Ids.Split(',').Select(int.Parse).ToList();
            Session["StudioReturnURL_" + sessionKey] = url;
            Session["StudioLoggedUser_" + sessionKey] = LoggedUser.ID;
            Session[Constants.ApprovalsResetParameters] = false;

            var returnUrl = Request.Url != null
                ? Request.Url.OriginalString.Replace(Request.Url.LocalPath, Url.Action("Html", "Studio"))
                : Request.UrlReferrer.OriginalString.Replace(Request.UrlReferrer.LocalPath, Url.Action("Html", "Studio"));


            return returnUrl.Split('?').First();
        }

        [HttpPost]
        public ActionResult Annotations(AnnotationsReportFilters reportFilters)
        {
            try
            {
                return GetAnnotationsPage(reportFilters, false);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "ProjectFoldersController.Index : Annotations Failed. {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpGet]
        public ActionResult ExternalAnnotations(AnnotationsReportFilters reportFilters)
        {
            try
            {
                reportFilters.IsFromAPICall = true;
                return GetAnnotationsPage(reportFilters, true);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "ProjectFoldersController.ExternalAnnotations Failed. {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpPost]
        [Compress]
        public JsonResult PageGroupedAnnotations(AnnotationsReportViewModel model)
        {
            try
            {

                var reportType = (AnnotationReportType)Enum.Parse(typeof(AnnotationReportType), model.ReportType);
                int[] groupedAnnotationsLst = model.GroupedAnnotations;
                int[] allAnnotationsLst = model.AllAnnotations;
                JsonResult result = null;

                switch (reportType)
                {
                    case AnnotationReportType.Comments:
                    case AnnotationReportType.Pins:
                    case AnnotationReportType.CommentsAndPins:
                        result = Json(
                            new
                            {
                                PinSVG = AnnotationBL.GetAnnotationPinSVG(),
                                Annotations = _annotationsInReportService.GetReportAnnotationsOnGroups(groupedAnnotationsLst, allAnnotationsLst,
                                    LoggedAccount.DateFormat1.Pattern, LoggedAccount.TimeZone, LoggedAccount.TimeFormat)
                            },
                            JsonRequestBehavior.AllowGet);

                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "ProjectFoldersController.PageAnnotations : Get Annotations ajax call failed. {0}", ex.Message);
                return Json(
                new
                {
                    error = true
                },
                 JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileResult GetApprovalThumbnailPath(string appGuid, int pageNumber, int pageID, int? timeFrame , string annotationId, string approvalName)
        {
            try
            {
                string pageRelativePath;
                if (pageID > 0 && timeFrame != null)
                {
                    var fileName = pageID.ToString() + "_" + timeFrame.ToString() + ".jpg";
                    pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + appGuid +
                                        @"\" + pageNumber + @"\" + fileName;
                }
                else if (approvalName.Contains(".zip"))
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + appGuid +
                                           @"\" + pageNumber + @"\" + annotationId + ".jpeg";
                    }
                    else
                    {
                        pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + appGuid +
                                          @"\" + pageNumber + @"\" + annotationId + ".jpg";

                    }
                }
                else
                {
                    var pageDimmensions = AnnotationBL.GetPageDimensions(appGuid, pageNumber, Context);

                    if (pageDimmensions.HasValue && pageDimmensions.Value > Approval.PreviewMaxSize)
                    {
                        pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + appGuid +
                                           "/" + pageNumber + "/Thumb-" + Approval.ProcessedThumbnailDimension + "px.jpg";
                    }
                    else
                    {
                        pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + appGuid +
                                        @"\" + pageNumber + @"\" + Approval.FullSizePreview;
                    }
                }

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    WebRequest req =
                        WebRequest.Create(
                            GMGColorConfiguration.AppConfiguration.PathToDataFolder(this.LoggedAccount.Region) +
                            pageRelativePath);

                    WebResponse response = req.GetResponse();
                    Stream stream = response.GetResponseStream();

                    return File(stream, "image/jpeg");

                }
                else
                {
                    return
                        File(GMGColorConfiguration.AppConfiguration.PathToDataFolder(this.LoggedAccount.Region) + pageRelativePath, "image/jpeg");
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.GetApprovalThumbnailPath : Failed to get approval thumbnail for annotation report. {0}", ex.Message);
                throw;
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RenameApproval")]
        public ActionResult RenameApproval(ApprovalDetailsModel model)
        {
            try
            {
                if (!CanAccess(model.ID, GMGColorDAL.ProjectFolder.ApprovalOperation.RenameApproval))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (!CheckIsValidHtml(model.Title))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                GMGColorDAL.Approval objApproval = DALUtils.GetObject<GMGColorDAL.Approval>(model.ID, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objApproval.Job1.Title = model.Title;

                    Context.SaveChanges();
                    ts.Complete();
                }

                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                }

                SignalRService signalRService = new SignalRService();
                signalRService.AddInstantDashboardNotification(new DashboardInstantNotification
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                    Version = model.ID.ToString(),
                    ApprovalName = model.Title
                });

                NotificationServiceBL.CreateNotification(new ApprovalWasUpdated
                {
                    EventCreator = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                    InternalRecipient = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                    CreatedDate = DateTime.UtcNow,
                    ApprovalsIds = new[] { model.ID }
                },
                                                    OutOfOfficeUserID == 0 ? LoggedUser : OutOfOfficeUser,
                                                    Context
                                                    );
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Rename Approval Failed. {0}", ex.Message);
            }
            Session[Constants.SelectedApprovalId] = model.ID;
            return RedirectToAction("ApprovalDetails", "ProjectFolders");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "LockApproval")]
        public ActionResult Lock(string SelectedApproval)
        {
            int approvalId = SelectedApproval.ToInt().GetValueOrDefault();

            if (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.LockApproval))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            if (approvalId > 0)
            {
                Session[Constants.SelectedApprovalId] = SelectedApproval;
                try
                {
                    GMGColorDAL.Approval objAproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, Context);
                    var jobOwner = Context.Jobs.Where(j => j.ID == objAproval.Job).Select(j => j.JobOwner).FirstOrDefault();

                    int OutOfOfficeUserID = 0;
                    if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                    {
                        OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    }

                    if (objAproval.Owner == (OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID) || jobOwner == (OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID))
                    {
                        using (
                            var ts = new TransactionScope(TransactionScopeOption.Required,
                                new TimeSpan(0, 2, 0)))
                        {
                            objAproval.IsLocked = !objAproval.IsLocked;
                            objAproval.ModifiedDate = DateTime.UtcNow;
                            objAproval.Modifier = OutOfOfficeUserID == 0 ? this.LoggedUser.ID : OutOfOfficeUserID;

                            Context.SaveChanges();
                            ts.Complete();
                        }
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Details : Lock Approval Failed. {0}", ex.Message);
                }
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RemovePrivateAnnotationsOnApproval")]
        public ActionResult RemovePrivateAnnotationsOnApproval(string SelectedApproval, string SelectedUser)
        {
            int approvalId = SelectedApproval.ToInt().GetValueOrDefault();
            int userId = SelectedUser.ToInt().GetValueOrDefault();
            if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            try
            {
                GMGColorDAL.Approval objAproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    if (objAproval.PrivateAnnotations == true)
                    {
                        objAproval.PrivateAnnotations = false;
                    }

                    Context.SaveChanges();

                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Details : PrivateAnnotations Maker Failed. {0}", ex.Message);
            }
            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddPrivateAnnotationsOnApproval")]
        public ActionResult AddPrivateAnnotationsOnApproval(string SelectedApproval, string SelectedUser)
        {
            int approvalId = SelectedApproval.ToInt().GetValueOrDefault();
            int userId = SelectedUser.ToInt().GetValueOrDefault();
            if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            try
            {
                GMGColorDAL.Approval objAproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    if (objAproval.PrivateAnnotations == false)
                    {
                        objAproval.PrivateAnnotations = true;
                    }

                    Context.SaveChanges();

                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Details : PrivateAnnotations Maker Failed. {0}", ex.Message);
            }
            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "PrimaryDecisionMaker")]
        public ActionResult PrimaryDecisionMaker(string SelectedApproval, string SelectedUser)
        {
            int OutOfOfficeUserID = 0;
            if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
            {
                OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
            }
            int approvalId = SelectedApproval.ToInt().GetValueOrDefault();
            int userId = SelectedUser.ToInt().GetValueOrDefault();
            if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            if (OutOfOfficeUserID == 0 ? (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.PDM)) : (!CanSubstituteUserAccess(approvalId, OutOfOfficeUserID, GMGColorDAL.ProjectFolder.ApprovalOperation.PDM)))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            Session[Constants.SelectedApprovalId] = SelectedApproval;
            try
            {
                GMGColorDAL.Approval objAproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAproval.OnlyOneDecisionRequired = true;
                    if (approvalId > 0)
                    {
                        objAproval.PrimaryDecisionMaker = userId;
                        objAproval.ExternalPrimaryDecisionMaker = null;
                    }
                    else
                        objAproval.PrimaryDecisionMaker = null;

                    objAproval.ModifiedDate = DateTime.UtcNow;
                    objAproval.Modifier = OutOfOfficeUserID == 0 ? this.LoggedUser.ID : OutOfOfficeUserID;

                    Context.SaveChanges();

                    var isExternal = false;
                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(ApprovalBL.CreateDashboardInstantNotificationChangePDM(LoggedUser.ID, isExternal, SelectedApproval, SelectedUser, Context));

                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Details : Primary Decision Maker Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ExternalPrimaryDecisionMaker")]
        public ActionResult ExternalPrimaryDecisionMaker(string SelectedApproval, string SelectedUser)
        {
            int approvalId = SelectedApproval.ToInt().GetValueOrDefault();
            int userId = SelectedUser.ToInt().GetValueOrDefault();
            if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(userId, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            if (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.PDM))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            Session[Constants.SelectedApprovalId] = SelectedApproval;
            try
            {
                GMGColorDAL.Approval objAproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAproval.OnlyOneDecisionRequired = true;
                    if (approvalId > 0)
                    {
                        objAproval.ExternalPrimaryDecisionMaker = userId;
                        objAproval.PrimaryDecisionMaker = null;
                    }
                    else
                        objAproval.ExternalPrimaryDecisionMaker = null;

                    objAproval.ModifiedDate = DateTime.UtcNow;
                    objAproval.Modifier = this.LoggedUser.ID;

                    Context.SaveChanges();

                    var isExternal = true;

                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(ApprovalBL.CreateDashboardInstantNotificationChangePDM(objAproval.Creator, isExternal, SelectedApproval, SelectedUser, Context));

                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Details : Primary Decision Maker Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RemovePrimaryDecisionMaker")]
        public ActionResult RemovePrimaryDecisionMaker(string SelectedApproval)
        {
            int approvalId = SelectedApproval.ToInt().GetValueOrDefault();
            if (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.PDM))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            Session[Constants.SelectedApprovalId] = SelectedApproval;
            try
            {
                int OutOfOfficeUserID = 0;
                if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }
                GMGColorDAL.Approval objAproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAproval.OnlyOneDecisionRequired = false;
                    if (objAproval.PrimaryDecisionMaker != null)
                    {
                        objAproval.PrimaryDecisionMaker = null;
                    }
                    if (objAproval.ExternalPrimaryDecisionMaker != null)
                    {
                        objAproval.ExternalPrimaryDecisionMaker = null;
                    }
                    objAproval.PrivateAnnotations = false;
                    objAproval.ModifiedDate = DateTime.UtcNow;
                    objAproval.Modifier = OutOfOfficeUserID == 0 ? this.LoggedUser.ID : OutOfOfficeUserID;

                    Context.SaveChanges();
                    ts.Complete();

                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(new DashboardInstantNotification()
                    {
                        ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                        Creator = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                        Version = approvalId.ToString(),
                        PDM = string.Empty
                    });

                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Details : Removing Primary Decision Maker Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RevokeAccess")]
        public ActionResult RevokeAccess(string SelectedApproval, string SelectedUser)
        {
            try
            {
                int userId = SelectedUser.ToInt().GetValueOrDefault();
                int approvalId = SelectedApproval.ToInt().GetValueOrDefault();
                if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(userId, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                if (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.RevokeAccess))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.SharedApproval.MarkSharedApprovalAsExpired(userId, approvalId, Context);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.RevokeAccess : Revoke Access Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "GrantAccess")]
        public ActionResult GrantAccess(string SelectedApproval, string SelectedUser)
        {
            try
            {
                int userId = SelectedUser.ToInt().GetValueOrDefault();
                int approvalId = SelectedApproval.ToInt().GetValueOrDefault();

                if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(userId, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                if (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.RevokeAccess))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                GMGColorDAL.SharedApproval.MarkSharedApprovalAsNotExpired(userId, approvalId, Context);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while granting access for external user in GrantAccess()", ex);
            }
            finally
            {
            }

            return RedirectToAction("ApprovalDetails");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "NewApproval")]
        public ActionResult NewApproval(NewApprovalModel model, string hdnFileName_1)
        {
            if (!CheckIsValidHtml(model.ExternalEmails))
            {
                return RedirectToAction("XSSRequestValidation", "Error");
            }

            int substituteOwnerId = 0;
            int border = 5;
            if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8)
            {
                substituteOwnerId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                GMGColorDAL.User substituteUser = Context.Users.FirstOrDefault(u => u.ID == substituteOwnerId);
                this.LoggedUser = substituteUser;
                model.Owner = substituteOwnerId;
            }

            if (model.PrivateAnnotations && model.OnlyOneDecisionRequired)
            {
                model.ExternalUsers = "[]";
                model.ExternalEmails = null;
            }
            List<ApprovalJob> files = new List<ApprovalJob>();
            List<int> lstTmp = Request.Form.AllKeys.Where(o => o.Contains("ApprovalTitle_")).Select(o => int.Parse(o.Split('_').Last())).ToList();
            var phasesCollaborators = new ApprovalWorkflowPhasesInfo();
            List<GMGColorDAL.ApprovalPage> approvalHtmlPageList = new List<GMGColorDAL.ApprovalPage>();

            #region Custom Validation

            if (!CheckIsValidHtml(model.OptionalMessage))
            {
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                return RedirectWithModelAndErrors(model, dicModelErrors);
            }

            if (model.IsUploadApproval)
            {
                if (String.IsNullOrEmpty(hdnFileName_1.Trim()))
                {
                    model.IsUploadApproval = true;

                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                    dicModelErrors.Add("fileValidator", Resources.Resources.reqApproval);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }
            }
            else
            {
                lstTmp.Clear();

                Uri myUri;
                if (Uri.TryCreate(GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + model.Url, UriKind.Absolute, out myUri))
                {
                    lstTmp.Add(1);
                }
                else
                {
                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                    dicModelErrors.Add("urlValidator", Resources.Resources.valUrl);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }
            }
            if (model.HasAdHocWorkflow && string.IsNullOrEmpty(model.ApprovalAdHocWorkflowName))
            {

                model.ErrorMessage = Resources.Resources.errMissingWorkflowInfo;
                TempData["NewApprovalModel"] = model;

                return RedirectToAction("NewProjectFolderApproval", "ProjectFolders");
            }

            if (model.ApprovalWorkflowId > 0)
            {
                var dicModelErrors = new Dictionary<string, string>();
                if (model.HasAdHocWorkflow)
                {
                    //using (profiler.Step("ProjectFoldersController.GetApprovalWorkflowCollaborators"))
                    //{
                    phasesCollaborators = ApprovalBL.GetApprovalWorkflowCollaborators(model.ApprovalWorkflowId, Context);
                    //}
                }
                else
                {
                    phasesCollaborators = JsonConvert.DeserializeObject<ApprovalWorkflowPhasesInfo>(model.PhasesCollaborators);
                }

                var phases = ApprovalBL.PhasesWithPdmDecisionHasPdm(phasesCollaborators, Context);
                if (!string.IsNullOrEmpty(phases))
                {
                    dicModelErrors.Add("collaboratorsAccess", Resources.Resources.errPdmMustBeSelectedForPhase + phases);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }

                if (!ApprovalBL.AllPhasesHaveApproverAndReviewer(phasesCollaborators))
                {
                    dicModelErrors.Add("collaboratorsAccess", Resources.Resources.SelectAtLeastOneApproverAndReviewerForEachPhase);

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }
            }

            #endregion

            //using (profiler.Step("ProjectFoldersController.foreach Create files"))
            //{
            foreach (int key in lstTmp)
            {
                try
                {
                    if (model.IsUploadApproval)
                    {
                        var lsthtmlfileInfo = Request.Form.AllKeys.Where(o => o.Contains("fileWidth_" + key)).Select(o => int.Parse(o.Split('_').Last())).ToList();

                        foreach (var obj in lsthtmlfileInfo)
                        {
                            GMGColorDAL.ApprovalPage objApprovalPage = new GMGColorDAL.ApprovalPage();
                            objApprovalPage.Number = 0;
                            objApprovalPage.Approval = 0;
                            objApprovalPage.DPI = 0;
                            objApprovalPage.PageSmallThumbnailHeight = 0;
                            objApprovalPage.PageSmallThumbnailWidth = 0;
                            objApprovalPage.PageLargeThumbnailHeight = 0;
                            objApprovalPage.PageLargeThumbnailWidth = 0;
                            objApprovalPage.Progress = 100;
                            objApprovalPage.HTMLFilePath = Request.Form.GetValues("HtmlFileDimensionlist_" + key + "_" + obj)[0].ToString();
                            objApprovalPage.OriginalImageWidth = Convert.ToInt32(Request.Form.GetValues("fileWidth_" + key + "_" + obj)[0]) + border;
                            objApprovalPage.OriginalImageHeight = Convert.ToInt32(Request.Form.GetValues("fileHeight_" + key + "_" + obj)[0]) + border;
                            objApprovalPage.OutputRenderWidth = Convert.ToInt32(Request.Form.GetValues("fileWidth_" + key + "_" + obj)[0]) + border;
                            objApprovalPage.OutputRenderHeight = Convert.ToInt32(Request.Form.GetValues("fileHeight_" + key + "_" + obj)[0]) + border;

                            if (objApprovalPage.OriginalImageHeight == border || objApprovalPage.OriginalImageWidth == border)
                            {
                                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                                ////dicModelErrors.Add("optionalMessage", Resources.Resources.regInvalidHtml);

                                return RedirectWithModelAndErrors(model, dicModelErrors);
                            }

                            approvalHtmlPageList.Add(objApprovalPage);
                        }

                        var approvalTitles = Request.Form.GetValues("ApprovalTitle_" + key);
                        var appprovalType = Convert.ToInt32(Request.Form.GetValues("ApprovalType" + key)[0].ToString());
                        if (approvalTitles.Any())
                        {
                            // Split in case files are concatenated in formcollection because delete was used in file uploader and another file was uploaded causing more files to be in the same key
                            var approvalFileNames = Request.Form.GetValues("ApprovalFileName_" + key);
                            var approvalSizes = Request.Form.GetValues("ApprovalSize_" + key);
                            var approvalGuids = Request.Form.GetValues("FileGuid_" + key);

                            for (int i = 0; i < approvalTitles.Length; i++)
                            {
                                if (string.IsNullOrEmpty(approvalTitles[i]))
                                {
                                    model.IsUploadApproval = true;

                                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                                    dicModelErrors.Add("fileTitleValidator", Resources.Resources.reqDeliverJobTtile);

                                    return RedirectWithModelAndErrors(model, dicModelErrors);
                                }

                                files.Add(new ApprovalJob()
                                {
                                    FileName = approvalFileNames[i] ?? string.Empty,
                                    FileTitle = approvalTitles[i] ?? string.Empty,
                                    FileSize = decimal.Parse(approvalSizes[i] ?? string.Empty),
                                    FileGuid = approvalGuids[i] ?? string.Empty,
                                    ApprovalType = appprovalType == 0 ? 1 : appprovalType
                                });
                            }
                        }
                    }
                    else
                    {
                        files.Add(new ApprovalJob()
                        {
                            FileName = Guid.NewGuid().ToString() + ".png",
                            FileTitle = model.Url.Contains("?")
                                ? model.Url.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0]
                                : model.Url,
                            FileSize = 0,
                            FileGuid = Guid.NewGuid().ToString()
                        });
                    }
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                        "ProjectFoldersController.NewApproval : Save New Approval Failed. {0}", ex.Message);
                    model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                    TempData["NewApprovalModel"] = model;

                    return RedirectToAction("NewProjectFolderApproval", "ProjectFolders");
                }
            }
            //}

            try
            {
                string warningMessage;


                //using (profiler.Step("ProjectFoldersController.CheckIfJobSubmissionAllowed"))
                //{
                warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser,
                   GMG.CoZone.Common.AppModule.Collaborate, lstTmp.Count, Context,
                   LoggedAccountBillingPlans.CollaborateBillingPlan, files.Select(t => t.FileSize).Sum());
                //}
                if (!string.IsNullOrEmpty(warningMessage))
                {
                    Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                    dicModelErrors.Add("warningMessage", warningMessage);

                    model.HasWarnings = true;

                    return RedirectWithModelAndErrors(model, dicModelErrors);
                }

                NewApprovalResponse approvalResponse;
                int folderId;
                //using (profiler.Step("ProjectFoldersController.AddNewApproval"))
                //{
                folderId = ApprovalBL.AddNewApproval(LoggedAccountBillingPlans.CollaborateBillingPlan, LoggedAccount,
                    LoggedUser, model, files, model.WebPageSnapshotDelay.ToInt(), phasesCollaborators, Context, out approvalResponse, false, approvalHtmlPageList);
                //}
                if (folderId > 0)
                {
                    Session[Constants.ApprovalsFolderId + this.LoggedUser.ID.ToString()] = folderId;
                    Session[Constants.ApprovalsResetParameters] = false;
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorLogging.log.Error(exbl.Message, exbl);

                model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                TempData["NewApprovalModel"] = model;
                return RedirectToAction("NewProjectFolderApproval", "ProjectFolders");
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);

                model.ErrorMessage = Resources.Resources.errFileCannotProcess;
                TempData["NewApprovalModel"] = model;
                return RedirectToAction("NewProjectFolderApproval", "ProjectFolders");
            }

            if (!string.IsNullOrEmpty(model.ErrorMessage))
            {
                TempData["NewApprovalModel"] = model;
                return RedirectToAction("NewProjectFolderApproval", "ProjectFolders");
            }

            // Nulable the existing folder value
            Session[Constants.ApprovalsExistingFolders + this.LoggedUser.ID.ToString()] = null;
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SetJobStatus")]
        public ActionResult SetJobStatus(ApprovalDetailsModel model, string SelectedJobStatus)
        {
            if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Job>(model.Job, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            try
            {
                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                }

                int approval = 0;
                Job objJob = DALUtils.GetObject<GMGColorDAL.Job>(model.Job, Context);

                approval = objJob.Approvals.First().ID;

                if (objJob.Approvals.Any(t => !String.IsNullOrEmpty(t.FTPSourceFolder)))
                {
                    var lastVersFromFTP =
                        objJob.Approvals.LastOrDefault(t => !String.IsNullOrEmpty(t.FTPSourceFolder)).ID;
                    ApprovalBL.CreateQueueMessageForUpdateApprovalJobStatus(lastVersFromFTP);
                }

                objJob.Status = int.Parse(SelectedJobStatus);
                objJob.ModifiedDate = DateTime.UtcNow;

                var approvalCollaborationDecision = objJob.Approvals.OrderByDescending(x => x.ID).FirstOrDefault().ApprovalCollaboratorDecisions.Where(x => x.Collaborator == this.LoggedUser.ID).Select(x => x).FirstOrDefault();
                if (approvalCollaborationDecision != null)
                {
                    if (SelectedJobStatus == "3")
                    {
                        approvalCollaborationDecision.CompletedDate = DateTime.UtcNow;

                        var approvalJobCompleted = new GMGColorNotificationService.Notifications.Approvals.ApprovalJobCompleted();
                        approvalJobCompleted.EventCreator = LoggedUser.ID;
                        approvalJobCompleted.ExternalCreator = null;
                        approvalJobCompleted.InternalRecipient = objJob.Approvals.First().Owner;
                        approvalJobCompleted.CreatedDate = DateTime.UtcNow;
                        approvalJobCompleted.ApprovalsIds = new[] { approval };
                        approvalJobCompleted.NewDecisionId = (int)JobStatu.ApprovalDecision.Approved;

                        NotificationServiceBL.CreateNotification(approvalJobCompleted, null, Context, true, null);

                    }

                    else
                    {
                        approvalCollaborationDecision.CompletedDate = null;
                    }
                }

                NotificationServiceBL.CreateNotification(new ApprovalWasUpdated
                {
                    EventCreator = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                    InternalRecipient = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                    CreatedDate = DateTime.UtcNow,
                    ApprovalsIds = new[] { approval }
                },
                                                    OutOfOfficeUserID == 0 ? LoggedUser : OutOfOfficeUser,
                                                    Context,
                                                    false
                                                    );

                var status = JobStatu.GetJobStatus(objJob.Status, Context);

                if (status == JobStatu.Status.ChangesComplete)
                {
                    NotificationServiceBL.CreateNotification(new ApprovalChangesCompleted
                    {
                        EventCreator = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                        InternalRecipient = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                        CreatedDate = DateTime.UtcNow,
                        ApprovalsIds = new[] { approval }
                    },
                                                            OutOfOfficeUserID == 0 ? LoggedUser : OutOfOfficeUser,
                                                            Context
                                                            );

                    JobBL.LogJobStatusChange(objJob.ID, objJob.Status, OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, Context);

                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(new DashboardInstantNotification()
                    {
                        ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                        Creator = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID,
                        Version = objJob.ID.ToString(),
                        EntityType = InstantNotificationEntityType.Decision,
                        DecisionDisplayText = status.ToString(),
                        DecisionLabelColorName = JobStatu.Status.ChangesComplete.ToString().ToLower()
                    });
                }

                Context.SaveChanges();

                var jobDetails = new JobStatusDetails()
                {
                    jobGuid = objJob.Guid,
                    jobStatus = objJob.JobStatu.Name,
                    jobStatusKey = objJob.JobStatu.Key,
                    username = OutOfOfficeUserID == 0 ? LoggedUser.Username : OutOfOfficeUser.Username,
                    email = OutOfOfficeUserID == 0 ? LoggedUser.EmailAddress : OutOfOfficeUser.EmailAddress
                };
                ApprovalBL.PushJobStatus(jobDetails, LoggedAccount.ID, Context);

            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Set Job Status Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalDetails");
        }

        

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RestoreFolder")]
        public ActionResult RestoreFolder(ApprovalsModel model)
        {
            try
            {
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Folder>(model.objFolderCR.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                FoldersModel objModel = new FoldersModel()
                {
                    Account = this.LoggedAccount.ID,
                    Creator = this.LoggedUser.ID,
                    Folder = model.objFolderCR.ID,
                    Status = FoldersModel.FolderStatus.Restore
                };

                FolderBL.FolderCRD(Context, ref objModel, true);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.RestoreFolder : Restore folder failed, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }


        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "MarkApprovalAsApproved")]
        public ActionResult MarkApprovalAsApproved(int selectedApproval, string HiddenSelectedApprovals)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (HiddenSelectedApprovals != "")
                {
                    lstApprovalIds = HiddenSelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }

                if (selectedApproval > 0)
                {
                    lstApprovalIds.Add(selectedApproval);
                }

                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                foreach (int approvalId in lstApprovalIds.Distinct())
                {
                    UpdateApprovalDecision model = new UpdateApprovalDecision();
                    model.Id = approvalId;
                    model.key = string.Empty;
                    model.user_key = LoggedUser.Guid;
                    model.session_key = string.Empty;
                    model.collaboratorId = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID;
                    model.isExternal = false;
                    model.decisionId = (int)Approval.ApprovalDecision.Approved;

                    var response = ApiBL.UpdateApprovalDecision(model);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.MarkApprovalAsApproved : Marking the approved as completed failed, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "MarkApprovalAsRejected")]
        public ActionResult MarkApprovalAsRejected(int selectedApproval, string HiddenSelectedApprovals)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (HiddenSelectedApprovals != "")
                {
                    lstApprovalIds = HiddenSelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }

                if (selectedApproval > 0)
                {
                    lstApprovalIds.Add(selectedApproval);
                }

                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                foreach (int approvalId in lstApprovalIds.Distinct())
                {
                    UpdateApprovalDecision model = new UpdateApprovalDecision();
                    model.Id = approvalId;
                    model.key = string.Empty;
                    model.user_key = LoggedUser.Guid;
                    model.session_key = string.Empty;
                    model.collaboratorId = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID;
                    model.isExternal = false;
                    model.decisionId = (int)Approval.ApprovalDecision.ChangesRequired;

                    var response = ApiBL.UpdateApprovalDecision(model);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.MarkApprovalAsRejected : Marking the approved as completed failed, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "MarkPhaseAsCompleted")]
        public ActionResult MarkPhaseAsCompleted(int selectedApproval, string HiddenSelectedApprovals)
        {
            try
            {
                List<int> lstApprovalIds = new List<int>();
                if (HiddenSelectedApprovals != "")
                {
                    lstApprovalIds = HiddenSelectedApprovals.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }

                if (selectedApproval > 0)
                {
                    lstApprovalIds.Add(selectedApproval);
                }
                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }

                foreach (int approvalId in lstApprovalIds.Distinct())
                {
                    var isLastPhaseCompleted = false;
                    var phase = ApprovalBL.MarkPhaseAsCompleted(approvalId, OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, LoggedAccount, Context, out isLastPhaseCompleted);

                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(phase);

                    if (!isLastPhaseCompleted)
                    {
                        ApprovalBL.SendEmailsToUsersFromCurrentPhase(approvalId, OutOfOfficeUserID == 0 ? LoggedUser : OutOfOfficeUser, Context);
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.MarkPhaseAsCompleted : Marking the phase as completed failed, message: {0}", ex.Message);
            }
            finally
            {
            }
            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        [HttpGet]
        public ActionResult LoadSubmitToDeliverPopup(bool? isFromProofStudio)
        {
            try
            {
                Deliver.AddInstance popupModel = new Deliver.AddInstance(LoggedAccount, LoggedUser.ID, null, Context) { ShowUploader = false };
                ViewBag.DeliverRole = LoggedUserDeliverRole;

                string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser, GMG.CoZone.Common.AppModule.Deliver, 0, Context, LoggedAccountBillingPlans.DeliverBillingPlan);
                if (!string.IsNullOrEmpty(warningMessage))
                {
                    popupModel.ErrorMessage = warningMessage;

                }
                if (isFromProofStudio != null)
                {
                    return Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, "~\\Views\\Studio\\DeliverAddJobFromProofStudio.cshtml", LoggedUser.ID, popupModel)
                    },
                    JsonRequestBehavior.AllowGet);
                }
                return Json(
                    new
                    {
                        Status = 400,
                        Content = RenderViewToString(this, "DeliverAddJob", LoggedUser.ID, popupModel)
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while loading submit to deliver popup, Method: LoadSubmitToDeliverPopup, Exception: {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error occured while populating Submit to Deliver Popup"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult LoadAdvancedSearchPopup(int TopLinkID)
        {
            try
            {
                GMGColorDAL.CustomModels.AdvancedSearch popupModel;

                if (TopLinkID <= -Constants.TopLinkOffset)
                {
                    int advsId = -TopLinkID - Constants.TopLinkOffset;
                    popupModel = ApprovalBL.GetAdvancedSearch(advsId, Context, LoggedAccount.DateFormat1.Pattern);
                }
                else
                {
                    popupModel = new GMGColorDAL.CustomModels.AdvancedSearch(LoggedAccount.DateFormat1.Pattern)
                    {
                        ID = 0,
                        AdvSName = "",

                    };
                }

                popupModel.AdvSGroups = ApiBL.GetAllGroupsForAccount(this.LoggedAccount, this.LoggedUser, LoggedUserCollaborateRole);
                popupModel.AdvSUsers = ApiBL.GetAllUsersForAccount(this.LoggedAccount, this.LoggedUser, LoggedUserCollaborateRole);
                popupModel.AdvSWorkflows = ApiBL.GetAllWorkflowsForAccount(this.LoggedAccount.ID);
                popupModel.AdvSPhases = ApiBL.GetAllPhasesForAccount(this.LoggedAccount.ID);

                popupModel.LoggedUserRole = LoggedUserCollaborateRole;

                ViewBag.AccountDateFormat = LoggedAccount.DateFormat1.Pattern;

                return Json(new
                {
                    Status = 400,
                    Content = RenderViewToString(this, "AdvancedSearch", LoggedUser.ID, popupModel)
                },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured while loading advanced search, Method: LoadAdvancedSearchPopup, Exception: {0}", ex.Message);

                return Json(
                           new
                           {
                               Status = 300,
                               Content = "Error occured while populating Advanced Search"
                           },
                           JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddDeliverJobFromPS(Deliver.AddInstance model)
        {
            return AddDeliverJob(model);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddDeliverJob")]
        public JsonResult AddDeliverJob(Deliver.AddInstance model)
        {
            bool success = true;
            try
            {
                if (!string.IsNullOrEmpty(model.SelectedApprovalJobs))
                {
                    int?[] jobIdList = model.SelectedApprovalJobs.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();
                    int?[] pageRange = (model.PagesRangeValue ?? string.Empty).Split(new string[] { "-", "," }, StringSplitOptions.RemoveEmptyEntries).ToIntArray();

                    if (!CanAccess(jobIdList, GMGColorDAL.ProjectFolder.ApprovalOperation.SendJobToDeliver))
                    {
                        success = false;
                        ModelState.AddModelError("", Resources.Resources.NoRightsForThisAction);
                    }

                    if (jobIdList.Count() == 1 && success)
                    {
                        GMGColorDAL.Approval approval =
                            DALUtils.GetObject<Approval>(jobIdList.FirstOrDefault().GetValueOrDefault(), Context);

                        if (approval == null || approval.Job1.Account != LoggedAccount.ID)
                        {
                            ModelState.AddModelError("", "Approval job does not exist for the current account");
                        }

                        if (model.SelectedCPServerId == null)
                        {
                            ModelState.AddModelError("", Resources.Resources.reqCPRegistration);
                            ModelState.AddModelError("", Resources.Resources.reqCPWorkflow);
                            success = false;
                        }
                        else
                        {
                            if (model.SelectedCPWorkflowId == null)
                            {
                                ModelState.AddModelError("", Resources.Resources.reqCPWorkflow);
                                success = false;
                            }
                        }

                        if (pageRange.Any(o => o == null || (approval != null && o > approval.DocumentPagesCount(Context))))
                        {
                            ModelState.AddModelError("", Resources.Resources.reqRangeValueMaxPageCount);
                            success = false;
                        }
                    }

                    if (success)
                    {
                        //if no other errors check if user can submit jobs
                        string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(LoggedAccount, LoggedUser, GMG.CoZone.Common.AppModule.Deliver, jobIdList.Length, Context, LoggedAccountBillingPlans.DeliverBillingPlan);
                        if (!string.IsNullOrEmpty(warningMessage))
                        {
                            model.ErrorMessage = warningMessage;
                            success = false;
                        }
                        else
                        {
                            List<DeliverJob> files = new List<DeliverJob>();
                            foreach (int? jobId in jobIdList)
                            {
                                if (!jobId.HasValue)
                                    continue;

                                GMGColorDAL.Approval approval = DALUtils.GetObject<GMGColorDAL.Approval>(jobId.Value,
                                                                                                         Context);

                                if (approval == null || approval.Job1.Account != LoggedAccount.ID)
                                    continue;

                                files.Add(new DeliverJob()
                                {
                                    Guid = Guid.NewGuid().ToString(),
                                    Name = approval.FileName,
                                    IncludeProofMetaInformation = model.IncludeProofMetaInformation,
                                    LogProofControlResults = model.LogProofControlResults,
                                    PagesRadioButton = model.PagesRadioButton,
                                    PagesRangeValue = model.PagesRangeValue,
                                    SelectedCpWorkflowId = model.SelectedCPWorkflowId.GetValueOrDefault(),
                                    Size = approval.Size,
                                    Title = approval.Job1.Title,
                                    SourcePath =
                                        GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath +
                                        "/" + approval.Guid + "/",
                                    TotalNrOfPages = approval.DocumentPagesCount(Context)
                                });
                            }
                            if (!CreateDeliverJobs(files.ToArray(), false, model.ExternalUsers, model.ExternalEmails, model.OptionalMessage))
                            {
                                throw new Exception("An error ocurred when creating the deliver jobs");
                            }

                            if (!jobIdList.Any())
                                throw new Exception("No approval jobs were selected from the page!");

                            var jobGuids = files.Select(t => t.Guid).ToArray();

                            NotificationServiceBL.CreateNotification(new GMGColorNotificationService.NewDeliverJob()
                            {
                                EventCreator = LoggedUser.ID,
                                InternalRecipient = LoggedUser.ID,
                                CreatedDate = DateTime.UtcNow,
                                JobsGuids = jobGuids
                            },
                                                                     LoggedUser,
                                                                     Context
                                                                    );


                            var deliverExternalUsers = DeliverExternalCollaborator.GetDeliverCollaboratorsByJobGuids(jobGuids, EventType.GetDeliverEventTypeKey(EventType.TypeEnum.Deliver_File_Submitted), Context);

                            //send emails for deliver external users
                            foreach (var user in deliverExternalUsers)
                            {
                                NotificationServiceBL.CreateNotification(new NewDeliverJob()
                                {
                                    EventCreator = LoggedUser.ID,
                                    ExternalRecipient = user,
                                    CreatedDate = DateTime.UtcNow,
                                    JobsGuids = jobGuids
                                },
                                                                   LoggedUser,
                                                                   Context
                              );
                            }

                            if (DeliverBL.IsSharedWorkflow(model.SelectedCPWorkflowId, Context))
                            {
                                var user = DeliverBL.GetUserByWorkflowID(model.SelectedCPWorkflowId, Context);

                                if (user.Account != LoggedAccount.ID)
                                {
                                    NotificationServiceBL.CreateNotification(new HostAdminUserFileSubmitted()
                                    {
                                        EventCreator = LoggedUser.ID,
                                        InternalRecipient = user.ID,
                                        JobsGuids = jobGuids,
                                        IsSharedJob = LoggedUser.Account != user.Account
                                    },
                                                                             LoggedUser,
                                                                             Context
                                        );

                                    NotificationServiceBL.CreateNotification(new InvitedUserFileSubmitted()
                                    {
                                        EventCreator = LoggedUser.ID,
                                        InternalRecipient = LoggedUser.ID,
                                        JobsGuids = jobGuids
                                    },
                                                                             LoggedUser,
                                                                             Context
                                        );
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Job could not be sent to deliver, message: {0}", ex.Message);
            }

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var errors = ModelState.Keys.Where(key => ModelState[key].Errors.Any()).ToDictionary(key => key, key => ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());

            return Json(serializer.Serialize(new
            {
                Success = success,
                Errors = errors
            }));
        }

        [HttpPost]
        public JsonResult UpdateApprovalsDeadLine(int approvalID, string deadlineDate, string deadlineTime, string deadlineTimeMeridiem, string dateFormat, int timeFormat)
        {
            try
            {
                int OutOfOfficeUserID = 0;
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    OutOfOfficeUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                }
                DateTime deadline = ApprovalBL.GetFormatedApprovalDeadline(deadlineDate, deadlineTime, deadlineTimeMeridiem, dateFormat, timeFormat);
                var utcDeadLine = GMGColorFormatData.SetUserTimeToUTC(deadline, LoggedAccount.TimeZone);

                ApprovalBL.UpdateApprovalDeadLine(approvalID, utcDeadLine, Context, LoggedAccount.ID, LoggedAccount.User.EmailAddress);

                SignalRService signalRService = new SignalRService();
                signalRService.AddInstantDashboardNotification(
                    ApprovalBL.CreateDashboardInstantNotificationDeadLineItem(approvalID, OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, deadline, dateFormat, timeFormat, false)
                );

                return Json(
                    new
                    {
                        Status = 400,
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error updating approval deadline" + ex.Message
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdatePhaseDeadline(int phaseID, string deadlineDate, string deadlineTime, string deadlineTimeMeridiem, string dateFormat, int timeFormat, int approvalId)
        {
            try
            {
                DateTime deadline = ApprovalBL.GetFormatedApprovalDeadline(deadlineDate, deadlineTime, deadlineTimeMeridiem, dateFormat, timeFormat);
                deadline = GMGColorFormatData.SetUserTimeToUTC(deadline, LoggedAccount.TimeZone);

                ApprovalBL.UpdatePhaseDeadline(phaseID, deadline, approvalId, Context, LoggedAccount.ID, LoggedUser.EmailAddress);

                if (ApprovalBL.UpdatedDeadlineIsFromCurrentPhase(phaseID, approvalId, Context))
                {
                    var Deadline = GMGColorFormatData.GetUserTimeFromUTC(deadline, LoggedAccount.TimeZone);
                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(ApprovalBL.CreateDashboardInstantNotificationDeadLineItem(approvalId, LoggedUser.ID, Deadline, dateFormat, timeFormat, true));
                }

                return Json(
                    new
                    {
                        Status = 400,
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error updating phase deadline" + ex.Message
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveViewingConditions(ApprovalDetailsModel model)
        {
            try
            {
                if (!DALUtils.IsFromCurrentAccount<Job>(model.Job, LoggedAccount.ID, Context))
                {
                    Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
                else
                {
                    if (SoftProofingBL.SaveViewingConditions(model.Job,
                        model.DefaultViewingConditions.SelectedSimulationProfile,
                        model.DefaultViewingConditions.SelectedPaperTint, Context))
                    {
                        //if viewing conditions changed send Instant Notification
                        int[] versions = ApprovalBL.GetVersionIds(model.Job, Context);
                        IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();

                        foreach (var version in versions)
                        {
                            SignalRService signalRService = new SignalRService();
                            signalRService.AddInstantNotification(context, InstantNotificationEntityType.ViewingConditions, version, LoggedUser.ID);
                        }
                    }
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Save Viewing Conditions failed, message: {0}", ex.Message);
            }

            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(false);
        }

        [HttpGet]
        public JsonResult GetDecision(string decisionKey, string decisionMaker)
        {
            try
            {
                SetUICulture(LoggedUser.ID);
                var decision = ApprovalBL.GetDecisionDashboardLabel(decisionKey, decisionMaker, Context, LoggedUser);
                return Json(decision, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving phase details" + ex.Message
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult CanUserAccess(string id)
        {
            try
            {
                if (id == "")
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<int> approvalIds = id.Split(',').Select(Int32.Parse).ToList();
                    //if (LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator || LoggedUserCollaborateRole == Role.RoleName.AccountManager)
                    //{
                    //    return Json(true, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                        bool userCanAccess = ApprovalBL.CheckUserCanAccess(approvalIds, GMGColorDAL.Approval.CollaborateValidation.ApprovalAccess, this.LoggedUser.ID, Context);
                        return Json(userCanAccess, JsonRequestBehavior.AllowGet);
                   // }
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetApprovalStatus(int approvalId, int decisionType, bool isPhaseComplete, string decisionKey, string decisionMakers)
        {
            try
            {
                SetUICulture(LoggedUser.ID);
                var status = ApprovalBL.GetApprovalStatus(approvalId, decisionType, isPhaseComplete, decisionKey, decisionMakers);
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error retrieving approval status" + ex.Message
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult InsertApprovalsTagwords(string tagword)
        {
            try
            {
                int approvaltagwordsId = PredefinedtagwordsBL.AddNewPredefinedtagword(tagword, LoggedAccount.ID, LoggedUser.ID, Context, GMGColorDAL.CustomModels.PredefinedTagwords.TypeOfTagword.ApprovalTagword);
                return Json(approvaltagwordsId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error inserting approval tagwords" + ex.Message
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }


        #endregion


        #region Methods

        [HttpPost]
        [System.Web.Services.WebMethod(Description = "Keeps the current session alive", EnableSession = true)]
        public void PingSession()
        {
            //No need any content.
            //This method will keeps the session alive if approval is uploading.
        }

        [HttpPost]
        public ActionResult ApplyFolderPermissions(int selectedFolder)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();

                GMGColorLogging.log.Info("Running 'ApplyFolderPermissions'...");
                PermissionsBL.ApplyFolderPermissions(selectedFolder, LoggedUser, Context);
                GMGColorLogging.log.InfoFormat("Finish 'ApplyFolderPermissions' method execution took: {0}", watch.ElapsedMilliseconds);

                watch.Restart();
                GMGColorLogging.log.InfoFormat("Running database operations for 'ApplyFolderPermissions' took : {0}", watch.ElapsedMilliseconds);
                Context.SaveChanges();
                GMGColorLogging.log.InfoFormat("Database operations for 'ApplyFolderPermissions' took : {0}", watch.ElapsedMilliseconds);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "GMGColorDAL.Approval.ApplyFolderPermissions : Applying folder permissions failed. {0}", ex.Message);
            }

            return RedirectToAction("ProjectFolderApprovalsIndex", "ProjectFolders");
        }

        public Dictionary<string, string> GetMessageParameters(string message)
        {
            Dictionary<string, string> dicPara = new Dictionary<string, string>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(message);

            if (xmlDoc.HasChildNodes)
            {
                foreach (XmlNode node in xmlDoc.LastChild.ChildNodes)
                {
                    dicPara.Add(node.Name, node.InnerText);
                }
            }
            return dicPara;
        }

        private void DeleteSelectedApproval(int approvalId, bool isRecursive, bool adminHasFullRights, DbContextBL context, Approval.ProcessingStatus processingStatus, bool sendNotifications = true)
        {
            try
            {
                int SubstituteUserId = 0;
                GMGColorDAL.User SubstituteUser;
                if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    SubstituteUserId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                    SubstituteUser = Context.Users.FirstOrDefault(u => u.ID == SubstituteUserId);
                }

                using (TransactionScope tscope = new TransactionScope())
                {
                    var approval = (from a in context.Approvals where a.ID == approvalId select a).FirstOrDefault();
                    if (approval != null)
                    {
                        if (JobStatu.GetJobStatus(approval.Job1.Status, Context) == JobStatu.Status.Archived)
                        {
                            foreach (var objApproval in approval.Job1.Approvals)
                            {
                                if ((LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator && adminHasFullRights) || objApproval.Owner == LoggedUser.ID)
                                {
                                    //delete from database
                                    Approval.MarkApprovalForPermanentDeletion(objApproval, LoggedUser.ID, context);
                                }
                            }
                        }
                        else if (approval.IsError)
                        {
                            Approval.MarkApprovalForPermanentDeletion(approval, LoggedUser.ID, context);
                        }
                        else
                        {
                            approval.IsDeleted = true;
                            approval.ModifiedDate = DateTime.UtcNow;
                            approval.Modifier = LoggedUser.ID;

                            ApprovalBL.ApprovalUserRecycleBinHistory(approval, (SubstituteUserId == 0 ? LoggedUser.ID : SubstituteUserId), true, context);

                            if (isRecursive)
                            {
                                var approvals = (from a in context.Approvals where a.Job == approval.Job && a.ID != approvalId select a).ToList();
                                foreach (var app in approvals)
                                {
                                    if (app.Owner == LoggedUser.ID || (adminHasFullRights && LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator))
                                    {
                                        app.IsDeleted = true;
                                        app.ModifiedDate = DateTime.UtcNow;

                                        ApprovalBL.ApprovalUserRecycleBinHistory(app, LoggedUser.ID, true, context);
                                    }
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                    tscope.Complete();
                }

                if (sendNotifications)
                {
                    GMGColorDAL.User substituteUser = Context.Users.FirstOrDefault(u => u.ID == SubstituteUserId);
                    NotificationServiceBL.CreateNotification(new ApprovalWasDeleted
                    {
                        EventCreator = SubstituteUserId == 0 ? LoggedUser.ID : SubstituteUserId,
                        InternalRecipient = SubstituteUserId == 0 ? LoggedUser.ID : SubstituteUserId,
                        CreatedDate = DateTime.UtcNow,
                        ApprovalsIds = new[] { approvalId }
                    },
                            (SubstituteUserId == 0 ? LoggedUser : substituteUser),
                                                             Context
                        );
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Delete Approval Failed. {0}", ex.Message);
            }
        }

        private void ArchivedSelectedApproval(int approvalId, DbContextBL context)
        {
            try
            {
                using (TransactionScope tscope = new TransactionScope())
                {
                    GMGColorDAL.Approval objApproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, context);

                    objApproval.Job1.Status = 4;
                    objApproval.Job1.ModifiedDate = DateTime.UtcNow;
                    objApproval.ModifiedDate = DateTime.UtcNow;
                    objApproval.Modifier = this.LoggedUser.ID;

                    Context.SaveChanges();
                    tscope.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ProjectFoldersController.Index : Archiving Approval Failed. {0}", ex.Message);
            }
        }

       

        private bool CanAccess(IEnumerable<int?> approvalIdList, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            return CanAccess(approvalIdList.Select(o => o.GetValueOrDefault()), operation);
        }
        private bool CanAccess(int? approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            return CanAccess(new int[] { approvalId.GetValueOrDefault() }, operation);
        }
        private bool CanAccess(int approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            return CanAccess(new int[] { approvalId }, operation);
        }
        private bool CanAccess(IEnumerable<int> approvalIdList, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            //if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
            //{
            //    int SubstituteUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
            //    Role.RoleName LoggedUserCollaborateRole1 = Role.GetRole(UserBL.GetRoleKey(GMG.CoZone.Common.AppModule.Collaborate, LoggedUser.ID, Context));
            //    return
            //      GMGColorDAL.ProjectFolder.CanAccess(SubstituteUserID, approvalIdList, operation, Context)
            //      || (LoggedUserCollaborateRole1 == Role.RoleName.AccountAdministrator && DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalIdList, LoggedAccount.ID, Context));
            //}
            //return
            //       GMGColorDAL.ProjectFolder.CanAccess(LoggedUser.ID, approvalIdList, operation, Context)
            //       || ((LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator || LoggedUserCollaborateRole == Role.RoleName.AccountManager) && DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalIdList, LoggedAccount.ID, Context));

            return true;
        }

        private bool CanAccessApproval(int approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            return CanAccessApproval(new int[] { approvalId }, operation);
        }

        private bool CanAccessApproval(IEnumerable<int> approvalIdList, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            //if (Session[Constants.ApprovalPopulateId] != null && int.Parse(Session[Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[Constants.OutOfOfficeUser].ToString()) > 0)
            //{
            //    int SubstituteUserID = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
            //    Role.RoleName LoggedUserCollaborateRole1 = Role.GetRole(UserBL.GetRoleKey(GMG.CoZone.Common.AppModule.Collaborate, LoggedUser.ID, Context));
            //    return
            //      GMGColorDAL.ProjectFolder.CanAccess(SubstituteUserID, approvalIdList, operation, Context)
            //      || (LoggedUserCollaborateRole1 == Role.RoleName.AccountAdministrator && DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalIdList, LoggedAccount.ID, Context));
            //}
            //return
            //       GMGColorDAL.ProjectFolder.CanAccess(LoggedUser.ID, approvalIdList, operation, Context)
            //       && (DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalIdList, LoggedAccount.ID, Context));
            return true;
        }

        private bool CanSubstituteUserAccess(int approvalIdList, int substituteOwnerId, GMGColorDAL.ProjectFolder.ApprovalOperation operation)
        {
            return
                   GMGColorDAL.ProjectFolder.CanAccess(substituteOwnerId, new int[] { approvalIdList }, operation, Context)
                   || (LoggedUserCollaborateRole == Role.RoleName.AccountAdministrator && DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalIdList, LoggedAccount.ID, Context));
        }


        

        /// <summary>
        /// used for annotations page and for external browshot service
        /// </summary>
        /// <param name="reportFilters"></param>
        /// <returns></returns>
        private ActionResult GetAnnotationsPage(AnnotationsReportFilters reportFilters, bool fromExternalSource)
        {
            var model = new ApprovalAnnotationsReportModel();

            List<int> selectedApprovals = GetSelectedApprovals(reportFilters);

            //Give default value in case initial values are null
            reportFilters.ReportType = ((AnnotationReportType?)reportFilters.ReportTypeID);
            reportFilters.GroupBy = ((AnnotationReportGrouping?)reportFilters.ReportGroupingID);
            reportFilters.AnnotationsPerPage = reportFilters.AnnotationsPerPage;
            reportFilters.FilterBy = ((AnnotationReportFiltering)reportFilters.ReportFilteringID);

            //check if groups option is selected and get users from selected groups
            if (reportFilters.FilterBy == AnnotationReportFiltering.Group)
            {
                if (reportFilters.IncludeAllGroups)
                {
                    reportFilters.AnnotationsReportUsers =
                    UserBL.GetUsersByGroupsId(reportFilters.AnnotationsReportGroups.Select(t => t.ID), Context);
                }
                else
                {
                    reportFilters.AnnotationsReportUsers =
                    UserBL.GetUsersByGroupsId(reportFilters.AnnotationsReportGroups.Where(t => t.IsSelected).Select(t => t.ID), Context);
                }
            }

            else if (!reportFilters.IncludeAllVersions)
            {
                if (reportFilters.IncludeAllInternalUsers || reportFilters.IncludeAllExternalUsers)
                {
                    if (reportFilters.IncludeAllInternalUsers && !reportFilters.IncludeAllExternalUsers)
                    {
                        reportFilters.AnnotationsReportUsers.RemoveAll(t => t.IsExternal && !t.IsSelected);
                    }
                    if (reportFilters.IncludeAllExternalUsers && !reportFilters.IncludeAllInternalUsers)
                    {
                        reportFilters.AnnotationsReportUsers.RemoveAll(t => !t.IsExternal && !t.IsSelected);
                    }
                }
                else
                {
                    reportFilters.AnnotationsReportUsers.RemoveAll(t => !t.IsSelected);
                }
            }
            int substituteOwnerId = 0;
            if (Session[Constants.SelectedTopLinkId] != null && int.Parse(Session[Constants.SelectedTopLinkId].ToString()) == -8)
            {
                substituteOwnerId = int.Parse(Session[Constants.OutOfOfficeUser].ToString());
                GMGColorDAL.User susbtitutedUser = Context.Users.FirstOrDefault(u => u.ID == substituteOwnerId);
                this.LoggedUser = susbtitutedUser;

            }

            bool IsApprovalTypeVideo = AnnotationBL.GetApprovalType(selectedApprovals, Context);
            if (reportFilters.ReportFormatType == AnnotationReportFormatType.PDFView)
            {

                var modelPDF = new ApprovalAnnotationsReportModelPDF();

                reportFilters.ReportType = AnnotationReportType.CommentsAndPins;//((AnnotationReportType?)reportFilters.ReportTypeID);

                Session[Constants.ApprovalsResetParameters] = false;
                int annotationOrderNumber = 0;
                foreach (var approvalId in selectedApprovals)
                {
                    annotationOrderNumber = 1;
                    if (!fromExternalSource && (substituteOwnerId == 0 ? (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.Annotations)) : (!CanSubstituteUserAccess(approvalId, substituteOwnerId, GMGColorDAL.ProjectFolder.ApprovalOperation.Annotations))))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    Session[Constants.SelectedApprovalId] = approvalId;

                    var u = LoggedUser; 

                    ApprovalAnnotationsModelPDF approvalAnnotationModelPDF = ApprovalBL.GetApprovalInfoPDF(approvalId,
                        LoggedAccount, Context);
                    if (approvalAnnotationModelPDF.ApprovalName.Length > 40)
                    {
                        approvalAnnotationModelPDF.ApprovalName = approvalAnnotationModelPDF.ApprovalName.Insert(40, " ");
                    }

                    if (approvalAnnotationModelPDF.JobTitle.Length > 40)
                    {
                        approvalAnnotationModelPDF.JobTitle = approvalAnnotationModelPDF.JobTitle.Insert(40, " ");
                    }

                    approvalAnnotationModelPDF.ReportType = reportFilters.ReportType.Value;

                    List<int> selectedPhases = GetWorkflowPhases(reportFilters.AnnotationsReportPhases, (reportFilters.IncludeAllPhases || reportFilters.IncludeAllVersions), approvalId, Context);

                    approvalAnnotationModelPDF.IsApprovalTypeVideo = IsApprovalTypeVideo;
                    approvalAnnotationModelPDF.PageAnnotations = AnnotationBL.GetCommentsAnnotationPDFReportModel(approvalId, reportFilters.GroupBy.Value, reportFilters.AnnotationsReportUsers,
                          selectedPhases, LoggedAccount, LoggedUser, IsApprovalTypeVideo, Context);

                    foreach (var pageAnnotation in approvalAnnotationModelPDF.PageAnnotations)
                    {
                        foreach (var annotationInfo in pageAnnotation.AnnotationInfo)
                        {
                            annotationInfo.AnnotationOrderNumber = annotationOrderNumber;
                            annotationOrderNumber++;
                        }
                    }

                    modelPDF.ApprovalAnnotationsModelPDF.Add(approvalAnnotationModelPDF);
                }

                ViewBag.ReportType = reportFilters.ReportType.ToString();
                ViewBag.IsFromDashboard = reportFilters.IsFromDashboard.GetValueOrDefault();
                ViewBag.IsFromApiCall = reportFilters.IsFromAPICall;
                ViewBag.ShowSummaryPage = true;

                return View("ProjectFolderApprovalAnnotationsForPDF", modelPDF);
            }
            else if (reportFilters.ReportFormatType == AnnotationReportFormatType.DetailView)
            {
                //visual report

                Session[Constants.ApprovalsResetParameters] = false;
                foreach (var approvalId in selectedApprovals)
                {
                    if (!fromExternalSource && (substituteOwnerId == 0 ? (!CanAccess(approvalId, GMGColorDAL.ProjectFolder.ApprovalOperation.Annotations)) : (!CanSubstituteUserAccess(approvalId, substituteOwnerId, GMGColorDAL.ProjectFolder.ApprovalOperation.Annotations))))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    Session[Constants.SelectedApprovalId] = approvalId;

                    ApprovalAnnotationsModel approvalAnnotationModel = ApprovalBL.GetApprovalInfo(approvalId, LoggedUser,
                        LoggedAccount, Context);
                    approvalAnnotationModel.ReportType = reportFilters.ReportType.Value;
                    approvalAnnotationModel.IsApprovalTypeVideo = IsApprovalTypeVideo;

                    List<int> selectedPhases = GetWorkflowPhases(reportFilters.AnnotationsReportPhases, (reportFilters.IncludeAllPhases || reportFilters.IncludeAllVersions), approvalId, Context);

                    approvalAnnotationModel.PageAnnotations =
                        AnnotationBL.GetCommentsAnnotationReportModel(approvalId, reportFilters.GroupBy.Value,
                            reportFilters.AnnotationsReportUsers, selectedPhases, LoggedAccount, reportFilters.AnnotationsPerPage, LoggedUser, IsApprovalTypeVideo,
                            Context);
                    model.ApprovalAnnotationsModel.Add(approvalAnnotationModel);
                }

                ViewBag.ReportType = reportFilters.ReportType.ToString();
                ViewBag.IsFromDashboard = reportFilters.IsFromDashboard.GetValueOrDefault();
                ViewBag.ShowSummaryPage = reportFilters.ShowSummaryPage;
                ViewBag.IsFromApiCall = reportFilters.IsFromAPICall;

                return View("ProjectFolderApprovalAnnotations", model);
            }
            else
            {
                //Download as CSV
                string csvFileName = String.Empty;
                MemoryStream output = AnnotationBL.CreateAnnotationReportCSV(selectedApprovals, LoggedUser, reportFilters.GroupBy.Value, reportFilters.AnnotationsReportUsers, LoggedAccount, out csvFileName, Context);
                output.Seek(0, SeekOrigin.Begin);
                return File(output, "text/csv", csvFileName);
            }
        }

        private List<int> GetWorkflowPhases(List<AnnotationReportPhase> annotationReportPhases, bool includeAllPhases, int approvalId, DbContextBL context)
        {
            List<int> selectedPhasesIds = new List<int>();

            // When include all version or include all Phases is checked, phases do not come as selected
            if (includeAllPhases)
            {
                List<ApprovalJobPhase> jobPhases = ApprovalBL.GetJobAllAvailablePhasesWithAnnotations(approvalId, LoggedUser.ID, context);

                if (jobPhases != null && jobPhases.Count > 0)
                {
                    selectedPhasesIds.AddRange(jobPhases.Select(t => t.ID).ToList());
                    selectedPhasesIds.Distinct().ToList();
                }
            }
            else
            {
                selectedPhasesIds = annotationReportPhases != null ? annotationReportPhases.Where(t => t.IsSelected).Select(t => t.ID).ToList()
                                                                                    : new List<int>();
            }

            return selectedPhasesIds;
        }

        private List<int> GetSelectedApprovals(AnnotationsReportFilters reportFilters)
        {
            string selectedApprovals = reportFilters.SelectedApprovals;
            string selectedJob = reportFilters.SelectedJob;

            List<int> allSelectedApprovalIds = new List<int>();

            // If only one version is selected
            if (!string.IsNullOrEmpty(selectedApprovals))
            {
                List<int> selectedApprovalIds = selectedApprovals.Split(',').Select(Int32.Parse).ToList();



                if (reportFilters.IncludeAllVersions)
                {
                    foreach (int approvalId in selectedApprovalIds)
                    {
                        int jobId = ApprovalBL.GetJobId(Context, approvalId);

                        List<Approval> jobApprovals = Context.Approvals.Where(a => a.Job == jobId &&
                            a.IsDeleted == false && a.DeletePermanently == false).ToList();

                        if (jobApprovals != null && jobApprovals.Count > 0)
                        {
                            allSelectedApprovalIds.AddRange(jobApprovals.Select(a => a.ID).ToList());
                        }
                    }
                }
                else if (reportFilters.SelectedVersion > 0 && selectedApprovalIds.Count() == 1)
                {
                    foreach (int approvalId in selectedApprovalIds)
                    {
                        int jobId = ApprovalBL.GetJobId(Context, approvalId);
                        Approval jobApprovals = Context.Approvals.Where(a => a.Job == jobId && a.Version == reportFilters.SelectedVersion &&
                            a.IsDeleted == false && a.DeletePermanently == false).FirstOrDefault();

                        if (jobApprovals != null && jobApprovals != null)
                        {
                            allSelectedApprovalIds.Add(jobApprovals.ID);
                        }
                    }

                }
                else
                {
                    allSelectedApprovalIds.AddRange(selectedApprovalIds);
                }
            }
            // If job is specified, get all of it's available versions 
            else if (!string.IsNullOrEmpty(selectedJob))
            {
                int jobId = Convert.ToInt32(selectedJob);

                List<Approval> jobApprovals = Context.Approvals.Where(a => a.Job == jobId &&
                    a.IsDeleted == false).ToList();

                if (jobApprovals != null && jobApprovals.Count > 0)
                {
                    allSelectedApprovalIds.AddRange(jobApprovals.Select(a => a.ID).ToList());
                }
            }

            allSelectedApprovalIds = allSelectedApprovalIds.Distinct().ToList();

            return allSelectedApprovalIds;
        }


        #endregion


        #endregion

    }
}
