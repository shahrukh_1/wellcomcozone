﻿using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.UI;
using AutoMapper;
using GMG.CoZone.Common;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using AppModule = GMG.CoZone.Common.AppModule;
using User = GMGColorDAL.User;
using GMG.CoZone.SSO.Interfaces;
using GMG.CoZone.SSO.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Module.Enums;
using GMG.CoZone.FileTransfer.Interfaces;

namespace GMGColor.Controllers
{
    public class AuthController : BaseController
    {
        private ISsoCore _ssoCore;
        private ICommonService _commonService;

        const string KEY_USERNAME = "Gx3HYk27oLsw";
        const string KEY_PASSWORD = "7fTnjD55YuPB";
        //This key is used to keep in session the version of the GMGColor assembly
        public static string VersionSessionKey = "VERSION";

        public AuthController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {

        }

        public AuthController(ISsoCore ssoCore, ICommonService commonService, IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {
            _ssoCore = ssoCore;
            _commonService = commonService;
        }

        #region Login

        [HttpGet]
        public ActionResult SamlLogIn(SsoSamlResponseModel model)
        {

            GMGColorLogging.log.InfoFormat("In SamlLogIn method entered");
            GMGColorLogging.log.InfoFormat("In SamlLogIn method model email is {0}", model.Email);

            var auth = new Authentication();
            if (model != null)
            {
                auth.objUser = _commonService.GetUserByEmail(model.Email, LoggedAccount.ID);
            }
            GMGColorLogging.log.InfoFormat("In SamlLogIn method after GetUserByEmail auth.objUser email address is {0}", auth.objUser.EmailAddress);

            return this.ProcessLogin(auth);
        }

        [HttpGet]
        public ActionResult AuthLogin()
        {
            var auth = new Authentication();
            if (string.IsNullOrEmpty(ClaimsPrincipal.Current.FindFirst("email").Value))
            {
                return ProcessLogin(auth);
            }

            var email = ClaimsPrincipal.Current.FindFirst("email").Value;

            auth.objUser = _commonService.GetUserByEmail(email, LoggedAccount.ID);
            return ProcessLogin(auth);
        }

        [HttpGet]
        public ActionResult Login(int? relogin, string user, string returUrl)
        {
            try
            {
                //List<string> mandatoryCookies = new List<string> { "ASP.NET_SessionId", "__RequestVerificationToken" };
                //string[] myCookies = Request.Cookies.AllKeys.Where(x => !mandatoryCookies.Contains(x.ToString())).ToArray();
                //foreach (string cookie in myCookies)
                //{
                //    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                //}

                GMGColorLogging.log.InfoFormat("Entered Login method");

                if ((Session["gmg_uid"] != null) && (this.LoggedUser != null && this.LoggedUser.ID > 0))
                {
                    this.CreateUserSession(this.LoggedUser.ID, Context);
                    switch (this.LoggedUserPermissions)
                    {
                        case SitePermissions.AdministratorSiteOnly: return Default(true);
                        case SitePermissions.ApplicationSiteOnly: return Default(false);
                        case SitePermissions.BothSites:
                        default:
                            if (this.LoggedSite != Sites.NotLoggedIn)
                            {
                                return Default((this.LoggedSite == Sites.AdministratorSite));
                            }
                            else
                            {
                                return RedirectToLandingPage();
                            }
                    }
                }
                else
                {
                    Authentication model = new Authentication();
                    model.objAccount = this.LoggedAccount;
                    model.objUser = new GMGColorDAL.User();
                    model.Relogin = relogin;

                    var activeSsoConfig = _ssoCore.ActiveSsoConfiguration(LoggedAccount.ID);
                    if (activeSsoConfig != SsoOptionsEnum.None && activeSsoConfig == SsoOptionsEnum.Auth0)
                    {
                        var ssoAuthSettings = _ssoCore.Auth0Plugin.GetSettings(HttpContext.Request.Url.Host);
                        model.SsoAuth0 = new AccountSsoAuth0()
                        {
                            ClientId = ssoAuthSettings.ClientId,
                            Domain = ssoAuthSettings.Domain,
                            Type = Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Auth0)
                        };
                    }
                    else if (activeSsoConfig != SsoOptionsEnum.None && activeSsoConfig == SsoOptionsEnum.Saml)
                    {
                        GMGColorLogging.log.InfoFormat("In Login page entered saml");

                        var idpUrl = _ssoCore.SamlPlugin.GetIdpUrl(LoggedAccount.ID, LoggedAccount.Guid, LoggedAccount.Region);

                        GMGColorLogging.log.InfoFormat("In Login page after GetIdpUrl {0}", idpUrl);

                        if (string.IsNullOrEmpty(idpUrl))
                        {
                            throw new Exception();
                        }
                        model.SsoSaml = new AccountSsoSaml()
                        {
                            IdpUrl = idpUrl,
                            Type = Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Saml)
                        };
                    }

                    HttpCookie cookieUsername = Request.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_USERNAME];
                    HttpCookie cookiePassword = Request.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_PASSWORD];

                    if ((cookieUsername != null && cookiePassword != null) && (!String.IsNullOrEmpty(cookieUsername.Value) && !String.IsNullOrEmpty(cookiePassword.Value)))
                    {
                        // Just Load the Values in TextBoxes From Cookies
                        model.objUser.Username = GMGColorIO.Decrypt(cookieUsername.Value.ToString());
                        model.objUser.Password = GMGColorIO.Decrypt(cookiePassword.Value.ToString());
                        model.RememberMe = true;

                        return this.ProcessLogin(model);
                    }
                    else
                    {
                        model.DisplayLoginError = false;
                        model.DisplayLoginInactive = false;
                        model.RememberMe = false;
                        model.EmailNotFound = false;
                        if (!string.IsNullOrEmpty(user))
                            model.objUser.Username = user;

                        if (Session["EmailNotFound"] != null)
                        {
                            model.EmailNotFound = Convert.ToBoolean(Session["EmailNotFound"].ToString());
                        }
                        Session["EmailNotFound"] = null;
                        HttpCookie nameCookie = Request.Cookies["app_usid"];
                        //Set the Expiry date to past date.
                        if (nameCookie != null)
                        {
                            nameCookie.Expires = DateTime.Now.AddDays(-1);
                            //Update the Cookie in Browser.
                            Response.Cookies.Add(nameCookie);
                        }
                        if(Session["IsNewPasswordSent"] != null)
                        {
                            model.IsNewPasswordSent = (bool)Session["IsNewPasswordSent"];
                        }
                        model.EmailIdOfNewPasswordSent = (string)Session["ResetPassword_SentTo_EmailId"];
                        if (model.IsNewPasswordSent)
                        {
                            Session["IsNewPasswordSent"] = false;
                            Session["ResetPassword_SentTo_EmailId"] = null;
                        } 
                        return View("Login", model);
                    }
                }
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in AuthController, Login Method exception: {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpPost]
        public ContentResult AutenticateUser(Login model, string token)
        {
            int accountId = 0;
            User loggedUser = null;
            string errorMessage = "";

            #region Validate model 
            bool isError = (token ?? string.Empty).ToLower() != GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower();
            if (isError)
            {
                errorMessage = TrialAccountMessages.InvalidSecurityToken;
            }
            if (!isError && !ModelState.IsValid)
            {
                isError = true;
                errorMessage = ModelState.FirstOrDefault(o => o.Value.Errors.Any()).Value.Errors.FirstOrDefault().ErrorMessage;
            }

            #endregion

            #region Look for accoount
            if (!isError)
            {
                try
                {
                    accountId = (from a in Context.Accounts
                                 join acs in Context.AccountStatus on a.Status equals acs.ID
                                 where
                                     acs.Key == "A" && // only active accounts
                                     ((a.Domain == model.AccountUrl && a.IsCustomDomainActive == false) ||
                                      (a.CustomDomain == model.AccountUrl && a.IsCustomDomainActive == true))
                                 select a.ID).FirstOrDefault();

                    if (accountId == 0)
                    {
                        isError = true;
                        errorMessage = Resources.Resources.lblAccountNotFound;
                    }
                }
                catch (Exception ex)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(
                        "AuthController.Login - Could not get account id, message: " + ex.Message, ex);
                    isError = true;
                    errorMessage = Resources.Resources.lblAccountNotFound;
                }
            }
            #endregion

            #region Loog for user
            if (!isError)
            {
                try
                {
                    loggedUser = GMGColorDAL.User.Login(accountId, model.Username, model.Password, Context);
                    if (loggedUser == null)
                    {
                        isError = true;
                        errorMessage = Resources.Resources.lblIncorrectPassword;
                    }
                }
                catch (Exception ex)
                {
                    GMGColorDAL.GMGColorLogging.log.Error("AuthController.Login - Could not login based on provided username and password, message: " + ex.Message, ex);
                    isError = true;
                    errorMessage = Resources.Resources.lblIncorrectPassword;
                }
            }
            #endregion

            Response.StatusCode = !isError ? 200 : 400;
            return loggedUser != null && !isError ? Content(loggedUser.Guid) : Content(errorMessage);
        }

        [HttpPost]
        public ActionResult Login(Authentication model)
        {
            return model.objUser.IsSsoUser ? RedirectToAction("Login", "Sso") : this.ProcessLogin(model);
        }

        private ActionResult ProcessLogin(Authentication model)
        {
            try
            {
                // Check if remember me checkbox is checked on login            
                Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_USERNAME].Value = (model.RememberMe) ? GMGColorIO.Encrypt(model.objUser.Username) : string.Empty;
                Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_USERNAME].Expires = (model.RememberMe) ? DateTime.UtcNow.AddDays(14) : DateTime.UtcNow.AddDays(-15);

                Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_PASSWORD].Value = (model.RememberMe) ? GMGColorIO.Encrypt(model.objUser.Password) : string.Empty;
                Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_PASSWORD].Expires = (model.RememberMe) ? DateTime.UtcNow.AddDays(14) : DateTime.UtcNow.AddDays(-15);

                bool isSsoUser = model.objUser.IsSsoUser;

                // User login Auto temporarily lock and permanently lock
                int? UserByUserName = GMGColorDAL.User.GetUserByUserName(this.LoggedAccount.ID, model.objUser.Username, Context);
                var UserLoginAttempts = UserByUserName.Value > 0 ? GMGColorDAL.User.GetUserLoginAttempts(UserByUserName.Value, Context) : null;
                var UserSoftLockAttempt = UserLoginAttempts != null ? UserLoginAttempts.Attempts == 5 ? UserLoginAttempts : null : null;
                bool IsHardLocked = UserLoginAttempts != null ? UserLoginAttempts.Attempts == 10 : false;

                bool IsLoginFailedValid = isSsoUser ? true: UserByUserName.Value > 0 && !IsHardLocked && (UserSoftLockAttempt == null ? true : (UserSoftLockAttempt.DateLastLoginFailed < DateTime.UtcNow.AddMinutes(-15)));
                User objUser = null;
                bool IsPasswordResetRequired = false;

                if (!isSsoUser)
                {
                    DateTime? UserModifiedDate = GMGColorDAL.User.GetModifiedDateByUserName(this.LoggedAccount.ID, model.objUser.Username, Context);
                    if(Convert.ToDateTime(UserModifiedDate).Year != 1)
                    {
                        if((DateTime.Now - Convert.ToDateTime(UserModifiedDate)).TotalDays > 90){
                            IsPasswordResetRequired = true;
                        }
                    }
                }
                objUser = isSsoUser ? GMGColorDAL.User.GetUser(model.objUser.ID, Context) :
                      GMGColorDAL.User.Login(this.LoggedAccount.ID, model.objUser.Username, model.objUser.Password, Context);

                if (objUser != null && objUser.ID > 0 && IsLoginFailedValid && !IsPasswordResetRequired)
                {
                    GMGColorLogging.log.InfoFormat("In ProcessLogin objUser value is : {0} ", objUser.ID);
                    LoggedUser = objUser;
                    this.CreateUserSession(objUser.ID, Context);

                    LoggedUser.NeedReLogin = false;
                    //LoggedUser.DateLastLogin = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));

                    // Delete User Login failed History
                    if (UserLoginAttempts != null)
                    {
                        Context.UserLoginFailedDetails.Remove(UserLoginAttempts);
                        Context.SaveChanges();
                    }

                    // Update the user login information
                    GMGColorDAL.UserLogin objLogin = new GMGColorDAL.UserLogin()
                    {
                        User = LoggedUser.ID,
                        IpAddress = Request.ServerVariables["REMOTE_ADDR"],
                        Success = true,
                        DateLogin = Convert.ToDateTime(DateTime.UtcNow.ToString("g"))
                };

                    Context.UserLogins.Add(objLogin);
                    Context.SaveChanges();

                    Session["gmg_lid"] = LoggedUser.ID.ToString();

                    GMGColorLogging.log.InfoFormat("In ProcessLogin LoggedUserStatus is : {0} ", LoggedUserStatus);

                    if (LoggedUserStatus == GMGColorDAL.UserStatu.Status.Inactive)
                    {
                        model = new Authentication();
                        model.DisplayLoginError = false;
                        model.DisplayLoginInactive = true;
                        model.objUser = new GMGColorDAL.User();
                        model.objAccount = this.LoggedAccount;

                        return View("Login", model);
                    }
                    else
                    {
                        GMGColorLogging.log.InfoFormat("In ProcessLogin before CreateSession");

                        CreateSession();

                        GMGColorLogging.log.InfoFormat("In ProcessLogin After CreateSession");


                        // Check request have redirection to controller action   
                        RedirectResult redirectResult = new RedirectResult(Request.Url.Host);
                        if (Request.UrlReferrer != null && Request.UrlReferrer.Query.Contains("do="))
                        {
                            string destinationUrl = Request.UrlReferrer.Query.ToString();
                            destinationUrl = destinationUrl.Substring(destinationUrl.IndexOf("do=") + 3, destinationUrl.Length - 4).Split('&').First();
                            destinationUrl = Server.UrlDecode(destinationUrl);

                            Uri destUri = new Uri(Server.UrlDecode(destinationUrl));
                            destinationUrl = destUri.AbsolutePath.Remove(0, 1);

                            List<string> ControlActionParameters = destinationUrl.Split('/').ToList();
                            List<GMGColorDAL.UserMenuItemRoleView> authorizedControllerAction = (from o in AuthorizedControllerActions
                                                                                                 where o.Controller.ToLower() == ControlActionParameters[0].ToLower() && o.Action.ToLower() == ControlActionParameters[1].ToLower()
                                                                                                 select o).ToList();

                            GMGColorLogging.log.InfoFormat("In ProcessLogin before authorizedControllerAction");

                            if (authorizedControllerAction.Count > 0)
                            {
                                try
                                {
                                    if (destinationUrl.ToLower() == "studio/flex")
                                    {
                                        destinationUrl = "Approvals/Index";
                                    }

                                    if ((destUri.Host == Request.Url.Host) && (destUri.AbsoluteUri.ToLower().IndexOf("logout") < 0))
                                    {
                                        redirectResult = new RedirectResult(destinationUrl + destUri.Query);
                                    }
                                }
                                catch (HttpException hex)
                                {
                                    GMGColorLogging.log.ErrorFormat(hex, this.LoggedAccount.ID, "Login.btnLogin_Click() Error occured while redirect to page {0}. {1}", destUri.AbsoluteUri, (Environment.NewLine + " Exception :" + hex.Message));
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(model.LoginDoUrl))
                        {
                            string destinationUrl = model.LoginDoUrl;
                            Uri destUri = new Uri(Server.UrlDecode(destinationUrl));
                            destinationUrl = destUri.AbsolutePath.Remove(0, 1);

                            redirectResult = new RedirectResult(destinationUrl);
                        }

                        // Set the session time out                    
                        Session["STO"] = (Session.Timeout * 60);

                        if (redirectResult.Url != Request.Url.Host)
                        {
                            return redirectResult;
                        }
                        else
                        {
                            switch (this.LoggedUserPermissions)
                            {
                                case SitePermissions.AdministratorSiteOnly: return Default(true);
                                case SitePermissions.ApplicationSiteOnly:
                                case SitePermissions.BothSites:
                                default: return RedirectToLandingPage();
                            }
                        }
                    }
                }
                else if (IsPasswordResetRequired)
                {
                    GMGColorLogging.log.WarnFormat("In ProcessLogin login failed after 90days for user ID {0} and Name {1} in Account {2}", model.objUser.ID , model.objUser.Username, this.LoggedAccount.ID);
                    ResetNewPassword(model, this.LoggedAccount.ID, this.LoggedAccount.Owner, Context);

                    model = new Authentication();
                    model.DisplayLoginError = true;
                    model.DisplayLoginInactive = false;
                    model.objUser = new GMGColorDAL.User();
                    model.objAccount = this.LoggedAccount;
                    model.IsPasswordResetRequired = true;

                    return View("Login", model);
                }
                else
                {
                    if (IsLoginFailedValid)
                    {
                        GMGColorLogging.log.WarnFormat("In ProcessLogin login failed attempts for user ID {0} and Name {1} in Account {2}", model.objUser.ID, model.objUser.Username, this.LoggedAccount.ID);

                        // Update the user login faild Attempts information
                        if (UserLoginAttempts != null)
                        {
                            UserLoginAttempts.IpAddress = Request.ServerVariables["REMOTE_ADDR"];
                            UserLoginAttempts.Attempts += 1;
                            UserLoginAttempts.DateLastLoginFailed = DateTime.UtcNow;
                            UserLoginAttempts.IsUserLoginLocked = UserLoginAttempts.Attempts >= 10;
                        }
                        else if (UserByUserName.Value > 0)
                        {
                            GMGColorDAL.UserLoginFailedDetail objLoginFailed = new GMGColorDAL.UserLoginFailedDetail()
                            {
                                User = UserByUserName.Value,
                                IpAddress = Request.ServerVariables["REMOTE_ADDR"],
                                Attempts = 1,
                                DateLastLoginFailed = DateTime.UtcNow,
                                IsUserLoginLocked = false
                            };
                            Context.UserLoginFailedDetails.Add(objLoginFailed);
                        }

                        // UserLogin failed Information
                        GMGColorDAL.UserLogin objUserLoginFailed = new GMGColorDAL.UserLogin()
                        {
                            User = UserByUserName.Value,
                            IpAddress = Request.ServerVariables["REMOTE_ADDR"],
                            Success = false,
                            DateLogin = DateTime.UtcNow,
                            DateLogout = null
                        };
                        Context.UserLogins.Add(objUserLoginFailed);
                        GMGColorLogging.log.WarnFormat("In ProcessLogin login failed attempts saved UserLogins for user ID {0} and Name {1} in Account {2}", model.objUser.ID, model.objUser.Username, this.LoggedAccount.ID);
                        Context.SaveChanges();
                    }

                    model = new Authentication();
                    model.DisplayLoginError = true;
                    model.DisplayLoginInactive = false;
                    model.objUser = new GMGColorDAL.User();
                    model.objAccount = this.LoggedAccount;
                    model.IsTemporarilyLocked = UserLoginAttempts == null ? false : UserLoginAttempts.Attempts == 5 ?
                        (UserLoginAttempts.DateLastLoginFailed > DateTime.UtcNow.AddMinutes(-15)) : false;
                    model.IsPermanentlyLocked = UserLoginAttempts == null ? false : UserLoginAttempts.IsUserLoginLocked;
                    return View("Login", model);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in AuthController, ProcessLogin Method exception: {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpPost]
        public ActionResult ForgotPassword(Authentication model)
        {
            try
            {
                string newPassword = GMGColorDAL.User.GetNewRandomPassword();
                string hashNewPassword = GMGColorDAL.User.GetNewEncryptedPassword(newPassword, Context);
                bool success = false;
                var user =
                    (from u in Context.Users
                     join us in Context.UserStatus on u.Status equals us.ID
                        where u.EmailAddress == model.objUser.EmailAddress
                        && u.Account == this.LoggedAccount.ID
                        && us.Key == "A"
                        select u).
                        FirstOrDefault();

                if (user != null)
                {
                    // Deleting Login failed Attempts 
                    var UserLoginAttempts = GMGColorDAL.User.GetUserLoginAttempts(user.ID, Context);
                    if (UserLoginAttempts !=null)
                    {
                        Context.UserLoginFailedDetails.Remove(UserLoginAttempts);
                    }

                    model.objUser.ID = user.ID;

                    user.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    user.Password = hashNewPassword;
                    Context.SaveChanges();
                    success = true;
                    Session["IsNewPasswordSent"] = true;
                    Session["ResetPassword_SentTo_EmailId"] = user.EmailAddress;
                }
                else
                {
                    Session["EmailNotFound"] = true;
                }

                if (success)
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        GMGColorBusinessLogic.Common.InstanceEmailService emailser = new GMGColorBusinessLogic.Common.InstanceEmailService();

                        if (!emailser.SendForgotPasswordEmail(Context, (int)LoggedAccount.Owner, model.objUser.ID, newPassword))
                        {
                            NotificationServiceBL.CreateNotification(new GMGColorNotificationService.LostPassword
                            {
                                EventCreator = LoggedAccount.Owner,
                                InternalRecipient = model.objUser.ID,
                                CreatedDate = DateTime.UtcNow,
                                NewPassword = newPassword
                            },
                                null,
                                Context
                                );
                        }
                    }
                    else
                    {
                        NotificationServiceBL.CreateNotification(new GMGColorNotificationService.LostPassword
                        {
                            EventCreator = LoggedAccount.Owner,
                            InternalRecipient = model.objUser.ID,
                            CreatedDate = DateTime.UtcNow,
                            NewPassword = newPassword
                        },
                                    null,
                                    Context
                                    );
                    }
                }
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                                                "LoginControl.ForgetPassword : Sending reset password information failed. {0} {1}",
                                                model.objUser.EmailAddress,
                                                ex.Message);
            }

            return RedirectToAction("Login");
        }

        [HttpGet]
        public JsonResult IsSsoUser(string email)
        {
            var infoMsg = _ssoCore.IsSsoUser(LoggedAccount.ID, email);
            return Json(new
            {
                Status = 200,
                Content = infoMsg
            }, JsonRequestBehavior.AllowGet);
        }

        public static void ResetNewPassword(Authentication model, int loggedAccountId, int? loggedOwnerId, DbContextBL Context)
        {
            try
            {
                string newPassword = GMGColorDAL.User.GetNewRandomPassword();
                string hashNewPassword = GMGColorDAL.User.GetNewEncryptedPassword(newPassword, Context);
                bool success = false;
                var user =
                    (from u in Context.Users
                     join us in Context.UserStatus on u.Status equals us.ID
                     where u.Username == model.objUser.Username
                     && u.Account == loggedAccountId
                     && us.Key == "A"
                     select u).
                        FirstOrDefault();

                if (user != null)
                {
                    // Deleting Login failed Attempts 
                    var UserLoginAttempts = GMGColorDAL.User.GetUserLoginAttempts(user.ID, Context);
                    if (UserLoginAttempts != null)
                    {
                        Context.UserLoginFailedDetails.Remove(UserLoginAttempts);
                    }

                    model.objUser.ID = user.ID;

                    user.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    user.Password = hashNewPassword;
                    Context.SaveChanges();
                    success = true;
                    GMGColorLogging.log.WarnFormat("In ResetNewPassword login failed after 90days for user ID {0} and Name {1} in Account {2}", model.objUser.ID, model.objUser.Username, loggedAccountId);
                }

                if (success)
                {
                    NotificationServiceBL.CreateNotification(new GMGColorNotificationService.LostPassword
                    {
                        EventCreator = loggedOwnerId,
                        InternalRecipient = model.objUser.ID,
                        CreatedDate = DateTime.UtcNow,
                        NewPassword = newPassword
                    },
                                                            null,
                                                            Context
                                                            );

                    GMGColorLogging.log.WarnFormat("In ResetNewPassword after mail sent for user ID {0} and Name {1} in Account {2}", model.objUser.ID, model.objUser.Username, loggedAccountId);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, loggedAccountId,
                                                "LoginControl.ResetnewPassword : Sending reset password information failed. {0} {1}",
                                                model.objUser.Username,
                                                ex.Message);
            }

            
        }

        private void CreateUserSession(int userId, DbContextBL context)
        {
            if (Request.Cookies["app_usid"] == null)
            {
                var newSession = new CollaborateAPISession
                {
                    User = userId,
                    TimeStamp = DateTime.UtcNow,
                    SessionKey = Guid.NewGuid().ToString()
                };
                context.CollaborateAPISessions.Add(newSession);
                context.SaveChanges();

                HttpCookie nameCookie = new HttpCookie("app_usid");
                //Set the Cookie value.
                nameCookie.Value = newSession.SessionKey;
                //Set the Expiry date.
                nameCookie.Expires = DateTime.Now.AddDays(1);
                //Add the Cookie to Browser.
                Response.Cookies.Add(nameCookie);
                //Set session value
                Session["app_usid"] = newSession.SessionKey;
            }
        }

        #endregion

        #region Default

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ProofApp")]
        [OutputCache(Duration = 3600, VaryByParam = "none", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult ProofApp()
        {
            return this.Default(false);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AdminApp")]
        [OutputCache(Duration = 3600, VaryByParam = "none", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult AdminApp()
        {
            return this.Default(true);
        }

        private ActionResult Default(bool IsAdministraorSite)
        {
            if (this.LoggedUser != null)
            {
                try
                {
                    Session["gmgc_app"] = IsAdministraorSite;

                    if (IsAdministraorSite)
                    {
                        if (GMGColorConfiguration.AppConfiguration.IsHideDashbordMenuItem)
                        {
                            return RedirectToAction("Index", "Account");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                    }
                    else
                    {
                        if (GMGColorConfiguration.AppConfiguration.IsHideDashbordMenuItem)
                        {
                            if (LoggedAccount.DisableLandingPage)
                            {
                                if (AuthorizedControllerActions.Any(o => o.Key == "APIN") &&
                                    this.LoggedAccount.HasCollaborateSubscriptionPlan(Context))
                                {
                                    return RedirectToAction("Index", "Approvals");
                                }
                                if (AuthorizedControllerActions.Any(o => o.Key == "DLIN") &&
                                    this.LoggedAccount.HasDeliverSubscriptionPlan(Context))
                                {
                                    return RedirectToAction("Index", "Deliver");
                                }                                
                            }
                            else if (AuthorizedControllerActions.Any(o => o.Key == "HOME"))
                            {
                                return RedirectToAction("Home", "Home");
                            }
                            return RedirectToAction("Index", "PersonalSettings");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Studio");
                        }

                    }
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "LoginControl.Login : Update user log information faild. {0} {1} (ID: {2} ex : {3})",
                                                    LoggedUser.GivenName,
                                                    LoggedUser.FamilyName,
                                                    LoggedUser.ID,
                                                    ex.Message);
                }
                return RedirectToAction("Cannotcontinued", "Error");
            }
            else
            {
                return RedirectToAction("Cannotcontinued", "Error");
            }
        }

        #endregion

        #region Logout

        public ActionResult Logout(int? relogin, string user)
        {
            try
            {
                string cookieValue = string.Empty;
                if (Response.Cookies[SoftProofingCookieName] != null)
                {
                    Response.Cookies[SoftProofingCookieName].Expires = DateTime.Now.AddSeconds(-1);
                    cookieValue = Response.Cookies[SoftProofingCookieName].Value;
                }
                if (Request.Cookies[SoftProofingCookieName] != null)
                {
                    Request.Cookies[SoftProofingCookieName].Expires = DateTime.Now.AddSeconds(-1);
                    cookieValue = Request.Cookies[SoftProofingCookieName].Value;
                }
                SoftProofingBL.SetWorkstationConnected(cookieValue, Context, true);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }

            this.LoggedUserLogOut(true, Context);

            return relogin.HasValue
                       ? RedirectToAction("Login", "Auth", new {relogin = relogin, user = user})
                       : RedirectToAction("Login", "Auth");
        }

       
        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ClearAPISession()
        {
            try
            {
                bool res = false;
                HttpCookie nameCookie = Request.Cookies["app_usid"];
                var apiUser = Context.CollaborateAPISessions.Where(c => c.SessionKey == nameCookie.Value).FirstOrDefault();
                if (apiUser != null)
                {
                    res = true;
                    Context.CollaborateAPISessions.Remove(apiUser);
                    Context.SaveChanges();
                }
                return Json(
                        new
                        {
                            Status = 400,
                            Content = Json(res, JsonRequestBehavior.AllowGet)
                        },
                        JsonRequestBehavior.AllowGet);
                
                
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
                    "AuthController. ClearAPISession Ajax Request failed. {0}", ex.Message);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = "Error in AuthController ClearAPISession()"
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        private void CreateAnnotationSession(int? annotationID)
        {
            Session["AnnotationAtUserInComment"] = annotationID;
        }

        #region Redirect

        [HttpGet]
        public ActionResult Redirect(int adhoc, string exgousg, string shp, string apg, int? aid)
        {
            try
            {
                bool isExternal = Convert.ToBoolean(adhoc);
                CreateAnnotationSession(aid);

                if (!String.IsNullOrEmpty(exgousg) && !String.IsNullOrEmpty(shp) && !String.IsNullOrEmpty(apg))
                {
                    int ApprovalID = 0, ExOrUserID = 0, RoleID = 0;
                    GMGColorDAL.ExternalCollaborator oExCollaborator = null; GMGColorDAL.User oCollaborator = null;

                    if (isExternal)
                    {
                        GMGColorDAL.SharedApproval oSharedApproval =
                            DALUtils.GetObject<GMGColorDAL.SharedApproval>(shp.ToInt().GetValueOrDefault(), Context);

                        oExCollaborator = DALUtils.SearchObjects<GMGColorDAL.ExternalCollaborator>(o => o.Guid == exgousg && o.IsDeleted == false, Context).SingleOrDefault();
                        bool isValied = (oSharedApproval != null && oSharedApproval.ID > 0) && !oSharedApproval.IsExpired && (oSharedApproval.Approval1.Guid == apg) && (oSharedApproval.Approval1.Job1.Account == this.LoggedAccount.ID) && (oExCollaborator != null && oExCollaborator.ID > 0) && (oExCollaborator != null && oExCollaborator.ID > 0
                    && (oSharedApproval.IsBlockedURL == true && oSharedApproval.ExpireDate < DateTime.UtcNow ? false : true));
                        if (isValied)
                        {
                            ApprovalID = oSharedApproval.Approval;
                            ExOrUserID = oExCollaborator.ID;
                            RoleID = oSharedApproval.ApprovalCollaboratorRole;
                            isExternal = true;
                        }
                        else
                        {
                            return RedirectToAction("AccessDenied", "Error");
                        }
                    }
                    else
                    {
                        GMGColorDAL.Approval oApproval = DALUtils.SearchObjects<GMGColorDAL.Approval>(o => o.Guid == apg, Context).SingleOrDefault();

                        int statusId = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Active, Context).ID;
                        oCollaborator = DALUtils.SearchObjects<GMGColorDAL.User>(o => o.Guid == exgousg && o.Status == statusId, Context).SingleOrDefault();

                        bool isValied = (oApproval != null && oApproval.ID > 0) && (oApproval.Job1.Account == this.LoggedAccount.ID) && (oCollaborator != null && oCollaborator.ID > 0);
                        if (isValied)
                        {
                            ApprovalID = oApproval.ID;
                            ExOrUserID = oCollaborator.ID;
                            RoleID = oCollaborator.CollaborateRoleId(Context).GetValueOrDefault();
                            isExternal = false;
                        }
                        else
                        {
                            return RedirectToAction("AccessDenied", "Error");
                        }
                    }

                    if (!isExternal && LoggedUser != null && LoggedUser.ID > 0 && LoggedUser.ID != oCollaborator.ID)
                    {
                        this.LoggedUserLogOut(false, Context);
                    }

                    if (isExternal || this.LoggedUser == null || LoggedUser.ID == 0)
                    {
                        Session[(isExternal ? "gmg_uex" : "gmg_uid")] = ExOrUserID;
                        Session["gmg_sid"] = Session.SessionID.ToString();
                        Session[Constants.CollaborateRoleIdInSession] = RoleID;
                        Session["gmg_aid"] = this.LoggedAccount.ID.ToString();
                    }

                    string sessionKey = (new Random()).Next(1000, 9999).ToString();
                    Session["SessionKey"] = sessionKey;
                    Session["StudioApproval_" + sessionKey] = new List<int>() { ApprovalID };
                    Session["StudioLoggedUser_" + sessionKey] = ExOrUserID;
                    Session["StudioReturnURL_" + sessionKey] = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + this.Domain + "/Auth/Login";
                    Session["StudioIsExternal_" + sessionKey] = isExternal;
                    Session[Constants.ApprovalsResetParameters] = true;

                    if (isExternal)
                    {
                        GMGColorLogging.log.InfoFormat(this.LoggedAccount.ID, "LoginControl.External : Login success for external user {0} {1} (ID: {2}) into host {3} with session id {4} from {5}",
                                                        oExCollaborator.GivenName,
                                                        oExCollaborator.FamilyName,
                                                        oExCollaborator.ID,
                                                        Request.Url.Host,
                                                        Session.SessionID,
                                                        Request.UserHostAddress);
                    }
                    else
                    {
                        GMGColorLogging.log.InfoFormat(this.LoggedAccount.ID, "LoginControl.External : Login success for user {0} {1} (ID: {2}) into host {3} with session id {4} from {5}",
                                                        oCollaborator.GivenName,
                                                        oCollaborator.FamilyName,
                                                        oCollaborator.ID,
                                                        Request.Url.Host,
                                                        Session.SessionID,
                                                        Request.UserHostAddress);
                    }

                    return RedirectToAction("RedirectHtml5", "Studio", new {isExternal});
                }
                else
                {
                    Session.Abandon();
                    // Logged users should log out before...
                    return RedirectToAction("InvalidRequest", "Error");
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                return RedirectToAction("AccessDenied", "Error");
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount != null ? LoggedAccount.ID : -1, "AccountController.Redirect : Welcoming New User Failed. {0}. ", ex.Message);
                return RedirectToAction("AccessDenied", "Error");
            }
        }

        #endregion

        #region GetFile

        [HttpGet]
        public ActionResult GetFile(string guid)
        {
            string model = string.Empty;

            if (string.IsNullOrEmpty(guid))
            {
                model = "File does not exist";
            }
            else
            {
                GMGColorDAL.Approval objApproval = GMGColorDAL.DALUtils.SearchObjects<GMGColorDAL.Approval>(o => o.Guid == guid, Context).SingleOrDefault();

                if (objApproval != null)
                {
                    string relativePath = "approvals/" + objApproval.Guid + "/" + objApproval.FileName;
                    string filePath = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +objApproval.Job1.Account1.Domain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(this.LoggedAccount.Region) + "/" + relativePath;

                    if (System.IO.File.Exists(GMGColorConfiguration.AppConfiguration.PathToDataFolder(this.LoggedAccount.Region) + relativePath))
                    {
                        this.DownloadFile(filePath);
                        model = "Thank you for downloading the approval.";
                    }
                    else
                    {
                        model = "<h3>404 - Not Found</h3><p>The file you are requesting does not exist. Please check the link you followed and try again.</p>";
                    }
                }
            }

            return View("GetFile", model);
        }

        #endregion

        #region Welcome

        [HttpGet]
        public ActionResult Welcome(string guid, int id)
        {
            GMGColorDAL.User userBo = DALUtils.SearchObjects<GMGColorDAL.User>(u => u.ID == id && u.Guid == guid, Context).FirstOrDefault();
            bool IsLinkExpired = userBo.CreatedDate < DateTime.UtcNow.AddMonths(-1);

            if (userBo != null && userBo.UserStatu.Key == "N" && !IsLinkExpired) // -- Invited user
            {
                UserInfo userModel = new UserInfo(userBo.ID, this.LoggedAccount.ID, Context);
                userModel.objUser.FirstName = (userBo.GivenName == "Invited") ? string.Empty : userBo.GivenName;
                userModel.objUser.LastName = (userBo.FamilyName == "User") ? string.Empty : userBo.FamilyName;

                //Temporary fix to allow to activate user no matter what subscription plan are attached to account
                userModel.LoggedAccountHasNoSubscription = false; //LoggedAccount != null &&
                                                                //GMGColorDAL.AccountType.GetAccountType(LoggedAccount.AccountType, Context) != GMGColorDAL.AccountType.Type.GMGColor &&
                                                                //!LoggedAccount.NeedSubscriptionPlan;
                TempData["gmg_aid"] = this.LoggedAccount.ID;
                ViewBag.AccountTheme = LoggedAccountTheme;
                if (TempData["modelErrors"] != null)
                {
                    Dictionary<string, string> modelErrors = TempData["modelErrors"] as Dictionary<string, string>;
                    foreach (KeyValuePair<string, string> error in modelErrors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    }

                    if (TempData["takenUsername"] != null)
                    {
                        userModel.objUser.Username = TempData["takenUsername"] as string;
                    }
                }

                return View("Welcome", (userModel));
            }
            else
            {
                return RedirectToAction("NotConfigured", "Error");
            }
        }

        [HttpPost]
        public ActionResult Welcome(UserInfo model)
        {
            try
            {
                GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(model.objUser.ID, Context);

                #region Validations

                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                if (!ValidateUserName(model.objUser.Username, null))
                {
                    dicModelErrors.Add("objUser.Username", Resources.Resources.lblUsernameTaken);
                    TempData["modelErrors"] = dicModelErrors;
                    TempData["takenUsername"] = model.objUser.Username;

                    return RedirectToAction("Welcome", new { guid = model.objUser.Guid, id = model.objUser.ID });
                }
                #endregion

                using (TransactionScope scope = new TransactionScope())
                {
                    objUser.Username = model.objUser.Username;
                    objUser.GivenName = model.objUser.FirstName;
                    objUser.FamilyName = model.objUser.LastName;
                    objUser.Status = 1;
                    objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(model.objChangePassword.NewPassword, Context);
                    objUser.Modifier = model.objUser.ID;
                    objUser.ModifiedDate = DateTime.UtcNow;

                    if (!LoggedAccount.IsAgreedToTermsAndConditions && model.IsAggreeToTermsAndConditions)
                    {
                        // this flag should be changed only from false (Default value) to true when the account owner activate the account
                        LoggedAccount.IsAgreedToTermsAndConditions = model.IsAggreeToTermsAndConditions;
                    }

                    if (LoggedAccount.Owner == objUser.ID)
                    {
                        LoggedAccount.Status = GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Active, Context).ID;
                        if (LoggedAccountType != GMGColorDAL.AccountType.Type.Subscriber)
                        {
                            AccountSubscriptionPlan collaborateSubscriptionPlan = Account.GetNonSubscriberPlan(LoggedAccount.ID, AppModule.Collaborate, Context);
                            AccountSubscriptionPlan deliverSubscriptionPlan = Account.GetNonSubscriberPlan(LoggedAccount.ID, AppModule.Deliver, Context);

                            if (collaborateSubscriptionPlan != null)
                            {
                                if (collaborateSubscriptionPlan.ContractStartDate == null)
                                {
                                    collaborateSubscriptionPlan.ContractStartDate = DateTime.UtcNow;
                                }

                                if (collaborateSubscriptionPlan.AccountPlanActivationDate == null)
                                {
                                    collaborateSubscriptionPlan.AccountPlanActivationDate = DateTime.UtcNow;
                                }
                            }

                            if (deliverSubscriptionPlan != null)
                            {
                                if (deliverSubscriptionPlan.ContractStartDate == null)
                                {
                                    deliverSubscriptionPlan.ContractStartDate = DateTime.UtcNow;
                                }

                                if (deliverSubscriptionPlan.AccountPlanActivationDate == null)
                                {
                                    deliverSubscriptionPlan.AccountPlanActivationDate = DateTime.UtcNow;
                                }
                            }                           
                        }
                        else
                        {
                            SubscriberPlanInfo collaborateSubscriberPlan = Account.GetSubscriberPlan(LoggedAccount.ID, AppModule.Collaborate, Context);
                            SubscriberPlanInfo deliverSubscriberPlan = Account.GetSubscriberPlan(LoggedAccount.ID, AppModule.Deliver, Context);                           

                            if (collaborateSubscriberPlan != null)
                            {
                                if (collaborateSubscriberPlan.ContractStartDate == null)
                                {
                                    collaborateSubscriberPlan.ContractStartDate = DateTime.UtcNow;
                                }

                                if (collaborateSubscriberPlan.AccountPlanActivationDate == null)
                                {
                                    collaborateSubscriberPlan.AccountPlanActivationDate = DateTime.UtcNow;
                                }
                            }

                            if (deliverSubscriberPlan != null)
                            {
                                if (deliverSubscriberPlan.ContractStartDate == null)
                                {
                                    deliverSubscriberPlan.ContractStartDate = DateTime.UtcNow;
                                }

                                if (deliverSubscriberPlan.AccountPlanActivationDate == null)
                                {
                                    deliverSubscriberPlan.AccountPlanActivationDate = DateTime.UtcNow;
                                }
                            }
                        }
                    }

                    objUser.DateLastLogin = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));

                    //CZ-263 Avoid log out user
                    objUser.NeedReLogin = false;

                    Context.SaveChanges();
                    scope.Complete();

                    Session["gmg_uid"] = objUser.ID.ToString();
                    Session["gmg_aid"] = LoggedAccount.ID.ToString();
                    if (LoggedAccount.Owner == objUser.ID)
                    {
                        Session["gmg_swscr_" + LoggedAccount.ID] = true;
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AccountController.Welcome : Welcoming New User Failed. {0}. ", ex.Message);
            }
            finally
            {
            }

            return RedirectToAction("Login", "Auth");
        }

        #endregion

        #region Methods

        protected void DownloadFile(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);

            Response.ClearContent();
            Response.ContentType = MimeType(fileInfo.Name);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", fileInfo.Name));
            Response.AddHeader("Content-Length", (fileInfo.Length).ToString("F0"));
            Response.TransmitFile(filePath);
            Response.End();
        }

        private string MimeType(string fileName)
        {
            string ext = Path.GetExtension(fileName).ToLower();
            string mime = "application/octetstream";

            if (string.IsNullOrEmpty(ext))
            {
                return mime;
            }
            else
            {
                Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

                if (regKey != null && regKey.GetValue("Content Type") != null)
                {
                    mime = regKey.GetValue("Content Type").ToString();
                }
                return mime;
            }
        }

        private void LoggedUserLogOut(bool sessionAbandon, DbContextBL context)
        {
            //try
            //{
            //    if (System.Web.HttpContext.Current.Session["gmg_lid"] != null)
            //    {
            //        int userId = Convert.ToString(System.Web.HttpContext.Current.Session["gmg_lid"]).ToInt().GetValueOrDefault();
            //        if (userId > 0)
            //        {
            //            GMGColorDAL.UserLogin objLogin = DALUtils.GetObject<GMGColorDAL.UserLogin>(userId, context);
            //            if (objLogin != null)
            //            {
            //                objLogin.DateLogout = DateTime.UtcNow;
            //                context.SaveChanges();
            //            }
            //        }
            //    }
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    DALUtils.LogDbEntityValidationException(ex);
            //}
            //catch (Exception ex)
            //{
            //    GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AuthController.Logout : Update user log information faild. {0} {1} (ID: {2} ex : {3})",
            //                                    this.LoggedUser.GivenName,
            //                                    this.LoggedUser.FamilyName,
            //                                    this.LoggedUser.ID,
            //                                    ex.Message);
            //}

            Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_USERNAME].Value = string.Empty;
            Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_USERNAME].Expires = DateTime.UtcNow.AddDays(-15);
            Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_PASSWORD].Value = string.Empty;
            Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + KEY_PASSWORD].Expires = DateTime.UtcNow.AddDays(-15);
            Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + "-xsite"].Value = string.Empty;
            Response.Cookies[this.LoggedAccount.Guid + this.LoggedAccount.ID.ToString() + "-xsite"].Expires = DateTime.UtcNow.AddDays(-3);

            System.Web.HttpContext.Current.Session["gmg_uid"] = null;
            System.Web.HttpContext.Current.Session["gmg_sid"] = null;
            System.Web.HttpContext.Current.Session[Constants.CollaborateRoleIdInSession] = null;
            System.Web.HttpContext.Current.Session["gmg_lid"] = null;
            System.Web.HttpContext.Current.Session["gmg_aid"] = null;
            System.Web.HttpContext.Current.Session[VersionSessionKey] = null;
            System.Web.HttpContext.Current.Session["app_usid"] = null;


            this.LoggedUserInfo = null;

            if (sessionAbandon)
            {
                System.Web.HttpContext.Current.Session.Abandon();

                SessionIDManager manager = new SessionIDManager();
                var isRedirected = false; var isAdded = false;
                manager.SaveSessionID(System.Web.HttpContext.Current, manager.CreateSessionID(System.Web.HttpContext.Current), out isRedirected, out isAdded);
            }

            HttpCookie nameCookie = Request.Cookies["app_usid"];
            if (nameCookie != null)
            {
                var apiUser = context.CollaborateAPISessions.Where(c => c.SessionKey == nameCookie.Value).FirstOrDefault();
                if (apiUser != null)
                {
                    context.CollaborateAPISessions.Remove(apiUser);
                    context.SaveChanges();
                }
                //Set the Expiry date to past date.
                nameCookie.Expires = DateTime.Now.AddDays(-1);
                //Update the Cookie in Browser.
                Response.Cookies.Add(nameCookie);
            }

        }

        #endregion
    }

    #region Public

    #region Errors

    public class ErrorController : GenericController
    {
        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult Index()
        {
            this.SetCulture();
            return View("Error");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult AccessDenied()
        {
            this.SetCulture();
            return View("AccessDenied");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult FileTransferExpired()
        {
            this.SetCulture();
            return View("FileTransferExpired");
        }
        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult FileTransferDeleted()
        {
            this.SetCulture();
            return View("FileTransferDeleted");
        }

        

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult UnexpectedError()
        {
            this.SetCulture();
            return View("UnexpectedError");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult Cannotcontinued()
        {
            this.SetCulture();
            return View("Cannotcontinued");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult RequestValidation()
        {
            this.SetCulture();
            return View("RequestValidation");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult XSSRequestValidation()
        {
            this.SetCulture();
            return View("XSSRequestValidation");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult SuspendedAccount(string reason)
        {
            this.SetCulture();
            var accId = TempData["AccId"];
            ViewBag.ContactEmailAdress = accId != null ? AccountSettingsBL.GetContactSupportEmail(TempData["AccId"].ToString().ToInt()) : AccountSettingsBL.GetHelpDeskEmailAddress();
            return View("SuspendedAccount", string.Empty, reason);
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult Custom(string title, string reason)
        {
            this.SetCulture();
            return View("Custom", new CustomError() {Title = title, Message = reason});
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult InvalidRequest()
        {
            this.SetCulture();
            return View("InvalidRequest");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult Unauthorised()
        {
            this.SetCulture();
            return View("Unauthorised");
        }

        [HttpGet]
        [OutputCache(Duration = int.MaxValue)]
        public ActionResult NotConfigured()
        {
            Session.Abandon();
            return View("NotConfigured");
        }

        private void SetCulture()
        {
            if (Session != null && Session["gmg_aid"] != null && int.Parse(Session["gmg_aid"].ToString()) > 0)
            {
                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(Session["gmg_aid"].ToString().ToInt().GetValueOrDefault(), Context);

                // Modify Current thread's cultureif
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(objAccount.Locale1.Name);
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(objAccount.Locale1.Name);
            }
        }
    }

    #endregion

    #region Download

    public class DownloadController : GenericController
    {

        private readonly IFileTransferService _fileTransferService;

      

        public DownloadController(IFileTransferService fileTransferService) 
        {
            _fileTransferService = fileTransferService;
        }
        [HttpGet]
        public ActionResult DownloadApproval(int adhoc, string exgousg, string apg)
        {
            try
            {
                bool isExternal = Convert.ToBoolean(adhoc);

                if (!String.IsNullOrEmpty(exgousg) && !String.IsNullOrEmpty(apg))
                {
                    if (isExternal)
                    {
                        GMGColorDAL.ExternalCollaborator objExUser = DALUtils.SearchObjects<GMGColorDAL.ExternalCollaborator>(o => o.Guid == exgousg, Context).SingleOrDefault();

                        if (objExUser != null && objExUser.ID > 0)
                        {
                            var approvalResult = objExUser.SharedApprovals.Where(o => o.Approval1.Guid == apg && !o.IsExpired && !objExUser.IsDeleted).SingleOrDefault();
                            if (approvalResult == null || approvalResult.ID == 0)
                            {
                                throw (new Exception());
                            }
                        }
                        else
                        {
                            throw (new Exception());
                        }
                    }

                    GMGColorDAL.Approval objApproval = DALUtils.SearchObjects<GMGColorDAL.Approval>(o => o.Guid == apg, Context).SingleOrDefault();

                    if (objApproval != null)
                    {
                        string filePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(objApproval.Job1.Account1.Region) + "approvals/" + objApproval.Guid + "/" + objApproval.FileName;
                        string fileName = Path.GetFileName(filePath);
                        string contentType = GMGColorCommon.GetContentType(filePath);

                        if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                        {
                            filePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(objApproval.Job1.Account1.Region) + "approvals/" + objApproval.Guid + "/" + HttpUtility.UrlEncode(objApproval.FileName);

                            System.Net.WebRequest req = System.Net.WebRequest.Create(new Uri(filePath));
                            System.Net.WebResponse response = req.GetResponse();
                            Stream stream = response.GetResponseStream();

                            return File(stream, contentType, fileName);
                        }
                        else
                        {
                            return File(filePath, contentType, fileName);
                        }
                    }

                    return null;
                }
                else
                {
                    Session.Abandon();
                    return RedirectToAction("Unauthorised", "Error");
                }
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in AuthController, DownloadApproval Method exception: {0}", ex.Message);

                Session.Abandon();
                return RedirectToAction("AccessDenied", "Error");
            }
        }



        [HttpGet]
        public ActionResult DownloadFileTransfer(int adhoc, string exgousg, string apg)
        {
            try
            {
                bool isExternal = Convert.ToBoolean(adhoc);

                if (!String.IsNullOrEmpty(exgousg) && !String.IsNullOrEmpty(apg))
                {
                    if (isExternal)
                    {
                        GMGColorDAL.ExternalCollaborator objExUser = DALUtils.SearchObjects<GMGColorDAL.ExternalCollaborator>(o => o.Guid == exgousg, Context).SingleOrDefault();

                        if (objExUser != null && objExUser.ID > 0)
                        {
                            var fileTransferResult = objExUser.FileTransferUserExternals.Where(o => o.FileTransfer.Guid == apg && !objExUser.IsDeleted).SingleOrDefault();
                            if (fileTransferResult == null || fileTransferResult.ID == 0)
                            {
                                throw (new Exception());
                            }

                            //set the download date
                            var fileTransferExternalID = fileTransferResult.ID;
                            _fileTransferService.SetDownloadDateForExternal(fileTransferExternalID);

                            //send the notification to the owner
                            GMGColorDAL.FileTransfer objFileTransfer = DALUtils.SearchObjects<GMGColorDAL.FileTransfer>(o => o.ID == fileTransferResult.FileTransferID, Context).SingleOrDefault();
                            _fileTransferService.SendDownloadNotification(objFileTransfer.ID, objFileTransfer.Creator, objExUser.ID, Context,isExternal);


                        }
                        else
                        {
                            throw (new Exception());
                        }
                    }

                  //  GMGColorDAL.Approval objApproval = DALUtils.SearchObjects<GMGColorDAL.Approval>(o => o.Guid == apg, Context).SingleOrDefault();
                    GMGColorDAL.FileTransfer fileTransfer = DALUtils.SearchObjects<GMGColorDAL.FileTransfer>(o => o.Guid == apg, Context).SingleOrDefault();
                    if (!isExternal)
                    {
                        GMGColorDAL.User objUser = DALUtils.SearchObjects<GMGColorDAL.User>(o => o.Guid == exgousg, Context).SingleOrDefault();
                        var fileTransferInternalResult = fileTransfer.FileTransferUserInternals.Where(o => o.FileTransfer.Guid == apg && objUser.ID == o.UserInternalID).SingleOrDefault();
                        var fileTransferInternalID = fileTransferInternalResult.ID;

                        //set download date
                        _fileTransferService.SetDownloadDateForInternal(fileTransferInternalID);

                        //send the notification to the owner
                        GMGColorDAL.FileTransfer objFileTransfer = DALUtils.SearchObjects<GMGColorDAL.FileTransfer>(o => o.ID == fileTransferInternalResult.FileTransferID, Context).SingleOrDefault();
                        _fileTransferService.SendDownloadNotification(objFileTransfer.ID, objFileTransfer.Creator, objUser.ID, Context, isExternal);
                    }

                   
                    if (fileTransfer.ExpirationDate < DateTime.UtcNow)
                    {
                        Session.Abandon();
                        return RedirectToAction("FileTransferExpired", "Error");
                    }

                    if (fileTransfer.IsDeleted == true)
                    {
                        Session.Abandon();
                        return RedirectToAction("FileTransferDeleted", "Error");
                    }

                    if (fileTransfer != null)
                    {
                        string filePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(fileTransfer.Account.Region) + "filetransfer/" + fileTransfer.Guid + "/" + fileTransfer.FileName;
                        string fileName = Path.GetFileName(filePath);
                        string contentType = GMGColorCommon.GetContentType(filePath);

                        if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                        {
                            filePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(fileTransfer.Account.Region) + "filetransfer/" + fileTransfer.Guid + "/" + HttpUtility.UrlEncode(fileTransfer.FileName);

                            System.Net.WebRequest req = System.Net.WebRequest.Create(new Uri(filePath));
                            System.Net.WebResponse response = req.GetResponse();
                            Stream stream = response.GetResponseStream();

                            return File(stream, contentType, fileName);
                        }
                        else
                        {
                            return File(filePath, contentType, fileName);
                        }
                    }

                    return null;
                }
                else
                {
                    Session.Abandon();
                    return RedirectToAction("Unauthorised", "Error");
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in AuthController, DownloadApproval Method exception: {0}", ex.Message);

                Session.Abandon();
                return RedirectToAction("AccessDenied", "Error");
            }
        }
    }

    #endregion

    #endregion
}