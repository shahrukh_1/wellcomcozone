﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using System.Web.Script.Serialization;
using System.Net;
using AutoMapper;
using GMGColor.Hubs;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.Api;
using Microsoft.AspNet.SignalR;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Collaborate.Services.Soad_State;
using GMG.CoZone.Common.Interfaces;

namespace GMGColor.Controllers
{
    public class ApprovalWorkflowController : BaseController
    {
        #region Fileds

        private const string SearchApprovalWorkflows = "SearchApprovalWorkflows";

        private readonly ISoadStateService _soadStateService;

        public ApprovalWorkflowController(ISoadStateService soadStateService, IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {
            _soadStateService = soadStateService;
        }
        #endregion

        #region GET Actions
        //
        // GET: /ApprovalWorkflow/
        [HttpGet]
        public ActionResult ApprovalWorkflows()
        {
            var model = new ApprovalWorkflowDashboardModel {ApprovalWorkflow = new ApprovalWorkflowModel()};

            if (TempData[SearchApprovalWorkflows] is string)
            {
                model.SearchText = TempData[SearchApprovalWorkflows] as string;
                model.ApprovalWorkflows = ApprovalWorkflowBL.GetApprovalWorkflows(LoggedAccount.ID, Context, model.SearchText);
            }
            else
            {
                model.ApprovalWorkflows = ApprovalWorkflowBL.GetApprovalWorkflows(LoggedAccount.ID, Context);
            }

            return View("ApprovalWorkflows", model);
        }

        [HttpGet]
        public ActionResult AddEditApprovalPhase(int? selectedApprovalWorkflow, int? selectedPhase, bool copyPhase = false)
        {
            var model = new PhaseViewModel {ApprovalWorkflow = selectedApprovalWorkflow.GetValueOrDefault()};
            if (selectedPhase.HasValue)
            {
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.ApprovalPhase>(selectedPhase.Value, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                model = ApprovalWorkflowBL.GetPhaseDetails(LoggedAccount, selectedPhase.Value, Context);
                model.ApprovalPhaseCollaboratorInfo.LoadPhaseCollaborats(selectedPhase.Value, Context);

                if (copyPhase)
                {
                    model.IsCopyPhase = true;
                    model.PhaseName = string.Empty;
                    model.ApprovalWorkflows = ApprovalWorkflowBL.GetApprovalWorkflows(LoggedAccount.ID, Context);
                }
               
                model.ID = selectedPhase.Value;
            }
            else
            {
                ApprovalWorkflowBL.PopulateStaticData(model, LoggedAccount, Context);
            }
            model.LoggedAccount = LoggedAccount;
           
            return View("AddEditPhase", model);
        }

        [HttpGet]
        public ActionResult AddEditAdHocWorkflowPhase()
        {
            var model = new PhaseViewModel();
            ApprovalWorkflowBL.PopulateStaticData(model, LoggedAccount, Context);
            model.LoggedAccount = LoggedAccount;
            model.isAdHocWorkflow = true;
            var jsonResult = Json(
                new
                {
                    Status = 200,
                    Content = RenderViewToString(this, "~/Views/Approvals/_AddAdHocWorkflow.cshtml", LoggedUser.ID, model)
                },
                JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpGet]
        public JsonResult IsApprovalWorkflowName_Available(ApprovalWorkflowDashboardModel model)
        {
            var workflow = model.ApprovalWorkflow;
            if (ApprovalWorkflow.IsApprovalWorkflowNameUnique(LoggedAccount.ID, workflow.Name,
                workflow.ID, Context))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            string infoMsg = String.Format(Resources.Resources.lblNameIsNotAvailable, workflow.Name);

            return Json(infoMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsApprovalPhaseNameUnique(PhaseViewModel model)
        {
            var workflowId = model.IsCopyPhase ? model.SelectedApprovalWorkflow : model.ApprovalWorkflow;
            if (ApprovalPhase.IsApprovalPhaseNameUnique(workflowId, model.PhaseName, model.ID, Context))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            string infoMsg = String.Format(Resources.Resources.lblNameIsNotAvailable, model.PhaseName);

            return Json(infoMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetApprovalWorkflowPhases(int? workflowId)
        {
            try
            {
                if (workflowId != null)
                {
                    if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.ApprovalWorkflow>(workflowId.Value, LoggedAccount.ID, Context))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    ApprovalWorkflowPhasesDashboardModel workflowPhases = ApprovalWorkflowBL.GetApprovalWorkflowPhasesById(workflowId.Value, LoggedAccount.ID, Context);
                    return Json(
                        new
                            {
                                Status = 200,
                                Content = RenderViewToString(this, "ApprovalWorkflowPhases", LoggedUser.ID, workflowPhases)
                            },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
                   "ApprovalWorkflowController.GetApprovalWorkflowPhases : error return creating json result. {0} StackTrace {1}", ex.Message, ex.StackTrace);
            }
            return Json(
                    new
                    {
                        Status = 300,
                        Content = Resources.Resources.errCannotRetrieveApprovalWorkflowPhases
                    },
                    JsonRequestBehavior.AllowGet);
            
        }
        #endregion

        #region POST Action

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchApprovalWorkflows")]
        public ActionResult Search(ApprovalWorkflowDashboardModel model)
        {
            string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
            TempData[SearchApprovalWorkflows] = s;

            return RedirectToAction("ApprovalWorkflows");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteApprovalWorkflow")]
        public ActionResult DeleteApprovalWorkflow(int? selectedApprovalWorflowId)
        {
            try
            {
                if (selectedApprovalWorflowId.HasValue && !DALUtils.IsFromCurrentAccount<ApprovalWorkflow>(selectedApprovalWorflowId.Value, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                var approvalWorkflow = Context.ApprovalWorkflows.FirstOrDefault(w => w.ID == selectedApprovalWorflowId.Value);
                if (approvalWorkflow != null)
                {
                    foreach (var phase in approvalWorkflow.ApprovalPhases.ToList())
                    {
                        ApprovalWorkflowBL.DeleteApprovalPhase(phase.ID, Context);
                    }

                    Context.ApprovalWorkflows.Remove(approvalWorkflow);
                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ApprovalWorkflowController.DeleteApprovalWorkflow : Deletet approval workflow failed. {0}", ex.Message);
            }
            return RedirectToAction("ApprovalWorkflows");
        }

        [HttpPost]
        public ActionResult AddEditApprovalWorkflow(ApprovalWorkflowDashboardModel model, int? page)
        {
            try
            {
                #region Validations

                if (model.ApprovalWorkflow.ID > 0 && !DALUtils.IsFromCurrentAccount<ApprovalWorkflow>(model.ApprovalWorkflow.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (!CheckIsValidHtml(model.ApprovalWorkflow.Name))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                #endregion

                #region Add/Update Approval Workflow

                ApprovalWorkflowBL.AddOrUpdateWorkflow(model.ApprovalWorkflow, LoggedAccount.ID, LoggedUser.ID, Context);
                Context.SaveChanges();

                #endregion

                ViewBag.Page = page;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ApprovalWorkflowController.AddEditApprovalWorkflow : Add or Edit Approval Workflow Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalWorkflows", new { page = page});
        }

        [HttpPost]
        public ActionResult AddEditApprovalPhase(PhaseViewModel model)
        {
            //bool success = ModelState.IsValid;
            bool success = true;

            try
            {
                #region Validations

                if (model.ID > 0 && !DALUtils.IsFromCurrentAccount<ApprovalPhase>(model.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (!CheckIsValidHtml(model.PhaseName))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                var selectedExternalsers = PermissionsBL.GetSelectedExternalUsersAndRoles(model.ExternalUsers);
                Dictionary<int, int> externalRoles = selectedExternalsers.Select(s => s.Split('|')).Select(
                                r =>
                                new
                                {
                                    ExternalUser = r.ElementAt(0).ToInt().GetValueOrDefault(),
                                    ApprovalRole = r.ElementAt(1).ToInt().GetValueOrDefault()
                                }).ToDictionary(t => t.ExternalUser, t => t.ApprovalRole);
                if (model.SelectedUsersAndGroups.Count(u => u.IsChecked) > 0)
                {
                    if (!(model.SelectedUsersAndGroups.Any(u => u.IsChecked && !u.IsExternal && u.ApprovalRole == (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer)) && externalRoles.All(k => k.Value != (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer))
                    {
                        success = false;
                        ModelState.AddModelError("AccessError", Resources.Resources.SelectAtLeastOneApproverAndReviewer);
                    }

                    if (model.SelectedDecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                    {
                        if (model.DecisionMakers == null || model.DecisionMakers.All(d => !d.IsSelected))
                        {
                            success = false;
                            ModelState.AddModelError("PDMRequired", Resources.Resources.reqDecisionMaker);
                        }
                        else
                        {
                            var selectedUser = model.DecisionMakers.FirstOrDefault(d => d.IsSelected);
                            if (selectedUser != null)
                            {
                                var userExist = selectedUser.IsExternal ? externalRoles.Where(k => k.Value == (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer).Select(u => u.Key).ToList().Contains(selectedUser.ID)
                                                                        : model.SelectedUsersAndGroups.Where(u => u.IsChecked && u.ApprovalRole == (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer && !u.IsExternal).Select(u => u.ID).ToList().Contains(selectedUser.ID);

                                if (!UserBL.IsUserFromCurrentAccount(selectedUser.ID, selectedUser.IsExternal, LoggedAccount.ID, Context) || !userExist)
                                {
                                    return RedirectToAction("Unauthorised", "Error");
                                }
                            }
                        }
                    }
                }

                if (model.SelectedDeadlineType == (int) GMG.CoZone.Common.DeadlineType.Automatic && model.SelectedUnitValue == null)
                {
                    success = false;
                }

                if (!success)
                {
                    var collabInfo = new ApprovalPhaseCollaboratorInfo
                        {
                            CollaboratorsWithRole = model.SelectedUsersAndGroups.Where(o => o.IsExternal == false && o.IsGroup == false && o.IsChecked).Select(u => u.ID + "|" + u.ApprovalRole).ToList(),
                            Groups = model.SelectedUsersAndGroups.Where(o => o.IsExternal == false && o.IsGroup == true && o.IsChecked).Select(g => g.ID).ToList(),
                            ExternalCollaboratorsWithRole = selectedExternalsers
                        };

                    model.ApprovalPhaseCollaboratorInfo = collabInfo;
                    model.DecisionMakers = new List<DecisionMaker>();
                    model.ApprovalWorkflows = ApprovalWorkflowBL.GetApprovalWorkflows(LoggedAccount.ID, Context);
                    model.LoggedAccount = LoggedAccount;
                    ApprovalWorkflowBL.PopulateStaticData(model, LoggedAccount, Context);
                    return View("AddEditPhase", model);
                }
                
                #endregion

                #region Save/Update approval phase

                ApprovalWorkflowBL.AddOrUpdatePhase(model, LoggedAccount, LoggedUser.ID, Context);

                #endregion
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ApprovalWorkflowController.AddEditApprovalPhase : Add or Edit Approval Phase Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalWorkflows");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteApprovalPhase")]
        public ActionResult DeleteApprovalPhase(int selectedPhase)
        {
            try
            {
                #region Validations

                if (selectedPhase > 0 && !DALUtils.IsFromCurrentAccount<ApprovalPhase>(selectedPhase, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                #endregion

                #region Delete approval phase

                ApprovalWorkflowBL.DeleteApprovalPhase(selectedPhase, Context);
                Context.SaveChanges();

                #endregion
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "ApprovalWorkflowController.DeleteApprovalPhase : Delete Approval Phase Failed. {0}", ex.Message);
            }

            return RedirectToAction("ApprovalWorkflows");
        }

        /// <summary>
        /// Swithces the position between two approval phases
        /// </summary>
        /// <param name="changingPhase">The id of the phase from whitch the change is triggred</param>
        /// <param name="changedPhase">The id of the phase that is affected by the change</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangePhasePosition(int changingPhase, int changedPhase)
        {
            try
            {
                ApprovalWorkflowBL.ChangeApprovalPhasePosition(changingPhase, changedPhase, Context);
                    return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
                   "ApprovalWorkflowController.ChangePhasePosition : error return creating json result. {0} StackTrace {1}", ex.Message, ex.StackTrace);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = Resources.Resources.errCannotChangeApprovalPhasePosition
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Save a Ad-hoc workflow phase
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveAdHocWorkflowPhase(PhaseViewModel model)
        {
            var phaseModel = new PhaseViewModel();
            var errors = new Dictionary<string, List<string>>();
            bool success = true;
      
            try
            {
                #region Validations

                if (string.IsNullOrEmpty(model.AdHocWorkflowName))
                {
                    errors.Add("AdHocWorkflowName", new List<string> { Resources.Resources.errEmptyWorkflowName });
                }

                if (model.ID > 0 && !DALUtils.IsFromCurrentAccount<ApprovalPhase>(model.ID, LoggedAccount.ID, Context))
                {
                    errors.Add("", new List<string> {Resources.Resources.errSavingAdHocPhase + model.PhaseName});
                }

                var selectedExternalsers = PermissionsBL.GetSelectedExternalUsersAndRoles(model.ExternalUsers);
                Dictionary<int, int> externalRoles = selectedExternalsers.Select(s => s.Split('|')).Select(
                                r =>
                                new
                                {
                                    ExternalUser = r.ElementAt(0).ToInt().GetValueOrDefault(),
                                    ApprovalRole = r.ElementAt(1).ToInt().GetValueOrDefault()
                                }).ToDictionary(t => t.ExternalUser, t => t.ApprovalRole);

                if (model.SelectedUsersAndGroups != null && model.SelectedUsersAndGroups.Count(u => u.IsChecked) > 0)
                {
                    if (!(model.SelectedUsersAndGroups.Any(u => u.IsChecked && !u.IsExternal && u.ApprovalRole == (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer)) && externalRoles.All(k => k.Value != (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer))
                    {
                        success = false;
                        errors.Add("aApprovalTrigger", new List<string> { Resources.Resources.SelectAtLeastOneApproverAndReviewer });
                    }

                    if (model.SelectedDecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                    {
                        if (model.DecisionMakers == null || model.DecisionMakers.All(d => !d.IsSelected))
                        {
                            success = false;
                            errors.Add("primaryDecisionMaker", new List<string> { Resources.Resources.reqDecisionMaker });
                        }
                        else
                        {
                            var selectedUser = model.DecisionMakers.FirstOrDefault(d => d.IsSelected);
                            if (selectedUser != null)
                            {
                                var userExist = selectedUser.IsExternal ? externalRoles.Where(k => k.Value == (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer).Select(u => u.Key).ToList().Contains(selectedUser.ID)
                                                                        : model.SelectedUsersAndGroups.Where(u => u.IsChecked && u.ApprovalRole == (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer && !u.IsExternal).Select(u => u.ID).ToList().Contains(selectedUser.ID);

                                if (!UserBL.IsUserFromCurrentAccount(selectedUser.ID, selectedUser.IsExternal, LoggedAccount.ID, Context) || !userExist)
                                {
                                    errors.Add("", new List<string> { Resources.Resources.errSavingAdHocPhase + model.PhaseName});
                                }
                            }
                        }
                    }
                }
                else
                {
                    success = false;
                    errors.Add("aApprovalTrigger", new List<string> { Resources.Resources.errNoCollaboratorsSelected });
                }

                if (model.SelectedDeadlineType == (int)GMG.CoZone.Common.DeadlineType.Automatic && model.SelectedUnitValue == null)
                {
                    success = false;
                }

                if (!success && model.SelectedUsersAndGroups != null)
                {
                    var collabInfo = new ApprovalPhaseCollaboratorInfo
                    {
                        CollaboratorsWithRole = model.SelectedUsersAndGroups.Where(o => o.IsExternal == false && o.IsGroup == false && o.IsChecked).Select(u => u.ID + "|" + u.ApprovalRole).ToList(),
                        Groups = model.SelectedUsersAndGroups.Where(o => o.IsExternal == false && o.IsGroup == true && o.IsChecked).Select(g => g.ID).ToList(),
                        ExternalCollaboratorsWithRole = selectedExternalsers
                    };

                    model.ApprovalPhaseCollaboratorInfo = collabInfo;
                    model.DecisionMakers = new List<DecisionMaker>();
                    model.ApprovalWorkflows = ApprovalWorkflowBL.GetApprovalWorkflows(LoggedAccount.ID, Context);
                    ApprovalWorkflowBL.PopulateStaticData(model, LoggedAccount, Context);
                    errors.Add("", new List<string> { Resources.Resources.errSavingAdHocPhase + model.PhaseName });
                }

                #endregion

                if (ModelState.IsValid && success)
                {
                    #region Save/Update Ad-Hoc Workflow
                    if (model.ApprovalWorkflow == 0)
                    {
                        if (!ApprovalWorkflowBL.CheckAdHocWorkflowNameIsUnique(model.AdHocWorkflowName, LoggedAccount.ID, Context))
                        {
                            ApprovalWorkflowModel workflowModel = new ApprovalWorkflowModel()
                            {
                                Name = model.AdHocWorkflowName,
                                RerunWorkflow = model.RerunWorkflow
                            };
                            model.ApprovalWorkflow = ApprovalWorkflowBL.AddOrUpdateWorkflow(workflowModel, LoggedAccount.ID, LoggedUser.ID, Context);
                            phaseModel = model;
                            //Save/Update Ad-Hoc Phase
                            if (model.ID == 0 && ApprovalPhase.IsApprovalPhaseNameUnique(model.ApprovalWorkflow, model.PhaseName, LoggedAccount.ID, Context))
                            {
                                var phaseID = ApprovalWorkflowBL.AddOrUpdatePhase(model, LoggedAccount, LoggedUser.ID, Context);
                                phaseModel.ID = phaseID;
                                phaseModel.PhaseName = model.PhaseName;
                            }
                            else
                            {
                                success = false;
                                errors.Add("PhaseName", new List<string> { Resources.Resources.errPhaseNameNotUnique });
                            }
                        }
                        else
                        {
                            success = false;
                            errors.Add("AdHocWorkflowName", new List<string> { Resources.Resources.errWorkflowNameNotUnique });
                        }
                    }
                    else
                    {
                        phaseModel = model;
                        //Save/Update Ad-Hoc Phase
                        if (model.ID == 0)
                        {
                            if (ApprovalPhase.IsApprovalPhaseNameUnique(model.ApprovalWorkflow, model.PhaseName, LoggedAccount.ID, Context))
                            {
                                var phaseID = ApprovalWorkflowBL.AddOrUpdatePhase(model, LoggedAccount, LoggedUser.ID, Context);
                                phaseModel.ID = phaseID;
                                phaseModel.PhaseName = model.PhaseName;   
                            }
                            else
                            {
                                success = false;
                                errors.Add("PhaseName", new List<string> { Resources.Resources.errPhaseNameNotUnique });
                            }
                        }
                        else
                        {
                            var phaseID = ApprovalWorkflowBL.AddOrUpdatePhase(model, LoggedAccount, LoggedUser.ID, Context);
                            phaseModel.ID = phaseID;
                            phaseModel.PhaseName = model.PhaseName;   
                        }
                    }

                    #endregion   
                }
                else
                {
                    foreach (string key in ModelState.Keys)
                    {
                        if (!ModelState[key].Errors.Any())
                            continue;

                        errors.Add(key, ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
                    }
                    success = false;
                }
                return Json(new 
                {
                    Success = success,
                    Errors = errors,
                    Content = phaseModel
                });
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
                     "ApprovalWorkflowController.SaveAdhocWorkflow : error return creating json result. {0} StackTrace {1}", ex.Message, ex.StackTrace);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = Resources.Resources.errSavingAdHocPhase
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteAdHocWorkflowPhase(int selectedPhase) 
        {
            var errors = new Dictionary<string, List<string>>();

            //check if user is allowed to delete the phase
            if (selectedPhase > 0 && !DALUtils.IsFromCurrentAccount<ApprovalPhase>(selectedPhase, LoggedAccount.ID, Context))
            {
                return Json(new 
                                { 
                                    redirectUrl =RedirectToAction("Unauthorised", "Error"), 
                                    isRedirect = true ,
                                    success = false
                                });
            }

            //delete the phase
            ApprovalWorkflowBL.DeleteApprovalPhase(selectedPhase, Context);
            Context.SaveChanges();

            return Json(new {
                success = true,
                phaseId = selectedPhase
            });
        }

        [HttpPost]
        public JsonResult ActivateOrDeactivateApprovalPhase(int phase, int approvalId, bool activatePhase)
        {
            try
            {
                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[GMG.CoZone.Common.Constants.ApprovalPopulateId] != null && int.Parse(Session[GMG.CoZone.Common.Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[GMG.CoZone.Common.Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    OutOfOfficeUserID = int.Parse(Session[GMG.CoZone.Common.Constants.OutOfOfficeUser].ToString());
                    OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                }
                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalId, LoggedAccount.ID, Context))
                {
                    return Json(
                            new
                            {
                                Status = 403
                            },
                            JsonRequestBehavior.AllowGet);
                }

                if (!UserBL.IsApprovalWorkflowOwner(OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, Context))
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("Unauthorised", "Error"),
                        isRedirect = true
                    });
                }

                var instantNotification = ApprovalWorkflowBL.ActivateOrDeactivatePhase(new ActivateDeactivatePhase { Phase = phase, ActivatePhase = activatePhase, ApprovalId = approvalId,  LoggedUserId = (OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID), LoggedAccount = LoggedAccount}, Context);
                if (instantNotification != null)
                {
                    SignalRService signalRService = new SignalRService();
                    signalRService.AddInstantDashboardNotification(instantNotification);
                }
                ApprovalWorkflowBL.SendActivateOrDeactivateEmail(OutOfOfficeUserID == 0 ? LoggedUser : OutOfOfficeUser, phase, approvalId, activatePhase, Context);

                return Json(true);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Activation/Deactiovation of phase failed, message: {0}", ex.Message);
            }

            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(false);
        }

        [HttpPost]
        public JsonResult RerunWorkflowFromSpecifiedPhase(int phase, int approvalId, bool phaseIsInactive)
        {
            try
            {
                int OutOfOfficeUserID = 0;
                GMGColorDAL.User OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                if (Session[GMG.CoZone.Common.Constants.ApprovalPopulateId] != null && int.Parse(Session[GMG.CoZone.Common.Constants.ApprovalPopulateId].ToString()) == -8 && int.Parse(Session[GMG.CoZone.Common.Constants.OutOfOfficeUser].ToString()) > 0)
                {
                    OutOfOfficeUserID = int.Parse(Session[GMG.CoZone.Common.Constants.OutOfOfficeUser].ToString());
                    OutOfOfficeUser = Context.Users.FirstOrDefault(u => u.ID == OutOfOfficeUserID);
                }

                if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalId, LoggedAccount.ID, Context))
                {
                    return Json(
                            new
                            {
                                Status = 403
                            },
                            JsonRequestBehavior.AllowGet);
                }
                if (!UserBL.IsApprovalWorkflowOwner(OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, Context))
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("Unauthorised", "Error"),
                        isRedirect = true
                    });
                }
                
                if (phaseIsInactive)
                {
                    ApprovalWorkflowBL.ActivateOrDeactivatePhase(new ActivateDeactivatePhase { Phase = phase, ActivatePhase = true, ApprovalId = approvalId, LoggedUserId = OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, LoggedAccount = LoggedAccount, UpdateStatusAndRerun = true}, Context);
                }
                
                ApprovalWorkflowBL.RerunFromThisPhase(approvalId, phase, OutOfOfficeUserID == 0 ? LoggedUser : OutOfOfficeUser, Context);

                _soadStateService.ResetSOADState(approvalId, phase);

                var signalRService = new SignalRService();
                signalRService.AddInstantDashboardNotification(ApprovalWorkflowBL.GetInstantNotification(approvalId, phase, LoggedAccount, OutOfOfficeUserID == 0 ? LoggedUser.ID : OutOfOfficeUserID, Context));

                return Json(true);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Rerun workflow from specified phase failed, message: {0}", ex.Message);
            }

            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(false);
        }
        #endregion

    }
}
