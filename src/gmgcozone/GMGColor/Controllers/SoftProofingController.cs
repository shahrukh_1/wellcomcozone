﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows.Controls;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using Newtonsoft.Json;
using AppModule = GMG.CoZone.Common.AppModule;
using System.Web.Script.Serialization;
using System.Threading;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Settings.Interfaces;
using GMG.CoZone.Common.Module;

namespace GMGColor.Controllers
{
    public class SoftProofingController : BaseController
    {
        private ISmtpQueueParser _smtpQueueReader;

        public SoftProofingController(IDownloadService downlaodService, IMapper mapper, ISmtpQueueParser smtpQueueReader) : base(downlaodService, mapper)
        {
            _smtpQueueReader = smtpQueueReader;
        }

        #region Post Actions
        [HttpPost]
        public ContentResult SubmitData(SoftProofing model, string token)
        {
            string errorMessage = string.Empty;
            bool isError = false;

            try
            {
                isError = (token ?? string.Empty).ToLower() != GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower();
                if (isError)
                {
                    errorMessage = TrialAccountMessages.InvalidSecurityToken;
                }

                if (!isError && !ModelState.IsValid)
                {
                    isError = true;
                    errorMessage = ModelState.FirstOrDefault(o => o.Value.Errors.Any()).Value.Errors.FirstOrDefault().ErrorMessage;
                }

                #region Check the model to have the required fields

                if (!isError && string.IsNullOrEmpty(model.AuthKey))
                {
                    errorMessage = Resources.Resources.reqAuthKey;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.MachineId))
                {
                    errorMessage = Resources.Resources.reqMachineId;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.FileName))
                {
                    errorMessage = Resources.Resources.reqFileName;
                    isError = true;
                }
                if (!isError && model.FileData == null)
                {
                    errorMessage = Resources.Resources.reqDataFile;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.CalibrationSoftware))
                {
                    errorMessage = Resources.Resources.reqCalibrationSoftware;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.WorkstationName))
                {
                    errorMessage = Resources.Resources.reqWorkstationName;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.DisplayIdentifier))
                {
                    errorMessage = Resources.Resources.reqDisplayIdentifier;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.DisplayName))
                {
                    errorMessage = Resources.Resources.reqDisplayName;
                    isError = true;
                }
                if (!isError && string.IsNullOrEmpty(model.CalibrationDateTimeUTC))
                {
                    errorMessage = Resources.Resources.reqCalibrationTimeZoneOffset;
                    isError = true;
                }

                #endregion

                if (!isError)
                {
                    var workstationCalibrationDataGuid =  SoftProofingBL.SaveWorkstationData(model, LoggedUser);

                    //create define queue message for validation of icc profile
                    Dictionary<string, string> dictJobParameters = new Dictionary<string, string>();
                    dictJobParameters.Add(GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid, workstationCalibrationDataGuid);

                    GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.SoftProofingCalibrationWrite);

                    var connectionError = string.Empty;
                    var gmgConfiguration = new GMGConfiguration();
                    var queueMessageManager = new QueueMessageManager(gmgConfiguration);
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                    {
                        connectionError = queueMessageManager.WaitForAWSMessage(GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationReadQueueName,
                                                                            GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid, workstationCalibrationDataGuid,
                                                                            GMGColorConfiguration.AppConfiguration.WaitForProcessedWorkstationDataTimeInSeconds);

                    }
                    else
                    {
                        connectionError = queueMessageManager.WaitForLocalMessage(GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationReadQueueName,
                                                                            GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid, workstationCalibrationDataGuid,
                                                                            GMGColorConfiguration.AppConfiguration.WaitForProcessedWorkstationDataTimeInSeconds);
                    }

                    if (!string.IsNullOrEmpty(connectionError))
                    {
                        errorMessage = connectionError;
                        isError = true;
                    }
                }
            }
            catch (ExceptionBL ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);

                isError = true;
                errorMessage = GMGColor.Resources.Resources.errUploadCalibraionDataRequestFailed;
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);

                isError = true;
                errorMessage = GMGColor.Resources.Resources.errUploadCalibraionDataRequestFailed;
            }

            Response.StatusCode = isError ? 400 : 200;
            return Content(errorMessage);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveCalibrationTimeout")]
        public ActionResult SaveCalibrationTimeout(SoftProofingSettings model)
        {
            try
            {
                GMGColorBusinessLogic.SoftProofingBL.UpdateCalibrationTimeout(model, LoggedAccount.ID);
                return RedirectToSettings(model);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return RedirectToAction("Settings");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteWorkstation")]
        public ActionResult DeleteWorkstation(SoftProofingSettings model)
        {
            try
            {
                GMGColorBusinessLogic.SoftProofingBL.DeleteWorkstation(model);
                return RedirectToSettings(model);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return RedirectToAction("Settings");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Search")]
        public ActionResult SearchWorkstations(SoftProofingSettings model)
        {
            return RedirectToSettings(model);
        }

        [HttpPost]
        public JsonResult GetWorkstation(int workstationId)
        {
            try
            {
                GMGColorDAL.CustomModels.WorkstationEdit workstation = SoftProofingBL.GetWorkstation(workstationId, LoggedAccount.ID);
                List<KeyValuePair<int, string>> accountUsers = AccountBL.GetUsers(LoggedAccount.ID);
                List<KeyValuePair<int, string>> usersWithWorstation = SoftProofingBL.GetUsersWithWorkstation(LoggedAccount.ID, workstationId);
                IEnumerable<KeyValuePair<int, string>> users = accountUsers.Except(usersWithWorstation);

                return Json(new { workstation, users });
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return Json("");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveWorkstation")]
        public JsonResult SaveWorkstation(GMGColorDAL.CustomModels.WorkstationEdit model)
        {
            try
            {
                bool error = !ModelState.IsValid;
                if (!error && string.IsNullOrEmpty(model.Name))
                {
                    ModelState.AddModelError("Name", Resources.Resources.reqName);
                    error = true;
                }
                if (!error && model.SelectedUserId == 0)
                {
                    ModelState.AddModelError("SelectedUserId", Resources.Resources.reqUsername);
                    error = true;
                }
                if (!error && SoftProofingBL.CheckIfWorkstationAlreadyExists(model.SelectedUserId, model.MachineId, model.ID))
                {
                    ModelState.AddModelError("SelectedUserId", Resources.Resources.errWorkstationNotUnique);
                    error = true;
                }
                if (!error && model.Name.Length > 128)
                {
                    ModelState.AddModelError("Name", Resources.Resources.errTooManyCharactors);
                    error = true;
                }
                if (!error)
                {
                    bool result = SoftProofingBL.UpdateWorkstation(model, LoggedAccount.ID);
                    return Json(new { saved = result });
                }
                else
                {
                    return Json(new { errors = GetErrorMessages() });
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return Json(new { saved = false });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ExportReportsToCSV")]
        public ActionResult ExportReportsToCsv(SoftProofingSettings model)
        {
            try
            {
                SoftProofingBL.SettingsParam param = new SoftProofingBL.SettingsParam()
                {
                    SettingsType = SoftProofingSwitchEnum.Reports,
                    LoggedUserId = LoggedUser.ID,
                    LoggedAccountId = LoggedAccount.ID,
                    CurrentPage = -1,
                    SearchText = model.SearchText,
                    SortField = model.SortField,
                    SortDirection = model.SortDir,
                };

                SoftProofingSettings settings = GMGColorBusinessLogic.SoftProofingBL.GetSettings(param, LoggedAccountType);
                byte[] fileContent = SoftProofingBL.ConvertReportsToCsv(settings.Reports);
                string filename = string.Format("Export_{0}.csv", DateTime.UtcNow.ToShortDateString().Replace('/', '-'));
                return File(fileContent, "text/csv", filename);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return RedirectToSettings(model);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchReports")]
        public ActionResult SearchReports(SoftProofingSettings model)
        {
            model.Filters.DateTimeFormat = DateTimeFormatBL.GetDateFormatPattern(LoggedUser.ID);
            return RedirectToSettings(model);
        }

        private Dictionary<string, List<string>> GetErrorMessages()
        {
            return ModelState.Keys.Where(key => ModelState[key].Errors.Any()).ToDictionary(key => key, key => ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
        }

        private ActionResult RedirectToSettings(SoftProofingSettings model, bool allPages = false)
        {
            return RedirectToAction("Settings", "SoftProofing",
                                  new
                                  {
                                      page = allPages ? -1 : model.Page,
                                      type = model.Type,
                                      search = model.SearchText,
                                      sort = model.SortField,
                                      sortdir = model.SortDir,
                                      rowsPerPage = model.RowsPerPage,
                                      SelectedWorkstations = string.Join("|", model.Filters.SelectedWorkstations),
                                      SelectedUsers = string.Join("|", model.Filters.SelectedUsers),
                                      CalibrationStartDate = model.Filters.CalibrationStartDate.HasValue ? model.Filters.CalibrationStartDate.Value.Ticks : 0,
                                      CalibrationEndDate = model.Filters.CalibrationEndDate.HasValue ? model.Filters.CalibrationEndDate.Value.Ticks : 0,
                                  });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ResetFilters")]
        public ActionResult ResetFilters(SoftProofingSettings model)
        {
            return RedirectToAction("Settings", "SoftProofing",
                                    new { type = SoftProofingSwitchEnum.Reports });
        }

        [HttpPost]
        public ActionResult AddEditPaperTint(PaperTintViewModel model, string hdnPaperTint, SoftProofingSwitchEnum type = SoftProofingSwitchEnum.PaperTint) 
        {
            try
            {
                if (!CheckIsValidHtml(model.Name))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                // convert to json string back to object
                var serializer = new JavaScriptSerializer();
                var pptMeasurementConditions = serializer.Deserialize<PaperTintViewModel>(hdnPaperTint);
                model.MeasurementConditionValues = pptMeasurementConditions.MeasurementConditionValues;
                model.SelectedMediaCategory = pptMeasurementConditions.SelectedMediaCategory;

                //check if name is unique
                if (SoftProofingBL.IsNameUnique(model.ID, SoftProofingSwitchEnum.PaperTint, model.Name, LoggedAccount.ID, Context))
                {
                    // check to see if a new paper tint should be added or an old one should be updated
                    if (model.ID == 0)
                    {
                        // add the new paper tint
                        SoftProofingBL.AddNewPaperTint(model, LoggedAccount.ID, LoggedUser.ID, Context);
                    }
                    else
                    {
                        SoftProofingBL.UpdatePaperTint(model, LoggedAccount.ID, Context);
                    }
                    return RedirectToAction("Settings", "SoftProofing", new { type = SoftProofingSwitchEnum.PaperTint });
                }
                else 
                {
                    ModelState.AddModelError("Name", Resources.Resources.lblSoftProofingDuplicateNameWarning);
                    model.MeasurementConditions = SoftProofingBL.GetMeasurementConditions(Context);
                    model.MediaCategories = SoftProofingBL.GetMediaCategoryList(Context);
                    return View("AddNewPaperTint", model);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "SoftProofingController.AddEditPaperTint : error occured while saving changes from AddEditPaperTint View. {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpPost]
        public ActionResult AddEditSimulationProfile(SimulationProfileViewModel model, bool isSysAdmin, string hdnFileName_1, SoftProofingSwitchEnum type = SoftProofingSwitchEnum.SimulationProfile)
        {
            // get the uploaded file's name
            model.FileName = hdnFileName_1.Trim().Replace("|", string.Empty);

            //check uploaded profile name is not larger than 50 characters
            if (model.FileName.Length > 50)
            {
                return RedirectToAction("UnexpectedError", "Error");
            }

            if (!CheckIsValidHtml(model.ProfileName))
            {
                return RedirectToAction("XSSRequestValidation", "Error");
            }

            //check if name is unique
            if (SoftProofingBL.IsNameUnique(model.ID, SoftProofingSwitchEnum.SimulationProfile, model.ProfileName, LoggedAccount.ID, Context))
            {
                // check to see whether an add or an update is required
                if (model.ID == 0)
                {
                    SoftProofingBL.AddNewSimulationProfile(model, isSysAdmin, LoggedAccount.ID, LoggedAccount.Guid, LoggedUser.ID, LoggedAccount.Region, Context);
                }
                else
                {
                    SoftProofingBL.UpdateSimulationProfile(model, isSysAdmin, LoggedAccount, Context);
                }
                return isSysAdmin ? RedirectToAction("DefaultSimulationProfiles", "SoftProofing") : RedirectToAction("Settings", "SoftProofing", new { type = SoftProofingSwitchEnum.SimulationProfile });
            }
            else 
            {
                ModelState.AddModelError("ProfileName", Resources.Resources.lblSoftProofingDuplicateNameWarning);
                model.MeasurementConditions = SoftProofingBL.GetMeasurementConditions(Context);
                model.MediaCategories = SoftProofingBL.GetMediaCategoryList(Context);
                ViewBag.isSysAdmin = isSysAdmin;
                return View("AddNewSimulationProfile", model);
            }
        }

        [HttpPost]
        public ActionResult AddEditPrintSubstrate(MediaCategoryViewModel model, int? page)
        {
            try
            {
                if (LoggedAccountType != AccountType.Type.GMGColor)
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (SoftProofingBL.IsPrintSubstrateNameInUse(model, Context))
                {
                    ModelState.AddModelError("MediaCategoryName", Resources.Resources.errCompanyNameInUse);
                    return View(model);
                }

                SoftProofingBL.SavePrintSubstrateChanges(model, Context);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return RedirectToAction("UnexpectedError", "Error");
            }

            return RedirectToAction("PrintSubstrates", new { page });
        }

        [HttpPost]
        public ActionResult DeletePrintSubstrate(MediaCategoryViewModel model, int? page)
        {
            string error = null;
            try
            {
                if (LoggedAccountType != AccountType.Type.GMGColor)
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (!SoftProofingBL.DeletePrintSubstrate(model.ID, Context))
                {
                    error = Resources.Resources.errPrintSubstrateInUse;
                }
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return RedirectToAction("UnexpectedError", "Error");
            }

            return RedirectToAction("PrintSubstrates", new { page, error });
        }
        

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeletePaperTint")]
        public ActionResult DeletePaperTint(PaperTintViewModel model)
        {
            try
            {
                SoftProofingBL.DeletePaperTint(model.ID, LoggedAccount.ID, Context);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return RedirectToAction("Settings", new { type = SoftProofingSwitchEnum.PaperTint });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchPaperTint")]
        public ActionResult SearchPaperTint(SoftProofingSettings model)
        {
            return RedirectToSettings(model);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchSimulationProfile")]
        public ActionResult SearchSimulationProfile(SoftProofingSettings model)
        {
            return RedirectToSettings(model);
        }


        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteSimulationProfile")]
        public ActionResult DeleteSimulationProfile(SimulationProfileViewModel model, bool isSysAdmin)
        {
            try
            {
                List<int> simulationProfileId = new List<int>();
                simulationProfileId.Add(model.ID);
                if (!ApprovalBL.CheckUserCanAccess(simulationProfileId, Approval.CollaborateValidation.DeleteSimulationProfile, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                SoftProofingBL.DeleteSimulationProfile(LoggedAccountType, isSysAdmin, model.ID, LoggedAccount.Region, LoggedAccount.Guid, LoggedAccount.ID, Context);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }

            return isSysAdmin ? RedirectToAction("DefaultSimulationProfiles", "SoftProofing") : RedirectToAction("Settings", "SoftProofing", new { type = SoftProofingSwitchEnum.SimulationProfile });
        }

        #endregion

        #region Get Actions
        /// <summary>
        /// Settingses the specified page.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="type">The type.</param>
        /// <param name="search">The search.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="sortdir">The sortdir.</param>
        /// <param name="rowsPerPage">The rows per page.</param>
        /// <param name="selectedUsers">The selected users.</param>
        /// <param name="selectedWorkstations">The selected workstations.</param>
        /// <param name="calibrationStartDate">The calibration start date.</param>
        /// <param name="calibrationEndDate">The calibration end date.</param>
        /// <param name="verificationStartDate">The verification start date.</param>
        /// <param name="verificationEndDate">The verification end date.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Settings(int page = 1, SoftProofingSwitchEnum type = SoftProofingSwitchEnum.SimulationProfile, string search = "", string sort = "CalibrationDate", string sortdir = "desc", int rowsPerPage = 10, string selectedUsers = "", string selectedWorkstations = "", long calibrationStartDate = 0, long calibrationEndDate = 0, long verificationStartDate = 0, long verificationEndDate = 0)
        {
            try
            {
                ViewBag.PageSize = rowsPerPage;

                SoftProofingBL.SettingsParam param = new SoftProofingBL.SettingsParam()
                {
                    SettingsType = type,
                    LoggedUserId = LoggedUser.ID,
                    LoggedAccountId = LoggedAccount.ID,
                    CurrentPage = page,
                    SearchText = search,
                    SortField = sort,
                    SortDirection = sortdir,
                    RowsPerPage = rowsPerPage,
                    SelectedUsers =
                        selectedUsers.Split(new string[] { "|" },
                                            StringSplitOptions.
                                                RemoveEmptyEntries).Select(
                                                    o =>
                                                    o.ToInt().GetValueOrDefault
                                                        ()).ToList(),
                    SelectedWorkstations =
                        selectedWorkstations.Split(new string[] { "|" },
                                                   StringSplitOptions.
                                                       RemoveEmptyEntries).
                        ToList(),
                    CalibrationStartDate = calibrationStartDate > 0 ? (DateTime?)new DateTime(calibrationStartDate) : null,
                    CalibrationEndDate = calibrationEndDate > 0 ? (DateTime?)new DateTime(calibrationEndDate).AddDays(1).AddSeconds(-1) : null,
                };
                SoftProofingSettings model = GMGColorBusinessLogic.SoftProofingBL.GetSettings(param, LoggedAccountType);

                int maxPageNr = (model.TotalRowsNr + model.RowsPerPage - 1) / model.RowsPerPage;
                if (model.Page > maxPageNr && maxPageNr > 0)
                {
                    model.Page = maxPageNr;
                    return RedirectToSettings(model);
                }

                return View("Settings", model);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return View("Settings", new SoftProofingSettings());
        }

        [HttpGet]
        public ActionResult StartSession(StartSession model)
        {
            if (ModelState.IsValid && string.IsNullOrEmpty(model.UserGuid))
            {
                ModelState.AddModelError("UserGuid", Resources.Resources.reqUserGuid);
            }

            if (ModelState.IsValid && string.IsNullOrEmpty(model.MachineId))
            {
                ModelState.AddModelError("MachineId", Resources.Resources.reqMachineId);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    LoggedUser = UserBL.GetUserByGuid(model.UserGuid, Context);
                    SoftProofingSessionState sessionState = SoftProofingBL.GetSessionState(model.UserGuid, model.MachineId);
                    if (sessionState == SoftProofingSessionState.Ok && CreateSession())
                    {
                        SoftProofingBL.SetWorkstationConnected(model.UserGuid, model.MachineId, Context);

                        if (Response.Cookies[SoftProofingCookieName] != null)
                        {
                            Response.Cookies.Remove(SoftProofingCookieName);
                        }

                        HttpCookie cookie = new HttpCookie(SoftProofingCookieName)
                        {
                            Expires = DateTime.Now.AddMinutes(SoftProofingCookieTimeout),
                            Value = model.ToString()
                        };
                        Response.SetCookie(cookie);

                        return RedirectToModule(AppModule.Collaborate, false, Resources.Resources.titleCannotContinued, Resources.Resources.lblNoApprovalsAccessMessage);
                    }
                    else if (sessionState == SoftProofingSessionState.InProgress)
                    {
                        return RedirectToAction("Custom", "Error",
                                                new
                                                {
                                                    title = HttpUtility.HtmlEncode(Resources.Resources.lblInProgressSoftProofingSessionTitle),
                                                    reason = HttpUtility.HtmlEncode(Resources.Resources.lblInProgressSoftProofingSessionMessage)
                                                });
                    }
                    else if (sessionState == SoftProofingSessionState.NotActive)
                    {
                        return RedirectToAction("Custom", "Error",
                                                new
                                                {
                                                    title = HttpUtility.HtmlEncode(Resources.Resources.lblNotActiveSoftProofingSessionTitle),
                                                    reason = HttpUtility.HtmlEncode(Resources.Resources.lblNotActiveSoftProofingSessionMessage)
                                                });
                    }
                }
                catch (ExceptionBL exbl)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                }
                catch (Exception ex)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                }
            }

            return RedirectToAction("Unauthorised", "Error");
        }

        [HttpGet]
        public ActionResult AddEditPaperTint(int? id)
        {
            if (id != null && !DALUtils.IsFromCurrentAccount<GMGColorDAL.PaperTint>(id.Value, LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            var model = new PaperTintViewModel()
            {
                MediaCategories = SoftProofingBL.GetMediaCategoryList(Context),
                MeasurementConditions = SoftProofingBL.GetMeasurementConditions(Context)
            };

            if (id != null)
            {
                model.ID = id.Value;
                SoftProofingBL.PopulatePaperTintModel(model, Context);
            }
                
            ViewBag.PaperTint = JsonConvert.SerializeObject(model);
            return View("AddNewPaperTint", model);
        }

        [HttpGet]
        public ActionResult AddEditSimulationProfile(int? id, bool? isSysAdmin)
        {
            if (id != null && !DALUtils.IsFromCurrentAccount<GMGColorDAL.SimulationProfile>(id.Value, LoggedAccount.ID, Context) && LoggedAccountType != AccountType.Type.GMGColor ||
                LoggedAccountType != AccountType.Type.GMGColor && isSysAdmin == true ||
                isSysAdmin == null)
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            // populate list of Print Substrates 
            var model = new SimulationProfileViewModel()
            {
                MediaCategories = SoftProofingBL.GetMediaCategoryList(Context),
                MeasurementConditions = SoftProofingBL.GetMeasurementConditions(Context)
            };

            // get the account region and temp path
            ViewBag.LoggedAccount.Region = LoggedAccount.Region;
            ViewBag.PathToTempFolder = LoggedUserTempFolderPath;

            if (id != null)
            {
                model.ID = id.Value;
                SoftProofingBL.PopulateSimulationProfileModel(isSysAdmin.Value, model, Context);
            }
            ViewBag.isSysAdmin = isSysAdmin;
            return View("AddNewSimulationProfile", model);
        }

        [HttpGet]
        public ActionResult AddEditPrintSubstrate(int? id, int? page)
        {
            MediaCategoryViewModel model = id != null ? SoftProofingBL.GetMediaCategoryViewModel(id.Value, Context) : new MediaCategoryViewModel();

            ViewBag.page = page;

            return View(model);
        }

        [HttpGet]
        [ChildActionOnly]
        public ActionResult GetSimulationProfilesList(SoftProofingSettings model = null)
        {
            try
            {
                ViewBag.isSysAdmin = false;
                ViewBag.SmpIdList = SoftProofingBL.GetSimulationProfileIdList(model.SimulationProfiles);
                return PartialView("_SoftProofingSimulationProfilePartial", model.SimulationProfiles);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "SoftProofingController.GetSimulationProfile : error occured while getting the Simulation Profiles {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpGet]
        [ChildActionOnly]
        public ActionResult GetPaperTintsList(SoftProofingSettings model = null)
        {
            try
            {
                return PartialView("_SoftProofingPaperTintPartial", model.PaperTints);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "SoftProofingController.GetPaperTintsList : error occured while getting the PaperTints List. {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpGet]
        public ActionResult DefaultSimulationProfiles(string error)
        {
            //bool for Sys Admin to edit the default simulation profiles
            ViewBag.isSysAdmin = true;

            // populate required parameters
            SoftProofingBL.SettingsParam param = new SoftProofingBL.SettingsParam(){
                LoggedAccountId = 0,
                CurrentPage = 0,
                SortField = "Name",
                SortDirection = "desc",
                RowsPerPage = 10,
                SearchText = string.Empty
            };

            //get the list of simulation profiles
            List<SimulationProfileDashBoardViewModel> model = SoftProofingBL.SearchSimulationProfiles(param, LoggedAccountType);
            
            if (!String.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("", error);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult PrintSubstrates(string error)
        {
            List<MediaCategoryViewModel> model = SoftProofingBL.GetMediaCategoryList(Context);

            if (!String.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("", error);
            }

            return View(model);
        }

        [HttpGet]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None, NoStore = true)]
        public ActionResult GetSimulationProfilesLabValues(int[] simProfileIdsList)
        {
            if (simProfileIdsList == null)
            {
                return Json(new { simProfValues = new List<SimulationProfileLabCalculations>() }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                List<SimulationProfileLabCalculations> simProfsCalculatedLabValues = SoftProofingBL.GetSimulationProfileCalculatedLabValues(simProfileIdsList, Context);
                return Json(new { simProfValues = simProfsCalculatedLabValues }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "SoftProofingController.GetSimulationProfilesLabValues : error occured while getting the Simulation Profile Lab Values. {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }
        #endregion
    }
}