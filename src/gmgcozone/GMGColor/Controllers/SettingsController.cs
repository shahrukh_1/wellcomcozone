﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using GMG.CoZone.Common;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;
using Newtonsoft.Json;
using Utils = GMGColor.Common.Utils;
using GMGColorDAL.CustomModels.XMLEngine;
using System.Net;
using System.Web.UI;
using System.Net.Cache;
using AppModule = GMG.CoZone.Common.AppModule;
using GMG.CoZone.Settings.Interfaces;
using GMG.CoZone.Settings.DomainModels;
using GMG.CoZone.Common.DomainModels;
using System.Text;
using GMG.CoZone.Settings.UI.ViewModels;
using AutoMapper;
using GMG.CoZone.SSO.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMGColorDAL.CustomModels.Checklists;
using System.IO;
using System.Data;


namespace GMGColor.Controllers
{
	public class SettingsController : BaseController
	{
		private const string CPErrors = "CPErrors";
		private const string CPSearchInstances = "CPSearchInstances";
		private const string CPSearchWorkflows = "CPSearchWorkflows";
		private const string PresetModelErrors = "PresetModelErrors";
		private const string ProfileModelErrors = "ProfileModelErrors";
		private const string UploadEngineSettingsModelErrors = "UploadEngineSettingsErrors";
		private const string UsersSearchText = "UsersSearchText";
		private const string ExternalUsersSearchText = "ExternalUsersSearchText";
		private const string SUserGroupsErrors = "SettingsUserGroupsErrors";
		private const string ExternalUsersErrors = "ExternalUsersErrors";
		private const string InternalUsersErrors = "InternalUsersErrors";
        private const string SearchChecklists = "SearchChecklists";
        private const string SearchPredefinedTagwords = "SearchPredefinedTagwords";

        private readonly ISettingsService _settingsService;
        private readonly IMapper _mapper;

		public SettingsController(ISettingsService settingsService, IMapper mapper, IDownloadService downlaodService) : base(downlaodService, mapper)
        {
			_settingsService = settingsService;
            _mapper = mapper;
		}


		#region GET Actions

		[HttpGet]
		public ActionResult Index()
		{
			/// Special case for HQ(gmgcozone), remove Account Settings Summary
			if (this.LoggedAccountType == GMGColorDAL.AccountType.Type.GMGColor)
			{
				return RedirectToAction("Profile", "Settings");
			}
			else
			{
				AccountSettings.Summary model = new AccountSettings.Summary(this.LoggedAccount, LoggedAccountSubscriptionDetails, LoggedAccountBillingPlans, Context);
				//Get subscription Plans from cache
				ViewBag.AccountSubscriptionDetails = LoggedAccountSubscriptionDetails;
				ViewBag.AccountBillingPlans = LoggedAccountBillingPlans;
				return View("Index", model);
			}
		}

		[HttpGet]
		public ActionResult Profile()
		{
			AccountSettings.Profile model = new AccountSettings.Profile(this.LoggedAccount, this.LoggedUser, Context);
			return View("Profile", model);
		}

		[HttpGet]
		public ActionResult SiteSettings()
		{
			AccountSettings.SiteSettings model = new AccountSettings.SiteSettings(this.LoggedAccount, Context);
            model.objSiteSettings.MeasurementType = model.objLoggedAccount.IsMeasurementsInInches ? "true" : "false";

            return View("SiteSettings", model);
		}

		[HttpGet]
		public ActionResult Customize()
		{
			AccountSettings.Customise model = new AccountSettings.Customise(this.LoggedAccount, this.LoggedUserTempFolderPath, Context);
			model.objCustomise.objColorSchema = ThemeBL.GetUserGlobalColorScheme(LoggedUser.ID, Context);
			model.objCustomise.ShowSelectedTheme = true;
			return View("Customize", model);
		}

		[HttpGet]
		public ActionResult BrandingCustomization(int? page)
		{
			Session[GMGColorConstants.AccountBrandingPagination] = (page == null) ? 1 : page.Value;
			AccountSettings.Branding model = new AccountSettings.Branding(this.LoggedAccount, Context) {Page = page};
			return View("BrandingPresetSettings", model);
		}

		[HttpGet]
		public ActionResult ApprovalCustomDecision()
		{
			var customDecisions = GMGColorDAL.ApprovalCustomDecision.GetCustomDecisionsViewModel(LoggedAccount.ID, Context);
			ViewBag.Languages = Locale.GetLocalizeLocaleNames(Context);
			var model = new AccountSettings.ApprovalStatus
				{
					Account = LoggedAccount.ID,
					CustomDecisions = customDecisions, 
					LoadedUserLanguage = LoggedUser.Locale
				};
			ViewBag.StatusTranslations = JsonConvert.SerializeObject(model);
			return View("ApprovalStatus", model);
		}

		[HttpPost]
		public ActionResult AddEditApprovalCustomDecision(string StatusesModel)
		{
			try
			{
                if (!CheckIsValidHtml(StatusesModel))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }
                var serializer = new JavaScriptSerializer();
				var newApprovalStatuses = serializer.Deserialize<AccountSettings.ApprovalStatus>(StatusesModel);
				SettingsBL.AddOrUpdateCustomDecision(LoggedAccount.ID, newApprovalStatuses, Context);

				return RedirectToAction("ApprovalCustomDecision");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
					"SetingsController.AddEditApprovalCustomDecision : error occured while saving changes from ApprovalStatus View. {0}", ex.Message);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpGet]
		public ActionResult AddEditBrandingPreset(int? selectedPreset, int? page)
		{
			try
			{
				AccountSettings.BrandingPreset model = new AccountSettings.BrandingPreset(LoggedAccount.ID, LoggedUserTempFolderPath, Context);
				model.Page = page;
				model.objCustomise.ShowSelectedTheme = true;
				model.objCustomise.objAccount.HeaderLogoPath = string.Empty;
				model.objCustomise.objAccount.EmailLogoPath = string.Empty;
				model.objCustomise.objAccount.FavIconPath = string.Empty;

				if (selectedPreset.HasValue && selectedPreset.Value > 0)
				{
					if (!DALUtils.IsFromCurrentAccount<BrandingPreset>(selectedPreset.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}
					var presetId = selectedPreset.GetValueOrDefault();
					BrandingPreset brandingBo = DALUtils.SearchObjects<BrandingPreset>(o => o.ID == selectedPreset.Value, Context).FirstOrDefault();
					if (brandingBo != null)
					{
						model.ID = selectedPreset.Value;
						model.Name = brandingBo.Name;
						model.objCustomise.objColorSchema = brandingBo.BrandingPresetThemes.Select(th => new ThemeColorSchema()
						{
							ID = th.ID,
							Key = th.Key,
							Active = th.Active,
							ThemeName = th.ThemeName,
							Colors = th.ThemeColorSchemes.Select(cl => new ThemeColors ()
							{
								ID = cl.ID,
								Name = cl.Name,
								TopColor = cl.THex,
								BottomColor = cl.BHex
							}).ToList()
						}).ToList();

						model.objCustomise.objColorSchema = ThemeBL.ConvertThemesColors(model.objCustomise.objColorSchema, GMGColorBusinessLogic.Common.ConversionEnum.Convert.HexToRbg);

						if (!string.IsNullOrEmpty(brandingBo.HeaderLogoPath))
						{
							var headerLogoPath = brandingBo.Guid + "/" + brandingBo.HeaderLogoPath;
							model.objCustomise.objAccount.HeaderLogoPath = UserBL.GetUserBrandingLogoPath(LoggedAccount, headerLogoPath, Account.LogoType.HeaderLogo, Context);
						}

						if (!string.IsNullOrEmpty(brandingBo.EmailLogoPath))
						{
							var emailLogoPath = brandingBo.Guid + "/" + brandingBo.EmailLogoPath;
							model.objCustomise.objAccount.EmailLogoPath = UserBL.GetUserBrandingLogoPath(LoggedAccount, emailLogoPath, Account.LogoType.EmailLogo, Context);
						}

						if (!string.IsNullOrEmpty(brandingBo.FavIconPath))
						{
							var favIconPath = brandingBo.Guid + "/" + brandingBo.FavIconPath;
							model.objCustomise.objAccount.FavIconPath = UserBL.GetUserBrandingLogoPath(LoggedAccount, favIconPath, Account.LogoType.FavIconLogo, Context);
						}

						model.UserGroups = SettingsBL.GetUserGroupsExceptSelected(LoggedAccount.ID, presetId, Context);

						//mark the selected groups
						foreach (var userGroupsInUser in model.UserGroups)
						{
							userGroupsInUser.IsUserGroupInUser = SettingsBL.IsPresetUserGroupSelected(selectedPreset.Value, userGroupsInUser.objUserGroup.ID, Context);
						}
					}
				}
				else
				{
					model.objCustomise.objColorSchema = ThemeBL.ConvertThemesColors(ThemeBL.GetDefaultSchema(Context), GMGColorBusinessLogic.Common.ConversionEnum.Convert.HexToRbg);
					model.objCustomise.objColorSchema.First().Active = true;
				}
				
				return View("AddEditBrandingPreset", model);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
					"SetingsController.AddEditBrandingPreset : error occured while populating AddEditBrandingPreset View. {0}", ex.Message);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult AddEditBrandingPreset(AccountSettings.BrandingPreset model, int? page, string hdnFileName_1, string hdnFileName_3, string hdnFileName_4)
		{
			try
			{
				if (model.ID > 0 && !DALUtils.IsFromCurrentAccount<BrandingPreset>(model.ID, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

                if (!CheckIsValidHtml(model.Name))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }
                if (!model.UserGroups.Any(u => u.IsUserGroupInUser))
				{
					ModelState.AddModelError("Error", Resources.Resources.reqGroup);
				}

				if (ModelState.IsValid)
				{
					SettingsBL.SaveOrUpdateBrandingPreset(model, LoggedAccount, hdnFileName_1, hdnFileName_3, hdnFileName_4, LoggedUserTempFolderPath, Context);
					Context.SaveChanges();
				}
				else
				{
					return View("AddEditBrandingPreset", new AccountSettings.BrandingPreset(LoggedAccount.ID, LoggedUserBrandingHeaderLogo, Context));
				}

			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					  "SetingsController.AddEditAccountBrandingPreset : error occured while saving a custom preset. {0}", ex.Message);
			}
			return RedirectToAction("BrandingCustomization", new { page });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteBrandingPreset")]
		public ActionResult DeleteBrandingPreset(int? selectedPreset, int? page)
		{
			try
			{
				if (selectedPreset.HasValue && selectedPreset.Value > 0)
				{
					if (!DALUtils.IsFromCurrentAccount<BrandingPreset>(selectedPreset.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					using (var ts = new TransactionScope())
					{
						BrandingPreset objPreset = SettingsBL.GetBrandingPresetByID(selectedPreset.GetValueOrDefault(), Context);

						if (objPreset != null)
						{
							foreach (BrandingPresetUserGroup presetGroup in objPreset.BrandingPresetUserGroups.ToList())
							{
								Context.BrandingPresetUserGroups.Remove(presetGroup);
							}

							//Delete the relation with BrandingPresetUserGroup for this preset
							objPreset.BrandingPresetUserGroups.Clear();

							foreach (var theme in objPreset.BrandingPresetThemes)
							{
								Context.ThemeColorSchemes.RemoveRange(theme.ThemeColorSchemes);
							}
							Context.BrandingPresetThemes.RemoveRange(objPreset.BrandingPresetThemes);
							Context.BrandingPresets.Remove(objPreset);
						}

						Context.SaveChanges();
						ts.Complete();
					}
				}

			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SetingsController.DeleteBrandingPreset : error occured while deleting a custom preset. {0}", ex.Message);
			}
			return page.HasValue
					   ? RedirectToAction("BrandingCustomization", new { page })
					   : RedirectToAction("BrandingCustomization");
		}

		[HttpGet]
		public ActionResult WhiteLabelSettings()
		{
			AccountSettings.WhiteLabelSettings model = new AccountSettings.WhiteLabelSettings(this.LoggedAccount, Context);
            ViewBag.AccountSubscriptionDetails = LoggedAccountSubscriptionDetails;

            return View("WhiteLabelSettings", model);
		}

		[HttpGet]
		public ActionResult DNSAndCustomDomain()
		{
			AccountSettings.DNSAndCustomDomain model = new AccountSettings.DNSAndCustomDomain(this.LoggedAccount, Context);

			if (TempData["modelErrors"] != null)
			{
				Dictionary<string, string> modelErrors = TempData["modelErrors"] as Dictionary<string, string>;
				foreach (KeyValuePair<string, string> error in modelErrors)
				{
					ModelState.AddModelError(error.Key, error.Value);
				}
			}

			return View("DNSAndCustomDomain", model);
		}

		[HttpGet]
		public ActionResult DeliverSettings(Enums.DeliverSettingsType? type, int? page)
		{
			ViewResult result;
			if (type.HasValue)
			{
				switch (type.Value)
				{
					case Enums.DeliverSettingsType.ColorProofWorkflows:
						{
							ColorProofSettings.ColorProofWorkflows model =
								new ColorProofSettings.ColorProofWorkflows(this.LoggedAccount, Context);
							model.page = page;

							if (TempData[CPSearchWorkflows] is string)
							{
								model.SearchText = TempData[CPSearchWorkflows] as string;
								model.UpdateDataSource(Context, model.SearchText);
							}

							if (TempData[CPErrors] is Dictionary<string, List<string>>)
							{
								Dictionary<string, List<string>> errors = TempData[CPErrors] as Dictionary<string, List<String>>;
								foreach (KeyValuePair<string, List<string>> error in errors)
								{
									foreach (string errorMessage in error.Value)
									{
										ModelState.AddModelError(error.Key, errorMessage);
									}
								}
							}

							ViewBag.IsDeliverAdministrator =
								LoggedUserDeliverRole ==
								Role.RoleName.AccountAdministrator;

							result = View("ColorProofWorkflowsSettings", model);
							break;
						}
					default:
						{
							ColorProofSettings.ColorProofInstances model = new ColorProofSettings.ColorProofInstances(LoggedAccount, Context);
							model.page = page;

							if (TempData[CPSearchInstances] is string)
							{
								model.SearchText = TempData[CPSearchInstances] as string;
								model.UpdateDataSource(Context, model.SearchText);
							}

							int totalConnections = 0;
							ViewBag.AvailableCPConnections = DeliverBL.GetAvailableColorProofConnnection(LoggedAccount, out totalConnections, Context);
							ViewBag.TotalConnections = totalConnections;
							result = View("ColorProofInstancesSettings", model);
							break;
						}
				}
			}
			else
			{
				int totalConnections = 0;
				ViewBag.AvailableCPConnections = DeliverBL.GetAvailableColorProofConnnection(LoggedAccount, out totalConnections, Context);
				ViewBag.TotalConnections = totalConnections;
				ColorProofSettings.ColorProofInstances model = new ColorProofSettings.ColorProofInstances(this.LoggedAccount, Context);
				result = View("ColorProofInstancesSettings", model);
			}
			return result;
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SwitchToInstances")]
		public ActionResult SwitchToInstances()
		{
			return RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SwitchToWorkflows")]
		public ActionResult SwitchToWorkflows()
		{
			return RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows });
		}

		[HttpGet]
		public ActionResult GlobalNotifications(Enums.Notification? type, int? page)
		{
			ViewResult result;
			ViewBag.page = page ?? 1;
			if (type.HasValue)
			{
				switch (type.Value)
				{
					case Enums.Notification.Schedule:
						{
							NotificationSchedules model = new NotificationSchedules(LoggedAccount.ID, Context);

							result = View("GlobalNotificationSchedules", model);
							break;
						}
					default:
						{
							NotificationPresets model = new NotificationPresets(LoggedAccount.ID, Context) {page = page};
							result = View("GlobalNotificationPresets", model);
							break;
						}
				}
			}
			else
			{
				NotificationPresets model = new NotificationPresets(LoggedAccount.ID, Context) { page = page };
				result = View("GlobalNotificationPresets", model);
			}

			return result;
		}

		[HttpGet]
		public ActionResult AddEditNotificationSchedule(int? SelectedInstance, int? page)
		{
			try
			{
				AddEditNotificationScheduleViewModel model = new AddEditNotificationScheduleViewModel(Context, LoggedAccount.ID, SelectedInstance.GetValueOrDefault());
				ViewBag.page = page ?? 1;
				ViewBag.userError = TempData["NoUserSelected"] != null ? true : false;
				return View("AddEditNotificationSchedule", model);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				   "SettingsController.AddEditNotificationSchedule : error occured in populating AddEdit notif schedule view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult SaveNotificationScheduleChanges(AddEditNotificationScheduleViewModel model, int? page)
		{
			try
			{
				if (!(model.Users.Any(us => us.IsUserGroupInUser == true) || model.UserGroups != null && model.UserGroups.Any(gr => gr.IsUserGroupInUser == true))) 
				{
					TempData["NoUserSelected"] = true;
					return RedirectToAction("AddEditNotificationSchedule", model);
				}

               

                if (model.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.NotificationSchedule>(model.ID, LoggedAccount.ID, Context) || !DALUtils.IsFromCurrentAccount<GMGColorDAL.AccountNotificationPreset>(model.SelectedPreset, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				SettingsBL.ProcessNotificationScheduleChanges(model, LoggedUser.ID, Context);
				SettingsBL.MarkNotificationAsDeleted(LoggedAccount.ID, Context, schedule: model.ID);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				   "SettingsController.SaveNotificationScheduleChanges : error occured in saving notif schedule changes. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}

			return RedirectToAction("GlobalNotifications", new {page, type = Enums.Notification.Schedule});
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteNotificationSchedule")]
		public ActionResult DeleteNotificationSchedule(int SelectedInstance, int? page)
		{
			try
			{
				if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.NotificationSchedule>(SelectedInstance, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				SettingsBL.DeleteNotificationSchedule(SelectedInstance, Context);
				Context.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				   "SettingsController.DeleteNotificationSchedule : error occured in deleting notif schedule. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}

			return RedirectToAction("GlobalNotifications", new { page, type = Enums.Notification.Schedule });
		}


		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SwitchToSchedules")]
		public ActionResult SwitchToSchedules()
		{
			return RedirectToAction("GlobalNotifications", new { type = Enums.Notification.Schedule });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SwitchToPresets")]
		public ActionResult SwitchToPresets()
		{
			return RedirectToAction("GlobalNotifications", new { type = Enums.Notification.Presets });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteColorProofInstance")]
		public ActionResult DeleteColorProofInstance(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue && SelectedInstance.Value > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ColorProofInstance>(SelectedInstance.Value, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}
				using (TransactionScope ts = new TransactionScope())
				{
					GMGColorDAL.ColorProofInstance instanceBO =
						DALUtils.GetObject<GMGColorDAL.ColorProofInstance>(SelectedInstance.Value, Context);

					List<GMGColorDAL.ColorProofWorkflow> workflowsForCurrentInstance =
						DALUtils.SearchObjects<GMGColorDAL.ColorProofWorkflow>(
							o => o.ColorProofInstance1.ID == SelectedInstance.Value, Context).ToList();
					bool exists =
						workflowsForCurrentInstance.Any(
							colorProofWorkflowBo =>
							colorProofWorkflowBo.ColorProofCoZoneWorkflows.Any(
								o => o.DeliverJobs.Count > 0));

					List<GMGColorDAL.ColorProofWorkflow> workflowsATP =
							DALUtils.SearchObjects<GMGColorDAL.ColorProofWorkflow>(
								o => o.ColorProofInstance == SelectedInstance.Value, Context).ToList();

                    List<int> workflowsATP_AsIntegers = workflowsATP.Select(w => w.ID).ToList();

                    List<GMGColorDAL.ColorProofCoZoneWorkflow> workflowsCZ =
						DALUtils.SearchObjects<GMGColorDAL.ColorProofCoZoneWorkflow>(
							o => workflowsATP_AsIntegers.Contains(o.ColorProofWorkflow), Context).ToList();

                    List<int> workflowsCZ_AsIntegers = workflowsCZ.Select(w => w.ID).ToList();

                    // delete user groups
                    List<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup> useGroupsList =
						DALUtils.SearchObjects<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(
							o => workflowsCZ_AsIntegers.Contains(o.ColorProofCoZoneWorkflow), Context).ToList();

					foreach (GMGColorDAL.ColorProofCoZoneWorkflowUserGroup userGroupBo in useGroupsList)
					{
						DALUtils.Delete<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(Context, userGroupBo);
					}

					foreach (GMGColorDAL.ColorProofCoZoneWorkflow w in workflowsCZ)
					{
						foreach (var sh in w.SharedColorProofWorkflows.ToList())
						{
							Context.SharedColorProofWorkflows.Remove(sh);
						}
					}

					if (exists)
					{
						GMGColorDAL.DeliverJobStatu statusBO = DALUtils.SearchObjects<GMGColorDAL.DeliverJobStatu>(o => o.Key == (int)GMG.CoZone.Common.DeliverJobStatus.Cancelled, Context).FirstOrDefault();
						if (statusBO == null)
							throw new Exception("CancelRequested status is not defined on deliver job status table");
						else
						{
							workflowsForCurrentInstance.ForEach(
								o =>
								o.ColorProofCoZoneWorkflows.ToList().ForEach(
									q => q.DeliverJobs.Where(n => n.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.Cancelled
																						&& n.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.CompletedDeleted
																						&& n.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.Completed
																					).ToList().ForEach(p => p.Status = statusBO.ID)));
						}
						instanceBO.IsDeleted = true;
						workflowsForCurrentInstance.ForEach(
							o =>
							o.ColorProofCoZoneWorkflows.ToList().ForEach(p => p.IsDeleted = true));
					}
					else
					{
						foreach (GMGColorDAL.ColorProofCoZoneWorkflow w in workflowsCZ)
						{
							DALUtils.Delete<GMGColorDAL.ColorProofCoZoneWorkflow>(Context, w);
						}
					

						foreach (GMGColorDAL.ColorProofWorkflow w in workflowsATP)
						{
							DALUtils.Delete<GMGColorDAL.ColorProofWorkflow>(Context, w);
						}

						DALUtils.Delete<GMGColorDAL.ColorProofInstance>(Context, instanceBO);
					}

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.DeleteColorProofInstance : error occured while deleting a ColorProof instance. {0}", ex.Message);
			}
			return page.HasValue
					   ? RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances, page = page.Value })
					   : RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances });
		}

		[HttpGet]
		public ActionResult AddEditColorProofInstance(int? id, int? page)
		{
			try
			{
				ColorProofSettings.ColorProofInstances model = new ColorProofSettings.ColorProofInstances();
				model.page = page;

				if (id.HasValue && id.Value > 0)
				{
					if (!DALUtils.IsFromCurrentAccount<ColorProofInstance>(id.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					ColorProofInstance instanceBo =
						DALUtils.SearchObjects<ColorProofInstance>(o => o.ID == id.Value, Context).FirstOrDefault();
					if (instanceBo != null)
					{
						model.ColorProofInstance.ID = id.Value;
						model.ColorProofInstance.CoZoneName = instanceBo.SystemName;
						model.ColorProofInstance.AdminName = instanceBo.AdminName;
						model.ColorProofInstance.Email = instanceBo.Email;
						model.ColorProofInstance.Address = instanceBo.Address;

						model.ColorProofInstance.Version = instanceBo.Version;
						model.ColorProofInstance.SerialNumber = instanceBo.SerialNumber;
						model.ColorProofInstance.LastSeen = instanceBo.LastSeen;
						model.ColorProofInstance.ManualStateKey = instanceBo.ColorProofInstanceState.Key;
						model.ColorProofInstance.IsOnline = instanceBo.IsOnline.GetValueOrDefault();
						model.ColorProofInstance.PairingCode = instanceBo.PairingCode;
						model.ColorProofInstance.PairingCodeExpirationDate = instanceBo.PairingCodeExpirationDate.GetValueOrDefault();
						model.ColorProofInstance.OldUserName = instanceBo.UserName;

						model.ColorProofInstance.ColorProofUsername = instanceBo.UserName;

						try
						{
							model.ColorProofInstance.ColorProofPassword = ColorProofInstance.GetDecryptedPassword(instanceBo.Password, Context);
						}
						catch (Exception)
						{
							model.ColorProofInstance.ColorProofPassword = String.Empty;
						}
					}
				}
				else
				{
					int totalConnections = 0;
					int availableConnections = DeliverBL.GetAvailableColorProofConnnection(LoggedAccount, out totalConnections, Context);

					if (availableConnections <= 0)
					{
						return RedirectToAction("Unauthorised", "Error");
					}


					model.ColorProofInstance.PairingCode =
						SettingsBL.GetPairingCode(GMGColorConfiguration.AppConfiguration.ColorProofPairingCodeRegion);
				}

				return View("AddEditColorProofInstance", model);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
					"BaseController.AddEditColorProofInstance : error occured while populating AddEditColorProofInstance View. {0}", ex.Message);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult AddEditColorProofInstance(ColorProofSettings.ColorProofInstance ColorProofInstance, int? page)
		{
			bool success = true;

			if (ColorProofInstance.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ColorProofInstance>(ColorProofInstance.ID, LoggedAccount.ID, Context))
			{
				return RedirectToAction("Unauthorised", "Error");
			}

			if (ModelState.IsValid)
			{
				if (string.IsNullOrEmpty(ColorProofInstance.Email))
				{
					success = false;
					ModelState.AddModelError("ColorProofInstance.Email", Resources.Resources.reqEmailAddress);
				}

				if (!GMGColorDAL.ColorProofInstance.IsCoZoneUsernameUnique(ColorProofInstance.ColorProofUsername,
					ColorProofInstance.ID, Context))
				{
					success = false;
					ModelState.AddModelError("ColorProofInstance.ColorProofUsername", Resources.Resources.reqColorProofInstance);
				}

				if (success)
				{
					// Edit instance
					if (ColorProofInstance.ID > 0)
					{
						bool isUniqueCoZoneName =
							GMGColorDAL.ColorProofInstance.IsCoZoneNameUnique(ColorProofInstance.CoZoneName,
																			  ColorProofInstance.ID, Context);
						if (!isUniqueCoZoneName)
						{
							success = false;
							ModelState.AddModelError("ColorProofInstance.CoZoneName",
													 Resources.Resources.reqColorProofInstanceUniqueName);
						}

						if (success)
						{
							int? colorProofInstanceId = SaveColorProofInstance(ColorProofInstance, Context);
							if (!colorProofInstanceId.HasValue)
							{
								success = false;
								ModelState.AddModelError(string.Empty,
														 Resources.Resources.DeliverSettingsCouldNotUpdateCPInstance);
							}
						}
					}
					else // New instance
					{
						bool isUniqueCoZoneName =
							GMGColorDAL.ColorProofInstance.IsCoZoneNameUnique(ColorProofInstance.CoZoneName, Context);
						if (!isUniqueCoZoneName)
						{
							success = false;
							ModelState.AddModelError("ColorProofInstance.CoZoneName",
													 Resources.Resources.reqColorProofInstanceUniqueName);
						}

						if (!SettingsBL.IsValidPairingCode(ColorProofInstance.PairingCode,
														   GMGColorConfiguration.AppConfiguration
																				.ColorProofPairingCodeRegion, Context))
						{
							success = false;
							ModelState.AddModelError("ColorProofInstance.PairingCode",
													 Resources.Resources.errInvalidPairingCode);
						}

						if (success)
						{
							int? colorProofInstanceId = SaveColorProofInstance(ColorProofInstance, Context);
							if (!colorProofInstanceId.HasValue)
							{
								success = false;
								ModelState.AddModelError(string.Empty,
														 Resources.Resources.DeliverSettingsCouldNotAddCPInstance);
							}
						}
					}
				}
			}

			if (!success)
			{
				return View("AddEditColorProofInstance", new ColorProofSettings.ColorProofInstances(LoggedAccount, Context) { ColorProofInstance = ColorProofInstance });
			}

			return RedirectToAction("DeliverSettings",
				new { type = Enums.DeliverSettingsType.ColorProofInstances, page });

		}

		[HttpGet]
		public ActionResult AddEditNotificationPreset(int? SelectedInstance, int? page)
		{
			try
			{
				NotificationPresets model = new NotificationPresets(Context);
				model.page = page;

				if (SelectedInstance.HasValue && SelectedInstance.Value > 0)
				{
					if (!DALUtils.IsFromCurrentAccount<AccountNotificationPreset>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					AccountNotificationPreset accountNotificationPreset = GlobalNotificationBL.GetAccountNotificationPreset(SelectedInstance.Value, Context);
					if (accountNotificationPreset != null)
					{
						model.NotificationPreset.ID = SelectedInstance.Value;
						model.NotificationPreset.Name = accountNotificationPreset.Name;
						model.NotificationPreset.DisablePersonalNotifications = accountNotificationPreset.DisablePersonalNotifications;

						//Get selected events for current preset
						List<int> events = GlobalNotificationBL.GetEventsIdsByNotificationPresetID(accountNotificationPreset.ID, Context);
                       
						//Set to false those events that were not selected for current preset
						foreach (var eventType in from module in model.NotificationPreset.NotificationModules
												  from @group in module.EventGroups
												  from eventType in @group.EventTypes
												  where !events.Contains(eventType.ID)
												  select eventType)
						{
							eventType.IsChecked = false;
						}
					}
				}

                ViewBag.ChecklistEdited = (int)NotificationType.ChecklistItemEdited + 7;

                return View("AddEditNotificationPreset", model);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
					"BaseController.AddEditNotificationPreset : error occured while populating AddEditNotificationPreset View. {0}", ex.Message);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult AddEditNotificationPreset(NotificationPreset notificationPreset, int? page)
		{
			bool success = true;

			if (notificationPreset.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.AccountNotificationPreset>(notificationPreset.ID, LoggedAccount.ID, Context))
			{
				return RedirectToAction("Unauthorised", "Error");
			}

            if (!CheckIsValidHtml(notificationPreset.Name))
            {
                return RedirectToAction("XSSRequestValidation", "Error");
            }

            if (ModelState.IsValid)
			{
				#region Validations

				if (string.IsNullOrEmpty(notificationPreset.Name))
				{
					success = false;
					ModelState.AddModelError("NotificationPreset.Name", Resources.Resources.reqName);
				}

				var events = new List<NotificationEventType>();
				
				foreach (var eventTypes in from module in notificationPreset.NotificationModules
										   from eventGroup in module.EventGroups
										   select eventGroup.EventTypes)
				{
					events.AddRange(eventTypes);
				}

				if (!events.Any(e => e.IsChecked))
				{
					success = false;
					ModelState.AddModelError("error", Resources.Resources.lblAtleastOneEvent);
				}

				#endregion

				if (success)
				{
					// Edit preset
					if (notificationPreset.ID > 0)
					{
						SettingsBL.MarkNotificationAsDeleted(LoggedAccount.ID, Context, preset: notificationPreset.ID);
						int? accountNotificationPreset = SaveAccountNotificationPreset(notificationPreset, events, Context);

						if (!accountNotificationPreset.HasValue)
						{
							success = false;
							ModelState.AddModelError(string.Empty,
														Resources.Resources.errCouldNotAddNewAccountNotificationPreset);
						}
					}
					else // New preset
					{
						bool isUniquePresetName = AccountNotificationPreset.IsPresetNameUnique(notificationPreset.Name, Context, LoggedAccount.ID);
						if (!isUniquePresetName)
						{
							success = false;
							ModelState.AddModelError("NotificationPreset.Name", Resources.Resources.reqPresetUniqueName);
						}

						if (success)
						{
							int? accountNotificationPreset = SaveAccountNotificationPreset(notificationPreset, events, Context);

							if (!accountNotificationPreset.HasValue)
							{
								success = false;
								ModelState.AddModelError(string.Empty,
															Resources.Resources.errCouldNotAddNewAccountNotificationPreset);
							}
						}
					}
				}
			}

			if (!success)
			{
				return View("AddEditNotificationPreset", new NotificationPresets(Context) { NotificationPreset = notificationPreset });
			}

			return RedirectToAction("GlobalNotifications",
				new { type = Enums.Notification.Presets, page });

		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteAccountNotificationPreset")]
		public ActionResult DeleteAccountNotificationPreset(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue && SelectedInstance.Value > 0)
				{
					if (!DALUtils.IsFromCurrentAccount<AccountNotificationPreset>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					using (var ts = new TransactionScope())
					{
						AccountNotificationPreset objPreset = GlobalNotificationBL.GetAccountNotificationPreset(SelectedInstance.Value, Context);

						if (objPreset != null)
						{
							Context.DeliverSharedJobs.RemoveRange(objPreset.DeliverSharedJobs);
							Context.AccountNotificationPresetEventTypes.RemoveRange(objPreset.AccountNotificationPresetEventTypes);

							//Delete the relation with AccountNotificationPresetEventTypes for this preset
							objPreset.AccountNotificationPresetEventTypes.Clear();

							Context.AccountNotificationPresets.Remove(objPreset);
						}

						Context.SaveChanges();
						ts.Complete();
					}
				}

			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.DeleteAccountNotificationPreset : error occured while deleting a custom preset. {0}", ex.Message);
			}
			return page.HasValue
					   ? RedirectToAction("GlobalNotifications", new { type = Enums.Notification.Presets, page })
					   : RedirectToAction("GlobalNotifications", new { type = Enums.Notification.Presets });
		}

		[HttpGet]
		public JsonResult IsBrandingPresetName_Available(BrandingPreset brandingPreset)
		{
			if (GMGColorDAL.BrandingPreset.IsPresetNameUnique(brandingPreset.Name, brandingPreset.ID, LoggedAccount.ID, Context))
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}

			string suggestedPresetName = String.Format(Resources.Resources.lblNameIsNotAvailable, brandingPreset.Name);

			return Json(suggestedPresetName, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult IsCPUsername_Available(ColorProofSettings.ColorProofInstance ColorProofInstance)
		{
			if (GMGColorDAL.ColorProofInstance.IsCoZoneUsernameUnique(ColorProofInstance.ColorProofUsername,
				ColorProofInstance.ID, Context))
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}

			string suggestedUserName = String.Format("{0} is not available.", ColorProofInstance.ColorProofUsername);

			//create suggestion for possible alternatives
			for (int i = 1; i < 100; i++)
			{
				string altCandidate = ColorProofInstance.ColorProofUsername + i;
				if (GMGColorDAL.ColorProofInstance.IsCoZoneUsernameUnique(altCandidate,
				ColorProofInstance.ID, Context))
				{
					suggestedUserName = String.Format(Resources.Resources.errCPUsernameUnavailable, ColorProofInstance.ColorProofUsername, altCandidate);
					break;
				}
			}
			return Json(suggestedUserName, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult IsPresetName_Available(NotificationPreset NotificationPreset)
		{
			if (GMGColorDAL.AccountNotificationPreset.IsPresetNameUnique(NotificationPreset.Name,
				NotificationPreset.ID, Context, LoggedAccount.ID))
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}

			string suggestedPresetName = String.Format("{0} is not available.", NotificationPreset.Name);

			return Json(suggestedPresetName, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult ActivateColorProofInstance(int? id, int? page)
		{
			if (id.HasValue && id.Value > 0 && !DALUtils.IsFromCurrentAccount<ColorProofInstance>(id.Value, LoggedAccount.ID, Context))
			{
				return RedirectToAction("Unauthorised", "Error");
			}

			try
			{
				using (TransactionScope ts = new TransactionScope())
				{
					GMGColorDAL.ColorProofInstance instanceBO =
						DALUtils.GetObject<GMGColorDAL.ColorProofInstance>(id.Value, Context);

					GMGColorDAL.ColorProofInstanceState stateBo = DALUtils.SearchObjects<GMGColorDAL.ColorProofInstanceState>(o => o.Key == (int)GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Running, Context).FirstOrDefault();
					if (stateBo == null)
						throw new Exception("ColorProofInstanceState Running is not defined into database");


					instanceBO.State = stateBo.ID;
					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.ActivateColorProofInstance : error occured while activating a ColorProof instance. {0}", ex.Message);
			}

			return page.HasValue
					   ? RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances, page = page.Value })
					   : RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "ActivateColorProofWorkflow")]
		public ActionResult ActivateColorProofWorkflow(ColorProofSettings.ColorProofWorkflows model, int? page)
		{
			try
			{
				int selectedInstance = model.SelectedInstance.ToInt().GetValueOrDefault();
				if (selectedInstance > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				using (TransactionScope ts = new TransactionScope())
				{
					GMGColorDAL.ColorProofCoZoneWorkflow workflowBo =
						DALUtils.GetObject<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, Context);

					workflowBo.IsAvailable = true;
					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.ActivateColorProofWorkflow : error occured while activating a ColorProof CoZoneWorkflow. {0}", ex.Message);
			}
			finally
			{
			}
			return page.HasValue
					   ? RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows, page = page.Value })
					   : RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeactivateColorProofWorkflow")]
		public ActionResult DeactivateColorProofWorkflow(ColorProofSettings.ColorProofWorkflows model, int? page)
		{
			try
			{
				int selectedInstance = model.SelectedInstance.ToInt().GetValueOrDefault();
				if (selectedInstance > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				using (TransactionScope ts = new TransactionScope())
				{
					GMGColorDAL.ColorProofCoZoneWorkflow workflowBo =
						DALUtils.GetObject<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, Context);

					workflowBo.IsAvailable = false;
					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.ActivateColorProofWorkflow : error occured while activating a ColorProof CoZoneWorkflow. {0}", ex.Message);
			}
			finally
			{
			}
			return page.HasValue
					   ? RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows, page = page.Value })
					   : RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows });
		}

		[HttpPost]
		public ActionResult CreateNewPairingCode(string code, int instanceId)
		{
			try
			{
				if (instanceId > 0 && !DALUtils.IsFromCurrentAccount<ColorProofInstance>(instanceId, LoggedAccount.ID, Context))
				{
					throw new Exception("Unauthorized call: instace is not part of current user account");
				}

				if (!SettingsBL.IsValidPairingCode(code, GMGColorConfiguration.AppConfiguration.ColorProofPairingCodeRegion, Context))
				{
					throw new Exception("Invalid pairing code");
				}

				var objColorProofInstance = DeliverBL.GetColorProofInstanceByID(instanceId, Context);

				using (var ts = new TransactionScope())
				{
					objColorProofInstance.PairingCode = code;
					objColorProofInstance.PairingCodeExpirationDate = SettingsBL.GetExpirationTime();
					objColorProofInstance.State = DeliverBL.GetColorProofInstanceStateID(ColorProofInstance.ColorProofInstanceStateEnum.Pending, Context);
					string password;
					SettingsBL.GenerateInstancePassword(out password);
					objColorProofInstance.Password = ColorProofInstance.GetEncryptedPassword(password, Context);

					Context.SaveChanges();
					ts.Complete();
				}

				NotificationServiceBL.CreateNotification(new NewDeliverPairingCode
																{
																	EventCreator = LoggedUser.ID,
																	InternalRecipient = null,
																	CreatedDate = DateTime.UtcNow,
																	PairingCode = code,
																	CPInstanceName = objColorProofInstance.SystemName,
																	IsNewCPInstance = false,
																	ReceiverEmail = objColorProofInstance.Email
																},
																null,
																Context
																);

				return Json(
					new
					{
						Status = 400,
						Content = ""
					},
					JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, "Error occured in SettingsController.CreateNewPairingCode method: {0}", ex.Message);

				return Json(
					   new
					   {
						   Status = 300,
						   Content = Resources.Resources.lblFailedCreateNewPairingCode
					   },
					   JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public ActionResult DeactivateColorProofInstance(int? id, int? page)
		{
			try
			{
				if (id.HasValue && id.Value > 0 && !DALUtils.IsFromCurrentAccount<ColorProofInstance>(id.Value, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				using (TransactionScope ts = new TransactionScope())
				{
					ColorProofInstance instanceBO = DALUtils.GetObject<ColorProofInstance>(id.Value, Context);

					GMGColorDAL.ColorProofInstanceState stateBo =
						DALUtils.SearchObjects<GMGColorDAL.ColorProofInstanceState>(o => o.Key == (int)ColorProofInstance.ColorProofInstanceStateEnum.Stopped, Context).FirstOrDefault();

					if (stateBo == null)
						throw new Exception("ColorProofInstanceState Sttopped is not defined into database");

					instanceBO.State = stateBo.ID;

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.ActivateColorProofInstance : error occured while activating a ColorProof instance. {0}", ex.Message);
			}

			return page.HasValue
					   ? RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances, page = page.Value })
					   : RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteColorProofWorkflow")]
		public ActionResult DeleteColorProofWorkflow(ColorProofSettings.ColorProofWorkflows model, int? page)
		{
			try
			{
				int selectedInstance = model.SelectedInstance.ToInt().GetValueOrDefault();
				if (selectedInstance > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				using (TransactionScope ts = new TransactionScope())
				{
					GMGColorDAL.ColorProofCoZoneWorkflow workflowBo =
						DALUtils.GetObject<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, Context);

					bool exists = workflowBo.DeliverJobs.Count > 0;

					// delete relation with user groups
					List<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup> groupsList =
							DALUtils.SearchObjects<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(
								o => o.ColorProofCoZoneWorkflow == workflowBo.ID, Context).ToList();
					foreach (GMGColorDAL.ColorProofCoZoneWorkflowUserGroup userGroupBo in groupsList)
					{
						DALUtils.Delete<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(Context, userGroupBo);
					}

					foreach (SharedColorProofWorkflow shwf in workflowBo.SharedColorProofWorkflows.ToList())
					{
						Context.SharedColorProofWorkflows.Remove(shwf);
					}

					if (exists)
					{
						List<Dictionary<string, string>> dictList = Utils.CancelWorkFlowJobs(workflowBo, workflowBo.ColorProofWorkflow1.ColorProofInstance1.UserName, workflowBo.ColorProofWorkflow1.ColorProofInstance1.Password, workflowBo.ColorProofWorkflow1.ColorProofInstance1.Guid);
						foreach (Dictionary<string, string> dict in dictList)
						{
							GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.DeliverJob);
						}

						workflowBo.DeliverJobs.Where(n => n.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.Cancelled
																					&& n.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.CompletedDeleted
																					&& n.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.Completed
																				).ToList().ForEach(p => p.IsCancelRequested = true);
						workflowBo.IsDeleted = true;
					}
					else
					{
						workflowBo = DALUtils.GetObject<GMGColorDAL.ColorProofCoZoneWorkflow>(selectedInstance, Context);
						DALUtils.Delete<GMGColorDAL.ColorProofCoZoneWorkflow>(Context, workflowBo);
					}
					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"BaseController.DeleteColorProofWorkflow : error occured while deleting a ColorProof CoZone Workflow. {0}", ex.Message);
			}

			return RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows, page });
		}

		[HttpGet]
		public ActionResult AddEditColorProofWorkflow(int? SelectedWorkflow, int? page, bool? isFromCurrentAccount)
		{

			var model = new ColorProofSettings.ColorProofWorkflow(LoggedAccount, SelectedWorkflow.GetValueOrDefault(), Context);
			model.page = page;

			if (SelectedWorkflow != null)
			{
				if (SelectedWorkflow.Value > 0 &&
					(isFromCurrentAccount.Value && !DALUtils.IsFromCurrentAccount<ColorProofCoZoneWorkflow>(SelectedWorkflow.Value, LoggedAccount.ID, Context) ||
					!isFromCurrentAccount.Value && !DeliverBL.IsWorkflowSharedWithAccount(SelectedWorkflow.Value, LoggedAccount.ID, Context)))
				{
					return RedirectToAction("Unauthorised", "Error");
				}
				if (SelectedWorkflow.Value > 0)
				{
					ColorProofWorkflowInstanceView workflowBo =
						DALUtils.SearchObjects<ColorProofWorkflowInstanceView>(
							o => o.ColorProofCoZoneWorkflowID == SelectedWorkflow.Value, Context).FirstOrDefault();
					if (workflowBo != null)
					{
						model.ID = SelectedWorkflow.Value;
						model.Name = workflowBo.CoZoneName;

						model.SelectedCPServerId = workflowBo.objColorProofInstance(Context).ID;
						model.ListServers =
							DALUtils.SearchObjects<ColorProofInstance>(
								o => o.ID == model.SelectedCPServerId, Context)
									.OrderBy(o => o.SystemName)
									.ToList();

						model.SelectedCPWorkflowId = workflowBo.ColorProofWorkflowID;
						model.ListWorkflows =
							DALUtils.SearchObjects<ColorProofWorkflow>(
								o => o.ID == model.SelectedCPWorkflowId, Context).ToList();

						model.IsAvailable = workflowBo.IsAvailable;
						model.ProofStandard = workflowBo.ProofStandard;
						model.MaximumUsablePaperWidth =
							workflowBo.MaximumUsablePaperWidth.GetValueOrDefault();
						model.IsInlineProofVerificationSupported =
							workflowBo.IsInlineProofVerificationSupported.GetValueOrDefault();
						model.SupportedSpotColors = workflowBo.SupportedSpotColorsForWeb;
						model.TransmissionTimeout = workflowBo.TransmissionTimeout;
						model.SelectedWorkflow = SelectedWorkflow.Value;

						//Determine if it's a shared cp wf or a owned one
						ViewBag.IsFromCurrentAccount = isFromCurrentAccount.GetValueOrDefault();
						ViewBag.IsWorkflowShared = DeliverBL.IsWorkflowShared(SelectedWorkflow.Value, Context);
						if (ViewBag.IsWorkflowShared && !ViewBag.IsFromCurrentAccount)
						{
							ViewBag.SharedOwnerInfo = DeliverBL.GetSharedWorkflowOwnerInfo(SelectedWorkflow.Value, LoggedAccount.ID, Context);
						}
						ViewBag.IsCPWorkflowActivated = workflowBo.IsActivated;
					}
				}
			}
			else
			{
				model.TransmissionTimeout = 1;
			}

			return View("AddEditColorProofWorkflows", model);
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SaveWorkflowChanges")]
		public ActionResult AddEditColorProofWorkflow(ColorProofSettings.ColorProofWorkflow WorkflowInstance, int? page)
		{
			bool success = true;

			//Determine if it is an owned workflow or a shared one
			bool isOwner = WorkflowInstance.ID <= 0 || DeliverBL.IsWorkflowOwner(WorkflowInstance.ID, LoggedAccount.ID, Context);
			//Check authorization
			if (WorkflowInstance.ID > 0 && (isOwner && !DALUtils.IsFromCurrentAccount<ColorProofCoZoneWorkflow>(WorkflowInstance.ID, LoggedAccount.ID, Context) ||
											!isOwner && !DeliverBL.IsWorkflowSharedWithAccount(WorkflowInstance.ID, LoggedAccount.ID, Context)))
			{
				return RedirectToAction("Unauthorised", "Error");
			}

			//Check model state only if the user is owner, shared colorProof wf can't be edited
			if (!isOwner || ModelState.IsValid)
			{
				if (isOwner)
				{
					bool isUniqueWorkflowName =
						ColorProofWorkflowInstanceView.IsWorkflowNameUnique(WorkflowInstance.Name,
							WorkflowInstance.SelectedCPServerId.GetValueOrDefault(), WorkflowInstance.ID, Context);
					if (!isUniqueWorkflowName)
					{
						success = false;
						ModelState.AddModelError("Name", Resources.Resources.
							reqColorProofWorkflowUniqueName
							);
					}
				}

				if (success)
				{
					int? instanceId = SaveColorProofWorkflow(WorkflowInstance, isOwner, Context);
					if (!instanceId.HasValue)
					{
						ModelState.AddModelError(string.Empty, Resources.Resources.errEditExistingWf);
						success = false;
					}
				}
			}
			else
			{
				success = false;
			}

			if (!success)
			{
				var model = new ColorProofSettings.ColorProofWorkflow(LoggedAccount, WorkflowInstance.ID, Context)
					{
						page = page
					};

				model.SelectedCPServerId = WorkflowInstance.SelectedCPServerId;
				model.SelectedCPWorkflowId = WorkflowInstance.SelectedCPWorkflowId;
				if (WorkflowInstance.ID > 0)
				{
					model.SelectedWorkflow = WorkflowInstance.ID;
				}

                var selectedCPServerIdValue = WorkflowInstance.SelectedCPServerId.GetValueOrDefault();

                GMGColorDAL.ColorProofInstance instance =
			    DALUtils.SearchObjects<GMGColorDAL.ColorProofInstance>(o => o.ID == selectedCPServerIdValue, Context).FirstOrDefault();

				// filter only the activated workflows
				model.ListWorkflows = instance != null
									? instance.ColorProofWorkflows.Where(o => o.IsActivated == true).ToList()
									: new List<GMGColorDAL.ColorProofWorkflow>();

				ViewBag.IsWorkflowShared = !isOwner;
				ViewBag.IsFromCurrentAccount = isOwner;
				return View("AddEditColorProofWorkflows", model);
			}

			return RedirectToAction("DeliverSettings",
				new { type = Enums.DeliverSettingsType.ColorProofWorkflows, page });
		}

		public JsonResult GetWorkflows(int? selectedCPServerId)
		{
            var selectedCPServerIdValue = selectedCPServerId.GetValueOrDefault();
            GMGColorDAL.ColorProofInstance instance =
				DALUtils.SearchObjects<GMGColorDAL.ColorProofInstance>(o => o.ID == selectedCPServerIdValue, Context).FirstOrDefault();

			// filter only the activated workflows
			List<GMGColorDAL.ColorProofWorkflow> lst = instance != null
												 ? instance.ColorProofWorkflows.Where(o => o.IsActivated == true).ToList()
												 : new List<GMGColorDAL.ColorProofWorkflow>();

			return
				Json(
					new
					{
						workflows =
					lst.Select(
						t =>
						new
						{
							ID = t.ID,
							Name = t.Name,
							ProofStandard = string.IsNullOrEmpty(t.ProofStandard) ? "&nbsp;" : t.ProofStandard,
							SpotColors = HttpUtility.HtmlEncode(t.SupportedSpotColorsForWeb),
							MaxPaperWidth =
						t.MaximumUsablePaperWidth.HasValue ? t.MaximumUsablePaperWidth.ToString() : "&nbsp;",
							InlineProof =
						t.IsInlineProofVerificationSupported.GetValueOrDefault() ? Resources.Resources.lblColorProofWorkflowInlineProofEnabled : Resources.Resources.lblColorProofWorkflowInlineProofDisabled
						})
					});
		}

		[HttpGet]
		public ActionResult Users()
		{
			AccountSettings.Users model = new AccountSettings.Users(this.LoggedAccount, Context);

			var userSeting = UserBL.GetUserSetting(LoggedUser.ID, GMGColorConstants.ShowAllUsers, Context);
			if (userSeting != null)
			{
				model.objUsers.ShowAllUsers = Boolean.Parse(userSeting.Value);
			}

			if (this.LoggedAccount.NeedSubscriptionPlan == false)
			{
				model.objUsers.ListUsers = model.objUsers.ListUsers.Where(o => o.UserStatus != "I").ToList();
			}
			if (TempData[UsersSearchText] != null)
			{
				string s = TempData[UsersSearchText].ToString();
				string searchString = s.ToLower();
				TempData[UsersSearchText] = null;
				
				model.objUsers.SearchText = s;
				model.objUsers.ListUsers =
					model.objUsers.ListUsers.Where(
						o =>
							o.FamilyName.ToLower().Contains(searchString) || o.GivenName.ToLower().Contains(searchString) ||
							o.Username.ToLower().Contains(searchString) ||
							(o.FamilyName + o.GivenName).ToLower().Trim().Contains(searchString.Trim().Replace(" ", "")) ||
							(o.GivenName + o.FamilyName).ToLower().Trim().Contains(searchString.Trim().Replace(" ", "")) ||
                            (o.EmailAddress).ToLower().Trim().Contains(searchString.Trim().Replace(" ", ""))).ToList();
			}

			if (TempData[InternalUsersErrors] is Dictionary<string, List<string>>)
			{
				Dictionary<string, List<string>> dict = TempData[InternalUsersErrors] as Dictionary<string, List<String>>;
				dict.Keys.ToList().ForEach(k => dict[k].ForEach(m => ModelState.AddModelError(k, m)));
			}

			if (TempData["DuplicateUserErrors"] != null)
			{
				Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["DuplicateUserErrors"];
				TempData["DuplicateUserErrors"] = null;

				foreach (KeyValuePair<string, string> item in dicModelErrors)
				{
					ModelState.AddModelError(item.Key, item.Value);
				}

				ViewBag.ModelDialog = "modalDuplicateUser";
				ViewBag.DuplicateUserModel = TempData["DuplicateUserModel"];
			}
			else
			{
				ViewBag.DuplicateUserModel = new DuplicateUser();
			}

			ViewBag.LoggedAccountType = LoggedAccountType;

			return View("Users", model);
		}

		[HttpGet]
		public ActionResult AddEditUser(int? SelectedUser)
		{
			var model = new AccountSettings.AddEditUser(LoggedAccount, Context);
            ViewBag.AccountHasSsoEnabled = LoggedAccountHasSsoEnabled;

            if (SelectedUser != null)
			{

				if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(SelectedUser.Value, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				if (TempData["editUserModel"] != null)
				{
					model.objUsers.objEditUser = (UserInfo)TempData["editUserModel"];
				}
				else
				{
					model.objUsers.objEditUser = new UserInfo(SelectedUser.Value, this.LoggedAccount.ID, Context);
					model.objUsers.objEditUser.LoggedAccountId = this.LoggedAccount.ID;
					model.objUsers.objEditUser.Locale = model.objUsers.objEditUser.objUser.Locale;
					model.objUsers.objEditUser.Controller = "Settings";
				}

				if (this.LoggedUser.ID == SelectedUser.Value || LoggedAccount.Owner == SelectedUser.Value)
					model.objUsers.objEditUser.ModulePermisions.ForEach(o => o.ReadOnly = true);

				
				ProofStudioUserColor color = Context.ProofStudioUserColors.Where(c => c.ID == model.objUsers.objEditUser.ProofStudioColorID).FirstOrDefault();
				PopulateProofStudioUserColors(color, model.objUsers.objEditUser.objUser.ID);
			}
			else
			{
				if (TempData["newUserModel"] != null)
				{
					model.objUsers.objNewUser = (UserInfo)TempData["newUserModel"];
				}
				else
				{
					model.objUsers.objNewUser = new UserInfo(LoggedAccount.ID, LoggedAccount.ID, Context, true);
					model.objUsers.objNewUser.LoggedAccountId = this.LoggedAccount.ID;
					model.objUsers.objNewUser.Controller = "Settings";
				}

				ProofStudioUserColor color = UserBL.GetAvailableUserColor(model.objUsers.objNewUser.objUser.Account, Context);
				PopulateProofStudioUserColors(color, model.objUsers.objNewUser.objUser.ID);
			}

			if (TempData["ModelErrors"] != null)
			{
				Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
				TempData["ModelErrors"] = null;

				foreach (KeyValuePair<string, string> item in dicModelErrors)
				{
					ModelState.AddModelError(item.Key, item.Value);
				}
			}

			return View("AddEditUser", model);
		}

		[HttpGet]
		public ActionResult UserGroups()
		{
			AccountSettings.UserGroups model = new AccountSettings.UserGroups(this.LoggedAccount, Context);
			if (TempData[SUserGroupsErrors] is Dictionary<string, List<string>>)
			{
				Dictionary<string, List<string>> dict = TempData[SUserGroupsErrors] as Dictionary<string, List<String>>;
				dict.Keys.ToList().ForEach(k => dict[k].ForEach(m => ModelState.AddModelError(k, m)));
			}
			return View("UserGroups", model);
		}

		[HttpGet]
		public ActionResult AddEditUserGroup()
		{
			AccountSettings.UserGroups model = new AccountSettings.UserGroups(this.LoggedAccount, Context);
			if (Session["GMGC_GroupID"] != null)
			{
				int group = int.Parse(Session["GMGC_GroupID"].ToString());
				model.UserGroupID = group;
			}

			return View("AddEditUserGroup", model);
		}

		[HttpGet]
		public ActionResult ExternalUsers(Enums.ExternalUserType? type, int? page)
		{
			ViewResult result;
			if (type.HasValue)
			{
				switch (type.Value)
				{
					case Enums.ExternalUserType.Deliver:
						{
							AccountSettings.DeliverExternalUsers model = new AccountSettings.DeliverExternalUsers(this.LoggedAccount, Context);

							if (TempData[ExternalUsersSearchText] != null)
							{
								string s = TempData[ExternalUsersSearchText].ToString();
								TempData[ExternalUsersSearchText] = null;

								model.SearchText = s;
								model.ListDeliverExternalUsers = model.ListDeliverExternalUsers.Where(o => o.Name.ToLower().Contains(s) || o.Email.Contains(s)).ToList();
							}
						   
							result = View("DeliverExternalUsers", model);
							break;
						}
					default:
						{
							AccountSettings.ExternalUsers model = new AccountSettings.ExternalUsers(this.LoggedAccount, Context);

							if (TempData[ExternalUsersSearchText] != null)
							{
								string s = TempData[ExternalUsersSearchText].ToString();
								TempData[ExternalUsersSearchText] = null;

								model.SearchText = s;
								model.ListExternalUsers = model.ListExternalUsers.Where(o => o.Name.ToLower().Contains(s) || o.Email.Contains(s)).ToList();
							}

							if (TempData[ExternalUsersErrors] is Dictionary<string, List<string>>)
							{
								Dictionary<string, List<string>> dict = TempData[ExternalUsersErrors] as Dictionary<string, List<String>>;
								dict.Keys.ToList().ForEach(k => dict[k].ForEach(m => ModelState.AddModelError(k, m)));
							}

							result = View("CollaborateExternalUsers", model);
							break;
						}
				}
			}
			else
			{
				if (TempData[ExternalUsersErrors] is Dictionary<string, List<string>>)
				{
					Dictionary<string, List<string>> dict = TempData[ExternalUsersErrors] as Dictionary<string, List<String>>;
					dict.Keys.ToList().ForEach(k => dict[k].ForEach(m => ModelState.AddModelError(k, m)));
				}

				AccountSettings.ExternalUsers model = new AccountSettings.ExternalUsers(this.LoggedAccount, Context);
				result = View("CollaborateExternalUsers", model);
			}
			return result;
		}

		[HttpGet]
		public ActionResult EditExternalCollaborator(int id, int? page, Enums.ExternalUserType userType)
		{
			ExternalUserDetails extUser = new ExternalUserDetails(id, userType, Context);
			ViewBag.page = page;

			if (userType == Enums.ExternalUserType.Collaborate)
			{
				return View("EditColExtCollaborators", extUser);
			}
			else
			{
				return View("EditDeliverExtCollaborators", extUser);
			}
		}

		[HttpGet]
		public ActionResult ReservedDomains()
		{
			AccountSettings.ReservedDomains model;

			if (Session["SelectedDomain"] == null)
			{
				ViewBag.ModelDialog = null;
				model = new AccountSettings.ReservedDomains(Context);
			}
			else
			{
				int selectedDomain = int.Parse(Session["SelectedDomain"].ToString());
				Session["SelectedDomain"] = null;

				ModelState.Clear();
				ViewBag.ModelDialog = "modalDialogAddNewDomain";
				model = new AccountSettings.ReservedDomains(selectedDomain, Context);
			}

			if (TempData["searchText"] != null)
			{
				string s = TempData["searchText"].ToString();
				TempData["searchText"] = null;

				model.SearchText = s;
				model.ListReservedDomains = model.ListReservedDomains.Where(o => o.Name.ToLower().Contains(s)).ToList();
			}

			return View("ReservedDomains", model);
		}

		[HttpGet]
		public ActionResult Global()
		{
			GMGColorDAL.CustomModels.Settings model = new GMGColorDAL.CustomModels.Settings(Context);
			return View("GlobalSettings", model);
		}

		[HttpGet]
		public ActionResult GlobalSettings()
		{
			GMGColorDAL.CustomModels.Settings model = new GMGColorDAL.CustomModels.Settings(Context);
			return View("GlobalSettings", model);
		}

		[HttpGet]
		public ActionResult CustomSmtpServer()
		{
			SmtpSettingsViewModel model = new SmtpSettingsViewModel();
			try
			{
				model = _mapper.Map<SmtpSettingsModel, SmtpSettingsViewModel>(_settingsService.SmtpSettings.Get(LoggedAccount.ID));
				model.Password = string.Empty;
				model.ConfirmPassword = string.Empty;
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "An error occured wille retrieving Custom Smtp settings", ex.Message);
			}
			return View(model);
		}

		[HttpGet]
		public ActionResult SingleSignOn()
		{
			var model = new SsoViewModel();
			ViewBag.AccountRegion = LoggedAccount.Region;
			ViewBag.PathToTempFolder = LoggedUserTempFolderPath;

			try
			{
				model = _mapper.Map<SsoModel, SsoViewModel>(_settingsService.SsoSettings.Get(LoggedAccount.ID));
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "An error occured wille retrieving Single Sign On settings", ex.Message);
			}
			return View(model);
		}

		[HttpGet]
		public ActionResult TestCustomSmtpServer(string testSettings)
		{
			var testDataObject = JsonConvert.DeserializeObject<SmtpSettingsViewModel>(testSettings);

			var result = _settingsService.SmtpSettings.TestConnection(new CustomSmtpTestConnectionModel
			{
				MessageGuid = Guid.NewGuid().ToString(),
				AccountId = LoggedAccount.ID,
				TestData =
				{
					Host = testDataObject.ServerName,
					Port = testDataObject.Port.Value,
					EnabledSsl = testDataObject.EnableSsl,
					Password = 	testDataObject.IsPasswordEnabled ? testDataObject.Password : GMG.CoZone.Common.Utils.Decrypt(_settingsService.SmtpSettings.GetPassword(LoggedAccount.ID), LoggedAccount.Guid),
					RecipientEmail = LoggedUser.EmailAddress,
					SenderEmail = testDataObject.EmailAddress,
					EmailAccountData =
					{
						EmailLogoPath = Account.GetLogoPath(LoggedAccount.ID, Account.LogoType.EmailLogo, Context, true),
						UserLogoPath = GMGColorDAL.User.GetImagePath(LoggedUser.ID, Context), 
						Domain = LoggedAccount.Domain,
						AccountName = LoggedAccount.Name,
						IsSecureConnection = HttpContext.Request.IsSecureConnection
					}
				}
			});

			if (string.IsNullOrEmpty(result))
			{
				return Json(new
				{
					Success = true,
					Errors = string.Empty
				}, JsonRequestBehavior.AllowGet);
			}
			else
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "Test SMTP connection failed with the folowing error: " + result, new Exception());
				return Json(new
				{
					Success = false,
					Errors = result
				}, JsonRequestBehavior.AllowGet);
			}
		}

        public  static void AddPredefinedtagwordsFromFile(string destinationPath, string accountRegion )
        {
            bool fileExists = GMGColorIO.FileExists(destinationPath, accountRegion);
            if (fileExists)
            {
                var values = destinationPath.Split('/');
                var path = values[2];
                var Ids = path.Split('-');
                var userid = Int32.Parse( Ids[0]);
                var accountid = Int32.Parse(Ids[1]);

                var CompleteDestinationPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + destinationPath;

                PredefinedtagwordsBL.AddPredefinedTagwordsFromFile(CompleteDestinationPath, userid, accountid);
                GMGColorIO.DeleteFile(destinationPath, accountRegion);
            }
        }

        [HttpGet]
        public ActionResult Predefinedtagwords()
        {
            var model = new GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsOnDashboard();

            if (TempData[SearchPredefinedTagwords] is string)
            {
                model.SearchText = TempData[SearchPredefinedTagwords] as string;
                model.predefinedTagwords = PredefinedtagwordsBL.GetPredefineTagwords(LoggedAccount.ID, Context, model.SearchText);
            }
            else
            {
                model.predefinedTagwords = PredefinedtagwordsBL.GetPredefineTagwords(LoggedAccount.ID, Context);
                model.region = LoggedAccount.Region;
                model.AcceptedFiles = "/(XLS)$/i";
                model.PathToTempFolder = "temp/tagwords/" + LoggedUser.ID.ToString() + "-" + LoggedAccount.ID.ToString() + "/";
            }

            return View("PredefinedTagwords", model);
        }

        [HttpGet]
        public ActionResult CheckLists()
        {
            var model = new ChecklistDashboardModel { Checklist = new ChecklistModel() };
            if (TempData[SearchChecklists] is string)
            {
                model.SearchText = TempData[SearchChecklists] as string;
                model.Checklists = ChecklistBL.GetCheckLists(LoggedAccount.ID, Context, model.SearchText);
            }
            else
            {
                model.Checklists = ChecklistBL.GetCheckLists(LoggedAccount.ID, Context);
            }

            return View("Checklist", model);
        }

        [HttpPost]
        public ActionResult AddPredefinedtagwords(GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsOnDashboard model)
        {
            try
            {
                if (!CheckIsValidHtml(model.predefinedtagword.tagword))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                PredefinedtagwordsBL.AddNewPredefinedtagword(model.predefinedtagword.tagword, LoggedAccount.ID, LoggedUser.ID, Context, GMGColorDAL.CustomModels.PredefinedTagwords.TypeOfTagword.PredefinedTagword);
               
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Settings.AddPredefinedtagwords : Add  Predefinedtagwords Failed. {0}", ex.Message);
            }
            return RedirectToAction("Predefinedtagwords");
        }

		      
        [MultiButton(MatchFormKey ="action", MatchFormValue = "DownloadPredefineTagwords")]
        public ActionResult DownloadPredefineTagwords()
        {
            string csvFileName = String.Empty;
            MemoryStream output = PredefinedtagwordsBL.CreatePredefineTagwordsReportCSV(LoggedAccount.ID, out csvFileName, Context);
            output.Seek(0, SeekOrigin.Begin);
            return File(output, "application/vnd.ms-excel", csvFileName);
            
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchPredefinedTagwords")]
        public ActionResult Search(GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsOnDashboard model)
        {
            string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
            TempData[SearchPredefinedTagwords] = s;

            return RedirectToAction("Predefinedtagwords");
        }

        [HttpPost]
        public ActionResult AddEditChecklist(ChecklistDashboardModel model, int? page)
        {
            try
            {
                #region Validations

                if (model.Checklist.ID > 0 && !DALUtils.IsFromCurrentAccount<CheckList>(model.Checklist.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (!CheckIsValidHtml(model.Checklist.Name))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                #endregion

                #region Add/Update Approval Workflow

                ChecklistBL.AddOrUpdateCheckList(model.Checklist, LoggedAccount.ID, LoggedUser.ID, Context);
                Context.SaveChanges();

                #endregion

                ViewBag.Page = page;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Settings.AddEditChecklist : Add or Edit Checklist Failed. {0}", ex.Message);
            }

            return RedirectToAction("CheckLists", new { page = page });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeletePredefinedTagword")]
        public ActionResult DeletePredefinedTagword(int? selectedPredefineTagwordId)
        {
            try
            {
                if (selectedPredefineTagwordId.HasValue && !DALUtils.IsFromCurrentAccount<PredefinedTagword>(selectedPredefineTagwordId.Value, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                var PredefinedTagword = Context.PredefinedTagwords.FirstOrDefault(cl => cl.ID == selectedPredefineTagwordId.Value);
                if (PredefinedTagword != null)
                {
                    bool noActiveApproval = PredefinedtagwordsBL.CheckIfAnyActiveApprovalForTagword((int)selectedPredefineTagwordId, LoggedAccount.ID, Context);
                    bool noActiveProjectFolder = ProjectFolderBL.CheckIfAnyActiveProjectFolderForTagword((int)selectedPredefineTagwordId, LoggedAccount.ID, Context);
                    if (noActiveApproval && noActiveProjectFolder)
                    {
                        Context.PredefinedTagwords.Remove(PredefinedTagword);
                    }
                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "SettingsController.DeletePredefineTagword : Delete predefinetagword failed. {0}", ex.Message);
            }
            return RedirectToAction("Predefinedtagwords");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteChecklist")]
        public ActionResult DeleteChecklist(int? selectedChecklistId)
        {
            try
            {
                if (selectedChecklistId.HasValue && !DALUtils.IsFromCurrentAccount<CheckList>(selectedChecklistId.Value, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                var checklist = Context.CheckList.FirstOrDefault(cl => cl.ID == selectedChecklistId.Value);
                if (checklist != null)
                {
                    checklist.IsDeleted = true;
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "SettingsController.DeleteChecklist : Deletet checklist failed. {0}", ex.Message);
            }
            return RedirectToAction("CheckLists");
        }

        //Delete ChecklistItem
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteChecklistItem")]
        public ActionResult DeleteChecklistItem(int selectedItem)
        {
            try
            {
                #region Validations

                if (selectedItem > 0 && !DALUtils.IsFromCurrentAccount<CheckListItem>(selectedItem, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                #endregion

                #region Delete checklist item

                ChecklistBL.DeleteCheckListItem(selectedItem, Context);
                Context.SaveChanges();

                #endregion
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Settings.DeleteChecklistItem : Delete Checklist  Item Failed. {0}", ex.Message);
            }

            return RedirectToAction("CheckLists");
        }

        [HttpGet]
        public ActionResult GetChecklistsItems(int? checklistId)
        {
            try
            {
                if (checklistId != null)
                {
                    #region Validations

                    if (checklistId.Value > 0 && !DALUtils.IsFromCurrentAccount<CheckList>(checklistId.Value, LoggedAccount.ID, Context))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    #endregion

                    ChecklistItemsDashboardModel checklistsItemes = ChecklistBL.GetChecklistsItemesByID(checklistId.Value, LoggedAccount.ID, Context);
                    return Json(
                        new
                        {
                            Status = 200,
                            Content = RenderViewToString(this, "ChecklistItem", LoggedUser.ID, checklistsItemes)
                        },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
                   "Settings.GetChecklistsItems : error return creating json result. {0} StackTrace {1}", ex.Message, ex.StackTrace);
            }
            return Json(
                    new
                    {
                        Status = 300,
                        Content = Resources.Resources.btnActivate
                    },
                    JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult AddEditChecklistItem(ChecklistItemViewModel model)
        {
            bool success = ModelState.IsValid;

            try
            {
                #region Validations

                if (model.ID > 0 && !DALUtils.IsFromCurrentAccount<CheckListItem>(model.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (!CheckIsValidHtml(model.ItemName))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                if (!success)
                {

                    model.Checklists = ChecklistBL.GetCheckLists(LoggedAccount.ID, Context);
                    model.LoggedAccount = LoggedAccount;
                    return View("AddEditChecklistItem", model);
                }

                #endregion

                #region Save/Update approval phase

                ChecklistBL.AddOrUpdateChecklistItem(model, LoggedAccount, LoggedUser.ID, Context);

                #endregion
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Settings.AddEditChecklistItem : Add or Edit Checklist Item Failed. {0}", ex.Message);
            }

            return RedirectToAction("CheckLists");
        }

        [HttpGet]
        public ActionResult AddEditChecklistItem(int? selectedChecklist, int? selectedItem, bool copyItem = false)
        {
            var model = new ChecklistItemViewModel { Checklist = selectedChecklist.GetValueOrDefault() };
            if (selectedItem.HasValue)
            {
                model = ChecklistBL.GetCheckListItemDetails(LoggedAccount, selectedItem.Value, Context);

                model.ID = selectedItem.Value;
            }
            else
            {
                ChecklistBL.PopulateStaticData(model, LoggedAccount, Context);
            }
            model.LoggedAccount = LoggedAccount;

            return View("AddEditChecklistItem", model);
        }

        [HttpGet]
        public JsonResult IsCheckListName_Available(ChecklistDashboardModel model)
        {
            var checklist = model.Checklist;
            if (CheckList.IsCheckListNameUnique(LoggedAccount.ID, checklist.Name,
                checklist.ID, Context))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            string infoMsg = String.Format(Resources.Resources.lblNameIsNotAvailable, checklist.Name);
            return Json(infoMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsCheckListItemNameUnique(ChecklistItemViewModel model)
        {
            var checklistId = model.ID > 0 ? model.SelectedChecklist : model.Checklist;
            if (CheckListItem.IsChecklistItemNameUnique(checklistId, model.ItemName, model.ID, Context))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            string infoMsg = String.Format(Resources.Resources.lblNameIsNotAvailable, model.ItemName);

            return Json(infoMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeChecklistItemPosition(int sourceChecklistId, int destinationChecklistId)
        {
            try
            {
                if (!DALUtils.IsFromCurrentAccount<CheckListItem>(new List<int>() { sourceChecklistId, destinationChecklistId }, LoggedAccount.ID, Context))
                {
                    return Json(
                            new
                            {
                                Status = 403
                            },
                            JsonRequestBehavior.AllowGet);
                }

                ChecklistBL.ChangeChecklistItemPosition(sourceChecklistId, destinationChecklistId, Context);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
                   "Settings.ChangeChecklistItemPosition : error return creating json result. {0} StackTrace {1}", ex.Message, ex.StackTrace);

                return Json(
                    new
                    {
                        Status = 300,
                        Content = Resources.Resources.errCannotChangeApprovalPhasePosition
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }


        #region SharedColorProofWorkflows

        [HttpGet]
		public ActionResult GetUsersByEmail(string email, int workflow)
		{
			try
			{
				// TODO - check if logged in user has access to share action

				return Json(
					new
					{
						Status = 400,
						Content = UserBL.GetUsersByEmail(email, workflow, LoggedAccount.ID, Context)
					},
					JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
					"SettingsController.GetUsersByEmail : error occured in GetUsersByEmail. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return Json(
					   new
					   {
						   Status = 300,
						   Content = Resources.Resources.txtNoUsersWithSpecifiedEmail
					   },
					   JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult ShareColorProofWorkflow(int workflow, int[] users)
		{
			try
			{
				if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.ColorProofCoZoneWorkflow>(workflow, LoggedAccount.ID, Context))
				{
					return Json(
					  new
					  {
						  Status = 300,
						  Content = Resources.Resources.errCMSItemSaving
					  },
					  JsonRequestBehavior.AllowGet);
				}
				//Gets shared workflow Name
				string workflowName = DeliverBL.GetWorkflowNameById(workflow, Context);
				//Gets LoggedAccount Company Name 
				string companyName = AccountBL.GetCompanyNameByAccountID(LoggedAccount.ID, Context);

				string mailTitle = Resources.Resources.notificationSharedWorkflowHeader;

				for (int i = 0; i < users.Length; i++)
				{
					int userId = users[i];
					//Do not allow users from same account
					if (
						DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, LoggedAccount.ID,
							Context))
					{
						GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "SettingsController.ShareColorProofWorkflow user {0} not allowd to share with, is from the same account", userId);
						continue;
					}
					try
					{
						DeliverBL.ProcessSharedWorkflow(workflow, LoggedUser, userId, workflowName, companyName, mailTitle, Context);
					}
					catch (ExceptionBL)
					{
						throw;
					}
					catch (Exception ex)
					{
						GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
						"SettingsController.ShareColorProofWorkflow : error occured in ProcessSharedWorkflow {0} with user {1} Error: {2} StackTrace {3}", workflow, userId, ex.Message, ex.StackTrace);
					}
				}

				Context.SaveChanges();

				return Json(
					new
					{
						Status = 400
					},
					JsonRequestBehavior.AllowGet);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw;
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
				   "SettingsController.ShareColorProofWorkflow : error occured in sharing workflow with users. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return Json(
					   new
					   {
						   Status = 300,
						   Content = Resources.Resources.errCMSItemSaving
					   },
					   JsonRequestBehavior.AllowGet);
			}
		}

		[HttpGet]
		public ActionResult AcceptRejectColorProofWorkflow(string token, bool isAccept)
		{
			try
			{
				SharedColorProofWorkflow sharedworkflow = DeliverBL.GetSharedWorkflowByToken(token, Context);
				string sharedMessage = null;
				if (sharedworkflow != null)
				{
					if (sharedworkflow.IsAccepted == null)
					{
						sharedworkflow.IsAccepted = isAccept;
						Context.SaveChanges();

						if (isAccept)
						{
							var workflow = DeliverBL.GetColorProofCoZoneWorkflowName(sharedworkflow.ColorProofCoZoneWorkflow, Context);
							var user = DeliverBL.GetCPWorkflowOwner(sharedworkflow.ColorProofCoZoneWorkflow, Context);

							NotificationServiceBL.CreateNotification(new ShareRequestAccepted()
																		{
																			EventCreator = sharedworkflow.User,
																			InternalRecipient = user,
																			WorkflowName = workflow
																		},
																		LoggedUser,
																		Context
																	);
						}

						sharedMessage = String.Format(Resources.Resources.lblAcceptRejectSharedWorkflow,
						DeliverBL.GetWorkflowNameById(sharedworkflow.ColorProofCoZoneWorkflow, Context),
						isAccept ? Resources.Resources.lblAccepted : Resources.Resources.lblRejected);
					}
					else
					{
						sharedMessage = Resources.Resources.lblSharedWfAlreadyDecided;
					}
				}
				else
				{
					sharedMessage = Resources.Resources.errSharedWorkflow;
				}

				ViewBag.SharedMessage = sharedMessage;
				ViewBag.LoggedAccount = LoggedAccount;

				return View("AcceptRejectSharedWorkflow");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
					"SettingsController.AcceptRejectColorProofWorkflow : Exception {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return View("UnexpectedError", "Error");
			}
		}

		[ChildActionOnly]
		public PartialViewResult GetSharedWorkflowDetails(int workflowID)
		{
			//Get details for workflow owner regarding external shares
			List<ColorProofSettings.SharedColorProofWorkflowDetails> model =
				DeliverBL.GetSharedWorkflowDetails(workflowID, Context);

			return PartialView("SharedWorkflowDetails", model);
		}

		[HttpPost]
		public ActionResult EnableRevokeSharedWorkflow(int sharedId, bool? isEnabled)
		{
			try
			{
				SharedColorProofWorkflow sharedWorkflow = DeliverBL.GetSharedColorProofWorkflowById(sharedId, Context);
				if (sharedWorkflow != null)
				{
					if (
						!DALUtils.IsFromCurrentAccount<ColorProofCoZoneWorkflow>(sharedWorkflow.ColorProofCoZoneWorkflow, LoggedAccount.ID,
							Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					sharedWorkflow.IsEnabled = !isEnabled.GetValueOrDefault();

					bool accountIsActive = true;
					if (isEnabled.GetValueOrDefault())
					{
						//Delete usergroups from the shared workflow account
						List<ColorProofCoZoneWorkflowUserGroup> sharedWfGroups =
							DeliverBL.GetWfUserGroupsListByWorkflowAndAccount(sharedWorkflow.ColorProofCoZoneWorkflow,
								UserBL.GetAccountIdByUser(sharedWorkflow.User, out accountIsActive, Context), Context);

						if (accountIsActive)
						{
							for (int i = 0; i < sharedWfGroups.Count; i++)
							{
								Context.ColorProofCoZoneWorkflowUserGroups.Remove(sharedWfGroups[i]);
							}
						}
					}

					Context.SaveChanges();

					if (!sharedWorkflow.IsEnabled && accountIsActive)
					{
						var user = SharedColorProofWorkflow.GetUserBySharedWorkflowId(sharedWorkflow.ID, Context);
						var workflowName = DeliverBL.GetColorProofCoZoneWorkflowName(sharedWorkflow.ColorProofCoZoneWorkflow, Context);


						NotificationServiceBL.CreateNotification(new SharedWorkflowAccessRevoked()
																		{
																			EventCreator = LoggedUser.ID,
																			InternalRecipient = user.ID,
																			WorkflowName = workflowName
																		},
																		LoggedUser,
																		Context
																	 );

					}

					return RedirectToAction("AddEditColorProofWorkflow", new { SelectedWorkflow = sharedWorkflow.ColorProofCoZoneWorkflow, isFromCurrentAccount = true });
				}
				else
				{
					return RedirectToAction("Unauthorised", "Error");
				}
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
				   "SettingsController.EnableRevokeSharedWorkflow for sharedworkflowid: {0} : Exception {1} StackTrace {2}", sharedId, ex.Message, ex.StackTrace);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult ResendShareWorkflowInvitation(int sharedId)
		{
			try
			{
				SharedColorProofWorkflow sharedWorkflow = DeliverBL.GetSharedColorProofWorkflowById(sharedId, Context);
				if (sharedWorkflow != null)
				{
					if (
						!DALUtils.IsFromCurrentAccount<ColorProofCoZoneWorkflow>(
							sharedWorkflow.ColorProofCoZoneWorkflow, LoggedAccount.ID,
							Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					sharedWorkflow.IsAccepted = null;

					Context.SaveChanges();

					DeliverBL.SendSharedWorkflowInvitation(LoggedUser, sharedWorkflow, Context);

					return RedirectToAction("AddEditColorProofWorkflow",
						new { SelectedWorkflow = sharedWorkflow.ColorProofCoZoneWorkflow, isFromCurrentAccount = true });
				}
				else
				{
					return RedirectToAction("Unauthorised", "Error");
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
				   "SettingsController.ResendShareWorkflowInvitation for sharedworkflowid: {0} : Exception {1} StackTrace {2}", sharedId, ex.Message, ex.StackTrace);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteSharedColorProofWorkflow")]
		public ActionResult DeleteSharedColorProofWorkflow(int? sharedWorkflowId, int? SelectedInstance, bool isOwner)
		{
			try
			{
				//For shares deleted by wf owner the shared workflow id is sent, for shared shares sent from shared account the cz wf id is sent
				SharedColorProofWorkflow sharedWorkflow = null;
				List<SharedColorProofWorkflow> externalShares = null;
				if (isOwner)
				{
					sharedWorkflow = DeliverBL.GetSharedColorProofWorkflowById(sharedWorkflowId.GetValueOrDefault(),
						Context);
				}
				else
				{
					externalShares = DeliverBL.GetSharedWorkflowByWfAndAccount(SelectedInstance.GetValueOrDefault(),
						LoggedAccount.ID,
						Context);
				}

				if (sharedWorkflow == null && (externalShares == null || externalShares.Count == 0))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				if (sharedWorkflow != null)
				{
					if (!DALUtils.IsFromCurrentAccount<ColorProofCoZoneWorkflow>(
						sharedWorkflow.ColorProofCoZoneWorkflow, LoggedAccount.ID,
						Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					DeliverBL.DeleteSharedColorProofWorkflow(sharedWorkflow, Context);
				}
				else
				{
					foreach (var share in externalShares)
					{
						DeliverBL.DeleteSharedColorProofWorkflow(share, Context);
					}
				}

				Context.SaveChanges();

				int wfId = sharedWorkflow != null
					? sharedWorkflow.ColorProofCoZoneWorkflow
					: externalShares[0].ColorProofCoZoneWorkflow;

				var user = sharedWorkflow != null ? LoggedUser.ID : DeliverBL.GetCPWorkflowOwner(wfId, Context);

				var workflow = DeliverBL.GetColorProofCoZoneWorkflowName(
					wfId, Context);


				NotificationServiceBL.CreateNotification(new SharedWorkflowDeleted()
															{
																EventCreator = LoggedUser.ID,
																InternalRecipient = user,
																WorkflowName = workflow
															},
															LoggedUser,
															Context
														);

				if (isOwner)
				{
					return RedirectToAction("AddEditColorProofWorkflow",
						new { SelectedWorkflow = sharedWorkflow.ColorProofCoZoneWorkflow, isFromCurrentAccount = true });
				}
				else
				{
					return RedirectToAction("DeliverSettings",
						new { type = Enums.DeliverSettingsType.ColorProofWorkflows });
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID,
				   "SettingsController.DeleteShareColorProofWorkflowId {0} : Exception {1} StackTrace {2}", sharedWorkflowId, ex.Message, ex.StackTrace);

				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		#endregion

		#region XML Engine

		[HttpGet]
		public ActionResult UploadEngineTemplates(Enums.UploadEngineTemplatesType? type, int? page)
		{
			try
			{
				if (type.HasValue)
				{
					switch (type)
					{
						case Enums.UploadEngineTemplatesType.Presets:
							UploadEnginePresets presetsModel = new UploadEnginePresets(LoggedAccount, Context);
							presetsModel.page = page;
							return View("UploadEnginePresets", presetsModel);

						case Enums.UploadEngineTemplatesType.Profiles:
							UploadEngineProfiles profilesModel = new UploadEngineProfiles(LoggedAccount, Context);
							profilesModel.page = page;
							return View("UploadEngineProfiles", profilesModel);

						default:
							UploadEngineXMLTemplates xmlTemplatesModel = new UploadEngineXMLTemplates(LoggedAccount, Context);
							xmlTemplatesModel.page = page;
							return View("UploadEngineXMLTemplates", xmlTemplatesModel);
					}
				}

				UploadEngineProfiles model = new UploadEngineProfiles(LoggedAccount, Context);
				return View("UploadEngineProfiles", model);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.UploadEngineTemplates : error occured in populating Templates view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpGet]
		public ActionResult UploadEngineSettings()
		{
			try
			{
				UploadEngineAccountSettings model =
				XMLEngineBL.GetAccoutUploadEngineSettings(LoggedAccount.ID, Context);

				if (TempData[UploadEngineSettingsModelErrors] != null)
				{
					Dictionary<string, List<string>> errors = TempData[UploadEngineSettingsModelErrors] as Dictionary<string, List<String>>;
					foreach (KeyValuePair<string, List<string>> error in errors)
					{
						foreach (string errorMessage in error.Value)
						{
							ModelState.AddModelError(error.Key, errorMessage);
						}
					}
					TempData[UploadEngineSettingsModelErrors] = null;
				}

				return View("UploadEngineSettings", model);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.UploadEngineSettings : error occured in populating UploadEngineSettings view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult SaveUploadEngineSettings(UploadEngineAccountSettings uploadEngineSettings, FormCollection form)
		{
			try
			{
				Dictionary<string, List<String>> errorsDict = new Dictionary<string, List<string>>();
				if (ModelState.IsValid)
				{
					#region Validation

					var selectedUploaderPermissions = form.GetValues("UploaderPermission");					
					var selectedDownloadFromCollaboratePermissions = form.GetValues("DownloadFromCollaboratePermission");
					var selectedDownloadFromDeliverPermissions = form.GetValues("DownloadFromDeliverPermission");

					List<int> uploaderPermissionUserGroups = selectedUploaderPermissions != null ? selectedUploaderPermissions.Select(n => int.Parse(n)).ToList() : new List<int>();					
					List<int> downloadFromCollaborateUserGroups = selectedDownloadFromCollaboratePermissions != null ? selectedDownloadFromCollaboratePermissions.Select(n => int.Parse(n)).ToList() : new List<int>();
					List<int> downloadFromDeliverUserGroups = selectedDownloadFromDeliverPermissions != null ? selectedDownloadFromDeliverPermissions.Select(n => int.Parse(n)).ToList() : new List<int>();

					List<int> userGroups = uploaderPermissionUserGroups.Union(downloadFromCollaborateUserGroups)
										   .Union(downloadFromDeliverUserGroups).ToList();

					foreach (int ugID in userGroups)
					{
						if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.UserGroup>(ugID, LoggedAccount.ID, Context))
						{
							return RedirectToAction("Unauthorised", "Error");
						}
					}

					#endregion

					using (TransactionScope ts = new TransactionScope())
					{
						UploadEngineAccountSetting accountUploadEngineSettings;

						if (LoggedAccount.UploadEngineAccountSetting != null)
						{
							#region Update UploadEngineAccountSetting

							accountUploadEngineSettings = LoggedAccount.UploadEngineAccountSetting;

							#endregion
						}
						else
						{
							#region Create UploadEngineAccountSetting

							accountUploadEngineSettings = new UploadEngineAccountSetting();

							#endregion
						}

						accountUploadEngineSettings.AllowImage = uploadEngineSettings.AllowImage;
                        accountUploadEngineSettings.AllowDoc = uploadEngineSettings.AllowDoc;
						accountUploadEngineSettings.AllowPDF = uploadEngineSettings.AllowPDF;
						accountUploadEngineSettings.AllowSWF = uploadEngineSettings.AllowSWF;
						accountUploadEngineSettings.AllowVideo = uploadEngineSettings.AllowVideo;
						accountUploadEngineSettings.AllowZip = uploadEngineSettings.AllowImage;
						accountUploadEngineSettings.ZipFilesWhenDownloading = uploadEngineSettings.ZipFilesWhenDownloading;
						accountUploadEngineSettings.DuplicateFileName = uploadEngineSettings.DuplicateFileOption.ID;
						accountUploadEngineSettings.VersionName = uploadEngineSettings.VersionNameOption.ID;
						accountUploadEngineSettings.VersionNumberPositionFromStart = uploadEngineSettings.VersionNumberPositionFromStart;
						accountUploadEngineSettings.VersionNumberPositionInName = uploadEngineSettings.VersionNumberPosition;
						accountUploadEngineSettings.ApplyUploadSettingsForNewApproval = uploadEngineSettings.ApplyUploadSettingsForNewApproval;
						accountUploadEngineSettings.UseUploadSettingsFromPreviousVersion = uploadEngineSettings.UseUploadSettingsFromPreviousVersion;
						accountUploadEngineSettings.EnableVersionMirroring = uploadEngineSettings.EnableVersionMirroring;
						accountUploadEngineSettings.PostStatusUpdatesURL = uploadEngineSettings.PostStatusUpdatesURL;
                        accountUploadEngineSettings.AllowApiStatusUpdate = uploadEngineSettings.AllowApiStatusUpdate;
                        accountUploadEngineSettings.AllowApiApprovalDueDate = uploadEngineSettings.AllowApiApprovalDueDate;
                        accountUploadEngineSettings.AllowApiPhaseDueDate = uploadEngineSettings.AllowApiPhaseDueDate;
                        accountUploadEngineSettings.AllowApiOnAnnotation = uploadEngineSettings.AllowApiOnAnnotation;

                        XMLEngineBL.ProcessUserGroupPermissions(LoggedAccount.ID,
																uploaderPermissionUserGroups,															
																downloadFromCollaborateUserGroups,
																downloadFromDeliverUserGroups,
																Context);
						if (LoggedAccount.UploadEngineAccountSetting == null)
						{
							Context.UploadEngineAccountSettings.Add(accountUploadEngineSettings);
							LoggedAccount.UploadEngineAccountSetting = accountUploadEngineSettings;
						}
						Context.SaveChanges();
						ts.Complete();
					}
				}
				else
				{
					ModelState.Where(o => o.Value.Errors.Count > 0).ToList().ForEach(
						o => errorsDict.Add(o.Key, o.Value.Errors.Select(e => e.ErrorMessage).ToList()));
					errorsDict.Add(string.Empty,
						 new string[] { Resources.Resources.errSavePresetChanges }.ToList());
					TempData[UploadEngineSettingsModelErrors] = errorsDict;
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.SaveUploadEngineSettings : error occured in saving UploadEngineSettings. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}

			return RedirectToAction("UploadEngineSettings");
		}

		#region Presets

		[HttpGet]
		public ActionResult AddEditPreset(int? SelectedInstance, int? page)
		{
			try
			{
				PresetModel model;

				if (TempData["PresetModel"] != null)
				{
					model = TempData["PresetModel"] as PresetModel;
				}
				else
				{
					if (SelectedInstance.HasValue)
					{
						#region Edit Preset

						model = new PresetModel(SelectedInstance.Value, Context);

						#endregion
					}
					else
					{
						#region New Preset

						model = new PresetModel();

						#endregion
					}
				}

				model.page = page;
				if (TempData[PresetModelErrors] != null)
				{
					Dictionary<string, List<string>> errors = TempData[PresetModelErrors] as Dictionary<string, List<String>>;
					foreach (KeyValuePair<string, List<string>> error in errors)
					{
						foreach (string errorMessage in error.Value)
						{
							ModelState.AddModelError(error.Key, errorMessage);
						}
					}
					TempData[PresetModelErrors] = null;
				}

				return View("AddEditPreset", model);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.AddEditPreset : error occured in populating AddEdit preset view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult SavePresetChanges(PresetModel model, int? page)
		{
			try
			{
				bool success = true;
				Dictionary<string, List<String>> dict = new Dictionary<string, List<string>>();
				if (ModelState.IsValid)
				{
					#region Validations

					if (String.IsNullOrEmpty(model.Name))
					{
						success = false;
						dict.Add("Name",
										new string[] { Resources.Resources.lblPresetNameRequired }.ToList());
					}

					if (success && String.IsNullOrEmpty(model.Host))
					{
						success = false;
						dict.Add("Host",
										new string[] { Resources.Resources.lblHostRequired }.ToList());
					}

					if (success && model.Host.Length > 64)
					{
						success = false;
						dict.Add("Host",
										new string[] { Resources.Resources.errTooManyCharactors }.ToList());
					}

					if (success && model.Name.Length > 128)
					{
						success = false;
						dict.Add("Name",
										new string[] { Resources.Resources.errTooManyCharactors }.ToList());
					}
					if (success && model.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEnginePreset>(model.ID, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					#endregion

					if (success)
					{
						using (TransactionScope ts = new TransactionScope())
						{
							UploadEnginePreset preset = null;
							if (model.ID > 0)
							{
								#region Update Existing Preset

								preset = UploadEnginePreset.GetObjectById(model.ID, Context);
								if (preset != null)
								{
									if (!XMLEngineBL.ValidateName(model.Name.ToLower(), preset.Name.ToLower(), LoggedAccount.ID, Context))
									{
										success = false;
										dict.Add("Name",
										new string[] { Resources.Resources.lblPresetNameUnique }.ToList());
									}
									else
									{
										preset.Name = model.Name;
										preset.Protocol = model.Protocol;
										if (model.IsChangeLogin)
										{
											preset.Username = model.Username;
											preset.Password = !String.IsNullOrEmpty(model.Password) ? GMG.CoZone.Common.Utils.Encrypt(model.Password, GMG.CoZone.Common.Constants.EncryptDecryptPrivateKeyXMLEnginePreset, true) : null;
										}
										preset.Host = model.Host;
										preset.Port = model.Port;
										preset.IsEnabledInCollaborate = model.IsEnabledInCollaborate;
										preset.IsEnabledInDeliver = model.IsEnabledInDeliver;
										preset.ReplicateCoZoneFolders = model.ReplicateCoZoneFolders;
										preset.DefaultDirectory = model.DefaultDirectory;
										preset.IsActiveTransferMode = model.IsActiveTransferMode;
									}
								}

								#endregion
							}
							else
							{
								#region Add New Preset

								if (!XMLEngineBL.ValidateName(model.Name.ToLower(), String.Empty, LoggedAccount.ID, Context))
								{
									success = false;
									dict.Add("Name",
									new string[] { Resources.Resources.lblPresetNameUnique }.ToList());
								}
								else
								{
									preset = new UploadEnginePreset();

									preset.Name = model.Name;
									preset.Account = LoggedAccount.ID;
									preset.Guid = Guid.NewGuid().ToString();
									preset.Protocol = model.Protocol;
									preset.Username = model.Username;
									preset.Password = !String.IsNullOrEmpty(model.Password) ? GMG.CoZone.Common.Utils.Encrypt(model.Password, GMG.CoZone.Common.Constants.EncryptDecryptPrivateKeyXMLEnginePreset, true) : null;
									preset.Host = model.Host;
									preset.Port = model.Port;
									preset.IsEnabledInCollaborate = model.IsEnabledInCollaborate;
									preset.IsEnabledInDeliver = model.IsEnabledInDeliver;									
									preset.ReplicateCoZoneFolders = model.ReplicateCoZoneFolders;
									preset.DefaultDirectory = model.DefaultDirectory;
									preset.IsActiveTransferMode = model.IsActiveTransferMode;

									Context.UploadEnginePresets.Add(preset);
								}
								#endregion
							}							

							Context.SaveChanges();
							ts.Complete();
						}
					}
				}
				else
				{
					success = false;
					ModelState.Where(o => o.Value.Errors.Count > 0).ToList().ForEach(
						o => dict.Add(o.Key, o.Value.Errors.Select(e => e.ErrorMessage).ToList()));
					dict.Add(string.Empty,
						 new string[] { Resources.Resources.errSavePresetChanges }.ToList());
				}

				if (!success)
				{
					TempData["PresetModelErrors"] = dict;
					TempData["PresetModel"] = model;

					return RedirectToAction("AddEditPreset");
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("Error", "UnexpectedError");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.SavePresetChanges : error occured in populating presets view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}

			return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.Presets, page = page });
		}

		[HttpPost]
		[OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
		public JsonResult CheckFTPConnectivity(PresetModel model)
		{
			try
			{
				try
				{
					string url = "ftp://" + model.Host + ":" + model.Port;
					if (!String.IsNullOrEmpty(model.DefaultDirectory))
					{
						url = url + "/" + HttpUtility.UrlEncode(model.DefaultDirectory);
					}

					FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
					request.Method = WebRequestMethods.Ftp.ListDirectory;
					request.KeepAlive = false;

					HttpRequestCachePolicy noCachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
					request.CachePolicy = noCachePolicy;

					if (XMLEngineBL.GetProtocolKey(model.Protocol, Context) == (int)GMG.CoZone.Common.FTPProtocol.FTPS)
					{
						request.EnableSsl = true;
					}

					if (!model.IsActiveTransferMode)
					{
						request.UsePassive = true;
					}

					if (model.ID > 0)
					{
						if (!model.IsChangeLogin)
						{
							KeyValuePair<string, string> presetLogin = XMLEngineBL.GetPresetLogin(model.ID, Context);
							if (!presetLogin.Equals(default(KeyValuePair<string, string>)))
							{
								model.Username = presetLogin.Key;
								model.Password = !String.IsNullOrEmpty(presetLogin.Value) ? GMG.CoZone.Common.Utils.Decrypt(presetLogin.Value, GMG.CoZone.Common.Constants.EncryptDecryptPrivateKeyXMLEnginePreset, true) : null;
							}
						}
					}

					if (!String.IsNullOrEmpty(model.Username))
					{
						request.Credentials = new NetworkCredential(model.Username, model.Password ?? String.Empty);
					}

					using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
					{
						GMGColorLogging.log.InfoFormat("SettingsController.CheckFTPConnectivity: FTP request for server <{0}> returned: {1} - {2}",
							model.Host, response.StatusCode, response.StatusDescription);
					}

				}
				catch (WebException ex)
				{
					GMGColorLogging.log.WarnFormat("SettingsController.CheckFTPConnectivity : FTP request for server <{0}> failed: {1} StackTrace {2}",
						model.Host, ex.Message, ex.StackTrace);
					return Json(false);
				}
				return Json(true);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				   "SettingsController.CheckFTPConnectivity : error occured in checking ftp connectivity. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return Json(false);
			}
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeletePreset")]
		public ActionResult DeletePreset(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue)
				{
					if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEnginePreset>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					UploadEnginePreset preset = UploadEnginePreset.GetObjectById(SelectedInstance.Value, Context);

					List<FTPPresetJobDownload> presetDownloadJobs = preset.FTPPresetJobDownloads.ToList();

					if (presetDownloadJobs.Count > 0)
					{
						for (int i = presetDownloadJobs.Count() - 1; i >= 0; i--)
						{
							XMLEngineBL.DeleteFTPDownloadJob(presetDownloadJobs[i], Context);
						}
					}

					DALUtils.Delete<UploadEnginePreset>(Context, preset);

					Context.SaveChanges();

					return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.Presets, page = page });
				}
				else
				{
					GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "DeletePreset method, invalid selectedInstance parameter");
					// handle invalid argument error
					return RedirectToAction("Cannotcontinued", "Error");
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("Error", "UnexpectedError");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				  "SettingsController.DeletePreset : error occured when deleting preset. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		#endregion

		#region Profiles

		[HttpGet]
		public ActionResult AddEditProfile(int? SelectedInstance, int? page)
		{
			try
			{
                
                ProfileModel model;

				if (SelectedInstance.HasValue)
				{
					#region Edit Profile

					model = new ProfileModel(LoggedAccount.ID, Context, SelectedInstance.Value);

					#endregion
				}
				else
				{
					#region New Profile

					model = new ProfileModel(LoggedAccount.ID, Context);

					#endregion
				}

				if (TempData["ProfileModel"] != null)
				{
					ProfileModel tempModel = TempData["ProfileModel"] as ProfileModel;
					model.Name = tempModel.Name;
					model.HasInputSettings = tempModel.HasInputSettings;
					model.OutputPreset = tempModel.OutputPreset;
					model.XMLTemplate = tempModel.XMLTemplate;
					model.ID = tempModel.ID;
				}

				model.page = page;
				if (TempData[ProfileModelErrors] != null)
				{
					Dictionary<string, List<string>> errors = TempData[ProfileModelErrors] as Dictionary<string, List<String>>;
					foreach (KeyValuePair<string, List<string>> error in errors)
					{
						foreach (string errorMessage in error.Value)
						{
							ModelState.AddModelError(error.Key, errorMessage);
						}
					}
					TempData[ProfileModelErrors] = null;
				}

				return View("AddEditProfile", model);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.AddEditPreset : error occured in populating AddEdit profile view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		public ActionResult SaveProfileChanges(ProfileModel model, int? page)
		{
			try
			{
				bool success = true;
				Dictionary<string, List<String>> dict = new Dictionary<string, List<string>>();
				if (ModelState.IsValid)
				{
					#region Validations

					if (model.OutputPreset == null && !model.HasInputSettings)
					{
						success = false;
						dict.Add("",
										new string[] { Resources.Resources.reqProfileStep }.ToList());
					}

					if (success && String.IsNullOrEmpty(model.Name))
					{
						success = false;
						dict.Add("Name",
										new string[] { Resources.Resources.reqProfileName }.ToList());
					}

					if (success && model.Name.Length > 128)
					{
						success = false;
						dict.Add("Name",
										new string[] { Resources.Resources.errTooManyCharactors }.ToList());
					}
					if (success && model.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEngineProfile>(model.ID, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					#endregion

					if (success)
					{
						using (TransactionScope ts = new TransactionScope())
						{
							if (model.ID > 0)
							{
								#region Update Existing Profile

								UploadEngineProfile profile = UploadEngineProfile.GetObjectById(model.ID, Context);
								if (profile != null)
								{
									if (!XMLEngineBL.ValidateProfileName(model.Name.ToLower(), profile.Name.ToLower(), LoggedAccount.ID, Context))
									{
										success = false;
										dict.Add("Name",
										new string[] { Resources.Resources.lblProfileNameUnique }.ToList());
									}
									else
									{
										profile.Name = model.Name;
										profile.HasInputSettings = model.HasInputSettings;
										profile.OutputPreset = model.OutputPreset;
										profile.XMLTemplate = model.XMLTemplate;
									}
								}

								#endregion
							}
							else
							{
								#region Add New Profile

								if (!XMLEngineBL.ValidateProfileName(model.Name.ToLower(), String.Empty, LoggedAccount.ID, Context))
								{
									success = false;
									dict.Add("Name",
									new string[] { Resources.Resources.lblProfileNameUnique }.ToList());
								}
								else
								{
									UploadEngineProfile profile = new UploadEngineProfile();

									profile.Name = model.Name;
									profile.Account = LoggedAccount.ID;
									profile.Guid = Guid.NewGuid().ToString();
									profile.HasInputSettings = model.HasInputSettings;
									profile.OutputPreset = model.OutputPreset;
									profile.XMLTemplate = model.XMLTemplate;
									profile.IsActive = true;

									Context.UploadEngineProfiles.Add(profile);
								}
								#endregion
							}
							Context.SaveChanges();
							ts.Complete();
						}
					}
				}
				else
				{
					success = false;
					ModelState.Where(o => o.Value.Errors.Count > 0).ToList().ForEach(
						o => dict.Add(o.Key, o.Value.Errors.Select(e => e.ErrorMessage).ToList()));
					dict.Add(string.Empty,
						 new string[] { Resources.Resources.errSavePresetChanges }.ToList());
				}

				if (!success)
				{
					TempData[ProfileModelErrors] = dict;
					TempData["ProfileModel"] = model;

					return RedirectToAction("AddEditProfile");
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.SaveProfileChanges : error occured in populating presets view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}

			return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.Profiles, page = page });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "ActivateXMLEngineProfile")]
		public ActionResult ActivateXMLEngineProfile(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue)
				{
					if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEngineProfile>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					UploadEngineProfile profile = UploadEngineProfile.GetObjectById(SelectedInstance.Value, Context);

					profile.IsActive = true;

					Context.SaveChanges();

					return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.Profiles, page = page });
				}
				else
				{
					GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "ActivateXMLEngineProfile method, invalid selectedInstance parameter");
					// handle invalid argument error
					return RedirectToAction("Cannotcontinued", "Error");
				}
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				  "SettingsController.ActivateXMLEngineProfile : error occured when activating profile. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeactivateXMLEngineProfile")]
		public ActionResult DeactivateXMLEngineProfile(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue)
				{
					if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEngineProfile>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					UploadEngineProfile profile = UploadEngineProfile.GetObjectById(SelectedInstance.Value, Context);

					profile.IsActive = false;

					Context.SaveChanges();

					return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.Profiles, page = page });
				}
				else
				{
					GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "DeactivateXMLEngineProfile method, invalid selectedInstance parameter");
					// handle invalid argument error
					return RedirectToAction("Cannotcontinued", "Error");
				}
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				  "SettingsController.DeactivateXMLEngineProfile : error occured when deactivating profile. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteXMLEngineProfile")]
		public ActionResult DeleteXMLEngineProfile(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue)
				{
					if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEngineProfile>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					UploadEngineProfile profile = UploadEngineProfile.GetObjectById(SelectedInstance.Value, Context);

					if (XMLEngineBL.ProfileHasJobsAttached(SelectedInstance.Value, Context))
					{
						profile.IsDeleted = true;
					}
					else
					{
						DALUtils.Delete<UploadEngineProfile>(Context, profile);
					}

					Context.SaveChanges();

					return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.Profiles, page = page });
				}
				else
				{
					GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "DeleteXMLEngineProfile method, invalid selectedInstance parameter");
					// handle invalid argument error
					return RedirectToAction("Cannotcontinued", "Error");
				}
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				  "SettingsController.DeleteXMLEngineProfile : error occured when deleting profile. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		#endregion

		#region XMLTemplates

		[HttpGet]
		public ActionResult AddEditXMLTemplate(int? SelectedInstance, int? page)
		{
			try
			{
				XMLTemplateModel model;

				if (SelectedInstance.HasValue)
				{
					#region Edit Profile

					model = new XMLTemplateModel(Context, SelectedInstance.Value);

					if (model.SelectedCustomFields != null && model.SelectedCustomFields.Count > 0)
					{
						model.SelectedCustomFields.ForEach(t => t.DefaultXMLTemplateNodeName = Utils.GetTranslatedXMLTemplateField((GMG.CoZone.Common.DefaultXMLTemplateField)t.DefaultXMLTemplateKey));
					}

					#endregion
				}
				else
				{
					#region New Profile

					model = new XMLTemplateModel(Context);

					#endregion
				}

				model.page = page;

				ViewBag.AllFieldsSelected = model.SelectedCustomFields != null && model.SelectedCustomFields.Count == model.AllDefaultTemplatesList.Count;

				model.DefaultFieldsList.ForEach(t => t.NodeName = Utils.GetTranslatedXMLTemplateField((GMG.CoZone.Common.DefaultXMLTemplateField)t.Key));

				return View("AddEditXMLTemplate", model);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("UnexpectedError", "Error");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
					"SettingsController.AddEditXMLTemplate : error occured in populating AddEdit XMLTemplate view. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SaveXMLTemplates")]
		public JsonResult SaveXMLTemplatesChanges(XMLTemplateModel model)
		{
			bool success = true;
			try
			{
				//Check if at least one custom field is set
				if (model.SelectedCustomFields == null || model.SelectedCustomFields.All(t => t.IsDeleted))
				{
					success = false;
					ModelState.AddModelError("", Resources.Resources.reqAtLeastOneXMLField);
				}
				else
				{
					//Remove all deleted custom fields
					model.SelectedCustomFields.RemoveAll(t => t.IsDeleted == true && t.ID == 0);
				}

				if (success)
				{
					//Remove validation errors for deleted fields
					for (int i = 0; i < model.SelectedCustomFields.Count; i++)
					{
						if (model.SelectedCustomFields[i].IsDeleted)
						{
							if (ModelState.ContainsKey(String.Format("SelectedCustomFields[{0}].XPath", i)))
							{
								ModelState[String.Format("SelectedCustomFields[{0}].XPath", i)].Errors.Clear();
							}
						}
					}

					if (ModelState.IsValid)
					{
						#region Validations

						//Check XML template access  
						if (success && model.ID.GetValueOrDefault() > 0 &&
							!DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEngineCustomXMLTemplate>(model.ID,
								LoggedAccount.ID, Context))
						{
							success = false;
							Response.Redirect(Url.Action("Unauthorised", "Error"));
						}

						#endregion


						UploadEngineCustomXMLTemplate customXMLTemplate;
						if (model.ID.GetValueOrDefault() > 0)
						{
							#region Update Custom XMLTemplate

							customXMLTemplate = XMLEngineBL.GetCustomXMLTemplateById(model.ID.Value, Context);

							// Check if name is unique withn current account
							if (!XMLEngineBL.ValidateXMLTemplateName(model.Name.ToLower(), customXMLTemplate.Name.ToLower(), LoggedAccount.ID, Context))
							{
								ModelState.AddModelError("Name", Resources.Resources.lblXMLTemplateUnique);
								success = false;
							}
							else
							{
								customXMLTemplate.Name = model.Name;
								XMLEngineBL.ProcessCustomXMLTemplates(customXMLTemplate, model.SelectedCustomFields, false, Context);
							}

							#endregion
						}
						else
						{
							#region Add New Custom XMLTemplate

							// Check if name is unique withn current account
							if (!XMLEngineBL.ValidateXMLTemplateName(model.Name.ToLower(), String.Empty, LoggedAccount.ID, Context))
							{
								ModelState.AddModelError("Name", Resources.Resources.lblXMLTemplateUnique);
								success = false;
							}
							else
							{
								customXMLTemplate = new UploadEngineCustomXMLTemplate();
								customXMLTemplate.Account = LoggedAccount.ID;
								customXMLTemplate.Name = model.Name;

								XMLEngineBL.ProcessCustomXMLTemplates(customXMLTemplate, model.SelectedCustomFields, true, Context);
								Context.UploadEngineCustomXMLTemplates.Add(customXMLTemplate);
							}

							#endregion
						}
						Context.SaveChanges();
					}
					else
					{
						success = false;
					}
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				ModelState.AddModelError("", Resources.Resources.errPreflightSaving);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "Error when saving Custom XMLTemplates changes, message: {0}", ex.Message);
				ModelState.AddModelError("", Resources.Resources.errPreflightSaving);
				success = false;
			}

			var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
			var errors = new Dictionary<string, List<string>>();
			foreach (string key in ModelState.Keys)
			{
				if (!ModelState[key].Errors.Any())
					continue;

				errors.Add(key, ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
			}

			return Json(serializer.Serialize(new
			{
				Success = success,
				Errors = errors
			}));
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteXMLTemplate")]
		public ActionResult DeleteXMLTemplate(int? SelectedInstance, int? page)
		{
			try
			{
				if (SelectedInstance.HasValue)
				{
					if (!DALUtils.IsFromCurrentAccount<GMGColorDAL.UploadEngineCustomXMLTemplate>(SelectedInstance.Value, LoggedAccount.ID, Context))
					{
						return RedirectToAction("Unauthorised", "Error");
					}

					UploadEngineCustomXMLTemplate xmlTemplate = XMLEngineBL.GetCustomXMLTemplateById(SelectedInstance.Value, Context);

					if (xmlTemplate.UploadEngineProfiles.Count == 0)
					{
						List<UploadEngineCustomXMLTemplatesField> customFields = xmlTemplate.UploadEngineCustomXMLTemplatesFields.ToList();

						if (customFields.Count > 0)
						{
							for (int i = customFields.Count() - 1; i >= 0; i--)
							{
								XMLEngineBL.DeleteCustomXMLTemplateField(customFields[i], Context);
							}
						}

						XMLEngineBL.DeleteCustomXMLTemplate(xmlTemplate, Context);
						Context.SaveChanges();
					}

					return RedirectToAction("UploadEngineTemplates", new { type = Enums.UploadEngineTemplatesType.XMLTemplates, page = page });
				}
				else
				{
					GMGColorLogging.log.WarnFormat(LoggedAccount.ID, "DeleteXMLTemplate method, invalid selectedInstance parameter");
					// handle invalid argument error
					return RedirectToAction("Cannotcontinued", "Error");
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				return RedirectToAction("Error", "UnexpectedError");
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				  "SettingsController.DeleteXMLTemplate : error occured when deleting preset. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}
		}

		#endregion

		#endregion

		#region Collaborate Global Settings Page

		[HttpGet]
		public ActionResult CollaborateGlobalSettings()
		{
			ViewBag.IsCustomProfileEnabled = ApprovalBL.GetPlanTypeOptions(LoggedAccount.ID, AppModule.Collaborate, Context).Contains((int)PlanFeatures.CustomProfile);
			var model = new AccountSettings.CollaborateGlobalSettings(LoggedAccount.ID, GMGColorConstants.ProofStudioPenWidthDefaultValue, ViewBag.IsCustomProfileEnabled, Context, true);
			ViewBag.IsAccountOwner = LoggedAccount.Owner == LoggedUser.ID;
			ViewBag.PathToTempFolder = LoggedUserTempFolderPath;

			if (ViewBag.IsCustomProfileEnabled)
			{
				ViewBag.PaperSubstrates = SoftProofingBL.GetMediaCategoryList(Context);
			}
            
            if (TempData["ColSetModelErrors"] != null)
			{
				Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ColSetModelErrors"];
				TempData["ColSetModelErrors"] = null;

				foreach (KeyValuePair<string, string> item in dicModelErrors)
				{
					ModelState.AddModelError(item.Key, item.Value);
				}
			}

			return View("CollaborateGlobalSettings", model);
		}

		[HttpPost]
		public ActionResult CollaborateGlobalSettings(AccountSettings.CollaborateGlobalSettings model, string hdnFileName_1)
		{            
			try
			{
				var success = true;
				var errors = new Dictionary<string, string>();
				if (model.ApprovalDecisions.All(d => d.IsChecked))
				{
					success = false;
					errors.Add("error", Resources.Resources.lblAtleastOneApprovalStatus);
				}
				if (success)
				{
					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID,
																   model.AllCollaboratorsDecisionRequired,
																   GMGColorConstants.AllColDecisionRequired,
																   Utils.GetLabelForCollaborateGlobalSetting(
																	   GMGColorConstants.AllColDecisionRequired),
																   Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, 
																	model.HideCompletedApprovalsInDashboard, 
																	GMGColorConstants.HideCompApprovals,
																	Utils.GetLabelForCollaborateGlobalSetting(GMGColorConstants.HideCompApprovals),
																	Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, 
                                                                    model.ShowAllFilesToManagers,
                                                                    GMGColorConstants.ShowAllFilesToManagers,
                                                                    Utils.GetLabelForCollaborateGlobalSetting(GMGColorConstants.ShowAllFilesToManagers),
                                                                    Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID,
                                                                      model.DisableExternalUsersEmailIndicator,
                                                                      GMGColorConstants.DisableExternalUsersEmailIndicator,
                                                                      GMGColorConstants.DisableExternalUsersEmailIndicator,
                                                                      Context);

					//SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.EnableFlexViewer,
					//                    GMGColorConstants.EnableFlexViewer, Utils.GetLabelForCollaborateGlobalSetting(GMGColorConstants.EnableFlexViewer), Context);

					if (!AccountBL.HasRetoucherUser(LoggedAccount.ID, Context))
					{
						SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.EnableRetoucherWorkflow,
																	   GMGColorConstants.EnableRetoucherWorkflow,
																	   GMGColorConstants.EnableRetoucherWorkflow,
																	   Context);
					}

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.DisplayRetouchers,
																   GMGColorConstants.DisplayRetouchers,
																   GMGColorConstants.DisplayRetouchers, Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.ShowBadges,
																   GMGColorConstants.ShowBadges,
																   GMGColorConstants.ShowBadges, Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID,
																   model.InheritParentFolderPermissions,
																   GMGColorConstants.InheritPermissionsFromParentFolder,
																   Utils.GetLabelForCollaborateGlobalSetting(
																	   GMGColorConstants
																		   .InheritPermissionsFromParentFolder), Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.ProofStudioPenWidth,
																   GMGColorConstants.ProofStudioPenWidth,
																   Utils.GetLabelForCollaborateGlobalSetting(
																	   GMGColorConstants.ProofStudioPenWidth), Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID,
																   model.ProofStudioShowAnnotationsForExternalUsers,
																   GMGColorConstants
																	   .ProofStudioShowAnnotationsForExternalUsers,
																   Utils.GetLabelForCollaborateGlobalSetting(
																	   GMGColorConstants
																		   .ProofStudioShowAnnotationsForExternalUsers),
																   Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.EnableCustomProfile,
																   GMGColorConstants.EnableCustomProfile,
																   GMGColorConstants.EnableCustomProfile, Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.DisableFileDueDateinUploader,
						GMGColorConstants.DisableFileDueDateinUploader, 
						Utils.GetLabelForCollaborateGlobalSetting(GMGColorConstants.DisableFileDueDateinUploader), Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.ApprovalDecisions, Context);


					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.DisableSOADIndicator,
																GMGColorConstants.DisableSOADIndicator,
																GMGColorConstants.DisableSOADIndicator, Context);

					SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.DisableVisualIndicator,
																GMGColorConstants.DisableVisualIndicator,
																GMGColorConstants.DisableVisualIndicator, Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.DisableTagwordsInDashbord,
                                                                GMGColorConstants.DisableTagwordsInDashbord,
                                                                GMGColorConstants.DisableTagwordsInDashbord, Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.KeepExternalUsersWhenUploadingNewVersion,
																GMGColorConstants.KeepExternalUsersWhenUploadingNewVersion,
																GMGColorConstants.KeepExternalUsersWhenUploadingNewVersion, Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.EnableProofStudioZoomlevelFitToPageOnload,
                                                                GMGColorConstants.EnableProofStudioZoomlevelFitToPageOnload,
                                                                GMGColorConstants.EnableProofStudioZoomlevelFitToPageOnload, Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.ShowSubstrateFromICCProfileInProofStudio,
                                                                GMGColorConstants.ShowSubstrateFromICCProfileInProofStudio,
                                                                GMGColorConstants.ShowSubstrateFromICCProfileInProofStudio, Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.EnableArchiveTimeLimit,
                                                                GMGColorConstants.EnableArchiveTimeLimit,
                                                                GMGColorConstants.EnableArchiveTimeLimit, Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, int.Parse(model.ArchiveTimeLimit),
                                               GMGColorConstants.ArchiveTimeLimit,
                                               Utils.GetLabelForCollaborateGlobalSetting(
                                                   GMGColorConstants.ArchiveTimeLimit), Context);

                    SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, int.Parse(model.ArchiveTimeLimitFormat),
                                               GMGColorConstants.ArchiveTimeLimitFormat,
                                               Utils.GetLabelForCollaborateGlobalSetting(
                                                   GMGColorConstants.ArchiveTimeLimitFormat), Context);

                    //only account owner should see this setting
                    if (LoggedAccount.Owner == LoggedUser.ID)
					{
						SettingsBL.AddOrUpdateCollaborateGlobalSetting(LoggedAccount.ID, model.ShowAllFilesToAdmins,
																	   GMGColorConstants.ShowAllFilesToAdmins,
																	   Utils.GetLabelForCollaborateGlobalSetting(
																		   GMGColorConstants.ShowAllFilesToAdmins),
																	   Context);
					}

					if (model.EnableCustomProfile)
					{
						SettingsBL.UpdateAccountCustomICCProfile(LoggedAccount, hdnFileName_1, model.CustomProfileModel.SelectedPaperSubstrate, LoggedUser.ID, Context);
					}

					Context.SaveChanges();
				}
				else
				{
					TempData["ColSetModelErrors"] = errors;
					return RedirectToAction("CollaborateGlobalSettings");
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,
				   "SettingsController.CollaborateGlobalSettings : error occured in saving CollaborateGlobalSettings settings. {0} StackTrace {1}", ex.Message, ex.StackTrace);
				return RedirectToAction("UnexpectedError", "Error");
			}

			return RedirectToAction("CollaborateGlobalSettings");
		}

        #endregion

        #endregion

        #region POST Actions

        [HttpPost]
		public ActionResult Profile(AccountSettings.Profile model)
		{
			try
			{
                #region XSS Words Validation
                string InputStringWords = model.objProfile.objCompany.Name + model.objProfile.objCompany.Address1 +
                        model.objProfile.objCompany.Address2 + model.objProfile.objCompany.City +
                        model.objProfile.objCompany.State + model.objProfile.objCompany.Postcode;
                if (!(CheckIsValidHtml(InputStringWords) ))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }
                #endregion

                if (model.objProfile.objCompany.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.Company>(model.objProfile.objCompany.ID, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				using (TransactionScope tscope = new TransactionScope())
				{
					//objAccount.Name = model.objProfile.Account1.Name;
					LoggedAccount.Owner = model.objProfile.objAccount.Owner ;
					if (LoggedAccountType!= GMGColorDAL.AccountType.Type.GMGColor)
					{
						LoggedAccount.DealerNumber = model.objProfile.objAccount.DealerNumber;
						LoggedAccount.CustomerNumber = model.objProfile.objAccount.CustomerNumber;
						LoggedAccount.VATNumber = model.objProfile.objAccount.VATNumber;
						LoggedAccount.ModifiedDate = DateTime.UtcNow;
					}

					GMGColorDAL.Company objCompany =
						DALUtils.GetObject<GMGColorDAL.Company>(model.objProfile.objCompany.ID, Context);

					objCompany.Name = model.objProfile.objCompany.Name;
					objCompany.Address1 = model.objProfile.objCompany.Address1;
					objCompany.Address2 = model.objProfile.objCompany.Address2;
					objCompany.City = model.objProfile.objCompany.City;
					objCompany.State = model.objProfile.objCompany.State;
					objCompany.Postcode = model.objProfile.objCompany.Postcode;
					objCompany.Country = model.objProfile.objCompany.Country;

					Context.SaveChanges();
					tscope.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "An error occured wille saving profile data in Profile()", ex.Message);
			}
			finally
			{
			}

			return RedirectToAction("Profile", "settings");
		}

		[HttpPost]
		public ActionResult SiteSettings(AccountSettings.SiteSettings model)
		{
			try
			{
				//Regional Settings
				LoggedAccount.Locale = model.objSiteSettings.objAccount.Locale;
				LoggedAccount.DateFormat = model.objSiteSettings.objAccount.DateFormat;
				LoggedAccount.TimeZone = model.objSiteSettings.objAccount.TimeZone;
				LoggedAccount.TimeFormat = model.objSiteSettings.objAccount.TimeFormat;
				LoggedAccount.ModifiedDate = DateTime.UtcNow;
                LoggedAccount.IsMeasurementsInInches = model.objSiteSettings.MeasurementType == "true" ? true : false;


                if (model.objSiteSettings.ApplyLanguageToAllUsers)
				{
					//clear cache if language is changed
					if (LoggedAccount.Locale != LoggedUser.Locale)
					{
						ClearLoggedUserSecondaryNavigationCache();
					}
					foreach (var user in LoggedAccount.Users)
					{
						user.Locale = LoggedAccount.Locale;
					}
				}

				Context.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured wille saving site settings in SiteSettings()", ex);
			}
			finally
			{
			}

			return RedirectToAction("SiteSettings", "Settings");
		}

		[HttpPost]
		public ActionResult Customize(AccountSettings.Customise model, string hdnFileName_1, string hdnFileName_2, string hdnFileName_3, string hdnFileName_4)
		{
			try
			{
                if (!CheckIsValidHtml(model.objCustomise.objAccount.SiteName))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                //remove not needed validation errors
                if (!ModelState.IsValid)
				{
					ModelState["objCustomise.objAccount.Name"].Errors.Clear();
					ModelState["objCustomise.objAccount.Domain"].Errors.Clear();
					ModelState["objCustomise.objAccount.TimeZone"].Errors.Clear();
					ModelState["objCustomise.objCompany.Name"].Errors.Clear();
					ModelState["objCustomise.objCompany.Address1"].Errors.Clear();
					ModelState["objCustomise.objCompany.City"].Errors.Clear();
				}

				if (ModelState.IsValid)
				{
					LoggedAccount.SiteName = model.objCustomise.objAccount.SiteName;

					//Account Themes 
					ThemeBL.UpdateThemeSchema(LoggedAccount.ID, model.objCustomise.objColorSchema, Context);

					LoggedAccount.ModifiedDate = DateTime.UtcNow;
					this.UpdateLogos(LoggedAccount, hdnFileName_1, hdnFileName_2, hdnFileName_3, hdnFileName_4);

					Context.SaveChanges();   
				}
				else
				{
					RedirectToAction("UnexpectedError", "Error");
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "SettingsController.SaveChanges.ProcessForm : Saving Account Details Failed. {0}", ex.Message);
			}

			return RedirectToAction("Customize", "Settings");
		}

		[HttpPost]
		public ActionResult ShowHideAllUsers(bool showAllUsers)
		{
			try
			{
				UserBL.AddOrUpdateUserSetting(LoggedUser.ID, showAllUsers, GMGColorConstants.ShowAllUsers, Context);
				Context.SaveChanges();
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while adding a user setting in ShowHideAllUsers()", ex);
			}

			return Json(
					new
					{
						Status = 400
					},
					JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult WhiteLabelSettings(AccountSettings.WhiteLabelSettings model)
		{
			try
			{

                if (!(CheckIsValidHtml(model.objWhiteLabelSettings.CustomFromEmail) || CheckIsValidHtml(model.objWhiteLabelSettings.CustomFromName)))
                {
                    return RedirectToAction("XSSRequestValidation", "Error");
                }

                string contactSupportEmailKey = Enum.GetName(typeof(AccountSettingsBL.AccountSettingsKeyEnum), AccountSettingsBL.AccountSettingsKeyEnum.ContactSupportEmail);
				GMGColorDAL.AccountSetting objAccountSetting = (from accs in Context.AccountSettings
																where accs.Account == model.objWhiteLabelSettings.AccountID && accs.Name == contactSupportEmailKey
																select accs).SingleOrDefault();


				using (TransactionScope ts = new TransactionScope())
				{
					LoggedAccount.IsRemoveAllGMGCollaborateBranding = model.objWhiteLabelSettings.IsRemoveAllGMGCollaborateBranding;
					LoggedAccount.CustomFromAddress = model.objWhiteLabelSettings.CustomFromEmail;
					LoggedAccount.CustomFromName = model.objWhiteLabelSettings.CustomFromName;
					LoggedAccount.DisableLandingPage = model.objWhiteLabelSettings.DisableLandingPage;
					LoggedAccount.DashboardToShow = Convert.ToInt32(model.objWhiteLabelSettings.DashboardToShow);

					LoggedAccount.ModifiedDate = DateTime.UtcNow;

					UpdateContactSupportEmailSetting(model, objAccountSetting);

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured wille saving the white label settings in WhiteLabelSettings()", ex);
			}
			finally
			{
			}

			return RedirectToAction("WhiteLabelSettings");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "ClaimDomain")]
		public ActionResult ClaimDomain(AccountSettings.DNSAndCustomDomain model)
		{
			bool hasError = false;
			Regex regeX = new Regex(GMG.CoZone.Common.Constants.DomainNameRegex);
			Dictionary<string, string> modelErrors = new Dictionary<string, string>();

			try
			{
				if (string.IsNullOrWhiteSpace(model.objLoggedAccount.CustomDomain))
				{
					hasError = true;
					modelErrors.Add("objLoggedAccount.CustomDomain", Resources.Resources.reqInvalidDomain);
				}
				else if (!regeX.IsMatch(model.objLoggedAccount.CustomDomain))
				{
					hasError = true;
					modelErrors.Add("Domain", Resources.Resources.reqInvalidDomain);
				}
				else
				{
					using (TransactionScope ts = new TransactionScope())
					{
						if (!String.IsNullOrEmpty(model.objLoggedAccount.CustomDomain))
						{
							LoggedAccount.CustomDomain = model.objLoggedAccount.CustomDomain;
						}
						else
						{
							LoggedAccount.CustomDomain = null;
						}
						LoggedAccount.ModifiedDate = DateTime.UtcNow;

						Context.SaveChanges();
						ts.Complete();
					}
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID,
					"An error occured wille saving the custom domain in ClaimDomain()", ex);
			}
			finally
			{
				Context.Dispose();
			}

			if (hasError)
			{
				TempData["modelErrors"] = modelErrors;
			}
			return RedirectToAction("DNSAndCustomDomain", "Settings");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "ChangeDomain")]
		public ActionResult ChangeDomain(AccountSettings.DNSAndCustomDomain model)
		{
			string returnUrl = string.Empty;
			try
			{
				LoggedAccount.IsCustomDomainActive = !LoggedAccount.IsCustomDomainActive;
				LoggedAccount.ModifiedDate = DateTime.UtcNow;
				Context.SaveChanges();

				returnUrl = (LoggedAccount.IsCustomDomainActive ? LoggedAccount.CustomDomain : LoggedAccount.Domain);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured wille saving the custom domain in ChangeDomain()", ex);
			}
			finally
			{
			}

			return Redirect(GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + returnUrl + "/Auth/Logout");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteDomain")]
		public ActionResult DeleteDomain(AccountSettings.DNSAndCustomDomain model)
		{
			try
			{
				LoggedAccount.CustomDomain = null;
				LoggedAccount.IsCustomDomainActive = false;
				LoggedAccount.ModifiedDate = DateTime.UtcNow;

				Context.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured wille saving the custom domain in DeleteDomain()", ex);
			}
			finally
			{
			}
			return RedirectToAction("DNSAndCustomDomain", "Settings");
		}

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "UnlockUser")]
        public ActionResult UnlockUser(string SelectedUser)
        {
            try
            {
                SettingsBL.UnlockUser(SelectedUser.ToInt().GetValueOrDefault(), LoggedAccount.ID, Context);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while unlocking the user", ex);
            }

            return RedirectToAction("Users");
        }
            

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ActivateUser")]
        public ActionResult ActivateUser(string SelectedUser)
        {
            try
            {
                SettingsBL.ActivateUserStatus(SelectedUser.ToInt().GetValueOrDefault(), LoggedAccount.ID, Context);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while activating the user in activateUser()", ex);
            }

            return RedirectToAction("Users");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeactivateUser")]
        public ActionResult DeactivateUser(string SelectedUser)
        {
            try
            {
                 SettingsBL.DeactivateUserStatus(SelectedUser.ToInt().GetValueOrDefault(), LoggedAccount.ID, Context);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while deactivating the user in DeactivateUser()", ex);
            }

            return RedirectToAction("Users");
        }

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "ReSendInvite")]
		public ActionResult ReSendInvite(string SelectedUser)
		{
            // Updating Created date of Invited User (because link expire after a month of User Created date.)
            var SelectedUserObj = GMGColorDAL.User.GetUser(SelectedUser.ToInt().GetValueOrDefault(), Context);
            Context.SaveChanges();

            try
            {
				BaseBL.SendInviteToUser(SelectedUser.ToInt().GetValueOrDefault(), LoggedUser.ID, LoggedAccount.ID, Context);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while re-sending the invite in ReSendInvite()", ex);
			}

			return RedirectToAction("Users");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SearchUser")]
		public ActionResult Search(AccountSettings.Users model, string SearchText)
		{
			string s = (model != null && model.objUsers != null && model.objUsers.SearchText != null) ? model.objUsers.SearchText : string.Empty;
			TempData[UsersSearchText] = s;

			return RedirectToAction("Users", "Settings");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SearchCollaborateExUser")]
		public ActionResult Search(AccountSettings.ExternalUsers model, string SearchText)
		{
			string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
			TempData[ExternalUsersSearchText] = s;

			return RedirectToAction("ExternalUsers", "Settings", new { type = Enums.ExternalUserType.Collaborate });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SearchDeliverExUser")]
		public ActionResult Search(AccountSettings.DeliverExternalUsers model, string SearchText)
		{
			string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
			TempData[ExternalUsersSearchText] = s;

			return RedirectToAction("ExternalUsers", "Settings", new { type = Enums.ExternalUserType.Deliver });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SearchColorProofInstance")]
		public ActionResult Search(ColorProofSettings.ColorProofInstances model, string SearchText)
		{
			string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
			TempData[CPSearchInstances] = s;

			return RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofInstances });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SearchColorProofWorkflow")]
		public ActionResult Search(ColorProofSettings.ColorProofWorkflows model, string SearchText)
		{
			string s = (model != null && model.SearchText != null) ? model.SearchText : string.Empty;
			TempData[CPSearchWorkflows] = s;

			return RedirectToAction("DeliverSettings", new { type = Enums.DeliverSettingsType.ColorProofWorkflows });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "EditUser")]
		public ActionResult EditUser(string SelectedUser)
		{
			int userId = SelectedUser.ToInt().GetValueOrDefault();
			if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, LoggedAccount.ID, Context))
			{
				return RedirectToAction("Unauthorised", "Error");
			}
			return RedirectToAction("AddEditUser", new { SelectedUser = userId });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "AddEditUserInformation")]
		public ActionResult AddEditUserInformation(AccountSettings.AddEditUser model, int? SelectedPrimaryGroup)
		{
			try
			{
                
                var dicModelErrors = new Dictionary<string, string>();
					bool hasError = false;

                if(model.objUsers.objEditUser != null)
                {
                    if (!(CheckIsValidHtml(model.objUsers.objEditUser.objUser.Username) || CheckIsValidHtml(model.objUsers.objEditUser.objUser.FirstName)
                         || CheckIsValidHtml(model.objUsers.objEditUser.objUser.LastName)))
                    {
                        return RedirectToAction("XSSRequestValidation", "Error");
                    }

                    if (model.objUsers.objEditUser.ModulePermisions[5].SelectedRole == 0 || (model.objUsers.objEditUser.ModulePermisions[0].SelectedRole != (int) RoleID.Collaborate_Administrator && model.objUsers.objEditUser.ModulePermisions[5].SelectedRole == (int) RoleID.Admin_Administrator) )
                    {
                        model.objUsers.objEditUser.ModulePermisions[5].SelectedRole = (int) RoleID.Admin_NoAccess;
                    }
                    

                }
                else if(model.objUsers.objNewUser != null)
                {
                    if (!(CheckIsValidHtml(model.objUsers.objNewUser.objUser.Username) || CheckIsValidHtml(model.objUsers.objNewUser.objUser.FirstName)
                         || CheckIsValidHtml(model.objUsers.objNewUser.objUser.LastName) ))
                    {
                        return RedirectToAction("XSSRequestValidation", "Error");
                    }

                    if (model.objUsers.objNewUser.ModulePermisions[5].SelectedRole == 0 || (model.objUsers.objNewUser.ModulePermisions[0].SelectedRole != (int)RoleID.Collaborate_Administrator && model.objUsers.objNewUser.ModulePermisions[5].SelectedRole == (int)RoleID.Admin_Administrator))
                    {
                        model.objUsers.objNewUser.ModulePermisions[5].SelectedRole = (int)RoleID.Admin_NoAccess;
                    }
                }

                    if (model.objUsers.objEditUser == null)
					{ 
						ModelState["objUsers.objNewUser.objUser.Username"].Errors.Clear();
					}
					if (ModelState.IsValid)
					{
						if (model.objUsers.objEditUser != null)
						{
							int userId = model.objUsers.objEditUser.objUser.ID;
							if (userId > 0 &&
								!DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, LoggedAccount.ID, Context))
							{
								return RedirectToAction("Unauthorised", "Error");
							}
							//Edit User
							model.objUsers.objEditUser.AccountID = LoggedAccount.ID;
							if (model.objUsers != null && model.objUsers.objEditUser != null &&
								model.objUsers.objEditUser.ModulePermisions != null)
							{
								model.objUsers.objEditUser.InitializePermissions(model.objUsers.objEditUser.objUser.ID,
									model.objUsers.objEditUser.ModulePermisions.
										ToDictionary(o => o.ModuleType,
											o => (int?)o.SelectedRole), Context);
							}
							GMGColorDAL.User objUser =
										DALUtils.GetObject<GMGColorDAL.User>(model.objUsers.objEditUser.objUser.ID, Context);

							if (
								!this.ValidateEmail(this.LoggedAccount, model.objUsers.objEditUser.objUser.EmailAddress,
									objUser.EmailAddress))
							{
								hasError = true;
								dicModelErrors.Add("objUsers.objEditUser.objUser.EmailAddress",
									Resources.Resources.reqEmailExist);
							}
							if (!this.ValidateUserName(model.objUsers.objEditUser.objUser.Username, objUser.Username))
							{
								hasError = true;
								dicModelErrors.Add("objUsers.objEditUser.objUser.Username", Resources.Resources.reqUsernameExist);
							}

							if (model.objUsers.objEditUser.ModulePermisions != null &&
								(!model.objUsers.objEditUser.ModulePermisions.Any() ||
									model.objUsers.objEditUser.ModulePermisions.Any(
										m => !m.Roles.Select(r => r.RoleId).Contains(m.SelectedRole))))
							{
								hasError = true;
								dicModelErrors.Add("objUsers.objEditUser.ValidPermissionsRequired",
									Resources.Resources.reqPermisionsRequired);
							}

							if (hasError)
							{
								model.objUsers.objEditUser.LoggedAccountId = this.LoggedAccount.ID;
								TempData["editUserModel"] = model.objUsers.objEditUser;
								TempData["ModelErrors"] = dicModelErrors;
								return RedirectToAction("AddEditUser",
									new { SelectedUser = model.objUsers.objEditUser.objUser.ID });
							}
							else
							{
								List<int> lstGroupUsers = objUser.UserGroupUsers.Select(t => t.UserGroup).ToList();

								//chear cache if language is changed
								if (LoggedUser.ID == objUser.ID && objUser.Locale != model.objUsers.objEditUser.Locale)
								{
									ClearLoggedUserSecondaryNavigationCache();
								}

								using (TransactionScope ts = new TransactionScope())
								{
									objUser.GivenName = model.objUsers.objEditUser.objUser.IsSsoUser && string.IsNullOrEmpty(model.objUsers.objEditUser.objUser.FirstName) ? model.objUsers.objEditUser.objUser.EmailAddress : model.objUsers.objEditUser.objUser.FirstName;
									objUser.FamilyName = model.objUsers.objEditUser.objUser.IsSsoUser && string.IsNullOrEmpty(model.objUsers.objEditUser.objUser.LastName) ? "." : model.objUsers.objEditUser.objUser.LastName;

									objUser.Username = model.objUsers.objEditUser.objUser.Username;
									objUser.EmailAddress = model.objUsers.objEditUser.objUser.EmailAddress;
									objUser.Modifier = this.LoggedUser.ID;
									objUser.PrinterCompany = model.objUsers.objEditUser.objUser.PrintCompany;
									objUser.ModifiedDate = DateTime.UtcNow;
									objUser.Locale = model.objUsers.objEditUser.Locale;
                                    objUser.IsSsoUser = model.objUsers.objEditUser.objUser.IsSsoUser;

									if (model.objUsers.objEditUser.objUser.HasPasswordEnabled)
									{
										objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(model.objUsers.objEditUser.objUser.Password, Context);
                                        UserBL.UpdateLoginFailedDetails(model.objUsers.objEditUser.objUser.ID, Context);
                                    }
									
									// account owner could not be edited
									// current user could not be edited
									if ((objUser.Account1.Owner != objUser.ID && LoggedUser.ID != objUser.ID))
									{
										List<int> formRoles =
											model.objUsers.objEditUser.ModulePermisions.Select(o => o.SelectedRole).ToList();
										List<int> dbRoles = objUser.UserRoles.Select(o => o.Role).ToList();

										List<int> deleteRoles = dbRoles.Where(o => !formRoles.Contains(o)).ToList();
										List<int> newRoles = formRoles.Where(o => !dbRoles.Contains(o)).ToList();

										if (deleteRoles.Any() || newRoles.Any())
										{
											objUser.NeedReLogin = true;
										}

										foreach (int roleId in deleteRoles)
										{
											GMGColorDAL.UserRole userRoleBo =
												DALUtils.SearchObjects<GMGColorDAL.UserRole>(
													o => o.Role == roleId && o.User == objUser.ID, Context).FirstOrDefault();
											if (userRoleBo != null)
											{
												DALUtils.Delete<GMGColorDAL.UserRole>(Context, userRoleBo);
											}
										}

										foreach (int roleId in newRoles)
										{
											GMGColorDAL.Role roleBo =
												DALUtils.SearchObjects<GMGColorDAL.Role>(o => o.ID == roleId, Context)
													.FirstOrDefault();
											if (roleBo != null)
											{
												GMGColorDAL.UserRole userRoleBo = new GMGColorDAL.UserRole()
												{
													User = objUser.ID,
													Role = roleBo.ID
												};

												objUser.UserRoles.Add(userRoleBo);											
											}
										}
									}
                                bool IsManager = model.objUsers.objEditUser.ModulePermisions[0].Roles[1].RoleName == Role.RoleName.Manager.ToString() && model.objUsers.objEditUser.ModulePermisions[0].Roles[1].RoleChecked ? true : false;
                                if (IsManager)
                                {
                                    UserBL.AddEditUserGroupUserVisibility(model.objUsers.objEditUser.UserGroupsInUserList, objUser, Context);
                                }
                                    //Add,Editing UserGroup Users
                                if (model.objUsers.objEditUser.UserGroupsInUserList != null)
									{
										UserBL.AddEditUserGroupUsers(model.objUsers.objEditUser.UserGroupsInUserList, objUser,
											SelectedPrimaryGroup, lstGroupUsers, Context);
									}

									// Update user color
									objUser.ProofStudioUserColor = UserBL.ValidateProofStudioUserColor(LoggedAccount.ID, LoggedUser.ID, model.objUsers.objEditUser.ProofStudioColor, Context);
									if (objUser.ProofStudioUserColor == null)
									{
										throw new Exception("Invalid Proof Studio User Color");
									}

                                    objUser.PrivateAnnotations = model.objUsers.objEditUser.objUser.PrivateAnnotations;
									Context.SaveChanges();

									//check if max users is set for collaborate billing plan
									if (LoggedAccountBillingPlans.CollaborateBillingPlan != null && PlansBL.IsNewCollaborateBillingPlan(LoggedAccountBillingPlans.CollaborateBillingPlan))
									{
										if (PlansBL.GetActiveUsersNr(LoggedAccount.ID, AppModule.Collaborate, Context) > LoggedAccountBillingPlans.CollaborateBillingPlan.MaxUsers)
										{
											dicModelErrors.Add("", Resources.Resources.lblMaxUsersLimit);
											model.objUsers.objEditUser.LoggedAccountId = this.LoggedAccount.ID;
											TempData["editUserModel"] = model.objUsers.objEditUser;
											TempData["ModelErrors"] = dicModelErrors;
											return RedirectToAction("AddEditUser",
												new { SelectedUser = model.objUsers.objEditUser.objUser.ID });
										}
									}

									ts.Complete();
								}
							}
						}
						else
						{
							//Add New User
							model.objUsers.objNewUser.AccountID = LoggedAccount.ID;
							if (model.objUsers != null && model.objUsers.objNewUser != null &&
								model.objUsers.objNewUser.ModulePermisions != null)
							{
								model.objUsers.objNewUser.InitializePermissions(
									model.objUsers.objNewUser.ModulePermisions.ToDictionary(o => o.ModuleType,
										o => (int?)o.SelectedRole), Context);
							}

							if (
								!this.ValidateEmail(this.LoggedAccount, model.objUsers.objNewUser.objUser.EmailAddress,
									string.Empty))
							{
								hasError = true;
								dicModelErrors.Add("objUsers.objNewUser.objUser.EmailAddress", Resources.Resources.reqEmailExist);
							}
							else if (model.objUsers.objNewUser.ModulePermisions != null &&
										(!model.objUsers.objNewUser.ModulePermisions.Any() ||
										model.objUsers.objNewUser.ModulePermisions != null &&
										model.objUsers.objNewUser.ModulePermisions.Any(
											m => !m.Roles.Select(r => r.RoleId).Contains(m.SelectedRole))))
							{
								hasError = true;
								dicModelErrors.Add("ValidPermissionsRequired", Resources.Resources.reqPermisionsRequired);
							}

                            

							if (!hasError)
							{
								// Set user color
								ProofStudioUserColor proofStudioUserColor = UserBL.ValidateProofStudioUserColor(LoggedAccount.ID, LoggedUser.ID, model.objUsers.objNewUser.ProofStudioColor, Context);
								if (proofStudioUserColor == null)
								{
									proofStudioUserColor = UserBL.GetAvailableUserColor(LoggedAccount.ID, Context);
								}
								model.objUsers.objNewUser.ProofStudioColorID = proofStudioUserColor.ID;
								

								model.objUsers.objNewUser.SelectedPrimaryGroup = SelectedPrimaryGroup.GetValueOrDefault();
								hasError = !CreateNewUser(model.objUsers.objNewUser, Context, LoggedAccountBillingPlans.CollaborateBillingPlan);
								if (hasError)
								{
									dicModelErrors.Add("", Resources.Resources.lblMaxUsersLimit);
								}
							}

							if (hasError)
							{
								model.objUsers.objNewUser.LoggedAccountId = LoggedAccount.ID;
								TempData["ModelErrors"] = dicModelErrors;
								TempData["newUserModel"] = model.objUsers.objNewUser;
								return RedirectToAction("AddEditUser");
							}
						}
					}
				return RedirectToAction("Users");
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
                StringBuilder sb = new StringBuilder();

                if (model.objUsers.objEditUser != null)
                {
                    sb.Append(". Editing user { " + model.objUsers.objEditUser.objUser.ID + " }");
                }
                if (model.objUsers.objNewUser != null)
                {
                    sb.Append(". Adding user { " + model.objUsers.objNewUser.objUser.FirstName + " " + model.objUsers.objNewUser.objUser.LastName + " }");
                }

                sb.Append(" by user { " + LoggedUser.ID + " }");

                sb.Append(" on account { " + LoggedAccount.ID + " }.");

                GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while adding/editing user" + sb.ToString(), ex);
			}
			return RedirectToAction("UnexpectedError", "Error");
		}

		[HttpPost]
		public ActionResult DuplicateUser(DuplicateUser model)
		{
			if (ModelState.IsValid)
			{
				if (model.DuplicatedUser > 0 &&
					!DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(model.DuplicatedUser, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				if (LoggedUserAdminRole != Role.RoleName.AccountAdministrator && LoggedAccountType != AccountType.Type.GMGColor)
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				try
				{
					Dictionary<string, string> errors = new Dictionary<string, string>();
					if (ValidateEmail(LoggedAccount, model.EmailAddress, string.Empty))
					{
						var duplicatedUserIsSso = Context.Users.Find(model.DuplicatedUser).IsSsoUser;
						var duplicatedUser = UserBL.DuplicateUser(model, LoggedUser.ID, LoggedAccount, duplicatedUserIsSso,Context);

						Context.SaveChanges();

						if (!duplicatedUserIsSso)
						{
							BaseBL.SendInviteToUser(duplicatedUser.ID, LoggedUser.ID, LoggedAccount.ID, Context);
						}

						NotificationServiceBL.CreateNotification(new UserWasAdded
														{
															EventCreator = LoggedUser.ID,
															CreatedUserID = duplicatedUser.ID,
															Account = LoggedAccount.ID
														},
														LoggedUser,
														Context
													);
					}
					else
					{
						errors.Add("EmailAddress", Resources.Resources.reqEmailExist);
						TempData["DuplicateUserErrors"] = errors;
						TempData["DuplicateUserModel"] = model;
					}
				}
				catch (DbEntityValidationException ex)
				{
					DALUtils.LogDbEntityValidationException(ex);
				}
				catch (Exception ex)
				{
					GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while duplicating user", ex);
				}
			}

			return RedirectToAction("Users");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "ResetPassword")]
		public ActionResult ResetPassword(AccountSettings.AddEditUser model)
		{
			try
			{
                if (model.objUsers.objEditUser.objUser.ID > 0 &&
                     !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(model.objUsers.objEditUser.objUser.ID, LoggedAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(model.objUsers.objEditUser.objUser.ID, Context);

				string password = GMGColorDAL.User.GetNewRandomPassword();
				objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(password, Context);
				objUser.NeedReLogin = true;
                UserBL.UpdateLoginFailedDetails(model.objUsers.objEditUser.objUser.ID, Context);
                Context.SaveChanges();

				NotificationServiceBL.CreateNotification(new ResetPassword
																{
																	EventCreator = LoggedAccount.Owner,
																	InternalRecipient = objUser.ID,
																	CreatedDate = DateTime.UtcNow,
																	NewPassword = password
																},
																null,
																Context
																);
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while reseting the password in ResetPassword()", ex);
			}
			return RedirectToAction("Users");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteUser")]
		public ActionResult DeleteUser(string SelectedUser)
		{
			bool success = true;
			Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

			int userId = SelectedUser.ToInt().GetValueOrDefault();
			if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, this.LoggedAccount.ID, Context))
			{
				return RedirectToAction("Unauthorised", "Error");
			}

			if (UserBL.IsApprovalWorkflowOwner(userId, Context))
			{
				success = false;
				ModelState.AddModelError(string.Empty, Resources.Resources.lblUserIsOwneronApprovalWorkflow);
			}

			if (UserBL.IsTemplatePhasePrimaryDecisionMaker(userId, false, Context) || UserBL.IsPendingPhasePrimaryDecisionMaker(userId, false, Context))
			{
				success = false;
				ModelState.AddModelError(string.Empty, Resources.Resources.lblUserIsPDMOnAnApprovalPhaseNotDeleted);
			}

			if (!success)
			{
				foreach (string key in ModelState.Keys)
				{
					if (!ModelState[key].Errors.Any())
						continue;

					errors.Add(key, ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
				}
				TempData[InternalUsersErrors] = errors;
			}
			else
			{
				this.DeleteUser(int.Parse(SelectedUser));
			}
			
			return RedirectToAction("Users");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteExUser")]
		public ActionResult DeleteExUser(string SelectedUser)
		{
			bool success = true;
			Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

			int userId = SelectedUser.ToInt().GetValueOrDefault();
			if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(userId, LoggedAccount.ID, Context))
			{
				// if trying to delete an external user from another account
				return RedirectToAction("Unauthorised", "Error");
			}

			if (UserBL.IsTemplatePhasePrimaryDecisionMaker(userId, true, Context) || UserBL.IsPendingPhasePrimaryDecisionMaker(userId, true, Context))
			{
				success = false;
				ModelState.AddModelError(string.Empty, Resources.Resources.lblUserIsPDMOnAnApprovalPhaseNotDeleted);
			}

			if (!success)
			{
				foreach (string key in ModelState.Keys)
				{
					if (!ModelState[key].Errors.Any())
						continue;

					errors.Add(key, ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
				}
				TempData[ExternalUsersErrors] = errors;
			}
			else
			{
				this.DeleteExUser(int.Parse(SelectedUser));
			}
		   
			return RedirectToAction("ExternalUsers");
		}

		[HttpPost]
		public ActionResult UpdateExternalCollaborator(ExternalUserDetails extUser, int? page)
		{
			try
			{
				if ((extUser.UserType == Enums.ExternalUserType.Collaborate && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(extUser.ID, LoggedAccount.ID, Context)) ||
					(extUser.UserType == Enums.ExternalUserType.Deliver && !DALUtils.IsFromCurrentAccount<GMGColorDAL.DeliverExternalCollaborator>(extUser.ID, LoggedAccount.ID, Context)))
				{
					// if trying to delete an external user from another account
					return RedirectToAction("Unauthorised", "Error");
				}

				ExternalCollaboratorBL.UpdateExternalCollaborator(extUser, Context);
				Context.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, String.Format("An error occured while updating external user with id {0} in UpdateExternalCollaborator()", extUser.ID), ex);
			}

			return RedirectToAction("ExternalUsers", new { page, type = extUser.UserType });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "RevokeAccess")]
		public ActionResult RevokeAccess(string SelectedUser)
		{
			try
			{
				int userId = SelectedUser.ToInt().GetValueOrDefault();
				if (userId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(userId, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}
				int exUser = int.Parse(SelectedUser);
				if (exUser > 0)
				{
					GMGColorDAL.ExternalCollaborator objExUser =
						DALUtils.GetObject<GMGColorDAL.ExternalCollaborator>(exUser, Context);

					using (TransactionScope ts = new TransactionScope())
					{
						foreach (int sharedApprovalId in objExUser.SharedApprovals.Select(o => o.ID).ToList())
						{
							GMGColorDAL.SharedApproval objSharedApproval =
								DALUtils.GetObject<GMGColorDAL.SharedApproval>(sharedApprovalId, Context);

							objSharedApproval.IsExpired = true;
						}

						Context.SaveChanges();
						ts.Complete();
					}
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while revoking access for external user in RevokeAccess()", ex);
			}
			finally
			{
			}

			return RedirectToAction("ExternalUsers");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "GrantAccess")]
		public ActionResult GrantAccess(string SelectedUser)
		{
			try
			{
				int exUser = SelectedUser.ToInt().GetValueOrDefault();
				if (exUser > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.ExternalCollaborator>(exUser, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}
				if (exUser > 0)
				{
					GMGColorDAL.ExternalCollaborator objExUser =
						DALUtils.GetObject<GMGColorDAL.ExternalCollaborator>(exUser, Context);

					using (TransactionScope ts = new TransactionScope())
					{
						List<GMGColorDAL.SharedApproval> lstNonExpiredSharedApprovals = objExUser.SharedApprovals.Where(o => DateTime.Compare(o.ExpireDate, DateTime.Now) > 0).ToList();
						if (lstNonExpiredSharedApprovals.Count > 0)
						{
							foreach (int sharedApprovalId in lstNonExpiredSharedApprovals.Select(o => o.ID).ToList())
							{
								GMGColorDAL.SharedApproval objSharedApproval =
									DALUtils.GetObject<GMGColorDAL.SharedApproval>(sharedApprovalId, Context);

								objSharedApproval.IsExpired = false;
							}

							Context.SaveChanges();
							ts.Complete();
						}
					}
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while granting access for external user in GrantAccess()", ex);
			}
			finally
			{
			}

			return RedirectToAction("ExternalUsers");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "NewGroup")]
		public ActionResult NewGroup()
		{
			Session["GMGC_GroupID"] = null;
			return RedirectToAction("AddEditUserGroup");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "EditGroup")]
		public ActionResult EditGroup(string SelectedGroup)
		{
			int groupId = SelectedGroup.ToInt().GetValueOrDefault();
			if (groupId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.UserGroup>(groupId, LoggedAccount.ID, Context))
			{
				return RedirectToAction("Unauthorised", "Error");
			}
			Session["GMGC_GroupID"] = SelectedGroup;
			return RedirectToAction("AddEditUserGroup");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteGroup")]
		public ActionResult DeleteGroup(string SelectedGroup)
		{
			try
			{
				bool success = true;
				Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

				int groupId = SelectedGroup.ToInt().GetValueOrDefault();
				if (groupId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.UserGroup>(groupId, LoggedAccount.ID, Context))
				{
					return RedirectToAction("Unauthorised", "Error");
				}

				bool CanDelete= DALUtils.SearchObjects<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(o => o.ID > 0, Context).All(o => o.UserGroup != groupId);

				if (!CanDelete)
				{
					success = false;
					ModelState.AddModelError(string.Empty, Resources.Resources.lblUserGroupLinkedToAWorkflowNotDeleted);
				}

				var isUserGroupLinkedToBrandingPreset = SettingsBL.GetUserGroupBrandingPreset(groupId, Context);
				if (isUserGroupLinkedToBrandingPreset)
				{
					success = false;
					ModelState.AddModelError(string.Empty, Resources.Resources.lblUserGroupLinkedToABrandingPresetNotDeleted);
				}

				if (!success)
				{
					foreach (string key in ModelState.Keys)
					{
						if (!ModelState[key].Errors.Any())
							continue;

						errors.Add(key, ModelState[key].Errors.Select(o => o.ErrorMessage).ToList());
					}
					TempData[SUserGroupsErrors] = errors;
					return RedirectToAction("UserGroups");
				}
				
				GMGColorDAL.UserGroup objUserGroup = DALUtils.GetObject<GMGColorDAL.UserGroup>(groupId, Context);
				using (TransactionScope ts = new TransactionScope())
				{
					List<ApprovalCollaboratorGroup> apcGr = objUserGroup.ApprovalCollaboratorGroups.ToList();
					for (int i = apcGr.Count - 1; i >= 0; i--)
					{
						GMGColorDAL.ApprovalCollaboratorGroup objACG =
							DALUtils.GetObject<GMGColorDAL.ApprovalCollaboratorGroup>(apcGr[i].ID, Context);

						DALUtils.Delete<GMGColorDAL.ApprovalCollaboratorGroup>(Context, objACG);
					}

					List<FolderCollaboratorGroup> fcGr = objUserGroup.FolderCollaboratorGroups.ToList();
					for (int i = fcGr.Count - 1; i >= 0; i--)
					{
						GMGColorDAL.FolderCollaboratorGroup objFCG =
							DALUtils.GetObject<GMGColorDAL.FolderCollaboratorGroup>(fcGr[i].ID, Context);

						DALUtils.Delete<GMGColorDAL.FolderCollaboratorGroup>(Context, objFCG);
					}

					List<UploadEngineUserGroupPermission> ueprm = objUserGroup.UploadEngineUserGroupPermissions.ToList();
					for (int i = ueprm.Count - 1; i >= 0; i--)
					{
						XMLEngineBL.DeleteUserGroupPermissions(ueprm[i], Context);
					}

					Context.ApprovalPhaseCollaboratorGroups.RemoveRange(Context.ApprovalPhaseCollaboratorGroups.Where(acg => acg.CollaboratorGroup == groupId));

					foreach (var usgu in objUserGroup.UserGroupUsers.ToList())
					{
						Context.UserGroupUsers.Remove(usgu);
					}

					foreach (var nsug in objUserGroup.NotificationScheduleUserGroups.ToList())
					{
						Context.NotificationScheduleUserGroups.Remove(nsug);
					}

					DALUtils.Delete<GMGColorDAL.UserGroup>(Context, objUserGroup);

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while deleting a group in DeleteGroup()", ex);
			}
			finally
			{
			}
			return RedirectToAction("UserGroups");
		}

		[HttpPost]
		public ActionResult AddEditReserveDomain(AccountSettings.ReservedDomains model)
		{
			try
			{
				using (TransactionScope ts = new TransactionScope())
				{
					GMGColorDAL.ReservedDomainName objReservedDomain;
					if (model.ID > 0)
					{
						objReservedDomain = GMGColorDAL.DALUtils.GetObject<GMGColorDAL.ReservedDomainName>(model.ID, Context);
					}
					else
					{
						objReservedDomain = new ReservedDomainName();
					}

					objReservedDomain.Name = model.Domain;


					if (model.ID == 0)
					{
						Context.ReservedDomainNames.Add(objReservedDomain);
					}

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while adding-editing reserved doamin name in AddEditReserveDomain()", ex);
			}
			finally
			{
			}

			return RedirectToAction("ReservedDomains");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "EditReservedDomain")]
		public ActionResult EditReservedDomain(string SelectedDomain)
		{
			Session["SelectedDomain"] = SelectedDomain;
			return RedirectToAction("ReservedDomains");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteReservedDomain")]
		public ActionResult DeleteReservedDomain(string SelectedDomain)
		{
			try
			{
				int domain = SelectedDomain.ToInt().GetValueOrDefault();

				GMGColorDAL.ReservedDomainName objReservedDomain = DALUtils.GetObject<GMGColorDAL.ReservedDomainName>(domain, Context);

				using (TransactionScope ts = new TransactionScope())
				{
					DALUtils.Delete<GMGColorDAL.ReservedDomainName>(Context, objReservedDomain);
					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while deleting a reserved domain in DeleteReservedDomain()", ex);
			}
			finally
			{
			}

			return RedirectToAction("ReservedDomains");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SaveSettings")]
		public ActionResult SaveSettings(GMGColorDAL.CustomModels.Settings model)
		{
			try
			{
				if (ModelState.IsValid)
				{
					using (TransactionScope ts = new TransactionScope())
					{
						var storeCompleted = GMGColorDAL.DALUtils.SearchObjects<GMGColorDAL.GlobalSetting>(o => o.Key == "STCF", Context).SingleOrDefault();
						if (storeCompleted != null)
						{
							storeCompleted.Value = model.StoreCompletedFor.ToString();
						}

						var storeArchived = GMGColorDAL.DALUtils.SearchObjects<GMGColorDAL.GlobalSetting>(o => o.Key == "STAF", Context).SingleOrDefault();
						if (storeArchived != null)
						{
							storeArchived.Value = model.StoreArchivedFor.ToString();
						}

						Context.SaveChanges();
						ts.Complete();
					}
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while updating global settings. Settings-SaveSettings()", ex);
			}
			finally
			{
			}

			return RedirectToAction("Global");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SwitchToCollaborate")]
		public ActionResult SwitchToCollaborate()
		{
			return RedirectToAction("ExternalUsers", new { type = Enums.ExternalUserType.Collaborate });
		}


		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "SwitchToDeliver")]
		public ActionResult SwitchToDeliver()
		{
			return RedirectToAction("ExternalUsers", new { type = Enums.ExternalUserType.Deliver });
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteDeliverExUser")]
		public ActionResult DeleteDeliverExUser(int selectedUser, bool isDeliverExternalUser)
		{
			try 
			{
                if(!DALUtils.IsFromCurrentAccount<GMGColorDAL.DeliverExternalCollaborator>(selectedUser, LoggedAccount.ID, Context))
				{
                    return RedirectToAction("Unauthorised", "Error");
                }
				using (var ts = new TransactionScope())
				{
					if (isDeliverExternalUser)
					{
						var deliverExternalCollaborator = DeliverExternalCollaborator.GetDeliverExternalCollaboratorById(selectedUser, Context);

						Context.DeliverSharedJobs.RemoveRange(deliverExternalCollaborator.DeliverSharedJobs);
						
						Context.DeliverExternalCollaborators.Remove(deliverExternalCollaborator);
					}
					else
					{
						//Remove all ColorProof Servers and Workflows that are shared to this user.
						var workflowsToDelete = SharedColorProofWorkflow.GetSharedWorkflowsByExUserID(selectedUser, LoggedAccount.ID, Context);

						foreach (var sharedColorProofWorkflow in workflowsToDelete)
						{
							bool accountIsActive;
							//Delete usergroups from the shared workflow account
							List<ColorProofCoZoneWorkflowUserGroup> sharedWfGroups =
								DeliverBL.GetWfUserGroupsListByWorkflowAndAccount(sharedColorProofWorkflow.ColorProofCoZoneWorkflow,
									UserBL.GetAccountIdByUser(sharedColorProofWorkflow.User, out accountIsActive, Context), Context);

							for (int i = 0; i < sharedWfGroups.Count; i++)
							{
								Context.ColorProofCoZoneWorkflowUserGroups.Remove(sharedWfGroups[i]);
							}

							Context.SharedColorProofWorkflows.Remove(sharedColorProofWorkflow);
						}
					}
					
					Context.SaveChanges();
					ts.Complete();
				}

				if (!isDeliverExternalUser)
				{
					NotificationServiceBL.CreateNotification(new HostAdminUserDeleted()
															{
																EventCreator = LoggedUser.ID,
																InternalRecipient = LoggedUser.ID,
																DeletedUserID = selectedUser
															},
															 LoggedUser,
															 Context
						);

					NotificationServiceBL.CreateNotification(new InvitedUserDeleted()
															{
																EventCreator = LoggedUser.ID,
																InternalRecipient = selectedUser,
																DeletedUserID = selectedUser
															},
															 LoggedUser,
															 Context
						);
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while deleting a deliver external user in DeleteDeliverExUser()", ex);
			}
			return RedirectToAction("ExternalUsers", new { type = Enums.ExternalUserType.Deliver });
		}

		[HttpGet]
		public ActionResult BouncedEmailUsers()
		{
			var model = new List<BouncedEmailUsers>();
			try
			{
				model = GMGColorBusinessLogic.SettingsBL.GetBouncedEmailUsers(LoggedAccount.ID);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while accessing Bounced Email Users in BouncedEmailUsers()", ex);
			}
			return View(model);
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteBounceBlock")]
		public ActionResult DeleteBounceBlock(int selectedMessageId, bool isExternal)
		{
			try
			{
				GMGColorBusinessLogic.SettingsBL.DeleteBouncedEmailBlock(selectedMessageId, isExternal, LoggedAccount.ID);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while removing bounce email flag in RemoveBouncedEmailUser()", ex);
			}
			return RedirectToAction("BouncedEmailUsers");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "AddUpdateSmtp")]
		public ActionResult CustomSmtpServer(SmtpSettingsViewModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
                    var domainModel = _mapper.Map<SmtpSettingsViewModel, SmtpSettingsModel>(model);
					_settingsService.SmtpSettings.AddUpdate(domainModel, LoggedAccount.ID, LoggedAccount.Guid, Enum.GetName(typeof(AccountSettingsBL.AccountSettingValueType), AccountSettingsBL.AccountSettingValueType.STRG));
				}
				catch (Exception ex)
				{
					GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while saving Custom SMTP Settings", ex);
				}
			}
			return RedirectToAction("CustomSmtpServer", "Settings");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DisableCustomSmtp")]
		public ActionResult DisableCustomSmtpServer()
		{
			try
			{
				_settingsService.SmtpSettings.Disable(LoggedAccount.ID);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while reset Custom SMTP Settings", ex);
			}

			return RedirectToAction("CustomSmtpServer", "Settings");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "AddUpdateSso")]
		public ActionResult SingleSignOn(SsoViewModel model, string hdnFileName_1)
		{
            if (!(CheckIsValidHtml(model.Auth0.Domain) || CheckIsValidHtml(model.Auth0.ClientId) || CheckIsValidHtml(model.Auth0.ClientSecret)))
            {
                return RedirectToAction("XSSRequestValidation", "Error");
            }

            model.Saml.CertificateName = hdnFileName_1;
			var accountSettingsData = new AccountSettingsDataModel
			{
				AccountGuid = LoggedAccount.Guid,
				AccountId = LoggedAccount.ID,
				AccountTempPath = LoggedUserTempFolderPath,
				SettingValueType = Enum.GetName(typeof(AccountSettingsBL.AccountSettingValueType), AccountSettingsBL.AccountSettingValueType.STRG),
				AccountRegion = LoggedAccount.Region,
				UserId = LoggedUser.ID
			};

            if (model.ActiveConfiguration == GMG.CoZone.Common.Module.Enums.SsoOptionsEnum.Saml)
            {
                this.ModelState.Remove("Auth0.Domain");
                this.ModelState.Remove("Auth0.ClientId");
                this.ModelState.Remove("Auth0.ClientSecret"); 
            }
            else
            {
                this.ModelState.Remove("Saml.EntityId");
                this.ModelState.Remove("Saml.IdpIssuerUrl");
            }

            if (ModelState.IsValid)
			{
				try
				{
                    var domainModel = _mapper.Map<SsoViewModel, SsoModel>(model);
					_settingsService.SsoSettings.AddUpdate(domainModel, accountSettingsData);
				}
				catch (Exception ex)
				{
					GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while savind Single Sign On settings", ex);
				}
			}
			return RedirectToAction("SingleSignOn", "Settings");
		}

		[HttpPost]
		[MultiButton(MatchFormKey = "action", MatchFormValue = "DisableSso")]
		public ActionResult DisableSingleSingOn()
		{
			try
			{
				_settingsService.SsoSettings.Disable(LoggedAccount.ID, LoggedAccount.Region, LoggedAccount.Guid);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(LoggedAccount.ID, "An error occured while reset Single Sign On Settings", ex);
			}

			return RedirectToAction("SingleSignOn", "Settings");
		}

		#endregion

		#region Methods

		/// <summary>
		/// Deletes the user.
		/// </summary>
		/// <param name="userId">The user id.</param>
		private void DeleteUser(int userId)
		{
			try
			{
				GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(userId, Context);

				using (TransactionScope ts = new TransactionScope())
				{
					objUser.Username = objUser.Username + "_" + objUser.Guid;
					//objUser.EmailAddress = objUser.EmailAddress + "_" + objUser.Guid;
					objUser.Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Deleted, Context).ID;

					foreach (var usgu in objUser.UserGroupUsers.ToList())
					{
						Context.UserGroupUsers.Remove(usgu);
					}

					foreach (var nsu in objUser.NotificationScheduleUsers.ToList())
					{
						Context.NotificationScheduleUsers.Remove(nsu);
					}

					objUser.NeedReLogin = true;

					// mark all the workstations from this user as deleted
					var workstations = (from ws in Context.Workstations where ws.User == userId select ws).ToList();
					foreach (GMGColorDAL.Workstation workstation in workstations)
					{
						workstation.IsDeleted = true;
					}

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while deleting a user in DeleteUser()", ex);
			}
			finally
			{
			}
		}

		/// <summary>
		/// Deletes the ex user.
		/// </summary>
		/// <param name="userId">The user id.</param>
		private void DeleteExUser(int userId)
		{
			try
			{
				GMGColorDAL.ExternalCollaborator objExUser = DALUtils.GetObject<GMGColorDAL.ExternalCollaborator>(userId, Context);

				using (TransactionScope ts = new TransactionScope())
				{
					objExUser.EmailAddress = objExUser.EmailAddress + "_" + objExUser.Guid;
					objExUser.IsDeleted = true;

					Context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while deleting a external user in DeleteExUser()", ex);
			}
			finally
			{
			}
		}

		/// <summary>
		/// Saves the color proof instance.
		/// </summary>
		/// <param name="instance">The instance.</param>
		/// <returns></returns>
		private int? SaveColorProofInstance(ColorProofSettings.ColorProofInstance instance, DbContextBL context)
		{
			ColorProofInstance objColorProofInstance = null;
			try
			{
				bool sendPairingCodeEmail = false;
				using (TransactionScope ts = new TransactionScope())
				{
					objColorProofInstance = instance.ID > 0
												? DALUtils.SearchObjects<ColorProofInstance>(o => o.ID == instance.ID, context).FirstOrDefault()
												: new ColorProofInstance() { Guid = Guid.NewGuid().ToString(), ID = 0 };

					if (objColorProofInstance == null)
						throw new Exception("Colorproof instance with ID='" + instance.ID + "' could not be found in database!");

					objColorProofInstance.SystemName = instance.CoZoneName.TrimStart().TrimEnd();
					objColorProofInstance.AdminName = instance.AdminName;
					objColorProofInstance.Address = instance.Address;
					objColorProofInstance.Email = instance.Email;

					if (instance.ID > 0)
					{
						//Edit Mode

						if (instance.IsUserPassLogin)
						{
							// User/Pass Mode
							if (instance.IsChangeLogin)
							{
								objColorProofInstance.UserName = instance.ColorProofUsername;
								string password = instance.ColorProofPassword;
								objColorProofInstance.Password = ColorProofInstance.GetEncryptedPassword(password, Context);
							}

							objColorProofInstance.PairingCode = null;
						}
						else
						{
							//Pairing Code Mode
							if (objColorProofInstance.PairingCode != instance.PairingCode)
							{
								if (SettingsBL.IsValidPairingCode(instance.PairingCode, GMGColorConfiguration.AppConfiguration.ColorProofPairingCodeRegion, Context))
								{
									objColorProofInstance.PairingCode = instance.PairingCode;
									objColorProofInstance.PairingCodeExpirationDate = SettingsBL.GetExpirationTime();
									sendPairingCodeEmail = true;
								}
							}
						}
					}
					else
					{
						//New Instance

						int instStateID =
						   DeliverBL.GetColorProofInstanceStateID(
							   ColorProofInstance.ColorProofInstanceStateEnum.Pending, Context);

						if (instStateID == 0)
						{
							throw new Exception("Colorproof instance status Waiting is not present in the database!");
						}

						objColorProofInstance.Owner = LoggedUser.ID;
						objColorProofInstance.State = instStateID;

						context.ColorProofInstances.Add(objColorProofInstance);

						string password;
						if (instance.IsUserPassLogin)
						{
							// User/Pass Mode
							objColorProofInstance.UserName = instance.ColorProofUsername;
							password = instance.ColorProofPassword;
						}
						else
						{
							//PairingCodeMode
							objColorProofInstance.PairingCode = instance.PairingCode;
							objColorProofInstance.PairingCodeExpirationDate = SettingsBL.GetExpirationTime();
							sendPairingCodeEmail = true;

							// Generate new instance credentials
							objColorProofInstance.UserName = objColorProofInstance.SystemName + "_" + LoggedUser.ID;
							SettingsBL.GenerateInstancePassword(out password);
							int index = 0;
							while (!ColorProofInstance.IsCoZoneUsernameUnique(objColorProofInstance.UserName, Context))
							{
								objColorProofInstance.UserName = objColorProofInstance.SystemName + "_" + index++;
								SettingsBL.GenerateInstancePassword(out password);
							}
						}

						objColorProofInstance.Password = ColorProofInstance.GetEncryptedPassword(password, Context);
					}

					context.SaveChanges();
					ts.Complete();
				}

				//Send notification to CP Admin with pairing code after a new CPInstance has been added
				if (sendPairingCodeEmail)
				{

					NotificationServiceBL.CreateNotification(new NewDeliverPairingCode
																{
																	EventCreator = LoggedUser.ID,
																	InternalRecipient = null,
																	CreatedDate = DateTime.UtcNow,
																	PairingCode = instance.PairingCode,
																	CPInstanceName = instance.CoZoneName,
																	IsNewCPInstance = true,
																	ReceiverEmail = instance.Email
																},
															   null,
															   Context
															   );
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "BaseController.SaveColorProofInstance : error occured while sending creating a ColorProof instance. {0}", ex.Message);
				objColorProofInstance = null;
			}
			return objColorProofInstance != null ? (Int32?)objColorProofInstance.ID : null;
		}


		/// <summary>
		/// Saves the color proof workflow.
		/// </summary>
		/// <param name="instance">The instance.</param>
		/// <returns></returns>
		private int? SaveColorProofWorkflow(ColorProofSettings.ColorProofWorkflow instance, bool isOwner, DbContextBL context)
		{
			GMGColorDAL.ColorProofCoZoneWorkflow objColorProofWorkflow = null;
			try
			{
				using (TransactionScope ts = new TransactionScope())
				{
					objColorProofWorkflow = instance.ID > 0
												? DALUtils.SearchObjects<GMGColorDAL.ColorProofCoZoneWorkflow>(o => o.ID == instance.ID, context).FirstOrDefault()
												: new GMGColorDAL.ColorProofCoZoneWorkflow();

					if (objColorProofWorkflow == null)
						throw new Exception("Colorproof workflow with ID='" + instance.ID + "' was not found in database!");

					//only owner can make changes
					if (isOwner)
					{
						objColorProofWorkflow.Name = instance.Name.TrimStart().TrimEnd();
						objColorProofWorkflow.TransmissionTimeout = instance.TransmissionTimeout;
						objColorProofWorkflow.IsAvailable = instance.IsAvailable;

						if (objColorProofWorkflow.ID == 0)
						{
							objColorProofWorkflow.ColorProofWorkflow = instance.SelectedCPWorkflowId.GetValueOrDefault();
							context.ColorProofCoZoneWorkflows.Add(objColorProofWorkflow);
						}
					}

					if (instance.ListUserGroups != null)
					{
						if (instance.ID > 0)
						{
							#region Update user groups

							//Get wf user groups from the database for the current account
							List<GMGColorDAL.UserGroup> dbUserGroups =
								DeliverBL.GetUserGroupsListByWorkflowAndAccount(objColorProofWorkflow.ID,
									LoggedAccount.ID, context);

							List<GMGColorDAL.UserGroup> selectedUserGroupInfoList =
								instance.ListUserGroups.Where(t => t.IsUserGroupInUser == true).Select(
									t =>
									DALUtils.SearchObjects<GMGColorDAL.UserGroup>(o => o.ID == t.objUserGroup.ID, context).FirstOrDefault()).ToList();

							List<GMGColorDAL.UserGroup> deletedUserGroups =
								dbUserGroups.Where(u => selectedUserGroupInfoList.All(u2 => u2.ID != u.ID)).ToList();

							List<GMGColorDAL.UserGroup> addedUserGroups =
								selectedUserGroupInfoList.Where(u => dbUserGroups.All(u2 => u2.ID != u.ID)).ToList();


							foreach (var userGroup in deletedUserGroups)
							{
								GMGColorDAL.ColorProofCoZoneWorkflowUserGroup item =
									DALUtils.SearchObjects<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(o => o.UserGroup == userGroup.ID && o.ColorProofCoZoneWorkflow == instance.ID, Context).FirstOrDefault();
								if (item != null)
								{
									DALUtils.Delete<GMGColorDAL.ColorProofCoZoneWorkflowUserGroup>(context, item);
								}
							}

							foreach (GMGColorDAL.UserGroup userGroup in addedUserGroups)
							{
								GMGColorDAL.ColorProofCoZoneWorkflowUserGroup item =
									new GMGColorDAL.ColorProofCoZoneWorkflowUserGroup() { UserGroup = userGroup.ID, ColorProofCoZoneWorkflow = objColorProofWorkflow.ID };
								context.ColorProofCoZoneWorkflowUserGroups.Add(item);
							}
							#endregion
						}
						else
						{
							#region Add user groups
							foreach (UserGroupsInUser userGroup in instance.ListUserGroups.Where(t => t.IsUserGroupInUser == true))
							{
								GMGColorDAL.UserGroup group =
									DALUtils.GetObject<GMGColorDAL.UserGroup>(userGroup.objUserGroup.ID, context);

								GMGColorDAL.ColorProofCoZoneWorkflowUserGroup objCoZoneWfUserGroup = new GMGColorDAL.ColorProofCoZoneWorkflowUserGroup();
								objCoZoneWfUserGroup.UserGroup = group.ID;

								objColorProofWorkflow.ColorProofCoZoneWorkflowUserGroups.Add(objCoZoneWfUserGroup);
							}
							#endregion
						}
					}

					context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "BaseController.SaveColorProofWorkflow : error occurred while sending creating a ColorProof workflow. {0}", ex.Message);
				objColorProofWorkflow = null;
			}
			finally
			{
			}
			return objColorProofWorkflow != null ? (Int32?)objColorProofWorkflow.ID : null;
		}

		/// <summary>
		/// Add/Update the account notification preset
		/// </summary>
		/// <param name="notificationPreset"></param>
		/// <param name="events"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		private int? SaveAccountNotificationPreset(NotificationPreset notificationPreset, List<NotificationEventType> events, DbContextBL context)
		{
			AccountNotificationPreset accountNotificationPreset = null;
			try
			{
				using (var ts = new TransactionScope())
				{
					accountNotificationPreset = notificationPreset.ID > 0
												? GlobalNotificationBL.GetAccountNotificationPreset(notificationPreset.ID, Context)
												: new AccountNotificationPreset() { ID = 0 };

					if (accountNotificationPreset == null)
						throw new Exception("Notification preset with ID='" + notificationPreset.ID + "' could not be found in database!");

					accountNotificationPreset.Name = notificationPreset.Name;
					accountNotificationPreset.DisablePersonalNotifications = notificationPreset.DisablePersonalNotifications;

					if (notificationPreset.ID > 0)
					{
						List<int> deliverEvents = DeliverBL.GetDeliverEventsIds(context);
						//check whether there is at least one deliver event checked for this preset
						var deliverIsChecked = events.Where(eventtype => deliverEvents.Contains(eventtype.ID)).Any(eventtype => eventtype.IsChecked);

						//if all deliver events are unchecked delete the relations between preset and deliver shared jobs, if any
						if (!deliverIsChecked)
						{
							var deliverJobShares = DeliverBL.GetDeliverJobSharesByPresetId(notificationPreset.ID, context);

							if (deliverJobShares != null && deliverJobShares.Count > 0)
							{
								context.DeliverSharedJobs.RemoveRange(deliverJobShares);
							}
						}

						//Update Preset
						foreach (var eventType in events)
						{
							bool existsInDatabase = GlobalNotificationBL.EventExistsInDB(eventType.ID, notificationPreset.ID, context);

							if (eventType.IsChecked && !existsInDatabase)
							{
								var accountNotificationPresetEventType = new AccountNotificationPresetEventType()
								{
									NotificationPreset = notificationPreset.ID,
									EventType = eventType.ID,
									ModifiedBy = LoggedUser.ID,
									ModifiedDate = DateTime.UtcNow
								};

								context.AccountNotificationPresetEventTypes.Add(accountNotificationPresetEventType);
							}
							else if (!eventType.IsChecked && existsInDatabase)
							{
								var presetEventType = GlobalNotificationBL.GetAccountNotificationPresetEventType(notificationPreset.ID, eventType.ID, context);

								if (presetEventType != null)
								{
									context.AccountNotificationPresetEventTypes.Remove(presetEventType);
								}
							}
						}

						accountNotificationPreset.ModifiedBy = LoggedUser.ID;
						accountNotificationPreset.ModifiedDate = DateTime.UtcNow;
					}
					else
					{
						//New Preset
						accountNotificationPreset.Account = LoggedAccount.ID;

						var selectedEvents = events.Where(e => e.IsChecked);
						foreach (var selectedEvent in selectedEvents)
						{
							accountNotificationPreset.AccountNotificationPresetEventTypes.Add(new AccountNotificationPresetEventType()
							{
								NotificationPreset = accountNotificationPreset.ID,
								EventType = selectedEvent.ID,
								CreatedBy =  LoggedUser.ID,
								ModifiedBy = LoggedUser.ID,
								CreatedDate = DateTime.UtcNow,
								ModifiedDate = DateTime.UtcNow
							});
						}

						accountNotificationPreset.CreatedBy =
							accountNotificationPreset.ModifiedBy = LoggedUser.ID;
						accountNotificationPreset.CreatedDate =
							accountNotificationPreset.ModifiedDate = DateTime.UtcNow;

						context.AccountNotificationPresets.Add(accountNotificationPreset);
					}

					context.SaveChanges();
					ts.Complete();
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "BaseController.SaveAccountNotificationPreset : error occured while saving Preset. {0}", ex.Message);
				accountNotificationPreset = null;
			}
			return accountNotificationPreset != null ? (Int32?)accountNotificationPreset.ID : null;
		}

       private void PopulateProofStudioUserColors(ProofStudioUserColor color, int userID)
		{
			//Show colors if logged account is admin
			if (LoggedAccountType == AccountType.Type.GMGColor || LoggedUserAdminRole == Role.RoleName.AccountAdministrator)
			{
				ViewBag.AvailableColors = UserBL.GetAvailableUserColors(userID, LoggedAccount.ID, Context);
				if (color != null)
				{
					ViewBag.ProofStudioColor = color.HEXValue;
				}
			}
		}
		
		private void UpdateContactSupportEmailSetting(AccountSettings.WhiteLabelSettings model, GMGColorDAL.AccountSetting objAccountSetting)
		{
			string contactSupportEmailKey = Enum.GetName(typeof(AccountSettingsBL.AccountSettingsKeyEnum), AccountSettingsBL.AccountSettingsKeyEnum.ContactSupportEmail);

			if (objAccountSetting != null)
			{
				objAccountSetting.Account = model.objWhiteLabelSettings.AccountID;
				objAccountSetting.Name = contactSupportEmailKey;
				objAccountSetting.Value = model.objWhiteLabelSettings.ContactSupportEmail != null ? model.objWhiteLabelSettings.ContactSupportEmail : String.Empty;
				objAccountSetting.ValueDataType = 1;
				objAccountSetting.Description = contactSupportEmailKey;
			}
			else
			{
				AccountSetting accountSetting = new AccountSetting();
				accountSetting.Account = model.objWhiteLabelSettings.AccountID;
				accountSetting.Name = contactSupportEmailKey;
				accountSetting.Description = contactSupportEmailKey;
				accountSetting.Value = model.objWhiteLabelSettings.ContactSupportEmail != null ? model.objWhiteLabelSettings.ContactSupportEmail : String.Empty;
				accountSetting.ValueDataType = 1;

				Context.AccountSettings.Add(accountSetting);
			}
		}

		#endregion
	}
}