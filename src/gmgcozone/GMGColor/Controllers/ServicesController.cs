﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using GMGColor.Common;
using System.Transactions;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMGColor.Controllers
{
    public class ServicesController : BaseController
    {
        public ServicesController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        #region Get methods

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult MediaServices(int? ID)
        {
            List<GMGColorDAL.MediaService> lstExistingServices = DALUtils.SearchObjects<GMGColorDAL.MediaService>(o => o.ID > 0, Context);
            ID = (ID != null && ID > 0) ? ID : (lstExistingServices.Count > 0) ? lstExistingServices[0].ID : 0;

            GMGColorDAL.MediaService objSelectedService = new GMGColorDAL.MediaService();

            if (ID > 0)
            {
                objSelectedService = lstExistingServices.SingleOrDefault(t => t.ID == ID.Value);
            }

            if (objSelectedService != null)
            {
                MediaServiceDetails modelMediaService = new MediaServiceDetails(objSelectedService);

                //if (TempData["ImageFormatParent"] != null)
                //{
                //    modelMediaService.ImageFormatParent = int.Parse(TempData["ImageFormatParent"].ToString());
                //}

                if (TempData["MediaServiceModel"] != null)
                {
                    MediaServiceDetails tmpModel = (MediaServiceDetails)TempData["MediaServiceModel"];

                    //modelMediaService.SelectedMediaService = tmpModel.SelectedMediaService;
                    modelMediaService.Smoothing = tmpModel.Smoothing;
                    modelMediaService.Resolution = tmpModel.Resolution;
                    modelMediaService.ColorSpace = tmpModel.ColorSpace;
                    modelMediaService.ImageFormatParent = tmpModel.ImageFormatParent;
                    modelMediaService.ImageFormat = (tmpModel.ImageFormat > 0) ? tmpModel.ImageFormat : GMGColorDAL.ImageFormat.GetImageFormat(GMGColorDAL.ImageFormat.Format.JPEG_medium, Context).ID;
                    modelMediaService.PageBox = tmpModel.PageBox;
                    modelMediaService.TileImageFormat = tmpModel.TileImageFormat;
                    modelMediaService.JpegCompression = tmpModel.JpegCompression;
                }

                return View(modelMediaService);
            }
            else
            {
                return RedirectToAction("Cannotcontinued", "Error");
            }
            
        }

        #endregion

        #region POST Actions

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveChanges")]
        public ActionResult SaveChanges(MediaServiceDetails model)
        {
            try
            {
                using (TransactionScope tscope = new TransactionScope())
                {
                    GMGColorDAL.MediaService objService =
                        DALUtils.GetObject<GMGColorDAL.MediaService>(model.SelectedMediaService, Context);

                    objService.Smoothing = model.Smoothing;
                    objService.Resolution = model.Resolution;
                    objService.Colorspace = model.ColorSpace;
                    objService.ImageFormat = (model.ImageFormat > 0) ? model.ImageFormat : model.ImageFormatParent;
                    objService.PageBox = model.PageBox;
                    objService.TileImageFormat = model.TileImageFormat;
                    objService.JPEGCompression = model.JpegCompression;

                    Context.SaveChanges();
                    tscope.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,"ServicesController.SaveChanges : Saving service Failed. {0}", ex.Message);
            }

            return RedirectToAction("MediaServices", "Services", new { @id = model.SelectedMediaService });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Delete")]
        public ActionResult Delete(MediaServiceDetails model)
        {
            try
            {
                using (TransactionScope tscope = new TransactionScope())
                {
                    GMGColorDAL.MediaService objService =
                        DALUtils.GetObject<GMGColorDAL.MediaService>(model.SelectedMediaService, Context);

                    DALUtils.Delete<GMGColorDAL.MediaService>(Context, objService);

                    Context.SaveChanges();
                    tscope.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,"ServicesController.Delete : Deleting service Failed. {0}", ex.Message);
            }

            return RedirectToAction("MediaServices", "Services", new { @id = 0 });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddService")]
        public ActionResult AddService(MediaServiceDetails model)
        {
            int serviceID = 0;

            try
            {
                using (TransactionScope tscope = new TransactionScope())
                {
                    GMGColorDAL.MediaService objService = new GMGColorDAL.MediaService();

                    objService.Name = model.NewService;
                    //set default values
                    objService.Smoothing = 2;
                    objService.Resolution = 300;
                    objService.Colorspace = 1;
                    objService.ImageFormat = 5;
                    objService.PageBox = 1;
                    objService.TileImageFormat = 1;
                    objService.JPEGCompression = 90;

                    Context.MediaServices.Add(objService);
                    Context.SaveChanges();
                    tscope.Complete();

                    serviceID = objService.ID;
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID,"ServicesController.AddService : Saving new service Failed. {0}", ex.Message);
            }

            return RedirectToAction("MediaServices", "Services", new { @id = serviceID });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "GetImageFormat")]
        public ActionResult GetImageFormat(MediaServiceDetails model)
        {
            //TempData["ImageFormatParent"] = model.ImageFormatParent;
            TempData["MediaServiceModel"] = model;
            return RedirectToAction("MediaServices", "Services", new { @id = model.SelectedMediaService });
        }

        #endregion
    }
}
