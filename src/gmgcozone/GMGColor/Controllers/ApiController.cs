﻿using GMG.CoZone.Proofstudio.DomainModels.Requests;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMG.CoZone.Proofstudio.Interfaces;
using GMGColor.ActionFilters;
using GMGColor.Common;
using GMGColor.Hubs;
using GMGColor.Infrastructure;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.Api;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using ApprovalPage = GMGColorDAL.CustomModels.Api.ApprovalPage;
using ApprovalSeparationPlate = GMGColorDAL.CustomModels.Api.ApprovalSeparationPlate;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using GMG.CoZone.Proofstudio.DomainModels;
using GMG.CoZone.Collaborate.Interfaces;
using static GMGColorDAL.ApprovalCollaboratorRole;
using System.Diagnostics;

namespace GMGColor.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class ApiController: Controller
    {
        public enum ErrorCodesEnum
        {
            NoContent = 204,
            BadRequest = 400,
            Forbidden = 403,
            NotFound = 404,
            Conflict = 409,
            InternalServerError = 500
        }
        private readonly IProofstudioApiService _proofstudioApiService;
        private readonly IApprovalService _approvalService;
        private readonly ISoftProofingService _softProofingService;
        public static int page { get; set; }
        public static int selectedApprovalID { get; set; }
        public static int scrollTop { get; set; }
        public static List<string> selectedApprovalTypes { get; set; }

        public static int GetHtmlPageId()
        {
           return page ;
        }
        public static List<string> GetSelectedApprovalType()
        {
            return selectedApprovalTypes;
        }
        public static int GetSelectedApprovalID ()
        {
            return selectedApprovalID;
        }
        public static int GetScrollTop()
        {
            return scrollTop;
        }
        public static void SetPageIdNull()
        {
            page = 0;
        }
        public static void SetSelectedApprovalTypesNull()
        {
            selectedApprovalTypes = null;
        }

        public static void SetSelectedApprovalIDNull()
        {
            selectedApprovalID = 0;
        }
        public static void SetScrollTopNull()
        {
            scrollTop = 0;
        }
        public ApiController(IProofstudioApiService proofstudioApiService, IApprovalService approvalService, ISoftProofingService softProofingService)
        {
            _proofstudioApiService = proofstudioApiService;
            _approvalService = approvalService;
            _softProofingService = softProofingService;
        }

        #region Public Methods

        [HttpGet]
        [ActionName("user")]
        public JsonResult GetUser(UserRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key) || model.approval_id == 0)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.approval_id }, model.user_key, "APPROVAL");

            #endregion

            try
            {
                SetCulturebyUserGuid(model.user_key);

                UserResponse user = ApiBL.GetUser(model.user_key, model.approval_id, model.is_external_user, GMGColorConstants.ProofStudioShowAnnotationsForExternalUsers);
                return user == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(new {user}, JsonRequestBehavior.AllowGet);
            }
            catch(ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("account")]
        public JsonResult GetAccount(AccountRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                SetCulturebyUserGuid(model.user_key);

                AccountResponse account = ApiBL.GetAccount(model.user_key, model.is_external_user);

                List<AvailableFeature> available_feature = null;
                if (account != null)
                {
                    available_feature = ApiBL.GetAvailableFeatureList(account.AvailableFeatures);
                }

                return account == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(new {account, available_feature}, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("collaborator-roles")]
        public JsonResult GetCollaboratorRoles(CollaboratorRolesRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.locale_name))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                SetCulturebyLocaleName(model.locale_name);

                CollaboratorRolesResponse roles = ApiBL.GetCollaboratorRoles();
                return roles == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(roles, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("collaborators-and-groups")]
        public JsonResult GetCollaboratorsAndGroups(ApprovalUsersAndGroupsRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.approval_id }, model.user_key, "APPROVAL");

            #endregion

            try
            {
                SetCulturebyUserGuid(model.user_key);

                ApprovalUsersAndGroupsResponse collaboratorsAndGroup = ApiBL.GetCollaboratorsAndGroups(model);
                return collaboratorsAndGroup == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(collaboratorsAndGroup, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("annotation-statuses")]
        public JsonResult GetAnnotationStatuses(AnnotationStatusRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.locale_name))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                SetCulturebyLocaleName(model.locale_name);

                AnnotationStatusResponse statuses = ApiBL.GetAnnotationStatuses();
                return statuses == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(statuses, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("decisions")]
        public JsonResult GetDecisions(DecisionsRequest model, bool is_external_user)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.locale_name))
            { 
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                SetCulturebyLocaleName(model.locale_name);

                DecisionsResponse decisions = ApiBL.GetDecisions(model.user_key, is_external_user);
                List<ProjectFolderDetails> projectfolders = ApiBL.GetProjectFolderDetails(model.user_key, is_external_user);                
                return decisions == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json( new { decisions.decisions, projectfolder = projectfolders }, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("shapes")]
        [Compress]
        public JsonResult GetShapes(ShapesRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                ShapesResponse shapes = ApiBL.GetShapes();
                return shapes == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(shapes, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("roles")]
        public JsonResult GetRoles(RolesRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.locale_name))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                SetCulturebyLocaleName(model.locale_name);

                RolesResponse roles = ApiBL.GetRoles();
                return roles == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(roles, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("comment-types")]
        public JsonResult GetComentTypes(CommentsRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.locale_name))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                SetCulturebyLocaleName(model.locale_name);

                CommentsResponse comments = ApiBL.GetComments();
                return comments == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(comments, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Put)]
        [ActionName("annotations")]
        public JsonResult AddAnnotation(AnnotationRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key) || model.annotation == null)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.annotation.Page }, model.user_key, "APPROVALPAGE");

            #endregion

            Response.StatusCode = 201;
            return AddEditAnnotations(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("annotations")]
        public JsonResult UpdateAnnotation(AnnotationRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key) || model.annotation == null)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.annotation.Page }, model.user_key, "APPROVALPAGE");

            #endregion

            Response.StatusCode = 201;
            return AddEditAnnotations(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("update-public-annotations")]
        public MaxJsonResult UpdatePublicAnnotations(UpdatePublicAnnotationsRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return MaxError(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return MaxError(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    ApiBL.UpdateAnnotationsToPublic(model);
                    
                    return new MaxJsonResult("", JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("user-settings")]
        public JsonResult UserSettings(UserRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion
            
            Response.StatusCode = 201;
            ApiBL.AddEditUserSettings(model);

            return Json("", JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Delete)]
        [ActionName("annotations")]
        public JsonResult DeleteAnnotation(string key, string user_key, int? id, bool is_external_user)
        {
            #region Validations
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(user_key) || id == null)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { (int)id }, user_key, "APPROVALANNOTAION");
            #endregion

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    int versionId = ApiBL.GetVersionIdByAnnotationId(context, id.GetValueOrDefault());

                    bool allChildsWasDeleted = true;
                    List<int> childs = ApiBL.GetAnnotationChilds(context, id.GetValueOrDefault());
                    foreach (int annotationId in childs)
                    {
                        if (!ApiBL.DeleteAnnotation(context, annotationId, user_key, is_external_user, false))
                        {
                            allChildsWasDeleted = false;
                        }
                    }

                    if (allChildsWasDeleted && ApiBL.DeleteAnnotation(context, id, user_key, is_external_user, true))
                    {
                        Response.StatusCode = 200;
                        RemoveInstantNotification(InstantNotificationEntityType.Annotation, versionId, is_external_user, user_key, context);
                        return Json("", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
                    }
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("update-annotation-status")]
        public JsonResult UpateAnnotationStatus(UpdateAnnotationStatusRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key) || !model.Id.HasValue)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.Id.GetValueOrDefault() }, model.user_key, "APPROVALANNOTAION");
            #endregion

            return UpdateAnnotationStatus(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("job-changescomplete")]
        public JsonResult SetJobChangesComplete(JobChangesCompleteRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.pageId }, model.user_key, "APPROVALPAGE");
            #endregion

            try
            {
                ApiBL.SetJobStatusChangesComplete(model.pageId, model.user_key);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("complete-session")]
        public JsonResult MarkSessionComplete(string key, string session_key)
        {
            #region Validations
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(session_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                ApiBL.MarkPsSessionComplete(session_key);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("block-externalurl")]
        public JsonResult BlockExternalUserOnExit(int approval_id, string user_guid)
        {
            #region Validations
            if (approval_id <=0 || string.IsNullOrEmpty(user_guid))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { approval_id }, user_guid, "APPROVAL");

            #endregion

            try
            {
                ApiBL.BlockExternalUrl(approval_id, user_guid);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("update-annotation-comment")]
        public JsonResult UpateAnnotationComment(UpdateAnnotationCommentRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key) || !model.Id.HasValue)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.Id.GetValueOrDefault() }, model.user_key, "APPROVALANNOTAION");
           
            #endregion

            return UpdateAnnotationComment(model);
        }

        [HttpGet]
        [ActionName("versions")]
        public JsonResult GetVersion(BaseGetEntityRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.Id.GetValueOrDefault() }, model.user_key, "APPROVAL");
            #endregion

            try
            {
                SetCulturebyUserGuid(model.user_key);

                VersionResponse version = ApiBL.GetVersion(model.Id, model.user_key, model.is_external_user);

                if (version.version != null)
                {
                    using (DbContextBL context = new DbContextBL())
                    {
                        UserResponse user = ApiBL.GetUser(model.user_key, version.version.ID, model.is_external_user, GMGColorConstants.ProofStudioShowAnnotationsForExternalUsers);
                        if (user != null)
                        {
                            ApprovalRoleName role;
                            if (model.is_external_user)
                            {
                                role = ApprovalCollaboratorRole.GetApprovalExternalCollaboratorRole(user.ID, version.version.ID, context);
                            }
                            else
                            {
                                role = ApprovalCollaboratorRole.GetApprovalCollaboratorRole(user.ID, version.version.ID, context);
                            }
                            if (role != ApprovalRoleName.Retoucher && role != ApprovalRoleName.ReadOnly)
                            {
                                var enableHighResolution = false;
                                List<ApprovalPage> pages = ApiBL.GetPages(context, version.version.ID, model.user_key, model.is_external_user);
                                foreach (var page in pages)
                                {
                                    if (page.OriginalImageHeight > GMGColorConfiguration.AppConfiguration.DefaultImageSize
                                        || page.OriginalImageWidth > GMGColorConfiguration.AppConfiguration.DefaultImageSize)
                                    {
                                        enableHighResolution = true;
                                        break;
                                    }
                                }

                                foreach (var spParams in version.softProofingParams)
                                {
                                    spParams.EnableHighResolution = enableHighResolution || spParams.HighResolutionInProgress;
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(version.version.MovieFilePath))
                {
                    version.version.MovieFilePath = GMGColor.Common.Utils.EncodeUrl(version.version.MovieFilePath);
                }
                
                return version == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(version, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }


        [HttpPost]
        [ActionName("version-switched-to-high-resolutions")]
        public JsonResult SendNotificationThatVersionSwitchedToHighRes(ApprovalRequestModel model)
        {
            try
            {
                _proofstudioApiService.ValidateRequest(model);
                _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.approvalId }, model.user_key, "APPROVAL");

                int userId = _proofstudioApiService.GetUserByUserKey(model.user_key);
                AddInstantNotification(InstantNotificationEntityType.VersionSwitchedToHighRes, model.approvalId, model.is_external_user, userId);

                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (ErrorModel err)
            {
                return Error(err);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpPost]
        [ActionName("approval-initial-loads")]
        public JsonResult NotifyApprovalInitialLoad(ApprovalBasicRequestModel model)
        {
            try
            {
                _proofstudioApiService.ValidateRequest(model);

                List<int> approvalIds = model.approvalsIds.Split(',').Select(Int32.Parse).ToList();

                _proofstudioApiService.IsFromCurrentAccount(approvalIds, model.user_key, "APPROVAL");

                SetCulturebyUserGuid(model.user_key);                
                UpdateInstantDashboardNotification(ApiBL.GetInstantNotificationModel(model.user_key, model.approvalsIds));
                ApprovalWorkflowBL.CheckApprovalShouldMoveToNextPhase(model.approvalsIds.Split(',').Select(Int32.Parse).ToList(), null, null, null);

                Response.StatusCode = 201;
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (ErrorModel err)
            {
                return Error(err);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("approval-basics")]
        public JsonResult GetApprovalBasic(ApprovalBasicRequestModel model)
        {
            try
            {
                _proofstudioApiService.ValidateRequest(model);

                List<int> approvalIds = model.approvalsIds.Split(',').Select(int.Parse).ToList<int>();
                _proofstudioApiService.IsFromCurrentAccount(approvalIds, model.user_key, "APPROVAL");

                UpdateInstantDashboardNotification(ApiBL.GetInstantNotificationModel(model.user_key, model.approvalsIds));
                ApprovalWorkflowBL.CheckApprovalShouldMoveToNextPhase(model.approvalsIds.Split(',').Select(Int32.Parse).ToList(), null, null, null);
                var approvals = _proofstudioApiService.GetApprovalsBasic(model);

                return Json(approvals, JsonRequestBehavior.AllowGet);
            }
            catch (ErrorModel err)
            {
                return Error(err);
            }
        }

        
        [HttpGet]
        [ActionName("approvals")]
        public JsonResult GetApprovalInitialVersions(ApprovalGranularRequestModel model)
        {
            try
            {
                _proofstudioApiService.ValidateRequest(model);

                List<ApprovalBasicModel> approvalsBasic = JsonConvert.DeserializeObject<List<ApprovalBasicModel>>(model.approvalBasic);

                List<int> approvalIds = approvalsBasic.Select(x => x.ApprovalId).ToList();
                _proofstudioApiService.IsFromCurrentAccount(approvalIds, model.user_key, "APPROVAL");

                SetCulturebyUserGuid(model.user_key);
                var approvals = _proofstudioApiService.GetApprovalInitialVersions(approvalsBasic, model.user_key, model.is_external_user);

                using (DbContextBL context = new DbContextBL())
                {
                    UserResponse user = ApiBL.GetUser(model.user_key, approvalsBasic[0].ApprovalId, model.is_external_user, GMGColorConstants.ProofStudioShowAnnotationsForExternalUsers);

                    if (user != null)
                    {
                        ApprovalRoleName role;
                        if (model.is_external_user)
                        {
                            role = ApprovalCollaboratorRole.GetApprovalExternalCollaboratorRole(user.ID, approvalsBasic[0].ApprovalId, context);
                        }
                        else
                        {
                            role = ApprovalCollaboratorRole.GetApprovalCollaboratorRole(user.ID, approvalsBasic[0].ApprovalId, context);
                        }
                        if (role != ApprovalRoleName.Retoucher && role != ApprovalRoleName.ReadOnly)
                        {
                            List<ApprovalPagesHeightAndWidth> pages = ApiBL.GetPagesHeightAndWidth(context, approvalsBasic[0].ApprovalId);
                            var enableHighResolution = false;
                            foreach (var page in pages)
                            {
                                if (page.OriginalImageHeight > GMGColorConfiguration.AppConfiguration.DefaultImageSize
                                    || page.OriginalImageWidth > GMGColorConfiguration.AppConfiguration.DefaultImageSize)
                                {
                                    enableHighResolution = true;
                                    break;
                                }
                            }

                            foreach (var spParams in approvals.softProofingParams)
                            {
                                spParams.EnableHighResolution = enableHighResolution || spParams.HighResolutionInProgress;
                            }
                        }
                    }
                }

                foreach (var version in approvals.versions)
                {
                    if (!string.IsNullOrEmpty(version.MovieFilePath))
                    {
                        version.MovieFilePath = GMGColor.Common.Utils.EncodeUrl(version.MovieFilePath);
                    }
                }

                return Json(approvals, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                if(exbl.InnerException != null)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(exbl.InnerException, exbl);
                }
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                if (ex.InnerException != null)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(ex.InnerException, ex);
                }
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }
        
        [HttpGet]
        [ActionName("user-groups")]
        public JsonResult GetApprovalUserGroups(ApprovalGranularRequestModel model)
        {
            try
            {
                
                _proofstudioApiService.ValidateRequest(model);

                List<ApprovalBasicModel> approvalsBasic = JsonConvert.DeserializeObject<List<ApprovalBasicModel>>(model.approvalBasic);

                List<int> approvalIds = approvalsBasic.Select(x => x.ApprovalId).ToList();
                _proofstudioApiService.IsFromCurrentAccount(approvalIds, model.user_key, "APPROVAL");

                var approvals = _proofstudioApiService.GetApprovalUserGroups(approvalsBasic, model.user_key, model.is_external_user);

                return Json(approvals, JsonRequestBehavior.AllowGet);
            }
            catch (ErrorModel err)
            {
                return Error(err);
            }
        }

        [HttpGet]
        [ActionName("internal-collaborators")]
        public JsonResult GetApprovalCollaborators(ApprovalGranularRequestModel model)
        {
            try
            {
                _proofstudioApiService.ValidateRequest(model);

                List<ApprovalBasicModel> approvalsBasic = JsonConvert.DeserializeObject<List<ApprovalBasicModel>>(model.approvalBasic);
                List<int> approvalIds = approvalsBasic.Select(x => x.ApprovalId).ToList();
                _proofstudioApiService.IsFromCurrentAccount(approvalIds, model.user_key, "APPROVAL");

                var approvals = _proofstudioApiService.GetApprovalCollaborators(approvalsBasic, model.user_key, model.is_external_user);

                return Json(approvals, JsonRequestBehavior.AllowGet);
            }
            catch (ErrorModel err)
            {
                return Error(err);
            }
        }

        [AcceptVerbs(HttpVerbs.Put)]
        [ActionName("approvaldecision")]
        public JsonResult UpdateApprovalDecision(UpdateApprovalDecision model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key) || model.Id == null)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.Id.GetValueOrDefault() }, model.user_key, "APPROVAL");
            #endregion           

            try
            {
                var response = ApiBL.UpdateApprovalDecision(model);
				UpdateInstantNotification(InstantNotificationEntityType.Decision, model.Id.GetValueOrDefault(), model.isExternal, model.collaboratorId.GetValueOrDefault()); //TODO check if collaboratorId can be null
               
                UpdateInstantDashboardNotification(response.ApprovalWorkflowInstantNotification);

                if (response.UpdateApprovalDecisionResponse != null)
                {
                    Response.StatusCode = 200;
                    return Json(response.UpdateApprovalDecisionResponse, JsonRequestBehavior.AllowGet);
                }
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("approval-zoom-levels")]
        public JsonResult ApprovalZoomLevel(ApprovalZoomLevelRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
  
                    if (model.ApprovalId > 0 )
                    {
                        ApiBL.UpdateApprovalZoomLevel(model.ApprovalId , model.ZoomLevel);
                    }
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Put)]
        [ActionName("versionlocking")]
        public JsonResult UpdateVersionLocking(UpdateVersionLocking model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                if (ApiBL.UpdateVersionLocking(model))
                {
                    Response.StatusCode = 200;
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpPost]
        [ActionName("translations")]
        [Compress]
        public JsonResult GetTranslations(TranslationsRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.TranslationKeys == null || (model.TranslationKeys != null && !model.TranslationKeys.Any()))
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (string.IsNullOrEmpty(model.locale))
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiMissingRequestParameters);
            }
            #endregion

            TranslationsResponse result = new TranslationsResponse();
            using (DbContextBL context = new DbContextBL())
            {
                var locale = (from u in context.Users
                              join l in context.Locales on u.Locale equals l.ID
                              where u.Guid == model.locale
                              select l.Name).FirstOrDefault();
                try
                {
                    SetCulturebyLocaleName(locale);
                    if (model.TranslationKeys != null && model.TranslationKeys.Any())
                    {
                        ResourceManager rm = Resources.Resources.ResourceManager;
                        foreach (string translationKey in model.TranslationKeys)
                        {
                            result.Translations[translationKey] = rm.GetString(translationKey) ??
                                                                  string.Format("{0}[nt]", translationKey);
                        }
                    }
                }
                catch (ExceptionBL exbl)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                    return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
                }
                catch (Exception ex)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                    return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
                }
                return Json(result);
            }
        }

        [HttpGet]
        [ActionName("softproofingstatus")]
        public JsonResult GetSoftProofingStatus(SoftProofingStatusRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                var response = new SoftProofingStatusResponse
                {
                    Softproofingstatus = SoftProofingBL.GetSoftProofingStatus(
                    new StartSession()
                    {
                        MachineId = model.machineId,
                        UserGuid = model.user_key
                    })
                };

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("account-settings")]
        public JsonResult SaveAccountSettings(AccountSettingsRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                AccountSettingsResponse response = ApiBL.UpdateAccountSettings(model);
                return Json(response);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("copy-annotation")]
        public JsonResult CopyAnnotation(CopyAnnotationRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.Id.GetValueOrDefault() }, model.user_key, "APPROVALANNOTAION");

            #endregion

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    CopyAnnotationResponse copyAnnotationResult = ApiBL.CopyAnnotation(context, model);
                    if (copyAnnotationResult.annotation != null && copyAnnotationResult.annotation.Any())
                    {
                        var annotation = copyAnnotationResult.annotation.FirstOrDefault();
                        AddInstantNotification(InstantNotificationEntityType.Annotation, annotation.annotation.Version, model.is_external_user, model.user_key, context);
                    }
                    return Json(copyAnnotationResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [ActionName("download")]
        public ActionResult DownloadFile(string apiKey, string fileGuid, string userKey, string sessionKey, bool isExternal)
        {
            try
            {
                bool hasAccess = ApprovalBL.HasAccessToDownloadFile(fileGuid, userKey, isExternal, apiKey);
                if (hasAccess)
                {
                    ApprovalDownloadDetails downloadDetails = ApprovalBL.DownloadFile(fileGuid);
                    string relativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + fileGuid + "/" + downloadDetails.FileName;

                    ApprovalBL.SendDownloadEmail(fileGuid, userKey, sessionKey, isExternal);

                    return new UnicodeFileContentResult(relativePath, downloadDetails.Region);
                }
                else
                {
                    GMGColorDAL.GMGColorLogging.log.Debug("User with guid " + userKey + " does no have access to download file with guid " + fileGuid);
                }
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return null;
        }

        [HttpGet]
        [ActionName("text-highlighting-words")]
        [Compress]
        public MaxJsonResult GetTextHighlightingData(TextHighlightingGetRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return MaxError(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return MaxError(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.user_key != null)
            {
                _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.approvalId }, model.user_key, "APPROVAL");
            }
            #endregion

            try
            {
                bool isFileTypePDF = ApiBL.CheckApprovalIsPDFFile(model.approvalId);
                if (isFileTypePDF)
                {
                    List<TextHighlightingWord> list = GetTextHightlightingWords(model);

                    return new MaxJsonResult(new { text_highlighting_words = list }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<int> list = new List<int>();
                    return new MaxJsonResult(new { text_highlighting_words = list }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("phase-complete")]
        [Compress]
        public JsonResult IsPhaseComplete(PhaseCompleteGetRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                PhaseCompleteResponse result = ApiBL.IsPhaseCompletedOrVersionRejected(model);
                return result == null
                           ? Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                           : Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpPost]
        [ActionName("annotation-attachment")]
        public JsonResult UploadAnnotationAttachment(UploadAnnotationAttachmentRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }

            #endregion

            UploadAnnotationAttachmentResponse response = new UploadAnnotationAttachmentResponse();
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    foreach (string key in Request.Files.Keys)
                    {
                        HttpPostedFileBase file = Request.Files[key];
                        if (file != null)
                        {
                            UploadAnnotationAttachmentFile attachment = AnnotationBL.UploadAttachment(context, file.FileName, file.InputStream, model);
                            response.files.Add(attachment);
                        }
                        else
                        {
                            throw new Exception("Could not upload annotation attachment since the file stream is null");
                        }
                    }
                    // this method should return an identifier of the file

                    int versionId = ApiBL.GetVersionIdByAnnotationId(context, model.annotationId.GetValueOrDefault());
                    UpdateInstantNotification(InstantNotificationEntityType.Annotation, versionId, model.is_external_user, model.user_key, context);

                    return Json(response);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpDelete]
        [ActionName("annotation-attachment")]
        public JsonResult DeleteAnnotationAttachment(DeleteAnnotationAttachmentRequest model)
        {
            #region Validations

            if (string.IsNullOrEmpty(model.key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.annotationId.GetValueOrDefault() }, model.user_key, "APPROVALANNOTAION");

            #endregion

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    DeleteAnnotationAttachmentFile result = AnnotationBL.DeleteAttachment(context, model);
                    int versionId = ApiBL.GetVersionIdByAnnotationId(context, model.annotationId.GetValueOrDefault());
                    UpdateInstantNotification(InstantNotificationEntityType.Annotation, versionId, model.is_external_user, model.user_key, context);
                    return Json(result);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [ActionName("download-attachment")]
        public FileResult DownloadAnnotationAttachment(DownloadAnnotationAttachmentRequest model)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    bool hasAccess = AnnotationBL.HasAccessToDownloadAttachmentFile(context, model.key, model.user_key, model.is_external_user, model.guid);
                    if (hasAccess)
                    {
                        string filename = AnnotationBL.GetAttachmentFilename(context, model.guid);
                        Stream fileStream = AnnotationBL.GetAttachmentStream(context, model.guid);

                        return File(fileStream, "application/octet-stream", filename);
                    }
                    else
                    {
                        throw new HttpException(403,
                            "User with guid " + model.user_key +
                            " does no have access to download attachment file with guid " + model.guid);
                    }
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return null;
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [ActionName("attachments-count")]
        public JsonResult GetAttachmentsCount(AnnotationAttachmentCountRequest model)
        {

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    int count = AnnotationBL.GetAttachmentsCount(context, model);
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }
            return Json(5, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("pages")]
        [Compress]
        public JsonResult GetPages(ApprovalRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.approvalId <= 0)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.approvalId }, model.user_key, "APPROVAL");
            #endregion

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    List<ApprovalPage> pages = ApiBL.GetPages(context, model);
                    if (pages == null)
                    {
                        return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
                    }
                    else
                    {
                        List<ApprovalChecklistApi> checklist = ApiBL.GetApprovalChecklists(context, model);
                        List<ApprovalSeparationPlate> separations = ApiBL.GetSeparations(context, pages.Select(o => o.ID));
                        return Json(new { Page = pages, approval_separation_plate = separations, approval_checklist = checklist }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("soft-proofing-params")]
        public JsonResult GetSoftProofingParams(TilesPageRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.pageId.GetValueOrDefault() <= 0)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            #endregion

            try
            {
                var data = ApiBL.GetSoftProofingParams(model, _softProofingService);
                return Json(new {softProofingParams = data}, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName("generate-tiles")]
        public JsonResult GenerateTiles(GenerateTilesRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.pageId.GetValueOrDefault() <= 0 || String.IsNullOrEmpty(model.SessionGuid))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            #endregion

            try
            {
                ApiBL.GenerateTiles(model, _approvalService, _proofstudioApiService);
                return Json("");
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message);
            }
        }

        [HttpGet]
        [ActionName("annotations")]
        [Compress]
        public MaxJsonResult GetApprovalAnnotations(ApprovalRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return MaxError(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return MaxError(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            //TODO the model.id is greather than 0 when is called from annotation save, this is caused by an ember data issue and should be removed in the future
            if (model.approvalId <= 0 && model.Id.GetValueOrDefault() <= 0)
            {
                return MaxError(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            #endregion

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    if (model.approvalId > 0)
                    {
                        _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.approvalId }, model.user_key, "APPROVAL");

                        var annotation = ApiBL.GetApprovalAnnotations(context, model);
                        return annotation == null
                            ? MaxError(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                            : new MaxJsonResult(annotation, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.Id.GetValueOrDefault() }, model.user_key, "APPROVALANNOTAION");

                        var annotation = ApiBL.GetAnnotation(context, model.Id.GetValueOrDefault(), null, model.user_key);
                        return annotation == null
                            ? MaxError(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed)
                            : new MaxJsonResult(annotation, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("update-html-height-widths")]
        [Compress]
        public JsonResult UpdateHtmlHeightWidth(HtmlPageRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.Page <= 0)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            #endregion
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    page = model.Page;
                    ApiBL.UpdateSelectedHtmlPageHeightWidth(model, context);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("load-page-id-as-sessions")]
        [Compress]
        public JsonResult LoadPageIdAsSession(HtmlPageRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    page = model.Page;
                    selectedApprovalID = (int)model.ApprovalId;
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("load-selected-approval-as-sessions")]
        [Compress]
        public JsonResult LoadSelectedApprovalAsSession(ProjectApprovalRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    selectedApprovalID = model.Page;
                    if (model.SelectedApprovalTypes != null && model.SelectedApprovalTypes.Count > 0)
                    {
                        selectedApprovalTypes = model.SelectedApprovalTypes;
                    }
                    if(model.Top > 0)
                    {
                        scrollTop = model.Top;
                    }
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("load-tagwordsand-approval-counts")]
        [Compress]
        public JsonResult LoadTagwordsandApprovalCount(HtmlPageRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    if(model.SelectedApprovalTypes == null)
                    {
                        model.SelectedApprovalTypes = string.Empty;
                    }
                    if(model.SelectedTagwords == null)
                    {
                        model.SelectedTagwords = string.Empty;
                    }
                    var modal = ApiBL.GetTagwordsBasedOnSelectedApprovalType(model.SelectedApprovalTypes, model.SelectedTagwords, (int)model.ProjectID);
                    return Json(
                     new
                     {
                         Status = 400,
                         NoOfImageApprovals = modal.NoOfImageApprovals,
                         NoOfVideoApprovals = modal.NoOfVideoApprovals,
                         NoOfDocumentApprovals = modal.NoOfDocumentApprovals,
                         NoOfHTMLApprovals = modal.NoOfHTMLApprovals,
                         tagwords = modal.tagwords
                     },
                     JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("custom-shapes")]
        [Compress]
        public MaxJsonResult GetApprovalAnnotationShapes(ApprovalPageRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return MaxError(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return MaxError(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.pageId.GetValueOrDefault() <= 0)
            {
                return MaxError(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.pageId.GetValueOrDefault() }, model.user_key, "APPROVALPAGE");
            #endregion

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    string proofStudioShowAnnotationsForExternalUsers = GMGColorConstants.ProofStudioShowAnnotationsForExternalUsers;
                    var shapes = ApiBL.GetShapes(context, model.pageId.GetValueOrDefault(), model.user_key, model.is_external_user, proofStudioShowAnnotationsForExternalUsers);
                    return new MaxJsonResult(new {custom_shapes = shapes});
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return MaxError(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        [HttpGet]
        [ActionName("bands")]
        public JsonResult GetBands(ApprovalPageRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (model.pageId.GetValueOrDefault() <= 0)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }

            _proofstudioApiService.IsFromCurrentAccount(new List<int>() { model.pageId.GetValueOrDefault() }, model.user_key, "APPROVALPAGE");

            #endregion

            try
            {
                List<PageBand> data = ApiBL.GetBands(model);
                return Json(new {bands = data}, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("custom-profiles")]
        public JsonResult GetCustomProfiles(CustomProfilesRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            if (string.IsNullOrEmpty(model.versionsIds))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            #endregion

            try
            {
                var profiles = ApiBL.GetProfilesData(model);

                return Json(new { custom_profiles = profiles }, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("soft-proofing-papertints")]
        public JsonResult GetSoftProofingPapertint(ApprovalRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            //if (model.approvalId <= 0)
            //{
            //    return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            //}
            #endregion

            try
            {
                var paperTintsModel = ApiBL.GetPaperTintsModel(model.user_key, model.is_external_user);
                return Json(new { soft_proofing_papertint = paperTintsModel.PaperTints, soft_proofing_papertint_measurements = paperTintsModel.PaperTintsMeasurements }, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("soft-proofing-has-failed")]
        public JsonResult GetSoftProofingHasFailed(PageSoftProofingSession model)
        {
            try
            {
                return Json(ApiBL.IsPageSoftProofingSessionInError(model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("tree-folders")]
        public JsonResult GetTreeFolders(BaseGetEntityRequest model, int pageNr = 0)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                var folders = ApiBL.GetTreeFolders(model);

                return Json(new { tree_folder = folders }, JsonRequestBehavior.AllowGet);

            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName("check-highres-is-inprogresses")]
        public JsonResult CheckHighResIsInProgress(ApprovalRequestModel model)
        {
            try
            {

                _proofstudioApiService.ValidateRequest(model);
                var isHighResInProgress = _proofstudioApiService.CheckHighResProcessingIsInProgressForApproval(model.approvalId);
                return Json(new { HighResIsInProgress = isHighResInProgress }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("approvals-in-trees")]
        public JsonResult GeApprovalsInTree(ApprovalsInTreeRequest model)
        {
            #region Validations
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                var approvals = ApiBL.GetPsApprovalsInTree(model);

                return Json(new { approvals_in_tree = approvals }, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Json(exbl.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName("checkApprovalShouldBeLocked")]
        public JsonResult CheckApprovalShouldBeLocked(LockApprovalRequest model)
        {          
            #region Validations
            if (string.IsNullOrEmpty(model.key) || model.collaboratorId == 0)
            {
                return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
            }
            #endregion

            try
            {
                var shouldLockDecision = ApiBL.CheckApprovalShouldBeLockedOnFirstDecission(model);
                Response.StatusCode = 200;
                return Json(shouldLockDecision, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }            
        }

        #endregion

        #region Protected Methods

        protected override void HandleUnknownAction(string actionName)
        {
            ViewData["actionName"] = actionName;
            View(new ResourceNotFound()).ExecuteResult(this.ControllerContext);
            Response.StatusCode = (int)ErrorCodesEnum.NotFound;
        }

        #endregion

        #region Private Methods

        private JsonResult AddEditAnnotations(AnnotationRequest model)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    if (model.annotation.IsVideo && model.annotation.Parent == null)
                    {
                        ApiBL.GrabInstanceOfComment(context, model);
                    }
                    
                    var annotationResponse = ApiBL.AddOrEditAnnotation(context, model);


                    if (model.annotation.base64 != null ? (model.annotation.base64.Length > 22 && model.annotation.Parent == null) : false)
                    {
                        ApiBL.ConvertBase64ToJPG(context, model, annotationResponse);
                    }

                    if (annotationResponse.IsDifferentPhase == true)
                  {
                        AnnotationResponse response = new AnnotationResponse();
                        response.annotation = new ApprovalAnnotationGet();
                        response.annotation.IsDifferentPhase = true;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                  else
                  {

                    if (!annotationResponse.AnnotationId.HasValue)
                    {
                        return Error(ErrorCodesEnum.BadRequest, Resources.Resources.lblApiInvalidRequestParameters);
                    }
                    else
                    {
                        if (annotationResponse.AnnotationId.Value > 0)
                        {
                            AnnotationResponse response = ApiBL.GetAnnotation(context, annotationResponse.AnnotationId.Value, annotationResponse.ParentAnnotationId, model.user_key);
                            if(model.annotation.Comment == null)
                            {
                                response.annotation.ChecklistItemUpdate = true;
                            }
                            if (model.Id.GetValueOrDefault() == 0)
                            {
                                AddInstantNotification(InstantNotificationEntityType.Annotation,
                                    response.annotation.Version, model.is_external_user,
                                    model.is_external_user
                                        ? response.annotation.ExternalCreator.GetValueOrDefault()
                                        : response.annotation.Creator.GetValueOrDefault());
                                UpdateInstantDashboardNotification(annotationResponse.InstantNotification);
                            }
                            else
                            {
                                UpdateInstantNotification(InstantNotificationEntityType.Annotation,
                                    response.annotation.Version, model.is_external_user,
                                    model.is_external_user
                                        ? response.annotation.ExternalCreator.GetValueOrDefault()
                                        : response.annotation.Creator.GetValueOrDefault());

                               
                            }
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Error(ErrorCodesEnum.Forbidden, Resources.Resources.lblApiOperationNotAllowed);
                        }
                    }
                }
            }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        private JsonResult UpdateAnnotationStatus(UpdateAnnotationStatusRequest model)
        {
            try
            {
                AnnotationResponse response = ApiBL.UpdateAnnotationStatus(model);
                UpdateInstantNotification(InstantNotificationEntityType.Annotation, model.Id.GetValueOrDefault(), model.is_external_user,
                                model.is_external_user
                                    ? response.annotation.ExternalCreator.GetValueOrDefault()
                                    : response.annotation.Creator.GetValueOrDefault());
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }

        private JsonResult UpdateAnnotationComment(UpdateAnnotationCommentRequest model)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    AnnotationResponse response = ApiBL.UpdateAnnotationComment(context, model);
                    int versionId = ApiBL.GetVersionIdByAnnotationId(context, model.Id.GetValueOrDefault());
                    UpdateInstantNotification(InstantNotificationEntityType.Annotation, versionId, model.is_external_user,
                        model.is_external_user
                        ? response.annotation.ExternalCreator.HasValue && response.annotation.ExternalCreator.Value > 0 ? response.annotation.ExternalCreator.Value : response.annotation.ExternalModifier.GetValueOrDefault()
                            : response.annotation.Creator.HasValue && response.annotation.Creator.Value > 0 ? response.annotation.Creator.Value : response.annotation.Modifier.GetValueOrDefault());
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return Error(ErrorCodesEnum.InternalServerError, Resources.Resources.lblApiServerSideError);
            }
        }
        

        private JsonResult Error(ErrorModel errorToReturn)
        {
            return Error((ErrorCodesEnum)(int)errorToReturn.code, errorToReturn.message);
        }

        private JsonResult Error(ErrorCodesEnum errCode, string errMessage)
        {
            Response.StatusCode = (int) errCode;
            return Json(errMessage, JsonRequestBehavior.AllowGet);
        }

        private MaxJsonResult MaxError(ErrorCodesEnum errCode, string errMessage)
        {
            Response.StatusCode = (int)errCode;
            return new MaxJsonResult(errMessage, JsonRequestBehavior.AllowGet);
        }

        private void SetCulturebyUserGuid(string userGuid)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    string localeName = (from u in context.Users
                                         join l in context.Locales on u.Locale equals l.ID
                                         where u.Guid == userGuid
                                         select l.Name).FirstOrDefault();
                    SetCulturebyLocaleName(localeName);
                }
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(CultureNotFoundException cnfe)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotFindCultureByLocaleName, cnfe);
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetCulture, ex);
            }
        }

        private void SetCulturebyLocaleName(string localeName)
        {
            try
            {
                if (!string.IsNullOrEmpty(localeName))
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(localeName);
                    System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(localeName);
                }
            }
            catch (ExceptionBL)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");
                throw;
            }
            catch (CultureNotFoundException cnfe)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");
                throw new ExceptionBL(ExceptionBLMessages.CouldNotFindCultureByLocaleName, cnfe);
            }
            catch (Exception ex)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetCulture, ex);
            }
        }

        #region Instant Notifications

        private void AddInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, int? ownerId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Added, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = isExternal ? null : (int?) ownerId,
                    ExternalCreator = isExternal ? (int?) ownerId : null,
                    Version = versionId,
                    EntityType = entityType
                });
            }
        }

        private void AddInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, string guid, DbContextBL context)
        {
            var ownerId = ApiBL.GetCollaboratorId(context, isExternal, guid);
            AddInstantNotification(entityType, versionId, isExternal, ownerId);
        }

        private void UpdateInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, int ownerId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Modified, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = isExternal ? null : (int?)ownerId,
                    ExternalCreator = isExternal ? (int?)ownerId : null,
                    Version = versionId,
                    EntityType = entityType
                });
            }
        }

        private void UpdateInstantDashboardNotification(DashboardInstantNotification notification)
        {
            var clients = InstantNotificationsHub.GetDashboardClients(notification.Version);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();

            foreach (string client in clients)
            {
                context.Clients.Client(client).addDashboardNotification(notification);
            }
        }

        private void UpdateInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, string guid, DbContextBL context)
        {
            var ownerId = ApiBL.GetCollaboratorId(context, isExternal, guid);
            UpdateInstantNotification(entityType, versionId, isExternal, ownerId);
        }

        private void RemoveInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, int ownerId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Deleted, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = isExternal ? null : (int?)ownerId,
                    ExternalCreator = isExternal ? (int?)ownerId : null,
                    Version = versionId,
                    EntityType = entityType
                });
            }
        }

        private void RemoveInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, string guid, DbContextBL context)
        {
            var ownerId = ApiBL.GetCollaboratorId(context, isExternal, guid);
            RemoveInstantNotification(entityType, versionId, isExternal, ownerId);
        }

        #endregion

        #endregion

        public static List<TextHighlightingWord> GetTextHightlightingWords(TextHighlightingGetRequest model) 
        {
            List<TextHighlightingWord> wordsList = new List<TextHighlightingWord>();

            string textHighLightingWordsJson = TextHighlightingBL.GetTextHighlightingJsonData(model);

            if (!string.IsNullOrEmpty(textHighLightingWordsJson))
            {
                wordsList = JsonConvert.DeserializeObject<List<TextHighlightingWord>>(textHighLightingWordsJson);
            }

            return TextHighlightingBL.GetTextHightlightingWords(model, wordsList);
        }
    }

    public class ResourceNotFound : IView
    {
        public void Render(ViewContext viewContext, System.IO.TextWriter writer)
        {
            writer.WriteLine(Resources.Resources.lblApiResourceNotFound);
        }
    }

}