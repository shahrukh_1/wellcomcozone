﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using User = GMGColorDAL.CustomModels.User;

namespace GMGColor.Controllers
{
    public class ReportsController : BaseController
    {
        private const string SessionAccountReport = "dicAccountReportPara";
        private const string SessionUserReport = "dicUserReportPara";
        public const string SessionBillingReport = "dicBillingReportPara";

        public const string SessionNewUserReport = "dicNewUserReportPara";
        
        public const string UserActivityReportData = "UserActivityReportData";
        public const string UserReportType = "User_Report_Type";

        private const string emailTemplatesFolder = "Content\\emailTemplates";

        private const string BillingInfoError = "BillingInfoError";
        private const string SubscriberModel = "SubscriberModel";
        private const string SubscriptionModel = "SubscriptionModel";

        private const string SessionFileReport = "dicFileReportPara";


        public ReportsController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        #region Fields

        private string billingReportTemplate;
        private string accountRawTemplate;

        #endregion

        #region Properties

        private string BillingReportTemplate
        {
            get
            {
                if (billingReportTemplate == null)
                {
                    billingReportTemplate = GMGColorCommon.ReadServerFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine(emailTemplatesFolder, "BillingReportDetails.html")));
                }
                return billingReportTemplate;
            }
        }

        private string AccountRawTemplate
        {
            get
            {
                if (accountRawTemplate == null)
                {
                    accountRawTemplate = GMGColorCommon.ReadServerFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine(emailTemplatesFolder, "BillingReportAccountRow.html")));
                }
                return accountRawTemplate;
            }
        }

        #endregion

        #region Get methods

        [OutputCache(Duration = 3600, VaryByParam = "none", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult Index()
        {
            if (Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex] != null ? Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex].ToString() == "true" : false)
            {
                RedirectToAction("ReportAppIndex");
            }

            ReportsIndex model = new ReportsIndex(this.LoggedAccount, Context);
            Session[SessionAccountReport] = null;
            Session[SessionUserReport] = null;
            Session[SessionBillingReport] = null;
            Session[SessionNewUserReport] = null;
            Session[SessionFileReport] = null;
            return View(model);
        }


        [OutputCache(Duration = 3600, VaryByParam = "none", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult ReportAppIndex()
        {
            ReportsIndex model = new ReportsIndex(this.LoggedAccount, Context);

            Session[SessionAccountReport] = null;
            Session[SessionUserReport] = null;
            Session[SessionBillingReport] = null;
            Session[SessionFileReport] = null;
            Session[SessionNewUserReport] = null;
            Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex] = "true";
            return View(model);
        }

        public ActionResult AccountReport(int? page)
        {
            Reports model;

            if (Session[SessionAccountReport] != null)
            {
                ReportAccountData dicAccountReportPara = Session[SessionAccountReport] as ReportAccountData;

                model = GetAccountReportData(dicAccountReportPara);
                model.ReportType = "Account";
                model.page = page;
                ViewBag.ShowResults = "true";
            }
            else
            {
                model = new Reports(this.LoggedAccount, Context);
                ViewBag.ShowResults = "false";
            }

            model.LoggedAccount = LoggedAccount.ID;
            return View(model);
        }

        [HttpGet]
        public ActionResult Plans() 
        {
            int accId = 0;
            EditAccount.Plans model;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("AccountReport");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            if (TempData["AccountPlansModel"] != null)
            {
                model = (EditAccount.Plans)TempData["AccountPlansModel"];
                TempData["AccountPlansModel"] = null;

                if (TempData["ModelErrors"] != null)
                {
                    Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                    TempData["ModelErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }
            }
            else
            {
                model = new EditAccount.Plans(accId, Context);
            }

            model.IsEditAccountPlansFromReport = true;
            return View("../Account/EditAccount-Plans", model);
        }

        [HttpGet]
        public ActionResult BillingInfo()
        {
            int accId = 0;
            var subscriberModel = (SubscriberPlanPerModule) TempData[SubscriberModel];
            var subscriptionModel = (SubscriptionPlanPerModule) TempData[SubscriptionModel];

            if (Session["SelectedAccId"] == null)
            {
                return RedirectToAction("AccountReport");
            }
            else
            {
                accId = int.Parse(Session["SelectedAccId"].ToString());
            }

            EditAccount.BillingInfo model = AccountBL.PopulateBillingInfoModel(accId, subscriberModel, subscriptionModel, true, LoggedAccount, Context);

            Dictionary<string, string> dict = TempData[BillingInfoError] as Dictionary<string, string>;
            if (dict != null)
            {
                dict.Keys.ToList().ForEach(k => ModelState.AddModelError(k, dict[k]));
            }

            return View("../Account/EditAccount-BillingInfo", model);           
        }

        [HttpGet]
        public ActionResult AccountReportDetails(string selectedAccountId)
        {
            int accountId = selectedAccountId.ToInt().GetValueOrDefault();
            if (string.IsNullOrEmpty(selectedAccountId))
            {
                if (Session["SelectedAccId"] != null)
                {
                    accountId = int.Parse(Session["SelectedAccId"].ToString());
                }
                else
                {
                    return RedirectToAction("AccountReport");
                }
            }        
           
            if (accountId > 0 && !CanAccessTree(accountId, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            Session["SelectedAccId"] = accountId;
            Session["AccountIsSelectedFromReport"] = true;

            AccountReportDetails model = ReportsBL.GetAccountReportDetails(accountId, Context);
           
            return View(model);
        }
        
        public ActionResult UserReport()
        {
            Reports model;

            if (Session[SessionUserReport] != null)
            {
                model = GetUserReportData(Session[SessionUserReport] as ReportUserData, Context);
                model.ReportType = "User";
                ViewBag.ShowResults = "true";
            }
            else
            {
                model = new Reports(this.LoggedAccount, Context);
                ViewBag.ShowResults = "false";
            }

            model.LoggedAccount = this.LoggedAccount.ID;
            return View(model);
        }

        // New User report
        public ActionResult NewUserReport()
        {
            Reports model;
            if (Session[SessionNewUserReport] != null)
            {
                model = GetAccountUserReportData(Session[SessionNewUserReport] as ReportAccountUserData, Context);
                model.UserReportType = Session[UserReportType] != null ? Session[UserReportType].ToString() : "0";
                model.ApprovalUserActivityReportView_Data = Session[UserActivityReportData] as List<ApprovalUserActivityReportViewData>;
                model.ReportType = "NewUser";
                ViewBag.ShowResults = "true";
            }
            else
            {

                model = new Reports(this.LoggedUser, this.LoggedAccount, Context);
                ViewBag.ShowResults = "false";
                model.UserReportType = "0";

                DateTime toDate = DateTime.Now;
                DateTime fromDate = DateTime.Now;
                string IEAccountId = LoggedAccount.ID.ToString();
                ViewBag.startDateYear = fromDate.Year;
                ViewBag.startDateMonth = fromDate.Month - 1;
                ViewBag.startDateDay = fromDate.Day;
                ViewBag.endDateYear = toDate.Year;
                ViewBag.endDateMonth = toDate.Month - 1;
                ViewBag.endDateDay = toDate.Day;
            }
            if (model.ListAccounts.Count == 1)
            {
                List<UserGroupUserVisibility> UserGroupVisibility = Context.UserGroupUserVisibilities.Where(o => o.User == this.LoggedUser.ID).ToList();

                if (LoggedUserCollaborateRole == Role.RoleName.AccountManager && UserGroupVisibility.Count() > 0)
                {
                    List<int> groupIds = UserGroupVisibility.Select(t => t.UserGroup).ToList();

                    if (LoggedUserReportRole == Role.RoleName.AccountModerator)
                    {
                        model.ListUserGroups = Context.UserGroups
                                    .Join(Context.UserGroupUsers,
                                    post => post.ID,
                                    meta => meta.UserGroup,
                                    (post, meta) => new { Post = post, Meta = meta }) // selection
                                    .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                                    .Where(a => groupIds.Contains(a.Post.ID))
                                    .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                                    .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                    }
                    else
                    {
                        model.ListUserGroups = this.LoggedAccount.UserGroups.Where(a => groupIds.Contains(a.ID)).Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                    }
                }
                else
                {
                if (LoggedUserReportRole == Role.RoleName.AccountModerator)
                {
                    model.ListUserGroups = Context.UserGroups
                                .Join(Context.UserGroupUsers,
                                post => post.ID,
                                meta => meta.UserGroup,
                                (post, meta) => new { Post = post, Meta = meta }) // selection
                                .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                                .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                                .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                }
                else
                {
                    model.ListUserGroups = this.LoggedAccount.UserGroups.Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                }
            }
            }
            else
            {
                model.ListUserGroups = Context.UserGroups.Select(o => new { o.ID, o.Name }).AsEnumerable().Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
            }

            model.LoggedAccount = this.LoggedAccount.ID;
            ViewBag.ShowAccounts = this.LoggedAccount.AccountType != 5 ? true : false;
            ViewBag.AccountName = AccountBL.GetAccountName(this.LoggedAccount.ID, Context);
            ViewBag.IsOpenedFromReportAppIndex = Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex] != null ? Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex].ToString() == "true" : false;
            return View(model);
        }


        // New file Report

        public ActionResult FileReport()
        {
            Reports model;
            if (Session[SessionFileReport] != null)
            {
                model = GetFileReportData(Session[SessionFileReport] as ReportFileData, Context);
                model.ReportType = "File";
                ViewBag.ShowResults = "true";
            }
            else
            {
                model = new Reports(this.LoggedUser.ID, Context, this.LoggedAccount);
                ViewBag.ShowResults = "false";

                DateTime toDate = DateTime.Now;
                DateTime fromDate = DateTime.Now;

                ViewBag.startDateYear = fromDate.Year;
                ViewBag.startDateMonth = fromDate.Month - 1;
                ViewBag.startDateDay = fromDate.Day;
                ViewBag.endDateYear = toDate.Year;
                ViewBag.endDateMonth = toDate.Month - 1;
                ViewBag.endDateDay = toDate.Day;
                model.FileReportType = "0";

                if (model.ListAccounts.Count == 1)
                {
                    List<UserGroupUserVisibility> UserGroupVisibility = Context.UserGroupUserVisibilities.Where(o => o.User == this.LoggedUser.ID).ToList();

                    if (LoggedUserCollaborateRole == Role.RoleName.AccountManager && UserGroupVisibility.Count() > 0)
                    {
                        List<int> groupIds = UserGroupVisibility.Select(t => t.UserGroup).ToList();

                        if (LoggedUserReportRole == Role.RoleName.AccountModerator)
                        {
                            model.ListUserGroups = Context.UserGroups
                                        .Join(Context.UserGroupUsers,
                                        post => post.ID,
                                        meta => meta.UserGroup,
                                        (post, meta) => new { Post = post, Meta = meta }) // selection
                                        .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                                        .Where(a => groupIds.Contains(a.Post.ID))
                                        .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                                        .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                        }
                        else
                        {
                            model.ListUserGroups = this.LoggedAccount.UserGroups.Where(a => groupIds.Contains(a.ID)).Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                        }
                    }
                    else
                    {
                        if (LoggedUserReportRole == Role.RoleName.AccountModerator)
                        {
                            model.ListUserGroups = Context.UserGroups
                                        .Join(Context.UserGroupUsers,
                                        post => post.ID,
                                        meta => meta.UserGroup,
                                        (post, meta) => new { Post = post, Meta = meta }) // selection
                                        .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                                        .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                                        .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                        }
                        else
                        {
                        model.ListUserGroups = this.LoggedAccount.UserGroups.Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                    }
                }
                }
                else
                {
                    model.ListUserGroups = Context.UserGroups.Select(o => new { o.ID, o.Name }).AsEnumerable().Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                }

            }

            model.LoggedAccount = this.LoggedAccount.ID;
            ViewBag.ShowAccounts = this.LoggedAccount.AccountType != 5 ? true : false;
            ViewBag.AccountName = AccountBL.GetAccountName(this.LoggedAccount.ID, Context);
            ViewBag.IsOpenedFromReportAppIndex = Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex] != null ? Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex].ToString() == "true" : false;
            return View(model);
        }

        [HttpGet]
        public ActionResult BillingReport()
        {
            try
            {
                Reports model;

                if (Session[SessionBillingReport] != null)
                {
                    ReportBillingData dicBillingReportPara = Session[SessionBillingReport] as ReportBillingData;
                    model = GetBillingReportData(dicBillingReportPara);

                    if (dicBillingReportPara.MinStartDate != null)
                    {
                        ViewBag.MinStartDate = dicBillingReportPara.MinStartDate;
                    }

                    model.ReportType = "Billing";
                    ViewBag.ShowResults = "true";
                }
                else
                {
                    model = new Reports(this.LoggedAccount, this.LoggedUser, Context);
                    ViewBag.ShowResults = "false";

                    DateTime toDate = DateTime.Now;
                    DateTime fromDate = DateTime.Now;

                    ViewBag.startDateYear = fromDate.Year;
                    ViewBag.startDateMonth = fromDate.Month - 1;
                    ViewBag.startDateDay = fromDate.Day;
                    ViewBag.endDateYear = toDate.Year;
                    ViewBag.endDateMonth = toDate.Month - 1;
                    ViewBag.endDateDay = toDate.Day;
                }

                model.LoggedAccount = this.LoggedAccount.ID;
                return View(model);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "ReportsController.BillingReport : Method Failed. {0}", ex.Message);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        public ActionResult Print(string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                return RedirectToAction("Index");
            }

            Reports model = new Reports(this.LoggedAccount, Context);

            switch (type)
            {
                case "Account":
                    model.ListAccountReportDetails = GetAccountReportData(Session[SessionAccountReport] as ReportAccountData).ListAccountReportDetails;
                    model.ReportType = type;
                    return View("Print", model);
                case "User":
                    model.ListUserReportDetails = GetUserReportData(Session[SessionUserReport] as ReportUserData, Context).ListUserReportDetails;
                    model.LoggedAccount = this.LoggedAccount.ID;
                    model.ReportType = type;
                    return View("Print", model);
                case "Billing":
                    model = GetBillingReportData(Session[SessionBillingReport] as ReportBillingData);
                    model.LoggedAccount = this.LoggedAccount.ID;
                    model.ReportType = type;
                    return View("Print", model);
                default:
                    return RedirectToAction("Index");
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult GetValidBillingPeriod(ReportBillingData model)
        {
            try
            {
                DateTime? minimStartDate = ReportsBL.GetValidBillingPeriod(model, Context);

                bool isFound = minimStartDate.HasValue;

                if (isFound)
                {
                    minimStartDate = new DateTime(minimStartDate.Value.Year, minimStartDate.Value.Month, minimStartDate.Value.Day);
                }

                return Json(new 
                {
                    minimStartDate,
                    isFound
                }, JsonRequestBehavior.AllowGet);

            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "GetValidBillingPeriod Ajax --> Failed to gate billing period", ex);
                return Json(DateTime.MinValue, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Post methods

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SuspendAccount")]
        public ActionResult SuspendAccount(string selectedAccountId, int? page)
        {

            try
            {
                int accountId = selectedAccountId.ToInt().GetValueOrDefault();
                if (accountId > 0 && !CanAccessTree(accountId, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                AccountBL.SuspendAccount(accountId, string.Empty, LoggedUser.ID, Context);

                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "AccountsReport --> SuspendAccount", ex);
            }
            finally
            {
            }

            return RedirectToAction("AccountReport", new { page = page});
        }

        [HttpPost]
        public ActionResult SuspendMultipleAccounts(FormCollection form)
        {
            try
            {
                var selectedAccounts = form.GetValues("AccountChechbox");

                List<int> selectedAccountIds = selectedAccounts != null
                    ? selectedAccounts.Select(int.Parse).ToList()
                    : null;

                if (selectedAccountIds != null)
                {
                    foreach (int accountId in selectedAccountIds)
                    {
                        if (accountId > 0 && !CanAccessTree(accountId, Context))
                        {
                            return RedirectToAction("Unauthorised", "Error");
                        }

                        AccountBL.SuspendAccount(accountId, string.Empty, LoggedUser.ID, Context);
                    }

                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "AccountsReport --> SuspendAccount", ex);
            }
            finally
            {
            }

            return RedirectToAction("AccountReport");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RestoreAccount")]
        public ActionResult RestoreAccount(string selectedAccountId, int? page)
        {
            try
            {
                int accountId = selectedAccountId.ToInt().GetValueOrDefault();
                if (accountId > 0 && !CanAccessTree(accountId, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                AccountBL.RestoreAccount(accountId, Context);

                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "AccountsReport --> RestoreAccount", ex);
            }
            finally
            {
            }

            return RedirectToAction("AccountReport", new { page = page});
        }

        [HttpPost]
        public ActionResult RestoreMultipleAccounts(FormCollection form)
        {
            try
            {
                var selectedAccounts = form.GetValues("AccountChechbox");

                List<int> selectedAccountIds = selectedAccounts != null
                    ? selectedAccounts.Select(int.Parse).ToList()
                    : null;

                if (selectedAccountIds != null)
                {
                    foreach (int accountId in selectedAccountIds)
                    {
                        if (accountId > 0 && !CanAccessTree(accountId, Context))
                        {
                            return RedirectToAction("Unauthorised", "Error");
                        }

                        AccountBL.RestoreAccount(accountId, Context);
                    }

                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "AccountsReport --> SuspendAccount", ex);
            }
            finally
            {
            }

            return RedirectToAction("AccountReport");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteAccount")]
        public ActionResult DeleteAccount(string selectedAccountId, int? page)
        {
            try
            {
                int accountId = selectedAccountId.ToInt().GetValueOrDefault();
                if (accountId > 0 && !CanAccessTree(accountId, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }
                AccountBL.DeleteAccount(accountId, LoggedUser, Context);
                Context.SaveChanges();               
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "AccountsReport --> DeleteAccount", ex);
            }
            finally
            {
            }

            return RedirectToAction("AccountReport", new { page = page});
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteMultipleAccounts")]
        public ActionResult DeleteMultipleAccounts(FormCollection form)
        {
            try
            {
                var selectedAccounts = form.GetValues("AccountChechbox");

                List<int> selectedAccountIds = selectedAccounts != null
                    ? selectedAccounts.Select(int.Parse).ToList()
                    : null;

                if (selectedAccountIds != null)
                {
                    foreach (int accountId in selectedAccountIds)
                    {
                        if (accountId > 0 && !CanAccessTree(accountId, Context))
                        {
                            return RedirectToAction("Unauthorised", "Error");
                        }

                        AccountBL.DeleteAccount(accountId, LoggedUser, Context);
                    }

                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "AccountsReport --> DeleteMultipleAccounts", ex);
            }

            return RedirectToAction("AccountReport");
        }
       
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "LoadBillingReport")]
        public ActionResult BillingReport(Reports model, DateTime? MinStartDate)
        {
            try
            {
                model.SelectedLocations = model.SelectedLocations ?? new string[] { };
                model.SelectedAccountTypes = model.SelectedAccountTypes ?? new string[] { };
                model.SelectedAccounts = model.SelectedAccounts ?? new string[] { };
                model.SelectedBillingPlans = model.SelectedBillingPlans ?? new string[] { };

                ReportBillingData dicBillingReportPara = new ReportBillingData();

                dicBillingReportPara.SelectedLocations = model.SelectedLocations;
                dicBillingReportPara.SelectedAccountTypes = model.SelectedAccountTypes;
                dicBillingReportPara.SelectedAccounts = model.SelectedAccounts;
                dicBillingReportPara.SelectedBillingPlans = model.SelectedBillingPlans;
                dicBillingReportPara.FromDate = DateTime.ParseExact(model.FromDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture);
                dicBillingReportPara.ToDate = DateTime.ParseExact(model.ToDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture);
                if (MinStartDate != null)
                {
                    dicBillingReportPara.MinStartDate = MinStartDate;
                }

                Session[SessionBillingReport] = dicBillingReportPara;
                return RedirectToAction("BillingReport");
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "BillingReport POST Method --> Exception", ex);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "LoadUserReport")]
        public ActionResult UserReport(Reports model)
        {
            model.SelectedAccountTypes = model.SelectedAccountTypes ?? new string[] { };
            model.SelectedLocations = model.SelectedLocations ?? new string[] { };
            model.SelectedUserStatuses = model.SelectedUserStatuses ?? new string[] { };
            model.SelectedAccounts = model.SelectedAccounts ?? new string[] { };

            ReportUserData dicUserReportPara = new ReportUserData
                                                   {
                                                       SelectedAccountTypes = model.SelectedAccountTypes,
                                                       SelectedLocations = model.SelectedLocations,
                                                       SelectedUserStatuses = model.SelectedUserStatuses,
                                                       SelectedAccounts = model.SelectedAccounts
                                                   };

            Session[SessionUserReport] = dicUserReportPara;
            return RedirectToAction("UserReport");
        }

        // New UserReport model

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "LoadNewUserReport")]
        public ActionResult NewUserReport(Reports model, DateTime? MinStartDate)
        {

            try
            {
                if (model.UserReportType != "0")
                {
                    UserActivityReportModel UserActivityReportModel1 = new UserActivityReportModel();
                    var accounIds = ApprovalBL.GetAllSubAccountIds(this.LoggedAccount.ID, Context);
                    List<int> selectedAccounts = model.SelectedAccounts != null ? model.SelectedAccounts.ToList().ConvertAll(int.Parse) : accounIds;
                    List<int> selectedGroups = model.SelectedUserGroup != null ? model.SelectedUserGroup.ToList().ConvertAll(int.Parse) : new List<int>();

                    var approvalsNonPhase = ApprovalBL.GetAllNonPhaseVersionIDBetweenTime(this.LoggedAccount.ID, selectedAccounts, DateTime.ParseExact(model.FromDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture), DateTime.ParseExact(model.ToDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture), Context);
                    var approvalsPhase = ApprovalBL.GetAllPhaseVersionIDBetweenTime(this.LoggedAccount.ID, selectedAccounts, DateTime.ParseExact(model.FromDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture), DateTime.ParseExact(model.ToDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture) ,Context);
                    model.ApprovalUserActivityReportView_Data = ApprovalBL.GetUserActivityReport(approvalsNonPhase, approvalsPhase, selectedGroups, LoggedAccount.DateFormat1.Pattern , Context);
                    Session[UserActivityReportData] = model.ApprovalUserActivityReportView_Data;
                    Session[UserReportType] = model.UserReportType;
                }
                
                model.SelectedLocations = model.SelectedLocations ?? new string[] { };
                model.SelectedAccountTypes = model.SelectedAccountTypes ?? new string[] { };
                model.SelectedAccounts = model.SelectedAccounts ?? new string[] { };
                model.SelectedBillingPlans = model.SelectedBillingPlans ?? new string[] { };
                model.SelectedUserGroup = model.SelectedUserGroup ?? new string[] { };

                ReportAccountUserData dicNewUserReportPara = new ReportAccountUserData();

                dicNewUserReportPara.SelectedAccounts = model.SelectedAccounts;
                dicNewUserReportPara.FromDate = DateTime.ParseExact(model.FromDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture);
                dicNewUserReportPara.ToDate = DateTime.ParseExact(model.ToDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture);
                dicNewUserReportPara.SelectedUserGroup = model.SelectedUserGroup;


                if (MinStartDate != null)
                {
                    dicNewUserReportPara.MinStartDate = MinStartDate;
                }

                Session[SessionNewUserReport] = dicNewUserReportPara;

                Session[UserReportType] = model.UserReportType;
                return RedirectToAction("NewUserReport");
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "NewUserRport POST Method --> Exception", ex);
                return RedirectToAction("UnexpectedError", "Error");
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "LoadAccountReport")]
        public ActionResult AccountReport(Reports model)
        {
            model.SelectedAccountTypes = model.SelectedAccountTypes ?? new string[] { };
            model.SelectedLocations = model.SelectedLocations ?? new string[] { };
            model.SelectedAccountStatuses = model.SelectedAccountStatuses ?? new string[] { };
            model.SelectedBillingPlans = model.SelectedBillingPlans ?? new string[] { };

            ReportAccountData dicAccountReportPara = new ReportAccountData();

            dicAccountReportPara.SelectedAccountTypes = model.SelectedAccountTypes;
            dicAccountReportPara.SelectedLocations = model.SelectedLocations;
            dicAccountReportPara.SelectedAccountStatuses = model.SelectedAccountStatuses;
            dicAccountReportPara.ListLocations = model.SelectedLocations;
            dicAccountReportPara.SelectedBillingPlans = model.SelectedBillingPlans;

            Session[SessionAccountReport] = dicAccountReportPara;
            return RedirectToAction("AccountReport");
        }

        [HttpPost]
        public ActionResult EmailReport(Reports model)
        {
            string message = model.EmailReport.Message;

            Reports modelDetails = GetBillingReportData(Session[SessionBillingReport] as ReportBillingData);
            model.ReportType = "Billing";

            var reportDetails = new System.Text.StringBuilder();

            foreach (ListAccountReportDetails lstAccReport in modelDetails.ListAccountBillingReportDetails)
            {
                string reportDetailHtml = this.BillingReportTemplate;
                reportDetailHtml = reportDetailHtml.Replace("<$Accountname$>", lstAccReport.SubAccountName);
                reportDetailHtml = reportDetailHtml.Replace("<$TotalAmount$>", lstAccReport.Total);

                reportDetailHtml = reportDetailHtml.Replace("<$lblName$>", Resources.Resources.lblName);
                reportDetailHtml = reportDetailHtml.Replace("<$lblAccountPath$>", Resources.Resources.lblAccountPath);
                reportDetailHtml = reportDetailHtml.Replace("<$lblType$>", Resources.Resources.lblType);
                reportDetailHtml = reportDetailHtml.Replace("<$lblPlan$>", Resources.Resources.lblPlan);
                reportDetailHtml = reportDetailHtml.Replace("<$lblBillingMonth$>", Resources.Resources.lblBillingMonth);
                reportDetailHtml = reportDetailHtml.Replace("<$lblContractStartDate$>", Resources.Resources.lblContractStart);
                reportDetailHtml = reportDetailHtml.Replace("<$lblContractEndDate$>", Resources.Resources.lblContractEnd);
                reportDetailHtml = reportDetailHtml.Replace("<$lblBillingFrequency$>", Resources.Resources.lblBillingFrequency);
                reportDetailHtml = reportDetailHtml.Replace("<$lblPricepermonth$>", Resources.Resources.lblPricePerMonth);
                reportDetailHtml = reportDetailHtml.Replace("<$lblUsage$>", Resources.Resources.lblUsage);
                reportDetailHtml = reportDetailHtml.Replace("<$lblModule$>", Resources.Resources.lblModule);
                reportDetailHtml = reportDetailHtml.Replace("<$lblCostperproof$>", Resources.Resources.lblCostPerProof);
                reportDetailHtml = reportDetailHtml.Replace("<$lblAmount$>", Resources.Resources.lblAmount);
                reportDetailHtml = reportDetailHtml.Replace("<$lblTotal$>", Resources.Resources.lblTotal);
                

                var accountRows = new System.Text.StringBuilder();
                foreach (AccountBillingDetails o in lstAccReport.AccountBillingDetails)
                {
                    string accountRowlHtml = this.AccountRawTemplate;

                    accountRowlHtml = accountRowlHtml.Replace("<$Name$>", o.Name);
                    accountRowlHtml = accountRowlHtml.Replace("<$AccountPath$>", o.AccountPath);
                    accountRowlHtml = accountRowlHtml.Replace("<$Type$>", o.Type);
                    accountRowlHtml = accountRowlHtml.Replace("<$Plan$>", o.Plan);
                    accountRowlHtml = accountRowlHtml.Replace("<$BillingMonth$>", o.BillingMonth);
                    accountRowlHtml = accountRowlHtml.Replace("<$ContractStartDate$>", o.ContractStartDate);
                    accountRowlHtml = accountRowlHtml.Replace("<$ContractEndDate$>", o.ContractEndDate);
                    accountRowlHtml = accountRowlHtml.Replace("<$BillingFrequency$>", o.BillingFrequency);
                    accountRowlHtml = accountRowlHtml.Replace("<$PricePerMonth$>", o.PricePerMonth);
                    accountRowlHtml = accountRowlHtml.Replace("<$Usage$>", o.Usage);
                    accountRowlHtml = accountRowlHtml.Replace("<$Module$>", o.ModuleName);
                    accountRowlHtml = accountRowlHtml.Replace("<$CostPerProof$>", !o.BillingPlanIsFixed ? Reports.GetExceededProofsString(o.CostPerProof, o.TotalExceededProofs, LoggedAccount.Currency.Symbol) : "-"); // o.TotalExceededProofs.);
                    accountRowlHtml = accountRowlHtml.Replace("<$Amount$>", o.Amount);

                    accountRows.Append(accountRowlHtml);
                }

                reportDetailHtml = reportDetailHtml.Replace("<$AccountRows$>", accountRows.ToString());

                reportDetails.Append(reportDetailHtml);
            }

            Dictionary<string, string> dicReport = new Dictionary<string, string>();
            dicReport.Add("message", message);
            dicReport.Add("reportBody", reportDetails.ToString());
            dicReport.Add("grandTotal", modelDetails.GrandTotal);

            if (model.EmailReport.SelectedUsers != null)
            {
                foreach (string i in model.EmailReport.SelectedUsers)
                {
                    NotificationServiceBL.CreateNotification(new GMGColorNotificationService.BillingReport
                                                                {
                                                                    EventCreator = LoggedUser.ID,
                                                                    InternalRecipient = i.ToInt(),
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    Subject = model.EmailReport.Subject,
                                                                    GrandTotal = modelDetails.GrandTotal,
                                                                    ReportBody = reportDetails.ToString(),
                                                                    Message = message
                                                                },
                                                                null,
                                                                Context
                                                                );
                }
            }

            if (model.EmailReport.IsSendToMyself)
            {
                NotificationServiceBL.CreateNotification(new GMGColorNotificationService.BillingReport
                                                                {
                                                                    EventCreator = LoggedUser.ID,
                                                                    InternalRecipient = LoggedUser.ID,
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    Subject = model.EmailReport.Subject,
                                                                    GrandTotal = modelDetails.GrandTotal,
                                                                    ReportBody = reportDetails.ToString(),
                                                                    Message = message
                                                                },
                                                                null,
                                                                Context
                                                                );
            }

            return RedirectToAction("BillingReport", "Reports");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "NewUserReportDownload")]
        public ActionResult NewUserReportDownload(Reports model)
        {
            if(model.ReportType != "NewUser")
            {
                return RedirectToAction("NewUserReport", "Reports");
            }

            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, this.LoggedAccount.TimeZone);
            MemoryStream output = new MemoryStream();
            StreamWriter writer = new StreamWriter(output, System.Text.Encoding.UTF8);
            writer.WriteLine(
                Resources.Resources.lblReportAccount + ","
                + Resources.Resources.lblName + ","
                + Resources.Resources.lblUserName + ","
                + Resources.Resources.lblEmailAddress + ","
                + Resources.Resources.lblNumberOfApprovals + ","
                + Resources.Resources.lblNumberOfUploads + ","
                + Resources.Resources.lblLastLoginDate + ","
                + Resources.Resources.lblGroupMembership
                );
            
            model = GetAccountUserReportData(Session[SessionNewUserReport] as ReportAccountUserData, Context);
            foreach(AccountUserReportDetails lstUserReport in model.ListAccountUserReportDetails)
            {
                writer.WriteLine(
                       "\"" + lstUserReport.Account
                       + "\",\"" + lstUserReport.Name
                       + "\",\"" + lstUserReport.UserName
                       + "\",\"" + lstUserReport.EmailAddress
                       + "\",\"" + lstUserReport.NumberOfApprovals
                       + "\",\"" + lstUserReport.NumberOfUploads
                       + "\",\"" + lstUserReport.LastLoginDate
                       + "\",\"" + lstUserReport.GroupMembership
                       + "\""
                       );
            }
            writer.Flush();
            output.Seek(0, SeekOrigin.Begin);

            return File(output, "text/csv", "UserReport_" + userCurTime.ToShortDateString().Replace('/', '-') + ".csv");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "FileReportDownload")]
        public ActionResult FileReportDownload(Reports model)
        {
            if (model.ReportType != "File")
            {
                return RedirectToAction("FileReport", "Reports");
            }

            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, this.LoggedAccount.TimeZone);
            MemoryStream output = new MemoryStream();
            StreamWriter writer = new StreamWriter(output, System.Text.Encoding.UTF8);
            model = GetFileReportData(Session[SessionFileReport] as ReportFileData, Context);
            string filename = "";

            switch(model.FileReportType)
            {
                case "0":
                    {
                        filename = "All_FileReport_";
                        string csvHeaderRow =
                    Resources.Resources.lblReportAccount + ","
                    + Resources.Resources.lblOwnerName + ","
                    + Resources.Resources.lblGroupMembership + ","
                    + Resources.Resources.lblFileName + ","
                    + Resources.Resources.lblV1Count + ","
                    + Resources.Resources.lblNumberofVersions + ","
                    + Resources.Resources.lblUploadDate + ","
                    + Resources.Resources.lblOverDue +" "+Resources.Resources.lblDate + ","
                    + Resources.Resources.lblCompletedDate + ","
                    + Resources.Resources.lblV1Users + ","
                    + Resources.Resources.lblLatestVersionUsers + ","
                    + Resources.Resources.lblArchivedBy + ","
                    + Resources.Resources.lblArchivedDate + ","
                    + Resources.Resources.lblUserDeletedBy + ","
                   + Resources.Resources.lblDeletedDate
                   ;
                        writer.WriteLine(csvHeaderRow);

                    foreach (FileReportDetails lstFileReport in model.ListFileReportDetails)
                    {
                        writer.WriteLine(
                                "\"" + lstFileReport.Account
                                + "\",\"" + lstFileReport.OwnerName
                                + "\",\"" + lstFileReport.OwnerGroupMembership
                                + "\",\"" + lstFileReport.FileName
                                + "\",\"" + lstFileReport.V1PageCount
                                + "\",\"" + lstFileReport.NumberOfVersion
                                + "\",\"" + lstFileReport.UploadDate
                                + "\",\"" + lstFileReport.OverDue
                                + "\",\"" + lstFileReport.CompletedDate
                                + "\",\"" + lstFileReport.V1Users
                                + "\",\"" + lstFileReport.LatestVersionUsers
                                + "\",\"" + lstFileReport.Modifier
                                + "\",\"" + lstFileReport.ModifiedDate
                                + "\",\"" + lstFileReport.DeletedByUser
                                + "\",\"" + lstFileReport.DeletedDate
                                + "\""
                                );

                    }
                        break;
                    }
                case "1":
                    {
                        filename = "Active_FileReport_";
                        string csvHeaderRow =
                    Resources.Resources.lblReportAccount + ","
                    + Resources.Resources.lblOwnerName + ","
                    + Resources.Resources.lblGroupMembership + ","
                    + Resources.Resources.lblFileName + ","
                    + Resources.Resources.lblV1Count + ","
                    + Resources.Resources.lblNumberofVersions + ","
                    + Resources.Resources.lblUploadDate + ","
                    + Resources.Resources.lblCompletedDate + ","
                    + Resources.Resources.lblV1Users + ","
                    + Resources.Resources.lblLatestVersionUsers
                   ;
                        writer.WriteLine(csvHeaderRow);

                    foreach (FileReportDetails lstFileReport in model.ListFileReportDetails)
                    {
                        writer.WriteLine(
                                "\"" + lstFileReport.Account
                                + "\",\"" + lstFileReport.OwnerName
                                + "\",\"" + lstFileReport.OwnerGroupMembership
                                + "\",\"" + lstFileReport.FileName
                                + "\",\"" + lstFileReport.V1PageCount
                                + "\",\"" + lstFileReport.NumberOfVersion
                                + "\",\"" + lstFileReport.UploadDate
                                + "\",\"" + lstFileReport.CompletedDate
                                + "\",\"" + lstFileReport.V1Users
                                + "\",\"" + lstFileReport.LatestVersionUsers
                                + "\""
                                );

                    }
                        break;
                    }
                case "2":
                    {
                        filename = "Archive_FileReport_";
                        string csvHeaderRow =
                    Resources.Resources.lblReportAccount + ","
                    + Resources.Resources.lblOwnerName + ","
                    + Resources.Resources.lblGroupMembership + ","
                    + Resources.Resources.lblFileName + ","
                    + Resources.Resources.lblV1Count + ","
                    + Resources.Resources.lblNumberofVersions + ","
                    + Resources.Resources.lblUploadDate + ","
                    + Resources.Resources.lblCompletedDate + ","
                    + Resources.Resources.lblV1Users + ","
                    + Resources.Resources.lblLatestVersionUsers + ","
                    + Resources.Resources.lblArchivedBy + ","
                    + Resources.Resources.lblArchivedDate
                       ;
                        writer.WriteLine(csvHeaderRow);
                    foreach (FileReportDetails lstFileReport in model.ListFileReportDetails)
                    {
                        writer.WriteLine(
                                "\"" + lstFileReport.Account
                                + "\",\"" + lstFileReport.OwnerName
                                + "\",\"" + lstFileReport.OwnerGroupMembership
                                + "\",\"" + lstFileReport.FileName
                                + "\",\"" + lstFileReport.V1PageCount
                                + "\",\"" + lstFileReport.NumberOfVersion
                                + "\",\"" + lstFileReport.UploadDate
                                + "\",\"" + lstFileReport.CompletedDate
                                + "\",\"" + lstFileReport.V1Users
                                + "\",\"" + lstFileReport.LatestVersionUsers
                                + "\",\"" + lstFileReport.Modifier
                                + "\",\"" + lstFileReport.ModifiedDate
                                + "\""
                                );
                    }
                        break;
                    }

                case "3":
                        {
                        filename = "Deleted_FileReport_";
                        string csvHeaderRow =
                   Resources.Resources.lblReportAccount + ","
                   + Resources.Resources.lblOwnerName + ","
                   + Resources.Resources.lblFileName + ","
                   + Resources.Resources.lblV1Count + ","
                   + Resources.Resources.lblNumberofVersions + ","
                   + Resources.Resources.lblUploadDate + ","
                   + Resources.Resources.lblCompletedDate + ","
                   + Resources.Resources.lblV1Users + ","
                   + Resources.Resources.lblLatestVersionUsers + ","
                   + Resources.Resources.lblUserDeletedBy + ","
                   + Resources.Resources.lblDeletedDate
                   ;
                        writer.WriteLine(csvHeaderRow);
                foreach (FileReportDetails lstFileReport in model.ListFileReportDetails)
                {
                    writer.WriteLine(
                            "\"" + lstFileReport.Account
                            + "\",\"" + lstFileReport.OwnerName
                            + "\",\"" + lstFileReport.FileName
                            + "\",\"" + lstFileReport.V1PageCount
                            + "\",\"" + lstFileReport.NumberOfVersion
                            + "\",\"" + lstFileReport.UploadDate
                            + "\",\"" + lstFileReport.CompletedDate
                            + "\",\"" + lstFileReport.V1Users
                            + "\",\"" + lstFileReport.LatestVersionUsers
                            + "\",\"" + lstFileReport.DeletedByUser
                            + "\",\"" + lstFileReport.DeletedDate
                            + "\""
                            );

                        }
                        break;
                    }

                case "4":
                    {
                        filename = "Overdue_FileReport_";
                        string csvHeaderRow =
                   Resources.Resources.lblReportAccount + ","
                   + Resources.Resources.lblOwnerName + ","
                   + Resources.Resources.lblGroupMembership + ","
                   + Resources.Resources.lblFileName + ","
                   + Resources.Resources.lblV1Count + ","
                   + Resources.Resources.lblNumberofVersions + ","
                   + Resources.Resources.lblUploadDate + ","
                   + Resources.Resources.lblOverDue + " " + Resources.Resources.lblDate + ","
                   + Resources.Resources.lblV1Users + ","
                   + Resources.Resources.lblLatestVersionUsers + ","
                   + Resources.Resources.lblUserDeletedBy + ","
                   + Resources.Resources.lblDeletedDate
                   ;
                        writer.WriteLine(csvHeaderRow);
                    foreach (FileReportDetails lstFileReport in model.ListFileReportDetails)
                    {
                        writer.WriteLine(
                            "\"" + lstFileReport.Account
                            + "\",\"" + lstFileReport.OwnerName
                            + "\",\"" + lstFileReport.OwnerGroupMembership
                            + "\",\"" + lstFileReport.FileName
                            + "\",\"" + lstFileReport.V1PageCount
                            + "\",\"" + lstFileReport.NumberOfVersion
                            + "\",\"" + lstFileReport.UploadDate
                            + "\",\"" + lstFileReport.OverDue
                            + "\",\"" + lstFileReport.V1Users
                            + "\",\"" + lstFileReport.LatestVersionUsers
                            + "\""
                            );
                    }
                        break;
                    }

                default: break;

            }
            
            writer.Flush();
            output.Seek(0, SeekOrigin.Begin);

            return File(output, "text/csv", filename + userCurTime.ToShortDateString().Replace('/', '-') + ".csv");
        }

        

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Download")]
        public ActionResult Download(Reports model)
        {
            if (model.ReportType != "Billing")
            {
                return RedirectToAction("BillingReport", "Reports");
            }

            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, this.LoggedAccount.TimeZone);
            string currentDateString = GMGColorFormatData.GetFormattedDate(userCurTime, this.LoggedAccount.DateFormat1.Pattern);

            MemoryStream output = new MemoryStream();
            StreamWriter writer = new StreamWriter(output, System.Text.Encoding.UTF8);
            writer.WriteLine(
                Resources.Resources.lblReportDate + ","
                + Resources.Resources.lblReportPeriod + ","
                + Resources.Resources.lblType + ","
                + Resources.Resources.lblAccountName + ","
                + Resources.Resources.lblAccountPath + ","
                + Resources.Resources.lblModule + ","
                + Resources.Resources.lblPlan + " (" + (Resources.Resources.lblVOD) + ")" + ","
                + Resources.Resources.lblBillingMonth + ","
                + Resources.Resources.lblInvoicePeriod + ","
                + Resources.Resources.lblContractStart + ","
                + Resources.Resources.lblContractEnd + ","
                + Resources.Resources.lblUsage + ","
                + Resources.Resources.lblPricePerMonth + ","
                + Resources.Resources.lblExceededProofs + ","
                + Resources.Resources.lblTotalExceeded + ","
                + Resources.Resources.lblTotalOverall
                );

            model = GetBillingReportData(Session[SessionBillingReport] as ReportBillingData);

            foreach (ListAccountReportDetails lstAccReport in model.ListAccountBillingReportDetails)
            {
                foreach (AccountBillingDetails accountDetails in lstAccReport.AccountBillingDetails)
                {
                    writer.WriteLine(
                        "\"" + currentDateString
                        + "\",\"" + model.FromDate + "-" + model.ToDate
                        + "\",\"" + accountDetails.Type
                        + "\",\"" + accountDetails.Name
                        + "\",\"" + accountDetails.AccountPath
                        + "\",\"" + accountDetails.ModuleName 
                        + "\",\"" + accountDetails.Plan
                        + "\",\"" + accountDetails.BillingMonth
                        + "\",\"" + accountDetails.BillingFrequency
                        + "\",\"" + accountDetails.ContractStartDate
                        + "\",\"" + accountDetails.ContractEndDate
                        + "\",\"" + accountDetails.Usage
                        + "\",\"" + accountDetails.PricePerMonth
                        + "\",\"" + accountDetails.TotalExceededProofs
                        + "\",\"" + (accountDetails.TotalExceededProofs*accountDetails.CostPerProof)
                        + "\",\"" + accountDetails.Amount
                        + "\""
                        );
                }
            }

            writer.Flush();
            output.Seek(0, SeekOrigin.Begin);

            return File(output, "text/csv", "BillingReport_" + userCurTime.ToShortDateString().Replace('/', '-') + ".csv");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ResetFilters")]
        public ActionResult ResetFilters(Reports model)
        {
            Session[SessionBillingReport] = null;
            return RedirectToAction("BillingReport", "Reports");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "LoadFileReport")]
        public ActionResult FileReport(Reports model)
        {
            model.SelectedAccountTypes = model.SelectedAccountTypes ?? new string[] { };
            model.SelectedLocations = model.SelectedLocations ?? new string[] { };
            model.SelectedUserStatuses = model.SelectedUserStatuses ?? new string[] { };
            model.SelectedAccounts = model.SelectedAccounts ?? new string[] { };
            model.SelectedUserGroup = model.SelectedUserGroup ?? new string[] { };

            ReportFileData dicFileReportPara = new ReportFileData();
            dicFileReportPara.SelectedStartDate = DateTime.ParseExact(model.FromDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture);
            dicFileReportPara.SelectedEndDate = DateTime.ParseExact(model.ToDate, LoggedAccount.DateFormat1.Pattern, CultureInfo.InvariantCulture);

            dicFileReportPara.SelectedAccounts = model.SelectedAccounts;
            dicFileReportPara.SelectedReportType = Convert.ToInt32(model.FileReportType);
            dicFileReportPara.SelectedUserGroup = model.SelectedUserGroup;

            Session[SessionFileReport] = dicFileReportPara;
            return RedirectToAction("FileReport");
        }

        public JsonResult FillUserGroups(string AccountId)
        {
            List<int> accountsIds = new List<int>();
            dynamic listUserGroups = null;
            if (AccountId != "")
            {
                accountsIds = AccountId.Split(',').Select(int.Parse).ToList();
                listUserGroups = (from usrgrp in Context.UserGroups where accountsIds.Contains(usrgrp.Account) select new { usrgrp.ID, usrgrp.Name }).ToList();
            }
            else
            {
                listUserGroups = (from usrgrp in Context.UserGroups select new { usrgrp.ID, usrgrp.Name }).ToList();
            }
            return Json(
                  new
                  {
                      Status = 400,
                      userGroups = listUserGroups
                  },
                  JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Methods

        private Reports GetAccountReportData(ReportAccountData dicAccountReportPara)
        {
            if (dicAccountReportPara == null)
                throw new Exception("dicAccountReportPara is null");
            Reports model = new Reports(this.LoggedAccount, Context);

            model.SelectedAccountTypes = dicAccountReportPara.SelectedAccountTypes;
            model.SelectedLocations = dicAccountReportPara.SelectedLocations;
            model.SelectedAccountStatuses = dicAccountReportPara.SelectedAccountStatuses;
            model.SelectedBillingPlans = dicAccountReportPara.SelectedBillingPlans;
            model.ListAccountReportDetails = AccountBL.GetAccountsReport(string.Join(",", model.SelectedLocations), string.Join(",", model.SelectedAccountTypes), string.Join(",", model.SelectedAccountStatuses), string.Join(",", model.SelectedBillingPlans), LoggedAccount, LoggedAccountType, Context);

            model.IsAdminUser = LoggedUserAdminRole == Role.RoleName.AccountAdministrator || LoggedUserSysAdminRole == Role.RoleName.GMGAdministrator;
            model.IsSysAdminAccount = LoggedAccountType == AccountType.Type.GMGColor;

            return model;
        }

        private Reports GetUserReportData(ReportUserData dicUserReportPara, DbContextBL context)
        {
            if (dicUserReportPara == null)
                throw new Exception("dicUserReportPara is null");

            Reports model = new Reports(this.LoggedAccount, Context);

            model.SelectedAccountTypes = dicUserReportPara.SelectedAccountTypes;
            model.SelectedLocations = dicUserReportPara.SelectedLocations;
            model.SelectedUserStatuses = dicUserReportPara.SelectedUserStatuses;
            model.SelectedAccounts = dicUserReportPara.SelectedAccounts;

            List<User> lstUsers = new List<User>();

            List<GMGColorDAL.ReturnAdminUserReportInfoView> lstReportInfo = new GMGColorDAL.User().GetUserReportInfo(string.Join(",", model.SelectedAccountTypes), string.Join(",", model.SelectedAccounts), string.Join(",", model.SelectedLocations), string.Join(",", model.SelectedUserStatuses), this.LoggedAccount.ID, Context);

            //foreach (GMGColorDAL.User user in (from o in new GMGColorDAL.User().SearchObjects()
            //                         where (
            //                         o.Account1.IsTemporary == false &&
            //                         ((model.SelectedLocations.ToList().Count == 0) ? true : model.SelectedLocations.Contains(o.Account1.Companies[0].Country.ToString())) &&
            //                         ((model.SelectedAccountTypes.ToList().Count == 0) ? true : model.SelectedAccountTypes.Contains(o.Account1.AccountType.ToString())) &&
            //                         ((model.SelectedAccounts.ToList().Count == 0) ? true : model.SelectedAccounts.Contains(o.Account.ToString())) &&
            //                         ((model.SelectedUserStatuses.ToList().Count == 0) ? true : model.SelectedUserStatuses.Contains(o.Status.ToString())) &&
            //                           ((this.LoggedAccount.Account1Type.Key == "H") ? true :
            //                                        ((this.LoggedAccount.ID == o.Account) ? true : (this.LoggedAccount.GetChilds().Select(m => m.ID).ToList().Contains(o.Account))))
            //                         )
            //                         select o).ToList())

            foreach (GMGColorDAL.ReturnAdminUserReportInfoView user in lstReportInfo)
            {
                lstUsers.Add(new User(user, this.LoggedAccount, context));
            }

            model.ListUserReportDetails = lstUsers;

            return model;
        }

        // New User Report
        private Reports GetAccountUserReportData(ReportAccountUserData dicUserReportPara, DbContextBL context)
        {
            if (dicUserReportPara == null)
                throw new Exception("dicUserReportPara is null");

            Reports model = new Reports(LoggedUser, LoggedAccount, Context);

            string datePattern = LoggedAccount.DateFormat1.Pattern;
            model.SelectedAccounts = dicUserReportPara.SelectedAccounts;
            model.ToDate = dicUserReportPara.ToDate.ToString(datePattern);
            model.FromDate = dicUserReportPara.FromDate.ToString(datePattern);
            model.SelectedUserGroup = dicUserReportPara.SelectedUserGroup;


            DateTime toDate = DateTime.ParseExact(model.ToDate, datePattern, CultureInfo.InvariantCulture);
            DateTime fromDate = DateTime.ParseExact(model.FromDate, datePattern, CultureInfo.InvariantCulture);

            ViewBag.startDateYear = fromDate.Year;
            ViewBag.startDateMonth = fromDate.Month - 1;
            ViewBag.startDateDay = fromDate.Day;
            ViewBag.endDateYear = toDate.Year;
            ViewBag.endDateMonth = toDate.Month - 1;
            ViewBag.endDateDay = toDate.Day;

            var lstAccountReportDetails = new List<AccountUserReportDetails>();
            string loggedAccountCurrency = LoggedAccount.Currency.Symbol;

            if ((int)this.LoggedAccountType == 5)
            {
                string IEAccountId = LoggedAccount.ID.ToString();
                IEnumerable<string> m_oEnum = new List<string>() { IEAccountId };
                model.SelectedAccounts = m_oEnum;
            }

            IEnumerable<string> selectedUserGroups = null;
            List<UserGroupUserVisibility> UserGroupVisibility = context.UserGroupUserVisibilities.Where(o => o.User == this.LoggedUser.ID).ToList();
            if (LoggedUserCollaborateRole == Role.RoleName.AccountManager && UserGroupVisibility.Count() > 0 && model.SelectedUserGroup.Count() == 0)
            {
                selectedUserGroups = UserGroupVisibility.Select(t => t.UserGroup.ToString()).ToList();
            }
            else
            {
            selectedUserGroups = model.SelectedUserGroup;
            }
            if (LoggedUserReportRole == Role.RoleName.AccountModerator)
            {
                model.ListUserGroups = Context.UserGroups
                            .Join(Context.UserGroupUsers,
                            post => post.ID,
                            meta => meta.UserGroup,
                            (post, meta) => new { Post = post, Meta = meta }) // selection
                            .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                            .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                            .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();

                if (model.SelectedUserGroup.Count() == 0)
                {
                    selectedUserGroups = model.ListUserGroups.Select(x => x.Key.ToString()).ToArray();
                }
            }
            else
            {
                model.ListUserGroups = Context.UserGroups.Where(x => model.SelectedAccounts.Contains(x.Account.ToString())).Select(o => new { o.ID, o.Name })
                .AsEnumerable().Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
            }

            List<GMGColorDAL.ReturnUserReportInfoView> lstReportInfo = new GMGColorDAL.User().GetAccountUserReportInfo(string.Join(",", model.SelectedAccounts), string.Join(",", selectedUserGroups), fromDate, toDate.AddDays(1), this.LoggedAccount.ID, Context);

            foreach (GMGColorDAL.ReturnUserReportInfoView user in lstReportInfo)
            {
                lstAccountReportDetails.Add(new AccountUserReportDetails(user, this.LoggedAccount, context));
            }

            model.ListAccountUserReportDetails = lstAccountReportDetails;
            return model;

        }


        private Reports GetBillingReportData(ReportBillingData dicBillingReportPara)
        {
            if (dicBillingReportPara == null)
                throw new Exception("dicBillingReportPara is null");

            Reports model = new Reports(LoggedAccount, LoggedUser, Context);

            string datePattern = LoggedAccount.DateFormat1.Pattern;

            model.SelectedLocations = dicBillingReportPara.SelectedLocations;
            model.SelectedAccountTypes = dicBillingReportPara.SelectedAccountTypes;
            model.SelectedAccounts = dicBillingReportPara.SelectedAccounts;
            model.SelectedBillingPlans = dicBillingReportPara.SelectedBillingPlans;
            model.ToDate = dicBillingReportPara.ToDate.ToString(datePattern);
            model.FromDate = dicBillingReportPara.FromDate.ToString(datePattern);

            DateTime toDate = DateTime.ParseExact(model.ToDate, datePattern, CultureInfo.InvariantCulture);
            DateTime fromDate = DateTime.ParseExact(model.FromDate, datePattern, CultureInfo.InvariantCulture);

            ViewBag.startDateYear = fromDate.Year;
            ViewBag.startDateMonth = fromDate.Month - 1;
            ViewBag.startDateDay = fromDate.Day;
            ViewBag.endDateYear = toDate.Year;
            ViewBag.endDateMonth = toDate.Month - 1;
            ViewBag.endDateDay = toDate.Day;

            model.EmailReport.Subject = Resources.Resources.lblBillingReport + ": " + GMGColorFormatData.GetFormattedDate(fromDate, datePattern) + " - " + GMGColorFormatData.GetFormattedDate(toDate, datePattern);

            var lstAccountReportDetails = new List<ListAccountReportDetails>();
            decimal grandTotal = 0;
            string loggedAccountCurrency = LoggedAccount.Currency.Symbol;

            List<GMGColorDAL.ReturnBillingReportInfoView> lstReportInfo = new GMGColorDAL.Account().GetBillingReportInfo(string.Join(",", model.SelectedAccountTypes), string.Join(",", model.SelectedAccounts), string.Join(",", model.SelectedLocations), string.Join(",", model.SelectedBillingPlans), fromDate, toDate, LoggedAccount.ID, Context);

            if(lstReportInfo != null && lstReportInfo.Any())
            {
                //get direct childrens of current account
                List<GMGColorDAL.ReturnBillingReportInfoView> lstSubAccounts = lstReportInfo.Where(m => m.Parent > 0 && m.Parent == LoggedAccount.ID).ToList();
                lstReportInfo = lstReportInfo.Except(lstSubAccounts).ToList();

                foreach (GMGColorDAL.ReturnBillingReportInfoView objSubAccount in lstSubAccounts)
                {
                    List<GMGColorDAL.ReturnBillingReportInfoView>  lstSubAccountReportInfo = GetChildsForCurrentAccount(objSubAccount.ID, lstReportInfo);

					decimal totalAmount = 0;

					if (objSubAccount.CollaborateBillingPlan > 0 || objSubAccount.DeliverBillingPlan > 0)
					{
						lstSubAccountReportInfo.Insert(0, objSubAccount);
					}

					var lstAccountBillingDetails = GetGroupBillingDetails(lstSubAccountReportInfo, loggedAccountCurrency,
																			datePattern, ref totalAmount);

					if (lstAccountBillingDetails.Count > 0)
                    {
                        lstAccountReportDetails.Add(new ListAccountReportDetails
                        {
                            SubAccountName = objSubAccount.AccountName,
                            AccountBillingDetails = lstAccountBillingDetails,
                            Total = GMGColorFormatData.GetFormattedCurrency(totalAmount, loggedAccountCurrency)
                        });
                    }

                    grandTotal += totalAmount;
                }

                //accounts that don t have their parents included in the report must be treated separately
                if (lstReportInfo.Count > 0)
                {
                    foreach (GMGColorDAL.ReturnBillingReportInfoView objSubAccount in lstReportInfo)
                    {
                        decimal totalAmount = 0;
                        var lstAccountBillingDetails = GetGroupBillingDetails(new List<ReturnBillingReportInfoView>{ objSubAccount}, loggedAccountCurrency,
                            datePattern, ref totalAmount);

                        if (lstAccountBillingDetails.Count > 0)
                        {
                            lstAccountReportDetails.Add(new ListAccountReportDetails
                            {
                                SubAccountName = objSubAccount.AccountName,
                                AccountBillingDetails = lstAccountBillingDetails,
                                Total = GMGColorFormatData.GetFormattedCurrency(totalAmount, loggedAccountCurrency)
                            });
                        }

                        grandTotal += totalAmount;
                    }
                }
            }

            model.ListAccountBillingReportDetails = lstAccountReportDetails;
            model.GrandTotal = GMGColorFormatData.GetFormattedCurrency(grandTotal, loggedAccountCurrency);

            return model;
        }

        private List<AccountBillingDetails> GetGroupBillingDetails(IEnumerable<ReturnBillingReportInfoView> groupBillinInfo, string loggedAccountCurrency, string datePattern, ref decimal totalAmount)
        {
            var lstAccountBillingDetails = new List<AccountBillingDetails>();

            foreach (GMGColorDAL.ReturnBillingReportInfoView objReportInfo in groupBillinInfo)
            {
                #region Collaborate

                if (objReportInfo.CollaborateBillingPlan > 0)
                {
                    GetSubscriptionBillingInfoViewModel(new ModuleSubsciptionBillingDetails
                    {
                        Account = objReportInfo.ID,
                        AccountName = objReportInfo.AccountName,
                        AccountPath = objReportInfo.AccountPath,
                        AccountTypeName = objReportInfo.AccountTypeName,
                        BillingCycleDetails = objReportInfo.CollaborateBillingCycleDetails,
                        BillingPlan = objReportInfo.CollaborateBillingPlan,
                        BillingPlanName = objReportInfo.CollaborateBillingPlanName,
                        ContractStartDate =
                            objReportInfo.CollaborateContractStartDate.GetValueOrDefault(),
                        GPPDiscount = objReportInfo.GPPDiscount,
                        IsFixedPlan = objReportInfo.CollaborateIsFixedPlan.GetValueOrDefault(),
                        IsMonthlyBillingFrequency =
                            objReportInfo.CollaborateIsMonthlyBillingFrequency.GetValueOrDefault(),
                        Proofs = objReportInfo.CollaborateProofs,
                        ModuleName = Resources.Resources.lblCollaborateModuleName,
                        IsQuotaAllocationEnabled = objReportInfo.CollaborateIsQuotaAllocationEnabled.GetValueOrDefault()
                    }, loggedAccountCurrency, datePattern, lstAccountBillingDetails, ref totalAmount);
                }
                #endregion

                #region Deliver

                if (objReportInfo.DeliverBillingPlan > 0)
                {
                    GetSubscriptionBillingInfoViewModel(new ModuleSubsciptionBillingDetails
                    {
                        Account = objReportInfo.ID,
                        AccountName = objReportInfo.AccountName,
                        AccountPath = objReportInfo.AccountPath,
                        AccountTypeName = objReportInfo.AccountTypeName,
                        BillingCycleDetails = objReportInfo.DeliverBillingCycleDetails,
                        BillingPlan = objReportInfo.DeliverBillingPlan,
                        BillingPlanName = objReportInfo.DeliverBillingPlanName,
                        ContractStartDate =
                            objReportInfo.DeliverContractStartDate.GetValueOrDefault(),
                        GPPDiscount = objReportInfo.GPPDiscount,
                        IsFixedPlan = objReportInfo.DeliverIsFixedPlan.GetValueOrDefault(),
                        IsMonthlyBillingFrequency =
                            objReportInfo.DeliverIsMonthlyBillingFrequency.GetValueOrDefault(),
                        Proofs = objReportInfo.DeliverProofs,
                        ModuleName = Resources.Resources.lblDeliverModuleName,
                        IsQuotaAllocationEnabled = objReportInfo.DeliverIsQuotaAllocationEnabled.GetValueOrDefault()
                    }, loggedAccountCurrency, datePattern, lstAccountBillingDetails, ref totalAmount);
                }
                #endregion
            }

            return lstAccountBillingDetails;
        }

        /// <summary>
        /// Gets all hierarchical children for the current account
        /// </summary>
        /// <param name="currentAccountId"></param>
        /// <param name="accountsInfo"></param>
        /// <returns></returns>
        private List<ReturnBillingReportInfoView> GetChildsForCurrentAccount(int currentAccountId,
            List<ReturnBillingReportInfoView> accountsInfo)
        {
            var childAccounts = new List<ReturnBillingReportInfoView>();

            foreach (var acc in accountsInfo)
            {
                int parent = acc.Parent;
                while (parent > 0)
                {
                    if (parent == currentAccountId && (acc.CollaborateBillingPlan > 0 ||
                            acc.DeliverBillingPlan > 0))
                    {
                        childAccounts.Add(acc);
                    }

                    var parentAcc = accountsInfo.FirstOrDefault(t => t.ID == parent);
                    parent = parentAcc != null ? parentAcc.Parent : 0;
                }
            }

            accountsInfo = accountsInfo.Except(childAccounts).ToList();
            return childAccounts;
        }

        private bool CanAccessTree(int accountId, DbContextBL context)
        {
            try
            {
                return DALUtils.IsFromCurrentAccountTree<GMGColorDAL.Account>(accountId, LoggedAccount.ID, context);
            }
            finally
            {
            }
        }

        private void GetSubscriptionBillingInfoViewModel(ModuleSubsciptionBillingDetails subscription,
            string loggedAccountCurrency, string datePattern, List<AccountBillingDetails> lstAccountBillingDetails,
            ref decimal totalAmount)
        {
            //get cycles per month
            string[] monthCycles =
                subscription.BillingCycleDetails.Split(new[] {','},
                    StringSplitOptions.RemoveEmptyEntries);

            decimal costPerProof = subscription.IsFixedPlan
                ? 0
                : GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(this.LoggedAccount.ID,
                    subscription.BillingPlan, LoggedAccountType, Context);

            DateTime contractStartDate =
                subscription.ContractStartDate;
            DateTime contractEndDate =
                subscription.ContractStartDate.AddYears(1);

            foreach (string monthCycle in monthCycles)
            {
                string[] currentMonthCycleDetails = monthCycle.Split('|');

                int addedProofsCount =
                    int.Parse(currentMonthCycleDetails[1]);
                int totalExceededCount =
                    int.Parse(currentMonthCycleDetails[2]);
                int proofsAllowedCount =
                    int.Parse(currentMonthCycleDetails[4]);

                decimal planPrice =
                    int.Parse(currentMonthCycleDetails[3]);
                decimal discount = (planPrice*(subscription.GPPDiscount/100));
                decimal discountedPrice = planPrice - discount;

                string currentMonthDate = currentMonthCycleDetails[0];

                //get year and month (year is represented by last two digits, month by first 3 chars from month name)
                string[] currentMonthInfo = currentMonthDate.Split('-');
                int monthYear = int.Parse(currentMonthInfo[1]);
                string currentMonthName = currentMonthInfo[0].ToLower();

                bool isExpired = false;
                DateTime currentMonth;
                DateTime.TryParseExact(currentMonthName, "MMM", CultureInfo.InvariantCulture,
                    DateTimeStyles.AllowWhiteSpaces, out currentMonth);

                //check if cotnract end date is lower then current month , if so mark it as expired
                if (contractEndDate.Year%100 < monthYear ||
                    contractEndDate.Year%100 == monthYear && contractEndDate.Month < currentMonth.Month)
                {
                    isExpired = true;
                }

                // if freq is early check to see if current month is contract first month and set monthly fee only for it
                if (!subscription.IsMonthlyBillingFrequency)
                {
                    //if this month is the contract start date show fee for all contract , else set value to 0
                    if (contractStartDate.Year%100 ==
                        monthYear &&
                        contractStartDate.ToString("MMM").ToLower() == currentMonthName)
                    {
                        discountedPrice = discountedPrice*12;
                    }
                    else
                    {
                        discountedPrice = 0;
                    }
                }

                decimal amountToPay = discountedPrice + totalExceededCount*costPerProof;

                totalAmount += amountToPay;

                lstAccountBillingDetails.Add(new AccountBillingDetails
                {
                    ID = subscription.Account,
                    Name = subscription.AccountName,
                    Type = subscription.AccountTypeName,
                    Plan =
                        subscription.BillingPlanName + " <br/> <small>(" +
                        String.Format("{0:0.##}", subscription.GPPDiscount) + "%)</small>",
                    PricePerMonth =
                        GMGColorFormatData.GetFormattedCurrency(discountedPrice,
                            loggedAccountCurrency),
                    Usage =
                        (subscription.IsFixedPlan)
                            ? ""
                            : addedProofsCount + "/" + (subscription.IsQuotaAllocationEnabled ? proofsAllowedCount : subscription.Proofs),
                    TotalExceededProofs = totalExceededCount,
                    CostPerProof = costPerProof,
                    Amount =
                        GMGColorFormatData.GetFormattedCurrency(amountToPay, loggedAccountCurrency),
                    AccountPath = subscription.AccountPath,
                    ExceededProofsString =
                        !subscription.IsFixedPlan
                            ? Reports.GetExceededProofsString(costPerProof,
                                totalExceededCount, loggedAccountCurrency)
                            : "-",
                    ModuleName = subscription.ModuleName,
                    BillingPlanIsFixed = subscription.IsFixedPlan,
                    BillingMonth = currentMonthDate,
                    ContractStartDate = GMGColorFormatData.GetFormattedDate(contractStartDate, datePattern),
                    ContractEndDate = GMGColorFormatData.GetFormattedDate(contractEndDate, datePattern),
                    BillingFrequency = subscription.IsMonthlyBillingFrequency
                        ? Resources.Resources.lblMonthly
                        : Resources.Resources.lblYearly,
                    IsExpired = isExpired
                });
            }
        }

        private Reports GetFileReportData(ReportFileData dicFileReportPara, DbContextBL context)
        {
            if (dicFileReportPara == null)
                throw new Exception("dicFileReportPara is null");

            Reports model = new Reports(Context,LoggedAccount);

            string datePattern = LoggedAccount.DateFormat1.Pattern;
            model.SelectedAccounts = dicFileReportPara.SelectedAccounts;
            model.SelectedUserGroup= dicFileReportPara.SelectedUserGroup;

            model.FromDate = dicFileReportPara.SelectedStartDate.ToString(datePattern);
            model.ToDate = dicFileReportPara.SelectedEndDate.ToString(datePattern);

            DateTime toDate = DateTime.ParseExact(model.ToDate, datePattern, CultureInfo.InvariantCulture);
            DateTime fromDate = DateTime.ParseExact(model.FromDate, datePattern, CultureInfo.InvariantCulture);

            ViewBag.startDateYear = fromDate.Year;
            ViewBag.startDateMonth = fromDate.Month - 1;
            ViewBag.startDateDay = fromDate.Day;
            ViewBag.endDateYear = toDate.Year;
            ViewBag.endDateMonth = toDate.Month - 1;
            ViewBag.endDateDay = toDate.Day;

            var lstAccountReportDetails = new List<ListAccountReportDetails>();
            string loggedAccountCurrency = LoggedAccount.Currency.Symbol;
            List<FileReportDetails> lstUsers = new List<FileReportDetails>();

            //For Account type 5
            if ((int)this.LoggedAccountType == 5)
            {
                string IEAccountId = LoggedAccount.ID.ToString();
                IEnumerable<string> m_oEnum = new List<string>() { IEAccountId };
                model.SelectedAccounts = m_oEnum;
            }

            IEnumerable<string> selectedUserGroups = null;
            List<UserGroupUserVisibility> UserGroupVisibility = context.UserGroupUserVisibilities.Where(o => o.User == this.LoggedUser.ID).ToList();
            if (LoggedUserCollaborateRole == Role.RoleName.AccountManager && UserGroupVisibility.Count() > 0 && model.SelectedUserGroup.Count() == 0)
            {
                selectedUserGroups = UserGroupVisibility.Select(t => t.UserGroup.ToString()).ToList();
                List<int> groupIds = UserGroupVisibility.Select(t => t.UserGroup).ToList();

                if (LoggedUserReportRole == Role.RoleName.AccountModerator)
                {
                    model.ListUserGroups = Context.UserGroups
                                .Join(Context.UserGroupUsers,
                                post => post.ID,
                                meta => meta.UserGroup,
                                (post, meta) => new { Post = post, Meta = meta }) // selection
                                .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                                .Where(a => groupIds.Contains(a.Post.ID))
                                .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                                .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();

                    if (model.SelectedUserGroup.Count() == 0)
                    {
                        selectedUserGroups = model.ListUserGroups.Select(x => x.Key.ToString()).ToArray();
                    }
                }
                else
                {
                    model.ListUserGroups = Context.UserGroups.Where(x => model.SelectedAccounts.Contains(x.Account.ToString())).Where(a => groupIds.Contains(a.ID)).Select(o => new { o.ID, o.Name })
                    .AsEnumerable().Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
                }
            }
            else
            {
                selectedUserGroups = model.SelectedUserGroup;
            if (LoggedUserReportRole == Role.RoleName.AccountModerator)
            {
                model.ListUserGroups = Context.UserGroups
                            .Join(Context.UserGroupUsers,
                            post => post.ID,
                            meta => meta.UserGroup,
                            (post, meta) => new { Post = post, Meta = meta }) // selection
                            .Where(postAndMeta => postAndMeta.Meta.User == this.LoggedUser.ID)
                            .Select(x => new { ID = x.Post.ID, Name = x.Post.Name }).AsEnumerable()
                            .Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();

                if(model.SelectedUserGroup.Count() == 0)
                {
                    selectedUserGroups = model.ListUserGroups.Select(x => x.Key.ToString()).ToArray();
                }
            }
            else
            {
                model.ListUserGroups = Context.UserGroups.Where(x => model.SelectedAccounts.Contains(x.Account.ToString())).Select(o => new { o.ID, o.Name })
                .AsEnumerable().Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
            }
            }

            List<GMGColorDAL.ReturnFileReportInfoView> lstReportInfo = new GMGColorDAL.User().GetFileReportInfo(string.Join(",", model.SelectedAccounts), string.Join(",", selectedUserGroups), fromDate, toDate.AddDays(1), Convert.ToInt32(dicFileReportPara.SelectedReportType), Context);

            foreach (GMGColorDAL.ReturnFileReportInfoView user in lstReportInfo)
            {
                lstUsers.Add(new FileReportDetails(user, this.LoggedAccount, context));
            }
            switch(dicFileReportPara.SelectedReportType)
            {
                case 1: { model.FileReportType = "1"; break; }
                case 2: { model.FileReportType = "2"; break; }
                case 3: { model.FileReportType = "3"; break; }
                case 4: { model.FileReportType = "4"; break; }
                default: { model.FileReportType = "0"; break; }
            }
            
            model.ListFileReportDetails = lstUsers;
            return model;
        }


        #endregion
    }

}
