﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using GMGColor.Common;
using System.Transactions;
using AutoMapper;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using GMG.CoZone.Collaborate.Services.Approval;
using GMG.CoZone.Collaborate.Interfaces.Proofstudio;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common;

namespace GMGColor.Controllers
{
    public class StudioController : BaseController
    {

        IApprovalPageService _approvalPageService = new ApprovalPageService();
        IApprovalService _approvalService;
        IApprovalUserViewInfoService _approvalUserViewInfoService;

        public StudioController(IApprovalService approvalService, IApprovalUserViewInfoService approvalUserViewInfoService, IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {
            _approvalService = approvalService;
            _approvalUserViewInfoService = approvalUserViewInfoService;
        }

        #region GET Actions

        [HttpGet]
        //TODO check if this action is needed
        public ActionResult Index()
        {
            Session["SelectedApproval"] = null;
            Dashboard model = new Dashboard(this.LoggedAccount.ID, Context);
            model.IsWelcome = (Session["gmg_swscr_" + this.LoggedAccount.ID] != null) ? true : false;

            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Html()
        {
            return GetResultURL(false);
        }

        [HttpGet]
        public ActionResult HtmlViewer(string apg, int? aid)
        {
            var approvalId = ApprovalBL.GetApprovalIdByGuid(apg, Context);
            CreateSessionVariables(approvalId, LoggedUser.ID);

            //if(aid != null && aid > 0)
            //{
                CreateAnnotationSession(aid);
            //}

            if (Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] != null && 
                    (Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] != null ? 
                    int.Parse(Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()].ToString().Trim()) == -8 ?
                    Session[GMG.CoZone.Common.Constants.OutOfOfficeUser] != null : false : false))
            {
                return RedirectToAction("RedirectHtml5");                
            }

            if (!CanAccessApproval(approvalId, Approval.ApprovalOperation.ViewApproval))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            return RedirectToAction("RedirectHtml5");
        }

        private void CreateAnnotationSession(int? annotationID)
        {
            Session["AnnotationAtUserInComment"] = annotationID;
        }
        private void CreateSessionVariables(int approvalId, int loggeUserId)
        {
            string sessionKey = (new Random()).Next(1000, 9999).ToString();
            Session["SessionKey"] = sessionKey;

            Session["StudioApproval_" + sessionKey] = new List<int> { approvalId };
            Session["StudioLoggedUser_" + sessionKey] = loggeUserId;           
        }

        [HttpGet]
        public ActionResult HtmlViewerExternal(string apg, string sessionKey)
        {
            Session["SessionKey"] = sessionKey;

            var approval_Id = new List<int> { ApprovalBL.GetApprovalIdByGuid(apg, Context) };
            var user_Id = ApprovalBL.GetUserIdByCollaborateAPISessionKey(sessionKey, Context);
            Session["StudioApproval_" + sessionKey] = approval_Id;
            Session["StudioLoggedUser_" + sessionKey] = ApprovalBL.GetUserIdByCollaborateAPISessionKey(sessionKey, Context);

            return GetResultURLForAPIUsers(approval_Id[0] , user_Id);
            //return RedirectToAction("RedirectHtml5");
        }

        [HttpGet]
        public ActionResult HtmlViewerCompareVersions(string v1Guid, string v2Guid, string sessionKey)
        {
            Session["SessionKey"] = sessionKey;

            Session["StudioVersionsToCompare_" + sessionKey] = new List<int> { ApprovalBL.GetApprovalIdByGuid(v1Guid, Context), ApprovalBL.GetApprovalIdByGuid(v2Guid, Context) };
            Session["StudioLoggedUser_" + sessionKey] = ApprovalBL.GetUserIdByCollaborateAPISessionKey(sessionKey, Context);

            return RedirectToAction("RedirectHtml5");
        }

        [HttpGet]
        public ActionResult Flex()
        {
            return GetResultURL(false);
        }

        [HttpGet]
        public ActionResult Redirect()
        {
            return GetResultURL(true);
        }

        [HttpGet]
        public ActionResult RedirectHtml5(bool? isExternal)
        {
            return GetResultURL(isExternal.GetValueOrDefault());
        }

        #endregion

        #region POST Actions

        #endregion

        #region Methods

        private ActionResult GetResultURL(bool isExternal)
        {
            string sessionKey = ((Session["SessionKey"] != null) ? Session["SessionKey"].ToString() : "0");
            string PSSessionKey = Guid.NewGuid().ToString();
            try
            {
                List<int> approvalIds;

                List<int> explicitVersions = ExplicitVersionsToCompare(sessionKey);
                if (explicitVersions.Any())
                {
                    approvalIds = GetApprovalsIdsForVersionsIds(explicitVersions);
                    @ViewBag.VersionsIds = string.Join(",", explicitVersions.ToArray());
                    @ViewBag.ExplicitVersionsToCompare = @ViewBag.VersionsIds;
                }
                else
                {
                    @ViewBag.ExplicitVersionsToCompare = "";

                    approvalIds = Session["StudioApproval_" + sessionKey] as List<int>;
                    int selectedApprovalIDFromProof = ApiController.GetSelectedApprovalID();
                    if (selectedApprovalIDFromProof > 0)
                    {
                        List<int> approvalid = new List<int>();
                        approvalid.Add(selectedApprovalIDFromProof);
                        approvalIds = approvalid;
                        ApiController.SetSelectedApprovalIDNull();
                    }
                    int SelectedProjectFolderID = Session[Constants.SelectedProjectId] != null ? int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                    ViewBag.IsfromProjectFolder = SelectedProjectFolderID > 0 ? true : false;
                    ViewBag.ProjectFolderID = 0;
                    if (ViewBag.IsfromProjectFolder)
                    {
                        int projectFolderID = ProjectFolderBL.GetProjectFolderIdFromApproval(approvalIds[0]);
                        ViewBag.ProjectFolderID = SelectedProjectFolderID == projectFolderID ? SelectedProjectFolderID : projectFolderID;
                    }
                    // In case approval ids are missing (call is made from UI)
                    if (approvalIds != null &&
                        // In case approval ids are missing (call is made from API)
                        (approvalIds.Count > 0 && !approvalIds.Any(apId => apId == 0)))
                    {
                        @ViewBag.VersionsIds = string.Join(",", ApiBL.GetVersionsIds(approvalIds).ToArray());
                    }
                    else
                    {
                        return StudioWithLastVisitedApproval(LoggedUser.ID);
                    }                    
                }
                int scrollTop = ApiController.GetScrollTop();
                ViewBag.ScrollTop = scrollTop > 0 ? scrollTop : 0;
                ApiController.SetScrollTopNull();
                List<string> selectedApprovalTypes = ApiController.GetSelectedApprovalType();
                ViewBag.SelectedApprovalTypes = string.Empty;
                if (selectedApprovalTypes != null && selectedApprovalTypes.Count > 0)
                {
                    ApiController.SetSelectedApprovalTypesNull();
                    string approvalTypes = string.Empty;
                    foreach(var type in selectedApprovalTypes)
                    {
                        if(approvalTypes == string.Empty)
                        {
                            approvalTypes = type;
                        } else
                        {
                            approvalTypes = approvalTypes + "," + type;
                        }
                    }
                    ViewBag.SelectedApprovalTypes = approvalTypes;
                }
                var approvalId = approvalIds[0];
                ViewBag.selectedHtmlPageId = 0;
                @ViewBag.ApprovalFirstPageId = _approvalPageService.GetApprovalFirstPageId(approvalId);
                @ViewBag.ApprovalType = _approvalService.GetApprovalType(approvalId);
                if(ViewBag.ApprovalType == Approval.ApprovalTypeEnum.Zip)
                {
                    var  result = ApiController.GetHtmlPageId();
                    var pageID = ApiBL.GetHtmlPageId(approvalId);
                    if (pageID > 0)
                    {
                        ViewBag.selectedHtmlPageId = pageID;
                    } else
                    {
                        ViewBag.selectedHtmlPageId = result;
                    }
                    ApiController.SetPageIdNull();
                }

                @ViewBag.ApprovalId = approvalIds[0];
                @ViewBag.Approvals = string.Join(",", approvalIds.ToArray());
                

                int accountId = Convert.ToString(Session["gmg_aid"]).ToInt().GetValueOrDefault();
                int exOrUserId = Convert.ToString(Session["StudioLoggedUser_" + sessionKey]).ToInt().GetValueOrDefault();
                @ViewBag.substituteLoggedUser = 0;
                @ViewBag.substituteOwnerUser = 0;
                @ViewBag.substituteLoggedUserName = 0;
                ViewBag.externalSessionKey = sessionKey.Length == PSSessionKey.Length ? true : false;

                if (Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] != null && int.Parse(Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()].ToString().Trim()) == -8)
                {
                    Session[GMG.CoZone.Common.Constants.SelectedTopLinkId] = -8;
                    var executorUser = int.Parse(Session[GMG.CoZone.Common.Constants.OutOfOfficeUser].ToString());
                    exOrUserId = executorUser;
                    @ViewBag.substituteLoggedUser = Convert.ToString(Session["StudioLoggedUser_" + sessionKey]).ToInt().GetValueOrDefault();
                    @ViewBag.substituteOwnerUser = exOrUserId;
                    @ViewBag.substituteLoggedUserName = LoggedUser.GivenName + " " + LoggedUser.FamilyName;
                }
                int jobId = 0;
                
                this.UpdateInformation(Context, approvalId, exOrUserId, isExternal, PSSessionKey);
                jobId = ApprovalBL.GetJobId(Context, approvalId);
                @ViewBag.ColorTheme = AccountBL.GetColorSchemeByAccountId(Context, accountId, exOrUserId, isExternal);
                @ViewBag.UserKey = UserBL.GetUserGuid(exOrUserId, isExternal, Context);
                ViewBag.SelectedAnnotationID = Session["AnnotationAtUserInComment"] != null ? Session["AnnotationAtUserInComment"] : 0;
                if (Session["AnnotationAtUserInComment"] != null)
                {
                    ViewBag.SelectedAnnotationID = AnnotationBL.GetParentAnnotationId(ViewBag.SelectedAnnotationID, Context);
                }
                Session["AnnotationAtUserInComment"] = null;

                @ViewBag.SessionKey = sessionKey;
                @ViewBag.Url = (Session["StudioReturnURL_" + sessionKey] != null)
                    ? Convert.ToString(Session["StudioReturnURL_" + sessionKey])
                    : "/Auth/Login";
                @ViewBag.SecurityKey = GMGColorConfiguration.AppConfiguration.RestApiSecurityToken;
                @ViewBag.RestApiHostURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + @"://api" + GMGColorConfiguration.AppConfiguration.HostAddress;
                @ViewBag.Namespace = "v1";
                @ViewBag.IsExternal = isExternal;
                @ViewBag.PSSessionKey = PSSessionKey;
                @ViewBag.UserLocaleName = ApiBL.GetUserLocaleName(@ViewBag.UserKey, isExternal);

                @ViewBag.NewVersionUrl = Url.Action("NewApproval", "Approvals", new { JobId = jobId });
                @ViewBag.NewVersionUrlTemplate = Url.Action("NewApproval", "Approvals");

                @ViewBag.DownloadFileUrl =
                    string.Format("{0}/{5}/download?apiKey={1}&userKey={2}&isExternal={3}&sessionKey={4}",
                        ViewBag.RestApiHostURL,
                        GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken,
                        ViewBag.UserKey,
                        isExternal,
                        PSSessionKey,
                        ViewBag.Namespace);
                @ViewBag.PrintVersionUrl = Url.Action("Annotations", "Approvals", routeValues: null, protocol: GMGColorConfiguration.AppConfiguration.ServerProtocol);
                @ViewBag.HasAccessToDeliver = !isExternal && ApiBL.validateUserAccessToAppModule((string)@ViewBag.UserKey, GMG.CoZone.Common.AppModule.Deliver, Context);
                @ViewBag.IsApprovalZoomLevelChangesEnabled = ApprovalBL.IsProofStudioZoomLevelEnabled(accountId, Context);
                @ViewBag.ApprovalZoomLevel = @ViewBag.IsApprovalZoomLevelChangesEnabled > 0 ? ApprovalBL.GetApprovalProofStudioZoomLevel(approvalIds[0], accountId, Context): 0;
                @ViewBag.UserName = UserBL.GetUserName(exOrUserId, isExternal, Context);
                @ViewBag.ApprovalKey = ApprovalBL.GetApprovalGuidById(approvalId, Context);
                @ViewBag.ShowSubstrateFromICCProfileInProofStudio = ApprovalBL.IsShowSubstrateFromICCProfileInProofStudio(accountId, Context);
                @ViewBag.IsMeasurementsInInches = LoggedAccount.IsMeasurementsInInches;
                ApprovalBL.JobStatusAutoUpdate(approvalIds, Context);

                return View("html");
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return RedirectToAction("Unauthorised", "Error");
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return RedirectToAction("Unauthorised", "Error");
            }
        }

        private ActionResult GetResultURLForAPIUsers(int approval_Id , int user_Id)
        {
            bool isExternal = false;
            string sessionKey = ((Session["SessionKey"] != null) ? Session["SessionKey"].ToString() : "0");
            string PSSessionKey = Guid.NewGuid().ToString();
            try
            {
                List<int> approvalIds;

                List<int> explicitVersions = ExplicitVersionsToCompare(sessionKey);
                if (explicitVersions.Any())
                {
                    approvalIds = GetApprovalsIdsForVersionsIds(explicitVersions);
                    @ViewBag.VersionsIds = string.Join(",", explicitVersions.ToArray());
                    @ViewBag.ExplicitVersionsToCompare = @ViewBag.VersionsIds;
                }
                else
                {
                    @ViewBag.ExplicitVersionsToCompare = "";

                    approvalIds = Session["StudioApproval_" + sessionKey] as List<int>;
                    int selectedApprovalIDFromProof = ApiController.GetSelectedApprovalID();
                    if (selectedApprovalIDFromProof > 0)
                    {
                        List<int> approvalid = new List<int>();
                        approvalid.Add(selectedApprovalIDFromProof);
                        approvalIds = approvalid;
                        ApiController.SetSelectedApprovalIDNull();
                    }
                    int SelectedProjectFolderID = Session[Constants.SelectedProjectId] != null ? int.Parse(Session[Constants.SelectedProjectId].ToString()) : 0;
                    ViewBag.IsfromProjectFolder = SelectedProjectFolderID > 0 ? true : false;
                    ViewBag.ProjectFolderID = 0;
                    if (ViewBag.IsfromProjectFolder)
                    {
                        int projectFolderID = ProjectFolderBL.GetProjectFolderIdFromApproval(approvalIds[0]);
                        ViewBag.ProjectFolderID = SelectedProjectFolderID == projectFolderID ? SelectedProjectFolderID : projectFolderID;
                    }
                    // In case approval ids are missing (call is made from UI)
                    if (approvalIds != null &&
                        // In case approval ids are missing (call is made from API)
                        (approvalIds.Count > 0 && !approvalIds.Any(apId => apId == 0)))
                    {
                        @ViewBag.VersionsIds = string.Join(",", ApiBL.GetVersionsIds(approvalIds).ToArray());
                    }
                    else
                    {
                        return StudioWithLastVisitedApproval(LoggedUser.ID);
                    }
                }
                if (approvalIds == null && approval_Id > 0)
                {
                    List<int> app_id = new List<int>();
                    app_id.Add(approval_Id);
                    approvalIds = app_id;
                }
                int scrollTop = ApiController.GetScrollTop();
                ViewBag.ScrollTop = scrollTop > 0 ? scrollTop : 0;
                ApiController.SetScrollTopNull();
                List<string> selectedApprovalTypes = ApiController.GetSelectedApprovalType();
                ViewBag.SelectedApprovalTypes = string.Empty;
                if (selectedApprovalTypes != null && selectedApprovalTypes.Count > 0)
                {
                    ApiController.SetSelectedApprovalTypesNull();
                    string approvalTypes = string.Empty;
                    foreach (var type in selectedApprovalTypes)
                    {
                        if (approvalTypes == string.Empty)
                        {
                            approvalTypes = type;
                        }
                        else
                        {
                            approvalTypes = approvalTypes + "," + type;
                        }
                    }
                    ViewBag.SelectedApprovalTypes = approvalTypes;
                }
                var approvalId = approvalIds[0];
                ViewBag.selectedHtmlPageId = 0;
                @ViewBag.ApprovalFirstPageId = _approvalPageService.GetApprovalFirstPageId(approvalId);
                @ViewBag.ApprovalType = _approvalService.GetApprovalType(approvalId);
                if (ViewBag.ApprovalType == Approval.ApprovalTypeEnum.Zip)
                {
                    var result = ApiController.GetHtmlPageId();
                    var pageID = ApiBL.GetHtmlPageId(approvalId);
                    if (pageID > 0)
                    {
                        ViewBag.selectedHtmlPageId = pageID;
                    }
                    else
                    {
                        ViewBag.selectedHtmlPageId = result;
                    }
                    ApiController.SetPageIdNull();
                }

                @ViewBag.ApprovalId = approvalIds[0];
                @ViewBag.Approvals = string.Join(",", approvalIds.ToArray());


                int accountId = Convert.ToString(Session["gmg_aid"]).ToInt().GetValueOrDefault();
                int exOrUserId = Convert.ToString(Session["StudioLoggedUser_" + sessionKey]).ToInt().GetValueOrDefault();
                if (exOrUserId == 0 && user_Id > 0)
                {
                    exOrUserId = user_Id;
                }
                @ViewBag.substituteLoggedUser = 0;
                @ViewBag.substituteOwnerUser = 0;
                @ViewBag.substituteLoggedUserName = 0;
                ViewBag.externalSessionKey = sessionKey.Length == PSSessionKey.Length ? true : false;

                if (Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()] != null && int.Parse(Session[GMG.CoZone.Common.Constants.ApprovalsTopLink + this.LoggedUser.ID.ToString()].ToString().Trim()) == -8)
                {
                    Session[GMG.CoZone.Common.Constants.SelectedTopLinkId] = -8;
                    var executorUser = int.Parse(Session[GMG.CoZone.Common.Constants.OutOfOfficeUser].ToString());
                    exOrUserId = executorUser;
                    @ViewBag.substituteLoggedUser = Convert.ToString(Session["StudioLoggedUser_" + sessionKey]).ToInt().GetValueOrDefault();
                    @ViewBag.substituteOwnerUser = exOrUserId;
                    @ViewBag.substituteLoggedUserName = LoggedUser.GivenName + " " + LoggedUser.FamilyName;
                }
                int jobId = 0;

                this.UpdateInformation(Context, approvalId, exOrUserId, isExternal, PSSessionKey);
                jobId = ApprovalBL.GetJobId(Context, approvalId);
                @ViewBag.ColorTheme = AccountBL.GetColorSchemeByAccountId(Context, accountId, exOrUserId, isExternal);
                @ViewBag.UserKey = UserBL.GetUserGuid(exOrUserId, isExternal, Context);
                ViewBag.SelectedAnnotationID = Session["AnnotationAtUserInComment"] != null ? Session["AnnotationAtUserInComment"] : 0;
                if (Session["AnnotationAtUserInComment"] != null)
                {
                    ViewBag.SelectedAnnotationID = AnnotationBL.GetParentAnnotationId(ViewBag.SelectedAnnotationID, Context);
                }
                Session["AnnotationAtUserInComment"] = null;

                @ViewBag.SessionKey = sessionKey;
                @ViewBag.Url = (Session["StudioReturnURL_" + sessionKey] != null)
                    ? Convert.ToString(Session["StudioReturnURL_" + sessionKey])
                    : "/Auth/Login";
                @ViewBag.SecurityKey = GMGColorConfiguration.AppConfiguration.RestApiSecurityToken;
                @ViewBag.RestApiHostURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + @"://api" + GMGColorConfiguration.AppConfiguration.HostAddress;
                @ViewBag.Namespace = "v1";
                @ViewBag.IsExternal = isExternal;
                @ViewBag.PSSessionKey = PSSessionKey;
                @ViewBag.UserLocaleName = ApiBL.GetUserLocaleName(@ViewBag.UserKey, isExternal);

                @ViewBag.NewVersionUrl = Url.Action("NewApproval", "Approvals", new { JobId = jobId });
                @ViewBag.NewVersionUrlTemplate = Url.Action("NewApproval", "Approvals");

                @ViewBag.DownloadFileUrl =
                    string.Format("{0}/{5}/download?apiKey={1}&userKey={2}&isExternal={3}&sessionKey={4}",
                        ViewBag.RestApiHostURL,
                        GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken,
                        ViewBag.UserKey,
                        isExternal,
                        PSSessionKey,
                        ViewBag.Namespace);
                @ViewBag.PrintVersionUrl = Url.Action("Annotations", "Approvals", routeValues: null, protocol: GMGColorConfiguration.AppConfiguration.ServerProtocol);
                @ViewBag.HasAccessToDeliver = !isExternal && ApiBL.validateUserAccessToAppModule((string)@ViewBag.UserKey, GMG.CoZone.Common.AppModule.Deliver, Context);
                @ViewBag.IsApprovalZoomLevelChangesEnabled = ApprovalBL.IsProofStudioZoomLevelEnabled(accountId, Context);
                @ViewBag.ApprovalZoomLevel = @ViewBag.IsApprovalZoomLevelChangesEnabled > 0 ? ApprovalBL.GetApprovalProofStudioZoomLevel(approvalIds[0], accountId, Context) : 0;
                @ViewBag.UserName = UserBL.GetUserName(exOrUserId, isExternal, Context);
                @ViewBag.ApprovalKey = ApprovalBL.GetApprovalGuidById(approvalId, Context);
                @ViewBag.ShowSubstrateFromICCProfileInProofStudio = ApprovalBL.IsShowSubstrateFromICCProfileInProofStudio(accountId, Context);

                ApprovalBL.JobStatusAutoUpdate(approvalIds, Context);

                return View("html");
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
                return RedirectToAction("Unauthorised", "Error");
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return RedirectToAction("Unauthorised", "Error");
            }
        }

        private bool CanAccessApproval(int approvalId, GMGColorDAL.Approval.ApprovalOperation operation)
        {
            return CanAccessApproval(new int[] { approvalId }, operation);
        }

        private bool CanAccessApproval(IEnumerable<int> approvalIdList, GMGColorDAL.Approval.ApprovalOperation operation)
        {
            
            return
                   GMGColorDAL.Approval.CanAccess(LoggedUser.ID, approvalIdList, operation, Context)
                   && (DALUtils.IsFromCurrentAccount<GMGColorDAL.Approval>(approvalIdList, LoggedAccount.ID, Context));
        }

        private void ValidateApprovalId(int? approvalId, int loggedUserId)
        {
            if (approvalId == null || approvalId <= 0)
            {
                // the user did not access any approval or it is an external user
                System.Diagnostics.StackTrace t = new System.Diagnostics.StackTrace(true);
                String trace = t.ToString();
                GMGColorDAL.GMGColorLogging.log.ErrorFormat(new Exception(Resources.Resources.txtNoApprovalIdsFound),
                    "{0}; UserId: {1}; StackTrace: {2}",
                    Resources.Resources.txtNoApprovalIdsFound, loggedUserId, trace);
                throw new Exception(Resources.Resources.txtNoApprovalIdsFound);
            }
        }

        private bool IsCaseToRedirectToDashboard(int? approvalId, int loggedUserId)
        {
            return (loggedUserId > 0) && (approvalId == null || approvalId <= 0);
        }

        private ActionResult StudioWithLastVisitedApproval(int loggedUserId)
        {
            var approvalId = _approvalUserViewInfoService.GetLatestVisitedApproval(loggedUserId);

            if (IsCaseToRedirectToDashboard(approvalId, loggedUserId))
            {
                return RedirectToAction("Index", "Approvals");
            }

            ValidateApprovalId(approvalId, loggedUserId);

            CreateSessionVariables(approvalId.GetValueOrDefault(), loggedUserId);

            return RedirectToAction("Html");
        }

        private List<int> ExplicitVersionsToCompare(string sessionKey)
        {
            if (Session["StudioVersionsToCompare_" + sessionKey] == null)
                return new List<int>();

            return (List<int>)Session["StudioVersionsToCompare_" + sessionKey];
        }

        private List<int> GetApprovalsIdsForVersionsIds(List<int> versionsIds)
        {
            try
            {
                return ApprovalBL.GetApprovalsIdsForVersionsIds(Context, versionsIds);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                return new List<int>();
            }
        }

        private void UpdateInformation(DbContextBL context, int approvalId, int externalOrUserId, bool isExternal, string PSSession)
        {
            try
            {
                if (isExternal)
                {
                    var objExUser = (from ec in context.ExternalCollaborators
                        where ec.ID == externalOrUserId
                        select ec).FirstOrDefault();
                    if (objExUser != null)
                    {
                        objExUser.DateLoggedIn = DateTime.UtcNow;
                        
                        var objExViewInfo = new GMGColorDAL.ApprovalUserViewInfo
                        {
                            Approval = approvalId,
                            ExternalUser = externalOrUserId,
                            ViewedDate = DateTime.UtcNow,
                            Phase = GetApprovalCurrentPhase(approvalId, context)
                        };

                        context.ApprovalUserViewInfoes.Add(objExViewInfo);
                    }
                }
                else
                {
                    if (approvalId > 0)
                    {
                        var user = (from u in context.Users
                                         where u.ID == externalOrUserId
                                         select u).FirstOrDefault();

                        if (user != null)
                        {
                            GMGColorDAL.ApprovalUserViewInfo objViewInfo = new GMGColorDAL.ApprovalUserViewInfo
                            {
                                Approval = approvalId,
                                User = externalOrUserId,
                                ViewedDate = DateTime.UtcNow,
                                Phase = GetApprovalCurrentPhase(approvalId, context)
                            };

                            context.ApprovalUserViewInfoes.Add(objViewInfo);

                            ProofStudioSession objPsSession = new ProofStudioSession()
                            {
                                Guid = PSSession,
                                User = externalOrUserId,
                                CreatedDate = DateTime.UtcNow
                            };

                            context.ProofStudioSessions.Add(objPsSession);
                        }
                    }
                }

                var searchResult = (from acd in context.ApprovalCollaboratorDecisions
                                    join ap in context.Approvals on acd.Approval equals ap.ID
                                    where
                                        ap.ID == approvalId && ap.CurrentPhase == acd.Phase &&
                                        ((isExternal && acd.ExternalCollaborator == externalOrUserId) ||
                                            (!isExternal && acd.Collaborator == externalOrUserId))
                                    select acd).FirstOrDefault();

                var IsApprovalviewed = (from auvi in context.ApprovalUserViewInfoes
                                        where auvi.ExternalUser == externalOrUserId && auvi.Approval == approvalId
                                        select auvi).FirstOrDefault();

                // Updating Expiredate of sharedApproval 24Hrs of Approval Opening Time
                if (isExternal)
                {
                    var objSharedApproval = (from sa in context.SharedApprovals
                                             where sa.Approval == approvalId && sa.ExternalCollaborator == externalOrUserId
                                             select sa).FirstOrDefault();

                    if (!objSharedApproval.IsBlockedURL)
                    {
                        objSharedApproval.IsBlockedURL = true;
                        objSharedApproval.ExpireDate = DateTime.UtcNow.AddDays(1);
                    }
                }

                if (searchResult != null)
                {
                    searchResult.OpenedDate = DateTime.UtcNow;
                }

                context.SaveChanges();
                  
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "StudioController Updating GMGColorDAL.ApprovalUserViewInfo {0}", ex.Message);
            }
        }

        private int? GetApprovalCurrentPhase(int approvalId, DbContextBL context)
        {
            return (from app in context.Approvals
                    where app.ID == approvalId
                    select app.CurrentPhase).FirstOrDefault();
        }

        #endregion
    }
}
