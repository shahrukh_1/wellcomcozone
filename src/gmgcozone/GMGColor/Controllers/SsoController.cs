﻿using System.Security.Claims;
using GMG.CoZone.SSO.Interfaces;
using GMGColor.Common;
using System.Web.Mvc;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColorDAL;


namespace GMGColor.Controllers
{
    [AllowAnonymous]
    public class SsoController : BaseController
    {
        private readonly ISsoCore _ssoCore;

        public SsoController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {
        }

        public SsoController(ISsoCore ssoCore, IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper)
        {
            _ssoCore = ssoCore;
        }

        [HttpPost]
        public ActionResult IdpResponse()
        {
            GMGColorLogging.log.InfoFormat("In SSOController file in IdpResponse method entered");
            GMGColorLogging.log.InfoFormat("In SSOController file SAMLResponse is: " + Request.Form["SAMLResponse"]);

            var samlModel = _ssoCore.SamlPlugin.ParseResponse(Request.Form["SAMLResponse"], LoggedAccount.ID, LoggedAccount.Guid, LoggedAccount.Region);

            GMGColorLogging.log.InfoFormat("In SSOController file in IdpResponse method after ParseResponse");

            if (samlModel != null)
            {
                return RedirectToAction("SamlLogIn", "Auth", samlModel);
            }
            return View("Error");
        }

        [HttpGet]
        public ActionResult Login()
        {
            GMGColorLogging.log.InfoFormat("In SSOController file in Login method entered");

            if (!Request.IsAuthenticated)
            {
                var redirectLocation = _ssoCore.SamlPlugin.GetIdpUrl(LoggedAccount.ID, LoggedAccount.Guid, LoggedAccount.Region);

                GMGColorLogging.log.InfoFormat("In SSOController file in Login method after GetIdpUrl: {0}", redirectLocation);

                if (string.IsNullOrEmpty(redirectLocation))
                {
                    return RedirectToAction("UnexpectedError", "Error");
                }
                Response.Redirect(redirectLocation);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult AuthLogin()
        {
            var email = ClaimsPrincipal.Current.FindFirst("email");
            return new EmptyResult();
        }
    }
}