﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Linq;
using System.Web.Mvc;
using System.Windows.Media.Media3D;
using GMGColor.Common;
using System.Globalization;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;
using AppModule = GMG.CoZone.Common.AppModule;
using System.Text;
using GMG.CoZone.Common.Interfaces;
using AutoMapper;

namespace GMGColor.Controllers
{
    public class AccountController : BaseController
    {
        private const string BillingInfoError = "BillingInfoError";
        private const string SubscriberModel = "SubscriberModel";
        private const string SubscriptionModel = "SubscriptionModel";

        public AccountController(IDownloadService downlaodService, IMapper mapper):base(downlaodService, mapper) { }


        #region Index

        #region GET Actions

        [HttpGet]
        public ActionResult Index(int? page)
        {
            Session[GMGColorConstants.AccountPagination] = (page == null) ? 1 : page.Value;

            ViewBag.SelectedUser = 0;
            Session["SelectedAccId"] = null;
            Session["NextTab"] = null;

            TempData["AccountInfoModel"] = null;
            TempData["AccountPlansModel"] = null;
            TempData["AccountCustomiseModel"] = null;
            TempData["Account"] = null;

            Session["IsFirstCompleted"] = null;
            Session["IsSecondCompleted"] = null;
            Session["IsThirdCompleted"] = null;

            ViewAccounts viewAccountsModel = new ViewAccounts(this.LoggedAccount, this.LoggedUser.ID, Context);
            ViewBag.SearchResults = false.ToString();
            viewAccountsModel.page = page;

            Session[GMG.CoZone.Common.Constants.IsOpenedFromReportAppIndex] = null;

            if (Session["Filters"] != null)
            {
                ViewBag.SearchResults = true.ToString();

                Dictionary<string, List<string>> dicFilters = (Dictionary<string, List<string>>)Session["Filters"];
                //Session["Filters"] = null;

                if (dicFilters.ContainsKey("AccountTypes") && dicFilters["AccountTypes"].Count > 0)
                {
                    viewAccountsModel.SelectedAccountTypes = dicFilters["AccountTypes"];
                    viewAccountsModel.ListAccountsView = viewAccountsModel.ListAccountsView.Where(o => viewAccountsModel.SelectedAccountTypes.Contains(o.AccountTypeID.ToString())).ToList();
                }
                if (dicFilters.ContainsKey("Locations") && dicFilters["Locations"].Count > 0)
                {
                    viewAccountsModel.SelectedLocations = dicFilters["Locations"];
                    viewAccountsModel.ListAccountsView = viewAccountsModel.ListAccountsView.Where(o => viewAccountsModel.SelectedLocations.Contains(o.Country.ToString())).ToList();
                }
                if (dicFilters.ContainsKey("Accounts") && dicFilters["Accounts"].Count > 0)
                {
                    viewAccountsModel.SelectedAccounts = dicFilters["Accounts"];
                    viewAccountsModel.ListAccountsView = viewAccountsModel.ListAccountsView.Where(o => viewAccountsModel.SelectedAccounts.Contains(o.ID.ToString())).ToList();
                }
                if (dicFilters.ContainsKey("AccountStatues") && dicFilters["AccountStatues"].Count > 0)
                {
                    viewAccountsModel.SelectedAccountStatues = dicFilters["AccountStatues"];
                    viewAccountsModel.ListAccountsView = viewAccountsModel.ListAccountsView.Where(o => viewAccountsModel.SelectedAccountStatues.Contains(o.StatusId.ToString())).ToList();
                }
            }

            return View("Index", viewAccountsModel);
        }

        #endregion

        #region POST Actions

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SendInviteToOwner")]
        public ActionResult SendInviteToOwner(ViewAccounts model, string selectedAccountId, int? page)
        {
            try
            {
                GMGColorDAL.Account objAccount =
                    DALUtils.GetObject<GMGColorDAL.Account>(selectedAccountId.ToInt().GetValueOrDefault(), Context);

                objAccount.OwnerUser.Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Invited, Context).ID;
                objAccount.Status = GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Inactive, Context).ID;

                Context.SaveChanges();

                string accountUrl = objAccount.IsCustomDomainActive ? objAccount.CustomDomain : objAccount.Domain;

                this.SendInvite(objAccount.Owner.GetValueOrDefault(), accountUrl);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error occured while sending the invite.", ex);
            }
            return RedirectToAction("Index", new { page = page });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteAccount")]
        public ActionResult DeleteAccount(int selectedAccountId, int? page)
        {
            try
            {
                AccountBL.DeleteAccount(selectedAccountId, LoggedUser, Context);
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error occured while suspending the account", ex);
            }

            return RedirectToAction("Index", new { page = page });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "RestoreAccount")]
        public ActionResult RestoreAccount(string selectedAccountId, int? page)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    AccountBL.RestoreAccount(selectedAccountId.ToInt().GetValueOrDefault(), Context);
                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error occured while suspending the account", ex);
            }

            return RedirectToAction("Index", new { page = page });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "GetLocations")]
        public PartialViewResult GetLocations(ViewAccounts model)
        {
            ViewAccounts modelAccount = new ViewAccounts(this.LoggedAccount, this.LoggedUser.ID, Context);

            modelAccount.Locations.Clear();
            modelAccount.Accounts.Clear();
            modelAccount.AccountStatuses.Clear();

            modelAccount.SelectedAccountTypes = model.SelectedAccountTypes ?? new List<string>();
            if (modelAccount.SelectedAccountTypes.Count > 0)
            {
                Dictionary<int, string> dicLocations = DALUtils.SearchObjects<GMGColorDAL.Company>(o => modelAccount.SelectedAccountTypes.Contains(o.Account1.AccountType.ToString()), Context).GroupBy(o => o.Country).Select(o => o.First()).ToDictionary(o => o.Country, o => o.Country1.ShortName);// .Select(o => o.objCountry).GroupBy(o => o.ID).Select(o => o.First()).ToList();

                foreach (KeyValuePair<int, string> item in dicLocations)
                {
                    if (!modelAccount.Locations.ContainsKey(item.Key))
                        modelAccount.Locations.Add(item.Key, item.Value);
                }

                Dictionary<int, string> dicAccounts = DALUtils.SearchObjects<GMGColorDAL.Account>(o => modelAccount.SelectedAccountTypes.Contains(o.AccountType.ToString()), Context).GroupBy(o => o.ID).Select(o => o.First()).ToDictionary(o => o.ID, o => o.Name);

                foreach (KeyValuePair<int, string> item in dicAccounts)
                {
                    if (!modelAccount.Accounts.ContainsKey(item.Key))
                        modelAccount.Accounts.Add(item.Key, item.Value);
                }

                Dictionary<int, string> dicAccountStatuses = DALUtils.SearchObjects<GMGColorDAL.Account>(o => modelAccount.SelectedAccountTypes.Contains(o.AccountType.ToString()), Context).GroupBy(o => o.Status).Select(o => o.First()).ToDictionary(o => o.Status, o => o.AccountStatu.Name);

                foreach (KeyValuePair<int, string> item in dicAccountStatuses)
                {
                    if (!modelAccount.AccountStatuses.ContainsKey(item.Key))
                        modelAccount.AccountStatuses.Add(item.Key, item.Value);
                }
            }
            ViewBag.ControlName = "Location";
            return PartialView("Controls", modelAccount);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "GetAccounts")]
        public PartialViewResult GetAccounts(ViewAccounts model)
        {
            ViewAccounts modelAccount = new ViewAccounts(this.LoggedAccount, this.LoggedUser.ID, Context);

            modelAccount.SelectedAccountTypes = model.SelectedAccountTypes ?? new List<string>();
            modelAccount.SelectedLocations = model.SelectedLocations ?? new List<string>();

            modelAccount.Accounts.Clear();

            if (modelAccount.SelectedAccountTypes.Count > 0)
            {
                Dictionary<int, string> dicAccounts = DALUtils.SearchObjects<GMGColorDAL.Account>(o => o.ID != this.LoggedAccount.ID && modelAccount.SelectedAccountTypes.Contains(o.AccountType.ToString()), Context).GroupBy(o => o.ID).Select(o => o.First()).ToDictionary(o => o.ID, o => o.Name);

                foreach (KeyValuePair<int, string> item in dicAccounts)
                {
                    if (!modelAccount.Accounts.ContainsKey(item.Key))
                        modelAccount.Accounts.Add(item.Key, item.Value);
                }
            }
            if (modelAccount.SelectedLocations.Count > 0)
            {
                Dictionary<int, string> dicAccounts = DALUtils.SearchObjects<GMGColorDAL.Account>(o => o.ID != this.LoggedAccount.ID && modelAccount.SelectedLocations.Contains(o.Companies.FirstOrDefault().Country.ToString()), Context).GroupBy(o => o.ID).Select(o => o.First()).ToDictionary(o => o.ID, o => o.Name);

                foreach (KeyValuePair<int, string> item in dicAccounts)
                {
                    if (!modelAccount.Accounts.ContainsKey(item.Key))
                        modelAccount.Accounts.Add(item.Key, item.Value);
                }
            }

            ViewBag.ControlName = "Accounts";
            return PartialView("Controls", modelAccount);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "GetStatues")]
        public PartialViewResult GetStatues(ViewAccounts model)
        {
            ViewAccounts modelAccount = new ViewAccounts(this.LoggedAccount, this.LoggedUser.ID, Context);

            modelAccount.SelectedAccountTypes = model.SelectedAccountTypes ?? new List<string>();
            modelAccount.SelectedLocations = model.SelectedLocations ?? new List<string>();
            modelAccount.SelectedAccounts = model.SelectedAccounts ?? new List<string>();

            if (modelAccount.SelectedAccounts.Count > 0)
            {
                modelAccount.AccountStatuses.Clear();
                Dictionary<int, string> dicAccountStatuses = DALUtils.SearchObjects<GMGColorDAL.Account>(o => modelAccount.SelectedAccounts.Contains(o.ID.ToString()), Context).GroupBy(o => o.Status).First().ToDictionary(o => o.Status, o => o.AccountStatu.Name);//.Select(o => o.objStatus).GroupBy(o => o.ID).Select(o => o.First()).ToList();

                foreach (KeyValuePair<int, string> item in dicAccountStatuses)
                {
                    if (!modelAccount.AccountStatuses.ContainsKey(item.Key))
                        modelAccount.AccountStatuses.Add(item.Key, item.Value);
                }
            }
            ViewBag.ControlName = "Status";
            return PartialView("Controls", modelAccount);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SearchAccounts")]
        public ActionResult Search(ViewAccounts model)
        {
            Dictionary<string, List<string>> dicFilters = new Dictionary<string, List<string>>();

            if (model.SelectedAccountTypes != null && model.SelectedAccountTypes.Count > 0)
            {
                dicFilters.Add("AccountTypes", model.SelectedAccountTypes);
            }
            if (model.SelectedLocations != null && model.SelectedLocations.Count > 0)
            {
                dicFilters.Add("Locations", model.SelectedLocations);
            }
            if (model.SelectedAccounts != null && model.SelectedAccounts.Count > 0)
            {
                dicFilters.Add("Accounts", model.SelectedAccounts);
            }
            if (model.SelectedAccountStatues != null && model.SelectedAccountStatues.Count > 0)
            {
                dicFilters.Add("AccountStatues", model.SelectedAccountStatues);
            }

            Session["Filters"] = dicFilters;
            return RedirectToAction("Index");
        }

        #endregion

        #endregion

        #region New Account

        #region GET Actions

        [HttpGet]
        public ActionResult NewAccount()
        {
            object model;// = new AccountInfo(this.LoggedAccount.ID);

            if (TempData["AccountInfoModel"] != null)
            {
                model = (AccountInfo)TempData["AccountInfoModel"];
                TempData["AccountInfoModel"] = null;
                Session["NextTab"] = 1;
            }
            else if (TempData["AccountPlansModel"] != null)
            {
                model = (AccountPlans)TempData["AccountPlansModel"];
                TempData["AccountPlansModel"] = null;
                (model as AccountPlans).LoggedAccount = this.LoggedAccount.ID;
            }
            else if (TempData["AccountCustomiseModel"] != null)
            {
                model = (AccountCustomise)TempData["AccountCustomiseModel"];
                TempData["AccountCustomiseModel"] = null;
            }
            else if (TempData["Account"] != null)
            {
                model = Convert.ToInt32(TempData["Account"].ToString());
                TempData["Account"] = null;
            }
            else
            {
                model = new AccountInfo(this.LoggedAccount.ID, Context);
                Session["NextTab"] = 1;
            }

            if (TempData["ModelErrors"] != null)
            {
                Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                TempData["ModelErrors"] = null;

                foreach (KeyValuePair<string, string> item in dicModelErrors)
                {
                    ModelState.AddModelError(item.Key, item.Value);
                }
            }

            switch (Session["NextTab"].ToString())
            {
                case "1":
                    ViewBag.Regions = Amazon.RegionEndpoint.EnumerableAllRegions.Where(o => o.SystemName == "ap-southeast-1").ToList();
                    return View("NewAccount-AccountInfo", model);
                case "2":
                    {
                        var accountPlansModel = model as AccountPlans;
                        if (accountPlansModel != null)
                        {
                            accountPlansModel.PlanPerModule.AccountId = accountPlansModel.Account;
                            accountPlansModel.SubscriberPlans.AccountId = accountPlansModel.Account;
                            accountPlansModel.PlanPerModule = AccountBL.UpdateDropDownsDataSourceForSubscriptionPlanPerModule(accountPlansModel.PlanPerModule, ModelState.IsValid, Context);
                            accountPlansModel.SubscriberPlans = AccountBL.UpdateDropDownsDataSourceForSubscriberPlanPerModule(accountPlansModel.SubscriberPlans, ModelState.IsValid, LoggedAccount, Context);
                            return View("NewAccount-Plans", accountPlansModel);
                        }
                        return View("NewAccount-Plans", model);
                    }
                case "3":
                    return View("NewAccount-Customize", model);
                case "4":
                    return View("NewAccount-Confirmation", model);
                default:
                    return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult AccountInfo(string id)
        {
            if (!string.IsNullOrEmpty(id) && int.Parse(id) > 0)
            {
                AccountInfo accountInfoModel = new AccountInfo(this.LoggedAccount.ID, int.Parse(id), Context);
                TempData["AccountInfoModel"] = accountInfoModel;
            }

            Session["NextTab"] = 1;
            return RedirectToAction("NewAccount");
        }

        [HttpGet]
        public ActionResult AccountPlans(string id)
        {
            if (!string.IsNullOrEmpty(id) && int.Parse(id) > 0)
            {
                AccountPlans accountPlansModel = new AccountPlans(this.LoggedAccount, int.Parse(id), Context, true);
                TempData["AccountPlansModel"] = accountPlansModel;
            }
            Session["NextTab"] = 2;
            return RedirectToAction("NewAccount");
        }

        [HttpGet]
        public ActionResult AccountCustomize(string id)
        {
            if (!string.IsNullOrEmpty(id) && int.Parse(id) > 0)
            {
                AccountCustomise accountCustomiseModel = new AccountCustomise(this.LoggedUserTempFolderPath, int.Parse(id), Context);
                TempData["AccountCustomiseModel"] = accountCustomiseModel;
            }

            Session["NextTab"] = 3;
            return RedirectToAction("NewAccount");
        }

        [HttpGet]
        public ActionResult AccountConfirm(string id)
        {
            if (!string.IsNullOrEmpty(id) && int.Parse(id) > 0)
            {
                TempData["Account"] = id;
            }
            Session["NextTab"] = 4;
            return RedirectToAction("NewAccount");
        }

        //[OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        //public JsonResult IsNrOfCreditsAvailable(int NrOfCredits, int SelectedBillingPlan)
        //{
        //    // TODO - use it for remote plan n umber of units validation
        //    int sum = 0;
        //    foreach (GMGColorDAL.Account accountVar in this.LoggedAccount.GetChilds())
        //    {
        //        if (GMGColorDAL.AccountType.GetAccountType(accountVar) == GMGColorDAL.AccountType.Type.Subscriber)
        //        {
        //            if (accountVar.SubscriberPlanInfo.HasValue && accountVar.SubscriberPlanInfo > 0 && accountVar.SelectedBillingPlan == SelectedBillingPlan)
        //            {
        //                sum += accountVar.objSubscriberPlanInfo.NrOfCredits;
        //            }
        //        }
        //    }

        //    int TotalCredits = (new GMGColorDAL.BillingPlan()).SearchObjects().Single(o => o.ID == SelectedBillingPlan).Proofs.Value;
        //    if ( sum + NrOfCredits <= TotalCredits)
        //        return Json(true, JsonRequestBehavior.AllowGet);

        //    string suggestedUID = String.Format(CultureInfo.InvariantCulture,
        //        Resources.Resources.lblNrOfCreditsExceeded, TotalCredits - sum);

        //    return Json(suggestedUID, JsonRequestBehavior.AllowGet);
        //}


        #endregion

        #region POST Actions

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AccountInfo")]
        public ActionResult AccountInfo(AccountInfo model, string isDomanSucess)
        {
            // This action is authorized as Login action due to MultiButton acction selector and must be checked for logged user first
            if (LoggedUser == null)
            {
                return RedirectToAction("Login", "Auth");
            }

            #region Custom Validation

            if (model.Type > 0 && !GMGColorDAL.Account.IsAccountAccessibleAccountType(this.LoggedAccount.ID, model.Type, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            bool hasError = false;
            Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();

            /*if (this.ValidateDomain(model.ID, model.Domain) != "success")
            {

            }*/

            if (model.ID == 0)
            {
                //no needs to check here.domain validation handles in BaseController.ValidateDomain() method
                /* if (DALUtils.SearchObjects<GMGColorDAL.Account>(o => o.Domain.Replace(GMGColorConfiguration.AppConfiguration.HostAddress, string.Empty) == model.Domain).ToList().Count > 0)
                {
                    hasError = true;
                    dicModelErrors.Add("Domain", Resources.Resources.reqDomainExist);
                }*/
                /* bug 386 if (!this.ValidateEmail(this.LoggedAccount, model.ContactEmailAddress))
                {
                    hasError = true;
                    dicModelErrors.Add("ContactEmailAddress", Resources.Resources.reqEmailExist);
                }*/
                if (!string.IsNullOrEmpty(isDomanSucess) && isDomanSucess == "0")
                {
                    hasError = true;
                    //dicModelErrors.Add("Domain", );
                }
            }

            if (!ValidateUserName(model.ObjOwner.Username, String.Empty))
            {
                hasError = true;
                dicModelErrors.Add("ObjOwner.Username", Resources.Resources.lblUsernameTaken);
            }

            if (String.IsNullOrEmpty(model.Domain))
            {
                hasError = true;
                dicModelErrors.Add("Domain", Resources.Resources.reqDomain);
            }

            if (!string.IsNullOrEmpty(model.CustomDomain))
            {
                Regex rgxDomain = new Regex(GMG.CoZone.Common.Constants.CustomDomainRegex);
                if (!rgxDomain.IsMatch(model.CustomDomain.Trim())) 
                {
                    hasError = true;
                    dicModelErrors.Add("CustomDomain", Resources.Resources.reqInvalidDomain);
                }
            }

            if (!hasError)
            {
                string domainNameWithoutHostAddress = model.Domain.Trim();
                int lastIndexOfHostAddress = model.Domain.Trim().LastIndexOf(GMGColorConfiguration.AppConfiguration.HostAddress);
                if (lastIndexOfHostAddress > -1)
                {
                    domainNameWithoutHostAddress = model.Domain.Trim().Substring(0, lastIndexOfHostAddress);
                }

                Regex regeX = new Regex(GMG.CoZone.Common.Constants.SubDomainNameRegex);
                if (!regeX.IsMatch(domainNameWithoutHostAddress))
                {
                    hasError = true;
                    dicModelErrors.Add("Domain", Resources.Resources.reqInvalidDomain);
                }
                if (domainNameWithoutHostAddress.Length > 255)
                {
                    hasError = true;
                    dicModelErrors.Add("Domain", Resources.Resources.errTooManyCharactors);
                }
            }

            if (hasError)
            {
                Session["NextTab"] = 1;
                TempData["AccountInfoModel"] = model;
                TempData["ModelErrors"] = dicModelErrors;
                return RedirectToAction("AccountInfo", "Account");
            }

            #endregion

            GMGColorDAL.Account objAccount = new GMGColorDAL.Account();
            GMGColorDAL.Company objCompany = new GMGColorDAL.Company();
            GMGColorDAL.User objUser = new GMGColorDAL.User();
          
            if (model.ID > 0 && !CanAccess(model.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            if (model.ObjCompany != null && model.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.Company>(model.ObjCompany.ID, model.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            if (model.ObjOwner != null && model.ID > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(model.ObjOwner.ID, model.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            if (model.ID > 0)
            {
                objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.ID, Context);
            }
            if (model.ObjCompany != null && model.ObjCompany.ID > 0)
            {
                objCompany = DALUtils.GetObject<GMGColorDAL.Company>(model.ObjCompany.ID, Context);
            }
            if (model.ObjOwner != null && model.ObjOwner.ID > 0)
            {
                objUser = DALUtils.GetObject<GMGColorDAL.User>(model.ObjOwner.ID, Context);
            }

            try
            {
                using (TransactionScope tscope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                {
                    #region Account

                    objAccount.Name = model.Name;
                    objAccount.AccountType = model.Type;
                    objAccount.Parent = this.LoggedAccount.ID;
                    objAccount.ContactFirstName = model.ContactFirstName;
                    objAccount.ContactLastName = model.ContactLastName;
                    objAccount.ContactPhone = model.ContactPhone;
                    objAccount.ContactEmailAddress = model.ContactEmailAddress;
                    objAccount.Locale = model.DefaultLanguage;
                    objAccount.TimeZone = model.TimeZone;
                    objAccount.DateFormat = model.DateFormat;
                    objAccount.TimeFormat = model.TimeFormat;
                    objAccount.Domain = (GMGColorConfiguration.AppConfiguration.Environment == "prod") ? model.Domain.ToLower().Trim() + GMGColorConfiguration.AppConfiguration.HostAddress : model.Domain.ToLower().Trim();
                    objAccount.IsCustomDomainActive = false;
                    objAccount.CustomDomain = null;
                    objAccount.ContractPeriod = 1;
                    objAccount.Status = GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Draft, Context).ID;
                    objAccount.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    objAccount.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    objAccount.DealerNumber = model.DealerNumber;
                    objAccount.CustomerNumber = model.CustomerNumber;
                    objAccount.VATNumber = model.VATNumber;
                    objAccount.Creator = this.LoggedUser.ID;
                    objAccount.Modifier = this.LoggedUser.ID;
                    if (!String.IsNullOrEmpty(model.CustomDomain))
                    {
                        objAccount.CustomDomain = model.CustomDomain.Trim();
                        objAccount.IsCustomDomainActive = model.IsCustomDomainActive &&
                                                          GMGColorCommon.PingDomain(objAccount.CustomDomain, objAccount.ID);
                    }

                    if (this.LoggedAccount.ID == GMGColorDAL.AccountType.GetAccountType(GMGColorDAL.AccountType.Type.GMGColor, Context).ID)
                    {
                        objAccount.Region = model.DataStorageLocation;
                        objAccount.CurrencyFormat = model.CurrencyFormat;
                    }
                    else
                    {
                        objAccount.Region = this.LoggedAccount.Region;
                        objAccount.CurrencyFormat = this.LoggedAccount.CurrencyFormat;
                    }

                    if (objAccount.ID == 0)
                    {
                        objAccount.Guid = Guid.NewGuid().ToString();
                        objAccount.SiteName = model.Name;
                        //objAccount.AccountThemes = GMGColorDAL.Theme.GetColorScheme(GMGColorDAL.Theme.ColorScheme.Default, Context).ID;
                    }

                    GMGColorDAL.AccountHelpCentre objAccHelpCentre = new GMGColorDAL.AccountHelpCentre();
                    objAccHelpCentre.ContactEmail = model.ContactEmailAddress;
                    objAccount.AccountHelpCentres.Add(objAccHelpCentre);

                    if (objAccount.ID == 0)
                    {
                        Context.Accounts.Add(objAccount);
                    }

                    #endregion

                    #region Company

                    objCompany.Name = model.ObjCompany.Name;
                    objCompany.Phone = model.ContactPhone;
                    objCompany.Mobile = model.ObjCompany.Mobile;
                    objCompany.Email = model.ContactEmailAddress;

                    objCompany.Address1 = model.ObjCompany.Address1;
                    objCompany.Address2 = model.ObjCompany.Address2;
                    objCompany.State = model.ObjCompany.State;
                    objCompany.Postcode = model.ObjCompany.PostCode;
                    objCompany.City = model.ObjCompany.City;
                    objCompany.Country = model.ObjCompany.Country;
                    objCompany.Guid = Guid.NewGuid().ToString();

                    if (model.ObjCompany.ID == 0)
                    {
                        objAccount.Companies.Add(objCompany);

                        Context.Companies.Add(objCompany);
                    }

                    #endregion

                    #region User

                    objUser.Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Invited, Context).ID;
                    objUser.Username = model.ObjOwner.Username;
                    objUser.Password = GMGColorDAL.User.GetNewEncryptedRandomPassword(Context);
                    objUser.GivenName = model.ContactFirstName;
                    objUser.FamilyName = model.ContactLastName;
                    objUser.EmailAddress = model.ContactEmailAddress;
                    objUser.PhotoPath = model.ObjOwner.PhotoPath;
                    objUser.Guid = Guid.NewGuid().ToString();
                    objUser.MobileTelephoneNumber = model.ObjOwner.MobileTelephoneNumber;
                    objUser.HomeTelephoneNumber = model.ObjOwner.HomeTelephoneNumber;
                    objUser.OfficeTelephoneNumber = model.ContactPhone;
                    objUser.NotificationFrequency = GMGColorDAL.NotificationFrequency.GetNotificationFrequency(GMGColorDAL.NotificationFrequency.Frequency.Never, Context).ID;
                    objUser.Preset = GMGColorDAL.Preset.GetPreset(GMGColorDAL.Preset.PresetType.Low, Context).ID;
                    objUser.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    objUser.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    //objUser.IsActive = false;
                    //objUser.IsDeleted = false;
                    objUser.Creator = this.LoggedUser.ID;
                    objUser.Modifier = this.LoggedUser.ID;
                    objUser.ProofStudioColor = UserBL.GetAvailableUserColor(null, Context).ID;
                    objUser.Locale = objAccount.Locale;

                    if (model.ObjOwner.ID == 0)
                    {
                        objAccount.Users.Add(objUser);

                        Context.Users.Add(objUser);
                    }

                    if (objAccount.ID == 0)
                    {
                        //TODO a role should be added for each module that exists into database!!!!

                        List<int> modules =
                            DALUtils.SearchObjects<GMGColorDAL.AppModule>(
                                t => t.Key != (int)GMG.CoZone.Common.AppModule.SysAdmin, Context).Select(t => t.ID).ToList();

                        foreach (int module in modules)
                        {
                            //TODO: this should be changed to a enum!!
                            GMGColorDAL.Role role =
                                DALUtils.SearchObjects<GMGColorDAL.Role>(o => o.Key == "ADM" && o.AppModule == module, Context).FirstOrDefault();
                            if (role != null)
                            {
                                GMGColorDAL.UserRole objUserRole = new GMGColorDAL.UserRole();
                                objUserRole.Role = role.ID;
                                objUser.UserRoles.Add(objUserRole);
                                Context.UserRoles.Add(objUserRole);
                            }
                        }
                    }

                    #endregion

                    Context.SaveChanges();

                    if (model.ID == 0)
                    {
                        objAccount.Owner = objUser.ID;
                       
                        Context.SaveChanges();
                    }
                    tscope.Complete();
                }

                if (model.ID == 0)
                {
                    objAccount.IsTemporary = true;
                    objUser.Accounts.Add(objAccount);
                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "AccountController.DefineGeneralInformation : Saving Account Details Failed. {0}", ex.Message);
            }

            AccountPlans accountPlansModel = (objAccount.AttachedAccountBillingPlans.Count == 0)
                                             ? new AccountPlans(this.LoggedAccount, objAccount.ID, Context)
                                             : new AccountPlans(this.LoggedAccount, objAccount.ID, Context, true);

            TempData["AccountPlansModel"] = accountPlansModel;
            Session["IsFirstCompleted"] = true;

            return RedirectToAction("AccountPlans", "Account");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Plans")]
        public ActionResult Plans(AccountPlans model)
        {
            try
            {
                if (!CanAccess(model.Account, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                List<int> lstBillingInfoIds = new List<int>();
                GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(this.LoggedAccount, Context).BillingPlans.ForEach(o => o.BillingPlans.ForEach(x => lstBillingInfoIds.Add(x.ID)));

                if (
                   (model.PlanPerModule.CollaborateBillingPlanId.HasValue && !lstBillingInfoIds.Contains(model.PlanPerModule.CollaborateBillingPlanId.GetValueOrDefault())) ||                 
                   (model.PlanPerModule.DeliverBillingPlanId.HasValue && !lstBillingInfoIds.Contains(model.PlanPerModule.DeliverBillingPlanId.GetValueOrDefault())))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                bool hasError = false;
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();

                if (GMGColorDAL.AccountType.GetAccountType(model.Account, Context) != GMGColorDAL.AccountType.Type.Subscriber)
                {
                    if (model.AttachedBillingPlans == null)
                    {
                        hasError = true;
                        dicModelErrors.Add("AttachedBillingPlans", Resources.Resources.reqPlanShouldBeSelected);
                    }
                    else if (model.AttachedBillingPlans.All(o => o == "false"))
                    {
                        hasError = true;
                        dicModelErrors.Add("AttachedBillingPlans", Resources.Resources.reqPlanShouldBeSelected);
                    }

                    if (model.NeedSubscriptionPlan &&
                        !model.PlanPerModule.CollaborateBillingPlanId.HasValue && !model.PlanPerModule.DeliverBillingPlanId.HasValue)
                    {
                        hasError = true;
                        dicModelErrors.Add("PlanPerModule.SelectedPlans", Resources.Resources.lblAtLeastOneSubscriptionPlanShouldBeChoosen);
                    }

                    if (hasError)
                    {
                        AccountPlans accountPlansModelTemp = new AccountPlans(this.LoggedAccount, model.Account, Context, true);
                        model.objAccountBillingPlans = accountPlansModelTemp.objAccountBillingPlans;

                        Session["NextTab"] = 2;
                        TempData["AccountPlansModel"] = model;
                        TempData["ModelErrors"] = dicModelErrors;
                        return RedirectToAction("AccountPlans", "Account");
                    }
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.Account, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    bool hasAccessToFileTransfer = model.HasFileTransferAccess;
                    objAccount.IsFileTransferEnabled = hasAccessToFileTransfer;

                    bool isSubscriber = (GMGColorDAL.AccountType.GetAccountType(model.Account, Context) == GMGColorDAL.AccountType.Type.Subscriber);

                    objAccount.NeedSubscriptionPlan = false;

                    if (model.NeedSubscriptionPlan && !isSubscriber)
                    {
                        objAccount.NeedSubscriptionPlan = true;

                        #region Plan for Collaborate

                        {
                            GMGColorDAL.AccountSubscriptionPlan collaboratePlan = GMGColorDAL.Account.GetNonSubscriberPlan(objAccount.ID, AppModule.Collaborate, Context);
                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.PlanPerModule.CollaborateBillingPlanId.HasValue && collaboratePlan != null)
                            {
                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.AccountSubscriptionPlan>(Context, collaboratePlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.PlanPerModule.CollaborateBillingPlanId.HasValue)
                                {
                                    GMGColorDAL.BillingPlan billingPlan =
                                        DALUtils.GetObject<GMGColorDAL.BillingPlan>(
                                            model.PlanPerModule.CollaborateBillingPlanId.Value, Context);

                                    if (collaboratePlan == null)
                                        collaboratePlan = new GMGColorDAL.AccountSubscriptionPlan();

                                    collaboratePlan.BillingPlan = billingPlan;
                                    collaboratePlan.IsMonthlyBillingFrequency =
                                        (model.PlanPerModule.CollaborateIsMonthlyBillingFrequency ==
                                         Enums.BillingFrequency.Monthly);
                                    collaboratePlan.ChargeCostPerProofIfExceeded =
                                        model.PlanPerModule.CollaborateChargeCostPerProofIfExceeded;
                                    collaboratePlan.Account1 = objAccount;
                                    if (!collaboratePlan.ContractStartDate.HasValue)
                                        collaboratePlan.ContractStartDate =
                                            (model.PlanPerModule.CollaborateSelectedContractStartDate == ContractStartDateEnum.Today.ToString() && !collaboratePlan.ContractStartDate.HasValue)
                                                ? (DateTime?)DateTime.Now
                                                : null;
                                    if (collaboratePlan.ID == 0)
                                        objAccount.AccountSubscriptionPlans.Add(collaboratePlan);

                                    if (!collaboratePlan.AccountPlanActivationDate.HasValue)
                                    {
                                        collaboratePlan.AccountPlanActivationDate = (model.PlanPerModule.CollaborateSelectedContractStartDate == ContractStartDateEnum.Today.ToString())
                                                ? (DateTime?)DateTime.UtcNow
                                                : null;
                                    }

                                    collaboratePlan.AllowOverdraw = model.PlanPerModule.CollaborateAllowOverdraw;
                                }
                            }
                        }

                        #endregion

                        #region Plan for Deliver

                        {
                            GMGColorDAL.AccountSubscriptionPlan deliverPlan = GMGColorDAL.Account.GetNonSubscriberPlan(objAccount.ID, AppModule.Deliver, Context);
                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.PlanPerModule.DeliverBillingPlanId.HasValue && deliverPlan != null)
                            {
                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.AccountSubscriptionPlan>(Context, deliverPlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.PlanPerModule.DeliverBillingPlanId.HasValue)
                                {
                                    GMGColorDAL.BillingPlan billingPlan =
                                        DALUtils.GetObject<GMGColorDAL.BillingPlan>(
                                            model.PlanPerModule.DeliverBillingPlanId.Value, Context);

                                    if (deliverPlan == null)
                                        deliverPlan = new GMGColorDAL.AccountSubscriptionPlan();

                                    deliverPlan.BillingPlan = billingPlan;
                                    deliverPlan.IsMonthlyBillingFrequency =
                                        (model.PlanPerModule.DeliverIsMonthlyBillingFrequency ==
                                         Enums.BillingFrequency.Monthly);
                                    deliverPlan.ChargeCostPerProofIfExceeded =
                                        model.PlanPerModule.DeliverChargeCostPerProofIfExceeded;
                                    deliverPlan.Account1 = objAccount;
                                    if (!deliverPlan.ContractStartDate.HasValue)
                                        deliverPlan.ContractStartDate =
                                            (model.PlanPerModule.DeliverSelectedContractStartDate == ContractStartDateEnum.Today.ToString() &&
                                             !deliverPlan.ContractStartDate.HasValue)
                                                ? (DateTime?)DateTime.Now
                                                : null;
                                    if (deliverPlan.ID == 0)
                                        objAccount.AccountSubscriptionPlans.Add(deliverPlan);


                                    if (!deliverPlan.AccountPlanActivationDate.HasValue)
                                    {
                                        deliverPlan.AccountPlanActivationDate = (model.PlanPerModule.DeliverSelectedContractStartDate == ContractStartDateEnum.Today.ToString() && !deliverPlan.AccountPlanActivationDate.HasValue)
                                                ? (DateTime?)DateTime.UtcNow
                                                : null;
                                    }

                                    deliverPlan.AllowOverdraw = model.PlanPerModule.DeliverAllowOverdraw;
                                }
                            }
                        }

                        #endregion                       
                    }
                    else
                    {
                        if (!isSubscriber)
                        {
                            GMGColorDAL.Account.DeleteAccountSubscriptionPlans(objAccount.ID, Context);
                        }
                    }

                    if (isSubscriber)
                    {
                        // the following validations will apply only if the logged account is a client
                        if (LoggedAccountType == GMGColorDAL.AccountType.Type.Client)
                        {
                            bool anyError = false;

                            if (model.SubscriberPlans.CollaborateCreditAllocationType == CreditAllocationTypeEnum.Quota &&
                                model.SubscriberPlans.CollaborateBillingPlanId.HasValue)
                            {
                                int creditsLeft = SubscriberGetNumberOfRemainingCredits(model.SubscriberPlans.CollaborateBillingPlanId.Value, model.Account, AppModule.Collaborate);
                                if (creditsLeft < model.SubscriberPlans.CollaborateNrOfCredits && !model.SubscriberPlans.SelectedCollaboratePlanIsFixed)
                                {
                                    dicModelErrors.Add("SubscriberPlans.CollaborateNrOfCredits", String.Format(CultureInfo.InvariantCulture, Resources.Resources.lblNrOfCreditsExceeded, creditsLeft));

                                    AccountPlans accountPlansModelTemp = new AccountPlans(this.LoggedAccount, model.Account, Context, true);
                                    model.objAccountBillingPlans = accountPlansModelTemp.objAccountBillingPlans;
                                    anyError = true;
                                }
                            }
                            
                            if (model.SubscriberPlans.DeliverCreditAllocationType == CreditAllocationTypeEnum.Quota &&
                                model.SubscriberPlans.DeliverBillingPlanId.HasValue && !model.SubscriberPlans.SelectedDeliverPlanIsFixed)
                            {
                                int creditsLeft = SubscriberGetNumberOfRemainingCredits(model.SubscriberPlans.DeliverBillingPlanId.Value, model.Account, AppModule.Deliver);
                                if (creditsLeft < model.SubscriberPlans.DeliverNrOfCredits)
                                {
                                    dicModelErrors.Add("SubscriberPlans.DeliverNrOfCredits", String.Format(CultureInfo.InvariantCulture, Resources.Resources.lblNrOfCreditsExceeded, creditsLeft));
                                    AccountPlans accountPlansModelTemp = new AccountPlans(this.LoggedAccount, model.Account, Context, true);
                                    model.objAccountBillingPlans = accountPlansModelTemp.objAccountBillingPlans;
                                    anyError = true;
                                }
                            }
                           
                            if (anyError)
                            {
                                Session["NextTab"] = 2;
                                TempData["AccountPlansModel"] = model;
                                TempData["ModelErrors"] = dicModelErrors;
                                return RedirectToAction("AccountPlans", "Account");
                            }
                        }

                        objAccount.NeedSubscriptionPlan = true;

                        #region Plan for Collaborate

                        {
                            GMGColorDAL.SubscriberPlanInfo collaboratePlan = GMGColorDAL.Account.GetSubscriberPlan(objAccount.ID, AppModule.Collaborate, Context);
                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.SubscriberPlans.CollaborateBillingPlanId.HasValue && collaboratePlan != null)
                            {
                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.SubscriberPlanInfo>(Context, collaboratePlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.SubscriberPlans.CollaborateBillingPlanId.HasValue)
                                {
                                    GMGColorDAL.BillingPlan billingPlan =
                                        DALUtils.GetObject<GMGColorDAL.BillingPlan>(
                                            model.SubscriberPlans.CollaborateBillingPlanId.Value, Context);

                                    if (collaboratePlan == null)
                                        collaboratePlan = new GMGColorDAL.SubscriberPlanInfo();

                                    collaboratePlan.BillingPlan = billingPlan;
                                    collaboratePlan.ChargeCostPerProofIfExceeded =
                                        model.SubscriberPlans.CollaborateChargeCostPerProofIfExceeded;
                                    collaboratePlan.Account1 = objAccount;
                                    collaboratePlan.IsQuotaAllocationEnabled =
                                        model.SubscriberPlans.CollaborateCreditAllocationType ==
                                        CreditAllocationTypeEnum.Quota;
                                    collaboratePlan.NrOfCredits =
                                        model.SubscriberPlans.CollaborateCreditAllocationType ==
                                        CreditAllocationTypeEnum.Quota
                                            ? model.SubscriberPlans.CollaborateNrOfCredits
                                            : 0;
                                    if (!collaboratePlan.ContractStartDate.HasValue)
                                        collaboratePlan.ContractStartDate = (model.SubscriberPlans.CollaborateSelectedContractStartDate == ContractStartDateEnum.Today.ToString())
                                                ? (DateTime?)DateTime.Now
                                                : null;
                                    if (collaboratePlan.ID == 0)
                                        objAccount.SubscriberPlanInfoes.Add(collaboratePlan);

                                    if (!collaboratePlan.AccountPlanActivationDate.HasValue)
                                    {
                                        collaboratePlan.AccountPlanActivationDate = (model.SubscriberPlans.CollaborateSelectedContractStartDate == ContractStartDateEnum.Today.ToString() && !collaboratePlan.AccountPlanActivationDate.HasValue)
                                                ? (DateTime?)DateTime.UtcNow
                                                : null;
                                    }

                                    collaboratePlan.AllowOverdraw = model.SubscriberPlans.CollaborateAllowOverdraw;
                                }
                            }
                        }

                        #endregion

                        #region Plan for Deliver

                        {
                            GMGColorDAL.SubscriberPlanInfo deliverPlan = GMGColorDAL.Account.GetSubscriberPlan(objAccount.ID, AppModule.Deliver, Context);

                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.SubscriberPlans.DeliverBillingPlanId.HasValue && deliverPlan != null)
                            {
                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.SubscriberPlanInfo>(Context, deliverPlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.SubscriberPlans.DeliverBillingPlanId.HasValue)
                                {
                                    GMGColorDAL.BillingPlan billingPlan =
                                        DALUtils.GetObject<GMGColorDAL.BillingPlan>(
                                            model.SubscriberPlans.DeliverBillingPlanId.Value, Context);

                                    if (deliverPlan == null)
                                        deliverPlan = new GMGColorDAL.SubscriberPlanInfo();

                                    deliverPlan.BillingPlan = billingPlan;
                                    deliverPlan.ChargeCostPerProofIfExceeded =
                                        model.SubscriberPlans.DeliverChargeCostPerProofIfExceeded;
                                    deliverPlan.Account1 = objAccount;
                                    deliverPlan.IsQuotaAllocationEnabled =
                                        model.SubscriberPlans.DeliverCreditAllocationType ==
                                        CreditAllocationTypeEnum.Quota;
                                    deliverPlan.NrOfCredits = model.SubscriberPlans.DeliverCreditAllocationType ==
                                                              CreditAllocationTypeEnum.Quota
                                                                  ? model.SubscriberPlans.DeliverNrOfCredits
                                                                  : 0;
                                    if (!deliverPlan.ContractStartDate.HasValue)
                                        deliverPlan.ContractStartDate =
                                            (model.SubscriberPlans.DeliverSelectedContractStartDate == ContractStartDateEnum.Today.ToString())
                                                ? (DateTime?)DateTime.Now
                                                : null;
                                    if (deliverPlan.ID == 0)
                                        objAccount.SubscriberPlanInfoes.Add(deliverPlan);

                                    if (!deliverPlan.AccountPlanActivationDate.HasValue)
                                    {
                                        deliverPlan.AccountPlanActivationDate = (model.SubscriberPlans.DeliverSelectedContractStartDate == ContractStartDateEnum.Today.ToString() && !deliverPlan.AccountPlanActivationDate.HasValue)
                                                ? (DateTime?)DateTime.UtcNow
                                                : null;
                                    }

                                    deliverPlan.AllowOverdraw = model.SubscriberPlans.DeliverAllowOverdraw;
                                }
                            }
                        }

                        #endregion                      
                    }

                    objAccount.GPPDiscount = (model.GPPDiscount != null) ? (model.GPPDiscount) : 0;

                    if (model.AttachedBillingPlans != null)
                    {
                        List<int> selectedBillingPlanIds = model.AttachedBillingPlans.Where(o => o != "false").Select(o => int.Parse(o)).ToList();
                        List<int> attachedBillingPlanIds = objAccount.AttachedAccountBillingPlans.Select(o => o.BillingPlan).ToList();

                        if (selectedBillingPlanIds.Count > 0)
                        {
                            decimal rate = 1;
                            if (this.LoggedAccount.CurrencyFormat != objAccount.CurrencyFormat)
                            {
                                rate = (new GMGColorCommon()).GetUpdatedCurrencyRate(objAccount, Context);
                            }

                            foreach (int selectedId in selectedBillingPlanIds)
                            {
                                if (attachedBillingPlanIds.Contains(selectedId))
                                {
                                    attachedBillingPlanIds.Remove(selectedId);
                                }
                                else
                                {
                                    GMGColorDAL.BillingPlan objBillingPlan = DALUtils.GetObject<GMGColorDAL.BillingPlan>(selectedId, Context);

                                    decimal price = GMGColorDAL.AttachedAccountBillingPlan.GetPlanPrice(this.LoggedAccount.ID, selectedId, Context, rate);
                                    decimal proofPrice = GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(this.LoggedAccount.ID, selectedId, Context, rate);

                                    if (!DALUtils.SearchObjects<GMGColorDAL.AttachedAccountBillingPlan>(o => o.Account == model.Account && o.BillingPlan == selectedId, Context).Any())
                                    {
                                        GMGColorDAL.AttachedAccountBillingPlan objAttachedAccountBillingPlan = new GMGColorDAL.AttachedAccountBillingPlan();
                                        objAttachedAccountBillingPlan.Account = model.Account;
                                        objAttachedAccountBillingPlan.BillingPlan = selectedId;
                                        objAttachedAccountBillingPlan.IsAppliedCurrentRate = true;
                                        objAttachedAccountBillingPlan.NewPrice = price;
                                        objAttachedAccountBillingPlan.IsModifiedProofPrice = false;
                                        objAttachedAccountBillingPlan.NewProofPrice = proofPrice;

                                        objAccount.AttachedAccountBillingPlans.Add(objAttachedAccountBillingPlan);
                                        Context.AttachedAccountBillingPlans.Add(objAttachedAccountBillingPlan);
                                        Context.SaveChanges();
                                    }
                                }
                            }

                            if (attachedBillingPlanIds.Count > 0)
                            {
                                foreach (int attachedId in attachedBillingPlanIds)
                                {
                                    GMGColorDAL.AttachedAccountBillingPlan objAttachedAccountBillingPlan =
                                        DALUtils.SearchObjects<GMGColorDAL.AttachedAccountBillingPlan>(
                                            o => o.Account == model.Account && o.BillingPlan == attachedId, Context).
                                            SingleOrDefault();

                                    DALUtils.Delete<GMGColorDAL.AttachedAccountBillingPlan>(Context, objAttachedAccountBillingPlan);
                                }
                            }
                        }
                    }

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "NewAccount --> Plans", ex);
            }
            finally
            {
            }

            AccountCustomise accountCustomiseModel = new AccountCustomise(this.LoggedUserTempFolderPath, model.Account, Context);
            TempData["AccountCustomiseModel"] = accountCustomiseModel;
            Session["IsSecondCompleted"] = true;

            return RedirectToAction("AccountCustomize", "Account");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Customize")]
        public ActionResult Customize(AccountCustomise model, string hdnFileName_1, string hdnFileName_2, string hdnFileName_3, string hdnFileName_4)
        {
            try
            {
                if (!CanAccess(model.Account, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                if (ModelState.IsValid)
                {
                    GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.Account, Context);

                    this.UpdateLogos(objAccount, hdnFileName_1, hdnFileName_2, hdnFileName_3, hdnFileName_4);

                    objAccount.SiteName = model.SiteName;

                    if (objAccount.AccountThemes.Count == 0)
                    {
                        objAccount.AccountThemes = ThemeBL.CreateAccountThemeSchema(model.AccountThemes, objAccount.ID);
                        Context.AccountThemes.AddRange(objAccount.AccountThemes);
                    }
                    else
                    {
                        ThemeBL.UpdateAccountDbThemes(objAccount.AccountThemes, model.AccountThemes);
                    }

                    Context.SaveChanges();
                }
                else
                {
                    return RedirectToAction("UnexpectedError", "Error");
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "NewAccount --> Customise", ex);
            }

            TempData["Account"] = model.Account.ToString();
            Session["IsThirdCompleted"] = true;

            return RedirectToAction("AccountConfirm", "Account");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveAndSendInvite")]
        public ActionResult SendInvite(string Account)
        {
            int accountId = Account.ToInt().GetValueOrDefault();
            if (!CanAccess(accountId, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(accountId, Context);

            bool isDomainExist = (DALUtils.SearchObjects<GMGColorDAL.Account>(o => !o.IsTemporary && o.Domain.Replace(GMGColorConfiguration.AppConfiguration.HostAddress, string.Empty) == objAccount.Domain, Context).Any());
            if (isDomainExist)
            {
                TempData["Account"] = Account;
                Session["IsThirdCompleted"] = true;

                return RedirectToAction("AccountConfirm", "Account");
            }
            else
            {
                this.ConfirmAccountSetup(int.Parse(Account), true);
                return RedirectToAction("Index", new { page = Session[GMGColorConstants.AccountPagination] });
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SaveAsDraft")]
        public ActionResult SaveAsDraft(string Account)
        {
            int accountId = Account.ToInt().GetValueOrDefault();
            if (!CanAccess(accountId, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(accountId, Context);

            bool isDomainExist = (DALUtils.SearchObjects<GMGColorDAL.Account>(o => !o.IsTemporary && o.Domain.Replace(GMGColorConfiguration.AppConfiguration.HostAddress, string.Empty) == objAccount.Domain, Context).ToList().Count > 0);
            if (isDomainExist)
            {
                TempData["Account"] = Account;
                Session["IsThirdCompleted"] = true;

                return RedirectToAction("AccountConfirm", "Account");
            }
            else
            {
                this.ConfirmAccountSetup(int.Parse(Account), false);
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Back")]
        public ActionResult Back(AccountPlans plansModel, AccountCustomise customiseModel, string Account)
        {
            Session["NextTab"] = Convert.ToInt32(Session["NextTab"]) - 1;

            switch (Session["NextTab"].ToString())
            {
                case "1":
                    {
                        int accountId = plansModel.Account;

                        AccountInfo accountInfoModel = new AccountInfo(this.LoggedAccount.ID, accountId, Context);
                        TempData["AccountInfoModel"] = accountInfoModel;
                        break;
                    }
                case "2":
                    {
                        int accountId = customiseModel.Account;

                        AccountPlans accountPlansModel = new AccountPlans(this.LoggedAccount, accountId, Context, true);
                        TempData["AccountPlansModel"] = accountPlansModel;
                        break;
                    }
                case "3":
                    {
                        int accountId = int.Parse(Account);

                        AccountCustomise accountCustomiseModel = new AccountCustomise(this.LoggedUserTempFolderPath, accountId, Context);
                        TempData["AccountCustomiseModel"] = accountCustomiseModel;
                        break;
                    }
                default:
                    break;
            }

            return RedirectToAction("NewAccount", "Account");
        }

        #endregion

        #endregion

        #region Edit Account

        #region GET Actions

        [HttpGet]
        public ActionResult Summary()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.Summary model = new EditAccount.Summary(accId, this.LoggedAccount, Context);
            return View("EditAccount-Summary", model);
        }

        [HttpGet]
        public ActionResult Profile()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.Profile model = new EditAccount.Profile(accId, this.LoggedUser.ID, Context);

            if (TempData["ModelErrors"] != null)
            {
                Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                TempData["ModelErrors"] = null;

                foreach (KeyValuePair<string, string> item in dicModelErrors)
                {
                    ModelState.AddModelError(item.Key, item.Value);
                }
            }

            return View("EditAccount-Profile", model);
        }

        [HttpGet]
        public ActionResult Customize()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.Customise model = new EditAccount.Customise(accId, this.LoggedUserTempFolderPath, Context);
            model.objColorSchema = ThemeBL.GetAccountGlobalColorScheme(accId, Context);

            return View("EditAccount-Customize", model);
        }

        [HttpGet]
        public ActionResult SiteSettings()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.SiteSettings model = new EditAccount.SiteSettings(accId, this.LoggedAccount.AccountType, Context);
            ViewBag.Regions = Amazon.RegionEndpoint.EnumerableAllRegions.Where(o => o.SystemName == "ap-southeast-1").ToList();
            return View("EditAccount-SiteSettings", model);
        }

        [HttpGet]
        public ActionResult WhiteLabel()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.WhiteLabelSettings model = new EditAccount.WhiteLabelSettings(accId, Context);

            return View("EditAccount-WhiteLabel", model);
        }

        [HttpGet]
        public ActionResult DNSAndCustomDomain()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.DNSAndCustomDomain model = new EditAccount.DNSAndCustomDomain(accId, Context);

            return View("EditAccount-DNSAndCustomDomain", model);
        }

        [HttpGet]
        public ActionResult Users()
        {
            //TempData["ModelErrors"] = null;

            if (Session["SelectedAccId"] == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                int accId = int.Parse(Session["SelectedAccId"].ToString());
                EditAccount.Users model = new EditAccount.Users(accId, this.LoggedAccount, Context);

                if (TempData["Filter"] != null)
                {
                    Dictionary<string, object> dicFilterPara = (Dictionary<string, object>)TempData["Filter"];
                    TempData["Filter"] = null;

                    model = new EditAccount.Users(int.Parse(dicFilterPara["account"].ToString()), this.LoggedAccount, Context);

                    string s = dicFilterPara["searchText"].ToString();
                    model.SearchText = s;

                    model.ListUsers = model.ListUsers.Where(o => o.FamilyName.ToLower().Contains(s) ||
                                                                 o.GivenName.ToLower().Contains(s) ||
                                                                 o.Username.ToLower().Contains(s)
                                                           ).ToList();

                }

                DuplicateUser duplicateUserModel = null;
                if (TempData["DuplicateUserErrors"] != null)
                {
                    Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["DuplicateUserErrors"];
                    TempData["DuplicateUserErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }

                    ViewBag.ModelDialog = "modalDuplicateUser";
                    duplicateUserModel = TempData["DuplicateUserModel"] as DuplicateUser;
                }
                else
                {
                    duplicateUserModel = new DuplicateUser();
                }

                duplicateUserModel.IsFromEditAccount = true;
                ViewBag.DuplicateUserModel = duplicateUserModel;

                return View("EditAccount-Users", model);
            }
        }

        [HttpGet]
        public ActionResult AddEditUser(int? SelectedUser)
        {
            int accId = int.Parse(Session["SelectedAccId"].ToString());
            EditAccount.AddEditUser model = new EditAccount.AddEditUser(LoggedAccount, Context);

            ViewBag.AccountHasSsoEnabled = LoggedAccountHasSsoEnabled;

            if (SelectedUser != null) //Edit User
            {
                if (TempData["editUserModel"] != null)
                {
                    model.objEditUser = (UserInfo)TempData["editUserModel"];
                    model.objEditUser.AccountID = accId;
                }
                else
                {
                    model.objEditUser = new UserInfo(SelectedUser.Value, this.LoggedAccount.ID, Context);
                    model.objEditUser.LoggedAccountId = this.LoggedAccount.ID;
                    model.objEditUser.Controller = "Account";
                }

                if (UserBL.IsAccountOwner(SelectedUser.Value, Context))
                {
                    model.objEditUser.ModulePermisions.ForEach(o => o.ReadOnly = true);
                }

                ProofStudioUserColor color = Context.ProofStudioUserColors.Where(c => c.ID == model.objEditUser.ProofStudioColorID).FirstOrDefault();
                PopulateProofStudioUserColors(color, model.objEditUser.objUser.ID);
            }
            else //New User
            {
                if (TempData["newUserModel"] != null)
                {
                    model.objNewUser = (UserInfo)TempData["newUserModel"];
                    model.objNewUser.AccountID = accId;
                }
                else
                {
                    model.objNewUser = new UserInfo(accId, LoggedAccount.ID, Context, true);
                    model.objNewUser.LoggedAccountId = this.LoggedAccount.ID;
                    model.objNewUser.Controller = "Account";
                }
                
                ProofStudioUserColor color = UserBL.GetAvailableUserColor(model.objNewUser.objUser.Account, Context);
                PopulateProofStudioUserColors(color, model.objNewUser.objUser.ID);
            }


            if (TempData["ModelErrors"] != null)
            {
                Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                TempData["ModelErrors"] = null;

                foreach (KeyValuePair<string, string> item in dicModelErrors)
                {
                    ModelState.AddModelError(item.Key, item.Value);
                }
            }

            return View("EditAccount-AddEditUser", model);
        }

        [HttpGet]
        public ActionResult BillingInfo()
        {
            int accId = 0;
            var subscriberModel = (SubscriberPlanPerModule)TempData[SubscriberModel];
            var subscriptionModel = (SubscriptionPlanPerModule)TempData[SubscriptionModel];

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.BillingInfo model = AccountBL.PopulateBillingInfoModel(accId, subscriberModel, subscriptionModel, false, LoggedAccount, Context);

            Dictionary<string, string> dict = TempData[BillingInfoError] as Dictionary<string, string>;
            if (dict != null)
            {
                dict.Keys.ToList().ForEach(k => ModelState.AddModelError(k, dict[k]));
            }

            return View("EditAccount-BillingInfo", model);
        }

        [HttpGet]
        public ActionResult Plans()
        {
            int accId = 0;
            EditAccount.Plans model;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            if (TempData["AccountPlansModel"] != null)
            {
                model = (EditAccount.Plans)TempData["AccountPlansModel"];
                TempData["AccountPlansModel"] = null;

                if (TempData["ModelErrors"] != null)
                {
                    Dictionary<string, string> dicModelErrors = (Dictionary<string, string>)TempData["ModelErrors"];
                    TempData["ModelErrors"] = null;

                    foreach (KeyValuePair<string, string> item in dicModelErrors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }
            }
            else
            {
                model = new EditAccount.Plans(accId, Context);
            }

            model.IsEditAccountPlansFromReport = false;
            return View("EditAccount-Plans", model);
        }

        [HttpGet]
        public ActionResult SuspendAccount()
        {
            int accId = 0;

            if (Session["SelectedAccId"] == null)
                return RedirectToAction("Index");
            else
                accId = int.Parse(Session["SelectedAccId"].ToString());

            EditAccount.SuspendAccount model = new EditAccount.SuspendAccount(accId, Context);

            return View("EditAccount-SuspendAccount", model);
        }

        #endregion

        #region POST Actions

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditAccount")]
        public ActionResult EditAccountSummary(string selectedAccountId)
        {
            int accountId = selectedAccountId.ToInt().GetValueOrDefault();
            if (accountId > 0 && !CanAccess(accountId, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            Session["SelectedAccId"] = selectedAccountId;
            Session["AccountIsSelectedFromReport"] = false; 
            return RedirectToAction("Summary");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Profile")]
        public ActionResult Profile(EditAccount.Profile model)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context) || !DALUtils.IsFromCurrentAccount<GMGColorDAL.Company>(model.objCompany.ID, model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();

                if (DALUtils.SearchObjects<GMGColorDAL.Account>(o => o.ID != model.objAccount.ID, Context)
                                                    .Any(o => o.Domain.Replace(GMGColorConfiguration.AppConfiguration.HostAddress, string.Empty) == model.objAccount.Domain))
                {
                    dicModelErrors.Add("objAccount.Domain", Resources.Resources.reqDomainExist);
                    TempData["ModelErrors"] = dicModelErrors;
                    return RedirectToAction("Profile", "Account");
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.objAccount.ID, Context);

                GMGColorDAL.AccountStatu.Status accStatus = GMGColorDAL.AccountStatu.GetAccountStatus(objAccount.ID, Context);

                if ((accStatus == GMGColorDAL.AccountStatu.Status.Draft || accStatus == GMGColorDAL.AccountStatu.Status.Inactive))
                {
                    if (!objAccount.NeedSubscriptionPlan)
                    {
                        bool isSubscriber = ((from act in Context.AccountTypes
                                              where act.ID == model.objAccount.AccountType
                                              select act.Key).FirstOrDefault() ?? String.Empty) == "SBSC";
                        if (isSubscriber)
                        {
                            dicModelErrors.Add("objAccount.AccountType", Resources.Resources.errNoSubcriptionForSubscriber);
                            TempData["ModelErrors"] = dicModelErrors;
                            return RedirectToAction("Profile", "Account");
                        }
                    }
                }

                //Account Information 
                objAccount.Owner = model.objAccount.Owner;
                objAccount.AccountType = (accStatus == GMGColorDAL.AccountStatu.Status.Draft || accStatus == GMGColorDAL.AccountStatu.Status.Inactive) ?
                                            model.objAccount.AccountType
                                            : objAccount.AccountType;
                objAccount.Name = model.objAccount.Name;
                objAccount.DealerNumber = model.objAccount.DealerNumber;
                objAccount.CustomerNumber = model.objAccount.CustomerNumber;
                objAccount.VATNumber = model.objAccount.VATNumber;
                objAccount.ModifiedDate = DateTime.UtcNow;

                GMGColorDAL.Company objCompany = DALUtils.GetObject<GMGColorDAL.Company>(model.objCompany.ID, Context);

                //Billing Address
                objCompany.Name = model.objCompany.Name;
                objCompany.Address1 = model.objCompany.Address1;
                objCompany.Address2 = model.objCompany.Address2;
                objCompany.City = model.objCompany.City;
                objCompany.State = model.objCompany.State;
                objCompany.Postcode = model.objCompany.Postcode;
                objCompany.Country = model.objCompany.Country;

                Context.SaveChanges();

            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> Profile", ex);
            }
            finally
            {
            }

            return RedirectToAction("Profile");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditCustomize")]
        public ActionResult Customize(EditAccount.Customise model, string hdnFileName_1, string hdnFileName_2, string hdnFileName_3, string hdnFileName_4)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                //remove not needed validation errors (these fileds are not sent from the client so the model will always have an error)
                if (!ModelState.IsValid)
                {
                    ModelState["objAccount.Name"].Errors.Clear();
                    ModelState["objAccount.Domain"].Errors.Clear();
                    ModelState["objAccount.TimeZone"].Errors.Clear();
                    ModelState["objCompany.Name"].Errors.Clear();
                    ModelState["objCompany.Address1"].Errors.Clear();
                    ModelState["objCompany.City"].Errors.Clear();
                }
                if (ModelState.IsValid)
                {
                    GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.objAccount.ID, Context);

                    this.UpdateLogos(objAccount, hdnFileName_1, hdnFileName_2, hdnFileName_3, hdnFileName_4);

                    objAccount.SiteName = model.objAccount.SiteName;

                    ThemeBL.UpdateAccountDbThemes(objAccount.AccountThemes, model.objColorSchema);

                    objAccount.ModifiedDate = DateTime.UtcNow;

                    Context.SaveChanges();
                }
                else
                {
                    RedirectToAction("UnexpectedError", "Error");
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> Customize", ex);
            }

            return RedirectToAction("Customize");
        }

        [HttpPost]
        public ActionResult SiteSettings(EditAccount.SiteSettings model)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.objAccount.ID, Context);

                //Regional Settings
                if (
                    (this.LoggedAccount.AccountType == GMGColorDAL.AccountType.GetAccountType(GMGColorDAL.AccountType.Type.GMGColor, Context).ID) &&
                    (
                        (objAccount.Status == GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Inactive, Context).ID) ||
                        (objAccount.Status == GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Draft, Context).ID)
                    )
                    )
                {
                    objAccount.Region = model.objAccount.Region;
                }
                objAccount.Locale = model.objAccount.Locale;
                objAccount.DateFormat = model.objAccount.DateFormat;
                objAccount.TimeZone = model.objAccount.TimeZone;
                objAccount.TimeFormat = model.objAccount.TimeFormat;
                objAccount.ModifiedDate = DateTime.UtcNow;

                if (model.ApplyLanguageToAllUsers)
                {
                    foreach (var user in objAccount.Users)
                    {
                        user.Locale = objAccount.Locale;
                    }
                }

                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> SiteSettings", ex);
            }
            return RedirectToAction("SiteSettings");
        }

        [HttpPost]
        public ActionResult WhiteLabelSettings(EditAccount.WhiteLabelSettings model)
        {
            try
            {
                if (!CanAccess(model.AccountID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.AccountID, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAccount.IsRemoveAllGMGCollaborateBranding = model.IsRemoveAllGMGCollaborateBranding;
                    //objAccount.CustomFromAddress = model.CustomFromEmail;
                    //objAccount.CustomFromName = model.CustomFromName;
                    objAccount.ModifiedDate = DateTime.UtcNow;

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> WhiteLabelSettings", ex);
            }
            finally
            {
            }

            return RedirectToAction("WhiteLabel");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ChangeDomain")]
        public ActionResult ChangeDomain(EditAccount.DNSAndCustomDomain model)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.objAccount.ID, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAccount.IsCustomDomainActive = !objAccount.IsCustomDomainActive;
                    objAccount.ModifiedDate = DateTime.UtcNow;

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error occured wille saving the custom domain", ex);
            }
            finally
            {
            }
            return RedirectToAction("DNSAndCustomDomain");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ResetPasswordOwner")]
        public ActionResult ResetPasswordOwner(EditAccount.Users model)
        {
            try
            {
                int accountId = model.objAccount != null
                                ? model.objAccount.ID
                                : 0;
                if (accountId > 0)
                {
                    if (!CanAccess(accountId, Context))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    int? userId = GMGColorDAL.Account.GetAccountOwnerId(accountId, Context);
                    if (userId.HasValue)
                    {
                        GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(userId.Value, Context);

                        string password = GMGColorDAL.User.GetNewRandomPassword();
                        objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(password, Context);
                        objUser.NeedReLogin = true;
                        Context.SaveChanges();

                        NotificationServiceBL.CreateNotification(new LostPassword
                                                                {
                                                                    EventCreator = LoggedAccount.Owner,
                                                                    InternalRecipient = userId.Value,
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    NewPassword = password
                                                                },
                                                                null,
                                                                Context
                                                                );
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> Profile --> ResetPassword", ex);
            }
            finally
            {
                Context.Dispose();
            }
            return RedirectToAction("Profile");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "ResetPassword")]
        public ActionResult ResetPassword(EditAccount.AddEditUser model)
        {
            int accountId = model.objEditUser != null && model.objEditUser.objUser != null
                                       ? model.objEditUser.objUser.Account
                                       : 0;
            int userId = model.objEditUser != null && model.objEditUser.objUser != null
                             ? model.objEditUser.objUser.ID
                             : 0;

            if (accountId > 0 && !CanAccess(accountId, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }
            if (userId > 0 && accountId > 0 && !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(userId, accountId, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            try
            {
                GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(userId, Context);

                string password = GMGColorDAL.User.GetNewRandomPassword();
                objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(password, Context);
                objUser.NeedReLogin = true;
                Context.SaveChanges();

                NotificationServiceBL.CreateNotification(new LostPassword
                                                        {
                                                            EventCreator = LoggedAccount.Owner,
                                                            InternalRecipient = objUser.ID,
                                                            CreatedDate = DateTime.UtcNow,
                                                            NewPassword = password
                                                        },
                                                        null,
                                                        Context
                                                        );
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> Profile --> ResetPassword", ex);
            }
            finally
            {
            }

            return RedirectToAction("AddEditUser", new { SelectedUser = userId });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditPlans")]
        public ActionResult Plans(EditAccount.Plans model)
        {
            try
            {
                if (!CanAccess(model.Account, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                bool hasError = false;
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();

                if (GMGColorDAL.AccountType.GetAccountType(model.Account, Context) != GMGColorDAL.AccountType.Type.Subscriber)
                {
                    if (model.AlreadyAttachedBillingPlans == null)
                    {
                        if (model.AttachedBillingPlans == null)
                        {
                            hasError = true;
                            dicModelErrors.Add("AttachedBillingPlans", Resources.Resources.reqPlan);
                        }
                        else
                        {
                            if (model.AttachedBillingPlans.Where(o => o != "false").Count() == 0)
                            {
                                hasError = true;
                                dicModelErrors.Add("AttachedBillingPlans", Resources.Resources.reqPlan);
                            }
                        }
                    }

                    if (hasError)
                    {
                        decimal discountTmp = model.GPPDiscount.GetValueOrDefault();
                        model = new EditAccount.Plans(model.Account, Context);
                        model.GPPDiscount = discountTmp;

                        Session["NextTab"] = 2;
                        TempData["AccountPlansModel"] = model;
                        TempData["ModelErrors"] = dicModelErrors;
                        return RedirectToAction("Plans", "Account");
                    }
                }

                using (TransactionScope ts = new TransactionScope())
                {
                    GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.Account, Context);

                    objAccount.GPPDiscount = (model.GPPDiscount != null) ? (model.GPPDiscount) : null;
                    objAccount.ModifiedDate = DateTime.UtcNow;

                    if (GMGColorDAL.AccountType.GetAccountType(model.Account, Context) != GMGColorDAL.AccountType.Type.Subscriber)
                    {

                        List<int> selectedBillingPlanIds = (model.AttachedBillingPlans == null) ? new List<int>() : model.AttachedBillingPlans.Where(o => o != "false").Select(o => int.Parse(o)).ToList();

                        //TODO What is in already attached represents the plans that are attached to the current accounts

                        if (model.AlreadyAttachedBillingPlans != null)
                        {
                            foreach (string currentID in model.AlreadyAttachedBillingPlans.Split(','))
                            {
                                selectedBillingPlanIds.Add(int.Parse(currentID));
                            }
                        }

                        List<int> attachedBillingPlanIds =
                            objAccount.AttachedAccountBillingPlans.Select(o => o.BillingPlan).
                                Intersect(model.BillingPlans).ToList();

                        if (selectedBillingPlanIds.Count > 0)
                        {
                            decimal rate = 1;
                            if (this.LoggedAccount.CurrencyFormat != objAccount.CurrencyFormat)
                            {
                                rate = (new GMGColorCommon()).GetUpdatedCurrencyRate(objAccount, Context);
                            }

                            foreach (int selectedId in selectedBillingPlanIds)
                            {
                                if (attachedBillingPlanIds.Contains(selectedId))
                                {
                                    attachedBillingPlanIds.Remove(selectedId);
                                }
                                else
                                {
                                    decimal price = GMGColorDAL.AttachedAccountBillingPlan.GetPlanPrice(this.LoggedAccount.ID, selectedId, Context, rate);
                                    decimal proofPrice = GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(this.LoggedAccount.ID, selectedId, Context, rate);

                                    if (!DALUtils.SearchObjects<GMGColorDAL.AttachedAccountBillingPlan>(o => o.Account == model.Account && o.BillingPlan == selectedId, Context).Any())
                                    {
                                        GMGColorDAL.AttachedAccountBillingPlan objAttachedAccountBillingPlan = new GMGColorDAL.AttachedAccountBillingPlan();
                                        objAttachedAccountBillingPlan.Account = model.Account;
                                        objAttachedAccountBillingPlan.BillingPlan = selectedId;
                                        objAttachedAccountBillingPlan.IsAppliedCurrentRate = true;
                                        objAttachedAccountBillingPlan.NewPrice = price;
                                        objAttachedAccountBillingPlan.IsModifiedProofPrice = false;
                                        objAttachedAccountBillingPlan.NewProofPrice = proofPrice;

                                        objAccount.AttachedAccountBillingPlans.Add(objAttachedAccountBillingPlan);
                                        Context.AttachedAccountBillingPlans.Add(objAttachedAccountBillingPlan);
                                        Context.SaveChanges();
                                    }
                                }
                            }

                            if (attachedBillingPlanIds.Count > 0)
                            {
                                foreach (int attachedId in attachedBillingPlanIds)
                                {
                                    AccountBL.UnassignPlanType(attachedId, objAccount.ID, Context);
                                }
                            }
                        }
                    }
                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> Plans", ex);
            }
            finally
            {
            }

            if ((bool)Session["AccountIsSelectedFromReport"])
            {
                return RedirectToAction("Plans", "Reports");
            }
            return RedirectToAction("Plans");
        }

        [HttpPost]
        public ActionResult BillingInfo(EditAccount.BillingInfo model)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.objAccount.ID, Context);
                string datePattern = objAccount.DateFormat1.Pattern;

                if (objAccount.ID == 0)
                    throw new Exception("You cannot edit an account with ID = 0!");

				int? billingPlanCollaborate1 = null,
					 billingPlanCollaborate2 = null,
					 billingPlanDeliver1 = null,
					 billingPlanDeliver2 = null,

					 collaborateOldNrOfCredits = null,
					 collaborateNewNrOfCredits = null,
					 deliverOldNrOfCredits = null,
					 deliverNewNrOfCredits = null;                   

                Dictionary<string, string> dict = new Dictionary<string, string>();

                bool isSubscriber = (GMGColorDAL.AccountType.GetAccountType(objAccount.AccountType1.Key) == GMGColorDAL.AccountType.Type.Subscriber);

                if (isSubscriber || model.NeedSubscriptionPlan)
                {
                    objAccount.NeedSubscriptionPlan = true;
                    if (!isSubscriber)
                    {
                        #region Check if any billing plan was selected
                        if (!model.PlanPerModule.CollaborateBillingPlanId.HasValue &&                          
                            !model.PlanPerModule.DeliverBillingPlanId.HasValue)
                        {
                            dict.Add("PlanPerModule.SelectedPlans", Resources.Resources.lblAtLeastOneSubscriptionPlanShouldBeChoosen);
                            TempData[BillingInfoError] = dict;
                            TempData[SubscriptionModel] = model.PlanPerModule;
                            return RedirectToAction("BillingInfo");
                        }
                        #endregion

                        #region Plan for Collaborate

                        {
                            GMGColorDAL.AccountSubscriptionPlan collaboratePlan = GMGColorDAL.Account.GetNonSubscriberPlan(objAccount.ID, AppModule.Collaborate, Context);

                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.PlanPerModule.CollaborateBillingPlanId.HasValue && collaboratePlan != null)
                            {
                                billingPlanCollaborate1 = collaboratePlan.SelectedBillingPlan;
                                billingPlanCollaborate2 = null;

                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.AccountSubscriptionPlan>(Context, collaboratePlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.PlanPerModule.CollaborateBillingPlanId.HasValue)
                                {
                                    billingPlanCollaborate1 = collaboratePlan != null
                                                                    ? (int?)collaboratePlan.SelectedBillingPlan
                                                                    : null;

                                    if (collaboratePlan == null)
                                        collaboratePlan = new GMGColorDAL.AccountSubscriptionPlan();

                                    collaboratePlan.SelectedBillingPlan = model.PlanPerModule.CollaborateBillingPlanId.Value;
                                    collaboratePlan.IsMonthlyBillingFrequency =
                                        (model.PlanPerModule.CollaborateIsMonthlyBillingFrequency ==
                                            Enums.BillingFrequency.Monthly);
                                    collaboratePlan.ChargeCostPerProofIfExceeded =
                                        model.PlanPerModule.CollaborateChargeCostPerProofIfExceeded;
                                    collaboratePlan.Account1 = objAccount;
                                    if (!collaboratePlan.ContractStartDate.HasValue)
                                    {
                                        collaboratePlan.ContractStartDate =
                                            (model.PlanPerModule.CollaborateContractStartDateNow ==
                                             ContractStartDateEnum.Today)
                                                ? (DateTime?) DateTime.Now
                                                : null;
                                    }
                                    else
                                    {
                                        collaboratePlan.ContractStartDate = DateTime.ParseExact(model.PlanPerModule.CollaborateContractStartDate, datePattern, CultureInfo.InvariantCulture);    
                                    }

                                    if (collaboratePlan.ID == 0)
                                        objAccount.AccountSubscriptionPlans.Add(collaboratePlan);

                                    if (!collaboratePlan.AccountPlanActivationDate.HasValue)
                                    {
                                        collaboratePlan.AccountPlanActivationDate = (model.PlanPerModule.CollaborateContractStartDateNow == ContractStartDateEnum.Today)
                                                ? (DateTime?)DateTime.UtcNow
                                                : null;
                                    }
                                    else if (billingPlanCollaborate1 != collaboratePlan.SelectedBillingPlan)
                                    {
                                        collaboratePlan.AccountPlanActivationDate = DateTime.UtcNow;
                                    }
                                   
                                    collaboratePlan.AllowOverdraw = model.PlanPerModule.CollaborateAllowOverdraw;    

                                    billingPlanCollaborate2 = model.PlanPerModule.CollaborateBillingPlanId;

                                    AccountBL.ResetAccountUserdStorage(objAccount.ID, Context);
                                }
                            }
                        }

                        #endregion

                        #region Plan for Deliver

                        {
                            GMGColorDAL.AccountSubscriptionPlan deliverPlan = GMGColorDAL.Account.GetNonSubscriberPlan(objAccount.ID, AppModule.Deliver, Context);
                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.PlanPerModule.DeliverBillingPlanId.HasValue && deliverPlan != null)
                            {
                                billingPlanDeliver1 = deliverPlan.SelectedBillingPlan;
                                billingPlanDeliver2 = null;

                                // remove the selection from database
                                DALUtils.Delete<AccountSubscriptionPlan>(Context, deliverPlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.PlanPerModule.DeliverBillingPlanId.HasValue)
                                {
                                    billingPlanDeliver1 = deliverPlan != null
                                                                ? (int?)deliverPlan.SelectedBillingPlan
                                                                : null;

                                    if (deliverPlan == null)
                                        deliverPlan = new GMGColorDAL.AccountSubscriptionPlan();

                                    deliverPlan.SelectedBillingPlan = model.PlanPerModule.DeliverBillingPlanId.Value;
                                    deliverPlan.IsMonthlyBillingFrequency =
                                        (model.PlanPerModule.DeliverIsMonthlyBillingFrequency ==
                                            Enums.BillingFrequency.Monthly);
                                    deliverPlan.ChargeCostPerProofIfExceeded =
                                        model.PlanPerModule.DeliverChargeCostPerProofIfExceeded;
                                    deliverPlan.Account1 = objAccount;
                                    if (!deliverPlan.ContractStartDate.HasValue)
                                    {
                                        deliverPlan.ContractStartDate =
                                            (model.PlanPerModule.DeliverContractStartDateNow ==
                                             ContractStartDateEnum.Today)
                                                ? (DateTime?) DateTime.Now
                                                : null;
                                    }
                                    else
                                    {
                                        deliverPlan.ContractStartDate = DateTime.ParseExact(model.PlanPerModule.DeliverContractStartDate, datePattern, CultureInfo.InvariantCulture);  
                                    }

                                    if (deliverPlan.ID == 0)
                                        objAccount.AccountSubscriptionPlans.Add(deliverPlan);

                                    if (!deliverPlan.AccountPlanActivationDate.HasValue)
                                    {
                                        deliverPlan.AccountPlanActivationDate = (model.PlanPerModule.DeliverContractStartDateNow == ContractStartDateEnum.Today)
                                                ? (DateTime?)DateTime.UtcNow
                                                : null;
                                    }
                                    else if (billingPlanDeliver1 != deliverPlan.SelectedBillingPlan)
                                    {
                                        deliverPlan.AccountPlanActivationDate = DateTime.UtcNow;
                                    }

                                    deliverPlan.AllowOverdraw = model.PlanPerModule.DeliverAllowOverdraw;

                                    billingPlanDeliver2 = model.PlanPerModule.DeliverBillingPlanId;
                                }
                            }
                        }

                        #endregion                       
                    }
                    else
                    {
                        #region Check if any billing plan was selected
                        if (!model.SubscriberPlans.CollaborateBillingPlanId.HasValue &&                           
                            !model.SubscriberPlans.DeliverBillingPlanId.HasValue)
                        {

                            dict.Add("SubscriberPlans.SelectedPlans", Resources.Resources.lblAtLeastOneSubscriptionPlanShouldBeChoosen);
                            TempData[BillingInfoError] = dict;
                            return RedirectToAction("BillingInfo");
                        }
                        #endregion

                        #region Check the number of credits
                        if (GMGColorDAL.AccountType.GetAccountType(this.LoggedAccount.ID, Context) == GMGColorDAL.AccountType.Type.Client)
                        {
                            bool anyError = false;
                            if (model.SubscriberPlans.CollaborateCreditAllocationType == CreditAllocationTypeEnum.Quota && model.SubscriberPlans.CollaborateBillingPlanId.HasValue)
                            {
                                int creditsLeft = SubscriberGetNumberOfRemainingCredits(model.SubscriberPlans.CollaborateBillingPlanId.Value, model.objAccount.ID, AppModule.Collaborate);
                                if (creditsLeft < model.SubscriberPlans.CollaborateNrOfCredits && !model.SubscriberPlans.SelectedCollaboratePlanIsFixed)
                                {
                                    dict.Add("SubscriberPlans.CollaborateNrOfCredits", String.Format(CultureInfo.InvariantCulture, Resources.Resources.lblNrOfCreditsExceeded, creditsLeft));

                                    AccountPlans accountPlansModelTemp = new AccountPlans(this.LoggedAccount, model.objAccount.ID, Context, true);
                                    model.objAccountBillingPlans = accountPlansModelTemp.objAccountBillingPlans;
                                    anyError = true;
                                }
                            }                           
                            if (model.SubscriberPlans.DeliverCreditAllocationType == CreditAllocationTypeEnum.Quota && model.SubscriberPlans.DeliverBillingPlanId.HasValue)
                            {
                                int creditsLeft =
                                    SubscriberGetNumberOfRemainingCredits(model.SubscriberPlans.DeliverBillingPlanId.Value, model.objAccount.ID, AppModule.Deliver);
                                if (creditsLeft < model.SubscriberPlans.DeliverNrOfCredits && !model.SubscriberPlans.SelectedDeliverPlanIsFixed)
                                {
                                    dict.Add("SubscriberPlans.DeliverNrOfCredits", String.Format(CultureInfo.InvariantCulture, Resources.Resources.lblNrOfCreditsExceeded, creditsLeft));

                                    AccountPlans accountPlansModelTemp = new AccountPlans(this.LoggedAccount, model.objAccount.ID, Context, true);
                                    model.objAccountBillingPlans = accountPlansModelTemp.objAccountBillingPlans;
                                    anyError = true;
                                }
                            }                           
                            if (anyError)
                            {
                                TempData[BillingInfoError] = dict;
                                TempData[SubscriberModel] = model.SubscriberPlans;
                                return RedirectToAction("BillingInfo");
                            }
                        }

                        #endregion

                        #region Plan for Collaborate
                        {
                            GMGColorDAL.SubscriberPlanInfo collaboratePlan = GMGColorDAL.Account.GetSubscriberPlan(objAccount.ID, AppModule.Collaborate, Context);
                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.SubscriberPlans.CollaborateBillingPlanId.HasValue && collaboratePlan != null)
                            {
                                billingPlanCollaborate1 = collaboratePlan.SelectedBillingPlan;
                                billingPlanCollaborate2 = null;

                                collaborateOldNrOfCredits = collaboratePlan.NrOfCredits;
                                collaborateNewNrOfCredits = null;

                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.SubscriberPlanInfo>(Context, collaboratePlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.SubscriberPlans.CollaborateBillingPlanId.HasValue)
                                {
                                    billingPlanCollaborate1 = collaboratePlan != null
                                                                    ? (int?)collaboratePlan.SelectedBillingPlan
                                                                    : null;
                                    collaborateOldNrOfCredits = collaboratePlan != null
                                                                ? (int?) collaboratePlan.NrOfCredits : null;

                                    if (collaboratePlan == null)
                                        collaboratePlan = new GMGColorDAL.SubscriberPlanInfo();

                                
                                    collaboratePlan.NrOfCredits = model.SubscriberPlans.CollaborateCreditAllocationType == CreditAllocationTypeEnum.Quota && !model.SubscriberPlans.SelectedCollaboratePlanIsFixed
                                           ? model.SubscriberPlans.CollaborateNrOfCredits
                                           : 0;

                                    collaboratePlan.SelectedBillingPlan = model.SubscriberPlans.CollaborateBillingPlanId.Value;
                                    collaboratePlan.ChargeCostPerProofIfExceeded =
                                        model.SubscriberPlans.CollaborateChargeCostPerProofIfExceeded;
                                    collaboratePlan.Account1 = objAccount;
                                    collaboratePlan.IsQuotaAllocationEnabled =
                                        model.SubscriberPlans.CollaborateCreditAllocationType ==
                                        CreditAllocationTypeEnum.Quota;
                                   
                                    if (!collaboratePlan.ContractStartDate.HasValue)
                                    {
                                        collaboratePlan.ContractStartDate =
                                            (model.SubscriberPlans.CollaborateContractStartDateNow ==
                                             ContractStartDateEnum.Today)
                                                ? (DateTime?) DateTime.Now
                                                : null;
                                    }
                                    else
                                    {                                       
                                        collaboratePlan.ContractStartDate = DateTime.ParseExact(model.SubscriberPlans.CollaborateContractStartDate, datePattern, CultureInfo.InvariantCulture);                                            
                                    }


                                    if (collaboratePlan.ID == 0)
                                        objAccount.SubscriberPlanInfoes.Add(collaboratePlan);

                                    if (!collaboratePlan.AccountPlanActivationDate.HasValue)
                                    {
                                        collaboratePlan.AccountPlanActivationDate = (model.SubscriberPlans.CollaborateContractStartDateNow == ContractStartDateEnum.Today)
                                               ? (DateTime?)DateTime.UtcNow
                                               : null;
                                    }
                                    else if (billingPlanCollaborate1 != collaboratePlan.SelectedBillingPlan)
                                    {
                                        collaboratePlan.AccountPlanActivationDate = DateTime.UtcNow;
                                    }

                                    collaboratePlan.AllowOverdraw = model.SubscriberPlans.CollaborateAllowOverdraw;

                                    billingPlanCollaborate2 = model.SubscriberPlans.CollaborateBillingPlanId;
                                    collaborateNewNrOfCredits = model.SubscriberPlans.CollaborateNrOfCredits;

                                    AccountBL.ResetAccountUserdStorage(objAccount.ID, Context);
                                }
                            }
                        }
                        #endregion

                        #region Plan for Deliver

                        {
                            GMGColorDAL.SubscriberPlanInfo deliverPlan = GMGColorDAL.Account.GetSubscriberPlan(objAccount.ID, AppModule.Deliver, Context);
                            // no plan was selected from web interface
                            // an plan is saved into db
                            if (!model.SubscriberPlans.DeliverBillingPlanId.HasValue && deliverPlan != null)
                            {
                                billingPlanDeliver1 = deliverPlan.SelectedBillingPlan;
                                billingPlanDeliver2 = null;

                                deliverOldNrOfCredits = deliverPlan.NrOfCredits;
                                deliverNewNrOfCredits = null;

                                // remove the selection from database
                                DALUtils.Delete<GMGColorDAL.SubscriberPlanInfo>(Context, deliverPlan);
                            }
                            else
                            {
                                // a plan was selected from web interface
                                if (model.SubscriberPlans.DeliverBillingPlanId.HasValue)
                                {
                                    billingPlanDeliver1 = deliverPlan != null
                                                                ? (int?)deliverPlan.SelectedBillingPlan
                                                                : null;
                                    deliverOldNrOfCredits = deliverPlan != null
                                                            ? (int?) deliverPlan.NrOfCredits : null;

                                    if (deliverPlan == null)
                                        deliverPlan = new GMGColorDAL.SubscriberPlanInfo();

                                    deliverPlan.SelectedBillingPlan = model.SubscriberPlans.DeliverBillingPlanId.Value;
                                    deliverPlan.ChargeCostPerProofIfExceeded =
                                        model.SubscriberPlans.DeliverChargeCostPerProofIfExceeded;
                                    deliverPlan.Account1 = objAccount;
                                    deliverPlan.IsQuotaAllocationEnabled =
                                        model.SubscriberPlans.DeliverCreditAllocationType ==
                                        CreditAllocationTypeEnum.Quota;
                                    deliverPlan.NrOfCredits = model.SubscriberPlans.DeliverCreditAllocationType ==
                                                                CreditAllocationTypeEnum.Quota && !model.SubscriberPlans.SelectedDeliverPlanIsFixed
                                                                    ? model.SubscriberPlans.DeliverNrOfCredits
                                                                    : 0;
                                    if (!deliverPlan.ContractStartDate.HasValue)
                                    {
                                        deliverPlan.ContractStartDate =
                                            (model.SubscriberPlans.DeliverContractStartDateNow ==
                                             ContractStartDateEnum.Today)
                                                ? (DateTime?) DateTime.Now
                                                : null;
                                    }
                                    else
                                    {
                                        deliverPlan.ContractStartDate = DateTime.ParseExact(model.SubscriberPlans.DeliverContractStartDate, datePattern, CultureInfo.InvariantCulture);
                                    }

                                    if (deliverPlan.ID == 0)
                                        objAccount.SubscriberPlanInfoes.Add(deliverPlan);

                                    if (!deliverPlan.AccountPlanActivationDate.HasValue)
                                    {
                                        deliverPlan.AccountPlanActivationDate = (model.SubscriberPlans.DeliverContractStartDateNow == ContractStartDateEnum.Today)
                                               ? (DateTime?)DateTime.UtcNow
                                               : null;
                                    }
                                    else if (billingPlanDeliver1 != deliverPlan.SelectedBillingPlan)
                                    {
                                        deliverPlan.AccountPlanActivationDate = DateTime.UtcNow;
                                    }
                                  
                                    deliverPlan.AllowOverdraw = model.SubscriberPlans.DeliverAllowOverdraw;

                                    billingPlanDeliver2 = model.SubscriberPlans.DeliverBillingPlanId;
                                    deliverNewNrOfCredits = model.SubscriberPlans.DeliverNrOfCredits;
                                }
                            }
                        }

                        #endregion                       
                    }

                    //PBD 2013/03/04 BUG 499 objAccount.BillingAnniversaryMonth = (objAccount.IsMonthlyBillingFrequency == null || (bool)objAccount.IsMonthlyBillingFrequency) ? (int?)null : model.objBillingAnniversary.SelectedBillingAnniversaryMonth;
                    //PBD 2013/03/04 BUG 499 objAccount.BillingAnniversaryDate = model.objBillingAnniversary.SelectedBillingAnniversaryDate;

                    //Avoid activating inactive users when subscription plan is removed for an account
                    //GMGColorDAL.Account.SetUsersStatusToActive(objAccount.ID, Context);
                }
                else
                {
                    #region Cleanup existing plan types

                    GMGColorDAL.Account.DeleteAccountSubscriptionPlans(objAccount.ID, Context);

                    if (objAccount.NeedSubscriptionPlan)
                    {
                        foreach (var user in objAccount.Users)
                        {
                            user.NeedReLogin = true;
                        }
                    }
                    objAccount.NeedSubscriptionPlan = false;
                    //PBD 2013/03/04 BUG 499 objAccount.BillingAnniversaryMonth = null;
                    //PBD 2013/03/04 BUG 499 objAccount.BillingAnniversaryDate = string.Empty;

                    #endregion

                    // Avoid setting users to Inactive when subscription plan is removed for an account
                    //GMGColorDAL.Account.SetUsersStatusToInactive(objAccount.ID, Context);
                }

                if (billingPlanCollaborate1.GetValueOrDefault() != billingPlanCollaborate2.GetValueOrDefault() || collaborateOldNrOfCredits != collaborateNewNrOfCredits)
                {
                    NotificationServiceBL.CreateNotification(new PlanWasChanged()
                                                                {
                                                                    EventCreator = LoggedUser.ID,
                                                                    OldBilingPlan = billingPlanCollaborate1,
                                                                    NewBilingPlan = billingPlanCollaborate2,
                                                                    Account = objAccount.ID
                                                                },
                                                                LoggedUser,
                                                                Context
                                                                );
                        
                    foreach (var user in objAccount.Users)
                    {
                        user.NeedReLogin = true;
                    }
                }
               
                if (billingPlanDeliver1.GetValueOrDefault() != billingPlanDeliver2.GetValueOrDefault() || deliverOldNrOfCredits != deliverNewNrOfCredits)
                {
                    NotificationServiceBL.CreateNotification(new PlanWasChanged()
                                                                {
                                                                    EventCreator = LoggedUser.ID,
                                                                    OldBilingPlan = billingPlanDeliver1,
                                                                    NewBilingPlan = billingPlanDeliver2,
                                                                    Account = objAccount.ID
                                                                },
                                                                LoggedUser,
                                                                Context
                                                                );

                    foreach (var user in objAccount.Users)
                    {
                        user.NeedReLogin = true;
                    }
                }
              
                objAccount.ModifiedDate = DateTime.UtcNow;
                objAccount.IsFileTransferEnabled = model.objAccount.IsFileTransferEnabled;

                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> BillingInfo", ex);
            }

            if ((bool)Session["AccountIsSelectedFromReport"])
            {
                return RedirectToAction("BillingInfo", "Reports");
            }

            return RedirectToAction("BillingInfo");
        }

        [HttpPost]
        public ActionResult SuspendAccount(EditAccount.SuspendAccount model)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                AccountBL.SuspendAccount(model.objAccount.ID, model.Reason, LoggedUser.ID, Context);

                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> SuspendAccount", ex);
            }
            finally
            {
            }

            return RedirectToAction("Index", new { page = Session[GMGColorConstants.AccountPagination] });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Search")]
        public ActionResult Search(EditAccount.Users model, string SearchText)
        {
            string s = model.SearchText ?? string.Empty;

            Dictionary<string, object> dicFilterPara = new Dictionary<string, object>();
            dicFilterPara.Add("searchText", s);
            dicFilterPara.Add("account", model.objAccount.ID);

            TempData["Filter"] = dicFilterPara;
            return RedirectToAction("Users", "Account");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "AddEditUserInformation")]
        public ActionResult AddEditUserInformation(EditAccount.AddEditUser model, int? SelectedPrimaryGroup)
        {
            try
            {
                Dictionary<string, string> dicModelErrors = new Dictionary<string, string>();
                bool hasError = false;
                if (model.objEditUser != null)
                {
                    //Edit User Case
                    if (!CanAccess(model.objEditUser.objUser.Account, Context) || GMGColorDAL.User.CanEditUser(model.objEditUser.objUser.ID, model.objEditUser.objUser.Account, Context))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(model.objEditUser.objUser.ID, Context);
                    model.objEditUser.AccountID = objUser.Account;

                    if (model.objEditUser != null && model.objEditUser.ModulePermisions != null)
                    {
                        model.objEditUser.InitializePermissions(model.objEditUser.objUser.ID,
                            model.objEditUser.ModulePermisions.ToDictionary(o => o.ModuleType,
                                                                                       o => (int?)o.SelectedRole), Context);
                    }

                    if (!this.ValidateEmail(model.objEditUser.objUser.Account, model.objEditUser.objUser.EmailAddress, objUser.EmailAddress, Context))
                    {
                        hasError = true;
                        dicModelErrors.Add("objEditUser.objUser.EmailAddress", Resources.Resources.reqEmailExist);
                    }
                    if (!this.ValidateUserName(model.objEditUser.objUser.Username, objUser.Username))
                    {
                        hasError = true;
                        dicModelErrors.Add("objEditUser.objUser.Username", Resources.Resources.reqUsernameExist);
                    }
                    if (model.objEditUser.ModulePermisions != null && (!model.objEditUser.ModulePermisions.Any() ||
                              model.objEditUser.ModulePermisions != null && model.objEditUser.ModulePermisions.Any(m => !m.Roles.Select(r => r.RoleId).Contains(m.SelectedRole))))
                    {
                        hasError = true;
                        dicModelErrors.Add("objEditUser.ValidPermissionsRequired", Resources.Resources.reqPermisionsRequired);
                    }

                    if (hasError)
                    {
                        model.objEditUser.LoggedAccountId = this.LoggedAccount.ID;

                        TempData["editUserModel"] = model.objEditUser;
                        TempData["ModelErrors"] = dicModelErrors;
                        return RedirectToAction("AddEditUser", new { SelectedUser = model.objEditUser.objUser.ID });
                    }
                    else
                    {
                        using (TransactionScope ts = new TransactionScope())
                        {
                            objUser.GivenName = model.objEditUser.objUser.FirstName;
                            objUser.FamilyName = model.objEditUser.objUser.LastName;
                            objUser.Username = model.objEditUser.objUser.Username;
                            objUser.EmailAddress = model.objEditUser.objUser.EmailAddress;
                            objUser.Modifier = this.LoggedUser.ID;
                            objUser.PrinterCompany = model.objEditUser.objUser.PrintCompany;
                            objUser.ModifiedDate = DateTime.UtcNow;
                            objUser.Locale = model.objEditUser.Locale;

                            if (model.objEditUser.objUser.HasPasswordEnabled)
                            {
                                objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(model.objEditUser.objUser.Password, Context);
                            }

                            //get user usergroups
                            List<int> lstGroupUsers = (from o in objUser.UserGroupUsers
                                                       select o.UserGroup).ToList();

                            // account owner could not be edited
                            // current user could not be edited
                            if ((objUser.Account1.Owner != objUser.ID && LoggedUser.ID != objUser.ID))
                            {
                                List<int> formRoles =
                                    model.objEditUser.ModulePermisions.Select(o => o.SelectedRole).ToList();
                                List<int> dbRoles = objUser.UserRoles.Select(o => o.Role).ToList();

                                List<int> deleteRoles = dbRoles.Where(o => !formRoles.Contains(o)).ToList();
                                List<int> newRoles = formRoles.Where(o => !dbRoles.Contains(o)).ToList();

                                if (deleteRoles.Any() || newRoles.Any())
                                {
                                    objUser.NeedReLogin = true;
                                }

                                foreach (int roleId in deleteRoles)
                                {
                                    GMGColorDAL.UserRole userRole = DALUtils.SearchObjects<GMGColorDAL.UserRole>(o => o.Role == roleId && o.User == objUser.ID, Context).FirstOrDefault();
                                    if (userRole != null)
                                    {
                                        DALUtils.Delete<GMGColorDAL.UserRole>(Context, userRole);
                                    }
                                }

                                foreach (int roleId in newRoles)
                                {
                                    Role roleBo = Context.Roles.FirstOrDefault(t => t.ID == roleId);
                                    if (roleBo != null)
                                    {
                                        UserRole userRole = new GMGColorDAL.UserRole() { User = objUser.ID, Role = roleBo.ID };

                                        objUser.UserRoles.Add(userRole);                                        
                                    }
                                }
                            }

                            //Add,Editing UserGroup Users

                            if (model.objEditUser.UserGroupsInUserList != null)
                            {
                                UserBL.AddEditUserGroupUsers(model.objEditUser.UserGroupsInUserList, objUser, SelectedPrimaryGroup, lstGroupUsers, Context);
                            }

                            // Update user color
                            objUser.ProofStudioUserColor = UserBL.ValidateProofStudioUserColor(LoggedAccount.ID, LoggedUser.ID, model.objEditUser.ProofStudioColor, Context);
                            if (objUser.ProofStudioUserColor == null)
                            {
                                throw new Exception("Invalid Proof Studio User Color");
                            }

                            Context.SaveChanges();

                            var collaborateBillingPlan =
                                (new Account {ID = model.objEditUser.objUser.Account}).GetBillingPlanByAppModule(
                                    AppModule.Collaborate, Context);

                            if (collaborateBillingPlan != null && PlansBL.IsNewCollaborateBillingPlan(collaborateBillingPlan))
                            {
                                if (PlansBL.GetActiveUsersNr(model.objEditUser.objUser.Account, AppModule.Collaborate, Context) > collaborateBillingPlan.MaxUsers)
                                {
                                    dicModelErrors.Add("", Resources.Resources.lblMaxUsersLimit);
                                    model.objEditUser.LoggedAccountId = this.LoggedAccount.ID;
                                    TempData["editUserModel"] = model.objEditUser;
                                    TempData["ModelErrors"] = dicModelErrors;
                                    return RedirectToAction("AddEditUser",
                                        new { SelectedUser = model.objEditUser.objUser.ID });
                                }
                            }

                            ts.Complete();
                        }
                    }
                }
                else
                {
                    //Add New User
                    if (model.objNewUser != null)
                    {
                        model.objNewUser.AccountID = model.objNewUser.objUser.Account;
                        if (model.objNewUser.ModulePermisions != null)
                        {
                            model.objNewUser.InitializePermissions(
                                model.objNewUser.ModulePermisions.ToDictionary(o => o.ModuleType,
                                                                                           o => (int?)o.SelectedRole), Context);
                        }

                        if (!this.ValidateEmail(model.objNewUser.objUser.Account, model.objNewUser.objUser.EmailAddress, string.Empty, Context))
                        {
                            hasError = true;
                            dicModelErrors.Add("objNewUser.objUser.EmailAddress", Resources.Resources.reqEmailExist);
                        }
                        else if (model.objNewUser.ModulePermisions != null && (!model.objNewUser.ModulePermisions.Any() ||
                            model.objNewUser.ModulePermisions != null && model.objNewUser.ModulePermisions.Any(m => !m.Roles.Select(r => r.RoleId).Contains(m.SelectedRole))))
                        {
                            hasError = true;
                            dicModelErrors.Add("ValidPermissionsRequired", Resources.Resources.reqPermisionsRequired);
                        }

                        // Set user color
                        ProofStudioUserColor color = UserBL.ValidateProofStudioUserColor(LoggedAccount.ID, LoggedUser.ID, model.objNewUser.ProofStudioColor, Context);
                        if (color == null)
                        {
                            color = UserBL.GetAvailableUserColor(LoggedAccount.ID, Context);
                        }
                       
                        if (!hasError)
                        {
                            model.objNewUser.ProofStudioColorID = color.ID;
                            model.objNewUser.SelectedPrimaryGroup = SelectedPrimaryGroup.GetValueOrDefault();

                            hasError = !CreateNewUser(model.objNewUser, Context);
                            if (hasError)
                            {
                                dicModelErrors.Add("", Resources.Resources.lblMaxUsersLimit);
                            }
                        }

                        if (hasError)
                        {
                            model.objNewUser.LoggedAccountId = LoggedAccount.ID;
                            TempData["ModelErrors"] = dicModelErrors;
                            TempData["newUserModel"] = model.objNewUser;
                            return RedirectToAction("AddEditUser");
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();

                if (model.objEditUser != null)
                {
                    sb.Append(". Editing user { " + model.objEditUser.objUser.ID + " }");
                }
                if (model.objNewUser != null)
                {
                    sb.Append(". Adding user { " + model.objNewUser.objUser.FirstName + " " + model.objNewUser.objUser.LastName + " }");
                }

                sb.Append(" by user { " + LoggedUser.ID + " }");

                sb.Append(" on account { " + LoggedAccount.ID + " }.");

                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> EditUser --> UpdateUser" + sb.ToString(), ex);
            }
            finally
            {
            }

            return RedirectToAction("Users");
        }

        [HttpPost]
        public ActionResult DuplicateUser(DuplicateUser model)
        {
            if (ModelState.IsValid)
            {
                int sourceAccount = int.Parse(Session["SelectedAccId"].ToString());
                Account objAccount = Context.Accounts.FirstOrDefault(t => t.ID == sourceAccount);
                try
                {
                    if (model.DuplicatedUser > 0 &&
                    !DALUtils.IsFromCurrentAccount<GMGColorDAL.User>(model.DuplicatedUser, objAccount.ID, Context))
                    {
                        return RedirectToAction("Unauthorised", "Error");
                    }

                    Dictionary<string, string> errors = new Dictionary<string, string>();
                    if (ValidateEmail(objAccount, model.EmailAddress, string.Empty))
                    {
                        var isSsoUser = Context.Users.Find(model.DuplicatedUser).IsSsoUser;

                        GMGColorDAL.User duplicatedUser = UserBL.DuplicateUser(model, LoggedUser.ID, objAccount, isSsoUser, Context);
                        Context.SaveChanges();

                        if (!isSsoUser)
                        {
                            BaseBL.SendInviteToUser(duplicatedUser.ID, LoggedUser.ID, LoggedAccount.ID, Context);
                        }

                        NotificationServiceBL.CreateNotification(new UserWasAdded
                                                        {
                                                            EventCreator = LoggedUser.ID,
                                                            CreatedUserID = duplicatedUser.ID,
                                                            Account = objAccount.ID
                                                        },
                                                        objAccount.OwnerUser,
                                                        Context
                                                    );
                    }
                    else
                    {
                        errors.Add("EmailAddress", Resources.Resources.reqEmailExist);
                        TempData["DuplicateUserErrors"] = errors;
                        TempData["DuplicateUserModel"] = model;
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.Error(this.LoggedAccount.ID, "An error occured while duplicating user", ex);
                    return RedirectToAction("UnexpectedError", "Error");
                }
            }

            return RedirectToAction("Users");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "SendInvite")]
        public ActionResult SendInvite(EditAccount.Users model, string SelectedUser)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.objAccount.ID, Context);

                BaseBL.SendInviteToUser(SelectedUser.ToInt().GetValueOrDefault(), LoggedUser.ID, LoggedAccount.ID, Context);

                if (AccountStatu.GetAccountStatus(objAccount.ID, Context) == AccountStatu.Status.Draft)
                {
                    objAccount.Status = GMGColorDAL.AccountStatu.GetAccountStatus(
                                                GMGColorDAL.AccountStatu.Status.Inactive, Context).ID;

                    Context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> Users --> ReSendInvite", ex);
            }

            return RedirectToAction("Users");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "EditUser")]
        public ActionResult EditUser(string SelectedUser)
        {
            int userId = SelectedUser.ToInt().GetValueOrDefault();
            if (!GMGColorDAL.User.CanEditUser(SelectedUser.ToInt().GetValueOrDefault(), LoggedAccount.ID, Context))
            {
                return RedirectToAction("Unauthorised", "Error");
            }

            return RedirectToAction("AddEditUser", new { SelectedUser = userId });
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "DeleteUser")]
        public ActionResult DeleteUser(EditAccount.Users model, string SelectedUser)
        {
            try
            {
                if (!CanAccess(model.objAccount.ID, Context))
                {
                    return RedirectToAction("Unauthorised", "Error");
                }

                int userId = SelectedUser.ToInt().GetValueOrDefault();

                GMGColorDAL.User objUser = DALUtils.GetObject<GMGColorDAL.User>(userId, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    //objUser.IsDeleted = true;

                    objUser.Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Deleted, Context).ID;
                    objUser.NeedReLogin = true;


                    // mark all the workstations from this user as deleted
                    var workstations = (from ws in Context.Workstations where ws.User == userId select ws).ToList();
                    foreach (GMGColorDAL.Workstation workstation in workstations)
                    {
                        workstation.IsDeleted = true;
                    }

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "EditAccount --> DeleteUser", ex);
            }
            finally
            {
            }

            return RedirectToAction("Users");
        }

        #endregion

        #endregion

        #region Account Welcome

        [HttpPost]
        public ActionResult GetStarted(GMGColorDAL.Account model)
        {
            try
            {
                GMGColorDAL.Account objAcccount = DALUtils.GetObject<GMGColorDAL.Account>(model.ID, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAcccount.TimeZone = model.TimeZone;
                    objAcccount.DateFormat = model.DateFormat;
                    objAcccount.TimeFormat = model.TimeFormat;

                    Context.SaveChanges();
                    ts.Complete();

                    Session["gmg_swscr_" + this.LoggedAccount.ID] = null;
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error occured while saving Account welcome screen data", ex);
            }
            finally
            {
            }
            return RedirectToAction("Index", ((this.LoggedSite == Sites.AdministratorSite) ? "Dashboard" : "Studio"));
        }

        #endregion

        #region Account Support

        [HttpPost]
        public ActionResult AccountSupport(GMGColorDAL.CustomModels.AccountSupport model, FormCollection collection)
        {
            try
            {
                string emailContent = "<p>" + model.Message + "</p>";
                string toName = GMGColorConfiguration.AppConfiguration.EmailSystemFromName;  //this.LoggedAccount.objOwner.GivenName + " " + this.LoggedAccount.objOwner.FamilyName;
                string fromEmail = GMGColorConfiguration.AppConfiguration.EmailSystemFromAddress;  
                string toEmail = AccountSettingsBL.GetContactSupportEmail(LoggedAccount.ID); // this.LoggedAccount.objOwner.EmailAddress;

                List<string> fileNames = new List<string>();
                List<int> lstTmp = collection.AllKeys.Where(o => o.Contains("ApprovalTitle_")).Select(o => int.Parse(o.Split('_').Last())).ToList();
                foreach (int key in lstTmp)
                {
                    fileNames.AddRange((collection["ApprovalFileName_" + key] ?? string.Empty).Split(
                        new string[] { "," }, StringSplitOptions.None));
                }

                try
                {
                    var paths = fileNames.Select(f => Path.Combine(model.UserPathToTempFolder, f)).ToList();

                    NotificationServiceBL.CreateNotification(new GMGColorNotificationService.AccountSupport
                        {
                            EventCreator = LoggedUser.ID,
                            ToEmail = toEmail,
                            Message = emailContent + "<br/>" + String.Format(Resources.Resources.lblEmailContactSupportFrom, LoggedUser.GivenName + " " + LoggedUser.FamilyName, LoggedUser.EmailAddress),
                            ReceiverName = toName,
                            FromName = model.YourName,
                            FromEmail = fromEmail,
                            Subject = "Account Support",
                            Attachments = paths.ToArray()
                        },
                        LoggedUser,
                        Context);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, "Error when sending email from support module with attachments, error: {0}", ex.Message);
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error when sending email from support module, error: {0}", ex.Message);
            }
            Response.Redirect(Request.UrlReferrer.ToString());
            return View();
        }

        #endregion

        #region Methods

        private void ConfirmAccountSetup(int Account, bool sendInvite)
        {
            try
            {
                int ownerId = 0;
                string domain = string.Empty;

                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(Account, Context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objAccount.Status = sendInvite
                                            ? GMGColorDAL.AccountStatu.GetAccountStatus(
                                                GMGColorDAL.AccountStatu.Status.Inactive, Context).ID
                                            : GMGColorDAL.AccountStatu.GetAccountStatus(
                                                GMGColorDAL.AccountStatu.Status.Draft, Context).ID;

                    BillingPlan collaborateBillingPlan = objAccount.CollaborateBillingPlan(Context);                  
                    BillingPlan deliverBillingPlan = objAccount.DeliverBillingPlan(Context);

                    if (collaborateBillingPlan != null)
                    {
                        GMGColorDAL.AccountBillingPlanChangeLog objAccountCollaborateBillingPlanChangeLog =
                            new GMGColorDAL.AccountBillingPlanChangeLog();
                        objAccountCollaborateBillingPlanChangeLog.Account = objAccount.ID;
                        objAccountCollaborateBillingPlanChangeLog.CreatedDate = DateTime.UtcNow;
                        objAccountCollaborateBillingPlanChangeLog.Creator = this.LoggedUser.ID;
                        objAccountCollaborateBillingPlanChangeLog.LogMessage = string.Empty;
                        objAccountCollaborateBillingPlanChangeLog.PreviousPlanAmount = null;
                        objAccountCollaborateBillingPlanChangeLog.PreviousProofsAmount = null;
                        objAccountCollaborateBillingPlanChangeLog.GPPDiscount = null;
                        objAccountCollaborateBillingPlanChangeLog.PreviousPlan = null;
                        objAccountCollaborateBillingPlanChangeLog.CurrentPlan = collaborateBillingPlan.ID;
                        Context.AccountBillingPlanChangeLogs.Add(objAccountCollaborateBillingPlanChangeLog);
                    }                 

                    if (deliverBillingPlan != null)
                    {
                        GMGColorDAL.AccountBillingPlanChangeLog objAccountDeliverBillingPlanChangeLog =
                            new GMGColorDAL.AccountBillingPlanChangeLog();
                        objAccountDeliverBillingPlanChangeLog.Account = objAccount.ID;
                        objAccountDeliverBillingPlanChangeLog.CreatedDate = DateTime.UtcNow;
                        objAccountDeliverBillingPlanChangeLog.Creator = this.LoggedUser.ID;
                        objAccountDeliverBillingPlanChangeLog.LogMessage = string.Empty;
                        objAccountDeliverBillingPlanChangeLog.PreviousPlanAmount = null;
                        objAccountDeliverBillingPlanChangeLog.PreviousProofsAmount = null;
                        objAccountDeliverBillingPlanChangeLog.GPPDiscount = null;
                        objAccountDeliverBillingPlanChangeLog.PreviousPlan = null;
                        objAccountDeliverBillingPlanChangeLog.CurrentPlan = deliverBillingPlan.ID;
                        Context.AccountBillingPlanChangeLogs.Add(objAccountDeliverBillingPlanChangeLog);
                    }

                    if (objAccount.OwnerUser != null &&
                        !string.IsNullOrEmpty(objAccount.OwnerUser.EmailAddress))
                    {
                        string contactSupportEmailKey = Enum.GetName(typeof(AccountSettingsBL.AccountSettingsKeyEnum), AccountSettingsBL.AccountSettingsKeyEnum.ContactSupportEmail);
                        GMGColorDAL.AccountSetting objAccountSetting = new AccountSetting();

                        objAccountSetting.Account = objAccount.ID;
                        objAccountSetting.Name = contactSupportEmailKey;
                        objAccountSetting.Value = String.Empty;
                        objAccountSetting.ValueDataType = 1;
                        objAccountSetting.Description = contactSupportEmailKey;

                        Context.AccountSettings.Add(objAccountSetting);
                    }
                    
                    objAccount.IsTemporary = false;

                    Context.SaveChanges();
                    ts.Complete();

                    ownerId = objAccount.Owner.Value;
                    domain = objAccount.Domain;
                }

                if (sendInvite)
                {
                    this.SendInvite(ownerId, domain);
                }

                NotificationServiceBL.CreateNotification(new AccountWasAdded()
                                                            {
                                                                EventCreator = LoggedUser.ID,
                                                                AccountName = objAccount.Name
                                                            },
                                                            LoggedUser,
                                                            Context
                                                        );
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(this.LoggedAccount.ID, "Error creating account", ex);
            }
            finally
            {
            }
        }

        private void SendInvite(int userId, string accountUrl)
        {
            NotificationServiceBL.CreateNotification(new UserWelcome
                                                    {
                                                        EventCreator = LoggedUser.ID,
                                                        InternalRecipient = userId,
                                                        CreatedDate = DateTime.UtcNow
                                                    },
                                                    null,
                                                    Context
                                                    );
        }

        private bool CanAccess(int accountId, DbContextBL context)
        {
            try
            {
                return DALUtils.IsFromCurrentAccount<GMGColorDAL.Account>(accountId, LoggedAccount.ID, context);
            }
            finally
            {
            }
        } 

        private int SubscriberGetNumberOfRemainingCredits(int subscriberPlanId, int accountId, AppModule module)
        {
            int creditsSum = 0;
            foreach (GMGColorDAL.Account accountVar in this.LoggedAccount.GetChilds(Context))
            {
                if (GMGColorDAL.AccountType.GetAccountType(accountVar, Context) == GMGColorDAL.AccountType.Type.Subscriber)
                {
                    GMGColorDAL.SubscriberPlanInfo subscriberPlanInfo =
                        accountVar.SubscriberPlanInfoes.FirstOrDefault(
                            o => o.BillingPlan.BillingPlanType.AppModule1.Key == (int)module);

                    GMGColorDAL.BillingPlan subscriberBillingPlan = subscriberPlanInfo != null ? subscriberPlanInfo.BillingPlan : null;

                    if (
                        accountVar.ID != accountId &&  // ignore current account credit account
                        subscriberPlanInfo != null &&
                        subscriberBillingPlan != null &&
                        subscriberBillingPlan.ID == subscriberPlanId
                        )
                        creditsSum += subscriberPlanInfo.NrOfCredits;
                }
            }
            int totalCredits = 0;
            int? proofs;
            proofs = (from bp in Context.BillingPlans where bp.ID == subscriberPlanId select bp.Proofs).FirstOrDefault();
            if (proofs.HasValue)
            {
                totalCredits = proofs.Value;
            }

            return totalCredits - creditsSum;
        }

        private void PopulateProofStudioUserColors(ProofStudioUserColor color, int userID)
        {
            //Show color if logged account is admin
            if (LoggedAccountType == AccountType.Type.GMGColor || LoggedUserAdminRole == Role.RoleName.AccountAdministrator)
            {
                ViewBag.AvailableColors = UserBL.GetAvailableUserColors(userID, LoggedAccount.ID, Context);
                if (color != null)
                {
                    ViewBag.ProofStudioColor = color.HEXValue;
                }
            }
        }
        
        #endregion

        #region Trial Account

        public ContentResult AddTrialAccount(TrialAccount model, string token)
        {
            string result = null;
            if (string.IsNullOrEmpty(token))
            {
                result = TrialAccountMessages.NotSetSecurityToken;
            }
            else
            {
                if (token.ToLower() != GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
                {
                    result = TrialAccountMessages.InvalidSecurityToken;
                }
                bool containsScriptTags = Utils.ContainsScriptTags(new string[]
                                                               {
                                                                   model.Address1, model.Address2, model.City,
                                                                   model.CompanyName, model.Country, model.Email,
                                                                   model.FirstName, model.Language, model.LastName,
                                                                   model.Phone, model.Timezone
                                                               });
                if (string.IsNullOrEmpty(result) && ModelState.IsValid && !containsScriptTags)
                {
                    string companyName = Account.GetCompanyNameForTrialAccounts(model.CompanyName, Context);
                    string accountDomain = Account.GetAccountDomainForTrialAccounts(model.CompanyName, Context);

                    result = Account.CreateTrialAccount(model, Context, accountDomain, companyName);

                    if (string.IsNullOrEmpty(result))
                    {
                        var data = (from a in Context.Accounts
                                    join u in Context.Users on a.Owner equals u.ID
                                    where a.Domain == accountDomain
                                    select new { OwnerUser = u.ID, AccountName = a.Name }).FirstOrDefault();

                        if (data != null)
                        {
                            this.SendInvite(data.OwnerUser, accountDomain);

                            NotificationServiceBL.CreateNotification(new AccountWasAdded()
                                                            {
                                                                EventCreator = LoggedUser.ID,
                                                                AccountName = data.AccountName
                                                            },
                                                            LoggedUser,
                                                            Context
                                                        );
                        }
                        else
                        {
                            GMGColorDAL.GMGColorLogging.log.Error("Notification could not be send for domain " + accountDomain);
                            result = TrialAccountMessages.UnknowError;
                        }
                    }
                }
                else if (containsScriptTags)
                {
                    result = TrialAccountMessages.ScriptsTagsNotAllowed;
                }
                else if (!ModelState.IsValid)
                {
                    // get first error message from model state
                    result = ModelState.FirstOrDefault(o => o.Value.Errors.Any()).Value.Errors.FirstOrDefault().ErrorMessage;
                }
            }

            Response.StatusCode = string.IsNullOrEmpty(result) ? 201 : 400;
            return this.Content(result);
        }

        [HttpGet]
        public JsonResult GetTrialAccounts(string token)
        {
            string result = string.Empty;
            if (string.IsNullOrEmpty(token))
            {
                result = TrialAccountMessages.NotSetSecurityToken;
            }
            if (!string.IsNullOrEmpty(token) && token.ToLower() != GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                result = TrialAccountMessages.InvalidSecurityToken;
            }

            // if no error
            if (string.IsNullOrEmpty(result))
            {
                TrialAccounts model = new TrialAccounts();
                try
                {
                    model.Accounts = (from a in Context.Accounts
                                      join company in Context.Companies on a.ID equals company.Account
                                      join country in Context.Countries on company.Country equals country.ID
                                      join language in Context.Locales on a.Locale equals language.ID
                                      join u in Context.Users on a.Owner equals u.ID
                                      join accs in Context.AccountStatus on a.Status equals accs.ID
                                      where
                                          a.IsTrial == true && accs.Key == TrialAccountMessages.DefaultAccountStatusKey
                                      select
                                          new TrialAccount
                                              {
                                                  Address1 = company.Address1,
                                                  Address2 = company.Address2,
                                                  City = company.City,
                                                  CompanyName = company.Name,
                                                  Country = country.ShortName,
                                                  Email = company.Email,
                                                  FirstName = u.GivenName,
                                                  LastName = u.FamilyName,
                                                  Language = language.DisplayName,
                                                  Phone = company.Phone,
                                                  Timezone = a.TimeZone
                                              }).ToList();

                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    GMGColorDAL.GMGColorLogging.log.Error("AccountController.GetTrialAccounts -> " + ex.Message, ex);
                    result = TrialAccountMessages.UnknowError;
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}