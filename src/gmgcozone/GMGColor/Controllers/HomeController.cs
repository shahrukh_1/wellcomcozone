﻿using System.Web.Mvc;

using GMGColor.Common;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GMG.CoZone.Common.Interfaces;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using AppModule = GMG.CoZone.Common.AppModule;

namespace GMGColor.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IDownloadService downlaodService, IMapper mapper) : base(downlaodService, mapper) { }

        [HttpGet]
        public ActionResult Home()
        {
            return View(ViewNameByBrowserAgent("Home"), GetHomePageModel());
        }

        private Home GetHomePageModel()
        {
            Home model = new Home();
            model.TrainingVideos = new List<CMSVideoModel>();
            model.FeatureVideos = new List<CMSVideoModel>();
            model.Announcements = new List<CMSAnnouncementModel>();

            var folderPath = GMGColorCommon.GetDataFolderHttpPath(EnvironmentRegion, (from c in Context.Accounts
                                                                                      where c.ID == LoggedAccount.ID
                                                                                      select c.IsCustomDomainActive ? c.CustomDomain : c.Domain).
                                                                                      FirstOrDefault());

            if (CMSBL.isSysAdmin(LoggedAccount.AccountType))
            {
                model.TrainingVideos.AddRange(
                DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning == true, Context)
                .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = true, Title = o.VideoTitle, Description = o.VideoDescription }));
                
                model.FeatureVideos.AddRange(
                DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning == false, Context).
                Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = false, Title = o.VideoTitle, Description = o.VideoDescription }));

                model.Announcements.AddRange(
                DALUtils.SearchObjects<GMGColorDAL.CMSAnnouncement>(o => o.ID > 0, Context).Select(t => new CMSAnnouncementModel() { ID = t.ID, Content = t.Content }));
                model.Announcements = model.Announcements.OrderByDescending(o => o.ID).ToList();

                model.Manuals.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Manuals));
                model.Downloads.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Downloads));

                model.AbsoluteFilesPath = folderPath;
            }
            else
            {
                model.TrainingVideos.AddRange(
                DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning == true, Context)
                .Where(o => o.Account == LoggedAccount.ID ||  o.Account == null)
                .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = true, Title = o.VideoTitle, Description = o.VideoDescription }));

                model.FeatureVideos.AddRange(
                DALUtils.SearchObjects<GMGColorDAL.CMSVideo>(o => o.IsTrainning == false, Context)
                .Where(o => o.Account == LoggedAccount.ID || o.Account == null)
                .Select(o => new CMSVideoModel() { ID = o.ID, Url = o.VideoUrl, IsVideoTraining = false, Title = o.VideoTitle, Description = o.VideoDescription }));

                model.Announcements.AddRange(
                DALUtils.SearchObjects<GMGColorDAL.CMSAnnouncement>(o => o.ID > 0, Context)
                .Where(o => o.Account == LoggedAccount.ID || o.Account == null)
                .Select(t => new CMSAnnouncementModel() { ID = t.ID, Content = t.Content }));
                model.Announcements = model.Announcements.OrderByDescending(o => o.ID).ToList();

                model.Manuals.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Manuals).Where(o => o.Account == LoggedAccount.ID || o.Account == null));
                model.Downloads.AddRange(CMSBL.GetItems(Context, Enums.CMSItemsType.Downloads).Where(o => o.Account == LoggedAccount.ID || o.Account == null));

                model.AbsoluteFilesPath = folderPath; 
            }

            return model;
        }

    }
}
