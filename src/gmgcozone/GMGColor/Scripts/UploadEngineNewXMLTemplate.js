﻿function AddNewCustomField(key, errToManyChars, errNodeNameRequired, errXPathRequired) {
    'use strict';

    var html =
            "<tr id='CustomField-{key}'>\n\
                <td class='span5'>\n\
                    <label id='defaultNodeName_{key}' class='defaultNodeName' name='SelectedCustomFields[{key}].DefaultXMLTemplateNodeName' />\n\
                    <input type='hidden' id='defaultNodeID_{key}' name='SelectedCustomFields[{key}].DefaultXMLTemplateID' />\n\
                    <input data-val='false' id='customFieldIsDeleted_{key}' name='SelectedCustomFields[{key}].IsDeleted' type='hidden' value='false' class='pull-left'>\n\
                </td>\n\
                <td class='span8'>\n\
                    <input data-val='true' name='SelectedCustomFields[{key}].XPath' id='XPath_{key}' class='span7' type='text' data-val-length='{errToManyChars}' data-val-length-max='128' data-val-required='{errXPathRequired}'>\n\
                    <span class='field-validation-valid' data-valmsg-for='SelectedCustomFields[{key}].XPath' data-valmsg-replace='true'></span>\n\
                </td>\n\
                <td class='span1'>\n\
                    <button ' class='btn btn-danger pull-right' id='delete-CustomField-{key}' onclick='return false;' ><i class='icon-white icon-trash'></i></button>\n\
                </td>\n\
                <div class='clearboth'></div>\n\
        </tr>\n";

    html = html.replace(/{errToManyChars}/g, errToManyChars);
    html = html.replace(/{errNodeNameRequired}/g, errNodeNameRequired);
    html = html.replace(/{errXPathRequired}/g, errXPathRequired);
    html = html.replace(/{key}/g, key);

    $("#XMLCustomFieldsTable > tbody").append(html);

    $('#defaultNodeName_' + key).text($('#SelectionDropdown').find("option:selected").text());
    $('#defaultNodeID_' + key).val($('#SelectionDropdown :selected').val());

    // Fills the XPath input with the current selected Job Field
    $('#XPath_' + key).val($("#XPathDropdown option[value='" + $('#SelectionDropdown :selected').val() + "']").text());

    // Removes the new added field from the selection dropdown 
    $("#SelectionDropdown option:selected").remove();

    //If all nodes are added then hide the add button and selection dropdown
    if($('#SelectionDropdown option').length == 0) {
         $('#btnNewCustomField').hide();
         $('#SelectionDropdown').hide();
    }

    //Bind delete event
    $('#delete-CustomField-' + key).click(function () {
        deleteRow(key);
    });
}

function deleteRow(key) {

    //Add the deleted option to the selection dropdown to make it available again
    $('#SelectionDropdown').append($('<option>', {
        value: $('#defaultNodeID_' + key).val(),
        text: $('#defaultNodeName_' + key).text()
    }));

    //Mark the field as deleted
    $('#customFieldIsDeleted_' + key).val('true');
    $('#CustomField-' + key).hide();

    if($('#btnNewCustomField').is(':hidden')) {
        $('#btnNewCustomField').show();
    }

     if($('#SelectionDropdown').is(':hidden')) {
        $('#SelectionDropdown').show();
    }


    return false;
}

function saveCompleted(data) {

    var json_data = eval('(' + eval('(' + data.responseText + ')') + ')');

    if (json_data.Success) {
        $('form').dirtyForms('setClean');
        window.location.href = "/Settings/UploadEngineTemplates?type=XMLTemplates";
    } else {

        $("#btnXMLTemplatesSaveChanges").removeClass('disabled');
        $("#btnXMLTemplatesSaveChanges").removeAttr('disabled');
        $("#btnXMLTemplatesSaveChanges").text($("#btnXMLTemplatesSaveChanges").attr('data-text'));

        //Clear previous errors
        $("input[name^='SelectedCustomFields']").removeClass('input-validation-error');
        $("span[data-valmsg-for^='SelectedCustomFields']").removeClass('field-validation-error');
        $("span[data-valmsg-for^='SelectedCustomFields']").addClass('field-validation-valid');
        $("span[data-valmsg-for^='SelectedCustomFields']").html("");

        //Check for errors and bind them to the inputs
        var errors = new Array;
        for (var key in json_data.Errors) {
            if (key == '') {
                for (var i = 0; i < json_data.Errors[key].length; i++) {
                    errors[errors.length] = "<li>" + json_data.Errors[key][i] + "</li>";
                }
            } else {
                $("input[name='" + key + "']").addClass('input-validation-error');
                $("span[data-valmsg-for='" + key + "']").removeClass('field-validation-valid');
                $("span[data-valmsg-for='" + key + "']").addClass('field-validation-error');

                $("span[data-valmsg-for='" + key + "']").html("<span for=\"" + key + "\" generated=\"true\" class=\"\">" + json_data.Errors[key][0] + "</span>");
            }
        }

        //Display General Validation errors that don't belong to a certain field
        $(".validation-summary-errors").remove();
        $("#XMLCustomFieldsTable").before("<div class=\"validation-summary-errors\" data-valmsg-summary=\"false\"><ul>" + errors.join("\n") + "</ul></div>");
    }
}

function GenerateNewCustomFieldKey() {
    return $("#XMLCustomFieldsTable tbody tr").length.toString();
}

