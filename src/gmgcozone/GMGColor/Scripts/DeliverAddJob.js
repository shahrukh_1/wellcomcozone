﻿$('#SelectedCPServerId').on('change', function (event, initialLoad) {
    if ($(this).val() != "") {
        var currentTextValue = $("#SelectedCPWorkflowId option[value='']").text();
        $("#SelectedCPWorkflowId option[value='']").text($('#ldgLbl').val());
        $.ajax({
            url: $('#getWorkflowsAction').val(),
            type: 'POST',
            data: "{'SelectedCPServerId':" + $(this).val() + "}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                if (typeof (result.workflows) != 'undefined') {
                    $("#SelectedCPWorkflowId option[value!='']").remove();
                    for (var i = 0; i < result.workflows.length; i++) {
                        $("#SelectedCPWorkflowId").append("<option value=\"" + result.workflows[i].ID + "\" proofstandard=\"" + result.workflows[i].ProofStandard + "\" SpotColors=\"" + result.workflows[i].SpotColors + "\" MaxPaperWidth=\"" + result.workflows[i].MaxPaperWidth + "\" >" + result.workflows[i].Name + "</option>");
                    }
                    DeliverFillData(null);
                    var cpDetails = $.cookie("LastSelectedCPDetails");
                    if (cpDetails != null) {
                        var arr = cpDetails.split("|");
                        if (arr.length > 1) {
                            $("#SelectedCPWorkflowId").val(arr[1]);
                            $("#SelectedCPWorkflowId").trigger('change');
                        } else {
                            var firstValue = $("#SelectedCPWorkflowId option:not([value='']):first").val();
                            $("#SelectedCPWorkflowId").val(firstValue);

                        }
                    } else {
                        var firstValue = $("#SelectedCPWorkflowId option:not([value='']):first").val();
                        $("#SelectedCPWorkflowId").val(firstValue);
                    }
                    $("#SelectedCPWorkflowId").trigger('change');
                }
                $("#SelectedCPWorkflowId option[value='']").text(currentTextValue);

                removeUnobstrusiveValidations();
                $("#SelectedCPWorkflowId").blur();

                if (typeof(initialLoad) != 'undefined' && typeof($.DirtyForms) != 'undefined') {
                    $('form').dirtyForms('setClean');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#SelectedCPWorkflowId option[value!='']").remove();
                DeliverFillData(null);
                $("#SelectedCPWorkflowId option[value='']").text(currentTextValue);
            }
        });
    }
    else {
        $("#SelectedCPWorkflowId option[value!='']").each(function () {
            $(this).remove();
        });
    }
});

function DeliverFillData(selectedOption) {
    if (typeof (selectedOption) == 'undefined' || selectedOption == null) {
        $("#SpotColors").html('&nbsp;');
        $("#ProofStandard").html("&nbsp;");
        $("#MaxPaperWidth").html("&nbsp;");
    } else {
        $("#ProofStandard").html("&nbsp;&nbsp;" + selectedOption.attr("proofstandard"));
        $("#SpotColors").html(selectedOption.attr("SpotColors"));
        $("#MaxPaperWidth").html("&nbsp;&nbsp;" + selectedOption.attr("MaxPaperWidth"));
    }
}
function ResetDeliverModel() {
    $("table.table-upload").find("td.delete button.btn-danger").click(function () {
        $(this).parents("tr:first").remove();
    });
    $("table.table-upload").find("td.delete button.btn-danger").trigger('click');

    DeliverFillData(null);
    $("#Pages_Single").click();

    $("#SelectedCPWorkflowId").find("option[value!='']").remove();

    // reset validators for fileValidator and fileTitleValidator
    $("span[data-valmsg-for='fileValidator']").removeClass('span.field-validation-error');
    $("span[data-valmsg-for='fileValidator']").text('');
    $("span[data-valmsg-for='fileTitleValidator']").removeClass('span.field-validation-error');
    $("span[data-valmsg-for='fileTitleValidator']").text('');

    $("#modalDialogSubmitToDeliver .modal-body .validation-summary-errors").remove();

    ResetModelCustom();
}
$("#SelectedCPWorkflowId").on('change', function () {
    var selectedValue = $("#SelectedCPWorkflowId").val();
    if (selectedValue.length > 0) {
        var currentItem = $("#SelectedCPWorkflowId option[value='" + $("#SelectedCPWorkflowId").val() + "']");
        DeliverFillData(currentItem);
    }
    else
        DeliverFillData(null);
});
$("input[name='PagesRadioButton']").change(function () {
    if ($(this).attr("id") == "Pages_Range") {
        $("#PagesRangeValue").removeAttr("readonly");
        $("#PagesRangeValue").focus();
        $("#PagesRangeHiddenField").val("");
    }
    else {
        $("#PagesRangeValue").val('');
        $("#PagesRangeValue").attr("readonly", "readonly");
        $("#PagesRangeHiddenField").val("no required");
        $("#PagesRangeValue").blur();
        $("#PagesRangeMaxPageCountHiddenField").val("no-required");
    }
    checkPageRange();
});
$("#PagesRangeValue").change(function () {
    $("#PagesRangeHiddenField").val($(this).val());
    checkPageRange();
});
$(document).ready(function () {
    $("input[name='PagesRadioButton']").change();
});

$(document).bind('fileuploaddone', callbackfuncadd);

$(document).bind('fileuploaddestroy', callbackfuncdelete);

function callbackfuncdelete(e, data) {

    // remove the sibling hidden row of the deleted row: each upload item has two rows (one for error and one for success) and only one is visible
    if ($(data.context).hasClass('displayerror')) {
        $(data.context).next().remove();
    } else if ($(data.context).hasClass('displaysuccess')) {
        $(data.context).prev().remove();
    }
    
    var files = $('#tblUpload1 tr').length;
    if (files == 1) {
        $('#btnAddDeliver').attr('disabled', 'disabled');
    }
   
    setTimeout(function () {
        checkPageRange();
    }, 500);
}

function callbackfuncadd(e, data) {
    setTimeout(function () {
        checkPageRange();
    }, 500);
}

function checkPageRange() {
    var lblPagesCount = $("#lblPagesCountInfoResource").val();
    var selectedApprovalPagesCount = $("#SelectedApprovalsPageCounts").val();

    if (selectedApprovalPagesCount === "") {
        var pagesNr = window.localStorage.getItem("SelectedApprovalsPageCounts");
        $("#SelectedApprovalsPageCounts").val(pagesNr);

        //clear local session
        window.localStorage.removeItem("SelectedApprovalsPageCounts");
    }

    $("#PagesRangeMaxPageCountHiddenField").val("no-required");

    $("span[data-valmsg-for='PagesRangeMaxPageCountHiddenField']").removeClass("field-validation-error");
    $("span[data-valmsg-for='PagesRangeMaxPageCountHiddenField']").find("span").remove();
    $("span[data-valmsg-for='PagesRangeMaxPageCountHiddenField']").addClass("field-validation-valid");
    $("#lblMaxPagesCount").text("");

    var isDeliverPage = $("#dvUploader").is(":visible"); // page can be approval or deliver

    var isSingleFile = isDeliverPage
        ? ($("input[name^='PdfPagesCount_']").length == 1)
        : ($("#SelectedApprovalsPageCounts").val().split(",").length == 1);

    var pageCounts = 0;
    if (isSingleFile) {
        if (isDeliverPage == true) {
            pageCounts = parseInt($("input[name^='PdfPagesCount_']:first").attr("maxpage"), 10);
        } else {
            pageCounts = parseInt($("#SelectedApprovalsPageCounts").val(), 10);
        }
    }

    if (pageCounts > 1) {
        $('#Pages_Range').removeAttr('disabled');
        $('#Pages_Single').removeAttr('disabled');
    } else {
        $('#Pages_Range').attr('disabled','disabled');
        $('#Pages_Single').attr('disabled', 'disabled');
        $('#PagesRangeValue').attr('readonly', 'readonly');
        $('#Pages_All').attr('checked', 'checked');
        $('#PagesRangeValue').val('');
    }

    if ($('#Pages_Range').is(":checked") && pageCounts > 0) {
        var i = 0, j = 0;
        var pages = new Array();
        var groups = $("input[name='PagesRangeValue']").val().split(",");
        for (i = 0; i < groups.length; i++) {
            var groups2 = groups[i].split("-");
            for (j = 0; j < groups2.length; j++) {
                pages[pages.length] = parseInt(groups2[j], 10);
            }
        }
        for (i = 0; i < pages.length; i++) { // if at least one page is greater than the max page count then show the activate the validation
            if (pages[i] > pageCounts) {
                $("#PagesRangeMaxPageCountHiddenField").val("");
                break;
            }
        }
        
        $("#lblMaxPagesCount").text(lblPagesCount.replace("{0}", pageCounts));
    }
}
$(document).ready(function () {
    $("#btnAddDeliver").mouseenter(function () {
        ValidateFileTitles();
    });
    $("#btnAddDeliver").focus(function () {
        ValidateFileTitles();
    });

    $('#SelectedCPServerId').trigger('change');

    $("#btnAddDeliver").click(function () {
        SaveDefaultSelections();
    });
    $("#AddDeliverJob").click(function () {
        SaveDefaultSelections();
    });
    $("#btnSubmitToDeliver").click(function () {
        LoadDefaultSelections();
    });
    LoadDefaultSelections(true);
});

function LoadDefaultSelections(initialLoad) {
    var cpDetails = $.cookie("LastSelectedCPDetails");
    if (cpDetails != null) {
        var arr = cpDetails.split("|");
        if (arr.length > 0 && $("#SelectedCPServerId option[value='" + arr[0] + "']").length > 0) {
            $("#SelectedCPServerId").val(arr[0]);
            $("#SelectedCPServerId").trigger('change', initialLoad);
        } else {
            var firstValue = $("#SelectedCPServerId option[value!='']:first").val();
            $("#SelectedCPServerId").val(firstValue);
            $("#SelectedCPServerId").trigger('change', initialLoad);
        }
    } else {
        var firstValue = $("#SelectedCPServerId option[value!='']:first").val();
        $("#SelectedCPServerId").val(firstValue);
        $("#SelectedCPServerId").trigger('change', initialLoad);
    }
}
function SaveDefaultSelections() {
    var details = "";
    if ($("#SelectedCPServerId").val() != "") {
        details = $("#SelectedCPServerId").val();
    }
    if ($("#SelectedCPWorkflowId").val() != null) {
        if (details.length > 0)
            details += "|";
        details += $("#SelectedCPWorkflowId").val();
    }
    $.cookie("LastSelectedCPDetails", details, { secure: false });
}

$('#AddDeliverJob').on('click', function () {
    if ($('div .block input[type=text].first-name').val() != undefined && ($('div .block input[type=text].first-name').val() != "" || $('div .block input[type=text].last-name').val() != "" || $('div .block input[type=text].email').val() != "")) {
        $('#errSelectExternalUser').show();

        $("#imgSavingLoader").hide();

        setTimeout(function () {
            $('button[data-loading-text]').button('reset');
        }, 0);
        return false;
    }
    return true;
});