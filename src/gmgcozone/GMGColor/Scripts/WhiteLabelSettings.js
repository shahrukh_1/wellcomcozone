﻿function UpdateDisableLandingPageFields() {
    if (!$("#objWhiteLabelSettings_DisableLandingPage").is(":checked")) {
        $(":radio[name='objWhiteLabelSettings.DashboardToShow']:first").attr("checked", "checked");
        $(":radio[name='objWhiteLabelSettings.DashboardToShow']").attr("disabled", "disabled");
    } else {
        $(":radio[name='objWhiteLabelSettings.DashboardToShow']").removeAttr("disabled", "disabled");
    }
}
$(document).ready(function () {
    UpdateDisableLandingPageFields();
});
$("#objWhiteLabelSettings_DisableLandingPage").change(function () {
    UpdateDisableLandingPageFields();
});