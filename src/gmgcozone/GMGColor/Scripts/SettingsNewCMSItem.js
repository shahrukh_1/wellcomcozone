﻿function AddNewCMSItem(parentId, key, errCMSItemToManyChars, errCMSItemRequired, fileType) {
    'use strict';
    var html =
            "<tr id='CMSItemRow-{key}'>\n\
                <td>\n\
                    <div id='fileUpload_{key}' class='fileupload CMSItemFileUploader' key='{key}' max-file-size='2199552000000' use-dropzone='False' acceptfiletypes='/({fileType})$/i' CMSItem='{key}'>\n\
                        <a id='hidbutton_{key}' class='fileinput-button btn-link' value='Browse' onclick=\"$('#btnUpload_{key}').click();\">Browse</a>\n\
                        <input data-val='false' id='hdnFileName_{key}' name='Items[{key}].FileName' type='hidden' value=''>\n\
                        <input data-val='false' id='hdnFileIsDeleted_{key}' name='Items[{key}].IsDeleted' type='hidden' value='false' class='pull-left'>\n\
                        <input id='btnUpload_{key}' type='file' class='hide'>\n\
                        <div id='tblUpload{key}'></div>\n\
                    </div>\n\
                    <span class='field-validation-valid' data-valmsg-for='Items[{key}].FileName' data-valmsg-replace='true'></span>\n\
                </td>\n\
                <td>\n\
                    <input data-val='true' name='Items[{key}].Name' id='CMSItemName_{key}' type='text' data-val-length='{errCMSItemToManyChars}' data-val-length-max='120' data-val-required='{errCMSItemRequired}'>\n\
                    <span class='field-validation-valid' data-valmsg-for='Items[{key}].Name' data-valmsg-replace='true'></span>\n\
                </td>\n\
                <td>\n\
                    <input data-val='true' name='Items[{key}].Description' id='Description_{key}' type='text' data-val-length='{errCMSItemToManyChars}' data-val-length-max='256' data-val-required='{errCMSItemRequired}'>\n\
                    <span class='field-validation-valid' data-valmsg-for='Items[{key}].Description' data-valmsg-replace='true'></span>\n\
                </td>\n\
                <td>\n\
                    <button ' class='btn btn-danger' id='delete-CMSItem-{key}' onclick='return false;' ><i class='icon-white icon-trash'></i></button>\n\
                </td>\n\
                <div class='clearboth'></div>\n\
        </tr>\n";

    html = html.replace(/{errCMSItemToManyChars}/g, errCMSItemToManyChars);
    html = html.replace(/{errCMSItemRequired}/g, errCMSItemRequired);
    html = html.replace(/{fileType}/g, fileType);
    html = html.replace(/{key}/g, key);

    $("#CMSItemTable > tbody").append(html);

    $('#fileUpload_' + key).fileupload({
        acceptFileTypes: eval($('#fileUpload_' + key).attr('acceptFileTypes')),
        maxFileSize: parseInt($('#fileUpload_' + key).attr('max-file-size')),
        uploadTemplateId: null,
        downloadTemplateId: null,
        autoUpload: true,
        dropZone: ($(this).attr('use-dropzone') == 'true') ? $('#dropzone_' + $(this).attr('key')) : $(this),
        maxNumberOfFiles: 1
    });

    $('#fileUpload_' + key).bind('fileuploaddone', function (e, data) {
        $('#hidbutton_' + key).addClass('hide');
        $('#hidbutton_' + key).attr('disabled', 'disabled');
        $("span[data-valmsg-for^='Items[" + key + "].FileName']").hide();
    });

    $("#btnUpload_" + key).change(function () {
        $("#tblUpload" + key).text($("#hdnFileName_" + key).val().substring(1));
        $("#hidbutton_" + key).addClass('hide');
        
        if (!$('tr:has(td.start)')) {
           $('#CMSItemTable tbody tr td.name').css({'max-width': 260,
                                                    'overflow': 'hidden',
                                                    'text-overflow': 'ellipsis'
                                                    }); 
        }
        CheckForErrorsOrSuccessUpload(key);
    });

    $('#delete-CMSItem-' + key).click(function () {
        deleteRow(key);
    });

    $('td').change(function () {
        $(this).css({ 'max-width': 260,
            'overflow': 'hidden',
            'text-overflow': 'ellipsis'
        });
    });
}
function deleteRow(key) {
    if ($('#delete-CMSItem-' + key + ' i.icon-white').hasClass('icon-trash')) {
        $('#tblUpload' + key + ' td.delete .btn:first').click();
        $('#fileUpload_' + key).addClass('hide');
        $('#hdnFileIsDeleted_' + key).val('true');
        $('#CMSItemRow-' + key).hide();
        $("input[name='Items[" + key + "].Name']").val("deleted, not required");
        $("input[name='Items[" + key + "].FileName']").val("deleted, not required");

    }
    else if ($('#delete-CMSItem' + key + ' i.icon-white').hasClass('icon-ban-circle')) {
        $('#tblUpload' + key + ' td.cancel .btn:first').click();
        $('#hidbutton_' + key).removeAttr('disabled');
        $('#hidbutton_' + key).removeClass('hide');
    }
    return false;
}

function saveCompleted(data) {
    var json_data = eval('(' + eval('(' + data.responseText + ')') + ')');

    if (json_data.Success) {
        $('form').dirtyForms('setClean');
        window.location.reload();
    } else {

        $("#btnCMSItemSaveChanges").removeClass('disabled');
        $("#btnCMSItemSaveChanges").removeAttr('disabled');
        $("#btnCMSItemSaveChanges").text($("#btnCMSItemSaveChanges").attr('data-text'));

        $("input[name^=']").removeClass('input-validation-error');
        $("input[name^=']']").removeClass('input-validation-error');
        $("span[data-valmsg-for^='[']").removeClass('field-validation-error');
        $("span[data-valmsg-for^='[']").addClass('field-validation-valid');
        $("span[data-valmsg-for^='[']").html("");

        var errors = new Array;
        for (var key in json_data.Errors) {
            if (key == '') {
                for (var i = 0; i < json_data.Errors[key].length; i++) {
                    errors[errors.length] = "<li>" + json_data.Errors[key][i] + "</li>";
                }
            } else {
                $("input[name='" + key + "']").addClass('input-validation-error');
                $("span[data-valmsg-for='" + key + "']").removeClass('field-validation-valid');
                $("span[data-valmsg-for='" + key + "']").addClass('field-validation-error');

                $("span[data-valmsg-for='" + key + "']").html("<span for=\"" + key + "\" generated=\"true\" class=\"\">" + json_data.Errors[key][0] + "</span>");
            }
        }

    }

}

function GenerateNewCMSItemKey() {
    return $('#CMSItemTable tbody tr[id^="CMSItemRow-"]').length.toString();
}

function CheckForErrorsOrSuccessUpload(key) {
    if (!$('#tblUpload' + key + ' td.cancel').is(':visible')) {
        setTimeout('CheckForErrorsOrSuccessUpload(' + key + ');', 100);
    }
    $('#tblUpload' + key + ' tbody tr td.name').css({ 'max-width': 140,
                                                       'overflow': 'hidden',
                                                       'text-overflow': 'ellipsis',
                                                    });
    $('#tblUpload' + key + ' td.cancel button').click(function () {
        $("#hidbutton_" + key).removeClass('hide');
    });
}
