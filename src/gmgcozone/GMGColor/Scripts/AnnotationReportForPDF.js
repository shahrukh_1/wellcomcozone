﻿var DISPLAY_CANVAS_WIDTH = 1100;

var PRINT_MAXHEIGHT = 150;

var PREVIEW_MAX_WIDTH = 650;
var PREVIEW_MAX_HEIGHT = 650;

var BALLOON_MARGIN_LEFT_RIGTH = 15;
var BALLOON_MARGIN_TOP_BOTTOM = 20;
var PREVIEW_MARGIN_LEFT = 230;

var PREVIEW_TOP_BOTTOM_MARGIN = 100;

var MAX_BALLOON_LINES_NUMBER = 3;
var MAX_BALLOON_WIDTH = 200;
var MAX_BALLOON_COMMENT_CHARS = 45; 
var MAX_BALLOON_USER_CHARS = 60;
var MAX_BALLOON_PHASE_CHARS = 50;
var MAX_BALLOON_TIME_CHARS = 50;
var BALLLON_INITIAL_SIZE = { width: 200, height: 50 };
var BALLOON_INTERNAL_PADDING = 5;
var BALLOON_BACKGROUND_COLOR = '#FFFFFF';//'#FFD479';
var BALLOONS_VERTICAL_SPACING = 15;

var _gFabricCanvasLst = [];
var _gCanvasImgPreviews = [];
var _gCheckReadyForPrintInterval = null;
var _gAnnotationSVGsToLoad = 0;

var _imgToBase64Lst = [];
var _approvalIdLst = [];
var _annotationIDs = [];
var approvalAnnotaionIndex = [];

var _gReportType = "";
var REPORT_TYPE_PINS = "Pins";
var REPORT_TYPE_COMMENTS = "Comments";
var REPORT_TYPE_COMMENTS_AND_PINS = "CommentsAndPins";

function printReport() {
    PrintButtonSetEnabled(false);
    LoadImgPreviews(true);

    _gCheckReadyForPrintInterval = setInterval(function () { GetReport(); }, 2000);
}

function GetPreviewLeftRigthMargin() {
    return MAX_BALLOON_WIDTH + 2 * BALLOON_MARGIN_LEFT_RIGTH;
}

function CanvasPreview() {
    this.Canvas = null;
    this.Balloons = null;

    this.BalloonRowLef = {
        left: BALLOON_MARGIN_LEFT_RIGTH,
        top: BALLOON_MARGIN_TOP_BOTTOM
    };

    this.BalloonRowRight = {
        left: 0, // to be set with GetBallonRowRightFromLeft when canvas width is available
        top: BALLOON_MARGIN_TOP_BOTTOM
    };

    this.AnnotationIDs = null;
}

function GetBallonRowRightFromLeft(canvasWidth) {
    return canvasWidth - MAX_BALLOON_WIDTH - 25;
}

function GetPreviewXPosition(previewWidth, canvasWidth) {
    return (canvasWidth - previewWidth) / 2;
}

function GetPreviewYPosition(canvasHeight, previewHeight) {
    return (canvasHeight - previewHeight) / 2;
}

function GetPreviewWidth(canvasElement) {
    return canvasElement.getAttribute('data-previewidth');
}

function GetPreviewHeight(canvasElement) {
    return canvasElement.getAttribute('data-previewheight');
}

function LoadPreviewsOnCanvas() {

    var deferreds = [];

    $("canvas[id^='canvas_']").each(function () {
        deferreds.push(LoadPreviews(this));
    });

    // use deffered sync to wait for all images to be loaded before starting to draw the annotations
    $.when.apply($, deferreds).promise().done(function () {

        var annotationsIds = [];
        var approvalsIds = [];

        //get only the annotations of the current approval
        $("canvas[id^='canvas_']").each(function () {
            annotationsIds.push($(this).attr('data-annotations'));
            approvalsIds.push($(this).attr('approval'));

            var objRes = approvalAnnotaionIndex.find(obj => {
                return obj.ApprovalId === $(this).attr('approval')
            })

            if (objRes == undefined) {
                approvalAnnotaionIndex.push({
                    "ApprovalId": $(this).attr('approval'),
                    "cnt": 0
                });
            }
        });
        if (annotationsIds.length > 0 && approvalsIds.length > 0) {
            LoadPreviewAnnotations(annotationsIds, approvalsIds);
            ResizeCanvasesToFixedWidth();
        }
    });
}

function ResizeCanvasesToFixedWidth() {
    for (var i = 0; i < _gFabricCanvasLst.length; i++) {
        var canvas = _gFabricCanvasLst[i].Canvas;
        var DISPLAY_CANVAS_WIDTH1 = 825;
        var canvasId = canvas.lowerCanvasEl.id;
        var canvasPlaceholder = canvasId.replace("canvas_", "canvas_placeholder_");
        var heightWidthRatio = canvas.getHeight() / canvas.getWidth();

        var displayCanvasHeight = Math.round(DISPLAY_CANVAS_WIDTH * heightWidthRatio);
        var displayCanvasHeight1 = Math.round(DISPLAY_CANVAS_WIDTH1 * heightWidthRatio);

        var width = '' + DISPLAY_CANVAS_WIDTH + 'px';
        var width1 = '' + DISPLAY_CANVAS_WIDTH1 + 'px';
        var height1 = '' + displayCanvasHeight1 + 'px';
        var height = '' + displayCanvasHeight + 'px';
        
        if (canvas.getWidth() > 1200 && canvas.getHeight() > 500) {
            $('#' + canvasId).css('width', width1);
            $('#' + canvasId).css('height', height1);

            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('width', width1);
            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('height', height1);

            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('width', width1);
            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('height', height1);
        }
        else if (canvas.getWidth() > 1200 && canvas.getHeight() < 500) {
            $('#' + canvasId).css('width', width1);
            $('#' + canvasId).css('height', height1);

            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('width', width1);
            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('height', 625);

            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('width', width1);
            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('height', 625);
        }
        else if (canvas.getWidth() < 1200 && canvas.getHeight() < 500) {
            $('#' + canvasId).css('width', width);
            $('#' + canvasId).css('height', height);

            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('width', width);
            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('height', 625);

            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('width', width);
            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('height', 625);
        }
        else {
            $('#' + canvasId).css('width', width);
            $('#' + canvasId).css('height', height);


            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('width', width);
            $('#' + canvasPlaceholder + ' .canvas-preview .canvas-container').css('height', height);

            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('width', width);
            $('#' + canvasPlaceholder + ' .canvas-preview .upper-canvas').css('height', height);
        }
    }
}

// Load the image previews
function LoadPreviews(context) {
    var deffered = new $.Deferred();

    var canvas = new fabric.Canvas($('#' + context.id).get(0), { stateful: false, selection: false, renderOnAddRemove: false });
    //Save canvases references to use later when drawing annotations
    var canvasPreview = new CanvasPreview();
    canvasPreview.Canvas = canvas;
    _gFabricCanvasLst.push(canvasPreview);

    //create list with all annotations Ids to send on ajax request
    var canvasAnnotationIDs = $('#' + canvas.lowerCanvasEl.id).attr("data-annotations");
    canvasPreview.AnnotationIDs = canvasAnnotationIDs.split(',');
    canvasPreview.ApprovalId = $('#' + canvas.lowerCanvasEl.id).attr("approval");

    _annotationIDs.push(canvasAnnotationIDs);
    var previewUrl = context.getAttribute('data-preview');

    var img = new Image();
    img.src = previewUrl;

    img.onload = function () {
        var canvasWidth = img.width + 2 * GetPreviewLeftRigthMargin();
        canvasPreview.BalloonRowRight.left = GetBallonRowRightFromLeft(canvasWidth);

        var canvasHeight = img.height + 2 * PREVIEW_TOP_BOTTOM_MARGIN;

        var imgWidth = img.width;
        var imgHeight = img.height;

        canvas.setWidth(canvasWidth);
        canvas.setHeight(canvasHeight);
                
        // set preview height and width values
        $('#' + canvas.lowerCanvasEl.id).attr("data-previewidth", imgWidth);
        $('#' + canvas.lowerCanvasEl.id).attr("data-previewheight", imgHeight);

        var imgCanvas = document.createElement('canvas');
        var ctx = imgCanvas.getContext("2d");

        imgCanvas.width = imgWidth;
        imgCanvas.height = imgHeight;
        ctx.drawImage(img, 0, 0, imgWidth, imgHeight);
        
        var previewSrc = imgCanvas.toDataURL();

        canvas.setBackgroundImage(previewSrc, canvas.renderAll.bind(canvas), {
            // Needed to position backgroundImage at 0/0
            originX: 'left',
            originY: 'top',
            events: false,
            //left: GetPreviewXPosition(imgWidth, canvasWidth),
            top: GetPreviewYPosition(canvasHeight, imgHeight),
            width: imgWidth,
            height: imgHeight,
            crossOrigin: 'anonymous'
        });

        _gCanvasImgPreviews[canvas.lowerCanvasEl.id] = 0;

        deffered.resolve();
    };
    return deffered;
}

function LoadPreviewAnnotations(annotationsIds) {
    var allAjaxCalls = LoadPreviewAnnotationsInGroups(annotationsIds, 25);
    $.when.apply(this, allAjaxCalls).done(function () {
        try {
            var annotations;
            if (arguments[0][0]) {
                var dataobject = arguments[0][0];
                annotations = dataobject;
                if (dataobject.error === true) {
                    throw true;
                }
                for (var i = 1; i < arguments.length; i++) {
                    var anotherDataObject = arguments[i][0];
                    annotations.Annotations = annotations.Annotations.concat(anotherDataObject.Annotations);
                    if (anotherDataObject.error === true) {
                        throw true;
                    }
                }
            } else {
                var dataobjectSingleRequest = arguments[0];
                annotations = dataobjectSingleRequest;
                if (dataobjectSingleRequest.error === true) {
                    throw true;
                }
            }
            DrawAnnotations(annotations);
        } catch (e) {
            $('#errLoad').show();
        }
    }).fail(function (xhr, textStatus, errorThrown) {
        $('#errLoad').show();
    });
}

function LoadPreviewAnnotationsInGroups(annotationsIds, groupSize) {
    var annotationsAsStr = "" + annotationsIds;
    var annotations = annotationsAsStr.split(',');

    var noOfGroups = Math.floor(annotations.length / groupSize);

    var ajaxCalls = [];
    var startIndex;

    for (var i = 0; i < noOfGroups; i++) {
        startIndex = i * groupSize;
        var annotationsToLoad = annotations.slice(startIndex, startIndex + groupSize);
        ajaxCalls.push(LoadPreviewAnnotationsOnGroup(annotationsToLoad, annotationsIds));
    }

    startIndex = (noOfGroups) * groupSize;

    if (startIndex < annotations.length) {
        var annotationsToLoadLastGroup = annotations.slice(startIndex, annotations.length);
        ajaxCalls.push(LoadPreviewAnnotationsOnGroup(annotationsToLoadLastGroup, annotationsIds));
    }

    return ajaxCalls;
}

function LoadPreviewAnnotationsOnGroup(groupAnnotationsIds, allAnnotationsIds) {
    _gReportType = $('#report-type').val();
    var urlAction = $('#data-annotations-url').val();
    var model = {
        ReportType: _gReportType,
        GroupedAnnotations: groupAnnotationsIds,
        AllAnnotations: allAnnotationsIds[0].split(',')
    };

    return $.ajax({
        url: urlAction,
        type: "POST",
        data: model,
        dataType: "JSON",
        cache: false,
    });

}


function PrintButtonSetEnabled(enable) {
    if (enable == true) {
        $('#printBtnpdf').removeProp('disabled');
        $('#btngeneratepdf').removeProp('disabled');
        $('#saveAsJPGpdf').removeProp('disabled');

    } else {
        $('#printBtnpdf').prop('disabled', 'disabled');
        $('#btngeneratepdf').prop('disabled', 'disabled');
        $('#saveAsJPGpdf').prop('disabled', 'disabled');
    }
}

function PrintButtonSetState() {
    $("#imgSavingLoader").hide();

    setTimeout(function () {
        $('button[data-loading-text]').button('reset');
    }, 0);
}

// Export canvas to image and update the corresponding img element
function LoadImgPreviews(isPDFReport) {
    var canvasId;
    for (var i = 0; i < _gFabricCanvasLst.length; i++) {
        canvasId = _gFabricCanvasLst[i].Canvas.lowerCanvasEl.id;
        var identifier = canvasId.substr(canvasId.indexOf("_") + 1);
        $('#img_' + identifier).attr('src', _gFabricCanvasLst[i].Canvas.toDataURL({ format: 'png' }));

        if (isPDFReport) {
            _imgToBase64Lst[i] = $('#img_' + identifier).attr('src').replace(/^data:image\/(png|jpg);base64,/, "");
        } else {
            _imgToBase64Lst[i] = $('#img_' + identifier).attr('src').replace(/^data:image\/[^;]/, "data:application/octet-stream");
        }

        //add approval id of each image preview
        _approvalIdLst.push(parseInt($('#img_' + identifier)[0].attributes.approval.value, 10));

        var canvasId = 'canvas_' + $('#img_' + identifier)[0].id.replace('img_', '');
        _gCanvasImgPreviews[canvasId] = 1;
    }
}

// Checks whether the document is ready to be printed (all preview images were are exported from canvas to img)
function GetReport() {
    var allImagesAreLoaded = true;
    $.each(_gCanvasImgPreviews, function (index, value) {
        if (value == 0) {
            allImagesAreLoaded = false;
            return false;
        }
    });

    if (allImagesAreLoaded == true) {
        // The document is ready for printing. Stop calling the check function
        window.clearInterval(_gCheckReadyForPrintInterval);

        PrintButtonSetEnabled(true);

        var pagesNumbers = [];
        var approvalPageIds = [];
        $("canvas[data-page]").each(function () {
            pagesNumbers.push($(this).attr("data-page"));
            approvalPageIds.push($(this).attr("data-pageID"))
        });

        var showSummary = $('#summary-page-check').val();
        $.ajax({
            url: '/Approvals/GenerateAnnotationPDFReport',
            data: { imagesToBase64: _imgToBase64Lst, approvalsIDs: _approvalIdLst, showSummaryPage: showSummary, pageNumbers: pagesNumbers, annotationIDs: _annotationIDs, approvalPageIds: approvalPageIds, __RequestVerificationToken: $("#formAnnotationsForPdf input").val() },
            type: 'POST',
            traditional: true,
            success: function (retData) {
                var approvalsCnt = 0;
                var ID = $("div[id^='approval_']")[0].id;
                for (var i = 0; i < $("div[id^='approval_']").length; i++)
                {
                    approvalsCnt = ($("div[id^='approval_']")[i].id == ID) ? 1 : $("div[id^='approval_']").length;
                }
                var jobTitle = $('#default-file-name').val();

                //if there is only one approval on page, then the name of the printed document will be the same as job name
                //otherwise the file name will be 'Annotation Report.pdf'
                if (approvalsCnt == 1) {
                    jobTitle = $("div[id^='approval_']").attr('job-name');
                }

                var blob = b64toBlob(retData, "application/pdf");
                saveAs(blob, jobTitle);

                PrintButtonSetState();
            },
            error: function (jqXHR) {
                if (jqXHR.readyState == 0 || jqXHR.status == 0) {
                    return;
                }
                else {
                    $('#errGeneratePdf').show();
                }
            }
        });
        _imgToBase64Lst.length = 0;
    }
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}

function DrawAnnotations(annotations) {
    var pinSVGTemplate = annotations.PinSVG;
    _gAnnotationSVGsToLoad = annotations.Annotations.length;

    SetAnnotationToCanvas(annotations);

    for (var i = 0; i < _gFabricCanvasLst.length; i++) {

        var canvasPreview = _gFabricCanvasLst[i];
        var previewWidth = $('#' + canvasPreview.Canvas.lowerCanvasEl.id)[0].getAttribute('data-previewidth');
        var previewHeight = $('#' + canvasPreview.Canvas.lowerCanvasEl.id)[0].getAttribute('data-previewheight');
        var previewXPos = GetPreviewXPosition(previewWidth, canvasPreview.Canvas.getWidth());
        var previewYPos = GetPreviewYPosition(canvasPreview.Canvas.getHeight(), previewHeight);

        if (_gReportType == REPORT_TYPE_COMMENTS) {
            SortAnnotationsByPosition(canvasPreview);
        }

        $.each(canvasPreview.Annotations, function (index, value) {
            var adjustScaleForHighResolution = value.ImageWidth / value.AnnotatedOnImageWidth;

            var scale = previewHeight / value.ImageHeight;
            scale = scale * adjustScaleForHighResolution;

            //var left = value.PositionX * scale + previewXPos;
            var top = value.PositionY * scale + previewYPos;
            var left = value.PositionX * scale;
            
            if (value.CommentType != "marker") { // Free drawing or shape annotation
                var opacity = 1;
                if (value.CommentType == "text") {
                    opacity = 0.3;
                }
                value.SVG.forEach(function (svg) {
                    if (_gReportType == REPORT_TYPE_COMMENTS || _gReportType == REPORT_TYPE_COMMENTS_AND_PINS) {
                        RenderSVG(canvasPreview.Canvas, svg, previewXPos - PREVIEW_MARGIN_LEFT, previewYPos, scale, opacity, true);
                    }
                });
            } else {// marker annoatation
                var X = left - 5;
                var Y = top - 15;
                if (_gReportType == REPORT_TYPE_COMMENTS) {
                    RenderMarker(canvasPreview.Canvas, X, Y, 1, true);
                }
            }

            if (_gReportType == REPORT_TYPE_PINS || _gReportType == REPORT_TYPE_COMMENTS_AND_PINS) {
                RenderPin(canvasPreview.Canvas, pinSVGTemplate, left, top, scale, value.Index);
            }

            // Balloon position
            var position = null;
            var isLeftPositioned = false;
           
            if(index <= canvasPreview.AnnotationIDs.length)
                {
                position = {
                    left: canvasPreview.BalloonRowRight.left,
                    top: canvasPreview.BalloonRowRight.top
                };
        }
        
            // Marker position
            var markerPosition = {
                left: value.PositionX * scale + previewXPos,
                top: value.PositionY * scale + previewYPos
            };

           
        });
    };
}

function RenderPin(canvas, svg, left, top, scale, index) {

    var annotationSvg = new String(svg);
    var svgScale = 0;
    annotationSvg = annotationSvg.replace("#index", index);

    if (index > 9) {
        annotationSvg = annotationSvg.replace('<g transform="translate(50 100)"', '<g transform="translate(30 100)"');
    }
    if (1100 <= canvas.width && canvas.width <= 1300) {
        svgScale = 0.25;
    } else if(1300 < canvas.width && canvas.width < 1500) {
        svgScale = 0.35;
    } else if (canvas.width >= 1500){
        svgScale = 0.45;
    } else {
        svgScale = 0.15;
    }
    RenderSVG(canvas, annotationSvg, left, top, svgScale);
}

function RenderMarker(canvas, left, top, scale, bringToBack) {
    var fontSize20 = GetFontSizeForCanvasDefault20(canvas);
    var marker = new fabric.Text("+", {
        lockMovementX: true,
        lockMovementY: true,
        hasRotatingPoint: false,
        hasControls: false,
        hasBorders: false,
        selectable: false,
        scaleX: scale,
        scaleY: scale,
        originX: 'left',
        originY: 'top',
        left: left,
        top: top,
        fontSize: fontSize20,
        fill: "#000000"
    });

    canvas.add(marker).renderAll();
    canvas.moveTo(marker, 0);

    _gAnnotationSVGsToLoad--;

    if (_gAnnotationSVGsToLoad == 0) {
        // enable Print button
        PrintButtonSetEnabled(true);
    }
    if (typeof (bringToBack) != "undefined" && bringToBack == true) {
        canvas.sendToBack(marker);
    } else {
        canvas.bringToFront(marker);
    }
}

function RenderSVG(canvas, svg, left, top, scale, opacity, bringToBack) {
    if (typeof (opacity) == "undefined")
        opacity = 1;

    fabric.loadSVGFromString(svg, function (objects, options) {
        var loadedObject = fabric.util.groupSVGElements(objects, options);
            if (loadedObject) {
                loadedObject.set({
                    lockMovementX: true,
                    lockMovementY: true,
                    hasRotatingPoint: false,
                    hasControls: false,
                    hasBorders: false,
                    selectable: false,
                    scaleX: scale,
                    scaleY: scale,
                    originX: 'left',
                    originY: 'top',
                    left: left,
                    top: top,
                    opacity: opacity
                }).setCoords();
                
            canvas.add(loadedObject).renderAll();
            canvas.moveTo(loadedObject, 0);

            _gAnnotationSVGsToLoad--;

            if (_gAnnotationSVGsToLoad == 0) {
                // enable Print button
                PrintButtonSetEnabled(true);
            }

            if (typeof (bringToBack) != "undefined" && bringToBack == true) {
                canvas.sendToBack(loadedObject);
            } else {
                canvas.bringToFront(loadedObject);
            }
        }
    });
}

function GetCanvasFromAnnotationId(annotationId) {
    var canvasId;
    //Get the right canvas for the annotation based on annotationId
    for (var i = 0; i < _gFabricCanvasLst.length; i++) {
        canvasId = _gFabricCanvasLst[i].Canvas.lowerCanvasEl.id;
        var canvasAnnot = $('#' + canvasId).attr("data-annotations").split(',');
        if (canvasAnnot.indexOf(annotationId.toString()) > -1) {
            return _gFabricCanvasLst[i];
        }
    }
}

function BreakLine(text, maxCharsInLine, maxLines) {
    if (typeof (maxLines) == 'undefined') {
        maxLines = MAX_BALLOON_LINES_NUMBER;
    }
    var charsInLine = 0, currentLine = 0, builder = "";
    for (var i = 0; i < text.length; i++) {
        if (currentLine <= maxLines) {
            if (text[i] == " ") {
                var nextWordLength = GetNextWordLength(text, i + 1);

                if (charsInLine + nextWordLength >= maxCharsInLine) {
                    builder += "\n";
                    charsInLine = 0;
                    currentLine++;
                } else {
                    builder += text[i];
                    charsInLine++;
                }
            } else if (charsInLine >= maxCharsInLine - 1) { // single words that exceeds the maximum number of characters per line
                builder += text[i];
                builder += "\n";
                charsInLine = 0;
                currentLine++;
            }
            else {
                builder += text[i];
                charsInLine++;
            }
        } else {
            builder = builder.substring(0, i - 3);
            builder += "...";
            i = text.length - 1;
        }
    }
    return builder;
}

function GetNextWordLength(text, index) {
    var wordLength = 0;
    for (var i = index; i < text.length; i++) {
        if (text[i] == " ")
            break;
        wordLength++;
    }
    return wordLength;
}

function GetDisplayCanvasWidthAndCanvasWidthRatio(canvas) {
    var canvasWidth = canvas.getWidth();
    return canvasWidth / DISPLAY_CANVAS_WIDTH;
}

function GetFontSizeForCanvasDefault12(canvas) {
    return Math.round(12 * GetDisplayCanvasWidthAndCanvasWidthRatio(canvas));
}
function GetFontSizeForCanvasDefault10(canvas) {
    return Math.round(10 * GetDisplayCanvasWidthAndCanvasWidthRatio(canvas));
}
function GetFontSizeForCanvasDefault20(canvas) {
    return Math.round(20 * GetDisplayCanvasWidthAndCanvasWidthRatio(canvas));
}
function GetFontSizeForCanvasDefault16(canvas) {
    return Math.round(14 * GetDisplayCanvasWidthAndCanvasWidthRatio(canvas));
}
function RenderAnnotationBalloon(canvas, annotation, backgroundColor, position, size, markerPosition) {
    var fontSize = {
        fontSize12: GetFontSizeForCanvasDefault12(canvas),
        fontSize10: GetFontSizeForCanvasDefault10(canvas),
        fontSize16: GetFontSizeForCanvasDefault16(canvas)
    }
    var width, height;
    if (typeof (size) == "undefined" || size == null) {
        width = BALLLON_INITIAL_SIZE.width;
        height = BALLLON_INITIAL_SIZE.height;
    } else {
        width = size.width;
        height = size.height;
    }

    if (backgroundColor == null || backgroundColor == '' || typeof (backgroundColor) == 'undefined') {
        backgroundColor = BALLOON_BACKGROUND_COLOR;
    }

    var padding = BALLOON_INTERNAL_PADDING;

    var annotationObjects = [];
    //create coordinates that will be updated for each ballon added in the comment box
    var currentItemCoord = { width: width, left: padding, top: padding };

    //create annotation fabric obj
    CreateFabricObj(annotation, currentItemCoord, padding, false, annotationObjects, fontSize);

    //create replies fabric obj
    for (var i = 0; i < annotation.Replies.length; i++) {
        CreateFabricObjForReplies(annotation.Replies[i], currentItemCoord, padding, true, annotationObjects, fontSize);
    }

    if (annotation.AnnotationAttachment != null) {
        //Create annotation attachment fabric obj
        for (var i = 0; i < annotation.AnnotationAttachment.length; i++) {
            CreateFabricObjForAttachment(annotation.AnnotationAttachment[i], currentItemCoord, padding, i, annotationObjects, fontSize);
        }
    }

    if (annotation.AnnotationChecklist != null) {
        //Create annotation checklist fabric obj
        for (var i = 0; i < annotation.AnnotationChecklist.length; i++) {
            CreateFabricObjForChecklist(annotation.AnnotationChecklist[i], currentItemCoord, padding, i, annotationObjects, fontSize);
        }
    }

    //create rect container for all anootation and replies text objects
    var rect = new fabric.Rect({
        left: 0,
        top: 0,
        fill: backgroundColor,
        width: currentItemCoord.width,
        height: currentItemCoord.top,
        stroke: "gainsboro",
        selectable: true,
        hasRotatingPoint: false,
        hasControls: false
    });

    annotationObjects.unshift(rect);

    //create fabric group for annotation including replies
    var group = new fabric.Group(annotationObjects, {
        left: position.left,
        top: position.top,
        hasRotatingPoint: false,
        hasControls: false,
        hasBorders: false,
        lockMovementX: false,
        lockMovementY: false,
        originX: "left",
        originY: "top",
        lockScalingX: true,
        lockScalingY: true,
        selectable: true,
        perPixelTargetFind: true,
        targetFindTolerance: 0,
        markerPosition: markerPosition
    });

    var isLeftColumn = position.left + width / 2 < markerPosition.left;
    if (_gReportType == REPORT_TYPE_COMMENTS) {
        var line = new fabric.Line([markerPosition.left, markerPosition.top, position.left + (isLeftColumn ? width : 0), position.top + height / 2], {
            fill: 'black',
            stroke: 'black',
            strokeWidth: 1,
            selectable: false,
            perPixelTargetFind: true,
            hasRotatingPoint: false,
            hasControls: false,
            targetFindTolerance: 0
        });
        group.line = line;
    }

    AdjustAnnotationBallonPosition(group, canvas);

    canvas.on('object:selected', function (e) {
        var activeObject = e.target;
        canvas.bringToFront(activeObject);
        if (activeObject.line != null) {
            canvas.bringToFront(activeObject.line);
        }
    });

    canvas.on('object:moving', function (e) {
        var activeObject = e.target;

        // don't allow baloons to leave canvas area
        AdjustAnnotationBallonPosition(activeObject, canvas);
        _gCanvasImgPreviews[canvas.lowerCanvasEl.id] = 0;
    });

    if (_gReportType == REPORT_TYPE_COMMENTS) {
        canvas.add(group).add(line).renderAll();
    } else {
        canvas.add(group).renderAll();
    }

    var baloonSize = new Object();
    baloonSize.Width = group.currentWidth;
    baloonSize.Height = group.currentHeight;

    return baloonSize;
}

function SetAnnotationToCanvas(annotations) {

    $.each(annotations.Annotations, function (index, item) {
        var canvasPreview = GetCanvasFromAnnotationId(item.ID);
        if (canvasPreview["Annotations"] == undefined) {
            canvasPreview["Annotations"] = new Array();
        }
        // keep the index returned from the server
        var objRes = approvalAnnotaionIndex.find(obj => {
            return obj.ApprovalId === canvasPreview["ApprovalId"]
        })
        objRes.cnt += 1;

        item["Index"] = objRes.cnt;
        canvasPreview.Annotations.push(item);
    });
}

function SortAnnotationsByPosition(canvasPreview) {
    //sort by X position first
    canvasPreview.Annotations.sort(function (a, b) { return a.PositionX - b.PositionX; });

    //split the annotations array into two arrays (one for the left column and one for the right columns)
    var halfLength = Math.ceil(canvasPreview.Annotations.length / 2);

    var rightSideAnnotations = canvasPreview.Annotations.splice(halfLength, canvasPreview.Annotations.length);

    //concatenate the sorted arrays (left and right) with the left array first
    canvasPreview.Annotations.sort(function (a, b) { return a.PositionY - b.PositionY; });
    rightSideAnnotations.sort(function (a, b) { return a.PositionY - b.PositionY; });
    canvasPreview.Annotations.push.apply(canvasPreview.Annotations, rightSideAnnotations);
}

function AdjustAnnotationBallonPosition(activeObject, canvas) {
    if (activeObject.getLeft() < 1)
        activeObject.setLeft(1);
    if (activeObject.getLeft() + activeObject.currentWidth > canvas.getWidth()) {
        activeObject.setLeft(canvas.getWidth() - activeObject.currentWidth - 1);
    }
    if (activeObject.getTop() + activeObject.currentHeight > canvas.getHeight()) {
        activeObject.setTop(canvas.getHeight() - activeObject.currentHeight - 1);
    }
    if (activeObject.getTop() < 1)
        activeObject.setTop(1);

    if (activeObject && activeObject.line != null && typeof (activeObject.line) != "undefined") {
        var isLeftColumn = activeObject.get('left') + BALLLON_INITIAL_SIZE.width / 2 < activeObject.markerPosition.left;
        activeObject.line.set({ x2: activeObject.get('left') + (isLeftColumn ? activeObject.get('width') : 0), y2: activeObject.get('top') + activeObject.get('height') / 2 });
    }
}

function CreateFabricObj(annotation, coordinates, padding, isReply, annotationObjects, fontSize) {
    // replace new lines with space
    var commentText = annotation.Comment.trim();
    commentText = commentText.replace(/(\r\n|\n|\r)/gm, ' ').replace(/(<([^>]+)>)/ig, '');
    //balloon header settings
    var headerLineText = isReply ? annotation.CreatorDisplayName.toUpperCase() : annotation.Index + " - " + annotation.CreatorDisplayName.toUpperCase();
    var headerLineLength = headerLineText.length;
    if (headerLineText > MAX_BALLOON_USER_CHARS) {
        var lines = BreakLine(headerLineText, MAX_BALLOON_USER_CHARS).split("\n");
        headerLineText = lines[0] + ' ...';
    }
    else {
        for (var i = headerLineLength; i < MAX_BALLOON_USER_CHARS; i++)
        {
            headerLineText = headerLineText + " ";  
        }
    }

    //phase name settings
    var displayPhase = annotation.PhaseName != null && annotation.PhaseName.length > 0;
    var phaseName = "";
    if (displayPhase) {
        if (annotation.PhaseName.length > MAX_BALLOON_PHASE_CHARS) {
            var phaseLines = BreakLine(annotation.PhaseName.trim(), MAX_BALLOON_PHASE_CHARS).split("\n");
            phaseName = phaseLines[0] + ' ...';
        } else {
            phaseName = annotation.PhaseName.trim();
        }
    }
    
    if (displayPhase) {
        var phaseNameWithDate = phaseName + " - " + annotation.AnnotatedDate;
        var phaseNameWithDateLength = phaseNameWithDate.length;
        if (phaseNameWithDateLength > MAX_BALLOON_TIME_CHARS) {
            var phaseLines = BreakLine(phaseNameWithDate, MAX_BALLOON_TIME_CHARS).split("\n");
            phaseNameWithDate = phaseLines[0] + ' ...';
        }
        else {
            for (var i = phaseNameWithDateLength; i < MAX_BALLOON_TIME_CHARS + 2; i++) {
                phaseNameWithDate = phaseNameWithDate + " ";
            }
        }
    }
    else {
        var AnnotatedDate = annotation.AnnotatedDate;
        var annotatedDateLength = AnnotatedDate.length;
        if (AnnotatedDate > MAX_BALLOON_TIME_CHARS) {
            var lines = BreakLine(AnnotatedDate, MAX_BALLOON_TIME_CHARS).split("\n");
            AnnotatedDate = lines[0] + ' ...';
        }
        else {
            for (var i = annotatedDateLength; i < MAX_BALLOON_TIME_CHARS + 9; i++) {
                AnnotatedDate = AnnotatedDate + " ";
            }
        }
    }

    //add header line text
    var headerLine = new fabric.IText(headerLineText, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: padding, top: coordinates.top });
    //add phase line text
    if (displayPhase) {
        var phaseLine = new fabric.IText(phaseNameWithDate, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: padding + 10, top: coordinates.top + headerLine.get('height') });
    }
    //add annotated date line text
    var annotationDateLineTop = null;
    var annotationDateLine = null;
    var commentBoxTop = null;
    if (displayPhase) {
        annotationDateLineTop = coordinates.top + headerLine.get('height') + (displayPhase ? phaseLine.get('height') : 0);
        commentBoxTop = annotationDateLineTop;
    }
    else {
        annotationDateLineTop = coordinates.top + headerLine.get('height');
        annotationDateLine = new fabric.IText(AnnotatedDate, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: coordinates.left + 10, top: annotationDateLineTop });
        commentBoxTop = annotationDateLineTop + annotationDateLine.get('height');
    }

    if (!displayPhase && (annotationDateLine.get('width') + 2 * padding > coordinates.width)) {
        coordinates.width = annotationDateLine.get('width') + 2 * padding;
    }
    if (displayPhase && (phaseLine.get('width') + 2 * padding > coordinates.width)) {
        coordinates.width = phaseLine.get('width') + 2 * padding;
    }
    if (headerLine.get('width') + 2 * padding > coordinates.width) {
        coordinates.width = headerLine.get('width') + 2 * padding;
    }
    coordinates.top = padding + commentBoxTop;

    annotationObjects.push(headerLine);
    if (displayPhase) {
        annotationObjects.push(phaseLine);
    }
    if (displayPhase == false) {
        annotationObjects.push(annotationDateLine);
    }
    var rect = new fabric.Rect({
        left: 0,
        top: 0,
        fill: "gainsboro",
        width: coordinates.width,
        height: coordinates.top,
        stroke: "gainsboro",
        selectable: true,
        hasRotatingPoint: false,
        hasControls: false
    });

    annotationObjects.unshift(rect);
    var commentBox = new fabric.IText(commentText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop + 5 });
    if (commentBox.get('width') > MAX_BALLOON_WIDTH) {
        commentText = BreakLine(commentText, MAX_BALLOON_COMMENT_CHARS, Number.MAX_VALUE);
        commentBox = new fabric.IText(commentText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop + 5 });
    }

    //adjust ballon width
    if (commentBox.get('width') + 2 * padding > coordinates.width) {
        coordinates.width = commentBox.get('width') + 2 * padding;
    }
   
    coordinates.top = padding + commentBoxTop + commentBox.get('height');
    annotationObjects.push(commentBox);
}


function CreateFabricObjForReplies(annotation, coordinates, padding, isReply, annotationObjects, fontSize) {
    // replace new lines with space
    var commentText = annotation.Comment.trim();
    commentText = commentText.replace(/(\r\n|\n|\r)/gm, ' ').replace(/(<([^>]+)>)/ig, '');
    //balloon header settings
    var headerLineText = isReply ? annotation.CreatorDisplayName.toUpperCase() : annotation.Index + " - " + annotation.CreatorDisplayName.toUpperCase();
    var headerLineLength = headerLineText.length;
    if (headerLineText > MAX_BALLOON_USER_CHARS) {
        var lines = BreakLine(headerLineText, MAX_BALLOON_USER_CHARS).split("\n");
        headerLineText = lines[0] + ' ...';
    }
    else {
        for (var i = headerLineLength; i < MAX_BALLOON_USER_CHARS; i++) {
            headerLineText = headerLineText + " ";
        }
    }

    //phase name settings
    var displayPhase = annotation.PhaseName != null && annotation.PhaseName.length > 0;
    var phaseName = "";
    if (displayPhase) {
        if (annotation.PhaseName.length > MAX_BALLOON_PHASE_CHARS) {
            var phaseLines = BreakLine(annotation.PhaseName.trim(), MAX_BALLOON_PHASE_CHARS).split("\n");
            phaseName = phaseLines[0] + ' ...';
        } else {
            phaseName = annotation.PhaseName.trim();
        }
    }

    if (displayPhase) {
        var phaseNameWithDate = phaseName + " - " + annotation.AnnotatedDate;
        var phaseNameWithDateLength = phaseNameWithDate.length;
        if (phaseNameWithDateLength > MAX_BALLOON_TIME_CHARS) {
            var phaseLines = BreakLine(phaseNameWithDate, MAX_BALLOON_TIME_CHARS).split("\n");
            phaseNameWithDate = phaseLines[0] + ' ...';
        }
        else {
            for (var i = phaseNameWithDateLength; i < MAX_BALLOON_TIME_CHARS + 2; i++) {
                phaseNameWithDate = phaseNameWithDate + " ";
            }
        }
    }
    else {
        var AnnotatedDate = annotation.AnnotatedDate;
        var annotatedDateLength = AnnotatedDate.length;
        if (AnnotatedDate > MAX_BALLOON_TIME_CHARS) {
            var lines = BreakLine(AnnotatedDate, MAX_BALLOON_TIME_CHARS).split("\n");
            AnnotatedDate = lines[0] + ' ...';
        }
        else {
            for (var i = annotatedDateLength; i < MAX_BALLOON_TIME_CHARS + 9; i++) {
                AnnotatedDate = AnnotatedDate + " ";
            }
        }
    }

    //add header line text
    var headerLine = new fabric.IText(headerLineText, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: padding + 10, top: coordinates.top });
    //add phase line text
    if (displayPhase) {
        var phaseLine = new fabric.IText(phaseNameWithDate, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: padding + 10, top: coordinates.top + headerLine.get('height') });
    }
    //add annotated date line text
    var annotationDateLineTop = null;
    var annotationDateLine = null;
    var commentBoxTop = null;
    if (displayPhase) {
        annotationDateLineTop = coordinates.top + headerLine.get('height') + (displayPhase ? phaseLine.get('height') : 0);
        commentBoxTop = annotationDateLineTop;
    }
    else {
        annotationDateLineTop = coordinates.top + headerLine.get('height');
        annotationDateLine = new fabric.IText(AnnotatedDate, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: coordinates.left + 10, top: annotationDateLineTop });
        commentBoxTop = annotationDateLineTop + annotationDateLine.get('height');
    }
    //add comment lines text(wrap if necessary)
    var commentBox = new fabric.IText(commentText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop });
    if (commentBox.get('width') > MAX_BALLOON_WIDTH) {
        commentText = BreakLine(commentText, MAX_BALLOON_COMMENT_CHARS, Number.MAX_VALUE);
        commentBox = new fabric.IText(commentText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop });
    }

    //adjust ballon width
    if (commentBox.get('width') + 2 * padding > coordinates.width) {
        coordinates.width = commentBox.get('width') + 2 * padding;
    }
    if (!displayPhase && (annotationDateLine.get('width') + 2 * padding > coordinates.width)) {
        coordinates.width = annotationDateLine.get('width') + 2 * padding;
    }
    if (displayPhase && (phaseLine.get('width') + 2 * padding > coordinates.width)) {
        coordinates.width = phaseLine.get('width') + 2 * padding;
    }
    if (headerLine.get('width') + 2 * padding > coordinates.width) {
        coordinates.width = headerLine.get('width') + 2 * padding;
    }

    coordinates.top = padding + commentBoxTop + commentBox.get('height');

    annotationObjects.push(headerLine);
    if (displayPhase) {
        annotationObjects.push(phaseLine);
    }
    if (displayPhase == false) {
        annotationObjects.push(annotationDateLine);
    }
    annotationObjects.push(commentBox);
}


// Create Fabric object for Attachments
function CreateFabricObjForAttachment(annotation, coordinates, padding, i, annotationObjects, fontSize) {

    var AttachmentText = (i + 1) + "." + annotation.trim();

    if (i == 0) {
        var AttachmentHead = i == 0;
        var headerLineText = "ATTACHMENTS:"
        var headerLine = new fabric.IText(headerLineText, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: padding + 10, top: coordinates.top });
    }

    var commentBoxTop = coordinates.top + (i == 0 ? headerLine.get('height') : -3);
    var commentBox = new fabric.IText(AttachmentText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop });
    if (commentBox.get('width') > MAX_BALLOON_WIDTH) {
        AttachmentText = BreakLine(AttachmentText, MAX_BALLOON_COMMENT_CHARS, Number.MAX_VALUE);
        commentBox = new fabric.IText(AttachmentText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop });
    }

    //adjust ballon width
    if (commentBox.get('width') + 2 * padding > coordinates.width) {
        coordinates.width = commentBox.get('width') + 2 * padding;
    }

    if (i == 0) {
        if (headerLine.get('width') + 2 * padding > coordinates.width) {
            coordinates.width = headerLine.get('width') + 2 * padding;
        }
    }
    coordinates.top = padding + commentBoxTop + commentBox.get('height');
    if (i == 0) {
        annotationObjects.push(headerLine);
    }
    annotationObjects.push(commentBox);

}

// Create Fabric object for Checklist
function CreateFabricObjForChecklist(annotation, coordinates, padding, i, annotationObjects, fontSize) {


    var a = annotation.ChecklistItem + ": " + annotation.TextValue;
    var ChecklistText = a.trim();

    if (i == 0) {

        var headerLineText = "CHECKLIST: " + annotation.Checklist;
        var headerLine = new fabric.IText(headerLineText, { fontSize: fontSize.fontSize16, fontFamily: "Helvetica", fontWeight: "Bold", left: padding + 10, top: coordinates.top });
    }

    var commentBoxTop = coordinates.top + (i == 0 ? headerLine.get('height') : -3);
    var commentBox = new fabric.IText(ChecklistText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop });
    if (commentBox.get('width') > MAX_BALLOON_WIDTH) {
        ChecklistText = BreakLine(ChecklistText, MAX_BALLOON_COMMENT_CHARS, Number.MAX_VALUE);
        commentBox = new fabric.IText(ChecklistText, { fontSize: fontSize.fontSize12, fontFamily: "Helvetica", left: padding + 10, top: commentBoxTop });
    }

    //adjust ballon width
    if (commentBox.get('width') + 2 * padding > coordinates.width) {
        coordinates.width = commentBox.get('width') + 2 * padding;
    }

    if (i == 0) {
        if (headerLine.get('width') + 2 * padding > coordinates.width) {
            coordinates.width = headerLine.get('width') + 2 * padding;
        }
    }
    coordinates.top = padding + commentBoxTop + commentBox.get('height');
    if (i == 0) {
        annotationObjects.push(headerLine);
    }
    annotationObjects.push(commentBox);


}

$(document).ready(function () {
    PrintButtonSetEnabled(false);
    LoadPreviewsOnCanvas();

    $('#printBtnpdf').on('click', function () {
        setButtonLoading($(this));
        printReport();
    });
    $('#btngeneratepdf').on('click', function () {
        setButtonLoading($(this));
        generatepdf();
    });

    $('#saveAsJPGpdf').on('click', function () {
        setButtonLoading($(this));
        saveAsJPG();
    });
});


function setButtonLoading(btn) {
    setTimeout(function () {
        btn.button('loading');
    }, 0);

    var imgLoading = btn.attr("data-loading-image");
    if (typeof imgLoading != "undefined" && imgLoading !== false) {
        $("#" + imgLoading).show();
    }
    PrintButtonSetEnabled(false);
}


function saveAsJPG() {
    LoadImgPreviews(false);

    _gCheckReadyForPrintInterval = setInterval(function () {
        var allImagesAreLoaded = true;
        $.each(_gCanvasImgPreviews, function (index, value) {
            if (value == 0) {
                allImagesAreLoaded = false;
                return false;
            }
        });

        if (allImagesAreLoaded == true) {
            // The document is ready for printing. Stop calling the check function
            window.clearInterval(_gCheckReadyForPrintInterval);

            PrintButtonSetEnabled(true);

            for (var i = 0; i < _imgToBase64Lst.length; i++) {
                // CZ - 2745 comment
                // Safari does not call load event on img objects second time issue
                var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
                if (isSafari) {
                    if (_imgToBase64Lst[i].indexOf("base64") < 0) {
                        _imgToBase64Lst[i] = 'data:application/octet-streamng;base64,' + _imgToBase64Lst[i];
                    }
                }

                download(_imgToBase64Lst[i], "AnnotationReport Page " + (i + 1) + ".jpeg", "image/jpeg");

                if (isSafari) {
                    _imgToBase64Lst[i] = _imgToBase64Lst[i].split(',')[1];
                }
            }
            PrintButtonSetState();
        }
    }, 2000);
}

function generatepdf() {
    PrintButtonSetEnabled(false);
    var sample = [];
    var annotationreportName = "";
    var canvasIndex = -1;

    for (var v = 0; v < parseInt($('#vpdf').attr("totalversions")) ; v++)
    {
     for (var p = 0; p < parseInt($('#totalNrOfPages' + v).attr("totalpages")) ; p++)
     {
        var canvas = document.getElementById('canvas_' + p + '_' + $('#V' + v + '_Page' + p).attr("ApprovalID"));
        var dataURL = canvas.toDataURL('image/png');
        canvasIndex++;

        var height = $('.canvas-container')[canvasIndex].clientHeight / 1.33;
        var width = $('.canvas-container')[canvasIndex].clientWidth / 1.33;
        var maxHeight = 400;
        var maxWidth = 500;
        var pdfWidth;
        var pdfHeight;
        if (height > maxHeight || width > maxWidth) {
            var ratio = height / width;
            if (height > maxHeight) {
                pdfWidth = maxHeight / ratio;
                pdfHeight = maxHeight;
            } else {
                pdfHeight = maxWidth * ratio;
                pdfWidth = maxWidth;
            }
        } else {
            pdfWidth = width;
            pdfHeight = height;
        }
        var loop = [];
        var intValue = 1;
        for (var z = 0; z < p; z++) {
           
                intValue = intValue + parseInt($('#V' + v + '_Page' + z).attr("NrOfannotation"));
        }

        for (var i = intValue; i < intValue + parseInt($('#V' + v + '_Page' + p).attr("NrOfannotation")) ; i++) {
            var headerText = i + "    " + $('#V_'+ v +'_AnnotationOwnerName' + i)[0].textContent + "\n" + $('#V_'+ v +'_AnnotationFormatedDate' + i)[0].textContent;
            var comment = [];
            var reply1 = [];

            for (var c = 0; c < $('#V_'+ v +'_AnnotationComment'+i)[0].children.length; c++) {
                var para = [];
                for (var d = 0; d < $('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes.length; d++) {
                    if (d != $('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes.length - 1) {
                        if ($('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].nodeName == "#text") {
                            var line = { text: $('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].nodeValue };
                            para.push(line);
                        }
                        else if($('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].nodeName == "SPAN"){
                            var line = { text: $('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].innerText };
                            para.push(line);
                        }
                        if ($('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].nodeName == "STRONG") {
                            if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].children.length > 0) {
                                var Italics = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<em>");
                                var Underline = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: true, italics: Italics, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: true, italics: Italics };
                                    para.push(line);
                                }
                                } else {
                                var line = { text: $('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].innerText, bold: true };
                                para.push(line);
                            }
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "EM") {
                            if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].children.length > 0) {
                                var Bold = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<strong>");
                                var Underline = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: Bold, italics: true, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: Bold, italics: Italics };
                                    para.push(line);
                                }
                            } else {
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, italics: true };
                                para.push(line);
                            }
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "U") {
                            if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].children.length > 0) {
                                var Bold = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<strong>");
                                var Italics = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<em>");
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: Bold, italics: Italics, decoration: 'underline' };
                                para.push(line);
                            } else {
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, decoration: 'underline', };
                                para.push(line);
                            }
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "BR") {
                            var line = { text: '\n' };
                            para.push(line);
                        }
                    } else {
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "#text") {
                            var line = { text: $('#V_'+ v +'_AnnotationComment'+i)[0].children[c].childNodes[d].nodeValue + "\n" };
                            para.push(line);
                        }
                        else if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "SPAN") {
                            var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText + "\n" };
                            para.push(line);
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "STRONG") {
                            if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].children.length > 0) {

                                var Italics = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<em>");
                                var Underline = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: true, italics: Italics, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: true, italics: Italics };
                                    para.push(line);
                                }
                            } else {
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText + "\n", bold: true };
                                para.push(line);
                            }
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "EM") {
                            if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].children.length > 0) {

                                var Bold = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<strong>");
                                var Underline = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: Bold, italics: true, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: Bold, italics: Italics };
                                    para.push(line);
                                }
                            } else {
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText + "\n", italics: true };
                                para.push(line);
                            }
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "U") {
                            if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].children.length > 0) {

                                var Bold = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<strong>");
                                var Italics = $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerHTML.includes("<em>");
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, bold: Bold, italics: Italics, decoration: 'underline' };
                                para.push(line);
                            } else {
                                var line = { text: $('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].innerText, decoration: 'underline', };
                                para.push(line);
                            }
                        }
                        if ($('#V_' + v + '_AnnotationComment' + i)[0].children[c].childNodes[d].nodeName == "BR") {
                            var line = { text: '\n' };
                            para.push(line);
                        }
                    }
                }
                var paragraph = {
                    text: para
                }
                comment.push(paragraph);
            }
         
            for (var c = 0; c < $('.V_' + v + '_AnnotationComment' + i + '_Reply').length; c++) {
                var headerText1 = $('.V_' + v + '_AnnotationComment' + i + '_Reply_AnnotationOwner')[c].innerText + "\n" + $('.V_' + v + '_AnnotationComment' + i + '_Reply_Date')[c].innerText;
                var comment1 = [];
                var para = [];
                for (var g = 0; g < $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children.length; g++){
                for (var d = 0; d < $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes.length; d++) {
                    if (d != $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes.length - 1) {
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "#text") {
                            var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeValue };
                            para.push(line);
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "STRONG") {
                            if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].children.length > 0) {
                                var Italics = $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerHTML.includes("<em>");
                                var Underline = $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: true, italics: Italics, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: true, italics: Italics };
                                    para.push(line);
                                }
                            } else {
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, bold: true };
                                para.push(line);
                            }
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "EM") {
                            if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].children.length > 0) {
                                var Bold = $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerHTML.includes("<strong>");
                                var Underline = $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: Bold, italics: true, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: Bold, italics: Italics };
                                    para.push(line);
                                }

                            } else {
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, italics: true };
                                para.push(line);
                            }
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "U") {
                            if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].children.length > 0) {
                                var Bold = $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerHTML.includes("<strong>");
                                var Italics = $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerHTML.includes("<em>");
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: Bold, italics: Italics, decoration: 'underline' };
                                para.push(line);

                            } else {
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, decoration: 'underline', };
                                para.push(line);
                            }
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "BR") {
                            var line = { text: '\n' };
                            para.push(line);
                        }
                    } else {
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "#text") {
                            var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeValue + "\n" };
                            para.push(line);
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "STRONG") {
                            if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].children.length > 0) {
                                var Italics = $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerHTML.includes("<em>");
                                var Underline = $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, bold: true, italics: Italics, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, bold: true, italics: Italics };
                                    para.push(line);
                                }
                            } else {
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText + "\n", bold: true };
                                para.push(line);
                            }
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "EM") {
                            if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].children.length > 0) {
                                var Bold = $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerHTML.includes("<strong>");
                                var Underline = $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerHTML.includes("<u>");
                                if (Underline) {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: Bold, italics: true, decoration: 'underline' };
                                    para.push(line);
                                } else {
                                    var line = { text: $('.V_' + v + '_AnnotationComment' + i)[c].children[g].childNodes[d].innerText, bold: Bold, italics: Italics };
                                    para.push(line);
                                }
                            } else {
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText + "\n", italics: true };
                                para.push(line);
                            }
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "U") {
                            if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].children.length > 0) {
                                var Bold = $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerHTML.includes("<strong>");
                                var Italics = $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerHTML.includes("<em>");
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, bold: Bold, italics: Italics, decoration: 'underline' };
                                para.push(line);
                            } else {
                                var line = { text: $('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].innerText, decoration: 'underline', };
                                para.push(line);
                            }
                        }
                        if ($('.V_' + v + '_AnnotationComment' + i + '_Reply')[c].children[g].childNodes[d].nodeName == "BR") {
                            var line = { text: '\n' };
                            para.push(line);
                        }
                    }
                }
            }
                var paragraph = {
                    text: para
                }
                comment1.push(paragraph);

                var ann1 = [
                        { text: headerText1, bold: true },
                        comment1
                        ]
                
                reply1.push(ann1);
                var em = [{ text: '\n' }];
                reply1.push(em);
            }

            if (reply1.length > 0) {
                var ann = {
                    style: 'tableExample',
                    table: {
                        widths: [300],
                        body: [
                            [
                           {
                               rowSpan: 1,
                               colSpan: 1,
                               border: [true, true, true, true],
                               fillColor: '#dcdcdc',
                               text: headerText,
                               bold: true
                           },
                            ],
                            [
                                comment
                            ],
                            [
                                reply1
                            ]
                        ]
                    }
                }
                loop.push(ann);
            } else {
                var ann = {
                    style: 'tableExample',
                    table: {
                        widths: [300],
                        body: [
                            [
                           {
                               rowSpan: 1,
                               colSpan: 1,
                               border: [true, true, true, true],
                               fillColor: '#dcdcdc',
                               text: headerText,
                               bold: true
                           },
                            ],
                            [
                                comment
                            ]
                        ]
                    }
                }
                loop.push(ann);
            }
        }
    
        var approvalName = $('#ReportJobTitle_V'+ v +'_P' + p)[0].innerText;
        var fileName = $('#ReportApprovalName_V'+ v +'_P' + p)[0].innerText;
        var uploadDate = $('#ReportCreatedDate_V'+ v +'_P' + p)[0].innerText;
        var generationDate = $('#ReportGenerationDate_V'+ v +'_P' + p)[0].innerText;
        var version = $('#ReportVersion_V'+ v +'_P' + p)[0].innerText;
        var owner = $('#ReportCreator_V' + v + '_P' + p)[0].innerText;

        annotationreportName = approvalName;
        var test1 = [{
            style: 'header',
            table: {
                widths: [120, 240, 130, 240],
                body: [
                    [{ text: 'Approval Name', bold: true }, approvalName, { text:'File Name', bold: true }, fileName],
                    [{ text: 'Upload Date', bold: true }, uploadDate, { text: 'Report Generation Date', bold: true }, generationDate],
                    [{ text: 'Version', bold: true }, version, { text: 'Approval Owner', bold: true }, owner]
                ]
            }
        },
            {
                style: 'body',
                columns: [
                        {
                            width: 450,
                            alignment: 'center',
                            stack: [
                                {
                                    image: dataURL,
                                    width: pdfWidth,
                                    height: pdfHeight
                                }
                            ]
                        },
                        {
                            stack: loop
                        }
                ]
            }];
        sample.push(test1);
    }
}
    var docDefinition = {
        pageOrientation: 'landscape',
        content: sample,
        styles: {
            body: {
                margin: [10, 10, 10, 200],
                fontSize: 8
            },
            header: {
                fontSize: 8,
                border: '1px solid #dddddd'
            },
            tableExample: {
                margin: [0, 5, 0, 0],
                fontSize: 8
            },
            tableHeader: {
                bold: true,
                fontSize: 8,
                color: 'black'
            }
        }
    }
    pdfMake.createPdf(docDefinition).download(annotationreportName + '.pdf');
    setTimeout(PrintButtonSetState, 2000);
    PrintButtonSetEnabled(true);
}
