﻿function SetColorLabel(element, decisionDisplayText, decisionLabelColorName) {
    if (decisionDisplayText !== "Pending") {

        $(element).removeClass(function (index, css) {
            return (css.match(/(^|\s)label-\S+/g) || []).join(' ');
        });

        $(element).addClass("label-" + decisionLabelColorName);

    } else {
        $(element).addClass("label-hidden");
    }
}

function UpdateDecisionText(message, versionId) {

    if (message.DecisionDisplayText === null) {
        return;
    }

    var element = $("label.lblApproval" + versionId);

    var spanLabel = $(element).find("span.approval-decision");

    $(spanLabel).text(message.DecisionDisplayText);

    SetColorLabel(spanLabel, message.DecisionDisplayText, message.DecisionLabelColorName);

    element = $("#" + versionId);
    spanLabel = $(element).find("span.approval-decision");
    SetColorLabel(spanLabel, message.DecisionDisplayText, message.DecisionLabelColorName);
    $(spanLabel).text(message.DecisionDisplayText);

};

function UpdateDecision(message, approvalId) {
    $.ajax({
        type: "GET",
        url: "/Approvals/GetDecision?decisionKey=" + message.Decision + "&decisionMaker=" +
            message.DecisionMaker + "&approvalId=" + approvalId + "&loggedUserID=" + message.Creator,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: true,
        success: function (decision) {
          if (decision != null)
          {
            var element = $("label.lblApproval" + approvalId);
            var elemSummaryView = $(element).find('div.decisions');
            var elemStatusView = $('div#' + approvalId + '.decisions');
            $(elemSummaryView).empty();
            $(elemStatusView).empty();

            if (message.MoveNext === false || message.IsPhaseComplete) {
                $(elemSummaryView).append(decision);
                $(elemStatusView).append(decision);
            }
          }
        }
    });
}

function UpdateApprovalStatus(message, approvalId) {
    $.ajax({
        type: "GET",
        url: "/Approvals/GetApprovalStatus?approvalId=" + approvalId + "&decisionType=" + message.DecisionType + "&isPhaseComplete=" + message.IsPhaseComplete + "&decisionKey=" + message.Decision + "&decisionMakers=" + message.DecisionMaker + "&loggedUserID=" + message.Creator,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: true,
        success: function (status) {
            if (status != null)
            {
            var element = $("label.lblApproval" + approvalId);
            $('span.status-col-' + approvalId).text("").append(status);
            }
        }
    });
}

function UpdatePhaseDetails(message, approvalId) {
    var element = $("label.lblApproval" + approvalId);
    if (message.PhaseName) {
        $('p.phase-col-' + approvalId + '> label').text(message.PhaseName);
        $('span.workflow-tooltip-' + approvalId).empty().append(message.WorkflowTooltip);
        $(element).find('span.currentPhaseName').text(message.PhaseName);
        $('p.pdm-col-' + approvalId).text(message.PrimaryDecisionMaker);
    }
    if (message.NextPhase) {
        $('p.nextphase-col-' + approvalId).text(message.NextPhase);
    } 
};

function UpdateApprovalName(approvalName, versionId) {
    if (approvalName !== null) {
        $(".lblApproval" + versionId + " h3.title span").replaceWith('<span style="margin-left: 0px">' + approvalName + '</span>');
        $("p.lblApprovalTitle" + versionId).text(approvalName);
    }
}

function UpdateApprovalPDM(versionId, PDM) {
    if (PDM !== null) {
        if (PDM) {
            $(".pdm-col-" + versionId).text(PDM);
            if ($(".await-pdm-col-" + versionId).text().length > 0) {
                $(".await-pdm-col-" + versionId).text($("#awaitPdm").val());
            }
        }
        else {
            $(".pdm-col-" + versionId).text("");
            $(".cell-align.pdm-col-" + versionId).text("-");
            $(".await-pdm-col-" + versionId).text("-");
        }
    }
}

function UpdateApprovalDeadline(deadLineDate, deadLineTime, versionId, IsOverdue, OverdueTimeDifference, IsPhaseDeadline) {
    if (deadLineDate !== null) {    
        if (IsPhaseDeadline) {
            var newDeadLineList = '<span>' +
                                deadLineDate +
                                '<br>' +
                                '<small class="medium-gray-color">' + deadLineTime + '</small>'
            $(".phasedeadline-col-" + versionId + " span").replaceWith(newDeadLineList);
        } else {

            var dueDate = SetDueDateTextAndStyle(IsOverdue);
            var newDeadLineThumbnail = '<span class="deadline">' +
                                    '<strong>' + dueDate.name + '</strong>' +
                                        '<i rel="tooltip" class="icon-time" data-original-title="' + deadLineTime + " (" + OverdueTimeDifference.Days + " " + $("#days").val() + ", " + OverdueTimeDifference.Hours + " " + $("#hours").val() + '")"></i>'
                                        + deadLineDate +
                                '</span>';

            '</span>';

            var $existingDeadlineElem = $("#spDue" + versionId + " span");

            if ($existingDeadlineElem.length > 0) {
                $existingDeadlineElem.replaceWith(newDeadLineThumbnail);
                $existingDeadlineElem.css('color', dueDate.color);
            } else {
                var $newDeadlineElem = $("#spDue" + versionId);

                $newDeadlineElem.append(newDeadLineThumbnail);
                $newDeadlineElem.css('color', dueDate.color);
            }

            $("[rel='tooltip']").tooltip();           
        }
    }
}

function UpdateApprovalOwner(owner, approvalId) {
    if (owner) {
        $('.owner-col-' + approvalId).text(owner);
    }
}

function SetDueDateTextAndStyle(IsOverdue) {
    if (IsOverdue) {
        return {
            name: $("#overdue").val() + ":",
            color: "#BA1319"
        };
    }
    else {
        return {
            name: $("#due").val() + ":",
            color: "#777777"
        };
    }
}

function UpdateApprovalProgress(messageObj) {
    var updateFor = {
        "Success": function () {
            return UpdateApprovalInDashboardForSuccess(messageObj);
        },
        "InProgress": function () {
            return UpdateApprovalInDashboardForInProgress(messageObj);
        },
        "Error": function () {
            return UpdateApprovalInDashboardForError(messageObj);
        }
    };
    return updateFor[messageObj.Status]();
}

function UpdateApprovalInDashboardForInProgress(messageObj) {
    $(".bar.dvProgress" + messageObj.ApprovalId).css({
        'width': messageObj.Progress + "%"
    });
    $('#dvProgress' + messageObj.ApprovalId).addClass("p" + messageObj.Progress);
    $('#perProgress' + messageObj.ApprovalId).html(messageObj.Progress + "%");
}

function UpdateApprovalInDashboardForSuccess(messageObj) {
    $('.h3Processing' + messageObj.ApprovalId).remove();
    $('.imgThumb' + messageObj.ApprovalId).attr('src', messageObj.Thumbnail);
    $('.aThumb' + messageObj.ApprovalId).removeClass('locked');
    //$('.chkThumb' + messageObj.ApprovalId).addClass('choose_item');
    $('.chkThumb' + messageObj.ApprovalId).removeClass('inProgress');
    $('.lblApproval' + messageObj.ApprovalId + ' .hide').removeClass('hide');
    $('#approvalMenuItem' + messageObj.ApprovalId).removeClass('hide');
    $('#approvalMenuItem' + messageObj.ApprovalId).find('*').removeClass('hide');
    $('.lblApproval' + messageObj.ApprovalId).parents('li.block:first').removeClass('render');
    $('#lblApprovalID' + messageObj.ApprovalId).removeClass('hide');
}

function UpdateApprovalInDashboardForError(messageObj) {
    $('.dvProgress' + messageObj.ApprovalId).parent().remove();
    $('.h3Processing' + messageObj.ApprovalId).text(messageObj.Status);
    $('.imgThumb' + messageObj.ApprovalId).attr('src', messageObj.Thumbnail);
    $('.lblApproval' + messageObj.ApprovalId).parents('li.block:first').removeClass('render');
    $('.dvThumb' + messageObj.ApprovalId).removeClass('hide');
    $('.chkApproval' + messageObj.ApprovalId).removeClass('hide');
    $('.dvThumb' + messageObj.ApprovalId + ' .dropdown-menu li').addClass('hide');
    if ($('.dvThumb' + messageObj.ApprovalId + ' .approval-action[action=DeleteVersion]')) {
        $('.dvThumb' + messageObj.ApprovalId + ' .approval-action[action=DeleteVersion]').parents('li:first').removeClass('hide');
    }
    if ($('.dvThumb' + messageObj.ApprovalId + ' .approval-action[action=DeleteApproval]')) {
        $('.dvThumb' + messageObj.ApprovalId + ' .approval-action[action=DeleteApproval]').parents('li:first').removeClass('hide');
    }
    $('.h3Processing' + messageObj.ApprovalId).removeClass('processing').addClass('label label-important');
}

$(document).ready(function () {
    var hub = $.connection ? $.connection.instantNotificationsHub : null;
    if (hub) {
        hub.client.addDashboardNotification = function (message) {
            if (message.Creator === $("#n").val()) {
                return;
            } else {
                LoadApprovalVisualIndicators([message.Version]);
                LoadApprovalNotViewedAnnotations([message.Version]);
                LoadSOADIndicator([message.Version]);

                UpdatePhaseDetails(message, message.Version);
                if (message.Decision) {
                    UpdateApprovalStatus(message, message.Version);
                }

                if (message.Decision) {
                    LoadApprovalDecisions([message.Version]);

                    setTimeout(function () {
                        UpdateDecision(message, message.Version);
                    }, 200);
                }

                UpdateApprovalDeadline(message.DeadLineDate, message.DeadLineTime, message.Version, message.IsOverdue, message.OverdueTimeDifference, message.IsPhaseDeadline);
                UpdateApprovalName(message.ApprovalName, message.Version);
                UpdateApprovalPDM(message.Version, message.PDM);
                UpdateApprovalOwner(message.Owner, message.Version);
            }
        }
        hub.client.updateApprovalStatus = function (messageObj) {
            UpdateApprovalProgress(messageObj);
        }

        $.connection.hub.start().done(function () { }).fail(function (error) {
            console.log("Failed starting SIGNALR: " + error);
        });

        $.connection.hub.disconnected(function () {
            console.log("SIGNALR disconnected");
        });
    }
});
