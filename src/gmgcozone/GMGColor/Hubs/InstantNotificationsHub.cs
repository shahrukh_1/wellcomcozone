﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GMGColor.Common;
using GMGColorDAL;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace GMGColor.Hubs
{
    public class InstantNotificationsHub: Hub
    {        
        private readonly static Dictionary<int, List<string>> dicUsers_VersionIds = new Dictionary<int, List<string>>();        
        private readonly static Dictionary<int, List<string>> dashboardUsers_VersionIds = new Dictionary<int, List<string>>();
        private bool _stopCalled = true;

        public static IHubContext Instance
        {
            get
            {
                return GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            }
        }

        public static List<string> GetClients(int versionId)
        {
            try
            {
                return (dicUsers_VersionIds.ContainsKey(versionId) ? dicUsers_VersionIds[versionId] : null) ??
                       new List<string>();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("InstantNotificationsHub.GetClients for approval " + versionId, ex);
            }

            return new List<string>();
        }

        public static List<string> GetDashboardClients(string versionIds)
        {
            try
            {
                var dashboardClients = new List<string>();
                if (!string.IsNullOrEmpty(versionIds) && versionIds.Length > 0)
                {
                    var versions = versionIds.Split(',').Select(Int32.Parse).ToList();
                    foreach (var version in versions)
                    {
                        if (dashboardUsers_VersionIds.ContainsKey(version))
                        {
                            dashboardClients.AddRange(dashboardUsers_VersionIds[version]);
                        }
                    }
                }
                return dashboardClients;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("InstantNotificationsHub.GetDashboardClients for approval " + versionIds, ex);
            }

            return new List<string>();
        }

        public void RegisterConnectionApprovalIds(string ids)
        {
            RegisterIds(dicUsers_VersionIds, ids);
        }

        public void RegisterDashboardConnectionApprovalIds(string ids)
        {
            RegisterIds(dashboardUsers_VersionIds, ids);
        }

        private void RegisterIds(Dictionary<int, List<string>> dictionary, string ids)
        {
            try
            {
                //each client need to connect with a list of the approval version ids
                var idArr = ids.Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
                foreach (string id in idArr)
                {
                    int versionId = id.ToInt().GetValueOrDefault();

                    if (versionId == 0)
                        continue;

                    bool alreadyExists = false;
                    if (dictionary.Count == 0)
                    {
                        dictionary.Add(versionId, new List<string>() { Context.ConnectionId });
                    }
                    else
                    {
                        // TODO Remove dictionary update from foreach
                        foreach (int key in dictionary.Keys)
                        {
                            if (key == versionId)
                            {
                                dictionary[key].Add(Context.ConnectionId);
                                alreadyExists = true;
                                break;
                            }
                        }

                        if (!alreadyExists)
                        {
                            // TODO alreadyExists does not always seems reliable as it throws already existing error
                            dictionary.Add(versionId, new List<string>() { Context.ConnectionId });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("InstantNotificationsHub.RegisterConnectionApprovalIds", ex);
            }
        }

        public override Task OnDisconnected(bool stopCalled = true)
        {

            UnregisterIds(dashboardUsers_VersionIds);
            return UnregisterIds(dicUsers_VersionIds);
        }

        private Task UnregisterIds(Dictionary<int, List<string>> dictionary)
        {
            try
            {
                var getExistingConnection = dictionary.FirstOrDefault(t => t.Value.Contains(Context.ConnectionId));
                if (dictionary.ContainsKey(getExistingConnection.Key))
                {
                    dictionary[getExistingConnection.Key].Remove(Context.ConnectionId);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("InstantNotificationsHub.OnDisconnect", ex);
            }
            return base.OnDisconnected(_stopCalled);
        }
    }
}