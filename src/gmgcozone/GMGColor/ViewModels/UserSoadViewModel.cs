﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GMGColor.ViewModels
{
    public class UserSoadViewModel
    {
        public int Approval { get; set; }
        public int Phase { get; set; }
        public int Collaborator { get; set; }
        public bool IsExternal { get; set; }
        public string ProgressStateClass { get; set; }
    }
}