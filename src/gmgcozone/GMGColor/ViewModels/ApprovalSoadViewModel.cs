﻿namespace GMGColor.ViewModels
{
    public class ApprovalSoadViewModel
    {
        public int Approval { get; set; }
        public int? Phase { get; set; }
        public string ProgressStateClass { get; set; }
    }
}