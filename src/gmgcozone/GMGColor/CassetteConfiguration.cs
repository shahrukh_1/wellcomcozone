using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;
using Cassette.BundleProcessing;

namespace GMGColor
{
    /// <summary>
    /// Configures the Cassette asset bundles for the web application.
    /// </summary>
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            // Please read http://getcassette.net/documentation/configuration

            // This default configuration treats each file as a separate 'bundle'.
            // In production the content will be minified, but the files are not combined.
            // So you probably want to tweak these defaults!
            //bundles.AddPerIndividualFile<StylesheetBundle>("Content");
            //bundles.AddPerIndividualFile<ScriptBundle>("Scripts");

            // To combine files, try something like this instead:

            // Ensure that Cassette doesn't compile all the
            //.less files in ~/Content/Styles.
            //bundles.Add<StylesheetBundle>("bootstrap", "Content/less/bootstrap.less");

            var styles = new[] {
               "~/Content/css/styles.css"
            };
            bundles.Add<StylesheetBundle>("styles", styles, b =>
                                                                {
                                                                    b.PageLocation = "head";
                                                                    b.Media = "screen";
                                                                    
                                                                });

            var basicstyles = new[] {
               "~/Content/css/styles-basic.css"
            };
            bundles.Add<StylesheetBundle>("basicstyles", basicstyles, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";

            });

            bundles.Add<StylesheetBundle>("annotationStyles", styles, b =>
            {
                b.PageLocation = "head";
            });

            var printStyles = new[] {
               "~/Content/css/styles-print.css"
            };
            bundles.Add<StylesheetBundle>("printStyles", printStyles, b =>
                                                                    {
                                                                        b.PageLocation = "head";
                                                                        b.Media = "print";
                                                                       
                                                                    });

            var responsiveStyles = new[]
                                   {
                                       "~/Content/css/styles-responsive.css"
                                   };
            bundles.Add<StylesheetBundle>("responsiveStyles", responsiveStyles, b =>
            {
                b.PageLocation = "head";
            });


            bundles.Add<StylesheetBundle>("annotation-report-filters", new[] { "~/Content/css/annotations-report-filters.css" }, b =>
            {
                b.PageLocation = "head";
            });

            //Theme scripts
            var greenTheme = new[] {"~/Content/css/theme-green.css"};
            var pinkTheme = new[] {"~/Content/css/theme-pink.css"};
            var whiteTheme = new[] {"~/Content/css/theme-white.css"};
            var blueTheme = new[] {"~/Content/css/theme-blue.css"};

            //green theme
            bundles.Add<StylesheetBundle>("greenTheme", greenTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "green");
            });
            bundles.Add<StylesheetBundle>("greenThemeAlternate", greenTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "green");
                b.HtmlAttributes.Remove("rel");
                b.HtmlAttributes.Add("rel", "alternate stylesheet");
            });
            //pink theme
            bundles.Add<StylesheetBundle>("pinkTheme", pinkTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "pink");
            });
            bundles.Add<StylesheetBundle>("pinkThemeAlternate", pinkTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "pink");
                b.HtmlAttributes.Remove("rel");
                b.HtmlAttributes.Add("rel", "alternate stylesheet");
            });
            //white theme
            bundles.Add<StylesheetBundle>("whiteTheme", whiteTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "white");
            });
            bundles.Add<StylesheetBundle>("whiteThemeAlternate", whiteTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "white");
                b.HtmlAttributes.Remove("rel");
                b.HtmlAttributes.Add("rel", "alternate stylesheet");
            });
            //blue theme
            bundles.Add<StylesheetBundle>("blueTheme", blueTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "blue");
            });
            bundles.Add<StylesheetBundle>("blueThemeAlternate", blueTheme, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                b.HtmlAttributes.Add("title", "blue");
                b.HtmlAttributes.Remove("rel");
                b.HtmlAttributes.Add("rel", "alternate stylesheet");
            });

            //ProofStudio
            var proofStudioVendorCss = new[] { "~/Content/ProofStudio/desktop/vendor.min.css" };

            var proofStudioAppCss = new[] { "~/Content/ProofStudio/desktop/app.min.css" };

            bundles.Add<StylesheetBundle>("proofStudioVendorCss", proofStudioVendorCss, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            bundles.Add<StylesheetBundle>("proofStudioAppCss", proofStudioAppCss, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            //BootStrap CSS
            var bootstrapCss = new[] { "~/Content/css/bootstrap.min.css" };

            bundles.Add<StylesheetBundle>("bootstrapCss", bootstrapCss, b =>
            {
                b.PageLocation = "head";
                b.Media = "screen";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            var bootstrapResponsiveCss = new[] { "~/Content/css/bootstrap-responsive.min.css" };

            bundles.Add<StylesheetBundle>("bootstrapResponsiveCss", bootstrapResponsiveCss, b =>
            {
                b.PageLocation = "head";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            var proofStudioVendor = new[] {
                "~/Content/ProofStudio/desktop/vendor.min.js", 
            };

            var proofStudioApp = new[] {
                "~/Content/ProofStudio/desktop/app.min.js"
            };

            var filedownload = new[] {
                "~/Content/js/file-download.js", 
            };
            bundles.Add<ScriptBundle>("filedownload", filedownload);

            bundles.Add<ScriptBundle>("proofStudioVendor", proofStudioVendor, b =>
            {
                b.PageLocation = "head";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            bundles.Add<ScriptBundle>("proofStudioApp", proofStudioApp, b =>
            {
                b.PageLocation = "head";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            //Fabric js Component
            var fabricScript = new[] {
                "~/Content/js/fabric.min.1.4.12.js"
            };

            bundles.Add<ScriptBundle>("fabric", fabricScript, b =>
            {
                b.PageLocation = "head";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            //Annotation Report PDF js
            var annotationReportForPDF = new[] {
                "~/Scripts/AnnotationReportForPDF.js",
                "~/Scripts/FileSaver.min.js"
            };

            bundles.Add<ScriptBundle>("annotationReportForPDF", annotationReportForPDF, b =>
            {
                b.PageLocation = "head";
            });

            //Annotation Report js
            var annotationReport = new[] {
                "~/Scripts/AnnotationReport.js",
                "~/Scripts/FileSaver.min.js"
            };

            bundles.Add<ScriptBundle>("annotationReport", annotationReport, b =>
            {
                b.PageLocation = "head";
            });

            var bootstrap_button = new[] {
                "~/Content/js/bootstrap/bootstrap-button.js"
            };

            bundles.Add<ScriptBundle>("bootstrap-button", bootstrap_button, b =>
            {
                b.PageLocation = "head";
            });            

            //Modernizr
            var modernizr = new[] {
                "~/Scripts/modernizr-2.5.3-respond-1.1.0.min.js"
            };

            bundles.Add<ScriptBundle>("modernizr", modernizr, b =>
            {
                b.PageLocation = "head";
                var index = b.Pipeline.IndexOf<MinifyAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
                index = b.Pipeline.IndexOf<ConcatenateAssets>();
                if (index >= 0)
                {
                    b.Pipeline.RemoveAt(index);
                }
            });

            // Validation scripts
            var validatescripts = new[] {
                "~/Scripts/jquery.validate.js", 
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery.unobtrusive-ajax.js"
            };
            bundles.Add<ScriptBundle>("validate", validatescripts);


            bundles.Add<ScriptBundle>("DeliverAddJob", "~/Scripts/DeliverAddJob.js");
            bundles.Add<ScriptBundle>("WhiteLabelSettings", "~/Scripts/WhiteLabelSettings.js");           
            bundles.Add<ScriptBundle>("SettingsNewCMSItem", "~/Scripts/SettingsNewCMSItem.js");
            bundles.Add<ScriptBundle>("UploadEngineNewXMLTemplate", "~/Scripts/UploadEngineNewXMLTemplate.js");

            // Bootstrap Scripts
            var bootstrapScripts = new[] {
                "~/Content/js/bootstrap/bootstrap-alert.js",
                "~/Content/js/bootstrap/bootstrap-button.js",
                //"~/Content/js/bootstrap/bootstrap-carousel.js",
                "~/Content/js/bootstrap/bootstrap-collapse.js",
                "~/Content/js/bootstrap/bootstrap-dropdown.js",
                "~/Content/js/bootstrap/bootstrap-modal.js",                
                "~/Content/js/bootstrap/bootstrap-scrollspy.js",
                "~/Content/js/bootstrap/bootstrap-tab.js",
                "~/Content/js/bootstrap/bootstrap-tooltip.js",
                "~/Content/js/bootstrap/bootstrap-popover.js",
                "~/Content/js/bootstrap/bootstrap-transition.js",
                "~/Content/js/bootstrap/bootstrap-typeahead.js"
            };
            bundles.Add<ScriptBundle>("bootstrap", bootstrapScripts);

            var stickyTableColumns = new[] {
                "~/Scripts/jquery.dataTables.js",
                "~/Scripts/dataTables.fixedColumns.js"

            };
            bundles.Add<ScriptBundle>("stickyTableColumns", stickyTableColumns);

            var approvalWorkflow = new[] {
                "~/Content/js/approval-workflow.js"
            };
            bundles.Add<ScriptBundle>("approvalWorkflow", approvalWorkflow);

            var approvalPhase = new[] {
                "~/Content/js/approval-phase.js"
            };
            bundles.Add<ScriptBundle>("approvalPhase", approvalPhase);

            var checklist = new[] {
                "~/Content/js/checklist.js"
            };
            bundles.Add<ScriptBundle>("checklist", checklist);

            var checklistItem = new[] {
                "~/Content/js/checklist-item.js"
            };
            bundles.Add<ScriptBundle>("checklistItem", checklistItem);

            // File upload scripts
            var fileuploadScripts = new[] {
                "~/Content/js/jquery/jquery.ui.widget.js", 
                "~/Content/js/blueimp/tmpl.min.js", 
                "~/Content/js/blueimp/canvas-to-blob.min.js",
                "~/Content/js/blueimp/load-image.min.js",
                "~/Content/js/blueimp/jquery.iframe-transport.js",
                "~/Content/js/blueimp/jquery.fileupload.js",
                "~/Content/js/blueimp/jquery.fileupload-ip.js",
                "~/Content/js/blueimp/jquery.fileupload-ui.js",
                "~/Content/js/blueimp/locale.main.js",
                "~/Content/js/cozone-fileupload.js",
            };
            bundles.Add<ScriptBundle>("fileupload", fileuploadScripts);

            // HTML5 Player
            var html5PlayerScript = new[] {
                "~/Content/js/html5Player/core.js",
                "~/Content/js/html5Player/lib.js",
                "~/Content/js/html5Player/json.js",
                "~/Content/js/html5Player/component.js",
                "~/Content/js/html5Player/player.js",
                "~/Content/js/html5Player/tech.js",
                "~/Content/js/html5Player/controls.js",
                "~/Content/js/html5Player/events.js",
                "~/Content/js/html5Player/tracks.js",
                "~/Content/js/tech/html5/html5.js",
                "~/Content/js/tech/flash/flash.js",
                "~/Content/js/html5Player/setup.js"
            };
            bundles.Add<ScriptBundle>("html5Player", html5PlayerScript);
            
            // HTML5 Player CSS
            var html5PlayerCSS = new[] {
                "~/Content/css/video-js.css"
            };
            bundles.Add<StylesheetBundle>("html5PlayerCSS", html5PlayerCSS);

            // Tablet styles
            bundles.Add<StylesheetBundle>("tablet-css", "~/Content/css/styles-tablet.css");

            // Account Index styles
            var accountStyles = new[] {
                "~/Content/css/Account.css"
            };
            bundles.Add<StylesheetBundle>("accountStyles", accountStyles, b =>
                                                                          {
                                                                              b.PageLocation = "head";
                                                                              b.Media = "screen";
                                                                          });

            //Tablet scripts
            bundles.Add<ScriptBundle>("tablet-js", "~/Content/js/scripts-tablet.js");

            // Collaborator Acces scripts            
            bundles.Add<ScriptBundle>("collaboratoraccess", "~/Content/js/codeflex/codeflex.permissions.js");

            // File Transfer Acces scripts            
            bundles.Add<ScriptBundle>("filetransferaccess", "~/Content/js/codeflex/codeflex.filetransferpermissions.js");

            bundles.Add<ScriptBundle>("collaboratorprojectapprovalaccess", "~/Content/js/codeflex/codeflex.projectnewapproval.js");

            // Collaborator Group scripts            
            bundles.Add<ScriptBundle>("collaboratorgroup", "~/Content/js/codeflex/codeflex.groups.js");

            // Collaborator Share scripts            
            bundles.Add<ScriptBundle>("collaboratorshare", "~/Content/js/codeflex/codeflex.share.js");

            // Collaborator Share scripts
            bundles.Add<ScriptBundle>("foldertree", "~/Content/js/codeflex/codeflex.folders.js");

            // Cookie scripts
            bundles.Add<ScriptBundle>("cookie", "~/Content/js/jquery/jquery.cookie.js");

            // Widget scripts
            bundles.Add<ScriptBundle>("widget", "~/Content/js/jquery/jquery.ui.widget.js");

            // Bootstrap Color picker 
            bundles.Add<ScriptBundle>("colorpicker", "~/Content/js/bootstrap/bootstrap-colorpicker.js");

            // Bootstrap Chosen
            bundles.Add<ScriptBundle>("chosen", "~/Content/js/chosen/chosen.jquery.js");

            // Bootstrap Date & time picker 
            bundles.Add<ScriptBundle>("datetimepicker", "~/Content/js/bootstrap/bootstrap-datepicker.js");

            // Password Strength
            bundles.Add<ScriptBundle>("passwordstrength", "~/Content/js/jquery/jquery.pstrength-min.1.2.js");

            // Password Strength
            bundles.Add<ScriptBundle>("swfobject", "~/Content/js/swfobject/swfobject.js");

            // Responsive Tables
            bundles.Add<ScriptBundle>("responsivetables", "~/Content/js/zurb/responsive-tables.js");

            // GMG coZone custom scripts 
            bundles.Add<ScriptBundle>("pluginscripts", "~/Content/js/plugins.js");
            bundles.Add<ScriptBundle>("customscripts", "~/Content/js/script.js");
            bundles.Add<ScriptBundle>("accountingscripts", "~/Content/js/accounting.js");

            bundles.Add<ScriptBundle>("jquery-mobile", "~/Content/js/jquery/jquery.mobile.custom.min.js");

            bundles.Add<ScriptBundle>("navigation-collapse", "~/Content/js/navigation.js");

            var approvalsDashboard = new[] {
                                            "~/Content/js/approval-sidebar.js"
                                            ,"~/Content/js/approvals-index.js"
                                            ,"~/Content/js/approvals-grid.js" 
                                            ,"~/Content/js/approval-advancedSearch.js"
                                          };

            bundles.Add<ScriptBundle>("approvals-dashboard", approvalsDashboard);

            var newApproval = new[] {
                                        "~/Content/js/newapproval.js",
                                        "~/Content/js/addAdHocWorkflow.js",
                                        "~/Content/js/tagwords/select2.min.js"
                                    };

            bundles.Add<ScriptBundle>("newApproval", newApproval);

            var newFolderApprovalVersionApproval = new[] {
                                        "~/Content/js/newprojectfolderapproval.js",
                                        "~/Content/js/addAdHocWorkflow.js",
                                        "~/Content/js/tagwords/select2.min.js"
                                    };
            bundles.Add<ScriptBundle>("newFolderApprovalVersionApproval", newFolderApprovalVersionApproval);

            var newProjectFolderApproval = new[] {
                                        "~/Content/js/tagwords/select2.min.js"
                                    };

            bundles.Add<ScriptBundle>("newProjectFolderApproval", newProjectFolderApproval);
            var projectFolder = new[] {
                                          "~/Content/js/projectFolder.js",
                                          "~/Content/js/addAdHocWorkflow.js",
                                          "~/Content/js/tagwords/select2.min.js"
                                      };

            bundles.Add<ScriptBundle>("projectFolder", projectFolder);

            bundles.Add<ScriptBundle>("filetransferindex", "~/Content/js/newFileTransfer.js");


            bundles.Add<ScriptBundle>("jobdetails", "~/Content/js/job-details.js");

            // ColorPicker sources 
            // https://github.com/tkrotoff/jquery-simplecolorpicker
            
            var simpleColorPickerScripts = new[]
            {
                "~/Content/simple-color-picker/jquery.simplecolorpicker.js",
                "~/Content/simple-color-picker/colorpicker.cozoneusercolor.setup.js"
            };

            bundles.Add<ScriptBundle>("simpleColorPicker", simpleColorPickerScripts);
            var colorPickerStyles = new[] {
                                               "~/Content/simple-color-picker/jquery.simplecolorpicker.css",
                                               "~/Content/simple-color-picker/jquery.simplecolorpicker-fontawesome.css",
                                               "~/Content/simple-color-picker/jquery.simplecolorpicker-glyphicons.css",
                                               "~/Content/simple-color-picker/jquery.simplecolorpicker-regularfont.css"
                                            };
            bundles.Add<StylesheetBundle>("simpleColorPickerStyles", colorPickerStyles);            

            var rezColsStyle = new[] {
                                        "~/Content/css/columnwidths-default.css"
                                     };
            bundles.Add<StylesheetBundle>("rezColStyle", rezColsStyle, b =>
                                                                          {
                                                                              b.PageLocation = "head";
                                                                              b.Media = "screen";
                                                                          });

            bundles.Add<ScriptBundle>("cvsToArray", "~/Scripts/CVSToArray.js");

            bundles.Add<ScriptBundle>("dirtyForms", "~/Content/js/jquery.dirtyforms.1.2.2.js");

            var approvalDetails = new[]
                {
                    "~/Content/js/approval-details.js"
                };
            bundles.Add<ScriptBundle>("approvalDetails", approvalDetails);

            bundles.Add<ScriptBundle>("approval-status", "~/Content/js/approval-status.js");

            bundles.Add<ScriptBundle>("settings-notificationschedule", "~/Content/js/settings-notificationschedule.js");

            bundles.Add<ScriptBundle>("simulationprofile", "~/Content/js/simulationprofile.js");
            
            bundles.Add<StylesheetBundle>("simulationProfileStyles", "~/Content/css/softproofing.css");

            var spfPPT = new[]{
                                "~/Content/js/softproofing-papertint.js"
                               };
            bundles.Add<ScriptBundle>("softProofinfPPT", spfPPT);

            bundles.Add<ScriptBundle>("simulationprofilelist", "~/Content/js/simulationprofile-list.js");

            bundles.Add<StylesheetBundle>("softproofing-papertint", "~/Content/css/softproofing-papertint.css");

            var sendJobToDeliverStyle = new[] {
                                        "~/Content/css/proofstudio-sendJobToDeliver.css"
                                     };
            bundles.Add<StylesheetBundle>("proofstudio-sendJobToDeliverStyles", sendJobToDeliverStyle, b => { b.PageLocation = "head"; });

            var sendJobToDeliverScript = new[] {
                                        "~/Content/js/proofStudio-sendJobToDeliver.js"
                                     };
            bundles.Add<ScriptBundle>("proofstudio-sendJobToDeliverScripts", sendJobToDeliverScript, b => { b.PageLocation = "head"; });

            bundles.Add<ScriptBundle>("plan-newCollaborateBillingPlan", "~/Content/js/plan-newCollaborateBillingPlan.js");

            bundles.Add<ScriptBundle>("accountSettings-addEditUser", "~/Content/js/accountSettings - addEditUser.js");

            bundles.Add<ScriptBundle>("account-report", "~/Content/js/account-report.js");

            bundles.Add<StylesheetBundle>("bouncedEmailUsers", "~/Content/css/bouncedEmailUsers.css");

            bundles.Add<StylesheetBundle>("addAdHocWorkflowStyles", "~/Content/css/addAdHocWorkflow.css",
                                                "~/Content/js/tagwords/select2.min.css");

            bundles.Add<ScriptBundle>("accountThemeScripts", "~/Content/js/setting-accountTheme.js");

            bundles.Add<StylesheetBundle>("accountThemeStyles", "~/Content/css/setting-accountTheme.css");

            bundles.Add<ScriptBundle>("themes", "~/Content/js/themes.js");

            bundles.Add<StylesheetBundle>("themeVariables", "~/Content/less/ThemeVariables.less");

            bundles.Add<ScriptBundle>("lessCompiler", "~/Content/js/less/less.min.js");

            bundles.Add<StylesheetBundle>("theme-blue", "~/Content/less/themes/theme-blue.less");

            bundles.Add<StylesheetBundle>("theme-green", "~/Content/less/themes/theme-green.less");

            bundles.Add<StylesheetBundle>("theme-pink", "~/Content/less/themes/theme-pink.less");

            bundles.Add<StylesheetBundle>("theme-white", "~/Content/less/themes/theme-white.less");

            bundles.Add<ScriptBundle>("settingsCustomise", "~/Content/js/settings-customise.js");

            bundles.Add<ScriptBundle>("settingsCustomSmtp", "~/Content/js/settings-customSmtpServer.js");

            bundles.Add<StylesheetBundle>("settingsCustomSmtpStyles", "~/Content/css/settings-customSmtpServer.css");

            bundles.Add<ScriptBundle>("requiredIfScript", "~/Content/js/requiredIfValidation.js");

            bundles.Add<ScriptBundle>("ssoSettings", "~/Content/js/settings-sso.js");

            bundles.Add<ScriptBundle>("ssoLoginCheck", "~/Content/js/login.js");

            bundles.Add<ScriptBundle>("tableHTMLExport", "~/Content/js/tableHTMLExport/tableHTMLExport.js");
            bundles.Add<StylesheetBundle>("quillStyles", "~/Content/css/quill-snow.css");
            bundles.Add<ScriptBundle>("quill", "~/Content/js/quill.js");


            // In production mode, all of ~/Content will be combined into a single bundle.

            // If you want a bundle per folder, try this:
            //   bundles.AddPerSubDirectory<ScriptBundle>("Scripts");
            // Each immediate sub-directory of ~/Scripts will be combined into its own bundle.
            // This is useful when there are lots of scripts for different areas of the website.
        }
    }
}