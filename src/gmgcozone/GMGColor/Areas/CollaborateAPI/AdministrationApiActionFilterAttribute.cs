﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GMGColor.Areas.CollaborateAPI
{
    public class AdministrationApiActionFilterAttribute: ActionFilterAttribute
    {
        private static readonly ILog _log = LogManager.GetLogger("CollaborateAPILogger");

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.RouteData.Values["controller"];
            var action = filterContext.RouteData.Values["action"];

            if (controller.ToString().Equals("AdministrationAPI")
                && action.ToString().ToLower().Equals("user"))
            {
                foreach (object parameter in filterContext.ActionParameters.Values)
                {
                    LogActionInfo(action.ToString(), parameter);
                }
            }

        }

        private void LogActionInfo(string action, object inputModel)
        {
            var properties = inputModel.GetType().GetProperties();
            var stringBuilder = new StringBuilder();

            foreach (PropertyInfo propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(inputModel, null);

                string propertyName = propertyInfo.Name.ToLower();

                if (propertyName == "permissions" || propertyName=="groups")
                {
                    ProcessList(value, stringBuilder, propertyInfo); 
                }
                else if (value != null)
                {
                    stringBuilder.AppendFormat(propertyInfo.Name + ": {0} | ", value);
                }
            }
            
            _log.InfoFormat("Action {0} | {1}", new object[] { action, stringBuilder.ToString() });
        }


        private static void ProcessList(object propertyInfo, StringBuilder stringBuilder, PropertyInfo ppropertyInfo)
        {
            try
            {
                IList objectList = propertyInfo as IList;
                
                if (objectList != null)
                {
                    stringBuilder.AppendFormat(" {0}: {{", ppropertyInfo.Name);

                    for (int i = 0; i < objectList.Count; i++)
                    {
                        PropertyInfo[] objProperties = objectList[i].GetType().GetProperties();

                        foreach (var objProperty in objProperties)
                        {
                            string property = objProperty.Name;
                            object value = null;

                            if (property != "Chars")
                            {
                                value = objProperty.GetValue(objectList[i], null);
                            }
                            else
                            {
                                // In case of compareversions call parsing
                                ProcessVersionEntries(objectList, ref property, ref value);
                            }

                            value = ProcessPhasesCollaborators(stringBuilder, property, value);

                            if (value != null)
                            {
                                stringBuilder.AppendFormat(" {0}: {1},", property, value.ToString());
                            }
                        }

                        AppendLastSeparator(stringBuilder, objectList, i);
                    }

                    stringBuilder.Append("} | ");
                }

            }
            catch
            {

            }
        }

        private static void ProcessVersionEntries(IList objectList, ref string property, ref object value)
        {
            property = "guid";

            foreach (var obj in objectList)
            {
                value = value + "; " + obj.ToString();
            }
        }

        private static void AppendLastSeparator(StringBuilder stringBuilder, IList objectList, int i)
        {
            if (i < objectList.Count - 1)
            {
                stringBuilder.Append(" | ");
            }
        }

        private static object ProcessPhasesCollaborators(StringBuilder stringBuilder, string property, object value)
        {
            // In case of phases's collaborators
            if (property == "collaborators")
            {
                stringBuilder.Append(" { Collaborators: { ");

                foreach (var collaborator in (IList)value)
                {
                    stringBuilder.AppendFormat("{0} | ", collaborator.ToString());
                }

                stringBuilder.Remove(stringBuilder.Length - 2, 2);

                stringBuilder.Append(" } | ");

                value = null;
            }

            return value;
        }
    }
}