﻿using System.Text;
using System.Web.Mvc;
using log4net;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

namespace GMGColor.Areas.CollaborateAPI
{
    public class CollaborateApiActionFilterAttribute : ActionFilterAttribute
    {
        private static readonly ILog _log = LogManager.GetLogger("CollaborateAPILogger");

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.RouteData.Values["controller"];
            var action = filterContext.RouteData.Values["action"];

            if (controller.ToString().Equals("CollaborateAPI") 
                && !action.ToString().ToLower().Equals("getjobstatus") 
                && !action.ToString().ToLower().Equals("startsession") 
                && !action.ToString().ToLower().Equals("confirmidentity"))
            {
                foreach (object parameter in filterContext.ActionParameters.Values)
                {
                    LogActionInfo(action.ToString(), parameter);
                }
            }
        }

        private void LogActionInfo(string action, object inputModel)
        {
            var properties = inputModel.GetType().GetProperties();
            var stringBuilder = new StringBuilder();

            foreach (PropertyInfo propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(inputModel, null);

                string propertyName = propertyInfo.Name.ToLower();

                if (propertyName == "approval")
                {
                    var subproperties = propertyInfo.GetValue(inputModel).GetType().GetProperties();
                    var stringBuilder1 = new StringBuilder();
                    foreach (PropertyInfo subpropertyInfo in subproperties)
                    {
						var newValue = subpropertyInfo.GetValue(value);
						if (newValue != null)
						{
							if (newValue is ICollection)
							{
								ProcessList(value, stringBuilder, subpropertyInfo);

							}
							else
							{
								if (newValue.GetType().IsPrimitive || newValue.GetType() == typeof(string))
								{
									stringBuilder.AppendFormat(subpropertyInfo.Name + ": {0} | ", newValue);
								}
								else
								{
									var objectProperties = newValue.GetType().GetProperties();
									if (objectProperties.Length > 0)
									{
										var objectStringBuilder = new StringBuilder();
										foreach (var property in objectProperties)
										{
											objectStringBuilder.AppendFormat(property.Name + ": {0} | ", property.GetValue(newValue));
										}
										stringBuilder.AppendFormat(subpropertyInfo.Name + ": {0}", objectStringBuilder.ToString());
									}
								}								
							}
						}						
					}
                }
				else if (value != null && (value is ICollection))
                {
                    ProcessList(value,stringBuilder,propertyInfo);
                }
                else if (value != null)
                {
                    stringBuilder.AppendFormat(propertyInfo.Name + ": {0} | ", value);
                }
            }

                

            _log.InfoFormat("Action {0} | {1}", new object[] { action, stringBuilder.ToString() });
        }


        private static void ProcessList(object propertyInfo, StringBuilder stringBuilder, PropertyInfo ppropertyInfo)
        {
            try
            {
                IList objectList;
                if(propertyInfo is IEnumerable)
                {
                     objectList = propertyInfo as IList;
                }
                else
                {
                     objectList = (IList)ppropertyInfo.GetValue(propertyInfo);
                }
               
                if (objectList != null)
                    {
                        stringBuilder.AppendFormat(" {0}: {{", ppropertyInfo.Name);

                        for (int i = 0; i < objectList.Count; i++)
                        {
                            PropertyInfo[] objProperties = objectList[i].GetType().GetProperties();

                            foreach (var objProperty in objProperties)
                            {
                                string property = objProperty.Name;
                                object value = null;

                                if (property != "Chars")
                                {
                                    value = objProperty.GetValue(objectList[i], null);
                                }
                                else
                                {
                                    // In case of compareversions call parsing
                                    ProcessVersionEntries(objectList, ref property, ref value);
                                }

                                value = ProcessPhasesCollaborators(stringBuilder, property, value);

                                if (value != null)
                                {
                                    stringBuilder.AppendFormat(" {0}: {1},", property, value.ToString());
                                }
                            }

                            AppendLastSeparator(stringBuilder, objectList, i);
                        }

                        stringBuilder.Append("} | ");
                    }
                
            }
            catch
            {

            }
        }

        private static void ProcessVersionEntries(IList objectList, ref string property, ref object value)
        {
            property = "guid";

            foreach (var obj in objectList)
            {
                value = value + "; " + obj.ToString();
            }
        }

        private static void AppendLastSeparator(StringBuilder stringBuilder, IList objectList, int i)
        {
            if (i < objectList.Count - 1)
            {
                stringBuilder.Append(" | ");
            }
        }

        private static object ProcessPhasesCollaborators(StringBuilder stringBuilder, string property, object value)
        {
            // In case of phases's collaborators
            if (property == "collaborators")
            {
                stringBuilder.Append(" { Collaborators: { ");

                foreach (var collaborator in (IList)value)
                {
                    stringBuilder.AppendFormat("{0} | ", collaborator.ToString());
                }

                stringBuilder.Remove(stringBuilder.Length - 2, 2);

                stringBuilder.Append(" } | ");

                value = null;
            }

            return value;
        }
    }
}