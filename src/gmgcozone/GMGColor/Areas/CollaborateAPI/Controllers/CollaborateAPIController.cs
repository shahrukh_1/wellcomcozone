﻿using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using GMG.CollaborateAPI.BL;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.Approval_Models;
using GMG.CollaborateAPI.BL.Models.FolderModels;
using GMG.CollaborateAPI.BL.Repositories;
using GMG.CollaborateAPI.BL.Validators;
using GMG.WebHelpers.Job;
using GMGColor.ActionFilters;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColor.Common;

using log4net;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;
using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators;
using GMG.CollaborateAPI.BL.Models.JobModels;

namespace GMGColor.Areas.CollaborateAPI.Controllers
{
    [ModelStateValidationFilter]
    [CollaborateApiActionFilter]
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class CollaborateAPIController : GenericController
    {
        #region Private Fields

        private static readonly ILog _log = LogManager.GetLogger("CollaborateAPILogger");
        private static JobStatusDetails endpoint;

        #endregion

        #region API Methods

        #region GET Methods

        #region Authentication

        [HttpGet]
        [ActionName("startsession")]
        public JsonResult StartSession(StartSessionRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new SessionRepository(Context);
            var response = repository.ProcessStartupRequest(model, out resultCode);

            Response.StatusCode = (int) resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("confirmidentity")]
        public JsonResult ConfirmIdentity(ConfirmIdentityRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new SessionRepository(Context);
            var response = repository.ProcessConfirmationRequest(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpGet]
        [ActionName("job")]
        public JsonResult GetJobs(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new JobRepository(Context);
            object response;
            if (model.guid != null)
            {
                response = repository.GetJob(model, out resultCode);
                Response.StatusCode = (int)resultCode;
            }
            else
            {
                response = repository.ListJobs(model, out resultCode);
                Response.StatusCode = (int)resultCode;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetJobStatus(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new JobRepository(Context);
            var response = repository.GetJobActivity(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetJobAnnotationReportUrl(AnnotationReportRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetJobAnnotationReportUrl(model, "/Approvals/ExternalAnnotations", out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("approval")]
        public JsonResult GetApproval(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetApproval(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetApprovalWithAccess(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetApprovalWithAccess(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetApprovalStatus(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetApprovalActivity(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAnnotationReport(AnnotationReportRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetAnnotationReport(model, "/Approvals/ExternalAnnotations", out resultCode);

            Response.StatusCode = (int)resultCode;
            if (resultCode == HttpStatusCodeEnum.Ok)
            {
                return File(response as Stream, "image/png");
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }                      
        }

        [HttpGet]
        public ActionResult GetAnnotationReportUrl(AnnotationReportRequest model)
        {
            HttpStatusCodeEnum resultCode;
            var repository = new ApprovalRepository(Context);
            var response = repository.GetAnnotationReportUrl(model, "/Approvals/ExternalAnnotations", out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAnnotationList(AnnotationListRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetAnnotationList(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetProofStudioURL(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetProofStudioURL(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("folders")]
        public JsonResult GetAllFolders(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new FolderRepository(Context);
            var response = repository.GetAllFolders(model, out resultCode);
            Response.StatusCode = (int)resultCode;

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [ActionName("folder")]
        public JsonResult GetFolderByName(GetFolderByNameRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new FolderRepository(Context);
            var response = repository.GetFolderByName(model, out resultCode);
            Response.StatusCode = (int)resultCode;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("workflow")]
        public JsonResult GetAllApprovalWorkflows(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;
            var repository = new ApprovalWorkflowRepository(Context);
            object response;
            if (model.guid != null)
            {
                response = repository.GetApprovalWorkflowDetails(model, out resultCode); 
                Response.StatusCode = (int)resultCode;
            }
            else
            {
                response = repository.GetAllWorkflows(model, out resultCode);
                Response.StatusCode = (int)resultCode;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetApprovalWorkflowDetails(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalWorkflowRepository(Context);
            var response = repository.GetApprovalWorkflowDetails(model, out resultCode);
            Response.StatusCode = (int)resultCode;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("checklist")]
        public JsonResult GetAllApprovalChecklist(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;
            var repository = new ChecklistRepository(Context);
            object response;
            response = repository.GetCheckListDetails(model, out resultCode);
            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region POST Methods

        [HttpPost]
        [ActionName("job")]
        public JsonResult CreateJob(CreateJobRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new JobRepository(Context);
            var response = repository.CreateJobMetadata(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateApprovalVersion(CreateApprovalRequest model)
        {
            HttpStatusCodeEnum resultCode;
            
            var repository = new JobRepository(Context);
            var response = repository.CreateApprovalVersionMetadata(model, out resultCode);
            Response.StatusCode = (int)resultCode;
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("upload-file")]
        public JsonResult UploadFile(UploadApprovalFileRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var fileStream = new MemoryStream();
            using (Stream responseStream = Request.InputStream)
            {
                responseStream.CopyTo(fileStream);
                fileStream.Seek(0, SeekOrigin.Begin);
            } 

            var repository = new JobRepository(Context);
            var response = repository.CreateJob(model, fileStream, out resultCode);
            Response.StatusCode = (int)resultCode;

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ActionName("folder")]
        public JsonResult CreateFolder(CreateFolderRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new FolderRepository(Context);
            var response = repository.CreateFolder(model, out resultCode);
            Response.StatusCode = (int)resultCode;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("compareVersions")]
        public JsonResult GetProofStudioCompareVersionsURL(ProofStudioCompareVersionsRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.GetCompareVersionsProofStudioURL(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PUT Methods

        [HttpPut]
        [ActionName("job")]
        public JsonResult UpdateJob(UpdateJobRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;

            var repository = new JobRepository(Context);
            var response = repository.UpdateJob(model, out resultCode, out errors);
            Response.StatusCode = (int)resultCode;

            if (!string.IsNullOrEmpty(errors))
            {
                _log.Error(errors);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        [ActionName("approval")]
        public JsonResult UpdateApproval(UpdateApprovalRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;

            var repository = new ApprovalRepository(Context);
            var response = repository.UpdateApprovalVersion(model, out resultCode, out errors);
            Response.StatusCode = (int)resultCode;

            if (!string.IsNullOrEmpty(errors))
            {
                _log.Error(errors);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        [ActionName("approvalPhaseDeadline")]
        public JsonResult UpdateApprovalPhaseDeadline(UpdateApprovalPhaseDeadlineRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;

            var repository = new ApprovalRepository(Context);
            var response = repository.UpdateApprovalPhaseDeadline(model, out resultCode, out errors);
            Response.StatusCode = (int)resultCode;

            if (!string.IsNullOrEmpty(errors))
            {
                _log.Error(errors);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public JsonResult RestoreApproval(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.RestoreApproval(model, out resultCode);
            Response.StatusCode = (int)resultCode;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public JsonResult UpdateApprovalDecision(UpdateApprovalDecisionRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;

            var repository = new ApprovalRepository(Context);
            var response = repository.UpdateApprovalDecision(model, out resultCode, out errors);
            Response.StatusCode = (int)resultCode;

            if (!string.IsNullOrEmpty(errors))
            {
                Response.StatusCode = (int)HttpStatusCodeEnum.InternalServerError;
                _log.Error(errors);
            }
            Type objtype = response.GetType();
            if (objtype.FullName.Contains("RuleViolation"))
            {
                _log.Error(Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public JsonResult RerunFromThisPhase(RerunFromThisPhaseRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;

            var repository = new ApprovalWorkflowRepository(Context);
            var response = repository.RerunFromThisPhase(model, out resultCode, out errors);

            if (!string.IsNullOrEmpty(errors))
            {
                _log.Error(errors);
            }

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ActionName("workflow")]
        public JsonResult CreateApprovalWorkflow(CreateApprovalWorkflowRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;
            
            var repository = new ApprovalWorkflowRepository(Context);
            var response = repository.CreateApprovalWorkflow(model, out resultCode, out errors);

            if (!string.IsNullOrEmpty(errors))
            {
                _log.Error(errors);
            }

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        [HttpPut]    
        public JsonResult AddApprovalPhaseCollaborators(AddCollaboratorsToApprovalPhaseRequest model)
        {
            HttpStatusCodeEnum resultCode;
            string errors;
            var repository = new ApprovalRepository(Context);
            var response = repository.AddCollaboratorsToApprovalPhaseRequest(model, out resultCode, out errors);

            if (!string.IsNullOrEmpty(errors))
            {
                _log.Error(errors);
            }
            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ActionName("job")]
        public JsonResult DeleteJob(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new JobRepository(Context);
            var response = repository.DeleteJob(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        [ActionName("approval")]
        public JsonResult DeleteApproval(CollaborateAPIIdentityModel model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new ApprovalRepository(Context);
            var response = repository.DeleteApproval(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region POST and GET Methods for Job status (only for test application)

        [HttpPost]
        [ActionName("pushstatus")]
        public JsonResult PushStatus(JobStatusDetails model)
        {
            HttpStatusCodeEnum resultCode = HttpStatusCodeEnum.Ok;

            endpoint = model;
            Response.StatusCode = (int)resultCode;

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("getstatus")]
        public JsonResult GetStatus()
        {
            HttpStatusCodeEnum resultCode = HttpStatusCodeEnum.Ok;

            var response = endpoint;
            Response.StatusCode = (int)resultCode;
            endpoint = new JobStatusDetails();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Events

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                string action = filterContext.RouteData.Values["action"].ToString();
                string sessionKey = Request.Params.Get("sessionKey") ?? String.Empty;
                _log.Error(String.Format("Error occured in action {0} for sessionKey {1}", action, sessionKey), filterContext.Exception);

                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        success = false,
                        message = ValidationErrorMessages.GenericError
                    }
                };
                filterContext.ExceptionHandled = true;
            }
            base.OnException(filterContext);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            View(new ResourceNotFound()).ExecuteResult(ControllerContext);
            Response.StatusCode = (int)HttpStatusCode.NotFound;
        }

        #endregion

        #region Util Methods

        public class ResourceNotFound : IView
        {
            public void Render(ViewContext viewContext, System.IO.TextWriter writer)
            {
                writer.WriteLine(Resources.Resources.lblApiResourceNotFound);
            }
        }

        #endregion
    }
}
