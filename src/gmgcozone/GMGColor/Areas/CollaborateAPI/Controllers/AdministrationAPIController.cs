﻿using System.Web.Mvc;
using GMG.CollaborateAPI.BL;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.UserModels;
using GMG.CollaborateAPI.BL.Repositories;
using GMGColor.ActionFilters;
using GMGColor.Common;
using log4net;

namespace GMGColor.Areas.CollaborateAPI.Controllers
{
   

    [ModelStateValidationFilter]
    [AdministrationApiActionFilter]
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class AdministrationAPIController : GenericController
    {
        
        [HttpGet]
        public JsonResult UserExists(UserExistsRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new UserRepository(Context);
            var response = repository.CheckIfUserExists(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("user")]
        public JsonResult CreateUser(CreateUserRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new UserRepository(Context);
            var response = repository.CreateUser(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response);
        }

        [HttpPost]
        [ActionName("external")]
        public JsonResult CreateExternalUser(CreateExternalUserRequest model)
        {
            HttpStatusCodeEnum resultCode;

            var repository = new UserRepository(Context);
            var response = repository.CreateExternalUser(model, out resultCode);

            Response.StatusCode = (int)resultCode;
            return Json(response);
        }
    }
}
