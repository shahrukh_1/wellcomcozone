﻿using System.Web.Mvc;
using GMGColor.Common;

namespace GMGColor.Areas.CollaborateAPI
{
    public class CollaborateAPIAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CollaborateAPIv1";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               "CollaborateAPIv1_userexists",
               "api/v1/user/exists",
               new { controller = "AdministrationAPI", action = "UserExists", },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
               "CollaborateAPIv1_createexternaluser",
               "api/v1/external/{guid}",
               new { controller = "AdministrationAPI", action = "external", guid = UrlParameter.Optional },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
               "CollaborateAPIv1_createuser",
               "api/v1/user/{guid}",
               new { controller = "AdministrationAPI", action = "user", guid = UrlParameter.Optional },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
                "CollaborateAPIv1_default",
                "api/v1/{action}/{guid}",
                new { controller = "CollaborateAPI", guid = UrlParameter.Optional },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
               "CollaborateAPIv1_approvalstatus",
               "api/v1/approval/{guid}/status",
               new { controller = "CollaborateAPI", action = "GetApprovalStatus" },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
                "CollaborateAPIv1_jobstatus",
                "api/v1/job/{guid}/status",
                new { controller = "CollaborateAPI", action = "GetJobStatus" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_annreport",
                "api/v1/approval/{guid}/report",
                new { controller = "CollaborateAPI", action = "GetAnnotationReport" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_annlist",
                "api/v1/approval/{guid}/annotationlist",
                new { controller = "CollaborateAPI", action = "GetAnnotationList" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_appraccess",
                "api/v1/approval/{guid}/access",
                new { controller = "CollaborateAPI", action="GetApprovalWithAccess" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_createvers",
                "api/v1/job/{guid}/approval",
                new { controller = "CollaborateAPI", action = "CreateApprovalVersion" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_restorejob",
                "api/v1/job/{guid}/restore",
                new { controller = "CollaborateAPI", action="RestoreJob" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_restoreapproval",
                "api/v1/approval/{guid}/restore",
                new { controller = "CollaborateAPI", action = "RestoreApproval" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_proofstudio",
                "api/v1/approval/{guid}/proofstudio",
                new { controller = "CollaborateAPI", action = "GetProofStudioURL" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
               "CollaborateAPIv1_pushstatus",
               "api/v1/pushstatus",
               new { controller = "CollaborateAPI", action = "PushStatus" },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
               "CollaborateAPIv1_getstatus",
               "api/v1/getstatus",
               new { controller = "CollaborateAPI", action = "GetStatus" },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
              "CollaborateAPIv1_approvaldecision",
              "api/v1/approval/{guid}/decision",
              new { controller = "CollaborateAPI", action = "UpdateApprovalDecision" },
              new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
          );

            context.MapRoute(
                "CollaborateAPIv1_annreportUrl",
                "api/v1/approval/{guid}/reporturl",
                new { controller = "CollaborateAPI", action = "GetAnnotationReportUrl" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
                "CollaborateAPIv1_jobannotationreporturl",
                "api/v1/job/{guid}/reporturl",
                new { controller = "CollaborateAPI", action = "GetJobAnnotationReportUrl" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
               "CollaborateAPIv1_foldername",
               "api/v1/folder",
               new { controller = "CollaborateAPI", action = "GetFolderByName"},
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );

            context.MapRoute(
                "CollaborateAPIv1_rerunworkflow",
                "api/v1/approval/{guid}/rerunworkflow",
                new { controller = "CollaborateAPI", action = "RerunFromThisPhase" },
                new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
            );

            context.MapRoute(
               "CollaborateAPIv1_addapprovalphasecollaborators",
               "api/v1/approval/{guid}/addphasecollaborators",
               new { controller = "CollaborateAPI", action = "AddApprovalPhaseCollaborators" },
               new { subdomain = new IncludeSubdomainRouteConstraint(new[] { "app" }) }
           );
        }
    }
}
