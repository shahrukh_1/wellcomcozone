﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using GMGColorDAL;


namespace GMGColor.ActionFilters
{
    public class TimeElapsedFilterAttribute : ActionFilterAttribute
    {
        public TimeElapsedFilterAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var stopwatch = new Stopwatch();
            filterContext.HttpContext.Items["TimeElapsedFilterAttribute"] = stopwatch;

            stopwatch.Start();
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var stopwatch = filterContext.HttpContext.Items["TimeElapsedFilterAttribute"] as Stopwatch;
            if (stopwatch != null)
            {
                stopwatch.Stop();

                var httpContext = filterContext.HttpContext;
                var response = httpContext.Response;

                string controller = filterContext.RouteData.Values.ContainsKey("controller")
                                    ? Convert.ToString(filterContext.RouteData.Values["controller"])
                                    : string.Empty;


                string view = filterContext.RouteData.Values.ContainsKey("action")
                                    ? Convert.ToString(filterContext.RouteData.Values["action"])
                                    : string.Empty;

                string altview = filterContext.Result is ViewResultBase
                                  ? (filterContext.Result as ViewResultBase).ViewName
                                  : string.Empty;

                GMGColorLogging.log.InfoFormat("Time elapsed {0} ms in {1} / {2} /{3}", stopwatch.ElapsedMilliseconds, controller, view, altview);
            }
        }
    }
}