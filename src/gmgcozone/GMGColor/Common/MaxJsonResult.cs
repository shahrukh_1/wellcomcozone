﻿using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace GMGColor.Common
{
    public class MaxJsonResult: ActionResult
    {
        private readonly object data;

        public MaxJsonResult(object data, JsonRequestBehavior behavior = JsonRequestBehavior.DenyGet)
        {
            this.data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.RequestContext.HttpContext.Response;
            response.ContentType = "application/json";
            var serializer = new JavaScriptSerializer {MaxJsonLength = int.MaxValue};
            response.Write(serializer.Serialize(this.data));
        }
    }
}