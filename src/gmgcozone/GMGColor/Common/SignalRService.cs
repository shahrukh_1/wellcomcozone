﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using GMGColorDAL.CustomModels.Api;
using GMGColor.Hubs;
using GMGColorDAL.CustomModels;
using GMGColorBusinessLogic;
using GMG.CoZone.Common.DomainModels;

namespace GMGColor.Common
{
    public class SignalRService
    {
        public void AddInstantNotification(IHubContext context, InstantNotificationEntityType entityType, int versionId, int loggedUserId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Modified, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Version = versionId,
                    EntityType = entityType,
                    Creator = loggedUserId
                });
            }
        }

        public void AddInstantDashboardNotification(DashboardInstantNotification notification)
        {
            var clients = InstantNotificationsHub.GetDashboardClients(notification.Version);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();

            foreach (string client in clients)
            {
                context.Clients.Client(client).addDashboardNotification(notification);
            }
        }

        public void AddEditAnnotationsAddInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, int ownerId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Added, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = isExternal ? null : (int?)ownerId,
                    ExternalCreator = isExternal ? (int?)ownerId : null,
                    Version = versionId,
                    EntityType = entityType
                });
            }
        }

        public void AddInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, string guid, DbContextBL context)
        {
            var ownerId = ApiBL.GetCollaboratorId(context, isExternal, guid);
            AddEditAnnotationsAddInstantNotification(entityType, versionId, isExternal, ownerId);
        }

        public void UpdateInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, int ownerId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Modified, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = isExternal ? null : (int?)ownerId,
                    ExternalCreator = isExternal ? (int?)ownerId : null,
                    Version = versionId,
                    EntityType = entityType
                });
            }
        }

        public void UpdateInstantDashboardNotification(DashboardInstantNotification notification)
        {
            var clients = InstantNotificationsHub.GetDashboardClients(notification.Version);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();

            foreach (string client in clients)
            {
                context.Clients.Client(client).addDashboardNotification(notification);
            }
        }

        public void UpdateInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, string guid, DbContextBL context)
        {
            var ownerId = ApiBL.GetCollaboratorId(context, isExternal, guid);
            UpdateInstantNotification(entityType, versionId, isExternal, ownerId);
        }

        public void RemoveInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, int ownerId)
        {
            var clients = InstantNotificationsHub.GetClients(versionId);
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<InstantNotificationsHub>();
            foreach (string client in clients)
            {
                context.Clients.Client(client).addnotification(InstantNotificationOperationType.Deleted, new InstantNotification()
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = isExternal ? null : (int?)ownerId,
                    ExternalCreator = isExternal ? (int?)ownerId : null,
                    Version = versionId,
                    EntityType = entityType
                });
            }
        }

        public void RemoveInstantNotification(InstantNotificationEntityType entityType, int versionId, bool isExternal, string guid, DbContextBL context)
        {
            var ownerId = ApiBL.GetCollaboratorId(context, isExternal, guid);
            RemoveInstantNotification(entityType, versionId, isExternal, ownerId);
        }

        public void UpdateDashboardApprovalProcessStatus(IHubContext context, MediaDefineApprovalProgressStatusModel statusObject)
        {
            var clients = InstantNotificationsHub.GetDashboardClients(statusObject.ApprovalId.ToString());
            foreach (var client in clients)
            {
                context.Clients.Client(client).updateApprovalStatus(statusObject);
            }
        }
        public void UpdateDashboardApprovalProcessStatus(MediaDefineApprovalProgressStatusModel statusObject)
        {
            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<Hubs.InstantNotificationsHub>();

            UpdateDashboardApprovalProcessStatus(hubContext, statusObject);
        }
    }
}