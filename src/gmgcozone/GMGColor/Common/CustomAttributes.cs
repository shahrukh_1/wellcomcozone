﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using Elmah;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace GMGColor.Common
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MultiButtonAttribute : ActionNameSelectorAttribute
    {
        public string MatchFormKey { get; set; }
        public string MatchFormValue { get; set; }

        public override bool IsValidName(ControllerContext controllerContext, string actionName, System.Reflection.MethodInfo methodInfo)
        {
            return controllerContext.HttpContext.Request[MatchFormKey] != null &&
                   controllerContext.HttpContext.Request[MatchFormKey] == MatchFormValue;
        }
    }

    public class ValidateEmailAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            using (DbContextBL context = new DbContextBL())
            {
                if (DALUtils.SearchObjects<GMGColorDAL.Account>(
                    o =>
                    o.Status != GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Deleted, context).ID, context).Any(o => o.ContactEmailAddress == (string) value))
                {
                    return new ValidationResult(Resources.Resources.reqEmailExist);
                }
                else if (GMGColorDAL.DALUtils.SearchObjects<GMGColorDAL.User>(
                    o => o.Status != GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Deleted, context).ID, context).Any(o => o.EmailAddress == (string) value))
                {
                    return new ValidationResult(Resources.Resources.reqEmailExist);
                }

                return ValidationResult.Success;
            }
        }
    }

    public class ValidateDomainAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            using (DbContextBL context = new DbContextBL())
            {
                if (
                    DALUtils.SearchObjects<GMGColorDAL.Account>(
                        o =>
                        o.Domain.Replace(GMGColorConfiguration.AppConfiguration.HostAddress, string.Empty) ==
                        (string) value, context).Any())
                {
                    return new ValidationResult(Resources.Resources.reqDomainExist);
                }

                return ValidationResult.Success;
            }
        }
    }

    public class HandleErrorWithELMAHAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            base.OnException(context);

            var e = context.Exception;
            if (!context.ExceptionHandled   // if unhandled, will be logged anyhow
                    || RaiseErrorSignal(e)  // prefer signaling, if possible
                    || IsFiltered(context)) // filtered?
                return;

            LogException(e);
        }

        private static bool RaiseErrorSignal(Exception e)
        {
            var context = HttpContext.Current;
            if (context == null)
                return false;
            var signal = ErrorSignal.FromContext(context);
            if (signal == null)
                return false;
            signal.Raise(e, context);
            return true;
        }

        private static bool IsFiltered(ExceptionContext context)
        {
            var config = context.HttpContext.GetSection("elmah/errorFilter")
                                     as ErrorFilterConfiguration;

            if (config == null)
                return false;

            var testContext = new ErrorFilterModule.AssertionHelperContext(
                                                                context.Exception, HttpContext.Current);

            return config.Assertion.Test(testContext);
        }

        private static void LogException(Exception e)
        {
            var context = HttpContext.Current;
            ErrorLog.GetDefault(context).Log(new Error(e, context));
        }
    }
}