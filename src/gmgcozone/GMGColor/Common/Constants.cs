﻿namespace GMGColor.Common
{
    public class GMGColorConstants
    {
        //Collaborate Global Settings
        public const string AccountPagination = "accountPagination";
        public const string AllColDecisionRequired = "AllCollaboratorsDecisionRequired";
        public const string HideCompApprovals = "HideCompletedApprovalsInDashboard";
        public const string EnableFlexViewer = "EnableFlexViewer";
        public const string ProofStudioPenWidth = "ProofStudioPenWidth";
        public const string ChangesCompleteDefaultGroup = "ChangesCompleteDefaultGroup";
        public const string ShowAllFilesToAdmins = "ShowAllFilesToAdmins";
        public const string ShowAllFilesToManagers = "ShowAllFilesToManagers";
        public const string DisableExternalUsersEmailIndicator = "DisableExternalUsersEmailIndicator";
        public const string EnableRetoucherWorkflow = "EnableRetoucherWorkflow";
        public const string EnableCustomProfile = "EnableCustomProfile";
        public const string DisplayRetouchers = "DisplayRetouchers";
        public const string ShowBadges = "ShowBadges";
        public const string ProofStudioShowAnnotationsForExternalUsers = "ProofStudioShowAnnotationsForExternalUsers";
        public const int ProofStudioPenWidthDefaultValue = 2;
        public const string AdminHasFullRights = "AdminHasFullRights_";
        public const string InheritPermissionsFromParentFolder = "InheritPermissionsFromParentFolder";
        public const string DisableFileDueDateinUploader = "DisableFileDueDateinUploader";
        public const string DisableSOADIndicator = "DisableSOADIndicator";
        public const string DisableTagwordsInDashbord = "DisableTagwordsInDashbord";
        public const string DisableVisualIndicator = "DisableVisualIndicator"; 
        public const string KeepExternalUsersWhenUploadingNewVersion = "KeepExternalUsersWhenUploadingNewVersion";
        public const string EnableProofStudioZoomlevelFitToPageOnload = "EnableProofStudioZoomlevelFitToPageOnload";
        public const string ShowSubstrateFromICCProfileInProofStudio = "ShowSubstrateFromICCProfileInProofStudio";
        public const string ArchiveTimeLimit = "ArchiveTimeLimit";
        public const string ArchiveTimeLimitFormat = "ArchiveTimeLimitFormat";
        public const string EnableArchiveTimeLimit = "EnableArchiveTimeLimit";





        //User settings constants
        public const string RememberNumberOfFilesShown = "RememberNumberOfFilesShown";
        public const string ShowAllUsers = "ShowAllUsers";
        public const int DefaultNumberOfFilesShown = 10;

        //Account settings
        public const string AccountBrandingPagination = "brandingPagination";
    }
}