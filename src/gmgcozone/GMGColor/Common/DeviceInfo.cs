﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColor.Common
{
    public class DeviceInfo
    {
        public string DeviceOS { get; set; }
        public Version DeviceOSVersion { get; set; }
        public string ModelName { get; set; }
        public string UserAgent { get; set; }
        public GMGColorEnums.BrowserType  DeviceType { get; set; }
        public bool HasCookieSupport { get; set; }
    }
}
