﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using GMG.CoZone.Common;
using GMGColor.Infrastructure;
using GMGColorBusinessLogic;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;
using AppModule = GMG.CoZone.Common.AppModule;
using WURFL;
using System.Web.Caching;
using AutoMapper;
using GMG.CoZone.Collaborate.UI;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.DomainModels;

namespace GMGColor.Common
{
    public class BaseController : GenericController
    {
        public const string SoftProofingCookieName = "spcn";
        public const int SoftProofingCookieTimeout = 45;// minutes

        private IDownloadService _downlaodService;
        private IMapper _mapper;

        public BaseController(IDownloadService downlaodService, IMapper mapper)
        {
            _downlaodService = downlaodService;
            _mapper = mapper;
        }

        public struct DeliverJob
        {
            public string Title;
            public string Name;
            public string SourcePath;
            public Decimal Size;
            public int SelectedCpWorkflowId;
            public string PagesRadioButton;
            public string PagesRangeValue;
            public bool IncludeProofMetaInformation;
            public bool LogProofControlResults;
            public string Guid;
            public int TotalNrOfPages;
        }        

        #region Enums

        private enum EventNotificationTemplates { Digest_Daily, Digest_Weekly }
        
        protected enum Sites
        {
            // The user can access the administrator site only.
            AdministratorSite,
            // The user can access the application site only.
            ApplicationSite,
            // The user still not logged in administrator or application site.
            NotLoggedIn
        }

        #endregion

        #region Fields

        private LoggedUserInfo loggedUserInfo = null;
        private string _loggedUserCulture = null;
        private Role.RoleName? _loggedUserCollaborateRole;
        private Role.RoleName? _loggedUserFileTransferRole;
        private Role.RoleName? _loggedUserDeliverRole;
        private Role.RoleName? _loggedUserAdminRole;
        private Role.RoleName? _loggedUserSysAdminRole;
        private Role.RoleName? _loggedUserReportRole;


        private AccountSubscriptionDetails _accountSubscriptionDetails;
        private AccountBillingPlansInUse _accountBillingPlans;

        private List<UserMenuItemRoleView> lstControllerActions = null;

        private string domain = null;
        private const string appServiceController = "appservice";
        private const string accountController = "account";
        private const string addTrialAccountAction = "AddTrialAccount";
        private const string getTrialAccountsAction = "GetTrialAccounts";
        private const string SoftProofSubmitData = "SubmitData";
        private const string SoftProofStartSession = "StartSession";
        private const string SoftProofingController = "SoftProofing";
        private const string cacheAuthorizedControllerActions = "ACA_"; //authorized controller actions
        private const string cacheLoggedUser = "LU_";
        private const string cacheAuthorizedControlls = "AC";
        private const string cacheAccountSubscriptionDetails = "ASD_";
        private const string cacheAccountBillingPlans = "ABP_";

        private DeviceInfo deviceInfo = null;
        #endregion

        #region Properties

        protected AccountSubscriptionDetails LoggedAccountSubscriptionDetails 
        {
            get
            {
                if (_accountSubscriptionDetails == null)
                {
                    _accountSubscriptionDetails =
                        HttpRuntime.Cache.GetItemFromCache(GetAccountSubscriptionDetailsCacheKey()) as AccountSubscriptionDetails;
                    if (_accountSubscriptionDetails == null)
                    {
                        if (LoggedAccountType == AccountType.Type.Subscriber)
                        {
                            _accountSubscriptionDetails = new AccountSubscriptionDetails
                                                          {
                                                              CollaborateSubscriberPlan =
                                                                  LoggedAccount.CollaborateSubscriberPlan(Context),
                                                              DeliverSubscriberPlan =
                                                                  LoggedAccount.DeliverSubscriberPlan(Context)                                                             
                                                          };
                        }
                        else if (LoggedAccountType != AccountType.Type.GMGColor)
                        {
                            _accountSubscriptionDetails = new AccountSubscriptionDetails
                                                          {
                                                              CollaborateSubscriptionPlan =
                                                                  LoggedAccount.CollaborateSubscriptionPlan(Context),
                                                              DeliverSubscriptionPlan =
                                                                  LoggedAccount.DeliverSubscriptionPlan(Context)
                                                          };
                        }
                        else
                        {
                            //Sys Admin doesn't have subsriptions
                            _accountSubscriptionDetails = new AccountSubscriptionDetails();
                        }

                        HttpRuntime.Cache.AddToCache(GetAccountSubscriptionDetailsCacheKey(),
                            _accountSubscriptionDetails, 120);
                    }
                }

                return _accountSubscriptionDetails;
            }

        }

        protected AccountBillingPlansInUse LoggedAccountBillingPlans
        {
            get
            {
                if (_accountBillingPlans == null)
                {
                    _accountBillingPlans =
                        HttpRuntime.Cache.GetItemFromCache(GetAccountBillingPlansCacheKey()) as AccountBillingPlansInUse;
                    if (_accountBillingPlans == null)
                    {
                        if (LoggedAccountType != AccountType.Type.GMGColor)
                        {
                            _accountBillingPlans = new AccountBillingPlansInUse
                            {
                                 CollaborateBillingPlan = LoggedAccount.CollaborateBillingPlan(Context),
                                 DeliverBillingPlan = LoggedAccount.DeliverBillingPlan(Context)
                            };
                        }
                        else
                        {
                            //Sys Admin doesn't have subsriptions
                            _accountBillingPlans = new AccountBillingPlansInUse();
                        }

                        HttpRuntime.Cache.AddToCache(GetAccountBillingPlansCacheKey(),
                            _accountBillingPlans, 120);
                    }
                }

                return _accountBillingPlans;
            }

        }

        /// <summary>
        /// Gets the logged account of the system, or null if not exist.
        /// </summary>
        protected Account LoggedAccount
        {
            get
            {
                if (LoggedUserInfo != null && LoggedUserInfo.User != null && LoggedUserInfo.User.Account1 != null && LoggedUserInfo.User.Account1.ID > 0)
                {
                    AccountStatu.Status status = AccountStatu.GetAccountStatus(LoggedUserInfo.LoggedAccountStatusKey);
                    if (status != AccountStatu.Status.Active && status != AccountStatu.Status.Inactive)
                    {
                        // This is handled in the BaseController OnAuthorization.
                        // Should have to show error page, account suspended / deleted
                        Session["gmg_error"] = "Unauthorised";
                    }
                }
                else
                {
                    // This is handled in the BaseController OnAuthorization.
                    // Invalied account URL, no account configured to this URL and logged account is null
                    Session["gmg_error"] = "NotConfigured";
                }

                return LoggedUserInfo != null && LoggedUserInfo.User != null ? LoggedUserInfo.User.Account1 : null;
            }
        }

        public bool LoggedAccountHasSsoEnabled
        {
            get
            {
                var accountSettingsKey = Enum.GetName(typeof(AccountSettingsBL.AccountSettingsKeyEnum),
                    AccountSettingsBL.AccountSettingsKeyEnum.SingleSignOn);

                var accSsoSettings = Context.AccountSettings.Where(acs => acs.Account == LoggedAccount.ID && acs.Name == accountSettingsKey).FirstOrDefault();

                return accSsoSettings != null;
            }
        }

        /// <summary>
        /// Logged user info
        /// </summary>
        protected LoggedUserInfo LoggedUserInfo
        {
            get
            {
                if (loggedUserInfo == null)
                {
                    string accountDomain = (Request.Url != null) ? Request.Url.Host.ToLower().Trim() : Request.UrlReferrer.Host.ToLower().Trim();

                    if (accountDomain.StartsWith("www."))
                    {
                        accountDomain = accountDomain.Substring("www.".Length);
                    }

                    int? userId = Session["gmg_uid"] != null ? Session["gmg_uid"].ToString().ToInt().GetValueOrDefault() : (int?)null;
                    if (userId == 0 || userId == null)
                    {
                        userId = getUserSessionAccrossServers();
                        loggedUserInfo = UserBL.GetLoggedUserInfo(accountDomain, userId, Context);
                        if (Session["gmg_aid"] == null && userId != null)
                        {
                            Session["gmg_aid"] = loggedUserInfo.User.Account;
                        }
                    }
                    else
                    {
                        loggedUserInfo = UserBL.GetLoggedUserInfo(accountDomain, userId, Context);
                    }
                    if(accountDomain.Contains("support."))
                    {
                        loggedUserInfo = UserBL.GetLoggedUserInfo(accountDomain.Replace("support.", ""), userId, Context);
                    }

                }
                return loggedUserInfo;
            }
            set
            {
                loggedUserInfo = value;
            }
        }

        /// <summary>
        /// Gets the logged account type of the system
        /// </summary>
        protected AccountType.Type LoggedAccountType
        {
            get
            {
                return AccountType.GetAccountType(LoggedUserInfo.LoggedAccountTypeKey);
            }
        }

        protected Theme.ColorScheme LoggedAccountTheme  
        {
            get { return Theme.GetColorScheme(LoggedUserInfo.LoggedAccountThemeKey); }
        }

        /// <summary>
        /// Gets the logged account status 
        /// </summary>
        protected AccountStatu.Status LoggedAccountStatus
        {
            get
            {
                return AccountStatu.GetAccountStatus(LoggedUserInfo.LoggedAccountStatusKey);
            }
        }

        protected string LoggedAccountCulture
        {
            get { return LoggedUserInfo.LoggedAccountCulture; }
        }

        protected string LoggedUserCulture
        {
            get
            {
                if (_loggedUserCulture == null && LoggedUser != null)
                {
                    _loggedUserCulture = Context.Locales.FirstOrDefault(t => t.ID == LoggedUser.Locale).Name;
                }

                return _loggedUserCulture;
            }
        }

        protected string LoggedUserBrandingHeaderLogo
        {
            get { return loggedUserInfo.LoggedUserBrandingHeaderLogo; }
        }

        /// <summary>
        /// Gets the logged user who accessing the page, or null if no user is valid.
        /// </summary>
        protected GMGColorDAL.User LoggedUser
        {
            get
            {
                if (LoggedUserInfo != null)
                {
                    if (LoggedUserInfo.User == null || LoggedUserInfo.User.ID == 0)
                    {
                        int? userId = Session["gmg_uid"] != null ? Session["gmg_uid"].ToString().ToInt().GetValueOrDefault() : (int?)null;
                        if (userId == 0 || userId == null)
                        {
                            userId = getUserSessionAccrossServers();
                            GMGColorLogging.log.WarnFormat("LoggedUser userId  {0} ", userId);
                        }
                        if (userId != null)
                        {
                            LoggedUserInfo.User = Context.Users.FirstOrDefault(u => u.ID == userId);
                        }
                    }
                    return LoggedUserInfo.User;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                LoggedUserInfo.User = value;
            }
        }

        /// <summary>
        /// Gets the logged user status 
        /// </summary>
        protected GMGColorDAL.UserStatu.Status LoggedUserStatus
        {
            get
            {
                return GMGColorDAL.UserStatu.GetUserStatus(LoggedUserInfo.User.UserStatu.Key);
            }
        }

        /// <summary>
        /// Gets logged user role on Collaborate
        /// </summary>
        protected Role.RoleName LoggedUserCollaborateRole
        {
            get
            {
                if (_loggedUserCollaborateRole == null)
                {
                    if (LoggedAccountType == AccountType.Type.GMGColor)
                    {
                        _loggedUserCollaborateRole = Role.RoleName.AccountNone;
                    }
                    else
                    {
                        if (Session[Constants.CollaborateRoleIdInSession + LoggedUser.ID] == null)
                        {
                            Session[Constants.CollaborateRoleIdInSession + LoggedUser.ID] =
                                LoggedUser.CollaborateRoleKey(Context);
                        }

                        _loggedUserCollaborateRole = Role.GetRole(Session[Constants.CollaborateRoleIdInSession + LoggedUser.ID].ToString());
                    }
                }

                return _loggedUserCollaborateRole.Value;
            }
        }


         /// <summary>
        /// Gets logged user role on FileTransfer
        /// </summary>
        protected Role.RoleName LoggedUserFileTransferRole
        {
            get
            {
                if (_loggedUserFileTransferRole == null)
                {
                    if (LoggedAccountType == AccountType.Type.GMGColor)
                    {
                        _loggedUserFileTransferRole = Role.RoleName.AccountNone;
                    }
                    else
                    {
                        if (Session[Constants.FileTransferIdInSession + LoggedUser.ID] == null)
                        {
                            Session[Constants.FileTransferIdInSession + LoggedUser.ID] =
                                LoggedUser.FileTransferRoleKey(Context);
                        }

                        _loggedUserFileTransferRole = Role.GetRole(Session[Constants.FileTransferIdInSession + LoggedUser.ID].ToString());
                    }
                }

                return _loggedUserFileTransferRole.Value;
            }
        }

        protected Role.RoleName LoggedUserReportRole
        {
            get
            {
                if (_loggedUserReportRole == null)
                {
                    if (LoggedAccountType == AccountType.Type.GMGColor)
                    {
                        _loggedUserReportRole = Role.RoleName.AccountNone;
                    }
                    else
                    {
                        if (Session[Constants.ReportIdInSession + LoggedUser.ID] == null)
                        {
                            Session[Constants.ReportIdInSession + LoggedUser.ID] =
                                LoggedUser.ReportRoleKey(Context);
                        }

                        _loggedUserReportRole = Role.GetRole(Session[Constants.ReportIdInSession + LoggedUser.ID].ToString());
                    }
                }

                return _loggedUserReportRole.Value;
            }
        }


        /// <summary>
        /// Gets logged user role on Deliver
        /// </summary>
        protected Role.RoleName LoggedUserDeliverRole
        {
            get
            {
                if (_loggedUserDeliverRole == null)
                {
                    if (LoggedAccountType == AccountType.Type.GMGColor)
                    {
                        _loggedUserDeliverRole = Role.RoleName.AccountNone;
                    }
                    else
                    {
                        if (Session[Constants.DeliverRoleIdInSession + LoggedUser.ID] == null)
                        {							
							Session[Constants.DeliverRoleIdInSession + LoggedUser.ID] = LoggedUser.DeliverRoleKey(Context);
						}

                        _loggedUserDeliverRole = Role.GetRole(Session[Constants.DeliverRoleIdInSession + LoggedUser.ID].ToString());
                    }
                }

                return _loggedUserDeliverRole.Value;
            }
        }      

        /// <summary>
        /// Gets logged user role for Administration
        /// </summary>
        protected Role.RoleName LoggedUserAdminRole
        {
            get
            {
                if (_loggedUserAdminRole == null)
                {
                    if (LoggedAccountType == AccountType.Type.GMGColor)
                    {
                        _loggedUserAdminRole = Role.RoleName.AccountNone;
                    }
                    else
                    {
                        if (Session[Constants.AdminRoleIdInSession + LoggedUser.ID] == null)
                        {
                            Session[Constants.AdminRoleIdInSession + LoggedUser.ID] = LoggedUser.AdminRoleKey(Context);
                        }

                        _loggedUserAdminRole = Role.GetRole(Session[Constants.AdminRoleIdInSession + LoggedUser.ID].ToString());
                    }
                }

                return _loggedUserAdminRole.Value;
            }
        }

        /// <summary>
        /// Gets logged user role for SysAdmin
        /// </summary>
        protected Role.RoleName LoggedUserSysAdminRole
        {
            get
            {
                if (_loggedUserSysAdminRole == null)
                {
                    if (Session[Constants.SysAdminRoleIdInSession + LoggedUser.ID] == null)
                    {
                        Session[Constants.SysAdminRoleIdInSession + LoggedUser.ID] = LoggedUser.SysAdminRoleKey(Context);
                    }

                    _loggedUserSysAdminRole = Role.GetRole(Session[Constants.SysAdminRoleIdInSession + LoggedUser.ID].ToString());
                }

                return _loggedUserSysAdminRole.Value;
            }
        }

        /// <summary>
        /// Gets the logged user temp folder path
        /// </summary>
        protected string LoggedUserTempFolderPath
        {
            get
            {
                int? loggedUserId = this.LoggedUser != null ? (int?)this.LoggedUser.ID : null;
                int? loggedAccountId = this.LoggedAccount != null ? (int?) this.LoggedAccount.ID : null;
                string accountRegion = this.LoggedAccount != null
                                           ? (this.LoggedAccount.Region ?? string.Empty)
                                           : string.Empty;
                return GMGColorDAL.User.GetUserTempFolderPath( loggedUserId, loggedAccountId, accountRegion);
            }
        }

        /// <summary>
        /// Gets the logged user authorized sites
        /// </summary>
        protected SitePermissions LoggedUserPermissions
        {
            get
            {
                bool IsGMGAdmin = LoggedAccountType == AccountType.Type.GMGColor;

                bool isAcountAdmin = LoggedUserAdminRole == Role.RoleName.AccountAdministrator;

                return IsGMGAdmin 
                           ? SitePermissions.AdministratorSiteOnly
                           : (isAcountAdmin && (LoggedAccountType != GMGColorDAL.AccountType.Type.Subscriber) ? SitePermissions.BothSites : SitePermissions.ApplicationSiteOnly);
            }
        }

        /// <summary>
        /// gmg_app value is 0 then user was logged in to the proof application,
        /// if value  is 1 then user logged in to the administrator application and
        /// if gmg_app is not exist or null then user not logged in to the system.
        /// </summary>
        protected Sites LoggedSite
        {
            get
            {
                switch (this.LoggedUserPermissions)
                {
                    case SitePermissions.AdministratorSiteOnly: return Sites.AdministratorSite;
                    case SitePermissions.ApplicationSiteOnly: return Sites.ApplicationSite;
                    case SitePermissions.BothSites:
                    default:
                        {
                            if (Session["gmgc_app"] != null)
                            {
                                if (Convert.ToBoolean(Session["gmgc_app"].ToString().ToLower()))
                                {
                                    return Sites.AdministratorSite;
                                }
                                else
                                {
                                    return Sites.ApplicationSite;
                                }
                            }
                            return Sites.NotLoggedIn;
                        }
                }
            }
        }

        private List<string> DefaultAuthorizedActions
        {
            get
            {
                List<string> defaultActions = new List<string>();
                defaultActions.Add("externalidp");
                defaultActions.Add("samllogin");
                defaultActions.Add("idpresponse");
                defaultActions.Add("authlogin");
                defaultActions.Add("login");
                defaultActions.Add("logout");
                defaultActions.Add("error");
                defaultActions.Add("welcome");
                defaultActions.Add("forgotpassword");
                defaultActions.Add("redirect");
                defaultActions.Add("redirecthtml5");
                defaultActions.Add("getpageimage");
                defaultActions.Add("acceptrejectcolorproofworkflow");
                defaultActions.Add("getapprovalthumbnailpath");
                defaultActions.Add("externaluserjobdetaillink");
                defaultActions.Add("externalannotations");
                defaultActions.Add("htmlviewerexternal");
                defaultActions.Add("htmlviewercompareversions");
                return defaultActions;
            }
        }

        private List<string> DefaultAuthorizedControlls
        {
            get
            {
                List<string> defaultControlls = new List<string>();
                defaultControlls.Add("navigation");
                defaultControlls.Add("widget");
                return defaultControlls;
            }
        }

        /// <summary>
        /// The Name (Controller name without .cs extension and controller suffix) of the current controller
        /// </summary>
        protected string ControllerName
        {
            get
            {
                return this.ControllerContext.RouteData.GetRequiredString("controller");
            }
        }

        /// <summary>
        /// The Action of the current controller
        /// </summary>
        protected string ControllerAction
        {
            get
            {
                return this.ControllerContext.RouteData.GetRequiredString("action");
            }
        }

        /// <summary>
        /// Get the acrtual domain in the Account table and return it. 
        /// </summary>
        protected string Domain
        {
            get
            {
                if (String.IsNullOrEmpty(domain))
                {
                    domain = (LoggedAccount.IsCustomDomainActive == false) ? LoggedAccount.Domain : LoggedAccount.CustomDomain;
                }
                return domain;
            }
        }

        protected List<GMGColorDAL.UserMenuItemRoleView> AuthorizedControllerActions
        {
            get
            {
                if (lstControllerActions == null)
                {
                    lstControllerActions = new List<GMGColorDAL.UserMenuItemRoleView>();
                    string cacheKey = cacheAuthorizedControllerActions + LoggedUser.ID;

                    if ((this.LoggedUser != null && this.LoggedUser.ID > 0))
                    {
                        lstControllerActions = (List<GMGColorDAL.UserMenuItemRoleView>)HttpRuntime.Cache.GetItemFromCache(cacheKey);
                        if (lstControllerActions == null)
                        {
                            List<int> loggedUserRoles =
                                this.LoggedUser.UserRoles.Select(o => o.Role).Distinct().ToList();

                            int loggedAccountTypeId = LoggedAccount.AccountType1.ID;

                            if (GMGColorConfiguration.AppConfiguration.IsHideDashbordMenuItem ||
                                GMGColorConfiguration.AppConfiguration.IsHideHelpCentreMenuItem)
                            {
                                lstControllerActions = DALUtils.SearchObjects<GMGColorDAL.UserMenuItemRoleView>(
                                    o =>
                                        o.AccountType == loggedAccountTypeId &&
                                        loggedUserRoles.Contains(o.Role), Context).ToList();

                                if (GMGColorConfiguration.AppConfiguration.IsHideDashbordMenuItem)
                                {
                                    lstControllerActions = (from o in lstControllerActions
                                        where
                                            o.AccountType ==
                                            loggedAccountTypeId &&
                                            o.Key != "DBIN" && o.Key != "DBAP" && o.Key != "STIN" &&
                                            o.Key != "STAD"
                                        select o).ToList();
                                }
                                if (GMGColorConfiguration.AppConfiguration.IsHideHelpCentreMenuItem)
                                {
                                    lstControllerActions = (from o in lstControllerActions
                                        where
                                            o.AccountType ==
                                            loggedAccountTypeId &&
                                            o.Key != "HLIN"
                                        select o).ToList();
                                }
                            }
                            else
                            {
                                lstControllerActions = DALUtils.SearchObjects<GMGColorDAL.UserMenuItemRoleView>(
                                    o =>
                                        o.AccountType == loggedAccountTypeId &&
                                        o.Key != "ACST" &&
                                        o.Key != "APDB", Context
                                    ).ToList();
                            }

                            // Special case for HQ(gmgcozone), remove Account Settings Summary and Groups
                            if (this.LoggedAccountType == GMGColorDAL.AccountType.Type.GMGColor)
                            {
                                lstControllerActions =
                                    lstControllerActions.Where(
                                        o =>
                                            o.AccountType == loggedAccountTypeId && o.Key != "SESM" &&
                                            o.Key != "SEGR").ToList();
                            }
                            else
                            {
                                //Remove restrictions for SYS admin so he can have access to Settings 

                                if (!LoggedAccount.HasCollaborateSubscriptionPlan(Context))
                                {
                                    lstControllerActions.RemoveAll(t => t.Controller == "Approvals");
                                }
                                if (!LoggedAccount.HasDeliverSubscriptionPlan(Context))
                                {
                                    lstControllerActions.RemoveAll(t => t.Controller == "Deliver");
                                }
                                if (LoggedAccount.IsFileTransferEnabled == false)
                                {
                                    lstControllerActions.RemoveAll(t => t.Controller == "FileTransfer");
                                }
                            }

                        lstControllerActions =
                                lstControllerActions.Distinct(
                                    new GMGColorDAL.UserMenuItemRoleView.UserMenuItemRoleViewEqualityComparer()).ToList();

                            HttpRuntime.Cache.AddToCache(cacheKey, lstControllerActions, 60); // Caching for one hour
                        }

                        //GMGColorLogging.log.WarnFormat("Navigation.AvailableUserPageRoles: Authorisation table for {0} {1} (ID: {2}) refreshed with session id {3} has {4} rows",
                        //                                this.LoggedUser.GivenName,
                        //                                this.LoggedUser.FamilyName,
                        //                                this.LoggedUser.ID,
                        //                                Session.SessionID,
                        //                                lstControllerActions.Count);
                    }
                    else
                    {
                        GMGColorLogging.log.WarnFormat("Navigation.AvailableUserPageRoles: LoggedUser for session {0} was strangely null or ID was 0 - logging the user out.", Session.SessionID);
                    }
                }
                return lstControllerActions;
            }
        }

        public static List<CustomTimeZone> TimeZones
        {
            get
            {
                List<CustomTimeZone> lstCustomTimeZone = new List<CustomTimeZone>();
                foreach (TimeZoneInfo tzi in TimeZoneInfo.GetSystemTimeZones())
                {
                    CustomTimeZone objCustomTZ = new CustomTimeZone();
                    objCustomTZ.Id = tzi.Id;
                    objCustomTZ.Name = tzi.DisplayName;

                    lstCustomTimeZone.Add(objCustomTZ);
                }
                return lstCustomTimeZone;
            }
        }

        private static string ZipFileName { get { return "Approvals.zip"; } }

        /// <summary>
        /// Retrieves the user device based on user agent
        /// </summary>
        protected DeviceInfo UserDeviceInfo
        {
            get
            {
                if (deviceInfo == null)
                {
                    deviceInfo = new DeviceInfo();
                    
                    // detect device type
                    deviceInfo.DeviceType = GMGColorEnums.BrowserType.Desktop;

                    var requestAgent = Request.UserAgent;

                    if (requestAgent != null)
                    {
                        if (requestAgent.ToLower().Contains("iphone"))
                        {
                            deviceInfo.DeviceType = GMGColorEnums.BrowserType.SmartphonePhone;
                        }
                        else if (requestAgent.ToLower().Contains("ipad"))
                        {
                            deviceInfo.DeviceType = GMGColorEnums.BrowserType.Tablet;
                        }
                        else
                        {
                            IDevice device = MvcApplication.WurflManager.GetDeviceForRequest(requestAgent);

                            bool isTablet = false;
                            string strIsTablet = device.GetCapability("is_tablet");
                            Boolean.TryParse(strIsTablet, out isTablet);
                            if (isTablet)
                            {
                                deviceInfo.DeviceType = GMGColorEnums.BrowserType.Tablet;
                            }
                            else
                            {
                                bool isSmartphone = false;
                                string strIsSmartphonePhone = device.GetCapability("is_smartphone");
                                Boolean.TryParse(strIsSmartphonePhone, out isSmartphone);
                                if (isSmartphone)
                                {
                                    deviceInfo.DeviceType = GMGColorEnums.BrowserType.SmartphonePhone;
                                }
                            }
                            
                        }
                    }
                }

                return deviceInfo;
            }
        }

        protected string EnvironmentRegion
        {
            get
            {
                return GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                           ? GMGColorConfiguration.AppConfiguration.AWSRegion
                           : string.Empty;
            }
        }

        #endregion


        #region Events

        private bool ValidateToken()
        {
            try
            {
                if (ControllerName.ToLower() == appServiceController || ControllerAction.ToLower() == "login" || ControllerAction.ToLower() == "isssouser" || ControllerAction.ToLower() == "samllogin" && Request.HttpMethod.ToLower() == "get")
                {
                    return true;
                }
                else if (ControllerAction.ToLower() == "idpresponse" && Request.HttpMethod.ToLower() == "post")
                {
                    return true;
                }
                else 
                {
                    if(Session["app_usid"] == null)
                    {
                        getUserSession();
                    }
                    if ((Request.Cookies["app_usid"].Value == Convert.ToString(Session["app_usid"])) || Convert.ToString(Session["app_usid"]).Length == 0)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                // update cookie for softproofing session - only if exists
                UpdateSoftProofingCookie();

                //if (!ValidateToken())
                //{
                //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "UnexpectedError" }));
                //    base.OnAuthorization(filterContext);
                //}

                //Allow calls to External Services controller
                if (Request.IsAjaxRequest() ||
                    ControllerContext.IsChildAction ||
                    ControllerName.ToLower() == appServiceController ||
                    (ControllerAction.ToLower() == SoftProofSubmitData.ToLower() && ControllerName.ToLower() == SoftProofingController.ToLower()) ||
                    (ControllerAction.ToLower() == SoftProofStartSession.ToLower() && ControllerName.ToLower() == SoftProofingController.ToLower()) ||
                    (ControllerAction.ToLower() == Constants.HubsControllerActionName && ControllerName.ToLower() == Constants.HubsControllerName) ||
                    (ControllerName.ToLower() == accountController && (ControllerAction == addTrialAccountAction || ControllerAction == getTrialAccountsAction)))
                {
                    base.OnAuthorization(filterContext);
                }

                else if (LoggedAccount != null && LoggedAccount.ID > 0)
                {
                    // Set Title to Site Name
                    ViewBag.SiteName = LoggedAccount.SiteName;

                    ViewBag.LoggedUser = LoggedUser;
                    ViewBag.LoggedAccount = LoggedAccount;
                    ViewBag.LoggedAccountTheme = LoggedAccountTheme;

                    //Update TimeOut Session value                
                    Session["STO"] = (Session.Timeout * 60);

                    // Modify Current thread's culture
                    System.Threading.Thread.CurrentThread.CurrentCulture = 
                    System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(LoggedAccountCulture);
                    System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
                    System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator = "/";
                    
                    if (LoggedAccountStatus == AccountStatu.Status.Active)
                    {
                        if ((Server.GetLastError() == null) && !(DefaultAuthorizedActions.Contains(ControllerAction.ToLower())))
                        {
                            if (LoggedUser != null && LoggedUser.ID > 0)
                            {
                                //if user logged in set his language as thread culture
                                System.Threading.Thread.CurrentThread.CurrentCulture = 
                                System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(LoggedUserCulture);
                                System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
                                System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator = "/";

                                ViewBag.LoggedUserFavIcon = LoggedUserInfo.LoggedUserFavIcon;
 
                                if (LoggedUser.NeedReLogin || !ValidateToken())
                                {
                                    bool userIsDeleted = LoggedUserStatus == UserStatu.Status.Deleted;
                                    filterContext.Result =
                                        new RedirectToRouteResult(
                                            new RouteValueDictionary(
                                                new
                                                    {
                                                        controller = "Auth",
                                                        action = "Logout",
                                                        @relogin = 1,
                                                        @user = userIsDeleted ? string.Empty : LoggedUser.Username
                                                    }));
                                }
                                else if ((System.Web.HttpContext.Current.Request.HttpMethod == "GET") && !(this.DefaultAuthorizedControlls.Contains(this.ControllerName.ToLower())))
                                {
                                    // Check heather requested controller and action is in the database. if not then should the invalid request error message otherwise show the unauthorized message
                                    if (!String.IsNullOrEmpty(this.ControllerName) && !String.IsNullOrEmpty(this.ControllerAction))
                                    {
                                        // Check request controller is available
                                        List<GMGColorDAL.ControllerAction> authorizedControls = GetAuthorizedControlls().Where(t => t.Controller.ToLower() == ControllerName.ToLower()).ToList();

                                        if (authorizedControls.Count > 0)
                                        {
                                            // Check request controller action is available
                                            ControllerAction authorizedAction = (from o in authorizedControls
                                                                                where o.Action.ToLower() == this.ControllerAction.ToLower()
                                                                                select o).FirstOrDefault();

                                            if (authorizedAction != null)
                                            {
                                                // Check user permission to controller and action
                                                List<GMGColorDAL.UserMenuItemRoleView> activeControllerAction =
                                                    (from o in AuthorizedControllerActions
                                                     where
                                                         o.Controller.ToLower() == this.ControllerName.ToLower() &&
                                                         o.Action.ToLower() == this.ControllerAction.ToLower()
                                                     select o).ToList();

                                                if (activeControllerAction.Count > 0)
                                                {
                                                    int navChildID;
                                                    string navChildKey;
                                                    if(activeControllerAction.Count > 1)
                                                    {
                                                        UserMenuItemRoleView childItem =
                                                            activeControllerAction.FirstOrDefault(m => m.Parent != 0);

                                                        navChildID = childItem.MenuItem;
                                                        navChildKey = childItem.Key;
                                                    }
                                                    else
                                                    {
                                                        navChildID = activeControllerAction[0].MenuItem;
                                                        navChildKey = activeControllerAction[0].Key;
                                                    }

                                                    MenuItem.MenuItemPair navParent = NavigationBL.GetParentMenuId(Context, navChildID);

                                                    TempData["Navigaionn_PM_ID"] = navParent.ID;
                                                    TempData["Nav_PM_Key"] = navParent.Key;
                                                    TempData["Navigaionn_CM_ID"] = navChildID;

                                                    this.ViewBag.Header = NavigationModel.GetLocalizedHeaderName(navParent.Key);
                                                    this.ViewBag.HeaderSmall = NavigationModel.GetLocalizedMenuName(navChildKey);

                                                    //TODO check if needed and remove the line below
                                                    bool isAdminSite = (this.LoggedSite == Sites.AdministratorSite);
                                                    Session["gmgc_app"] = ((activeControllerAction[0].IsAdminAppOwned != null) && (activeControllerAction[0].IsAdminAppOwned != isAdminSite));
                                                }
                                                // Menu Item is not available to logged user
                                                else
                                                {
                                                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "Unauthorised" }));
                                                }
                                            }
                                            // Invalid action request (all Ajax calls are allowed but they must be validated individually)
                                            else
                                            {
                                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "InvalidRequest" }));
                                            }

                                        }
                                        // Invalid controller request
                                        else
                                        {
                                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "Cannotcontinued" }));
                                        }
                                    }
                                    // Unexpected error
                                    else
                                    {
                                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "UnexpectedError" }));
                                    }
                                }
                            }
                            // have we lost our user/session or not logged in at all?
                            else if (this.ControllerAction.ToLower() != "autenticateuser" && this.ControllerAction.ToLower() != "acceptrejectcolorproofworkflow")
                            {
                                if (this.ControllerAction.ToLower() != "default")
                                {
                                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Auth", action = "Login", @do = Server.UrlEncode(Request.Url.AbsoluteUri) }));
                                }
                                else
                                {
                                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Auth", action = "Login" }));
                                }
                            }
                        }
                    }
                    else if (LoggedAccountStatus == AccountStatu.Status.Suspended)
                    {
                        TempData["AccId"] = LoggedAccount.ID;
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "SuspendedAccount", @reason = HttpUtility.HtmlEncode(LoggedAccount.SuspendReason) }));
                    }
                    else if (this.ControllerAction.ToLower() != "welcome")
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "NotConfigured" }));
                    }
                }
                else
                {
                    string strAction = "NotConfigured";
                    if (Session["gmg_error"] != null)
                    {
                        strAction = Session["gmg_error"].ToString();
                    }
                    Session["gmg_error"] = null;

                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = strAction }));
                }
                base.OnAuthorization(filterContext);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured in BaseController, OnAuthorization Method exception: {0}", ex.Message);
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "UnexpectedError" }));
                base.OnAuthorization(filterContext);
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Needed on ViewBag as it is used on _LaoutApplication (temporary)
            ViewData["UserDeviceInfo"] = UserDeviceInfo;

            base.OnActionExecuting(filterContext);
        }

        #endregion

        #region Methods

        [HttpPost]
        public void UpdateLocation(string Site)
        {
            if (Site == "true" || Site == "false")
            {
                Session["gmgc_app"] = Site;
            }
        }

        [HttpPost]
        public string ValidateDomain(int ID, string domainName)
        {
            string message = string.Empty;
            bool validate = false;

            string domainNameWithoutHostAddress = domainName;
            int lastIndexOfHostAddress = domainName.LastIndexOf(GMGColorConfiguration.AppConfiguration.HostAddress);
            if (lastIndexOfHostAddress > -1)
            {
                domainNameWithoutHostAddress = domainName.Substring(0, lastIndexOfHostAddress);
            }

            Regex regeX = new Regex(Constants.SubDomainNameRegex);
            validate = regeX.IsMatch(domainNameWithoutHostAddress);
            message = (validate ? "valied" : "invalied");

            if (validate)
            {
                validate = !Context.ReservedDomainNames.Any(t => t.Name == domainNameWithoutHostAddress);
                message = (validate ? "valied" : "reserved");
            }

            if (validate)
            {
                List<GMGColorDAL.Account> domains = DALUtils.SearchObjects<GMGColorDAL.Account>(o => !o.IsTemporary && o.Domain == domainName.ToLower(), Context);

                List<GMGColorDAL.Account> customDomains = DALUtils.SearchObjects<GMGColorDAL.Account>( o => o.CustomDomain  == domainName.ToLower(), Context);

                if (ID > 0)
                {
                    validate = ((domains.Where(o => o.ID != ID).ToList().Count == 0) && (customDomains.Where(o => o.ID != ID).ToList().Count == 0));
                }
                else
                {
                    validate = ((domains.Count == 0) && (customDomains.Count == 0));
                }
                message = (validate ? "success" : "exists");

                if (validate)
                {
                    validate = (domainName.Length <= 254);
                    message = (validate ? "success" : "failed");
                }
            }
            return message;
        }

        /// <summary>
        /// Validate given email address withing given account.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="newEmail">The new email.</param>
        /// <param name="currentEmail">The current email.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// returns 'true' if validate true(email not exist withing the given account)
        /// returns 'false' if validate false(email exist withing the given account)
        /// </returns>
        public bool ValidateEmail(int account, string newEmail, string currentEmail, DbContextBL context)
        {
            GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(account, context);

            return this.ValidateEmail(objAccount, newEmail, currentEmail);
        }

        /// <summary>
        /// Validate given email address withing given account.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="email"></param>
        /// <returns>returns 'true' if validate true(email not exist withing the given account)
        /// returns 'false' if validate false(email exist withing the given account)
        /// </returns>
        public bool ValidateEmail(Account objAccount, string email, string currentEmail)
        {
            List<string> lstAccountEmails = objAccount.Users.Where(o => o.Status != GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Deleted, Context).ID).Select(o => o.EmailAddress.ToLower()).ToList();
            if (!string.IsNullOrEmpty(currentEmail))
            {
                lstAccountEmails.Remove(currentEmail.ToLower());
            }
            return !lstAccountEmails.Contains(email.ToLower());
        }

        /// <summary>
        /// Validate given username address withing given account.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="newUsername"></param>
        /// <returns>returns 'true' if validate true(username not exist in the system)
        /// returns 'false' if validate false(username exist in the system)
        /// </returns>
        public bool ValidateUserName(string newUsername, string currentUsername)
        {
            /*
             * get all active users from active or suspended accounts
             */
            List<string> lstUserNames = (from u in Context.Users
                                         join us in Context.UserStatus on u.Status equals us.ID
                                         join a in Context.Accounts on u.Account equals a.ID
                                         join accs in Context.AccountStatus on a.Status equals accs.ID
                                         where
                                             us.Key == "A" && (accs.Key == "A" || accs.Key == "S") && a.ID == LoggedAccount.ID
                                         select u.Username.ToLower()).ToList();

            if (!string.IsNullOrEmpty(currentUsername))
            {
                lstUserNames.Remove(currentUsername.ToLower());
            }

            return !lstUserNames.Contains(newUsername.ToLower());
        }

        public bool CreateNewUser(UserInfo model, DbContextBL context, BillingPlan collaboratePlan = null)
        {
            bool success = false;
            GMGColorDAL.User objUser = BaseBL.CreateNewUser(model, this.LoggedUser.ID, this.LoggedAccount.ID, context, collaboratePlan);
            
            if (objUser != null)
            {
                success = true;
            }
            
            if (success && !model.objUser.HasPasswordEnabled)
            {
                if (!model.objUser.IsSsoUser)
                {
                    BaseBL.SendInviteToUser(objUser.ID, LoggedUser.ID, LoggedAccount.ID, context);
                }

                NotificationServiceBL.CreateNotification(new UserWasAdded()
                {
                    EventCreator = LoggedUser.ID,
                    CreatedUserID = objUser.ID,
                    Account = objUser.Account
                },
                    LoggedUser,
                    context
                    );
            }
            return success;
        }

        public void UpdateLogos(GMGColorDAL.Account objAccount, string uploadedHeaderLogo, string uploadedLoginLogo, string uploadedEmailLogo, string favIconLogo)
        {
            try
            {
                string relativeSourceFilePath = string.Empty, uploadFileName = string.Empty;
                string relativeFolderPath = "accounts/" + objAccount.Guid + "/";
                GMGColorIO.FolderExists(relativeFolderPath, this.LoggedAccount.Region, true);

                // Header Logo
                uploadFileName = (!String.IsNullOrEmpty(uploadedHeaderLogo.Trim())) ? uploadedHeaderLogo.Replace("|", string.Empty) : string.Empty;
                if (!String.IsNullOrEmpty(uploadFileName))
                {
                    objAccount.HeaderLogoPath = "headerlogo-122px-33px" + Path.GetExtension(uploadFileName);

                    relativeSourceFilePath = this.LoggedUserTempFolderPath + uploadFileName;
                    if (GMGColorIO.FileExists(relativeSourceFilePath, this.LoggedAccount.Region))
                    {
                        GMGColorCommon.ResizeImageBasedOnHeight(relativeSourceFilePath, relativeFolderPath + objAccount.HeaderLogoPath, 33, this.LoggedAccount.Region);
                    }
                }
                else //File has been deleted
                {
                    string relativeDestinationFilePath = relativeFolderPath + objAccount.HeaderLogoPath;
                    try
                    {
                        if(GMGColorIO.DeleteFileIfExists(relativeDestinationFilePath, LoggedAccount.Region))
                        {
                            GMGColorLogging.log.Info("File was deleted from the location of :" + relativeDestinationFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(String.Format("GMGColorIO.DeleteFileIfExists : {0} Exception details : ", relativeDestinationFilePath) + ex.Message, ex);
                    }
                    
                    objAccount.HeaderLogoPath = null;
                }

                // Login Logo
                uploadFileName = (!String.IsNullOrEmpty(uploadedLoginLogo.Trim())) ? uploadedLoginLogo.Replace("|", string.Empty) : string.Empty;
                if (!String.IsNullOrEmpty(uploadFileName))
                {
                    objAccount.LoginLogoPath = "loginlogo-200px-120px" + Path.GetExtension(uploadFileName);

                    relativeSourceFilePath = this.LoggedUserTempFolderPath + uploadFileName;
                    if (GMGColorIO.FileExists(relativeSourceFilePath, this.LoggedAccount.Region))
                    {
                        GMGColorCommon.ResizeImageBasedOnWidth(relativeSourceFilePath, relativeFolderPath + objAccount.LoginLogoPath, 200, this.LoggedAccount.Region);
                    }
                }
                else //File has been deleted
                {
                    string relativeDestinationFilePath = relativeFolderPath + objAccount.LoginLogoPath;
                    try
                    {
                        if (GMGColorIO.DeleteFileIfExists(relativeDestinationFilePath, LoggedAccount.Region))
                        {
                            GMGColorLogging.log.Info("File was deleted from the location of :" + relativeDestinationFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(String.Format("GMGColorIO.DeleteFileIfExists : {0} Exception details : ", relativeDestinationFilePath) + ex.Message, ex);
                    }
                    objAccount.LoginLogoPath = null;
                }

                // Email Logo
                uploadFileName = (!String.IsNullOrEmpty(uploadedEmailLogo.Trim())) ? uploadedEmailLogo.Replace("|", string.Empty) : string.Empty;
                if (!String.IsNullOrEmpty(uploadFileName))
                {
                    objAccount.EmailLogoPath = "emaillogo-160px-60px" + Path.GetExtension(uploadFileName);

                    relativeSourceFilePath = this.LoggedUserTempFolderPath + uploadFileName;
                    if (GMGColorIO.FileExists(relativeSourceFilePath, this.LoggedAccount.Region))
                    {
                        GMGColorCommon.ResizeImageBasedOnWidthAndHeight(relativeSourceFilePath, relativeFolderPath + objAccount.EmailLogoPath, 160, 60, this.LoggedAccount.Region);
                    }
                }
                else //File has been deleted
                {
                    string relativeDestinationFilePath = relativeFolderPath + objAccount.EmailLogoPath;
                    try
                    {
                        if (GMGColorIO.DeleteFileIfExists(relativeDestinationFilePath, LoggedAccount.Region))
                        {
                            GMGColorLogging.log.Info("File was deleted from the location of :" + relativeDestinationFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(String.Format("GMGColorIO.DeleteFileIfExists : {0} Exception details : ", relativeDestinationFilePath) + ex.Message, ex);
                    }
                    objAccount.EmailLogoPath = null;
                }

                // FavIcon Logo
                uploadFileName = (!String.IsNullOrEmpty(favIconLogo.Trim())) ? favIconLogo.Replace("|", string.Empty) : string.Empty;
                if (!String.IsNullOrEmpty(uploadFileName))
                {
                    objAccount.FavIconPath = "favicon-16px-16px" + Path.GetExtension(uploadFileName);

                    relativeSourceFilePath = this.LoggedUserTempFolderPath + uploadFileName;
                    if (GMGColorIO.FileExists(relativeSourceFilePath, this.LoggedAccount.Region))
                    {
                        GMGColorCommon.ResizeImageBasedOnWidthAndHeight(relativeSourceFilePath, relativeFolderPath + objAccount.FavIconPath, 16, 16, this.LoggedAccount.Region);
                    }
                }
                else //File has been deleted
                {
                    string relativeDestinationFilePath = relativeFolderPath + objAccount.FavIconPath;
                    try
                    {
                        if (GMGColorIO.DeleteFileIfExists(relativeDestinationFilePath, LoggedAccount.Region))
                        {
                            GMGColorLogging.log.Info("File was deleted from the location of :" + relativeDestinationFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(String.Format("GMGColorIO.DeleteFileIfExists : {0} Exception details : ", relativeDestinationFilePath) + ex.Message, ex);
                    }
                    objAccount.FavIconPath = null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost]
        public JsonResult GetDownloadTransferFilePath(string fileIDs)
        {
            try
            {
                var jobIDs = fileIDs;
                if (String.IsNullOrEmpty(jobIDs) && string.IsNullOrEmpty(fileIDs))
                {
                    // return server error 
                    Response.StatusCode = 500;
                    return Json(Resources.Resources.txtFileDownloadError, JsonRequestBehavior.AllowGet);
                }

                Role.RoleName LoggedUserCollaborateRole1 = Role.GetRole(UserBL.GetRoleKey(GMG.CoZone.Common.AppModule.FileTransfer, LoggedUser.ID, Context));

                int[] arrfileids = Array.ConvertAll(fileIDs.Split(','), int.Parse);

                if (!(CanAccessFileDownload(LoggedUser.ID, arrfileids, Context)
                    || (LoggedUserCollaborateRole1 == Role.RoleName.AccountAdministrator &&
                    DALUtils.IsFromCurrentAccount<GMGColorDAL.FileTransfer>(arrfileids, LoggedAccount.ID, Context
                    ))))
                {
                    // return server error 
                    Response.StatusCode = 500;
                    return Json(Resources.Resources.txtFileDownloadError, JsonRequestBehavior.AllowGet);
                }
                    

                var fileDownloadViewModel = new FileDownloadViewModel();
                fileDownloadViewModel.FileIds = fileIDs;
                fileDownloadViewModel.ApplicationModule = AppModule.FileTransfer;
                fileDownloadViewModel.DownloadFileTypes = DownloadFileTypes.Zip;
                fileDownloadViewModel.DownloadVersionTypes = DownloadVersionTypes.CurrentVersion;
                fileDownloadViewModel.AccountId = LoggedAccount.ID;
                var fileDownloadModel = _mapper.Map<FileDownloadViewModel, FileDownloadModel>(fileDownloadViewModel);
                var relativeFolderPath = _downlaodService.GetDownloadPaths(fileDownloadModel, LoggedUser.ID, GetRoleName(fileDownloadModel.ApplicationModule));


                if ((relativeFolderPath == null || relativeFolderPath.Count == 0) && fileDownloadModel.ApplicationModule == AppModule.FileTransfer)
                {
                    //return not found
                    Response.StatusCode = 404;
                    return Json("File Not Found", JsonRequestBehavior.AllowGet);
                }

                if (fileDownloadModel.FileIds.Count == 1)
                {
                    var fileId = fileDownloadModel.FileIds[0];
                    var path = relativeFolderPath[fileId];
                    var size = GMGColorIO.GetFileSizeInBytes(path, LoggedAccount.Region);
                    //size less than 50MB
                    if (size < 52428800)
                    {
                        fileDownloadModel.DownloadFileTypes = DownloadFileTypes.Native;
                    }
                    
                }
               
                if (relativeFolderPath == null || relativeFolderPath.Count == 0)
                {
                    return Json("ftp", JsonRequestBehavior.AllowGet);
                }

               
                return Json(new
                {
                    ModuleName = Enum.GetName(typeof(AppModule), fileDownloadViewModel.ApplicationModule),
                    ContentType = Enum.GetName(typeof(DownloadFileTypes), fileDownloadModel.DownloadFileTypes),
                    RelativePaths = relativeFolderPath.Values.ToList()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exBl)
            {
                throw exBl;
            }
            catch (Exception ex)
            {
               // GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "Error in Base Control DownloadFile Method. FileType : {0}, DownloadType : {1}, Exception : {2} ", fileDownloadViewModel.ApplicationModule.ToString(), fileDownloadViewModel.DownloadFileTypes.ToString(), ex.Message);
                Response.StatusCode = 500;
                return Json(Resources.Resources.txtFileDownloadError, JsonRequestBehavior.AllowGet);
            }
        }

        public static bool CanAccessFileDownload(int userId, int[] fileIdList, GMGColorContext context)
        {
            try
            {
                bool result = false;
                bool hasRole =
                    (from u in context.Users
                     join ur in context.UserRoles on u.ID equals ur.User
                     join r in context.Roles on ur.Role equals r.ID
                     join am in context.AppModules on r.AppModule equals am.ID
                     where u.ID == userId &&
                         am.Key == 0 &&
                         r.Key != "CON" &&
                         r.Key != "VIE" &&
                         r.Key != "NON"
                     select u.ID).Any();

                if (!hasRole)
                    return false;

                if ((from ft in context.FileTransfers
                     where fileIdList.Contains(ft.ID) && ft.IsDeleted == false && ft.Creator == userId
                     select ft.ID).Count() == fileIdList.Count() ? true : false)
                {
                    return true;
                }
                else
                {
                    result = ((from ac in context.FileTransfers
                               join fui in context.FileTransferUserInternals
                               on ac.ID equals fui.FileTransferID
                               where fileIdList.Contains(ac.ID) && ac.IsDeleted == false && ac.Creator == userId
                               select ac.ID).Union(from ac in context.FileTransfers
                                                   join fue in context.FileTransferUserExternals
                                                   on ac.ID equals fue.FileTransferID
                                                   where fileIdList.Contains(ac.ID) && ac.IsDeleted == false && ac.Creator == userId
                                                   select ac.ID).Count()) == fileIdList.Count() ? true : false;
                }




                return result;
            }
            catch(Exception ex)
            {

            }
            finally
            {
            }
            return false;
        }


        #region Download Jobs

        [HttpPost]
        public JsonResult GetDownloadFilePath(FileDownloadViewModel fileDownloadViewModel)
        {
            try
            {
                var jobIDs = fileDownloadViewModel.FileIds;
                if (String.IsNullOrEmpty(jobIDs) && string.IsNullOrEmpty(fileDownloadViewModel.FolderIds))
                {
                    // return server error 
                    Response.StatusCode = 500;
                    return Json(Resources.Resources.txtFileDownloadError, JsonRequestBehavior.AllowGet);
                }

                if (!string.IsNullOrEmpty(fileDownloadViewModel.FolderIds)) {
                    List<int> folderId = fileDownloadViewModel.FolderIds.Split(',').Select(Int32.Parse).ToList();
                    if (!ApprovalBL.CheckUserCanAccess(folderId, GMGColorDAL.Approval.CollaborateValidation.DownloadFolder, this.LoggedUser.ID, Context))
                    {
                        return Json(Resources.Resources.txtFileDownloadError, JsonRequestBehavior.AllowGet);
                    }
                }
                var fileDownloadModel = _mapper.Map<FileDownloadViewModel, FileDownloadModel>(fileDownloadViewModel);
                fileDownloadModel.AccountId = LoggedAccount.ID;
                var relativeFolderPath = _downlaodService.GetDownloadPaths(fileDownloadModel, LoggedUser.ID, GetRoleName(fileDownloadModel.ApplicationModule));


                if (relativeFolderPath == null || relativeFolderPath.Count == 0)
                {
                    return Json("ftp", JsonRequestBehavior.AllowGet);
                }

                if (fileDownloadModel.ApplicationModule == AppModule.Collaborate)
                {
                    var approvalIds = relativeFolderPath.Keys.ToList();
                    ApprovalBL.SendApprovalWasDownloadedNotification(approvalIds, LoggedUser, Context);
                }
                return Json(new
                {
                    ModuleName = Enum.GetName(typeof(AppModule), fileDownloadViewModel.ApplicationModule),
                    ContentType = Enum.GetName(typeof(DownloadFileTypes), fileDownloadViewModel.DownloadFileTypes),
                    RelativePaths = relativeFolderPath.Values.ToList()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (ExceptionBL exBl)
            {
                throw exBl;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "Error in Base Control DownloadFile Method. FileType : {0}, DownloadType : {1}, Exception : {2} ", fileDownloadViewModel.ApplicationModule.ToString(), fileDownloadViewModel.DownloadFileTypes.ToString(), ex.Message);
                Response.StatusCode = 500;
                return Json(Resources.Resources.txtFileDownloadError, JsonRequestBehavior.AllowGet);
            }
        }

		#endregion

		[HttpPost]
        public ActionResult DownloadFile(string downloadLink, string requestUrl)
        {
            try
            {                
				return new UnicodeFileContentResult(downloadLink, LoggedAccount.Region);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Error in Bsse Control GetDownloadFile Method. Download Link : {0}, Request Url : {1}, Exception : {2}", downloadLink, requestUrl, ex.Message);
                return Redirect(requestUrl);
            }
        }

        [HttpPost]
        public ActionResult DownloadFilesAsZip(string relativePaths, string moduleName, string requestUrl)
        {
            try
            {
                var filesPaths = relativePaths.Split(',').ToList();
                var zipResult = new ZipResult(filesPaths, LoggedAccount.Region) { FileName = moduleName };

                return zipResult;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Error in Base Controller DownloadFilesAsZip Method. Download Link : {0}, Request Url : {1}, Exception : {2}", relativePaths, requestUrl, ex.Message);
                return Redirect(requestUrl);
            }
        }

        protected bool CreateDeliverJobs(DeliverJob[] files, bool deleteSourceFiles, string usersAndPresets, string usersAndEmails, string emailOptMessage)
        {
            bool success = true;
            try
            {
                using(DbContextBL context = new DbContextBL())
                {
                    #region Create a database item for the current deliver job
                    try
                    {
                        using (TransactionScope ts = new TransactionScope())
                        {
                            #region Generic data validations

                            JobStatu jobStatusNew = JobStatu.GetJobStatus(GMGColorDAL.JobStatu.Status.New, context);
                            if (jobStatusNew == null)
                                throw new Exception("JobStatus New is not present in the database");


                            ColorProofJobStatu colorProofStatusSubmitting =
                                DALUtils.SearchObjects<ColorProofJobStatu>(o => o.Key == (int)ColorProofJobStatus.Submitting, context).FirstOrDefault();

                            if (colorProofStatusSubmitting == null)
                                throw new Exception("ColorProofStatus Submitting is not present in the database!");

                            DeliverJobStatu deliverJobStatusSubmitting =
                                DALUtils.SearchObjects<DeliverJobStatu>(o => o.Key == (int)DeliverJobStatus.Submitting, context).SingleOrDefault();

                            if (deliverJobStatusSubmitting == null)
                                throw new Exception("DeliverJobStatus Submitting is not present in the database!");

                            #endregion

                            #region Add the job to Deliver

                            List<DeliverExternalCollaboratorPreset> deliverExternalCollaboratorPreset = DeliverBL.GetDeliverExternalCollaborators(LoggedAccount.ID, LoggedUser.ID, usersAndEmails, usersAndPresets, context);

                            foreach (DeliverJob deliverFile in files)
                            {
                                #region Create JobBo Item

                                Job job = new Job()
                                                {
                                                    Title = deliverFile.Title,
                                                    Guid = Guid.NewGuid().ToString(),
                                                    Status = jobStatusNew.ID,
                                                    Account = LoggedAccount.ID,
                                                    ModifiedDate = DateTime.UtcNow
                                                };

                                #endregion

                                #region Create DeliverJobBo Item

                                GMGColorDAL.DeliverJob item = new GMGColorDAL.DeliverJob()
                                                        {
                                                            Guid = deliverFile.Guid,
                                                            Owner = LoggedUser.ID,
                                                            StatusInColorProof = colorProofStatusSubmitting.ID,
                                                            FileName = deliverFile.Name,
                                                            CPWorkflow = deliverFile.SelectedCpWorkflowId,
                                                            Pages = GMGColorDAL.DeliverJob.PageToString(
                                                                (PagesSelectionEnum)
                                                                Convert.ToInt32(deliverFile.PagesRadioButton),
                                                                deliverFile.PagesRangeValue),
                                                            IncludeProofMetaInformationOnLabel =
                                                                deliverFile.IncludeProofMetaInformation,
                                                            LogProofControlResults = deliverFile.LogProofControlResults,
                                                            Status = deliverJobStatusSubmitting.ID,
                                                            Size = deliverFile.Size,
                                                            CreatedDate = DateTime.UtcNow,
                                                            TotalNrOfPages = deliverFile.TotalNrOfPages,
                                                            JobSource = JobBL.GetJobSourceByKey(GMG.CoZone.Common.JobSource.WebUI, context)
                                                        };

                                //add deliver job data for job if opt message is set
                                if (!String.IsNullOrEmpty(emailOptMessage))
                                {
                                    DeliverJobData deliverJobData = new DeliverJobData();
                                    deliverJobData.EmailOptionalMsg = emailOptMessage;
                                    deliverJobData.DeliverJob1 = item;
                                    context.DeliverJobDatas.Add(deliverJobData);
                                }

                                #endregion

                                foreach (var externalCollaborator in deliverExternalCollaboratorPreset)
                                {
                                    var sharedJob = new DeliverSharedJob()
                                    {
                                        NotificationPreset = externalCollaborator.PresetId,
                                        DeliverExternalCollaborator1 = externalCollaborator.DeliverExternalCollaborator
                                    };

                                    item.DeliverSharedJobs.Add(sharedJob);
                                }
                               
                                job.DeliverJobs.Add(item);
                                context.Jobs.Add(job);
                            }
                            context.SaveChanges();

                            #endregion

                            ts.Complete();
                        }
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        DALUtils.LogDbEntityValidationException(ex);
                        success = false;
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Deliver job could not be created in database, mesage: {0}", ex.Message);
                    }
                    finally
                    {
                    }
                    #endregion

                    foreach (DeliverJob deliverFile in files)
                    {
                        #region Copy or move file necesary for the deliver job
                        try
                        {
                            bool isSameFilePresent = false;
                            int indexOfFile = files.Cast<DeliverJob>().ToList().IndexOf(deliverFile);
                            for (int i = indexOfFile + 1; i < files.Length; i++)
                            {
                                if (files[i].Name == deliverFile.Name)
                                {
                                    isSameFilePresent = true;
                                }
                            }

                            string sourceAttachedFilePath = deliverFile.SourcePath + deliverFile.Name;
                            string relativeFolderPath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + deliverFile.Guid + "/";
                            string destinationFilePath = relativeFolderPath + deliverFile.Name;
                            if (GMGColorIO.FolderExists(relativeFolderPath, this.LoggedAccount.Region, true) &&
                                GMGColorIO.FileExists(sourceAttachedFilePath, this.LoggedAccount.Region))
                            {
                                if (deleteSourceFiles && !isSameFilePresent)
                                {
                                    GMGColorIO.MoveFile(sourceAttachedFilePath, destinationFilePath, this.LoggedAccount.Region);
                                }
                                else
                                {
                                    GMGColorIO.CopyFile(sourceAttachedFilePath, destinationFilePath, this.LoggedAccount.Region);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            // delete the deliver job from db
                            DeleteDeliverJobByGuid(deliverFile.Guid, context);

                            success = false;
                            GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Deliver job could not be copied or moved, mesage: {0}", ex.Message);
                            continue;
                        }
                        #endregion

                        #region Send message to queue for the current deliver job
                        try
                        {
                            GMGColorDAL.DeliverJob item = context.DeliverJobs.Include("ColorProofCoZoneWorkflow").FirstOrDefault(o => o.Guid == deliverFile.Guid);
                            if (item != null)
                            {
                                // job for the process deliver job
                                string backLinkUrl = HttpContext.Request.Url.Scheme + "://"
                                                           + HttpContext.Request.Url.Authority
                                                           +
                                                           Url.Action("JobDetailLink", "Deliver",
                                                                      new { SelectedInstance = item.ID.ToString() });

                                Dictionary<string, string> dict = DeliverBL.GetDictionaryForDeliverQueue(item, backLinkUrl, this.LoggedAccount.Name);
                                GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.DeliverJob);

                                // job for generating preview
                                dict = DeliverBL.GetDictionaryForDefineQueue(item);
                                GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.ApprovalJob);
                            }
                        }
                        catch (Exception ex)
                        {
                            // delete the deliver job from db
                            DeleteDeliverJobByGuid(deliverFile.Guid, context);

                            // delete the remaining files from the storage
                            DeleteDeliverFilesFromStorage(deliverFile);

                            success = false;
                            GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Deliver job {1} could not be sent to deliver or approval queue, mesage: {0}", ex.Message, deliverFile.Guid);
                            continue;
                        }
                        #endregion
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "A deliver job could not be added to the system, message: {0}", ex.Message);
                success = false;
            }

            return success;
        }

       
        private bool DeleteDeliverJobByGuid(string guid, DbContextBL context)
        {
            try
            {
                GMGColorDAL.DeliverJob item = DALUtils.SearchObjects<GMGColorDAL.DeliverJob>(o => o.Guid == guid, context).FirstOrDefault();
                if (item != null)
                {
                    GMGColorDAL.Job jobItem = DALUtils.SearchObjects<GMGColorDAL.Job>(o => o.ID == item.Job, context).FirstOrDefault();
                    if (jobItem != null)
                    {
                        DALUtils.Delete<GMGColorDAL.Job>(context, jobItem);
                    }
                    DALUtils.Delete<GMGColorDAL.DeliverJob>(context, item);
                }
                return true;
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Deliver job could not be deleted from database, message: {0}", ex.Message);
                return false;
            }
        }

        private bool DeleteDeliverFilesFromStorage(DeliverJob deliverFile)
        {
            try
            {
                string relativeFolderPath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + deliverFile.Guid + "/";
                string destinationFilePath = relativeFolderPath + deliverFile.Name;

                return GMGColorIO.DeleteFile(destinationFilePath, this.LoggedAccount.Region);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "Deliver job could not be deleted from storage, message: {0}", ex.Message);
                return false;
            }
        }       

        protected ActionResult RedirectToModule(GMG.CoZone.Common.AppModule module)
        {
            return RedirectToModule(module, true, string.Empty, string.Empty);
        }

        protected ActionResult RedirectToModule(GMG.CoZone.Common.AppModule module, bool redirectToLandingPageIfNoAccess, string customErrorTitle, string customErrorMessage)
        {
            if (Account.HasRight(LoggedAccount.ID, LoggedUser.ID, module, Context))
            {
                switch (module)
                {
                    case AppModule.Collaborate:
                        return RedirectToAction("Index", "Approvals");
                    case AppModule.Deliver:
                        return RedirectToAction("Index", "Deliver");                    
                    default:
                        return RedirectToAction("Index", "PersonalSettings");
                }
            }
            else if (redirectToLandingPageIfNoAccess)
            {
                return RedirectToLandingPage();
            }
            else
            {
                return RedirectToAction("Custom", "Error",
                                                new
                                                {
                                                    title = HttpUtility.HtmlEncode(customErrorTitle),
                                                    reason = HttpUtility.HtmlEncode(customErrorMessage)
                                                });
            }
        }

        protected ActionResult RedirectToLandingPage()
        {
            ActionResult result = RedirectToAction("Index", "PersonalSettings");
            //SysAdmin user
            if (this.LoggedAccount.Parent.GetValueOrDefault() == 0)
            {
                result = RedirectToAction("Index", "Account");
            }
            else
            {
                if (LoggedAccount.DisableLandingPage)
                {
                    if (LoggedAccount.DashboardToShow == (int)GMG.CoZone.Common.AppModule.Deliver &&
                        Account.HasRight(LoggedAccount.ID, LoggedUser.ID, AppModule.Deliver, Context))
                    {
                        result = RedirectToAction("Index", "Deliver");
                    }
                    else if (LoggedAccount.DashboardToShow == (int)GMG.CoZone.Common.AppModule.Collaborate &&
                             Account.HasRight(LoggedAccount.ID, LoggedUser.ID, AppModule.Collaborate, Context))
                    {
                        result = RedirectToAction("Index", "Approvals");
                    }                  
                    else
                    {
                        result = RedirectToAction("Index", "PersonalSettings");
                    }
                }
                else if (AuthorizedControllerActions.Any(o => o.Key == "HOME"))
                {
                    result = RedirectToAction("Home", "Home");
                }
            }
            return result;
        }

        protected string UrlLandingPage()
        {
            string result = Url.Action("Index", "PersonalSettings");
            //SysAdmin user
            if (this.LoggedAccount.Parent.GetValueOrDefault() == 0)
            {
                result = Url.Action("Index", "Account");
            }
            else
            {
                if (LoggedAccount.DisableLandingPage)
                {
                    if (LoggedAccount.DashboardToShow == (int) GMG.CoZone.Common.AppModule.Deliver &&
                        LoggedUserDeliverRole != Role.RoleName.AccountNone && LoggedAccountSubscriptionDetails.HasDeliverSubscriptionPlan)
                    {
                        result = Url.Action("Index", "Deliver");
                    }
                    else if (LoggedAccount.DashboardToShow == (int) GMG.CoZone.Common.AppModule.Collaborate &&
                             LoggedUserCollaborateRole != Role.RoleName.AccountNone && LoggedAccountSubscriptionDetails.HasCollaborateSubscriptionPlan)
                    {
                        result = Url.Action("Index", "Approvals");
                    }                   
                    else
                    {
                        result = Url.Action("Index", "PersonalSettings");
                    }
                }
                else if (AuthorizedControllerActions.Any(o => o.Key == "HOME"))
                {
                    result = Url.Action("Home", "Home");
                }
            }
            return result;
        }

        /// <summary>
        /// Creates the session after user login into application.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="redirectToPage">if set to <c>true</c> [redirect to page].</param>
        protected bool CreateSession()
        {

            if (LoggedUser != null)
            {
                LoggedUser.NeedReLogin = false;
                Context.SaveChanges();
            }

            if (LoggedAccount == null) return false;

            Session["gmg_uid"] = Convert.ToString(LoggedUser.ID);
            Session["gmg_sid"] = Convert.ToString(Session.SessionID);

            List<KeyValuePair<string, AppModule>> roles = UserBL.GetUserRoles(LoggedUser.ID);
            foreach (KeyValuePair<string, AppModule> keyValuePair in roles)
            {
                switch (keyValuePair.Value)
                {
                    case AppModule.Collaborate:
                        Session[Constants.CollaborateRoleIdInSession + LoggedUser.ID] = keyValuePair.Key;
                        break;
                    case AppModule.Deliver:
                        Session[Constants.DeliverRoleIdInSession + LoggedUser.ID] = keyValuePair.Key;
                        break;                   
                    case AppModule.Admin:
                        Session[Constants.AdminRoleIdInSession + LoggedUser.ID] = keyValuePair.Key;
                        break;
                    case AppModule.SysAdmin:
                        Session[Constants.SysAdminRoleIdInSession + LoggedUser.ID] = keyValuePair.Key;
                        break;                        
                }
            }

            Session["gmg_aid"] = Convert.ToString(LoggedAccount.ID);
            Session["gmg_lid"] = LoggedUser.ID;

            // clear list of authorization menu list
            HttpRuntime.Cache.Remove(cacheAuthorizedControllerActions + LoggedUser.ID);
            ClearLoggedUserSecondaryNavigationCache();
            ClearAccountSubscriptionDetailsCache();
            ClearAccountBillingPlansCache();

            return true;
        }

        protected void UpdateSoftProofingCookie()
        {
            try
            {
                if (Request.Cookies[SoftProofingCookieName] != null && !string.IsNullOrEmpty(Request.Cookies[SoftProofingCookieName].Value))
                {
                    string value = Request.Cookies[SoftProofingCookieName].Value;
                    Response.Cookies.Remove(SoftProofingCookieName);
                    HttpCookie cookie = new HttpCookie(SoftProofingCookieName, value) {Expires = DateTime.Now.AddMinutes(SoftProofingCookieTimeout)};
                    Response.Cookies.Add(cookie);
                    SoftProofingBL.SetWorkstationConnected(value, Context);
                }
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("could not update cookie for softproofing session", ex);
            }
        }

        /// <summary>
        /// Returns view by browser agent
        /// </summary>
        /// <param name="view"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        protected string ViewNameByBrowserAgent(string view)
        {
            if (UserDeviceInfo.DeviceType == GMGColorEnums.BrowserType.Tablet || UserDeviceInfo.DeviceType == GMGColorEnums.BrowserType.SmartphonePhone)
            {
                view = view + ".Tablet";
            }

            return view;
        }

        /// <summary>
        /// Retrieves the navigation items the user is authorized for
        /// </summary>
        /// <param name="type"></param>
        /// <param name="activeMenuID"></param>
        /// <returns></returns>
        protected List<NavigationItem> GetUserSecondaryNavigation(SecondaryNavigationTypeEnum type)
        {
            int? selectedAccount = null;
            if (Session["SelectedAccId"] != null)
            {
                selectedAccount = int.Parse(Session["SelectedAccId"].ToString());
            }
           
            string cacheKey = type == SecondaryNavigationTypeEnum.EditAccountReports ? NavigationBL.GetNavigationCacheKeyForSelectedAccount(type, LoggedAccount.ID, selectedAccount) : NavigationBL.GetNavigationCacheKey(type, LoggedAccount.ID);
            var navigationItems = (List<NavigationItem>)HttpRuntime.Cache.GetItemFromCache(cacheKey);

            if (navigationItems == null)
            {
                navigationItems = NavigationBL.GetSecondaryNavigations(Context, LoggedUser.ID, LoggedAccount, type, selectedAccount);
                HttpRuntime.Cache.AddToCache(cacheKey, navigationItems, 60); // Caching for one hour
            }

            return navigationItems;
        }

        protected List<ControllerAction> GetAuthorizedControlls()
        {
            List<ControllerAction> authorizedControlls = HttpRuntime.Cache.GetItemFromCache(cacheAuthorizedControlls) as List<ControllerAction>;

            if (authorizedControlls == null)
            {
                authorizedControlls = Context.ControllerActions.ToList();

                HttpRuntime.Cache.AddToCache(cacheAuthorizedControlls, authorizedControlls, 1440);
            }

            return authorizedControlls;
        }

        /// <summary>
        /// Removes The Saved Secondary Navigation from Cache. Needed when changing translations
        /// </summary>
        protected void ClearLoggedUserSecondaryNavigationCache()
        {
            HttpRuntime.Cache.Remove(NavigationBL.GetNavigationCacheKey(SecondaryNavigationTypeEnum.AccountSettings, LoggedAccount.ID));
            HttpRuntime.Cache.Remove(NavigationBL.GetNavigationCacheKey(SecondaryNavigationTypeEnum.AdminSettings, LoggedAccount.ID));
            HttpRuntime.Cache.Remove(NavigationBL.GetNavigationCacheKey(SecondaryNavigationTypeEnum.EditAccountSettings, LoggedAccount.ID));
            HttpRuntime.Cache.RemoveKeysThatStartsWith(NavigationBL.GetNavigationCacheKeyForSelectedAccount(SecondaryNavigationTypeEnum.EditAccountReports, LoggedAccount.ID));
        }

        /// <summary>
        /// Removes Primary Navigation saved in cache
        /// </summary>
        protected void ClearAccountSubscriptionDetailsCache()
        {
            HttpRuntime.Cache.Remove(GetAccountSubscriptionDetailsCacheKey());
        }

        /// <summary>
        /// Removes Billing Plans saved in cache
        /// </summary>
        protected void ClearAccountBillingPlansCache()
        {
            HttpRuntime.Cache.Remove(GetAccountBillingPlansCacheKey());
        }
        /// <summary>
        /// Removes loggeduser from cache
        /// </summary>
        /// <param name="userId"></param>
        protected void ClearLoggedUserCache(int userId)
        {
            HttpRuntime.Cache.Remove(cacheLoggedUser + userId);
        }

        /// <summary>
        /// Set the current active menu
        /// </summary>
        /// <param name="lstMenu"></param>
        /// <param name="activeMenuID"></param>
        protected void SetActiveMenu(List<NavigationItem> lstMenu, int activeMenuID)
        {
            foreach (NavigationItem menuItem in lstMenu)
            {
                menuItem.Active = menuItem.MenuId == activeMenuID;
                if (menuItem.Childs.Count > 0)
                {
                    SetActiveMenu(menuItem.Childs, activeMenuID);
                }
            }
        }

        protected string GetAccountSubscriptionDetailsCacheKey()
        {
            return cacheAccountSubscriptionDetails + LoggedAccount.ID;
        }

        protected string GetAccountBillingPlansCacheKey()
        {
            return cacheAccountBillingPlans + LoggedAccount.ID;
        }

        public void SetUICulture(int loggedUserId)
        {
            string culture = Session != null && LoggedUser != null && LoggedUser.ID > 0 ? LoggedUserCulture : String.Empty;
            if (String.IsNullOrEmpty(culture) && loggedUserId > 0)
            {
                culture = UserBL.GetUserCulture(loggedUserId, Context);
            }

            SetUICulture(culture);
        }

        public void SetUICulture(string culture)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(culture);
        }

        /// <summary>
        /// Render view and return it as string
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string RenderViewToString(Controller controller, string viewName, int loggedUserId, object model)
        {
            SetUICulture(loggedUserId);

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        private Role.RoleName GetRoleName(AppModule appModule)
        {
            switch (appModule)
            {
                case AppModule.Deliver:
                    return LoggedUserDeliverRole;
                case AppModule.FileTransfer:
                    return LoggedUserFileTransferRole;
                default:
                    return LoggedUserCollaborateRole;
            }
        }

        public bool CheckIsValidHtml(string inputText)
        {
            bool isValid = true; 

            if (!string.IsNullOrEmpty(inputText))
            {
                isValid = !Constants.DangerousXSSTags.Any(o => inputText.ToLower().Contains("<" + o.ToLower())) ||
                                  !Constants.DangerousXSSTags.Any(o => inputText.ToLower().Contains("</" + o.ToLower()));

                if (isValid)
                    isValid = !Constants.DangerousXSSWords.Any(o => inputText.ToLower().Contains(o.ToLower()));
            }

            return isValid;
        }

        private void getUserSession()
        {
            if (Request.Cookies["app_usid"] != null)
            {
                string app_usid = Request.Cookies["app_usid"].Value.ToString();
                string userSession = string.Empty;
                if (LoggedUser != null && LoggedUser.ID > 0)
                {
                    userSession = Context.CollaborateAPISessions.Where(x => x.User == LoggedUser.ID && x.SessionKey == app_usid).Select(x => x.SessionKey.ToString()).FirstOrDefault();
                    if (userSession != null) { Session["app_usid"] = userSession; }
                }
                else
                {
                    userSession = Context.CollaborateAPISessions.Where(x => x.SessionKey == app_usid).Select(x => x.SessionKey.ToString()).FirstOrDefault();
                    if (userSession != null) { Session["app_usid"] = userSession; }
                }
            }
        }

        private int? getUserSessionAccrossServers()
        {
            if (Request.Cookies["app_usid"] != null)
            {
                string app_usid = Request.Cookies["app_usid"].Value.ToString();
                var userSession = Context.CollaborateAPISessions.Where(x => x.SessionKey == app_usid).Select(x => x).FirstOrDefault();
                if (userSession != null) {
                    Session["app_usid"] = userSession.SessionKey;
                    Session["gmg_uid"] = Convert.ToString(userSession.User);
                    Session["gmg_lid"] = Convert.ToString(userSession.User);
                    return userSession.User;
                }
            }
            return null;
        }


        #endregion
    }
}