﻿using System;
using System.Collections.Generic;
using System.Threading;
using GMGColorBusinessLogic;
using GMGColorDAL;


namespace GMGColor.Common
{
    public class Jobs
    {
        private static readonly TimeSpan NewApprovalsRequestTimeout = TimeSpan.FromMilliseconds(2000);

        private readonly JobHost _newApprovalsRequestJobTask = new JobHost();

        private Timer _newApprovalsRequestTimer;

        public void Start()
        {
            _newApprovalsRequestTimer = new Timer(ProcessNewApprovalsRequestCallback, null, TimeSpan.Zero, NewApprovalsRequestTimeout);
        }

        #region New appoval requests
        
        private void ProcessNewApprovalsRequestCallback(object state)
        {
            _newApprovalsRequestTimer.Change(Timeout.Infinite, Timeout.Infinite);
            _newApprovalsRequestJobTask.DoWork(ProcessNewApprovalsRequests);
        }

        private void ProcessNewApprovalsRequests()
        {
            try
            {
                ApprovalBL.ProcessNewApprovals();
            }           
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);
            }
            finally
            {
                _newApprovalsRequestTimer.Change(NewApprovalsRequestTimeout, NewApprovalsRequestTimeout);
            }           
        }
        #endregion

    }
}