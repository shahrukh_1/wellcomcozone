﻿namespace GMGColor.Common
{
    public static class GMGColorEnums
    {
        /// <summary>
        /// Enum for sending the correct approval type to the flex service , value taken from FileType Table
        /// </summary>
        public enum ApprovalTypeFO
        {
            image,
            video,
            swf
        }

        /// <summary>
        /// The browser type that sent the request
        /// </summary>
        public enum BrowserType
        {
            Desktop = 0,
            Tablet,
            SmartphonePhone
        }
    }
}