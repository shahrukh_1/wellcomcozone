﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace GMGColor.Common
{
    public class JobHost : IRegisteredObject
    {
        private Task _task = null;
        private readonly CancellationToken _taskToken = new CancellationToken();
        private string _jobName = string.Empty;

        public JobHost()
        {
            HostingEnvironment.RegisterObject(this);
        }

        public void Stop(bool immediate)
        {
            try
            {
                GMGColorDAL.GMGColorLogging.log.DebugFormat("Request for job {0} stop was received", _jobName);
                if (_task != null)
                {
                    _task.Wait(_taskToken); // this line should block the current thread until the work is done
                    _task.Dispose();
                    _task = null;
                }
                HostingEnvironment.UnregisterObject(this);
                GMGColorDAL.GMGColorLogging.log.DebugFormat("Job {0} was stopped", _jobName);
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Error on stopping job host, message: " + ex.Message, ex);
            }
        }

        public void DoWork(Action work)
        {
            _jobName = work.Method.Name;
            _task = new Task(work, _taskToken);
            _task.Start();
        }

    }
}