﻿using System.Web;
using System.Web.Compilation;
using System.Web.Routing;

public class HttpHandlerRouteHandler<T> : IRouteHandler where T : IHttpHandler, new()
{

    public HttpHandlerRouteHandler() { }

    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        return new T();
    }
}

public class HttpHandlerRouteHandler : IRouteHandler
{

    private string virtualPath;

    public HttpHandlerRouteHandler(string virtualPath)
    {
        this.virtualPath = virtualPath;
    }

    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        return (IHttpHandler)BuildManager.CreateInstanceFromVirtualPath(this.virtualPath, typeof(IHttpHandler));
    }

}