﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using GMG.CoZone.Common;
using System.Collections.ObjectModel;
using GMGColorDAL;
using GMGColorDAL.Common;
using iTextSharp.text.pdf;
using GMGColorBusinessLogic.Common;

namespace GMGColor.Common
{
    public class NewApprovals
    {
        #region Properties

        public User loggedUser { get; set; }

        public List<int> approvalsId { get; set; }

        public string optionalMessage { get; set; }

        public GMGColorDAL.EventType.TypeEnum approvalsEventType { get; set; }

        #endregion

        #region Constructors

        public NewApprovals()
        {
            approvalsId = new List<int>();
        }
        #endregion
    }

    public static class Utils
    {
        //private static readonly byte[] PrivateKey = Encoding.ASCII.GetBytes("o6806642kbM7c5@");
        //internal static string PublicKey = "GMGColor";
        public const string RequestValidationException = "HttpRequestValidationException";        
        public const string DatabaseContext = DALUtils.DatabaseContext;

        /// <summary>
        /// Gets the pages label.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string GetPagesLabel(string value)
        {
            PagesSelectionEnum pageType = PagesSelectionEnum.Single;
            string rangeValue;
            GMG.CoZone.Common.Utils.StringToPage(value ?? string.Empty, out pageType, out rangeValue);

            string returnValue = Resources.Resources.lblDeliverIndexGridPagesSingle;
            switch (pageType)
            {
                case PagesSelectionEnum.Single:
                    {
                        returnValue = Resources.Resources.lblDeliverIndexGridPagesSingle;
                        break;
                    }
                case PagesSelectionEnum.All:
                    {
                        returnValue = Resources.Resources.lblDeliverIndexGridPagesAll;
                        break;
                    }
                case PagesSelectionEnum.Range:
                    {
                        returnValue = string.Format("{0} {1}", Resources.Resources.lblDeliverIndexGridPagesRange, rangeValue);
                        break;
                    }
            }
            return returnValue;
        }

        /// <summary>
        /// Cancel Jobs for a Cozone WorkFlow
        /// </summary>
        /// <param name="workFlow">CoZone Workflow for which to delete the jobs</param>
        /// <param name="userName">ColorProof Server Username</param>
        /// <param name="password">ColorProof Server Password</param>
        /// <returns>The list of Commands to be sent to CP</returns>
        public static List<Dictionary<string, string>> CancelWorkFlowJobs(GMGColorDAL.ColorProofCoZoneWorkflow workFlow, string userName, string password, string colorProofInstanceGuid)
        {
            List<Dictionary<string, string>> jobCommands = new List<Dictionary<string, string>>();

            foreach (GMGColorDAL.DeliverJob job in workFlow.DeliverJobs)
            {
                if (job.DeliverJobStatu.Key != (int)DeliverJobStatus.Cancelled && job.DeliverJobStatu.Key != (int)DeliverJobStatus.CompletedDeleted && job.DeliverJobStatu.Key != (int)DeliverJobStatus.Completed)
                {
                    Dictionary<string, string> dict = GMG.CoZone.Common.Utils.CreateCommandQueueMessage(job.Guid, userName, password, colorProofInstanceGuid, CommandType.Cancel);
                    jobCommands.Add(dict);
                }
            }

            return jobCommands;
        }

        /// <summary>
        /// Gets the date time from ticks.
        /// </summary>
        /// <param name="ticksCount">The ticks count.</param>
        /// <returns></returns>
        public static DateTime GetDateTimeFromTicks(long ticksCount)
        {
            return new DateTime(ticksCount);
        }

        /// <summary>
        /// Get Module Name from Module Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetModuleNameByKey(int key)
        {
            switch (key)
            {
                case (int)GMG.CoZone.Common.AppModule.Collaborate:
                    return Resources.Resources.navApprovalsIndex;
                case (int)GMG.CoZone.Common.AppModule.Deliver:
                    return Resources.Resources.lblDeliver;               
                case (int)GMG.CoZone.Common.AppModule.Admin:
                default:
                    return Resources.Resources.navSettingsIndex;
            }
        }

        /// <summary>
        /// Get deliver job status translation
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string GetJobStatusLabel(DeliverJobStatus status)
        {
            string jobStatus = "NO_STATUS";

            switch (status)
            {
                case DeliverJobStatus.Submitting:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusSubmitting;
                        break;
                    }
                case DeliverJobStatus.Cancelled:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusCancelled;
                        break;
                    }
                case DeliverJobStatus.Completed:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusCompleted;
                        break;
                    }
                case DeliverJobStatus.ProcessingError:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusProcessingError;
                        break;
                    }
                case DeliverJobStatus.Created:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusCreated;
                        break;
                    }
                case DeliverJobStatus.Downloading:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusDownloading;
                        break;
                    }
                case DeliverJobStatus.Waiting:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusWaiting;
                        break;
                    }
                case DeliverJobStatus.Processing:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusProcessing;
                        break;
                    }
                case DeliverJobStatus.DownloadToCPFailed:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusDownloadFailed;
                        break;
                    }
                case DeliverJobStatus.ValidationInCpFailed:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusValidationFailed;
                        break;
                    }
                case DeliverJobStatus.CompletedDeleted:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusCompletedDeleted;
                        break;
                    }
                case DeliverJobStatus.ErrorDeleted:
                    {
                        jobStatus = GMGColor.Resources.Resources.lblDeliverIndexJobStatusErrorDeleted;
                        break;
                    }
            }

            return jobStatus;
        }

        public static string GetTranslatedXMLTemplateField(DefaultXMLTemplateField field)
        {
            string translatedField = "NO_LABEL";

            switch (field)
            {
                case DefaultXMLTemplateField.JobName:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldJobName;
                        break;
                    }
                case DefaultXMLTemplateField.AllowUsersToDownloadOriginalFile:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldAllowUsersToDownloadOriginalFile;
                        break;
                    }
                case DefaultXMLTemplateField.ColorProofCozoneInstanceName:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldColorProofCozoneInstanceName;
                        break;
                    }
                case DefaultXMLTemplateField.ColorProofCozoneWorkflow:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldColorProofCozoneWorkflow;
                        break;
                    }
                case DefaultXMLTemplateField.Deadline:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldDeadline;
                        break;
                    }
                case DefaultXMLTemplateField.DestinationFolder:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldDestinationFolder;
                        break;
                    }               
                case DefaultXMLTemplateField.ExternalCollaborator:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldExternalCollaborator;
                        break;
                    }
                case DefaultXMLTemplateField.GenerateSeparations:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldGenerateSeparations;
                        break;
                    }
                case DefaultXMLTemplateField.IncludeProofMetaInformationOnProofLabel:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldIncludeProofMetaInformationOnProofLabel;
                        break;
                    }               
                case DefaultXMLTemplateField.InternalCollaborator:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldInternalCollaborator;
                        break;
                    }
                case DefaultXMLTemplateField.LockWhenAllDecisionsHaveBeenMade:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldLockWhenAllDecisionsHaveBeenMade;
                        break;
                    }
                case DefaultXMLTemplateField.LogProofControlResults:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldLogProofControlResults;
                        break;
                    }
                case DefaultXMLTemplateField.Modules:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldModules;
                        break;
                    }               
                case DefaultXMLTemplateField.Owner:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldOwner;
                        break;
                    }
                case DefaultXMLTemplateField.Pages:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldPages;
                        break;
                    }               
                case DefaultXMLTemplateField.PrimaryDecisionMaker:
                    {
                        translatedField = Resources.Resources.XMLTemplateFieldPrimaryDecisionMaker;
                        break;
                    }
                case DefaultXMLTemplateField.LockWhenFirstDecisionHasBeenMade:
                {
                    translatedField = Resources.Resources.lblLockProofsAfterFirstDec;
                    break;
                }
            }

            return translatedField;
        }

        public static List<KeyValuePair<int, string>> GetDeliverTranslationsFromStatuses()
        {
            List<KeyValuePair<int, string>> statusTranslations = new List<KeyValuePair<int, string>>();

            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Submitting, GMGColor.Resources.Resources.lblDeliverIndexJobStatusSubmitting));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Cancelled, GMGColor.Resources.Resources.lblDeliverIndexJobStatusCancelled));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Completed, GMGColor.Resources.Resources.lblDeliverIndexJobStatusCompleted));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.ProcessingError, GMGColor.Resources.Resources.lblDeliverIndexJobStatusProcessingError));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Created, GMGColor.Resources.Resources.lblDeliverIndexJobStatusCreated));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Downloading, GMGColor.Resources.Resources.lblDeliverIndexJobStatusDownloading));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Waiting, GMGColor.Resources.Resources.lblDeliverIndexJobStatusWaiting));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.Processing, GMGColor.Resources.Resources.lblDeliverIndexJobStatusProcessing));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.DownloadToCPFailed, GMGColor.Resources.Resources.lblDeliverIndexJobStatusDownloadFailed));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.ValidationInCpFailed, GMGColor.Resources.Resources.lblDeliverIndexJobStatusValidationFailed));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.CompletedDeleted, GMGColor.Resources.Resources.lblDeliverIndexJobStatusCompletedDeleted));
            statusTranslations.Add(new KeyValuePair<int, string>((int)DeliverJobStatus.ErrorDeleted, GMGColor.Resources.Resources.lblDeliverIndexJobStatusErrorDeleted));

            return statusTranslations;
        }

        /// <summary>
        /// Validates pagination values
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rowsPerPage"></param>
        /// <returns></returns>
        public static bool ValidatePaginationValues(ref int? page, ref int? rowsPerPage)
        {
            if (!rowsPerPage.HasValue || rowsPerPage.GetValueOrDefault() <= 0)
            {
                rowsPerPage = (int?)10;
            }

            if (page.GetValueOrDefault() <= 0)
            {
                page = 1;
            }

            if (rowsPerPage.HasValue && !GMGColorDAL.CustomModels.WebGridNavigationModel.PagesCount.Contains(rowsPerPage.Value))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the deliver page status label.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public static string GetDeliverPageStatusLabel(DeliverPageStatus status)
        {
            string deliverJobStatus = "NO_STATUS";

            switch (status)
            {
                case DeliverPageStatus.Created:
                    {
                        deliverJobStatus = GMGColor.Resources.Resources.lblDeliverPageStatusCreated;
                        break;
                    }
                case DeliverPageStatus.Waiting:
                    {
                        deliverJobStatus = GMGColor.Resources.Resources.lblDeliverPageStatusWaiting;
                        break;
                    }
                case DeliverPageStatus.Printing:
                    {
                        deliverJobStatus = GMGColor.Resources.Resources.lblDeliverPageStatusPrinting;
                        break;
                    }
                case DeliverPageStatus.Completed:
                    {
                        deliverJobStatus = GMGColor.Resources.Resources.lblDeliverPageStatusCompleted;
                        break;
                    }
                case DeliverPageStatus.Deleted:
                    {
                        deliverJobStatus = GMGColor.Resources.Resources.lblDeliverPageStatusDeleted;
                        break;
                    }
                case DeliverPageStatus.Failed:
                    {
                        deliverJobStatus = GMGColor.Resources.Resources.lblDeliverPageStatusFailed;
                        break;
                    }
            }

            return deliverJobStatus;
        }

        /// <summary>
        /// Returns job pages CSV as string 
        /// </summary>
        /// <returns></returns>
        public static string GetPagesAsStringCSV(string selectedPages)
        {
            Collection<int> pages = GMG.CoZone.Common.Utils.GetPages(selectedPages);
            string strPages = String.Empty;
            if (pages != null)
            {
                List<int> pagesLst = pages.ToList();
                for (int i = 0; i < pagesLst.Count; i++)
                {
                    pagesLst[i]++;
                }
                strPages = String.Join(",", pagesLst.ToArray());
            }
            else
            {
                strPages = Resources.Resources.lblDeliverIndexGridPagesAll;
            }
            return strPages;
        }

        public enum ApprovalTab
        {
            AllApprovals = -1,
            OwnedByMe = -2,
            SharedWithMe = -3,
            RecentlyViewed = -4,
            Archive_PastTense = -5,
            RecycleBin = -6,
            MyOpenApprovals = -7,
            SubstitutedApprovals = -8,
            OverdueApprovals = -9
        }

        /// <summary>
        /// Gets the PDF page count.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <returns></returns>
        public static int GetPdfPageCount(Stream inputStream, string pdfName)
        {
            int pageCounts = 0;

            try
            {
                if (inputStream != null)
                {
                    PdfReader reader = new PdfReader(GetByteArray(inputStream));
                    pageCounts = reader.NumberOfPages;
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Could not retrieve PDF page count for input stream. File {0}. Message: {1}", pdfName, ex.Message);
            }

            return pageCounts;
        }

        /// <summary>
        /// Get stream as a byte array
        /// </summary>
        /// <param name="inputStream">The input stream</param>
        /// <returns></returns>
        public static byte[] GetByteArray(Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Gets the PDF page count.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <returns></returns>
        public static int GetPdfPageCount(String filePath)
        {
            int pageCounts = 0;
            try
            {
                PdfReader reader = new PdfReader(filePath);
                pageCounts = reader.NumberOfPages;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Coult not retrieve PDF page count for input stream. {0}", ex.Message);
            }
            return pageCounts;
        }

        /// <summary>
        /// Gets the PDF page count.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <returns></returns>
        public static int GetPdfPageCount(Uri uri)
        {
            int pageCounts = 0;
            try
            {
                PdfReader reader = new PdfReader(uri);
                pageCounts = reader.NumberOfPages;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Coult not retrieve PDF page count for input stream. {0}", ex.Message);
            }
            return pageCounts;
        }

        /// <summary>
        /// Toes the int array.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static int?[] ToIntArray(this string[] obj)
        {
            return obj.Select(o=>o.ToInt()).ToArray();
        }

        /// <summary>
        /// Toes the int.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static int? ToInt(this string obj)
        {
            int retValue = 0;
            return Int32.TryParse(obj, out retValue) ? (int?) retValue : null;
        }

        public static decimal RemoveTrailingZeroes(this decimal obj)
        {
            return obj/1.000000000000000000000000000000000m;
        }

        /// <summary>
        /// Extends the specified dest.
        /// </summary>
        /// <param name="dest">The dest.</param>
        /// <param name="src">The SRC.</param>
        /// <returns></returns>
        public static RouteValueDictionary Extend(this RouteValueDictionary dest, IEnumerable<KeyValuePair<string, object>> src)
        {
            src.ToList().ForEach(x => { dest[x.Key] = x.Value; });
            return dest;
        }

        /// <summary>
        /// Merges the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="newData">The new data.</param>
        /// <returns></returns>
        public static RouteValueDictionary Merge(this RouteValueDictionary source, IEnumerable<KeyValuePair<string, object>> newData)
        {
            return (new RouteValueDictionary(source)).Extend(newData);
        }

        ///// <summary>
        ///// Encrypts the specified plain text.
        ///// </summary>
        ///// <param name="plainText">The plain text.</param>
        ///// <returns></returns>
        //internal static string Encrypt(string plainText)
        //{
        //    return Encrypt(plainText, PublicKey);
        //}

        ///// <summary>
        ///// Encrypt the given string using AES.  The string can be decrypted using 
        ///// DecryptStringAES().  The sharedSecret parameters must match.
        ///// </summary>
        ///// <param name="plainText">The text to encrypt.</param>
        ///// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        //internal static string Encrypt(string plainText, string sharedSecret)
        //{
        //    if (string.IsNullOrEmpty(plainText))
        //        throw new ArgumentNullException("plainText");
        //    if (string.IsNullOrEmpty(sharedSecret))
        //        throw new ArgumentNullException("sharedSecret");

        //    string outStr = null;                       // Encrypted string to return
        //    RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.

        //    try
        //    {
        //        // generate the key from the shared secret and the salt
        //        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, PrivateKey);

        //        // Create a RijndaelManaged object
        //        aesAlg = new RijndaelManaged();
        //        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

        //        // Create a decryptor to perform the stream transform.
        //        ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

        //        // Create the streams used for encryption.
        //        using (MemoryStream msEncrypt = new MemoryStream())
        //        {
        //            // prepend the IV
        //            msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
        //            msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
        //            using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
        //            {
        //                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
        //                {
        //                    //Write all data to the stream.
        //                    swEncrypt.Write(plainText);
        //                }
        //            }
        //            outStr = Convert.ToBase64String(msEncrypt.ToArray());
        //        }
        //    }
        //    finally
        //    {
        //        // Clear the RijndaelManaged object.
        //        if (aesAlg != null)
        //            aesAlg.Clear();
        //    }

        //    // Return the encrypted bytes from the memory stream.
        //    return outStr;
        //}

        ///// <summary>
        ///// Decrypts the specified cipher text.
        ///// </summary>
        ///// <param name="cipherText">The cipher text.</param>
        ///// <returns></returns>
        //internal static string Decrypt(string cipherText)
        //{
        //    return Decrypt(cipherText, PublicKey);
        //}

        ///// <summary>
        ///// Decrypt the given string.  Assumes the string was encrypted using 
        ///// EncryptStringAES(), using an identical sharedSecret.
        ///// </summary>
        ///// <param name="cipherText">The text to decrypt.</param>
        ///// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        //internal static string Decrypt(string cipherText, string sharedSecret)
        //{
        //    if (string.IsNullOrEmpty(cipherText))
        //        throw new ArgumentNullException("cipherText");
        //    if (string.IsNullOrEmpty(sharedSecret))
        //        throw new ArgumentNullException("sharedSecret");

        //    // Declare the RijndaelManaged object
        //    // used to decrypt the data.
        //    RijndaelManaged aesAlg = null;

        //    // Declare the string used to hold
        //    // the decrypted text.
        //    string plaintext = null;

        //    try
        //    {
        //        // generate the key from the shared secret and the salt
        //        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, PrivateKey);

        //        // Create the streams used for decryption.                
        //        byte[] bytes = Convert.FromBase64String(cipherText);
        //        using (MemoryStream msDecrypt = new MemoryStream(bytes))
        //        {
        //            // Create a RijndaelManaged object
        //            // with the specified key and IV.
        //            aesAlg = new RijndaelManaged();
        //            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
        //            // Get the initialization vector from the encrypted stream
        //            aesAlg.IV = ReadByteArray(msDecrypt);
        //            // Create a decrytor to perform the stream transform.
        //            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
        //            using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
        //            {
        //                using (StreamReader srDecrypt = new StreamReader(csDecrypt))

        //                    // Read the decrypted bytes from the decrypting stream
        //                    // and place them in a string.
        //                    plaintext = srDecrypt.ReadToEnd();
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        // Clear the RijndaelManaged object.
        //        if (aesAlg != null)
        //            aesAlg.Clear();
        //    }

        //    return plaintext;
        //}

        ///// <summary>
        ///// Reads the byte array.
        ///// </summary>
        ///// <param name="s">The s.</param>
        ///// <returns></returns>
        //private static byte[] ReadByteArray(Stream s)
        //{
        //    byte[] rawLength = new byte[sizeof(int)];
        //    if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
        //    {
        //        throw new SystemException("Stream did not contain properly formatted byte array");
        //    }

        //    byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
        //    if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
        //    {
        //        throw new SystemException("Did not read byte array properly");
        //    }

        //    return buffer;
        //}

        public static bool ContainsScriptTags(IEnumerable<string> data)
        {
            try
            {
                return data.Any(o => Regex.Match(o ?? string.Empty, TrialAccountMessages.ScriptTagsPattern).Success);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Utils.ContainsScriptTags --> " + ex.Message, ex);
                return false;
            }
        }

        /// <summary>
        /// Retrieves the translation for the given upload engine duplicate filename option
        /// </summary>
        /// <param name="uploadEngineDuplicateFileNameOption"></param>
        /// <returns></returns>
        public static string GetLabelForUploadEngineDuplicateFileName(FileUploadDuplicateName uploadEngineDuplicateFileNameOption)
        {
            string optionName = "NO_OPTION";

            switch (uploadEngineDuplicateFileNameOption)
            {
                case FileUploadDuplicateName.Ignore:
                {
                    optionName = Resources.Resources.lblUploadSettingsDuplicateFileNameIgnore;
                    break;
                }
                case FileUploadDuplicateName.ImportAsNewJob:
                {
                    optionName = Resources.Resources.lblUploadSettingsDuplicateFileNameImportAsNew;
                    break;
                }
                case FileUploadDuplicateName.MatchFileNameExactly:
                {
                    optionName = Resources.Resources.lblUploadSettingsDuplicateFileNameMatchFileNameExactly;
                    break;
                }
                case FileUploadDuplicateName.FileWithSuffix_vX:
                {
                    optionName = Resources.Resources.lblUploadSettingsDuplicateFileNameFileWithSuffix_vX;
                    break;
                }
            }

            return optionName;
        }

        /// <summary>
        /// Retrieves the translation for the given upload engine version name option
        /// </summary>
        /// <param name="uploadEngineVersionName"></param>
        /// <returns></returns>
        public static string GetLabelForUploadVersionName(VersionUploadName uploadEngineVersionName)
        {
            string optionName = "NO_OPTION";

            switch (uploadEngineVersionName)
            {
                case VersionUploadName.AppendSuffixvX:
                    {
                        optionName = Resources.Resources.lblUploadSettingsVersionNameAppendvx;
                        break;
                    }
                case VersionUploadName.AppendSuffix_vX:
                    {
                        optionName = Resources.Resources.lblUploadSettingsVersionNameAppend_vx;
                        break;
                    }
                case VersionUploadName.AppendvXAtPosition:
                    {
                        optionName = Resources.Resources.lblUploadSettingsAddvxAtLocation;
                        break;
                    }
                case VersionUploadName.KeepOriginalName:
                    {
                        optionName = Resources.Resources.lblUploadSettingsVersionNameKeepOriginal;
                        break;
                    }
            }

            return optionName;
        }

        /// <summary>
        /// Retrieves the translation for the given collaborate global setting option
        /// </summary>
        /// <param name="accountSettingName"></param>
        /// <returns></returns>
        public static string GetLabelForCollaborateGlobalSetting(string accountSettingName)
        {
            string optionName = "NO_OPTION";

            switch (accountSettingName)
            {
                case GMGColorConstants.ShowAllFilesToManagers:
                    {
                        optionName = "Determines whether or not show all files filter is available for managers in collaborate dashboard";
                        break;
                    }
                case GMGColorConstants.AllColDecisionRequired:
                    {
                        optionName = Resources.Resources.lblAllCollaboratorsDecisionSetting;
                        break;
                    }
                case GMGColorConstants.EnableFlexViewer:
                    {
                        optionName = Resources.Resources.lblAccountSettingsEnableFlexViewer;
                        break;
                    }
                case GMGColorConstants.ProofStudioPenWidth:
                    {
                        optionName = Resources.Resources.lblAccountSettingsProofStudioPenWidth;
                        break;
                    }
                case GMGColorConstants.ChangesCompleteDefaultGroup:
                    {
                        optionName = Resources.Resources.lblAccountSettingsChangesCompleteDefaultGroup;
                        break;
                    }
                case GMGColorConstants.ShowAllFilesToAdmins:
                    {
                        optionName = "Determines whether or not show all files filter is available for administrators in collaborate dashboard";
                        break;
                    }
                case GMGColorConstants.ProofStudioShowAnnotationsForExternalUsers:
                    {
                        optionName = Resources.Resources.lblProofStudioShowAnnotationsForExternalUsers;
                        break;
                    }
                case GMGColorConstants.InheritPermissionsFromParentFolder:
                    {
                        optionName = Resources.Resources.lblAutomaticallyInheritParentFolderPermissions;
                        break;
                    }
                case GMGColorConstants.DisableFileDueDateinUploader:
                    {
                        optionName = Resources.Resources.lblDisableFileDueDateinUploader;
                        break;
                    }
            }

            return optionName;
        }

        public static string EncodeUrl(string url)
        {
            int last = url.LastIndexOf("/") + 1;
            string fileName = url.Substring(last);
            string encodedFileName = HttpUtility.UrlEncode(fileName);
            return url.Substring(0, last) + encodedFileName;
        }
    }
}