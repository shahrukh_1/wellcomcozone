﻿using System.Web.Mvc;
using GMGColorBusinessLogic;
using System.Web.Routing;

namespace GMGColor.Common
{
    public class GenericController : Controller
    {
        #region Private Member

        private DbContextBL context;
        #endregion        

        #region Properties
        
        public DbContextBL Context
        {
            get { return context; }
            protected set { context = value; }
        }
        #endregion

        #region Events

        protected override void Initialize(RequestContext requestContext)
        {
            // IPrincipal user = HttpContext.User;
            if (!requestContext.HttpContext.Items.Contains(Utils.DatabaseContext))
            {
                // Attach the context to the HTTPContext to be used for all DB operations performed on views
                // The context is disposed on MvcApplication.Application_EndRequest
                Context = new DbContextBL();
                requestContext.HttpContext.Items.Add(Utils.DatabaseContext, Context);
            }
            else
            {
                Context = requestContext.HttpContext.Items[Utils.DatabaseContext] as DbContextBL;
            }

            base.Initialize(requestContext);
        }

        #endregion
    }
}