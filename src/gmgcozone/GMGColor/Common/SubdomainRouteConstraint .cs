﻿
using System.Web.Routing;
using System.Web;
using GMGColorDAL;
namespace GMGColor.Common
{
    public class ExcludeSubdomainRouteConstraint : IRouteConstraint
    {
        private readonly string[] _subdomains;

        public ExcludeSubdomainRouteConstraint(string[] subdomain)
        {
            _subdomains = subdomain;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if( httpContext.Request.Url != null )
            {
                foreach( string subdomain in _subdomains )
                {
                    if (httpContext.Request.Url.Host.StartsWith(subdomain + GMGColorConfiguration.AppConfiguration.HostAddress))
                    {
                        return false;
                    }
                }
                return true;
            }
            
            return true;
        }
    }

    public class IncludeSubdomainRouteConstraint : IRouteConstraint
    {
        private readonly string[] _subdomains;

        public IncludeSubdomainRouteConstraint(string[] subdomain)
        {
            _subdomains = subdomain;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (httpContext.Request.Url != null)
            {
                foreach (string subdomain in _subdomains)
                {
                    if (httpContext.Request.Url.Host.StartsWith(subdomain + GMGColorConfiguration.AppConfiguration.HostAddress))
                    {
                        return true;
                    }
                }
                return false;
            }

            return true;
        }
    }
}