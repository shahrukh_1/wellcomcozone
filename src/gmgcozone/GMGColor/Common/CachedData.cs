﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Caching;

namespace GMGColor.Common
{
    public static class CacheExtensions
    {
        public static void AddToCache(this Cache cache, string key, object obj, int expirationMinutes)
        {
            cache.Insert(key, obj,
            null, DateTime.Now.AddMinutes(expirationMinutes),
            Cache.NoSlidingExpiration, CacheItemPriority.High, null);
        }

        public static object GetItemFromCache(this Cache cache, string key)
        {
            return cache[key];
        }

        public static void RemoveKeysThatStartsWith(this Cache cache, string prefix)
        {
            var enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix.ToLower()))
                {
                    cache.Remove(enumerator.Key.ToString());
                }
            }
        }
    }
}
