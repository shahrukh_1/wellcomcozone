﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using GMG.CoZone.Common;
using GMGColorBusinessLogic;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using System.Threading.Tasks;
using Syncfusion.HtmlConverter;
using System.Text.RegularExpressions;
using GMGColor.Controllers;
using RestSharp;
using System.Text;
using Newtonsoft.Json.Linq;
using GMGColor.AWS;

namespace GMGColor.FileUpload
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler
    {
        public static Dictionary<string, Dictionary<string, string>> CacheItem = new Dictionary<string, Dictionary<string, string>>();
        private readonly JavaScriptSerializer js;// = new JavaScriptSerializer();
        private string accountRegion = string.Empty;
        private string pathtoStorage = string.Empty;
        private RestClient cloud_convert_client;

        public UploadHandler()
        {
            js = new JavaScriptSerializer();
            js.MaxJsonLength = 100 * 1024 * 1024; // 100 MB
        }

        /// <summary>
        /// Path should! always end with '/'
        /// </summary>
        private string StorageRoot
        {
            get
            {
                if (!String.IsNullOrEmpty(pathtoStorage))
                {
                    return pathtoStorage.ToLower();
                }
                else
                {
                    return "/temp/";
                }
            }
        }

        public bool IsReusable { get { return false; } }

        public void ProcessRequest(HttpContext context)
        {
            if (!GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                Thread.Sleep(1000);
            }

            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");

            accountRegion = context.Request.Params.Get("accountRegion");
            pathtoStorage = context.Request.Params.Get("pathToTempFolder");

            HandleMethod(context);
        }

        // Handle request based on method
        private void HandleMethod(HttpContext context)
        {
            switch (context.Request.HttpMethod)
            {
                case "HEAD":
                    DeleteFile(context);
                    break;
                case "GET":
                    if (GivenFilename(context)) DeliverFile(context);
                    else ListCurrentFiles(context);
                    break;

                case "POST":
                case "PUT":
                    UploadFile(context);
                    break;

                case "DELETE":
                    break;

                case "OPTIONS":
                    ReturnOptions(context);
                    break;

                default:
                    context.Response.ClearHeaders();
                    context.Response.StatusCode = 405;
                    break;
            }
        }

        private static void ReturnOptions(HttpContext context)
        {
            context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
            context.Response.StatusCode = 200;
        }

        // Delete file from the server
        private void DeleteFile(HttpContext context)
        {
            if (context.Request.UrlReferrer != null && context.Request.UrlReferrer.AbsolutePath == "/Approvals/NewApproval")
            {
                string relativeFilePath = StorageRoot + "/" + context.Request["f"];
                string absoluteSourcePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath;
                string approvalFolder = Path.GetDirectoryName(absoluteSourcePath);
                if (Directory.Exists(approvalFolder))
                {
                    Directory.Delete(approvalFolder, true);
                }
            }
            else
            {
                string relativeFilePath = StorageRoot + context.Request["f"];
                string absoluteSourcePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath;
                if (File.Exists(absoluteSourcePath))
                {
                    File.Delete(absoluteSourcePath);
                }
            }
        }

        // Upload file to the server
        private void UploadFile(HttpContext context)
        {
            try
            {
                var statuses = new List<FilesStatus>();
                bool isZipFileUploaded = false;

                // retrieve PDF page count only if needed
                bool isDeliverJob = (context.Request.UrlReferrer != null &&
                                     context.Request.UrlReferrer.AbsolutePath == "/Deliver/NewDeliver");
                bool isApprovalJob = (context.Request.UrlReferrer != null &&
                                      context.Request.UrlReferrer.AbsolutePath == "/Approvals/NewApproval"
                                      || context.Request.UrlReferrer.AbsolutePath == "/ProjectFolders/ProjectFolderApprovalsIndex"
                                      || context.Request.UrlReferrer.AbsolutePath == "/ProjectFolders"
                || context.Request.UrlReferrer.AbsolutePath == "/ProjectFolders/Index");
                bool isNewFileJob = (context.Request.UrlReferrer != null &&
                                     context.Request.UrlReferrer.AbsolutePath == "/FileTransfer/NewFileTransfer");
                bool isImportFileJob = (context.Request.UrlReferrer != null &&
                                        context.Request.UrlReferrer.AbsolutePath == "/Settings/Predefinedtagwords");
                #region Approval params

                int? approvalFolderId = null;
                bool? isNewVersionRequest = null;
                int? uploaderId = null;
                Approval.NewApprovalUploadInfo newApprovalUploadInfo = null;
                ApprovalFileStatus? approvalFileStatus = null;
                AccountFileTypeFilter fileTypeFilter = null;
                string zipFileGuid = string.Empty;
                #endregion

                // Gather all files and unzip zip assets (if any)
                List<AssetFile> approvalFiles = new List<AssetFile>();
                List<AssetFile> approvalZipOriginalFile = new List<AssetFile>();

                for (int i = 0; i < context.Request.Files.Count; i++)
                {
                    HttpPostedFile file = context.Request.Files[i];

                    if ((Path.GetExtension(file.FileName).ToLower() == ".zip") && !isNewFileJob)
                    {
                        approvalZipOriginalFile.Add(new AssetFile()
                        {
                            Name = file.FileName,
                            Stream = file.InputStream,
                            Size = file.ContentLength,
                            fileUUID = context.Request.Headers["fileUUID"],
                            uploadFileSize = Convert.ToInt64(context.Request.Headers["uploadFileSize"]),
                            chunkCount = Convert.ToInt32(context.Request.Headers["chunkCount"]),
                            contentRange = context.Request.Headers["Content-Range"]
                        });

                        MemoryStream destination = new MemoryStream();
                        file.InputStream.CopyTo(destination);
                        approvalFiles.AddRange(GMGColorBusinessLogic.Common.Utils.UnzipStreamRootFolder(destination, file.FileName.Replace(".zip", "")));
                        approvalFiles.AddRange(GMGColorBusinessLogic.Common.Utils.UnzipStreamSubFolders(file.InputStream, file.FileName.Replace(".zip", "")));

                        zipFileGuid = Guid.NewGuid().ToString();
                    }
                    else
                    {
                        approvalFiles.Add(new AssetFile()
                        {
                            Name = file.FileName,
                            Stream = file.InputStream,
                            Size = file.ContentLength,
                            fileUUID = context.Request.Headers["fileUUID"],
                            uploadFileSize = Convert.ToInt64(context.Request.Headers["uploadFileSize"]),
                            chunkCount = Convert.ToInt32(context.Request.Headers["chunkCount"]),
                            contentRange = context.Request.Headers["Content-Range"]
                        });
                    }
                }

                bool success = false;
                using (DbContextBL dbContext = new DbContextBL())
                {
                    if (isApprovalJob || isNewFileJob)
                    {
                        string folderId = context.Request.Params.Get("approvalFolderId");
                        approvalFolderId = String.IsNullOrEmpty(folderId) ? null : (int?)Int32.Parse(folderId);
                        isNewVersionRequest = bool.Parse(context.Request.Params.Get("isNewVersionRequest"));
                        uploaderId = Int32.Parse(context.Request.Params.Get("uploader"));

                        approvalFileStatus = isNewVersionRequest.GetValueOrDefault()
                            ? ApprovalFileStatus.NewVersion
                            : ApprovalFileStatus.NewJob;

                        if (!isNewVersionRequest.Value && approvalFolderId.HasValue && approvalFolderId.Value > 0)
                        {
                            newApprovalUploadInfo = ApprovalBL.GetUEngineAccountSettingByUserId(uploaderId.Value,
                                dbContext);
                        }

                        fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(uploaderId.GetValueOrDefault(), dbContext);
                    }

                    if (zipFileGuid != "")
                    {
                        var listoftasks = new List<Task>();
                        string errorMessage = null;
                        string fileDimensions = null;
                        int isHtmlExist = approvalFiles.Where(x => x.Name.Contains(".html")).Count();
                        if (isHtmlExist == 0)
                        {
                            errorMessage = Resources.Resources.lblApprovalZipFileUploadError;
                        }
                        else
                        {
                            UploadZipFile(approvalZipOriginalFile[0], zipFileGuid, newApprovalUploadInfo, fileTypeFilter, dbContext, approvalFileStatus, approvalFolderId, isNewVersionRequest);

                            foreach (var file in approvalFiles)
                            {
                                listoftasks.Add(Task.Factory.StartNew(() =>
                               UploadZipFile(file, zipFileGuid, newApprovalUploadInfo, fileTypeFilter, dbContext, approvalFileStatus, approvalFolderId, isNewVersionRequest)));
                            }

                            Task.WhenAll(listoftasks).Wait();
                            Thread.Sleep(1000);
                            fileDimensions = GetHtmlFileDimensions(approvalZipOriginalFile[0].Name, zipFileGuid, approvalFiles.Count);

                        }

                        if (fileTypeFilter != null && !isNewFileJob)
                        {
                            if (!ApprovalBL.IsValidApprovalFileType(fileTypeFilter, Path.GetExtension(approvalZipOriginalFile[0].Name).ToLower()))
                            {
                                errorMessage = Resources.Resources.errFileTypeNotAllowed;
                            }
                        }

                        if (errorMessage == null)
                        {
                            if (approvalFileStatus != null &&
                                (approvalFileStatus == ApprovalFileStatus.DuplicatedFile ||
                                 approvalFileStatus == ApprovalFileStatus.IgnoredFile))
                            {
                                errorMessage = Resources.Resources.lblApprovalIgnoreFile;
                            }
                        }

                        statuses.Add(new FilesStatus(approvalZipOriginalFile[0].Name, approvalZipOriginalFile[0].Size, StorageRoot + zipFileGuid + "/" + approvalZipOriginalFile[0].Name, accountRegion)
                        {
                            pagesCount = 0,
                            fileguid = zipFileGuid,
                            approvalFileStatus =
                                       approvalFileStatus != null ? approvalFileStatus.ToString() : String.Empty,
                            ApprovalTypeValue = ApprovalBL.GetApprovalType(Path.GetExtension(approvalZipOriginalFile[0].Name).ToLower(), dbContext),
                            approvalFileStatusLabel = approvalFileStatus == ApprovalFileStatus.NewJob ? Resources.Resources.lblApprovalNewJob : Resources.Resources.lblApprovalNewVersion,
                            error = errorMessage,
                            fileHtmlDimensions = fileDimensions
                        });
                        success = true;
                    }
                    else
                    {
                        foreach (var file in approvalFiles)
                        {
                            string fileName = file.Name;
                            if (Path.GetExtension(fileName).ToLower() == ".doc" || Path.GetExtension(fileName).ToLower() == ".docx" || Path.GetExtension(fileName).ToLower() == ".ppt" || Path.GetExtension(fileName).ToLower() == ".pptx")
                            {
                                if (fileName.Length > 47) // cloud convert will take filename upto a max of char length 50
                                {
                                    fileName = fileName.Substring(0, 43) + Path.GetExtension(fileName);
                                }
                            }
                            string fileGuid = string.Empty;
                            Dictionary<string, string> chunkFileDetails = new Dictionary<string, string>();
                            if (file.fileUUID != null)
                            {
                                if (!CacheItem.ContainsKey(file.fileUUID))
                                {
                                    fileGuid = (isApprovalJob || isNewFileJob) ? Guid.NewGuid().ToString() : null;
                                    Dictionary<string, string> chunkFileInfo = new Dictionary<string, string>();
                                    chunkFileInfo["fileGuid"] = fileGuid;
                                    chunkFileInfo["awsFileUploadId"] = "";
                                    CacheItem.Add(file.fileUUID, chunkFileInfo);
                                }
                                else
                                {
                                    Dictionary<string, string> chunkFileInfo = CacheItem[file.fileUUID];
                                    chunkFileDetails = CacheItem[file.fileUUID];
                                    fileGuid = chunkFileInfo["fileGuid"];
                                }
                            }
                            else
                            {
                                fileGuid = (isApprovalJob || isNewFileJob) ? Guid.NewGuid().ToString() : null;
                            }

                            var fullPath = String.IsNullOrEmpty(fileGuid)
                                ? StorageRoot + fileName
                                : StorageRoot + fileGuid + "/" + fileName;

                            string destinationFolderPath = StorageRoot +
                                                           (string.IsNullOrEmpty(fileGuid) ? string.Empty : fileGuid + "/");
                            string destinationFilePath = string.Empty;

                            int pagesCount = !isDeliverJob && !isApprovalJob
                                ? 0
                                : ((fileName ?? string.Empty).ToLower().EndsWith(".pdf")
                                    ? Common.Utils.GetPdfPageCount(file.Stream, fileName)
                                    : 1);

                            long fileSize = file.Size;

                            // make sure to create the temp path if does not exists
                            GMGColorIO.FolderExists(destinationFolderPath, accountRegion, true);
                            GMGColorLogging.log.Info($" DONE: created item path {destinationFolderPath}");

                            // calling file scanner to check the uploaded file stream for virus and malicious mime type
                            GMG.CoZone.ScanFile.ScanFile fileScanner = new GMG.CoZone.ScanFile.ScanFile();
                            bool IsCleanFile = fileScanner.ProcessFileScanner(file.Stream, file.Name);
                            if (!IsCleanFile)
                                throw new Exception("Issue in file scanner");

                            string errorMessage = null;

                            if (file.fileUUID == null || file.fileUUID == string.Empty)
                            {
                                success = GMGColorIO.UploadFile(file.Stream, destinationFolderPath + fileName, accountRegion, false);
                            }
                            else
                            {
                                success = GMGColorIO.UploadChunkFile(file.Stream, destinationFolderPath, accountRegion, CacheItem[file.fileUUID], file.chunkCount, file.contentRange, fileName, file.uploadFileSize, false);
                            }
                            if (success)
                            {
                                if (Path.GetExtension(fileName).ToLower() == ".docx" || Path.GetExtension(fileName).ToLower() == ".ppt" || Path.GetExtension(fileName).ToLower() == ".pptx")
                                {
                                    errorMessage = ConvertDocumentToPdf(fileGuid, fileName, fileSize.ToString(), accountRegion, destinationFolderPath);
                                    if(errorMessage == "1")
                                    {
                                        errorMessage = null;
                                    }
                                }
                                if(Path.GetExtension(fileName).ToLower() == ".doc")
                                {
                                    errorMessage = ConvertDocToPdf(fileGuid, fileName, fileSize.ToString(), accountRegion, destinationFolderPath);
                                    if (errorMessage == "1")
                                    {
                                        errorMessage = null;
                                    }
                                }
                                if (file.fileUUID != null)
                                    CacheItem.Remove(file.fileUUID);
                                if (newApprovalUploadInfo != null && newApprovalUploadInfo.CanApplySettings)
                                {
                                    Job job;
                                    approvalFileStatus = ApprovalBL.NewFileStatus(newApprovalUploadInfo.AccountId,
                                        file.Name, approvalFolderId.Value,
                                        (FileUploadDuplicateName)newApprovalUploadInfo.DuplicateFileKey, out job, dbContext);
                                }

                                string approvalFileStatusLabel = null;
                                if (approvalFileStatus != null)
                                {
                                    switch (approvalFileStatus)
                                    {
                                        case ApprovalFileStatus.NewJob:
                                            approvalFileStatusLabel = Resources.Resources.lblApprovalNewJob;
                                            break;
                                        case ApprovalFileStatus.NewVersion:
                                            approvalFileStatusLabel = Resources.Resources.lblApprovalNewVersion;
                                            break;
                                        case ApprovalFileStatus.DuplicatedFile:
                                        case ApprovalFileStatus.IgnoredFile:
                                            approvalFileStatusLabel = Resources.Resources.lblApprovalIgnoreFile;
                                            break;
                                    }
                                }

                                //check if add new version was explicitly selected, in this case settings shouldn't be disabled
                                if (approvalFileStatus != null && isNewVersionRequest.GetValueOrDefault())
                                {
                                    approvalFileStatus = null;
                                }

                                //for files uploaded from approval check if file type is valid based on account upload settings
                                if (fileTypeFilter != null && !isNewFileJob)
                                {
                                    if (!ApprovalBL.IsValidApprovalFileType(fileTypeFilter, Path.GetExtension(fileName).ToLower()))
                                    {
                                        errorMessage = Resources.Resources.errFileTypeNotAllowed;
                                    }
                                }

                                if (errorMessage == null)
                                {
                                    if (approvalFileStatus != null &&
                                        (approvalFileStatus == ApprovalFileStatus.DuplicatedFile ||
                                         approvalFileStatus == ApprovalFileStatus.IgnoredFile))
                                    {
                                        errorMessage = Resources.Resources.lblApprovalIgnoreFile;
                                    }
                                }
                                statuses.Add(new FilesStatus(fileName, fileSize, fullPath, accountRegion)
                                {
                                    pagesCount = pagesCount,
                                    fileguid = fileGuid,
                                    ApprovalTypeValue = ApprovalBL.GetApprovalType(Path.GetExtension(fileName).ToLower(), dbContext),
                                    approvalFileStatus =
                                        approvalFileStatus != null ? approvalFileStatus.ToString() : String.Empty,
                                    approvalFileStatusLabel = approvalFileStatusLabel,
                                    error = errorMessage
                                });
                            }
                            if (isImportFileJob)
                            {
                                GMGColor.Controllers.SettingsController.AddPredefinedtagwordsFromFile(destinationFolderPath + file.Name, accountRegion);
                            }
                        }
                    }

                }
                if (success)
                    WriteJsonIframeSafe(context, statuses);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, 0, "UploadHandler: Upload file failed. {0}", ex.Message);
                throw;
            }
        }

        public string ConvertDocToPdf(string guid, string fileName, string fileSize, string accountRegion, string tempFolderPath)
        {
            string returnStatus = "1";
            try
            {
                List<int> fileSizes = fileSize.Split(',').Select(x => int.Parse(x)).ToList();
                int fileSizeInKb = fileSizes.Sum();
                string approvalTempPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + tempFolderPath + fileName;

                string aws_source_access_key_id = GMGColorConfiguration.AppConfiguration.AWSAccessKey;
                string aws_source_secret_access_key = GMGColorConfiguration.AppConfiguration.AWSSecretKey;
                string aws_source_bucket = GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion);
                string aws_source_region = accountRegion;
                string cloudconver_api_key = GMGColorConfiguration.AppConfiguration.CloudConvertApiKey;

                cloud_convert_client = new RestClient("https://api.cloudconvert.com/v2/jobs");

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", "Bearer " + cloudconver_api_key);
                StringBuilder json_body = new StringBuilder();
                json_body.AppendLine("{");
                json_body.AppendLine("\"tasks\": {");
                json_body.AppendLine("\"import-1" + "\": {");
                json_body.AppendLine("\"operation\": \"import/s3\",");
                json_body.AppendLine("\"bucket\": \"" + aws_source_bucket + "\",");
                json_body.AppendLine("\"region\": \"" + aws_source_region + "\",");
                json_body.AppendLine("\"access_key_id\": \"" + aws_source_access_key_id + "\",");
                json_body.AppendLine("\"secret_access_key\": \"" + aws_source_secret_access_key + "\",");
                json_body.AppendLine("\"key\": \"" + tempFolderPath + fileName + "\"");
                json_body.AppendLine("},");

                json_body.AppendLine("\"work\": {");
                json_body.AppendLine("\"operation\": \"convert\",");
                json_body.AppendLine("\"output_format\": \"pdf\",");
                json_body.AppendLine("\"engine\": \"libreoffice\",");
                json_body.AppendLine("\"input\": [");
                json_body.AppendLine("\"import-1" + "\",");
                json_body.AppendLine("],");
                json_body.AppendLine("\"filename\": \"" + fileName.Replace(".doc", "") + ".pdf" + "\"");
                json_body.AppendLine("},");
                json_body.AppendLine("\"export-1\": {");
                json_body.AppendLine("\"operation\": \"export/s3\",");
                json_body.AppendLine("\"input\": [\"work\"],");
                json_body.AppendLine("\"bucket\": \"" + aws_source_bucket + "\",");
                json_body.AppendLine("\"region\": \"" + aws_source_region + "\",");
                json_body.AppendLine("\"access_key_id\": \"" + aws_source_access_key_id + "\",");
                json_body.AppendLine("\"secret_access_key\": \"" + aws_source_secret_access_key + "\",");
                json_body.AppendLine("\"key\": \"" + tempFolderPath + fileName.Replace(".doc", "") + ".pdf" + "\"");
                json_body.AppendLine("}");
                json_body.AppendLine("}");
                json_body.AppendLine("}");

                var jsonRequestBody = JObject.Parse(json_body.ToString());
                request.AddParameter("application/json", jsonRequestBody, ParameterType.RequestBody);
                IRestResponse response = cloud_convert_client.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    var responseContent = JObject.Parse(response.Content);
                    string id = responseContent["data"]["id"].ToString();
                    var taskDetails = (JArray)responseContent["data"]["tasks"];
                    string info = GetInfo(id, cloudconver_api_key);

                    AWSSimpleStorageService.DeleteAllFiles(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), tempFolderPath + guid, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    returnStatus = "An error occured while converting document to pdf";
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("An error occured while converting document to pdf", ex);
                returnStatus = "An error occured while converting document to pdf";
            }
            return returnStatus;
        }


        public string ConvertDocumentToPdf(string guid, string fileName, string fileSize, string accountRegion, string tempFolderPath)
        {
            string returnStatus = "1";
            try
            {
                List<int> fileSizes = fileSize.Split(',').Select(x => int.Parse(x)).ToList();
                int fileSizeInKb = fileSizes.Sum();
                string approvalTempPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + tempFolderPath + fileName;

                string aws_source_access_key_id = GMGColorConfiguration.AppConfiguration.AWSAccessKey;
                string aws_source_secret_access_key = GMGColorConfiguration.AppConfiguration.AWSSecretKey;
                string aws_source_bucket = GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion);
                string aws_source_region = accountRegion;
                string cloudconver_api_key = GMGColorConfiguration.AppConfiguration.CloudConvertApiKey;

                cloud_convert_client = new RestClient("https://api.cloudconvert.com/v2/jobs");

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", "Bearer " + cloudconver_api_key);
                StringBuilder json_body = new StringBuilder();
                json_body.AppendLine("{");
                json_body.AppendLine("\"tasks\": {");
                    json_body.AppendLine("\"import-1" + "\": {");
                    json_body.AppendLine("\"operation\": \"import/s3\",");
                    json_body.AppendLine("\"bucket\": \"" + aws_source_bucket + "\",");
                    json_body.AppendLine("\"region\": \"" + aws_source_region + "\",");
                    json_body.AppendLine("\"access_key_id\": \"" + aws_source_access_key_id + "\",");
                    json_body.AppendLine("\"secret_access_key\": \"" + aws_source_secret_access_key + "\",");
                    json_body.AppendLine("\"key\": \"" + tempFolderPath + fileName + "\"");
                    json_body.AppendLine("},");

                json_body.AppendLine("\"work\": {");
                json_body.AppendLine("\"operation\": \"convert\",");
                json_body.AppendLine("\"output_format\": \"pdf\",");
                json_body.AppendLine("\"engine\": \"office\",");
                json_body.AppendLine("\"input\": [");
                        json_body.AppendLine("\"import-1" + "\",");
                json_body.AppendLine("],");
                json_body.AppendLine("\"filename\": \"" + fileName.Replace(".docx", ".pdf").Replace(".pptx",".pdf").Replace(".ppt", ".pdf") + "\"");
                json_body.AppendLine("},");
                json_body.AppendLine("\"export-1\": {");
                json_body.AppendLine("\"operation\": \"export/s3\",");
                json_body.AppendLine("\"input\": [\"work\"],");
                json_body.AppendLine("\"bucket\": \"" + aws_source_bucket + "\",");
                json_body.AppendLine("\"region\": \"" + aws_source_region + "\",");
                json_body.AppendLine("\"access_key_id\": \"" + aws_source_access_key_id + "\",");
                json_body.AppendLine("\"secret_access_key\": \"" + aws_source_secret_access_key + "\",");
                json_body.AppendLine("\"key\": \"" + tempFolderPath + fileName.Replace(".docx", ".pdf").Replace(".pptx", ".pdf").Replace(".ppt", ".pdf") + "\"");
                json_body.AppendLine("}");
                json_body.AppendLine("}");
                json_body.AppendLine("}");

                var jsonRequestBody = JObject.Parse(json_body.ToString());
                request.AddParameter("application/json", jsonRequestBody, ParameterType.RequestBody);
                IRestResponse response = cloud_convert_client.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    var responseContent = JObject.Parse(response.Content);
                    string id = responseContent["data"]["id"].ToString();
                    var taskDetails = (JArray)responseContent["data"]["tasks"];
                    string info = GetInfo(id, cloudconver_api_key);

                    AWSSimpleStorageService.DeleteAllFiles(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), tempFolderPath + guid, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    returnStatus = "An error occured while converting document to pdf";
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error( "An error occured while converting document to pdf", ex);
                returnStatus = "An error occured while converting document to pdf";
            }
            return returnStatus;
        }
        public string GetInfo(string id, string cloudconver_api_key)
        {
            RestClient restClient = new RestClient("https://api.cloudconvert.com/v2/jobs/" + id + "/wait ");
            RestRequest request = new RestRequest();
            request.AddHeader("Content-type", "application/json");
            request.AddHeader("Authorization", "Bearer " + cloudconver_api_key);
            request.Timeout = 1800000;
            IRestResponse cloudResponse = null;
            cloudResponse = restClient.Get(request);
            var Content = JObject.Parse(cloudResponse.Content);
            string status = Content["data"]["status"].ToString();

            return Content.ToString();
        }
        private async Task<bool> UploadZipFile(AssetFile file, string fileGuid, Approval.NewApprovalUploadInfo newApprovalUploadInfo, AccountFileTypeFilter fileTypeFilter, DbContextBL dbContext, ApprovalFileStatus? approvalFileStatus, int? approvalFolderId, bool? isNewVersionRequest)
        {
            bool success = false;
            string fileName = file.Name;
            var fullPath = String.IsNullOrEmpty(fileGuid)
                ? StorageRoot + fileName
                : StorageRoot + fileGuid + "/" + fileName;

            string destinationFolderPath = StorageRoot +
                                           (string.IsNullOrEmpty(fileGuid) ? string.Empty : fileGuid + "/");
            string destinationFilePath = string.Empty;

            // make sure to create the temp path if does not exists
            GMGColorIO.FolderExists(destinationFolderPath, accountRegion, true);
            GMGColorLogging.log.Info($" DONE: created item path {destinationFolderPath}");

            //// calling file scanner to check the uploaded file stream for virus and malicious mime type
            //GMG.CoZone.ScanFile.ScanFile fileScanner = new GMG.CoZone.ScanFile.ScanFile();
            //bool IsCleanFile = fileScanner.ProcessFileScanner(file.Stream, file.Name);
            //if (!IsCleanFile)
            //    throw new Exception("Issue in file scanner");


            if (file.fileUUID == null || file.fileUUID == string.Empty)
            {
                success = await GMGColorIO.UploadZipFile(file.Stream, destinationFolderPath + fileName, accountRegion, false);
            }
            else
            {
                success = GMGColorIO.UploadChunkFile(file.Stream, destinationFolderPath, accountRegion, CacheItem[file.fileUUID], file.chunkCount, file.contentRange, fileName, file.uploadFileSize, false);
            }

            return success;
        }


        // Upload partial file
        private void UploadPartialFile(string fileName, HttpContext context, List<FilesStatus> statuses, string region, bool retrievePDFPageCount)
        {
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    var file = context.Request.Files[i];

                    var fullPath = StorageRoot + fileName;
                    var destinationFilePath = StorageRoot.Replace(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion), string.Empty).Replace("\\", "/").ToLower().Trim() + fileName;

                    int fileLength = Convert.ToInt32(file.InputStream.Length);
                    int pagesCount = !retrievePDFPageCount ? 0 : ((fileName ?? string.Empty).ToLower().EndsWith(".pdf") ? Common.Utils.GetPdfPageCount(file.InputStream, fileName) : 1);
                    bool success = AWS.AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), destinationFilePath, file.InputStream, file.ContentLength,
                                                                            GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    if (success)
                    {
                        statuses.Add(new FilesStatus(fileName, fileLength, fullPath, accountRegion) { pagesCount = pagesCount });
                    }
                }
                else
                {
                    var inputStream = context.Request.Files[i].InputStream;
                    var file = context.Request.Files[i];
                    var fullName = StorageRoot + Path.GetFileName(fileName);
                    string relativeSourcePath = StorageRoot + Path.GetFileName(file.FileName);
                    string absoluteSourcePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeSourcePath;

                    using (var fs = new FileStream(absoluteSourcePath, FileMode.Append, FileAccess.Write))
                    {
                        var buffer = new byte[1024];

                        var l = inputStream.Read(buffer, 0, 1024);
                        while (l > 0)
                        {
                            fs.Write(buffer, 0, l);
                            l = inputStream.Read(buffer, 0, 1024);
                        }
                        fs.Flush();
                        fs.Close();
                    }

                    // TODO - get page count for deliver jobs only
                    int pagesCount = !retrievePDFPageCount ? 1 : (fullName.ToLower().EndsWith(".pdf") ? Common.Utils.GetPdfPageCount(absoluteSourcePath) : 1);

                    statuses.Add(new FilesStatus(fullName, file.ContentLength, relativeSourcePath, accountRegion) { pagesCount = pagesCount });
                }
            }
        }

        // Upload entire file
        private void UploadWholeFile(HttpContext context, List<FilesStatus> statuses, string region, bool retrievePDFPageCount)
        {
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                var file = context.Request.Files[i];
                string relativeSourcePath = StorageRoot + Path.GetFileName(file.FileName);
                string absoluteSourcePath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeSourcePath;
                file.SaveAs(absoluteSourcePath);

                string fullName = Path.GetFileName(file.FileName);
                int pagesCount = !retrievePDFPageCount ? 1 : (absoluteSourcePath.ToLower().EndsWith(".pdf") ? Common.Utils.GetPdfPageCount(file.InputStream, file.FileName) : 1);
                statuses.Add(new FilesStatus(fullName, file.ContentLength, relativeSourcePath, accountRegion) { pagesCount = pagesCount });
            }
        }

        private void WriteJsonIframeSafe(HttpContext context, List<FilesStatus> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
            try
            {
                if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                    context.Response.ContentType = "application/json";
                else
                    context.Response.ContentType = "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }

            var jsonObj = js.Serialize(statuses.ToArray());
            context.Response.Write(jsonObj);
        }

        private static bool GivenFilename(HttpContext context)
        {
            return !string.IsNullOrEmpty(context.Request["f"]);
        }

        private void DeliverFile(HttpContext context)
        {
            var filename = context.Request["f"];
            var filePath = StorageRoot + filename;

            if (File.Exists(filePath))
            {
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                context.Response.ContentType = "application/octet-stream";
                context.Response.ClearContent();
                context.Response.WriteFile(filePath);
            }
            else
                context.Response.StatusCode = 404;
        }

        private void ListCurrentFiles(HttpContext context)
        {
            var files =
                new DirectoryInfo(StorageRoot)
                    .GetFiles("*", SearchOption.TopDirectoryOnly)
                    .Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden))
                    .Select(f => new FilesStatus(f, accountRegion))
                    .ToArray();

            string jsonObj = js.Serialize(files);
            context.Response.AddHeader("Content-Disposition", "inline; filename=\"files.json\"");
            context.Response.Write(jsonObj);
            context.Response.ContentType = "application/json";
        }

        private string GetHtmlFileDimensions(string fileName, string fileGuid, int totalFiles)
        {

            string str = "{ \"filePath\":\"approvals\\\\0ce60179-daa5-470e-a411-c50f2faa0c6f\"}";


            Newtonsoft.Json.Linq.JObject json1 = Newtonsoft.Json.Linq.JObject.Parse(str);

            object approvalsBasic1 = Newtonsoft.Json.JsonConvert.DeserializeObject<object>(str);


            List<string> lsthtmlfiles = new List<string>();
            string fileStoragePath = (StorageRoot + fileGuid + "\\" + fileName).Replace(@"\", "/");
            lsthtmlfiles = GetHtmlListObjects(fileStoragePath, totalFiles);
            if (lsthtmlfiles.Count == 0)
            {
                return null;
            }

            List<System.Threading.Tasks.Task> AsyncList = new List<System.Threading.Tasks.Task>();
            Dictionary<int, string> htmlfilePath = new Dictionary<int, string>();
            List<Dictionary<int, string>> temphtmlfilePath = new List<Dictionary<int, string>>();

            int cnt = 0;
            foreach (var obj in lsthtmlfiles)
            {
                string htmlFileStoragePath = (obj.ToString().Substring(obj.ToString().LastIndexOf(fileGuid), (obj.ToString().Length - (obj.ToString().LastIndexOf(fileGuid)))));
                string fileFolderPath = obj.ToString().Substring(obj.ToString().LastIndexOf(fileName.Replace(".zip", "")), (obj.ToString().Length - (obj.ToString().LastIndexOf(fileName.Replace(".zip", "")))));

                htmlfilePath.Add(cnt++, "{\"filePath\":\"approvals\\" + htmlFileStoragePath + "\",\"fileFolderPath\":\"" + fileFolderPath + "\",\"Width\": \"" + 0 + "\", \"Height\": \"" + 0 + "\" },");
            }
            cnt = 0;
            var tasks = new List<Task>();
            List<FilesStatus> htmlfileStatus = new List<FilesStatus>();
            System.Text.StringBuilder fileDimensions = new System.Text.StringBuilder();
            fileDimensions.Append("{\"htmlfileList\":[");
            foreach (var obj in lsthtmlfiles)
            {
                Thread.Sleep(250);
                tasks.Add(Task.Factory.StartNew(() => temphtmlfilePath.Add(ExtractHtmlDimensions(obj.ToString(), fileGuid, fileName.Replace(".zip", ""), cnt++))));
            }
            Task.WhenAll(tasks).Wait();

            //To change: Updating the width height based on extracted values
            foreach (var obj in temphtmlfilePath)
            {
                foreach (KeyValuePair<int, string> dict in obj)
                {
                    Thread.Sleep(250);
                    Newtonsoft.Json.Linq.JObject jsonhtmlDimesion = Newtonsoft.Json.Linq.JObject.Parse(dict.Value.TrimEnd(',').Replace("\\", @"\\"));
                    if ((string)jsonhtmlDimesion["Width"] == "0")
                    {
                        var mnlHtmlDim = ManualExtractHtmlDimensions(lsthtmlfiles[dict.Key]);
                        jsonhtmlDimesion["Width"] = mnlHtmlDim[0].ToString();
                        jsonhtmlDimesion["Height"] = mnlHtmlDim[1].ToString();
                        htmlfilePath[dict.Key] = jsonhtmlDimesion.ToString().Replace(@"\\", @"\") + ",";
                    }
                    else
                    {
                        htmlfilePath[dict.Key] = dict.Value;
                    }
                }
            }

            fileDimensions.Append(string.Join("", htmlfilePath.Select(kv => kv.Value).ToArray()));

            return fileDimensions.ToString().TrimEnd(',') + "]}";
        }

        private List<string> GetHtmlListObjects(string fileStoragePath, int totalFiles)
        {
            List<string> htmllistobject = new List<string>();
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                List<string> lsthtmlfilesS3Object = new List<string>();

                //-------------> Retry to retrieve files from s3 bucket
                for (int i = 0; i < 3; i++)
                {
                    lsthtmlfilesS3Object = AWS.AWSSimpleStorageService.GetListObjects(GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion),
                                            GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                            fileStoragePath.Replace(".zip", ""));

                    if (totalFiles == lsthtmlfilesS3Object.Count)
                    {
                        break;
                    }
                }
                if (lsthtmlfilesS3Object != null)
                {
                    htmllistobject = lsthtmlfilesS3Object.Where(x => x.EndsWith(".html") && !x.Contains("__MACOSX")).Select(x => GMGColorConfiguration.AppConfiguration.PathToHtmlDataFolder(accountRegion) + x).ToList();
                }

            }
            else
            {
                string destinationFolderPath = (GMGColorConfiguration.AppConfiguration.FileSystemPathToDataFolder + fileStoragePath).Replace(@"/", "\\");

                htmllistobject = System.IO.Directory.GetFiles(destinationFolderPath.Replace(".zip", ""),
                    "*.html", SearchOption.AllDirectories).Where(x => !x.Contains("__MACOSX")).Select(x => x).ToList();
            }
            return htmllistobject;
        }
        public Dictionary<int, string> ExtractHtmlDimensions(string txtUrlPath, string fileStoragePath, string fileName, int pathkey)
        {
            string htmlFileStoragePath = txtUrlPath.Substring(txtUrlPath.LastIndexOf(fileStoragePath), (txtUrlPath.Length - (txtUrlPath.LastIndexOf(fileStoragePath))));
            string fileFolderPath = txtUrlPath.Substring(txtUrlPath.LastIndexOf(fileName), (txtUrlPath.Length - (txtUrlPath.LastIndexOf(fileName))));
            Dictionary<int, string> returHtmlPath = new Dictionary<int, string>();
            try
            {
                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);
                System.Threading.Thread.Sleep(200);
                WebKitConverterSettings settings = new WebKitConverterSettings();
                settings.EnableJavaScript = false;
                settings.AdditionalDelay = 1500;
                settings.EnableHyperLink = false;

                //Set WebKit path
                settings.WebKitPath = System.Web.HttpContext.Current == null ? System.Web.Hosting.HostingEnvironment.MapPath("~/QtBinaries/") : System.Web.HttpContext.Current.Server.MapPath("~/QtBinaries/");

                //Assign WebKit settings to HTML converter
                htmlConverter.ConverterSettings = settings;

                //Convert URL to PDF
                Syncfusion.Pdf.PdfDocument document = htmlConverter.Convert(txtUrlPath);
                System.Drawing.Image[] extractedImages = null;//document.Pages[0].ExtractImages();
                List<int> imageWidthHeight = new List<int>();
                if (document.Pages.Count > 1)
                {
                    imageWidthHeight.Add(0);
                    imageWidthHeight.Add(0);
                }
                else
                {
                    extractedImages = document.Pages[0].ExtractImages();
                    imageWidthHeight.Add(extractedImages[0].Width);
                    imageWidthHeight.Add(extractedImages[0].Height);
                }
                document.Close(true);

                returHtmlPath.Add(pathkey,"{\"filePath\":\"approvals\\" + htmlFileStoragePath + "\",\"fileFolderPath\":\"" + fileFolderPath + "\",\"Width\": \"" + extractedImages[0].Width + "\", \"Height\": \"" + extractedImages[0].Height + "\" },");
                return returHtmlPath;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, 0, "ExtractHtmlDimensions failed at {0}, Stacktrace:{1}", ex.Message, ex.StackTrace);

            }
            returHtmlPath.Add(pathkey, "{\"filePath\":\"approvals\\" + htmlFileStoragePath + "\",\"fileFolderPath\":\"" + fileFolderPath + "\",\"Width\": \"" + 0 + "\", \"Height\": \"" + 0 + "\" },");
            return returHtmlPath;
        }

        public List<int> ManualExtractHtmlDimensions(string txtUrlPath)
        {
            List<int> htmldimensions = new List<int>();
            try
            {
                string content = "";
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    content = client.DownloadString(txtUrlPath);
                }
                string htmlwidth = "", htmlheight = "";
                var lstwidth = Regex.Matches(content, @"width:(.*?)px");
                var lstheight = Regex.Matches(content, @"height:(.*?)px");

                if (lstwidth.Count == 0)
                {
                    var getwidth = Regex.Matches(content, @"content=""width=(.*?),");
                    if (getwidth.Count > 0)
                    {
                        if (getwidth[0].ToString().Contains("%"))
                        {
                            htmlwidth = getwidth[1].ToString().Substring(15, (getwidth[1].ToString().LastIndexOf(',') - 15));
                        }
                        else
                        {
                            htmlwidth = getwidth[0].ToString().Substring(15, (getwidth[0].ToString().LastIndexOf(',') - 15));
                        }
                    }
                }
                else
                {
                    if (lstwidth[0].ToString().Contains("%"))
                    {
                        htmlwidth = lstwidth[1].ToString().Substring(6, (lstwidth[1].ToString().LastIndexOf("px") - 6));
                    }
                    else
                    {
                        htmlwidth = lstwidth[0].ToString().Substring(6, (lstwidth[0].ToString().LastIndexOf("px") - 6));
                    }
                }

                if (lstheight.Count == 0)
                {
                    var getheight = Regex.Matches(content, @",height=(.*?)""");

                    if (getheight.Count > 0)
                    {
                        htmlheight = getheight[0].ToString().Substring(8, (getheight[0].ToString().LastIndexOf('"') - 8));
                    }
                }
                else
                {
                    htmlheight = lstheight[0].ToString().Substring(7, (lstheight[0].ToString().LastIndexOf("px") - 7));
                }

                int widthnumeric, heightnumeric;
                if (int.TryParse(htmlwidth, out widthnumeric) && int.TryParse(htmlheight, out heightnumeric))
                {
                    if (widthnumeric == 0 || heightnumeric == 0)
                    {
                        htmldimensions.Add(0);
                        htmldimensions.Add(0);
                    }
                    else
                    {
                        htmldimensions.Add(widthnumeric);
                        htmldimensions.Add(heightnumeric);
                    }
                }
                else
                {
                    htmldimensions.Add(0);
                    htmldimensions.Add(0);
                }

                return htmldimensions;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, 0, "ExtractHtmlDimensions failed at {0}, Stacktrace:{1}", ex.Message, ex.StackTrace);

            }
            htmldimensions.Add(0);
            htmldimensions.Add(0);
            return htmldimensions;
        }


        public Dictionary<int, string> ManualExtractHtmlDimensions_NeedToUse(string txtUrlPath, string fileStoragePath, string fileName, int pathkey)
        {
            string htmlFileStoragePath = txtUrlPath.Substring(txtUrlPath.LastIndexOf(fileStoragePath), (txtUrlPath.Length - (txtUrlPath.LastIndexOf(fileStoragePath))));
            string fileFolderPath = txtUrlPath.Substring(txtUrlPath.LastIndexOf(fileName), (txtUrlPath.Length - (txtUrlPath.LastIndexOf(fileName))));
            Dictionary<int, string> returHtmlPath = new Dictionary<int, string>();
            try
            {
                string content = "";
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    content = client.DownloadString(txtUrlPath);
                }
                string htmlwidth = "", htmlheight = "";
                var lstwidth = Regex.Matches(content, @"width:(.*?)px");
                var lstheight = Regex.Matches(content, @"height:(.*?)px");

                if (lstwidth.Count == 0)
                {
                    var getwidth = Regex.Matches(content, @"content=""width=(.*?),");
                    if (getwidth.Count > 0)
                    {
                        if (getwidth[0].ToString().Contains("%"))
                        {
                            htmlwidth = getwidth[1].ToString().Substring(15, (getwidth[1].ToString().LastIndexOf(',') - 15));
                        }
                        else
                        {
                            htmlwidth = getwidth[0].ToString().Substring(15, (getwidth[0].ToString().LastIndexOf(',') - 15));
                        }
                    }
                }
                else
                {
                    if (lstwidth[0].ToString().Contains("%"))
                    {
                        htmlwidth = lstwidth[1].ToString().Substring(6, (lstwidth[1].ToString().LastIndexOf(',') - 6));
                    }
                    else
                    {
                        htmlwidth = lstwidth[0].ToString().Substring(6, (lstwidth[0].ToString().LastIndexOf(',') - 6));
                    }
                }

                if (lstheight.Count == 0)
                {
                    var getheight = Regex.Matches(content, @",height=(.*?)""");

                    if (getheight.Count > 0)
                    {
                        htmlheight = getheight[0].ToString().Substring(8, (getheight[0].ToString().LastIndexOf('"') - 8));
                    }
                }
                else
                {
                    htmlheight = lstheight[0].ToString().Substring(7, (lstheight[0].ToString().LastIndexOf("px") - 7));
                }

                int widthnumeric, heightnumeric;
                if (int.TryParse(htmlwidth, out widthnumeric) && int.TryParse(htmlheight, out heightnumeric))
                    returHtmlPath.Add(pathkey, "{\"filePath\":\"approvals/" + htmlFileStoragePath + "\",\"fileFolderPath\":\"" + fileFolderPath + "\",\"Width\": \"" + htmlwidth + "\", \"Height\": \"" + htmlheight + "\" },");
                else
                    returHtmlPath.Add(pathkey, "{\"filePath\":\"approvals/" + htmlFileStoragePath + "\",\"fileFolderPath\":\"" + fileFolderPath + "\",\"Width\": \"" + 0 + "\", \"Height\": \"" + 0 + "\" },");

                return returHtmlPath;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, 0, "ExtractHtmlDimensions failed at {0}, Stacktrace:{1}", ex.Message, ex.StackTrace);

            }
            returHtmlPath.Add(pathkey, "{\"filePath\":\"approvals/" + htmlFileStoragePath + "\",\"fileFolderPath\":\"" + fileFolderPath + "\",\"Width\": \"" + 0 + "\", \"Height\": \"" + 0 + "\" },");
            return returHtmlPath;
        }

    }

}
