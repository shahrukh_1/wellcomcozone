﻿using System;
using System.IO;
using GMGColor.Common;
using GMGColorDAL;
using GMGColorDAL.Common;

namespace GMGColor.FileUpload
{
    [Serializable]
    public class FileUploadModel
    {
        #region Properties

        /// <summary>
        /// Default : IsMultiple = false
        /// </summary>
        public bool IsMultiple { get; set; }
        /// <summary>
        /// Default : IsRequired = false
        /// </summary>
        public bool IsRequired { get; set; }
        /// <summary>
        /// Default : ShowDeleteButton = true;
        /// </summary>
        public bool ShowDeleteButton { get; set; }
        /// <summary>
        /// Defaulst : UseDropZone = false;
        /// </summary>
        public bool UseDropZone { get; set; }
        /// <summary>
        /// Default : Key = Random (int)
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// Default : CssClass = "icon-white"
        /// </summary>
        public string CssClass { get; set; }
        /// <summary>
        /// Default : DefaultImagePath = string.Empty
        /// </summary>
        public string DefaultImagePath { get; set; }
        /// <summary>
        /// Default : DispalyName = string.Empty
        /// </summary>
        public string DispalyName { get; set; }
        /// <summary>
        /// Default : HelpInlineHtml = string.Empty
        /// </summary>
        public string HelpBlockHtml { get; set; }
        /// <summary>
        /// Default : HelpInlineHtml = string.Empty
        /// </summary>
        public string HelpInlineHtml { get; set; }
        /// <summary>
        /// Default : PathToTempFolder = PathToProjectFolder + \Temp\
        /// </summary>
        public string PathToTempFolder { get; set; }
        /// <summary>
        /// Default : UploadButtonCaption = "Browse"
        /// </summary>
        public string UploadButtonCaption { get; set; }
        /// <summary>
        /// Default : MaxFileSize = "5MB"
        /// Set value in KB
        /// </summary>
        public long MaxFileSize { get; set; }
        /// <summary>
        /// Maximum number of files allowed
        /// </summary>
        public int MaxNumberOfFiles { get; set; }

        public string AccountRegion { get; set; }

        public int ApprovalFolderId { get; set; }

        public bool IsNewVersionRequest { get; set; }
        public bool IsDisableTagwordsInDashbord { get; set; }


        public int Uploader { get; set; }
        public bool IsImportXls { get; set; }
        public bool UseLabelForFileName { get; set; }

        private string _acceptFileTypes;
        public string AcceptFileTypes
        {
            get { return _acceptFileTypes ?? string.Empty ; /* "/(jpe?g|png|gif)$/i" */ }
            set { _acceptFileTypes = value; }
        }

        #endregion

        #region Contrustors & Methods

        /*public FileUploadModel()
        {

        }*/

        /// <summary>
        /// IsMultiple = false
        /// IsRequired = false
        /// ShowDeleteButton = true
        /// UseDropZone = false
        /// Key = Random (int)
        /// CssClass = "icon-white"
        /// DefaultImagePath = string.Empty
        /// DispalyName = string.Empty
        /// HelpInlineHtml = string.Empty
        /// HelpInlineHtml = string.Empty
        /// PathToTempFolder = PathToProjectFolder + \\Temp\\
        /// UploadButtonCaption = "Browse"
        /// MaxFileSize = 5 * 1024
        /// UseFileNameForLable = true
        /// </summary>
        public FileUploadModel(string accountRegion)
        {
            IsMultiple = false;
            IsRequired = false;
            ShowDeleteButton = true;
            UseDropZone = false;
            IsImportXls = false;

            Key = (new Random()).Next(0, 10);

            CssClass = "icon-white";
            DefaultImagePath = string.Empty;
            DispalyName = string.Empty;
            HelpInlineHtml = string.Empty;
            HelpInlineHtml = string.Empty;
            AccountRegion = accountRegion;
            PathToTempFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? "temp/" : "\\temp\\");
            UploadButtonCaption = Resources.Resources.btnBrowse;
            MaxFileSize = GMGColorConfiguration.AppConfiguration.WhiteLableMaxFileSize;
            UseLabelForFileName = true;
            MaxNumberOfFiles = 100;
        }

        public static FileUploadModel FileUploadTemplates(string PathToTempFolder, string accountRegion)
        {
            FileUploadModel model = new FileUploadModel(accountRegion);
            model.PathToTempFolder = PathToTempFolder;
            return model;
        }

        #endregion
    }

    public class FilesStatus
    {
        #region Fields

        public const string HandlerPath = "/FileUpload/";

        #endregion

        #region Properties

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public long size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }
        public int pagesCount { get; set; }
        public string fileguid { get; set; }
        public string approvalFileStatus { get; set; }
        public string approvalFileStatusLabel { get; set; }
        public string fileHtmlDimensions { get; set; }
        public string ApprovalTypeValue { get; set; }

        public string encryptedPagesCount
        {
            get { return GMG.CoZone.Common.Utils.Encrypt(Convert.ToString(pagesCount)); }
        }

        #endregion

        #region Constructors & Methods

        public FilesStatus()
        {

        }

        public FilesStatus(FileInfo fileInfo, string accountRegion) { SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName, accountRegion); }

        public FilesStatus(string fileName, long fileLength, string fullPath, string accountRegion) { SetValues(fileName, fileLength, fullPath, accountRegion); }

        private void SetValues(string fileName, long fileLength, string relativeSourcePath, string accountRegion)
        {
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "UploadHandler.ashx?f=" + fileName;
            delete_url = HandlerPath + "UploadHandler.ashx?f=" + fileName;
            delete_type = "HEAD"; // ! Changed method type from delete to head because delete method is not allowed by webserver

            string fileExtention = Path.GetExtension(fileName).Replace(".", string.Empty);

            try
            {
                if (GMGColorCommon.IsImageFile(fileExtention))
                {
                    string relativeDestinationPath = relativeSourcePath;
                    if ((fileLength / 1024) > 3)
                    {
                        relativeDestinationPath = relativeSourcePath.Replace("." + fileExtention, "_thumb." + fileExtention);
                        GMGColorCommon.CreateThumbnail(relativeSourcePath, relativeDestinationPath, 240, 135, accountRegion);
                    }

                    thumbnail_url = GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ?
                                    GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeDestinationPath :
                                    "../../../" + GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion) + "/" + relativeDestinationPath;
                }
                else
                {


                    string fileServerPath = System.Web.HttpContext.Current == null ? System.Web.Hosting.HostingEnvironment.MapPath("/") : System.Web.HttpContext.Current.Server.MapPath("/");
                    if(fileExtention == Approval.ApprovalTypeEnum.Zip.ToString().ToLower())
                    {
                        fileExtention = "html";
                    }
                    string fileIconPath = fileServerPath + "\\Content\\img\\" + fileExtention + ".gif";
                    if (!File.Exists(fileIconPath))
                    {
                        fileIconPath = fileServerPath + "\\Content\\img\\unknown.gif";
                    }
                    thumbnail_url = @"data:image/png;base64," + GMGColorCommon.EncodeFile(fileIconPath);
                }

                //var ext = Path.GetExtension(fullPath);
                //var fileSize = ConvertBytesToMegabytes(/*new FileInfo(fullPath).Length*/fileLength);

                /*if (fileSize > 3 && IsImage(ext))
                {
                    string newFilePath = Path.GetDirectoryName(fullPath) + "\\" + Path.GetFileNameWithoutExtension(fullPath) + "_thumb" + ext;
                    FileProcessor.ResizeImageBasedOnWidthAndHeight(fullPath, newFilePath, 240, 135);
                    thumbnail_url = @"data:image/png;base64," + EncodeFile(newFilePath);
                }
                else if (fileSize > 3 || !IsImage(ext))
                {
                    thumbnail_url = "/Content/img/generalFile.png";
                }
                else
                {
                    //thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
                    thumbnail_url = "/Content/img/" + Path.GetExtension(fileName).Replace(".", string.Empty) + ".gif";
                    //thumbnail_url = GMGColorConfiguration.AppConfiguration.PathToDataFolder + fullPath;
                }*/
            }
            catch (Exception)
            {
                var fileIconPath = System.Web.HttpContext.Current == null ? System.Web.Hosting.HostingEnvironment.MapPath("/") + "\\Content\\img\\noimage-240px-135px.png" : System.Web.HttpContext.Current.Server.MapPath("/") + "\\Content\\img\\noimage-240px-135px.png";

                thumbnail_url = @"data:image/png;base64," + GMGColorCommon.EncodeFile(fileIconPath);
            }
        }

        #endregion
    }
}