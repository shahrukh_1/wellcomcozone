﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using GMGColor.Common;
using GMGColorDAL.CustomModels;

namespace GMGColor.HtmlHelpers
{
    public static class PagerExtensions
    {
        public static MvcHtmlString Pager(this HtmlHelper helper, int currentPage, int totalItemCount, object htmlAttributes, int pageSize = 10)
        {
            var container = new TagBuilder("ul");

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var actionName = helper.ViewContext.RouteData.GetRequiredString("Action");
            var routingValues = new RouteValueDictionary(htmlAttributes);

            WebGridNavigationModel model = new WebGridNavigationModel(currentPage, totalItemCount, pageSize);
            foreach (WebGridNavigationItem item in model.Items)
            {
                RouteValueDictionary routeValueDictionary = GMGColorDAL.CustomModels.Common.GetCurrentQueryStrings(HttpContext.Current.Request.QueryString, "page", item.Value);
                routeValueDictionary = new RouteValueDictionary(routeValueDictionary.Merge(routingValues));
                string linkUrl = urlHelper.Action("Populate", "Approvals", routeValueDictionary);
                string textUrl = "";

                if (item.IsFirst)
                {
                    textUrl = "«";
                }
                else if (item.IsLast)
                {
                    textUrl = "»";
                }
                //else if (item.IsNext)
                //{
                //    textUrl = ">";
                //}
                //else if (item.IsPrev)
                //{
                //    textUrl = "<";
                //}
                else
                {
                    textUrl = item.Value;
                }
                container.InnerHtml += "<li onclick=\"\" class=\"" + string.Format("{0}", currentPage.ToString() == item.Value ? "active" : "") + "\"><a url=\"" + linkUrl + "\" href=\"" + linkUrl + "\">" + textUrl + "</a></li>";
            }
            return MvcHtmlString.Create(container.ToString());
        }

        public static MvcHtmlString ProjectFolderPager(this HtmlHelper helper, int currentPage, int totalItemCount, object htmlAttributes, int pageSize = 10)
        {
            var container = new TagBuilder("ul");

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var actionName = helper.ViewContext.RouteData.GetRequiredString("Action");
            var routingValues = new RouteValueDictionary(htmlAttributes);

            WebGridNavigationModel model = new WebGridNavigationModel(currentPage, totalItemCount, pageSize);
            foreach (WebGridNavigationItem item in model.Items)
            {
                RouteValueDictionary routeValueDictionary = GMGColorDAL.CustomModels.Common.GetCurrentQueryStrings(HttpContext.Current.Request.QueryString, "page", item.Value);
                routeValueDictionary = new RouteValueDictionary(routeValueDictionary.Merge(routingValues));
                string linkUrl = urlHelper.Action("Populate", "ProjectFolders", routeValueDictionary);
                string textUrl = "";

                if (item.IsFirst)
                {
                    textUrl = "«";
                }
                else if (item.IsLast)
                {
                    textUrl = "»";
                }
                else
                {
                    textUrl = item.Value;
                }
                container.InnerHtml += "<li onclick=\"\" class=\"" + string.Format("{0}", currentPage.ToString() == item.Value ? "active" : "") + "\"><a url=\"" + linkUrl + "\" href=\"" + linkUrl + "\">" + textUrl + "</a></li>";
            }
            return MvcHtmlString.Create(container.ToString());
        }
    }
}