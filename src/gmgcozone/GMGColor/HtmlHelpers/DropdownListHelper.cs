﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Linq.Expressions;
using System.Xml.Linq;
using System.Collections;
using System.Reflection;
using System.Windows.Documents;
using GMGColorDAL.CustomModels;

namespace GMGColor.HtmlHelpers
{
    public static class DropDownListExtension
    {
        /// <summary>
        /// DropDownList extension method that allows adding addition HTML attributes on options elements
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="selectList"></param>
        /// <param name="optionLabel"></param>
        /// <returns></returns>
        public static MvcHtmlString DropDownListExFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel)
        {
            var selectDoc = XDocument.Parse(htmlHelper.DropDownListFor(expression, selectList, optionLabel).ToString());

            var options = from XElement el in selectDoc.Element("select").Descendants()
                          select el;

            List<IDropDownExtItem> srcItems = ((((MultiSelectList)(selectList))).Items as IEnumerable<IDropDownExtItem>).ToList();

            if (srcItems != null || options != null)
            {
                // the name of the model property bound to value field
                string propertyKeyName = (((MultiSelectList)(selectList))).DataValueField;
                foreach (var item in options)
                {
                    string itemValue = item.Attribute("value").Value;
                    if (String.IsNullOrEmpty(itemValue))
                    {
                        continue;
                    }
                    // identify the model item by value key
                    var modelItem = srcItems.Where(x => x.GetType().GetProperty(propertyKeyName).GetValue(x, null).ToString() == itemValue).SingleOrDefault();
                    if (modelItem != null)
                    {
                        // add the html attributes to the option element
                        foreach (var attribute in modelItem.GetHtmlAttributes())
                        {
                            item.SetAttributeValue(attribute.Key, attribute.Value);
                        }
                    }
                }
            }

            // rebuild the control, resetting the options with the ones you modified
            selectDoc.Root.ReplaceNodes(options.ToArray());
            return MvcHtmlString.Create(selectDoc.ToString());
        }
    }
}