﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace GMGColor.HtmlHelpers
{
    public static class ValidationSummaryExtensions
    {
        /// <summary>
        /// Custom Validation Summary Helper 
        /// </summary>
        /// <param name="ErrorHeading">Error Heading Message</param>
        /// <param name="headingClass">Error Heading Css Class </param>
        /// <param name="outerCssClass">Outer Css Class</param>
        /// <returns></returns>
        public static MvcHtmlString ValidationSummaryHelper(string ErrorHeading, string headingClass, string outerCssClass)
        {
            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.AddCssClass("customValidationSummary " + outerCssClass);
            outerDiv.Attributes.Add("style", "display:none");

            TagBuilder closeButton = new TagBuilder("a");
            closeButton.AddCssClass("close");
            closeButton.Attributes.Add("data-dismiss", "alert");
            closeButton.Attributes.Add("href", "#");
            closeButton.SetInnerText("x");

            TagBuilder innerHeading = new TagBuilder("h4");
            innerHeading.AddCssClass(headingClass);
            innerHeading.SetInnerText(ErrorHeading);

            outerDiv.InnerHtml = closeButton.ToMvcHtmlString(TagRenderMode.Normal).ToString();
            outerDiv.InnerHtml += innerHeading.ToMvcHtmlString(TagRenderMode.Normal).ToString();

            return outerDiv.ToMvcHtmlString(TagRenderMode.Normal);
        }

        /// <summary>
        /// Custom Validation Summary Helper
        /// </summary>
        /// <param name="ErrorHeading">Error Heading Message</param>
        /// <param name="headingClass">Error Heading Css Class</param>
        /// <param name="errorMessage">Error Message</param>
        /// <param name="errorMessageClass">Error Message Css Class</param>
        /// <param name="outerCssClass">Outer Css Class</param>
        /// <returns></returns>
        public static MvcHtmlString ValidationSummaryHelper(string ErrorHeading, string headingClass, List<string> errorMessages, string errorMessageClass, string outerCssClass)
        {
            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.AddCssClass("customValidationSummary " + outerCssClass);
            outerDiv.Attributes.Add("style", "display:none");

            TagBuilder closeButton = new TagBuilder("a");
            closeButton.AddCssClass("close");
            closeButton.Attributes.Add("data-dismiss", "alert");
            closeButton.Attributes.Add("href", "#");
            closeButton.SetInnerText("x");

            TagBuilder innerHeading = new TagBuilder("h4");
            innerHeading.AddCssClass(headingClass);
            innerHeading.SetInnerText(ErrorHeading);

            outerDiv.InnerHtml = closeButton.ToMvcHtmlString(TagRenderMode.Normal).ToString();
            outerDiv.InnerHtml += innerHeading.ToMvcHtmlString(TagRenderMode.Normal).ToString();

            foreach (string msg in errorMessages)
            {
                TagBuilder innerError = new TagBuilder("div");
                innerError.AddCssClass(errorMessageClass);
                innerError.SetInnerText(msg);

                outerDiv.InnerHtml += innerError.ToMvcHtmlString(TagRenderMode.Normal).ToString();
            }

            return outerDiv.ToMvcHtmlString(TagRenderMode.Normal);
        }

        private static MvcHtmlString ToMvcHtmlString(this TagBuilder tagBuilder, TagRenderMode renderMode)
        {
            return new MvcHtmlString(tagBuilder.ToString(renderMode));
        }
    }
}