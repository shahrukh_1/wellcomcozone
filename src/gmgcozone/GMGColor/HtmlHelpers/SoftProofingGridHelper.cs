﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using GMGColorDAL.CustomModels;
using GMGColor.Resources;
using System.Text;
using System.Configuration;

namespace GMGColor.HtmlHelpers
{
    public static class SoftProofingGridHelper
    {
        public static string GetActionButtons(string profileName, int id, bool isSysAdmin, bool showEdit, string type)
        {
            if (!isSysAdmin || profileName + ".icc" != ConfigurationManager.AppSettings["InputProfile"])
            {
                System.Text.StringBuilder html = new System.Text.StringBuilder();

                html.Append("<div class=\"btn-group pull-right\" data-profile=\"" + id + "\">");
                html.Append("<a href=\"javascript:void(0);\" data-toggle=\"dropdown\" class=\"btn btn-mini dropdown-toggle ellipsisButtonList\"><i class=\"icon-ellipsis-horizontal\"></i></a>");
                html.Append("<ul class=\"dropdown-menu pull-right\">");
                if (showEdit || isSysAdmin)
                {
                    html.AppendFormat(
                        string.Format("<li><a href=\"/SoftProofing/" + type + "?id=" + id + "&" + "isSysAdmin=" + isSysAdmin +
                                      "\"><i class=\"icon-pencil\"></i>{{0}}</a></li>"), Resources.Resources.btnEdit);
                }
                if (type == "AddEditSimulationProfile")
                {
                    if (showEdit)
                    {
                        html.Append("  <li><a href=\"javascript:void(0);\" onclick=\"$('#hdnSimulationProfileId')[0].value =" + id + ";$('#modelDialogDeleteConfirmation_1').modal({ backdrop: 'static', keyboard: false });\" rel=\"nofollow\" data-method=\"delete\" data-disable-with=\"Deleting...\" ><i class=\"icon-trash\"></i>" + Resources.Resources.btnDelete + "</a></li>");
                    }
                }
                else
                {
                    html.Append("  <li><a href=\"javascript:void(0);\" onclick=\"$('#hdnPaperTintId')[0].value =" + id + ";$('#modelDialogDeleteConfirmation_1').modal({ backdrop: 'static', keyboard: false });\" rel=\"nofollow\" data-method=\"delete\" data-disable-with=\"Deleting...\" ><i class=\"icon-trash\"></i>" + Resources.Resources.btnDelete + "</a></li>");
                }
                html.Append("</ul>");
                html.Append("</div>");

                return html.ToString();
            }
            else 
            {
                return string.Empty;
            }
        }

        public static string GetMeasurementConditionsName(List<DashboardMeasurementConditions> mConditions)
        {
            StringBuilder html = new System.Text.StringBuilder();
            html.Append("<div>");
            if (mConditions != null)
            {
                foreach (var mCondition in mConditions)
                {
                    html.Append("<p style=\"margin-bottom:0px;\">" + mCondition.Name + "</p>");
                }
            }
            html.Append("</div>");
            return html.ToString();
        }

        public static string GetMeasurementConditionsLab(List<DashboardMeasurementConditions> mConditionValues, string valueType)
        {
            StringBuilder html = new System.Text.StringBuilder();
            html.Append("<div style=\" display: inline-block; \">");
            if (mConditionValues != null)
            {
                foreach (var mCondition in mConditionValues)
                {
                    if (valueType == "L")
                    {
                        AddLabColumn(html, mCondition.L, valueType);
                    }
                    else if (valueType == "a")
                    {
                        AddLabColumn(html, mCondition.a, valueType);
                    }
                    else
                    {
                        AddLabColumn(html, mCondition.b, valueType);
                    }
                }
            }
            html.Append("</div>");
            return html.ToString();
        }

        public static string GetValidationHtml(bool? isValid)
        {
            var html = new System.Text.StringBuilder();
            string cssClass = isValid.HasValue ? isValid.Value ? "label-success" : "label-warning" : "label-default";
            string icon = isValid.HasValue ? isValid.Value ? "V" : "I" : "N/A";
            string tooltipMessage = isValid.HasValue ? isValid.Value ? Resources.Resources.lblSimulationProfileValidationSuccess : Resources.Resources.lblSimulationProfileValidationError : Resources.Resources.lblSimulationProfileValidationProgress;
            html.Append("<span rel=\"tooltip\" title=\"" + tooltipMessage + "\" class=\"badge " + cssClass + " validationIcon\">" + icon + "</span>");

            return html.ToString();
        }

        private static void AddLabColumn(StringBuilder html, double? labValue , string labType)
        {
            html.Append("<div id=\"" + labType + "\" class=\"customToolTip\">");
            html.Append("<p style=\"display: inline-block; margin-bottom:0px;\">");
            if (labValue !=null)
            {
                var valToString = Convert.ToString(labValue.Value);
                if (valToString.IndexOf(".") > 0)
                {
                    var decimalString = valToString.Substring(valToString.IndexOf("."));
                    if (decimalString.Length >= 2)
                    {
                        valToString = valToString.Substring(0, valToString.IndexOf(".") + 2);
                    }   
                }
                else
                {
                    valToString = String.Format("{0:0.#}", labValue.Value);
                }
                html.Append("<a title=" + HttpUtility.HtmlEncode(labValue) + " data-html=\"true\" rel=\"tooltip\" href=\"#\">" + HttpUtility.HtmlEncode(valToString) + "</a>");
            }
            html.Append("</p>");
            html.Append("</div>");
        }
    }
}