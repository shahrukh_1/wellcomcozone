﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GMGColor.HtmlHelpers
{
    public static class ImageExtensions
    {
        public static HtmlString Image(this HtmlHelper helper, string id, string url, string alternateText, int height, int width)
        {
            return Image(helper, id, url, alternateText, null, height, width);
        }

        public static HtmlString Image(this HtmlHelper helper, string id, string url, string alternateText, object htmlAttributes, int height, int width)
        {
            // Instantiate a UrlHelper  
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            // Create tag builder 
            var builder = new TagBuilder("img");

            // Create valid id 
            builder.GenerateId(id);

            // Add attributes 
            builder.MergeAttribute("src", ((url != null) ? urlHelper.Content(url) : string.Empty));
            builder.MergeAttribute("alt", alternateText);
            builder.MergeAttribute("height", height.ToString());
            builder.MergeAttribute("width", width.ToString());
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            // Render tag 
            var ret = new MvcHtmlString(builder.ToString(TagRenderMode.SelfClosing));
            return ret;
        }
    }
}