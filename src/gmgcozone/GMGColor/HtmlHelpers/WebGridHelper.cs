﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Web.WebPages;

namespace GMGColor.HtmlHelpers
{
    public static class WebGridHelper
    {
        public static IHtmlString WebGridHelperSelectAll(this WebGrid webGrid,
                                                               string tableStyle = null,
                                                               string headerStyle = null,
                                                               string footerStyle = null,
                                                               string rowStyle = null,
                                                               string alternatingRowStyle = null,
                                                               string selectedRowStyle = null,
                                                               string caption = null,
                                                               bool displayHeader = true,
                                                               bool fillEmptyRows = false,
                                                               string emptyRowCellValue = null,
                                                               IEnumerable<WebGridColumn> columns = null,
                                                               IEnumerable<string> exclusions = null,
                                                               WebGridPagerModes mode = WebGridPagerModes.All,
                                                               string firstText = null,
                                                               string previousText = null,
                                                               string nextText = null,
                                                               string lastText = null,
                                                               int numericLinksCount = 5,
                                                               object htmlAttributes = null,
                                                               string dataGrdPrefix = "Prefix",
                                                               string checkBoxID = "ID",
                                                               string checkBoxValue = "ID",
                                                               string checkBoxName = "Name",
                                                               string isChecked = "IsChecked",
                                                               string isDisabled = "IsDisabled")
        {
            var newColumn = webGrid.Column(header: "{}", format: item => new HelperResult(writer =>
                            {
                                writer.Write("<input id=\"" + checkBoxID + "\"  class=\"singleCheckBox_" + dataGrdPrefix + "\" name=\"" + checkBoxName
                                    + "\" value=\"" + item.Value.GetType().GetProperty(checkBoxValue).GetValue(item.Value, null).ToString()
                                    + "\" type=\"checkbox\"" + ((bool)(item.Value.GetType().GetProperty(isChecked).GetValue(item.Value, null)) ? " checked=\"checked\"" : "")
                                    + ((bool)(item.Value.GetType().GetProperty(isDisabled).GetValue(item.Value, null)) ? " disabled=\"disabled\"" : "") + " /> <input type=\"hidden\" data-val=\"false\" name=\"BillingPlans\" value=\"" + item.Value.GetType().GetProperty(checkBoxValue).GetValue(item.Value, null).ToString() + "\">");
                            }));

            var newColumns = columns.ToList();
            newColumns.Insert(0, newColumn);

            var script = @"<script>
                            (function(){
                                var prefix = '" + dataGrdPrefix + "';" +
                                @"$('#allCheckBox_' + prefix).live('click',function () {
                                    var isChecked = $(this).attr('checked');
                                    if(isChecked){
                                            $('.singleCheckBox_' + prefix + ':not(:disabled)').attr('checked', true); 
                                    }
                                    else{
                                        $('.singleCheckBox_' + prefix + ':not(:disabled)').removeAttr('checked');
                                    }
                                    //$('.singleCheckBox_' + prefix).attr('checked', isChecked  ? true: false);
                                    $('.singleCheckBox_' + prefix).closest('tr').addClass(isChecked  ? 'selected-row': 'not-selected-row');
                                    $('.singleCheckBox_' + prefix).closest('tr').removeClass(isChecked  ? 'not-selected-row': 'selected-row');
                                });

                                $('.singleCheckBox_'+ prefix).live('click',function () {
                                    var isChecked = $(this).attr('checked');
                                    $(this).closest('tr').addClass(isChecked  ? 'selected-row': 'not-selected-row');
                                    $(this).closest('tr').removeClass(isChecked  ? 'not-selected-row': 'selected-row');
                                    if(isChecked && $('.singleCheckBox_' + prefix + ':checked').length == $(this).closest('tr').siblings().andSelf().length)
                                            $('#allCheckBox_'+ prefix).attr('checked',true);
                                    else
                                        $('#allCheckBox_'+ prefix).attr('checked',false);
                                });
                            })();
                        </script>";

            var html = webGrid.GetHtml(tableStyle, headerStyle, footerStyle, rowStyle,
                                    alternatingRowStyle, selectedRowStyle, caption,
                                    displayHeader, fillEmptyRows, emptyRowCellValue,
                                    newColumns, exclusions, mode, firstText,
                                    previousText, nextText, lastText,
                                    numericLinksCount, htmlAttributes);

            return MvcHtmlString.Create(html.ToString().Replace("{}", "<input type='checkbox' id='allCheckBox_" + dataGrdPrefix + "'/>") + script);
        }
    }
}