﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GMGColor.HtmlHelpers
{
    public class ConfirmationPopup : IHtmlString
    {
        private string _title,
            _heading,
            _message,
            _pleaseNoteMessage,
            _secondMessage,
            _url,
            _secondAction,
            _loadingText,
            _id;

        public ConfirmationPopup(string id)
        {
            this._id = id;
        }

        public ConfirmationPopup Title(string title)
        {
            this._title = title;
            return this;
        }

        public ConfirmationPopup Heading(string heading)
        {
            this._heading = heading;
            return this;
        }

        public ConfirmationPopup Message(string message)
        {
            this._message = message;
            return this;
        }

        public ConfirmationPopup PleaseNoteMessage(string pleaseNoteMessage)
        {
            this._pleaseNoteMessage = pleaseNoteMessage;
            return this;
        }

        public ConfirmationPopup SecondMessage(string secondMessage)
        {
            this._secondMessage = secondMessage;
            return this;
        }

        public ConfirmationPopup Url(string url)
        {
            this._url = url;
            return this;
        }

        public ConfirmationPopup SecondAction(string secondAction)
        {
            this._secondAction = secondAction;
            return this;
        }

        public ConfirmationPopup LodingText(string loadingText)
        {
            this._loadingText = loadingText;
            return this;
        }

        public string ToHtmlString()
        {
            StringBuilder tagBuilder =  new StringBuilder();
            tagBuilder.AppendFormat("<div class='modal hide' id='modelDialogConfirmation_{0}'>" +
                                    "<div class='modal-header'>" +
                                    "<h3 class='widget-title'>{1}</h3>" +
                                    "</div>" +
                                    "<div class='globaldelete-base modal-body'><p><strong>{2}</strong></p><p>{3}<em>{4}</em></p>", _id, _title, _heading, _message, _pleaseNoteMessage);

            if (!string.IsNullOrEmpty(_secondMessage))
            {
                tagBuilder.AppendFormat("<p>{0}</p></div>", _secondMessage);
            }

            var aa = "</div><div class=\"modal-footer\"><button class=\"btn\" data-dismiss=\"modal\" type=\"submit\">"+ Resources.Resources.btnNo +"</button>" +
                                      "<button class=\"btn btn-primary delete-yes\" onclick=\"executeAction('"+ _url +"');event.preventDefault();\" data-loading-text=\""+_loadingText +"\">"+ (!string.IsNullOrEmpty(_secondAction) ? _secondAction : Resources.Resources.btnYes) +"</button>";
            tagBuilder.AppendFormat(aa);
            
            if (!string.IsNullOrEmpty(_secondAction))
            {
                tagBuilder.AppendFormat(
                    "<button class='btn btn-primary delete-yes' type='submit' name='action' value='{0}' data-loading-text='{1}'>{2}</button>", _url, _loadingText, Resources.Resources.btnDelete);
            }
            tagBuilder.Append("</div></div>");

            return new HtmlString(tagBuilder.ToString()).ToHtmlString();
        }
    }

    public static class HtmlFluentExtensions
    {
        public static ConfirmationPopup ConfirmationPopup(this HtmlHelper helper, string id)
        {
            return new ConfirmationPopup(id);
        }
    }
}