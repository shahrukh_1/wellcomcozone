﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Threading;
using GMGColor.Common;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.ServiceModel.Activation;
using GMGColor.ActionFilters;
using GMGColor.FileUpload;
using log4net.Config;
using WURFL;
using WURFL.Config;
using GMGColor.AWS;
using System.Collections.Generic;

namespace GMGColor
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		private static readonly Jobs jobs = new Jobs();

		#region Properties

		public static IWURFLManager WurflManager { get; private set; }

        #endregion

        protected void Application_Start()
		{
            //Allow X-Frame-Options to diffrent origin 
            //System.Web.Helpers.AntiForgeryConfig.SuppressXFrameOptionsHeader = true;

            // Cleanup renders and keep Razor engine only (improves overall render views performance)
            ViewEngines.Engines.Clear();
			IViewEngine razorEngine = new RazorViewEngine() { FileExtensions = new string[] { "cshtml" } };
			ViewEngines.Engines.Add(razorEngine);

			AreaRegistration.RegisterAllAreas();

			MvcHandler.DisableMvcResponseHeader = true;

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);

			//#if DEBUG
			//System.Diagnostics.Debugger.Break();
			//#endif


			string wurflDataPath = Server.MapPath(@"~/App_Data/wurfl-latest.zip");
			var configurer = new InMemoryConfigurer().MainFile(wurflDataPath);
			WurflManager = WURFLManagerBuilder.Build(configurer);
			XmlConfigurator.Configure();

			// initi AWS settings
			if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
			{
				AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion, UseV4Signature = true });
			}

			jobs.Start();
		}

        //Remove XFrame options from request header
        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("X-Frame-Options");
        }

        protected void Application_End()
		{
		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			if (Request.Url.Host.StartsWith("www.", StringComparison.InvariantCultureIgnoreCase))
			{
				Response.Clear();
				Response.AddHeader("Location",
					String.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Host.Substring(4), Request.Url.PathAndQuery)
					);
				Response.StatusCode = (int)HttpStatusCode.MovedPermanently;
				Response.End();
			}

			// Get current path
			string currentPath = Request.Path.ToLower();
			// Check if URL  needs rewriting, other URLs will be ignored

			var numberOfWords = currentPath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Length;

			if ((currentPath.Length > 1) && currentPath.EndsWith("/") && numberOfWords == 1)
			{
				// Rewrite URL to use query strings
				HttpContext context = HttpContext.Current;
				context.RewritePath(currentPath + "/Index");
			}
			else if (currentPath.ToLower().Contains("elmah"))
			{
				// Rewrite URL to use query strings
				HttpContext context = HttpContext.Current;
				context.RewritePath("/Error/InvalidRequest");
			}

			EnableCrossDomainAjaxCall();
		}

		private void EnableCrossDomainAjaxCall()
		{
			HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

			if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
			{
				HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
				HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, Range, Access-Control-Allow-Origin");
				HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
				HttpContext.Current.Response.End();
			}
		}

		protected void Application_EndRequest(object sender, EventArgs e)
		{
			DbContextBL DbContext = HttpContext.Current.Items[Utils.DatabaseContext] as DbContextBL;
			if (DbContext != null)
			{
				DbContext.Dispose();
				HttpContext.Current.Items.Remove(Utils.DatabaseContext);
				DbContext = null;
			}
		}

		protected void Application_Error(object sender, EventArgs e)
		{
			Exception exception = Server.GetLastError();
			while (exception != null && exception.InnerException != null)
			{
				exception = exception.InnerException;
			}

			HttpException httpException = exception as HttpException;

			if (exception != null)
			{
				if (httpException == null || !(httpException.GetHttpCode() > 399 && httpException.GetHttpCode() < 500))
				{
					GMGColorLogging.log.Error(
						"Application_Error() Unhandled exception encountered. Message was " + exception.Message,
						exception);
				}
			}

			Response.Clear();

			if (httpException != null)
			{
				string redirectAction;
				switch (httpException.GetHttpCode())
				{
					case 404: // page not found
						{
							redirectAction = "Cannotcontinued";
							break;
						}
					case 500: // server error
						{
							redirectAction = "UnexpectedError";
							break;
						}
					default:
						{
							redirectAction = "Error";
							break;
						}
				}
				// Clear error on server
				Server.ClearError();

				if (httpException is HttpRequestValidationException)
				{
					var context = new HttpContextWrapper(Context);
					if (context.Request.IsAjaxRequest())
					{
						Response.Write(Utils.RequestValidationException);
					}
					else
					{
						Response.Redirect("/Error/RequestValidation");
					}
				}
				else
				{
					Response.Redirect("/Error/" + redirectAction);
				}
			}
		}

		protected void Session_Start(object sender, EventArgs e)
		{
			string str = string.Empty;
		}

		protected void Session_End(object sender, EventArgs e)
		{
			string str = string.Empty;
		}

		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
            FilterProviders.Providers.Add(new AntiForgeryTokenFilter());
#if (DEBUG)
            filters.Add(new TimeElapsedFilterAttribute());
#endif
        }

        public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.IgnoreRoute("{*fileUploader}", new { weborb = @".*UploadHandler\.ashx(/.*)?" });
			routes.IgnoreRoute("{*indevelopment}", new { indevelopment = @".*indevelopment\.html(/.*)?" });
			routes.IgnoreRoute("{*diagnostics}", new { diagnostics = @".*diagnostics\.aspx(/.*)?" });

			routes.MapRoute(
				"Login", // Route name
				"{controller}/{action}", // URL with parameters
				new { controller = "Auth", action = "Login" }, // Parameter defaults
				constraints: new { subdomain = new ExcludeSubdomainRouteConstraint(new string[] { "api" }) }
			);

			routes.MapRoute(
			   "SamlLogIn", // Route name
			   "{controller}/{action}", // URL with parameters
			   new { controller = "Saml", action = "SamlResponse" }, // Parameter defaults
			   constraints: new { subdomain = new ExcludeSubdomainRouteConstraint(new string[] { "api" }) }
			   );

			// Route for API version 1
			routes.MapRoute(
				"Api", // Route name
				"v1/{action}/{Id}", // URL with parameters
				new { controller = "Api", action = "Session", Id = UrlParameter.Optional }
			);
		}

		public override string GetVaryByCustomString(HttpContext context, string custom)
		{
			switch (custom.ToLower().Trim())
			{
				case "culture":
					{
						// culture name (e.g. "en-US") is what should vary caching
						return Thread.CurrentThread.CurrentCulture.Name;
					}
				/*case "navigation":
                    {
                        if (!String.IsNullOrEmpty(context.Request.CurrentExecutionFilePath) && (context.Session["gmg_uid"] != null))
                        {
                            return custom + context.Request.CurrentExecutionFilePath + context.Session["gmg_uid"].ToString() + context.Session.SessionID;
                        }
                    }*/
				default:
					{
						return base.GetVaryByCustomString(context, custom);
					}
			}
		}
    }

    public class AntiForgeryTokenFilter : IFilterProvider
    {
        public IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            List<Filter> result = new List<Filter>();

            string incomingVerb = controllerContext.HttpContext.Request.HttpMethod;

            if (String.Equals(incomingVerb, "POST", StringComparison.OrdinalIgnoreCase) && !controllerContext.HttpContext.Request.FilePath.Contains("/v1/") 
                && !controllerContext.HttpContext.Request.FilePath.Contains("/AppService/") && !controllerContext.HttpContext.Request.FilePath.Contains("/PingSession") 
                && controllerContext.HttpContext.Request.FilePath.ToString() != "/Sso/IdpResponse/" && !controllerContext.HttpContext.Request.FilePath.Contains("/ForgotPassword")
                && !controllerContext.HttpContext.Request.FilePath.Contains("/PageGroupedAnnotations"))
            {
                result.Add(new Filter(new ValidateAntiForgeryTokenAttribute(), FilterScope.Global, null));
            }
            return result;
        }
    }
}