using GMG.CoZone.Repositories;
using GMG.CoZone.Repositories.Interfaces;

namespace GMGColor
{
    using System;
    using System.Threading.Tasks;
    using Auth0.AuthenticationApi;
    using Auth0.AuthenticationApi.Models;
    using Auth0.AspNet;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IdentityModel.Services;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using GMG.CoZone.SSO.Interfaces.Auth0Interfaces;
    using Microsoft.Practices.Unity;
    using GMG.CoZone.SSO.Services.Auth0_Plugin;
    using GMGColorDAL;

    public class LoginCallback : HttpTaskAsyncHandler
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var container = new UnityContainer();
            container.RegisterType<IAuth0Plugin, Auth0Plugin>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();

            var ssoAuth0Settings = container.Resolve<Auth0Plugin>().GetSettings(context.Request.Url.Host);

            if (ssoAuth0Settings == null)
            {
                context.Response.Redirect("/Error/UnexpectedError");
                return;
            }
            AuthenticationApiClient client = new AuthenticationApiClient(
                new Uri(string.Format("https://{0}", ssoAuth0Settings.Domain)));

            var token = await client.ExchangeCodeForAccessTokenAsync(new ExchangeCodeRequest
            {
                ClientId = ssoAuth0Settings.ClientId,
                ClientSecret = ssoAuth0Settings.ClientSecret,
                AuthorizationCode = context.Request.QueryString["code"],
                RedirectUri = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + context.Request.Url.ToString().Substring(context.Request.Url.ToString().IndexOf("://") + 3)
            });

            var profile = await client.GetUserInfoAsync(token.AccessToken);

            var user = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("name", profile.UserName ?? profile.Email),
                new KeyValuePair<string, object>("email", profile.Email),
                new KeyValuePair<string, object>("family_name", profile.LastName),
                new KeyValuePair<string, object>("given_name", profile.FirstName),
                new KeyValuePair<string, object>("nickname", profile.NickName),
                new KeyValuePair<string, object>("picture", profile.Picture),
                new KeyValuePair<string, object>("user_id", profile.UserId),
                new KeyValuePair<string, object>("id_token", token.IdToken),
                new KeyValuePair<string, object>("access_token", token.AccessToken),
                new KeyValuePair<string, object>("refresh_token", token.RefreshToken),
                new KeyValuePair<string, object>("connection", profile.Identities.First().Connection),
                new KeyValuePair<string, object>("provider", profile.Identities.First().Provider)
            };

            // NOTE: Uncomment the following code in order to include claims from associated identities
            //profile.Identities.ToList().ForEach(i =>
            //{
            //    user.Add(new KeyValuePair<string, object>(i.Connection + ".access_token", i.AccessToken));
            //    user.Add(new KeyValuePair<string, object>(i.Connection + ".provider", i.Provider));
            //    user.Add(new KeyValuePair<string, object>(i.Connection + ".user_id", i.UserId));
            //});

            // NOTE: uncomment this if you send roles
            // user.Add(new KeyValuePair<string, object>(ClaimTypes.Role, profile.ExtraProperties["roles"]));

            // NOTE: this will set a cookie with all the user claims that will be converted 
            //       to a ClaimsPrincipal for each request using the SessionAuthenticationModule HttpModule. 
            //       You can choose your own mechanism to keep the user authenticated (FormsAuthentication, Session, etc.)
            FederatedAuthentication.SessionAuthenticationModule.CreateSessionCookie(user);

            if (context.Request.QueryString["state"] != null && context.Request.QueryString["state"].StartsWith("ru="))
            {
                var state = HttpUtility.ParseQueryString(context.Request.QueryString["state"]);
                context.Response.Redirect(state["ru"], true);
            }

            context.Response.Redirect("/Auth/AuthLogin");
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}