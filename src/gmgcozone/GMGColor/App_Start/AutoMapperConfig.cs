﻿using AutoMapper;
using Microsoft.Practices.Unity;

namespace GMGColor.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure(IUnityContainer container)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfiles(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            });

            //create and register mapper 
            IMapper mapper = config.CreateMapper();
            container.RegisterInstance(mapper);
        }
    }
}
