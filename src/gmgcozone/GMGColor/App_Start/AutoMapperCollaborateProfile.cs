﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GMG.CoZone.Collaborate.UI;
using GMG.CoZone.Common.DomainModels;
using GMGColorDAL;

namespace GMGColor
{
    public class AutoMapperCollaborateProfile : Profile
    {
        public AutoMapperCollaborateProfile()
        {
            CreateMap<FileDownloadViewModel, FileDownloadModel>().ForMember(x => x.FileIds, y => y.MapFrom(z => z.FileIds.Split(',').Select(int.Parse).ToList()))
                                                                 .ForMember(x => x.FolderIds, y => y.MapFrom(z => z.FolderIds.Split(',').Select(int.Parse).ToList()));
            CreateMap<FtpPresetJobDownloadModel, FTPPresetJobDownload>();
        }
    }
}