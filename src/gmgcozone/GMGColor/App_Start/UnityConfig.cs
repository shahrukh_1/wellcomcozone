using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using GMG.CoZone.SSO.Interfaces;
using GMG.CoZone.SSO.Services;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;
using GMG.CoZone.SSO.Services.SAML_Plugin;
using GMG.CoZone.Settings.Interfaces;
using GMG.CoZone.Settings.Services.Settings;
using GMG.CoZone.Settings.Services;
using GMG.CoZone.Settings.Interfaces.Settings;
using GMG.CoZone.SSO.Interfaces.Auth0Interfaces;
using GMG.CoZone.SSO.Services.Auth0_Plugin;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Services;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Collaborate.Services.Approval;
using GMG.CoZone.Collaborate.Services.Soad_State;
using GMG.CoZone.Common.Module;
using GMG.CoZone.Collaborate.Interfaces.Annotation;
using GMG.CoZone.Collaborate.Services;
using GMG.CoZone.Collaborate.Services.Annotation;
using GMG.CoZone.Proofstudio.Interfaces;
using GMG.CoZone.Proofstudio.Services;
using GMG.CoZone.Common.AWS.Interfaces;
using GMG.CoZone.Common.AWS;
using GMG.CoZone.Repositories.Interfaces;
using GMG.CoZone.Repositories;
using GMG.CoZone.Deliver.Services;
using GMG.CoZone.Repositories.Interfaces.Common;
using GMG.CoZone.FileTransfer.Interfaces;
using GMG.CoZone.FileTransfer.Services;

namespace GMGColor.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            container.RegisterType<ISoftProofingService, SoftProofingService>();
            container.RegisterType<IColorHelper, ColorHelper>();
            container.RegisterType<ISsoSettings, SsoSettings>();
            container.RegisterType<ISmtpSettings, SmtpSettings>();
            container.RegisterType<ISsoCore, SsoCoreService>();
            container.RegisterType<ISamlPlugin, SamlPlugin>();
            container.RegisterType<ISamlAuthRequest, SamlAuthRequest>();
            container.RegisterType<ISamlConfiguration, SamlConfiguration>();
            container.RegisterType<ISamlResponseParser, SamlResponseParser>();
            container.RegisterType<ISamlCertificate, SamlCertificate>();
            container.RegisterType<ISettingsService, SettingsService>();
            container.RegisterType<IAuth0Plugin, Auth0Plugin>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<ICommonService, CommonService>();
            container.RegisterType<IApprovalService, ApprovalService>();            
            container.RegisterType<IApprovalUserViewInfoService, ApprovalUserViewInfoService>();
            container.RegisterType<ISoadStateService, SoadStateService>();
            container.RegisterType<IGMGConfiguration, GMGConfiguration>(new HierarchicalLifetimeManager());
            container.RegisterType<IQueueMessageManager, QueueMessageManager>();
            container.RegisterType<ISmtpQueueParser, SmtpQueueReader>();
            container.RegisterType<IAnnotationsInReportService, AnnotationsInReportService>();
            container.RegisterType<IGetProofstudioApproval, GetProofstudioApproval>();
            container.RegisterType<IProofstudioApiService, ProofstudioApiService>();
            container.RegisterType<IAWSSimpleNotificationService, AWSSimpleNotificationService>();
            container.RegisterType<IAWSSNSService, AWSSNSService>();

            container.RegisterType<IDownloadService, DownloadService>();
            container.RegisterType<IDeliverDownloadService, DeliverDownloadService>();
            container.RegisterType<ICollaborateDownloadService, CollaborateDownloadService>();
            container.RegisterType<IFileTransferDownloadService, FileTransferDownloadService>();

            container.RegisterType<IFtpService, FtpService>();
            container.RegisterType<IFileTransferService, FileTransferService>();

            AutoMapperConfig.Configure(container);

            // container.RegisterType<IProductRepository, ProductRepository>();
        }
    }
}
