﻿using AutoMapper;
using GMG.CoZone.Settings.DomainModels;
using GMG.CoZone.Settings.UI.ViewModels;
using GMG.CoZone.SSO.DomainModels;

namespace GMGColor.App_Start
{
    public class AutoMapperSettingsProfile : Profile
    {
        public AutoMapperSettingsProfile()
        {
            CreateMap<SsoModel, SsoViewModel>();
            CreateMap<SsoViewModel, SsoModel>();
            CreateMap<SsoAuth0SettingsViewModel, SsoAuth0SettingsModel>();
            CreateMap<SsoAuth0SettingsModel, SsoAuth0SettingsViewModel>();
            CreateMap<SsoSamlSettingsViewModel, SsoSamlSettingsModel>();
            CreateMap<SsoSamlSettingsModel, SsoSamlSettingsViewModel>();
            CreateMap<SmtpSettingsModel, SmtpSettingsViewModel>();
            CreateMap<SmtpSettingsViewModel, SmtpSettingsModel>();
        }
    }
}