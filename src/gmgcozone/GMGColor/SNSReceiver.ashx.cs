﻿using GMGColor.App_Start;
using System.Web;
using Microsoft.Practices.Unity;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Services;
using Microsoft.AspNet.SignalR;
using GMGColor.Common;
using System;
using GMGColorDAL;
using GMG.CoZone.Common.DomainModels;
using System.IO;
using Newtonsoft.Json;

namespace GMGColor
{
    /// <summary>
    /// Summary description for SNSReceiver
    /// </summary>
    public class SNSReceiver : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                var approvalStatus = GetMessageObject(context);

                GMGColorLogging.log.InfoFormat("SNSReceiver ProcessRequest approvalid {0} and status is {1}", approvalStatus.ApprovalId, approvalStatus.Progress);


                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<Hubs.InstantNotificationsHub>();
                SignalRService signalRService = new SignalRService();
                signalRService.UpdateDashboardApprovalProcessStatus(hubContext, approvalStatus);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occured while getting progress request {0} Stacktrace {1} Innerexecption", ex.Message, ex.StackTrace, ex.InnerException);
            }
        }

        private MediaDefineApprovalProgressStatusModel GetMessageObject(HttpContext context)
        {
            var unityContainer = UnityConfig.GetConfiguredContainer();
            var gmgConfig = unityContainer.Resolve<IGMGConfiguration>();

            if (!gmgConfig.IsEnabledS3Bucket)
            {
                using (var reader = new StreamReader(context.Request.InputStream))
                {
                    var contentBoby = reader.ReadToEnd();
                    return JsonConvert.DeserializeObject<MediaDefineApprovalProgressStatusModel>(contentBoby);
                }
            }
            else
            {
                var snsService = unityContainer.Resolve<IAWSSNSService>();
                return snsService.ProcessPost(context);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}