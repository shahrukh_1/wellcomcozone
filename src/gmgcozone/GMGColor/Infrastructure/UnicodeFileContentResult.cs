﻿using GMGColorDAL.Common;
using ICSharpCode.SharpZipLib.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GMGColor.Infrastructure
{
    public class UnicodeFileContentResult : ActionResult
    {
		private string _downloadLink;
		private string _region;

		public UnicodeFileContentResult(string downloadLink, string region)
		{
			_downloadLink = downloadLink;
			_region = region;
		}

        public override void ExecuteResult(ControllerContext context)
        {			
			context.HttpContext.Response.ContentType = GMGColorCommon.GetContentType(Path.GetFileName(_downloadLink));
			var request = context.HttpContext.Request;
			var isIE = request.Browser.Browser.ToLower() == "ie" ||
				request.Browser.Browser.ToLower() == "internetexplorer";
			var fileName = Path.GetFileName(_downloadLink);
			if (isIE)
			{
				fileName = HttpUtility.UrlEncode(fileName, UnicodeEncoding.UTF8);
			}

            var fileExtension = Path.GetExtension(_downloadLink);


            context.HttpContext.Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
			context.HttpContext.Response.BufferOutput = false;

			var outputStream = context.HttpContext.Response.OutputStream;

            if (fileExtension == ".zip")
            {
                using (Stream fileStream = GMGColorIO.GetMemoryStreamOfZipFile(_downloadLink, _region))
                {
                    StreamUtils.Copy(fileStream, outputStream, new byte[4096]);
                    fileStream.Close();
                }
            }
            else
            {
                using (Stream fileStream = GMGColorIO.GetMemoryStreamOfAFile(_downloadLink, _region))
                {
                    StreamUtils.Copy(fileStream, outputStream, new byte[4096]);
                    fileStream.Close();
                }
            }
		}
    }
}