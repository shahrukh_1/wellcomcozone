﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GMGColorDAL.Common;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace GMGColor.Infrastructure
{
    public class ZipResult : ActionResult
    {
        private IEnumerable<string> _files;
        private string _fileName;
        private string _region;

        public string FileName
        {
            get
            {
                return _fileName ?? "file";
            }
            set { _fileName = value; }
        }

        public ZipResult(params string[] files)
        {
            this._files = files;
        }

        public ZipResult(IEnumerable<string> files, string region)
        {
            this._files = files;
            _region = region;
        }

		public override void ExecuteResult(ControllerContext context)
		{
			context.HttpContext.Response.ContentType = "application/zip";
			context.HttpContext.Response.AppendHeader("content-disposition", "attachment; filename=" + FileName + ".zip");
			context.HttpContext.Response.BufferOutput = false;
				
			using (var zipStream = new ZipOutputStream(context.HttpContext.Response.OutputStream))
			{
                zipStream.UseZip64 = UseZip64.Off;
				foreach (string filePath in _files)
				{
					using (Stream fileStream = GMGColorIO.GetMemoryStreamOfAFile(filePath, _region))
					{
                        
						var fileEntry = new ZipEntry(Path.GetFileName(filePath));

						zipStream.PutNextEntry(fileEntry);

						StreamUtils.Copy(fileStream, zipStream, new byte[4096]);
						zipStream.CloseEntry();
						fileStream.Close();
					}						
				}

				zipStream.IsStreamOwner = false;
				zipStream.Flush();
				zipStream.Close();
			}				
		}
	} 
}