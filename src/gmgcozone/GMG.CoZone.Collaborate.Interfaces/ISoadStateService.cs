﻿namespace GMG.CoZone.Collaborate.Interfaces
{
    public interface ISoadStateService
    {
        bool IsApprovalOpenedByAllCollaborators(int approvalId, int? phaseId);
        bool IsApprovalOpenedByCollaborator(int approvalId, int? phaseId, int? userId, bool isExternal);
        bool AllCollaboratorsMadeAnnotations(int approvalId, int? phaseId, int[] collaboratorsRoleIds);
        bool CollaboratorMadeAnnotation(int approvalId, int? phaseId, int? userId, bool isExternal);
        bool AllCollaboratorsHaveMadeDecisions(int approvalId, int? phaseId, int collaboratorRoleId);
        bool CollaboratorHasMadeDecision(int approvalId, int? phaseId, int? userId, bool isExternal);
        void SaveApprovalSoadState(int approval, int state);
        void SaveUserSoadState(int approval, int collaborator, int state, int? phase, bool isExternal);
        void SavePhaseSoadState(int approval, int phase, int state);
        void ResetSOADState(int approval, int phase);
        int GetANRCollaboratorRoleID();
		int[] GetANRAndRVWCollaboratorRolesListID();
    }
}
