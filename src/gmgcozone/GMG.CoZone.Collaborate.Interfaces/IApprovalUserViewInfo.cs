﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.Collaborate.Interfaces
{
    public interface IApprovalUserViewInfoService
    {
        int? GetLatestVisitedApproval(int userId);
    }
}
