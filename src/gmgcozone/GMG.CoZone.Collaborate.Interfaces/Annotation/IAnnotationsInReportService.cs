﻿using System.Collections.Generic;
using GMG.CoZone.Collaborate.DomainModels.Annotation;

namespace GMG.CoZone.Collaborate.Interfaces.Annotation
{
    public interface IAnnotationsInReportService
    {
        List<IndividualAnnotationModel> GetReportAnnotationsOnGroups(int[] groupAnnotationIds, int[] annotationIdsToLoad, string datePattern, string timezone, int timeFormat);
    }
}
