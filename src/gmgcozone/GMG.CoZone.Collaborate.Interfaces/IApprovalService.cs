﻿using System.Collections.Generic;
using GMG.CoZone.Collaborate.DomainModels;
using GMG.CoZone.Common.DomainModels;

namespace GMG.CoZone.Collaborate.Interfaces
{
    public interface IApprovalService
    {
        List<ApprovalSoadModel> GetApprovalSoadDetails(List<int> approvals);
        List<CollaboratorSoadModel> GetUserSoadDetialsForSpecifiedApproval(int approval);
        List<ApprovalSoadModel> GetPhasesSoadDetialsForSpecifiedApproval(int approval);
        Dictionary<int, int> GetNotViewedAnnotations(List<int> ids, int iD);

        GMGColorDAL.Approval.ApprovalTypeEnum GetApprovalType(int approvalId);

        int? GetApprovalIdForTranscoding(string transcodingJobId);
        string GetVideoThumbnailPath(int approvalId);
        void ResetApprovalProcessingDate(int approvalId);
        ApprovalProcessingModel GetApprovalProcessingData(int approvalId);
    }
}
