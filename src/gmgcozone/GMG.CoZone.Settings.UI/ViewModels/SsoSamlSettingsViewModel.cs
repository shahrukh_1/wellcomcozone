﻿using System.ComponentModel.DataAnnotations;

namespace GMG.CoZone.Settings.UI.ViewModels
{
    public class SsoSamlSettingsViewModel
    {
        [Required]
        public string EntityId { get; set; }

        [Required]
        public string IdpIssuerUrl { get; set; }

        public string CertificateName { get; set; }

        public string Type { get; set; }
    }
}
