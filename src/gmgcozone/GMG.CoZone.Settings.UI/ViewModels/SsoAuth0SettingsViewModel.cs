﻿using System.ComponentModel.DataAnnotations;

namespace GMG.CoZone.Settings.UI.ViewModels
{
    public class SsoAuth0SettingsViewModel
    {
        [Required]
        public string Domain { get; set; }

        [Required]
        public string ClientId { get; set; }

        [Required]
        public string ClientSecret { get; set; }

        public string Type { get; set; }
    }
}
