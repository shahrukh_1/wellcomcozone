package org.openzoom.flash.events {

import flash.events.Event;

public final class MultiScaleEvent extends Event {

		public static const SOURCE_CHANGE:String = "sourceChanged";
		public static const SCALE_CHANGE:String = "scaleChanged";
		public static const ZOOM_CHANGE:String = "zoomChanged";
		public static const CONTAINER_CHANGE:String = "containerChanged";
		public static const VIEWPORT_CHANGE:String = "viewportChanged";
		public static const VIEWPORT_UPDATE:String = "viewportUpdate";
		public static const TRANSFORM_CHANGE:String = "transformChanged";
		public static const TRANSFORMER_CHANGE:String = "transformerChanged";
		public static const CONSTRAINT_CHANGE:String = "constraintChanged";
		public static const CONTROLLERS_CHANGE:String = "controllersChanged";
		public static const LAYERS_CHANGE:String = "layersChanged";
		public static const SCENE_CHANGE:String = "sceneChanged";
		public static const SCENE_UPDATE:String = "sceneUpdate";
		public static const SCENE_RESIZE:String = "sceneResize";

		public function MultiScaleEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable)

		}


		//--------------------------------------------------------------------------
		//
		//  Overriden methods: Event
		//
		//--------------------------------------------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function clone():Event
		{
			return new MultiScaleEvent(type, bubbles, cancelable)
		}
	}

}
