package org.openzoom.flash.viewport.layers {
import flash.display.DisplayObject;

import org.openzoom.flash.viewport.INormalizedViewport;
import org.openzoom.flash.viewport.IViewportLayer;

public class ViewportLayer extends DisplayObject implements IViewportLayer{

		private var _viewport:INormalizedViewport;

		public function get viewport():INormalizedViewport {
			return _viewport;
		}

		public function set viewport(value:INormalizedViewport):void {
			_viewport = value;
		}

		public function ViewportLayer()
		{
		}
	}
}
