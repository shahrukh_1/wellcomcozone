package org.openzoom.flex.components {

import flash.display.DisplayObject;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.core.UIComponent;

import org.openzoom.flash.events.MultiScaleEvent;
import org.openzoom.flash.net.INetworkQueue;
import org.openzoom.flash.scene.IMultiScaleScene;
import org.openzoom.flash.viewport.IViewportConstraint;
import org.openzoom.flash.viewport.IViewportTransformer;
import org.openzoom.flash.viewport.NormalizedViewport;

[Event(name="scaleChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="zoomChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="containerChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="viewportChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="viewportUpdate", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="transformerChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="constraintChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="controllersChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="layersChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneUpdate", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneWidthChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneHeightChanged", type="org.openzoom.flash.events.MultiScaleEvent")]

	/**
	 * @private
	 *
	 * Base class for MultiScaleImage and DeepZoomContainer.
	 */
	internal class MultiScaleImageBase extends UIComponent {


		//--------------------------------------------------------------------------
		//
		//  Class constants
		//
		//--------------------------------------------------------------------------

		protected static const DEFAULT_SCENE_DIMENSION:Number = 16384; // 2^14

		private static const DEFAULT_MEASURED_WIDTH:Number = 400;
		private static const DEFAULT_MEASURED_HEIGHT:Number = 300;

		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------

		/**
		 * Constructor.
		 */
		public function MultiScaleImageBase()
		{
		}

		//--------------------------------------------------------------------------
		//
		//  Variables
		//
		//--------------------------------------------------------------------------

		protected var _container:MultiScaleContainer

		//--------------------------------------------------------------------------
		//
		//  Properties: Scene
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  sceneWidth
		//----------------------------------

		/**
		 * @copy org.openzoom.flash.scene.IMultiScaleScene#sceneWidth
		 */
		[Bindable(event='sceneResize')]
		public function get sceneWidth():Number
		{
			return _container ? _container.sceneWidth : NaN;
		}

		//----------------------------------
		//  sceneHeight
		//----------------------------------

		/**
		 * @copy org.openzoom.flash.scene.IMultiScaleScene#sceneHeight
		 */
		[Bindable(event='sceneResize')]
		public function get sceneHeight():Number
		{
			return _container ? _container.sceneHeight : NaN;
		}

		[Bindable(event='sceneChanged')]
		public function get scene():IMultiScaleScene
		{
			return _container ? _container.scene : null;
		}

		//--------------------------------------------------------------------------
		//
		//  Properties: Viewport
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  viewport
		//----------------------------------

		[Bindable(event="viewportChanged")]
		public function get viewport():NormalizedViewport
		{
			return _container ? _container.viewport : null
		}

		//----------------------------------
		//  transformer
		//----------------------------------


		/**
		 * Viewport transformer. Transformers are used to create the transitions
		 * between transformations of the viewport.
		 *
		 * @see org.openzoom.flash.viewport.transformers.TweenerTransformer
		 * @see org.openzoom.flash.viewport.transformers.NullTransformer
		 */
		private var _transformer:IViewportTransformer;

		[Bindable(event="transformerChanged")]
		public function get transformer():IViewportTransformer
		{
			return _container ? _container.transformer : null;
		}

		public function set transformer(value:IViewportTransformer):void
		{
			if (transformer != value) {
				_transformer = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  constraint
		//----------------------------------


		/**
		 * Viewport transformer constraint. Constraints are used to control
		 * the positions and zoom levels the viewport can reach.
		 *
		 * @see org.openzoom.flash.viewport.constraints.ZoomConstraint
		 * @see org.openzoom.flash.viewport.constraints.ScaleConstraint
		 * @see org.openzoom.flash.viewport.constraints.VisibilityConstraint
		 * @see org.openzoom.flash.viewport.constraints.CompositeConstraint
		 * @see org.openzoom.flash.viewport.constraints.NullConstraint
		 */
		private var _constraint:IViewportConstraint;

		[Bindable(event="constraintChanged")]
		public function get constraint():IViewportConstraint
		{
			return _container ? _container.constraint : null;
		}

		public function set constraint(value:IViewportConstraint):void
		{
			if (constraint != value) {
				_constraint = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  controllers
		//----------------------------------


		/**
		 * Controllers of type IViewportController applied to this MultiScaleImage.
		 * For example, viewport controllers are used to navigate the MultiScaleImage
		 * by mouse or keyboard.
		 *
		 * @see org.openzoom.flash.viewport.controllers.MouseController
		 * @see org.openzoom.flash.viewport.controllers.KeyboardController
		 * @see org.openzoom.flash.viewport.controllers.ContextMenuController
		 */
		private var _controllers:Array;

		[Bindable(event="controllersChanged")]
		public function get controllers():Array
		{
			return _container ? _container.controllers : null;
		}

		public function set controllers(value:Array):void
		{
			if (controllers != value) {
				_controllers = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  layers
		//----------------------------------

		private var _layers:Array;

		[Bindable(event="layersChanged")]
		public function get layers():Array
		{
			return _container ? _container.layers : null;
		}

		public function set layers(value:Array):void
		{
			if (_layers != value) {
				_layers = value;
				invalidateProperties();
			}
		}


		//--------------------------------------------------------------------------
		//
		//  Properties: ILoaderClient
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  loader
		//----------------------------------

		/**
		 * @copy org.openzoom.flash.net.ILoaderClient#loader
		 */
		[Bindable(event='loaderChanged')]
		[Bindable(event='containerChanged')]
		public function get loader():INetworkQueue
		{
			return _container ? _container.loader : null
		}

		//--------------------------------------------------------------------------
		//
		//  Overridden methods: UIComponent
		//
		//--------------------------------------------------------------------------

		[Bindable]
		override public function get rotation():Number {
			return super.rotation;
		}

		override public function set rotation(value:Number):void {
			super.rotation = value;
		}

		override protected function createChildren():void
		{
			super.createChildren()

			if (!_container) {
				_container = new MultiScaleContainer();

				// Add all events that needs to be propagated up
				_container.addEventListener(MultiScaleEvent.SCALE_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.ZOOM_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.CONTAINER_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.VIEWPORT_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.VIEWPORT_UPDATE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.TRANSFORMER_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.CONSTRAINT_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.CONTROLLERS_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.LAYERS_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.SCENE_CHANGE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.SCENE_UPDATE, dispatchEvent);
				_container.addEventListener(MultiScaleEvent.SCENE_RESIZE, dispatchEvent);

				super.addChild(_container);

				dispatchEvent(new MultiScaleEvent(MultiScaleEvent.CONTAINER_CHANGE));
				dispatchEvent(new MultiScaleEvent(MultiScaleEvent.VIEWPORT_CHANGE));
			}
		}

		override protected function commitProperties():void
		{
			super.commitProperties();

			if (_transformer) {
				_container.transformer = _transformer;
				_transformer = null;
			}

			if (_constraint) {
				_container.constraint = _constraint;
				_constraint = null;
			}

			if (_controllers) {
				_container.controllers = _controllers;
				_controllers = null;
			}

			if (_layers) {
				_container.layers = _layers;
				_layers = null;
			}

			if (_zoom) {
				_container.zoom = _zoom;
				_zoom = NaN;
			}

			if (_scale) {
				_container.scale = _scale;
				_scale = NaN;
			}

			if (_viewportX) {
				_container.viewportX = _viewportX;
				_viewportX = NaN;
			}

			if (_viewportY) {
				_container.viewportY = _viewportY;
				_viewportY = NaN;
			}
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);

			if (_container) {
				_container.setActualSize(unscaledWidth, unscaledHeight);
			}
		}

		override protected function measure():void
		{
			measuredWidth = DEFAULT_MEASURED_WIDTH;
			measuredHeight = DEFAULT_MEASURED_HEIGHT;
		}

		//--------------------------------------------------------------------------
		//
		//  Properties: IMultiScaleContainer
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  zoom
		//----------------------------------


		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoom
		 */
		private var _zoom:Number;

		[Bindable(event='zoomChanged')]
		public function get zoom():Number
		{
			return _container ? _container.zoom : NaN;
		}

		public function set zoom(value:Number):void
		{
			if (zoom != value) {
				_zoom = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  scale
		//----------------------------------

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#scale
		 */
		private var _scale:Number;

		[Bindable(event="scaleChanged")]
		public function get scale():Number
		{
			return _container ? _container.scale : NaN;
		}

		public function set scale(value:Number):void
		{
			if (scale != value) {
				_scale = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  viewportX
		//----------------------------------

		private var _viewportX:Number;

		[Bindable(event="viewportUpdate")]
		public function get viewportX():Number
		{
			return _container ? _container.viewportX : NaN;
		}

		public function set viewportX(value:Number):void
		{
			if (viewportX != value) {
				_viewportX = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  viewportY
		//----------------------------------

		private var _viewportY:Number;

		[Bindable(event="viewportUpdate")]
		public function get viewportY():Number
		{
			return _container ? _container.viewportY : NaN;
		}

		public function set viewportY(value:Number):void
		{
			if (viewportY != value) {
				_viewportY = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  viewportWidth
		//----------------------------------


		[Bindable(event="viewportUpdate")]

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#width
		 */
		public function get viewportWidth():Number
		{
			return _container ? _container.viewportWidth : NaN;
		}

		//----------------------------------
		//  viewportHeight
		//----------------------------------

		[Bindable(event="viewportUpdate")]

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#height
		 */
		public function get viewportHeight():Number
		{
			return _container ? _container.viewportHeight : NaN;
		}

		//--------------------------------------------------------------------------
		//
		//  Methods: IMultiScaleImage
		//
		//--------------------------------------------------------------------------

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoomTo()
		 */
		public function zoomTo(zoom:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			_container.zoomTo(zoom, transformX, transformY, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoomBy()
		 */
		public function zoomBy(factor:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			_container.zoomBy(factor, transformX, transformY, immediately)
		}

		public function scaleTo(scale:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			_container.scaleTo(scale, transformX, transformY, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoomBy()
		 */
		public function scaleBy(factor:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			_container.scaleBy(factor, transformX, transformY, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#panTo()
		 */
		public function panTo(x:Number, y:Number, immediately:Boolean = false):void
		{
			_container.panTo(x, y, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#panBy()
		 */
		public function panBy(deltaX:Number, deltaY:Number, immediately:Boolean = false):void
		{
			_container.panBy(deltaX, deltaY, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#fitToBounds()
		 */
		public function fitToBounds(bounds:Rectangle, scale:Number = 1.0, immediately:Boolean = false):void
		{
			_container.fitToBounds(bounds, scale, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#showAll()
		 */
		public function showAll(immediately:Boolean = false):void
		{
			_container.showAll(immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#localToScene()
		 */
		public function localToScene(point:Point):Point
		{
			return _container.localToScene(point)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#sceneToLocal()
		 */
		public function sceneToLocal(point:Point):Point
		{
			return _container.sceneToLocal(point)
		}

		public function getViewportBounds():Rectangle
		{
			return _container.getViewportBounds();
		}

		//--------------------------------------------------------------------------
		//
		//  Overridden methods: UIComponent
		//
		//--------------------------------------------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function addChild(child:DisplayObject):DisplayObject
		{
			return _container.addChild(child)
		}

		/**
		 * @inheritDoc
		 */
		override public function removeChild(child:DisplayObject):DisplayObject
		{
			return _container.removeChild(child)
		}

		override public function getChildIndex(child:DisplayObject):int
		{
			return _container.getChildIndex(child)
		}

		override public function getChildAt(index:int):DisplayObject
		{
			return _container.getChildAt(index)
		}

		override public function getChildByName(name:String):DisplayObject
		{
			return _container.getChildByName(name)
		}

		override public function removeChildAt(index:int):DisplayObject
		{
			return _container.removeChildAt(index)
		}

		override public function addChildAt(child:DisplayObject, index:int):DisplayObject
		{
			return _container.addChildAt(child, index)
		}

		override public function get numChildren():int
		{
			return _container ? _container.numChildren : 0
		}

		// TODO: Implement rest of child management methods
	}

}
