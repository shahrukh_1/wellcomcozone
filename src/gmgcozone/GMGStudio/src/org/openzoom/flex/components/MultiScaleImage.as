////////////////////////////////////////////////////////////////////////////////
//
//  OpenZoom SDK
//
//  Version: MPL 1.1/GPL 3/LGPL 3
//
//  The contents of this file are subject to the Mozilla Public License Version
//  1.1 (the "License"); you may not use this file except in compliance with
//  the License. You may obtain a copy of the License at
//  http://www.mozilla.org/MPL/
//
//  Software distributed under the License is distributed on an "AS IS" basis,
//  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
//  for the specific language governing rights and limitations under the
//  License.
//
//  The Original Code is the OpenZoom SDK.
//
//  The Initial Developer of the Original Code is Daniel Gasienica.
//  Portions created by the Initial Developer are Copyright (c) 2007-2010
//  the Initial Developer. All Rights Reserved.
//
//  Contributor(s):
//    Daniel Gasienica <daniel@gasienica.ch>
//
//  Alternatively, the contents of this file may be used under the terms of
//  either the GNU General Public License Version 3 or later (the "GPL"), or
//  the GNU Lesser General Public License Version 3 or later (the "LGPL"),
//  in which case the provisions of the GPL or the LGPL are applicable instead
//  of those above. If you wish to allow use of your version of this file only
//  under the terms of either the GPL or the LGPL, and not to allow others to
//  use your version of this file under the terms of the MPL, indicate your
//  decision by deleting the provisions above and replace them with the notice
//  and other provisions required by the GPL or the LGPL. If you do not delete
//  the provisions above, a recipient may use your version of this file under
//  the terms of any one of the MPL, the GPL or the LGPL.
//
////////////////////////////////////////////////////////////////////////////////
package org.openzoom.flex.components {

import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;

import mx.core.UIComponent;

import org.openzoom.flash.descriptors.IImagePyramidDescriptor;
import org.openzoom.flash.descriptors.ImagePyramidDescriptorFactory;
import org.openzoom.flash.events.MultiScaleEvent;
import org.openzoom.flash.renderers.images.ImagePyramidRenderer;

/**
	 *  Dispatched when the image has successfully loaded.
	 *
	 *  @eventType flash.events.Event.COMPLETE
	 */
	[Event(name="complete", type="flash.events.Event")]

	/**
	 *  Dispatched when an IO error has occured while loading the image.
	 *
	 *  @eventType flash.events.IOErrorEvent.IO_ERROR
	 */
	[Event(name="ioError", type="flash.events.IOErrorEvent")]

	/**
	 *  Dispatched when an security error has occured while loading the image.
	 *
	 *  @eventType flash.events.SecurityErrorEvent.SECURITY_ERROR
	 */
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]


	[Event(name="sourceChanged", type="org.openzoom.flash.events.MultiScaleEvent")]

	/**
	 * Flex component for displaying a single multiscale image.
	 * Inspired by the <a href="http://msdn.microsoft.com/en-us/library/system.windows.controls.multiscaleimage(VS.95).aspx">
	 * Microsoft Silverlight Deep Zoom MultiScaleImage control.</a>
	 * This implementation has built-in support for Zoomify, Deep Zoom and OpenZoom images.
	 * Basic keyboard and mouse navigation can be added by specifying viewport controllers through the <code>controllers</code> property.
	 * The animation can be customized by adding a viewport transformer through the <code>transformer</code> property.
	 * Zoom, visibility or custom constraints can be added through the <code>constraint</code> property.
	 */
	public final class MultiScaleImage extends MultiScaleImageBase {


		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------

		/**
		 * Constructor.
		 */
		public function MultiScaleImage()
		{
			super();

			tabEnabled = false;
			tabChildren = true;

			this.addEventListener(MultiScaleEvent.SCENE_CHANGE, onSceneUpdate);
			this.addEventListener(MultiScaleEvent.SCENE_UPDATE, onSceneUpdate);
		}

		//--------------------------------------------------------------------------
		//
		//  Variables
		//
		//--------------------------------------------------------------------------

		private var _url:String;
		private var _urlLoader:URLLoader;

		private var _image:ImagePyramidRenderer;

		//--------------------------------------------------------------------------
		//
		//  Properties
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  source
		//----------------------------------

		private var _source:IImagePyramidDescriptor;
		private var _sourceOrigin:Object;
		private var _sourceChanged:Boolean;

		[Bindable(event="sourceChanged")]

		/**
		 * Source of this image. Either a URL as String or an
		 * instance of IMultiScaleImageDescriptor.
		 *
		 * @see org.openzoom.flash.descriptors.IMultiScaleImageDescriptor
		 */
		public function get source():Object
		{
			return _source;
		}

		public function set source(value:Object):void
		{
			if (value != _sourceOrigin) {
				_sourceOrigin = value;
				_sourceChanged = true;
				invalidateProperties();
			}
		}

		//--------------------------------------------------------------------------
		//
		//  Overridden methods: UIComponent
		//
		//--------------------------------------------------------------------------

		private function onSceneUpdate(e:Event):void
		{
			// Change layers to follow scene
			if (_image && _image.source && _container && _container.scene) {
				var scene:DisplayObject = _container.scene.targetCoordinateSpace;
				for each(var layer:UIComponent in layers) {
					layer.move(scene.x, scene.y);
					layer.setActualSize(_image.source.width, _image.source.height);
					layer.scaleX = scene.width/_image.source.width;
					layer.scaleY = scene.height/_image.source.height;
				}
			}
		}

		override protected function commitProperties():void
		{
			super.commitProperties();

			if (_sourceChanged) {
				_sourceChanged = false;
				if (_sourceOrigin is String) {
					if (_url == String(_sourceOrigin))
						return

					_url = String(_sourceOrigin)
					_urlLoader = new URLLoader(new URLRequest(_url))

					_urlLoader.addEventListener(Event.COMPLETE,
							urlLoader_completeHandler,
							false, 0, true)
					_urlLoader.addEventListener(IOErrorEvent.IO_ERROR,
							urlLoader_ioErrorHandler,
							false, 0, true)
					_urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR,
							urlLoader_securityErrorHandler,
							false, 0, true)
				}

				if (_source) {
					_source = null

					if (_container.numChildren > 0)
						_container.removeChildAt(0)
				}

				if (_sourceOrigin is IImagePyramidDescriptor) {
					_source = IImagePyramidDescriptor(_sourceOrigin).clone();
					dispatchEvent(new MultiScaleEvent(MultiScaleEvent.SOURCE_CHANGE));
					addImage(_source);
					invalidateDisplayList();
				}
			}
		}


		//--------------------------------------------------------------------------
		//
		//  Methods: Internal
		//
		//--------------------------------------------------------------------------

		/**
		 * @private
		 */
		private function addImage(descriptor:IImagePyramidDescriptor):void
		{
			var aspectRatio:Number = descriptor.width / descriptor.height;
			var sceneWidth:Number;
			var sceneHeight:Number;

			if (aspectRatio > 1) {
				sceneWidth = DEFAULT_SCENE_DIMENSION;
				sceneHeight = DEFAULT_SCENE_DIMENSION / aspectRatio;
			}
			else {
				sceneWidth = DEFAULT_SCENE_DIMENSION * aspectRatio;
				sceneHeight = DEFAULT_SCENE_DIMENSION;
			}

			// resize scene
			_container.sceneWidth = sceneWidth;
			_container.sceneHeight = sceneHeight;

			// create renderer
			_image = new ImagePyramidRenderer();
			_image.source = descriptor;
			//var padding:Number = sceneHeight * 0.04;
			_image.width = sceneWidth;
			//_image.y += padding/2;
			_image.height = sceneHeight/* - padding*/;

			_container.addEventListener(Event.COMPLETE, dispatchEvent);
			_container.addEventListener(ProgressEvent.PROGRESS, dispatchEvent);
			_container.addChild(_image);
			//_container.showAll(true);
			//dispatchEvent(new Event(Event.COMPLETE));
		}


		//--------------------------------------------------------------------------
		//
		//  Event handlers
		//
		//--------------------------------------------------------------------------

		/**
		 * @private
		 */
		private function urlLoader_completeHandler(event:Event):void
		{
			if (!_urlLoader || !_urlLoader.data)
				return;

			var data:XML = new XML(_urlLoader.data);
			var factory:ImagePyramidDescriptorFactory = ImagePyramidDescriptorFactory.getInstance();
			var descriptor:IImagePyramidDescriptor = factory.getDescriptor(_url, data);

			source = descriptor;
		}

		/**
		 * @private
		 */
		private function urlLoader_ioErrorHandler(event:IOErrorEvent):void
		{
			dispatchEvent(event);
		}

		/**
		 * @private
		 */
		private function urlLoader_securityErrorHandler(event:SecurityErrorEvent):void
		{
			dispatchEvent(event);
		}

		[Bindable(event='zoomChanged')]
		[Bindable(event='sourceChanged')]
		public function get zoomPercent():Number
		{
			return _container && source is IImagePyramidDescriptor ? _container.zoom / IImagePyramidDescriptor(source).numLevels : NaN;
		}

		public function set zoomPercent(value:Number):void
		{
			if (zoomPercent != value) {
				zoomPercentTo(value);
			}
		}

		public function zoomPercentTo(zoomPercent:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			if (source is IImagePyramidDescriptor) {
				_container.zoomTo(zoomPercent * IImagePyramidDescriptor(source).numLevels, transformX, transformY, immediately);
			}
		}

		public function zoomPercentBy(factor:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			zoomTo(zoomPercent * factor, transformX, transformY);
		}

		public function getBitmapData():BitmapData
		{
			var i:uint, len:uint;
			for(i = 0, len = layers.length; i<len; i++) {
				layers[i].visible = false;
			}
			var bitmap:BitmapData = new BitmapData(width, height, true, 0);
			bitmap.draw(this);
			for(i = 0, len = layers.length; i<len; i++) {
				layers[i].visible = true;
			}
			return bitmap;
		}

	}
}
