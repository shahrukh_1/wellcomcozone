package org.openzoom.flex.components {

import flash.display.DisplayObject;
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.ProgressEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.core.UIComponent;

import org.openzoom.flash.events.MultiScaleEvent;
import org.openzoom.flash.events.ViewportEvent;
import org.openzoom.flash.net.ILoaderClient;
import org.openzoom.flash.net.INetworkQueue;
import org.openzoom.flash.net.NetworkQueue;
import org.openzoom.flash.renderers.IRenderer;
import org.openzoom.flash.renderers.images.ImagePyramidRenderManager;
import org.openzoom.flash.renderers.images.ImagePyramidRenderer;
import org.openzoom.flash.scene.IMultiScaleScene;
import org.openzoom.flash.scene.IReadonlyMultiScaleScene;
import org.openzoom.flash.scene.MultiScaleScene;
import org.openzoom.flash.viewport.INormalizedViewport;
import org.openzoom.flash.viewport.IViewportConstraint;
import org.openzoom.flash.viewport.IViewportController;
import org.openzoom.flash.viewport.IViewportLayer;
import org.openzoom.flash.viewport.IViewportTransformer;
import org.openzoom.flash.viewport.NormalizedViewport;

[DefaultProperty("children")]

	[Event(name="scaleChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="zoomChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="containerChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="viewportChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="transformerChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="constraintChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="controllersChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="layersChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneUpdate", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneWidthChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	[Event(name="sceneHeightChanged", type="org.openzoom.flash.events.MultiScaleEvent")]
	/**
	 * Generic container for multiscale content.
	 */
	public final class MultiScaleContainer extends UIComponent implements ILoaderClient {


		//--------------------------------------------------------------------------
		//
		//  Class constants
		//
		//--------------------------------------------------------------------------

		private static const DEFAULT_VIEWPORT_WIDTH:Number = 800;
		private static const DEFAULT_VIEWPORT_HEIGHT:Number = 600;

		private static const DEFAULT_SCENE_WIDTH:Number = 24000;
		private static const DEFAULT_SCENE_HEIGHT:Number = 18000;
		private static const DEFAULT_SCENE_BACKGROUND_COLOR:uint = 0x333333;
		private static const DEFAULT_SCENE_BACKGROUND_ALPHA:Number = 0;

		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------

		/**
		 * Constructor.
		 */
		public function MultiScaleContainer()
		{
			super();

			tabEnabled = false;
			tabChildren = true;
		}

		//--------------------------------------------------------------------------
		//
		//  Variables
		//
		//--------------------------------------------------------------------------

		private var _mouseCatcher:Sprite;
		private var _contentMask:Shape;
		private var _renderManager:ImagePyramidRenderManager;

		//--------------------------------------------------------------------------
		//
		//  Properties
		//
		//--------------------------------------------------------------------------

//    //----------------------------------
//    //  scene
//    //----------------------------------
//
		private var _scene:MultiScaleScene;
		[Bindable(event="sceneChanged")]

		/**
		 * @inheritDoc
		 */
		public function get scene():IMultiScaleScene
		{
			return _scene;
		}

		//----------------------------------
		//  viewport
		//----------------------------------

		private var _viewport:NormalizedViewport;
		[Bindable(event="viewportChanged")]

		/**
		 * @inheritDoc
		 */
		public function get viewport():NormalizedViewport
		{
			return _viewport;
		}

		//----------------------------------
		//  constraint
		//----------------------------------


		/**
		 * @inheritDoc
		 *
		 * @see org.openzoom.flash.viewport.constraints.DefaultConstraint
		 * @see org.openzoom.flash.viewport.constraints.NullConstraint
		 */
		private var _constraint:IViewportConstraint;
		[Bindable(event="constraintChanged")]
		public function get constraint():IViewportConstraint
		{
			return viewport ? viewport.transformer.constraint : null;
		}

		public function set constraint(value:IViewportConstraint):void
		{
			if (constraint != value) {
				_constraint = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  loader
		//----------------------------------

		private var _loader:INetworkQueue;

		public function get loader():INetworkQueue
		{
			return _loader;
		}

		public function set loader(value:INetworkQueue):void
		{
			_loader = value;
		}

		//----------------------------------
		//  transformer
		//----------------------------------

		private var _transformer:IViewportTransformer;
		[Bindable(event="transformerChanged")]

		public function get transformer():IViewportTransformer
		{
			return viewport ? viewport.transformer : null;
		}

		public function set transformer(value:IViewportTransformer):void
		{
			if (transformer != value) {
				_transformer = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  controllers
		//----------------------------------

		private var _controllers:Array = [];
		private var _controllersHolder:Array = [];
		private var _controllersChanged:Boolean = false;

		[ArrayElementType("org.openzoom.flash.viewport.IViewportController")]
		[Inspectable(arrayType="org.openzoom.flash.viewport.IViewportController")]
		[Bindable(event="controllersChanged")]

		/**
		 * Controllers of type IViewportController applied to this MultiScaleImage.
		 * For example, viewport controllers are used to navigate the MultiScaleImage
		 * by mouse or keyboard.
		 *
		 * @see org.openzoom.flash.viewport.controllers.MouseController
		 * @see org.openzoom.flash.viewport.controllers.KeyboardController
		 * @see org.openzoom.flash.viewport.controllers.ContextMenuController
		 */
		public function get controllers():Array
		{
			return _controllers;
		}

		public function set controllers(value:Array):void
		{
			if (_controllers != value) {
				// remove old controllers
				for each (var oldController:IViewportController in _controllers)
					removeController(oldController);

				_controllersHolder = value;
				_controllersChanged = true;
				invalidateProperties();

				//dispatchEvent(new Event("_controllersChanged"))
			}
		}

		private var _layers:Array = [];
		private var _layersHolder:Array = [];
		private var _layersChanged:Boolean = false;

		[Bindable(event="layersChanged")]

		public function get layers():Array
		{
			return _layers;
		}

		public function set layers(value:Array):void
		{
			if (_layers != value) {
				// remove old controllers
				for each (var oldLayer:DisplayObject in _layers)
					removeLayer(oldLayer);

				_layersHolder = value;
				_layersChanged = true;
				invalidateProperties();

				dispatchEvent(new MultiScaleEvent(MultiScaleEvent.LAYERS_CHANGE));
			}
		}

		//----------------------------------
		//  children
		//----------------------------------

		private var _children:Array;
		private var _childrenChanged:Boolean = false;

		public function get children():Array
		{
			return _children;
		}

		public function set children(value:Array):void
		{
			if (_children != value) {
				_children = value;
				_childrenChanged = true;
				invalidateProperties();
			}
		}

		//--------------------------------------------------------------------------
		//
		//  Properties: Scene
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  sceneWidth
		//----------------------------------


		/**
		 * @copy org.openzoom.flash.scene.IMultiScaleScene#sceneWidth
		 */
		private var _sceneWidth:Number;

		[Bindable(event="sceneResize")]
		public function get sceneWidth():Number
		{
			return scene ? scene.sceneWidth : NaN;
		}

		public function set sceneWidth(value:Number):void
		{
			if (sceneWidth != value) {
				_sceneWidth = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  sceneHeight
		//----------------------------------

		private var _sceneHeight:Number;

		[Bindable(event="sceneResize")]

		/**
		 * @copy org.openzoom.flash.scene.IMultiScaleScene#sceneHeight
		 */
		public function get sceneHeight():Number
		{
			return scene ? scene.sceneHeight : NaN;
		}

		public function set sceneHeight(value:Number):void
		{
			if (sceneHeight != value) {
				_sceneHeight = value;
				invalidateProperties();
			}
		}

		//--------------------------------------------------------------------------
		//
		//  Overridden methods: UIComponent
		//
		//--------------------------------------------------------------------------

		/**
		 * @private
		 */
		override protected function createChildren():void
		{
			super.createChildren();

			if (!scene)
				createScene();

			if (!viewport)
				createNormalizedViewport(_scene);

			if (!_mouseCatcher)
				createMouseCatcher();

			if (!_contentMask)
				createContentMask();

			if (!loader)
				createLoader();

			if (!_renderManager)
				createRenderManager();
		}

		/**
		 * @private
		 */
		override protected function commitProperties():void
		{
			super.commitProperties()

			if (_childrenChanged) {
				// remove all existing children
				while (numChildren > 0)
					removeChildAt(0)

//            for each (var child:DisplayObject in _children)
//                _scene.addItem(child)
			}

			if (_controllersChanged) {
				for each (var controller:IViewportController in _controllersHolder)
					addController(controller);

				_controllersHolder = [];
				_controllersChanged = false;
				dispatchEvent(new MultiScaleEvent(MultiScaleEvent.CONTROLLERS_CHANGE));
			}

			if (_layersChanged) {
				for each (var layer:DisplayObject in _layersHolder)
					addLayer(layer);

				_layersHolder = [];
				_layersChanged = false;
				invalidateDisplayList();
			}

			if (_constraint) {
				viewport.transformer.constraint = _constraint;
				_constraint = null;
			}

			if (_transformer) {
				viewport.transformer = _transformer;
				_transformer = null;
			}

			if (_sceneWidth) {
				scene.sceneWidth = _sceneWidth;
				_sceneWidth = NaN;
			}

			if (_sceneHeight) {
				scene.sceneHeight = _sceneHeight;
				_sceneHeight = NaN;
			}

			if (_zoom) {
				viewport.zoom = _zoom;
				_zoom = NaN;
			}

			if (_scale) {
				viewport.scale = _scale;
				_scale = NaN;
			}

			if (_viewportX) {
				viewport.x = _viewportX;
				_viewportX = NaN;
			}

			if (_viewportY) {
				viewport.y = _viewportY;
				_viewportY = NaN;
			}
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{

			if (viewport.viewportWidth != unscaledWidth ||
					viewport.viewportHeight != unscaledHeight)
				_viewport.setSize(unscaledWidth, unscaledHeight);

			if (_mouseCatcher.width != unscaledWidth ||
					_mouseCatcher.height != unscaledHeight) {
				_mouseCatcher.width = unscaledWidth
				_mouseCatcher.height = unscaledHeight
			}

			if (_contentMask.width != unscaledWidth ||
					_contentMask.height != unscaledHeight) {
				_contentMask.width = unscaledWidth
				_contentMask.height = unscaledHeight
			}

			var vp:INormalizedViewport = _viewport;
			var targetWidth:Number = vp.viewportWidth / vp.width;
			var targetHeight:Number = vp.viewportHeight / vp.height;
			var targetX:Number = -vp.x * targetWidth;
			var targetY:Number = -vp.y * targetHeight;

			var target:DisplayObject = _scene.targetCoordinateSpace;
			if (target.x != targetX || target.y != targetY || target.width != targetWidth || target.height != targetHeight) {
				target.x = targetX;
				target.y = targetY;
				target.width = targetWidth;
				target.height = targetHeight;
				dispatchEvent(new MultiScaleEvent(MultiScaleEvent.SCENE_UPDATE));
			}
		}

		//--------------------------------------------------------------------------
		//
		//  Overridden properties: UIComponent
		//
		//--------------------------------------------------------------------------

		override public function get numChildren():int
		{
			return _scene ? _scene.numChildren : 0
		}

		//--------------------------------------------------------------------------
		//
		//  Overridden methods: UIComponent
		//
		//--------------------------------------------------------------------------

		override public function addChild(child:DisplayObject):DisplayObject
		{
			return addChildAt(child, numChildren)
		}

		override public function removeChild(child:DisplayObject):DisplayObject
		{
			return removeChildAt(getChildIndex(child))
		}

		override public function addChildAt(child:DisplayObject, index:int):DisplayObject
		{
			var renderer:IRenderer = child as IRenderer
			if (renderer) {
				renderer.viewport = _viewport
				renderer.scene = IReadonlyMultiScaleScene(_scene)

				var imagePyramidRenderer:ImagePyramidRenderer = renderer as ImagePyramidRenderer
				if (imagePyramidRenderer)
					_renderManager.addRenderer(imagePyramidRenderer)
			}

			return _scene.addChildAt(child, index)
		}

		override public function removeChildAt(index:int):DisplayObject
		{
			var child:DisplayObject = _scene.getChildAt(index)

			var renderer:IRenderer = child as IRenderer
			if (renderer) {
				var imagePyramidRenderer:ImagePyramidRenderer = renderer as ImagePyramidRenderer
				if (imagePyramidRenderer)
					_renderManager.removeRenderer(imagePyramidRenderer)

				renderer.scene = null
				renderer.viewport = null
			}

			return _scene.removeChildAt(index)
		}

		override public function getChildAt(index:int):DisplayObject
		{
			return _scene.getChildAt(index)
		}

		override public function getChildByName(name:String):DisplayObject
		{
			return _scene.getChildByName(name)
		}

		override public function getChildIndex(child:DisplayObject):int
		{
			return _scene.getChildIndex(child)
		}

		override public function setChildIndex(child:DisplayObject, index:int):void
		{
			_scene.setChildIndex(child, index)
		}

		override public function swapChildren(child1:DisplayObject, child2:DisplayObject):void
		{
			_scene.swapChildren(child1, child2)
		}

		override public function swapChildrenAt(index1:int, index2:int):void
		{
			_scene.swapChildrenAt(index1, index2)
		}

		//--------------------------------------------------------------------------
		//
		//  Methods: Children
		//
		//--------------------------------------------------------------------------

		/**
		 * @private
		 */
		private function createScene():void
		{
			_scene = new MultiScaleScene(DEFAULT_SCENE_WIDTH,
					DEFAULT_SCENE_HEIGHT,
					DEFAULT_SCENE_BACKGROUND_COLOR,
					DEFAULT_SCENE_BACKGROUND_ALPHA);

			super.addChildAt(_scene, 0);

			// TODO: Add scene specific events listeners

			dispatchEvent(new MultiScaleEvent(MultiScaleEvent.SCENE_CHANGE));
		}

		/**
		 * @private
		 */
		private function createMouseCatcher():void
		{
			_mouseCatcher = new Sprite()

			var g:Graphics = _mouseCatcher.graphics
			g.beginFill(0x000000, 0)
			g.drawRect(0, 0, 100, 100)
			g.endFill()

			_mouseCatcher.mouseEnabled = false

			super.addChildAt(_mouseCatcher, 1)
		}

		private function createLoader():void
		{
			_loader = new NetworkQueue();
			_loader.addEventListener(Event.COMPLETE, dispatchEvent);
			_loader.addEventListener(ProgressEvent.PROGRESS, dispatchEvent);
		}

		/**
		 * @private
		 */
		private function createContentMask():void
		{
			_contentMask = new Shape()

			var g:Graphics = _contentMask.graphics
			g.beginFill(0xFF0000, 0)
			g.drawRect(0, 0, 100, 100)
			g.endFill()

			super.addChildAt(_contentMask, 2)

			mask = _contentMask
		}

		private function createNormalizedViewport(scene:IReadonlyMultiScaleScene):void
		{
			_viewport = new NormalizedViewport(DEFAULT_VIEWPORT_WIDTH,
					DEFAULT_VIEWPORT_HEIGHT,
					scene);

			_viewport.addEventListener(ViewportEvent.TRANSFORM_START,
					viewport_transformStartHandler,
					false, 0, true);
			_viewport.addEventListener(ViewportEvent.TRANSFORM_UPDATE,
					viewport_transformUpdateHandler,
					false, 0, true);
			_viewport.addEventListener(ViewportEvent.TRANSFORM_END,
					viewport_transformEndHandler,
					false, 0, true);

			_viewport.addEventListener(MultiScaleEvent.ZOOM_CHANGE, dispatchEvent);
			_viewport.addEventListener(MultiScaleEvent.SCALE_CHANGE, dispatchEvent);
			_viewport.addEventListener(MultiScaleEvent.VIEWPORT_UPDATE, dispatchEvent);
			_viewport.addEventListener(MultiScaleEvent.TRANSFORMER_CHANGE, dispatchEvent);
			_viewport.addEventListener(MultiScaleEvent.TRANSFORM_CHANGE, dispatchEvent);
			_viewport.addEventListener(MultiScaleEvent.CONSTRAINT_CHANGE, dispatchEvent); //Not wired yet

			/*if(_zoom) {
			 _viewport.zoomTo(_zoom);
			 }*/

			dispatchEvent(new MultiScaleEvent(MultiScaleEvent.VIEWPORT_CHANGE));
		}

		private function createRenderManager():void
		{
			_renderManager = new ImagePyramidRenderManager(this, scene, viewport, loader)
		}

		//--------------------------------------------------------------------------
		//
		//  Event handlers: Viewport
		//
		//--------------------------------------------------------------------------

		private function viewport_transformStartHandler(event:ViewportEvent):void
		{
			invalidateDisplayList();
		}

		private function viewport_transformUpdateHandler(event:ViewportEvent):void
		{
			invalidateDisplayList();
		}

		private function viewport_transformEndHandler(event:ViewportEvent):void
		{
			invalidateDisplayList();
		}

		//--------------------------------------------------------------------------
		//
		//  Methods: Controllers
		//
		//--------------------------------------------------------------------------

		/**
		 * @private
		 */
		private function addController(controller:IViewportController):Boolean
		{
			if (_controllers.indexOf(controller) != -1)
				return false;

			_controllers.push(controller);
			controller.viewport = _viewport;
			controller.view = this;
			return true;
		}

		private function addLayer(layer:DisplayObject):Boolean
		{
			if (_layers.indexOf(layer) != -1)
				return false

			_layers.push(layer);
			super.addChild(layer);
			if (layer is IViewportLayer) {
				IViewportLayer(layer).viewport = _viewport;
			}
			return true
		}

		/**
		 * @private
		 */
		private function removeController(controller:IViewportController):Boolean
		{
			var index:int = _controllers.indexOf(controller);
			if (index == -1)
				return false

			_controllers.splice(index, 1);
			controller.viewport = null;
			controller.view = null;
			return true;
		}

		private function removeLayer(layer:DisplayObject):Boolean
		{
			var index:int = _layers.indexOf(layer);
			if (index == -1)
				return false

			_layers.splice(index, 1);
			if (layer is IViewportLayer) {
				IViewportLayer(layer).viewport = null;
			}
			super.removeChild(layer);
			return true;
		}

		//--------------------------------------------------------------------------
		//
		//  Properties: IMultiScaleContainer
		//
		//--------------------------------------------------------------------------

		//----------------------------------
		//  zoom
		//----------------------------------

		private var _zoom:Number;
		[Bindable(event="zoomChanged")]
		public function get zoom():Number
		{
			return viewport ? viewport.zoom : NaN;
		}

		public function set zoom(value:Number):void
		{
			if (zoom != value) {
				_zoom = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  scale
		//----------------------------------

		private var _scale:Number;
		[Bindable(event="scaleChanged")]
		public function get scale():Number
		{
			return viewport ? viewport.scale : NaN;
		}

		public function set scale(value:Number):void
		{
			if (scale != value) {
				_scale = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  viewportX
		//----------------------------------
		private var _viewportX:Number;
		[Bindable(event="viewportUpdate")]

		public function get viewportX():Number
		{
			return viewport ? viewport.x : NaN;
		}

		public function set viewportX(value:Number):void
		{
			if (viewportX != value) {
				_viewportX = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  viewportY
		//----------------------------------
		private var _viewportY:Number;
		[Bindable(event="viewportUpdate")]
		public function get viewportY():Number
		{
			return viewport ? viewport.y : NaN;
		}

		public function set viewportY(value:Number):void
		{
			if (viewportY != value) {
				_viewportY = value;
				invalidateProperties();
			}
		}

		//----------------------------------
		//  viewportWidth
		//----------------------------------

		[Bindable(event="viewportUpdate")]

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#width
		 */
		public function get viewportWidth():Number
		{
			return viewport ? viewport.viewportWidth : NaN;
		}


		//----------------------------------
		//  viewportHeight
		//----------------------------------

		[Bindable(event="viewportUpdate")]

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#height
		 */
		public function get viewportHeight():Number
		{
			return viewport ? viewport.viewportHeight : NaN;
		}

		//--------------------------------------------------------------------------
		//
		//  Methods: IMultiScaleImage
		//
		//--------------------------------------------------------------------------

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoomTo()
		 */
		public function zoomTo(zoom:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			viewport.zoomTo(zoom, transformX, transformY, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoomBy()
		 */
		public function zoomBy(factor:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			viewport.zoomBy(factor, transformX, transformY, immediately);
		}

		public function scaleTo(scale:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			viewport.scaleTo(scale, transformX, transformY, immediately);
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#zoomBy()
		 */
		public function scaleBy(factor:Number, transformX:Number = 0.5, transformY:Number = 0.5, immediately:Boolean = false):void
		{
			viewport.scaleBy(factor, transformX, transformY, immediately);
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#panTo()
		 */
		public function panTo(x:Number, y:Number, immediately:Boolean = false):void
		{
			viewport.panTo(x, y, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#panBy()
		 */
		public function panBy(deltaX:Number, deltaY:Number, immediately:Boolean = false):void
		{
			viewport.panBy(deltaX, deltaY, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#fitToBounds()
		 */
		public function fitToBounds(bounds:Rectangle, scale:Number = 1.0, immediately:Boolean = false):void
		{
			viewport.fitToBounds(bounds, scale, immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#showAll()
		 */
		public function showAll(immediately:Boolean = false):void
		{
			viewport.showAll(immediately)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#localToScene()
		 */
		public function localToScene(point:Point):Point
		{
			return viewport.localToScene(point)
		}

		/**
		 * @copy org.openzoom.flash.viewport.IViewport#sceneToLocal()
		 */
		public function sceneToLocal(point:Point):Point
		{
			return viewport.sceneToLocal(point)
		}

		public function getViewportBounds():Rectangle
		{
			return viewport.getBounds();
		}
	}
}
