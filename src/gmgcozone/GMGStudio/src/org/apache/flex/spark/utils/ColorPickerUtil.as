////////////////////////////////////////////////////////////////////////////////
//
//  Licensed to the Apache Software Foundation (ASF) under one or more
//  contributor license agreements.  See the NOTICE file distributed with
//  this work for additional information regarding copyright ownership.
//  The ASF licenses this file to You under the Apache License, Version 2.0
//  (the "License"); you may not use this file except in compliance with
//  the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////
package org.apache.flex.spark.utils
{
import mx.collections.ArrayList;
import mx.collections.IList;

/**
	 *  
	 */
	public class ColorPickerUtil
	{
		
		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  Constructor.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 9
		 *  @playerversion AIR 1.1
		 *  @productversion Flex 3
		 */
		public function ColorPickerUtil()
		{
			super();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		public function getColorsList():IList /* of Number */
		{
			// Dynamically generate the default websafe color palette.
			var dp:IList = new ArrayList([
				0xFFFFFF,0xE7E7E7,0xD0D0D0,0xB9B9B9,0xA2A2A2,0x8B8B8B,0x737373,0x5C5C5C,0x454545,0x2E2E2E,0x171717,0x000000,
				0x003247,0x030857,0x0e0038,0x2f003b,0x400012,0x630000,0x610a00,0x592e00,0x573a00,0x636300,0x4a5900,0x143d00,
				0x004b66,0x051780,0x1b0054,0x4a0059,0x5c0022,0x8f0000,0x851400,0x804400,0x7d5700,0x8c8c00,0x6a7d00,0x275c06,
				0x006b8f,0x0c28b0,0x31007a,0x6a0080,0x820036,0xc70000,0xba2800,0xb36500,0xab7a00,0xc4c400,0x98b000,0x3a8012,
				0x028db8,0x123bde,0x400099,0x8800a3,0xa80049,0xf50000,0xeb3b00,0xde8100,0xd99f00,0xf5f500,0xbedb00,0x4da61e,
				0x04a4de,0x1f39ff,0x5a00ba,0xa800c2,0xc90057,0xff0000,0xff5500,0xffaa00,0xffcc00,0xffff00,0xd0f500,0x59c72a,
				0x05c9ff,0x337aff,0x7100f2,0xd400fa,0xf70073,0xff4640,0xff7b29,0xffb300,0xffd000,0xfcff52,0xdef74d,0x7fde50,
				0x00d9ff,0x70a0ff,0x9900ff,0xe600ff,0xff599e,0xff817d,0xffa375,0xffc869,0xffdc69,0xfdff8a,0xe6fa84,0xa4e687,
				0x80e8ff,0xa8c5ff,0xbe78ff,0xf478ff,0xff9cc2,0xffb0ad,0xffc4a8,0xffdca3,0xffe8a3,0xfeffb5,0xeffcb3,0xc4edb2,
				0xc4f2ff,0xd4e2ff,0xe0c4ff,0xfbc2ff,0xffd1e2,0xffdbd9,0xffe2d6,0xffeed4,0xfff3d4,0xfdffdb,0xf6fcd9,0xddf0d3
			]);
			/*
			var n:Number = 0;
			
			var spacer:Number = 0x000000;
			
			var c1:Array = [ 0x000000, 0x333333, 0x666666, 0x999999,
				0xCCCCCC, 0xFFFFFF, 0xFF0000, 0x00FF00,
				0x0000FF, 0xFFFF00, 0x00FFFF, 0xFF00FF ];
			
			var ra:Array = [ "00", "00", "00", "00", "00", "00",
				"33", "33", "33", "33", "33", "33",
				"66", "66", "66", "66", "66", "66" ];
			
			var rb:Array = [ "99", "99", "99", "99", "99", "99",
				"CC", "CC", "CC", "CC", "CC", "CC",
				"FF", "FF", "FF", "FF", "FF", "FF" ];
			
			var g:Array = [ "00", "33", "66", "99", "CC", "FF",
				"00", "33", "66", "99", "CC", "FF",
				"00", "33", "66", "99", "CC", "FF" ];
			
			var b:Array = [ "00", "33", "66", "99", "CC", "FF",
				"00", "33", "66", "99", "CC", "FF" ];
			
			for (var x:int = 0; x < 12; x++)
			{
				for (var j:int = 0; j < 20; j++)
				{
					var item:Number;
					
					if (j == 0)
					{
						item = c1[x];
						
					}
					else if (j == 1)
					{
						item = spacer;
					}
					else
					{
						var r:String;
						if (x < 6)
							r = ra[j - 2];
						else
							r = rb[j - 2];
						item = Number("0x" + r + g[j - 2] + b[x]);
					}
					
					dp.addItem(item);
					n++;
				}
			}*/
			
			return dp;
		}
		
		public static function uintToHex(dec:uint):String
		{	
			var digits:String = "0123456789ABCDEF";
			var hex:String = '';				
			while (dec > 0) 
			{					
				var next:uint = dec & 0xF;
				dec >>= 4;
				hex = digits.charAt(next) + hex;					
			}				
			if (hex.length == 0) hex = '000000';				
			return "0x"+hex;
			
		}

		public static function uintToRGB(c:uint):Array
		{
			return [
				(( c >> 16 ) & 0xFF)/255,
				(( c >> 8) & 0xFF)/255,
				(( c & 0xFF ))/255
			];
		}
	}
}
