﻿package org.libspark.fxgparser.parser.filters
{
import flash.filters.BitmapFilter;

public interface IFilter
	{
		function parse( xml:XML ):void;
		function getFlashFilter():BitmapFilter;
	}
	
}
