package com.thanksmister.components.controls
{
import spark.components.ButtonBarButton;
import spark.primitives.BitmapImage;

public class IconButtonBarButton extends ButtonBarButton
	{
		[SkinPart(required = "false")]
		public var iconElement:BitmapImage;

		public function IconButtonBarButton()
		{
			super();
		}
		
		override protected function getCurrentSkinState():String 
		{
			var state:String = super.getCurrentSkinState();
			
			if(!iconElement)
				return state;
			
			if (state == "downAndSelected" || state == "overAndSelected" || state == "upAndSelected") {
				iconElement.source = data.iconActive;
			}
			else if(state == "over")
			{
				iconElement.source = data.iconOver;
			} else {
				iconElement.source = data.iconNormal;
			}
			
			return state;
		}
	
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			
			if (data !== null && instance == iconElement) {
				iconElement.source = data.iconNormal;
			}
		}
	}
}
