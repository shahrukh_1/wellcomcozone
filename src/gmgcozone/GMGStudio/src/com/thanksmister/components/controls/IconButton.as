package com.thanksmister.components.controls
{
import spark.components.Button;
import spark.primitives.BitmapImage;

public class IconButton extends Button
	{
		public function IconButton()
		{
			super();
		}
		
		//----------------------------------
		//  icon for up state
		//----------------------------------
		private var _icon:Class;
		private var _iconOver:Class;
		
		[Bindable]
		public function get icon():Class
		{
			return _icon;
		}
		
		public function set icon(val:Class):void
		{
			_icon = val;
		}
		
		//----------------------------------
		//  icon for over and down state
		//----------------------------------
		
		[Bindable]
		public function get iconOver():Class
		{
			return _iconOver;
		}

		public function set iconOver(val:Class):void
		{
			_iconOver = val;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Skin Parts
		//
		//--------------------------------------------------------------------------
		
		[SkinPart(required = "false")]
		public var iconElement:BitmapImage;
		
		
		//--------------------------------------------------------------------------
		//
		//  Overridden methods
		//
		//--------------------------------------------------------------------------
		
		override protected function getCurrentSkinState():String
		{
			var state:String = super.getCurrentSkinState();
			
			if (state == "up" || state == "disabled") {
				iconElement.source = icon;
			} else if (state == "over" || state == "down") {
				if(iconOver) iconElement.source = iconOver;
			}
			
			return state;
		}
		
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			
			if (icon !== null && instance == iconElement) {
				iconElement.source = icon;
			}
		}
	}
}
