package com.gmgcolor.components {
import spark.components.ToggleButton;

[Bindable]
	public class CustomIconToggleButton extends ToggleButton {

		private var _iconUp:Class;
		private var _iconOver:Class;
		private var _iconDown:Class;
		private var _iconDisabled:Class;
		private var _iconUpAndSelected:Class;
		private var _iconOverAndSelected:Class;
		private var _iconDownAndSelected:Class;
		private var _iconDisabledAndSelected:Class;


		public function CustomIconToggleButton()
		{
			super();
			width = 35;
			height = 30;
		}

		public override function get visible():Boolean
		{
			return super.visible;
		}

		public override function set visible(value:Boolean):void
		{
			super.visible = value;
		}

		public function get iconOver():Class
		{
			return _iconOver;
		}

		public function set iconOver(value:Class):void
		{
			_iconOver = value;
		}

		public function get iconUp():Class
		{
			return _iconUp;
		}

		public function set iconUp(value:Class):void
		{
			_iconUp = value;
		}


		public function get iconDown():Class
		{
			return _iconDown;
		}

		public function set iconDown(value:Class):void
		{
			_iconDown = value;
		}


		public function get iconDisabled():Class
		{
			return _iconDisabled;
		}

		public function set iconDisabled(value:Class):void
		{
			_iconDisabled = value;
		}

		public function get iconUpAndSelected():Class
		{
			return _iconUpAndSelected;
		}

		public function set iconUpAndSelected(value:Class):void
		{
			_iconUpAndSelected = value;
		}

		public function get iconOverAndSelected():Class
		{
			return _iconOverAndSelected;
		}

		public function set iconOverAndSelected(value:Class):void
		{
			_iconOverAndSelected = value;
		}

		public function get iconDownAndSelected():Class
		{
			return _iconDownAndSelected;
		}

		public function set iconDownAndSelected(value:Class):void
		{
			_iconDownAndSelected = value;
		}

		public function get iconDisabledAndSelected():Class
		{
			return _iconDisabledAndSelected;
		}

		public function set iconDisabledAndSelected(value:Class):void
		{
			_iconDisabledAndSelected = value;
		}


	}
}
