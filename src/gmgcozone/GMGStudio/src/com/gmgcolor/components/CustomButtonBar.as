package com.gmgcolor.components
{
import flash.events.Event;

import mx.collections.IList;
import mx.core.EventPriority;
import mx.core.IFactory;
import mx.core.mx_internal;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;
import mx.managers.IFocusManagerComponent;

import spark.components.supportClasses.ButtonBarBase;

use namespace mx_internal;  //ListBase and List share selection properties that are mx_internal
	
	[Event(name="filterChange", type="com.gmgcolor.skins.FilterIndexChangeEvent")]
	
	public class CustomButtonBar extends ButtonBarBase implements IFocusManagerComponent 
	{
		public function CustomButtonBar()
		{
			super();
			
			itemRendererFunction = defaultButtonBarItemRendererFunction;
		}
		
		
		private var _filterIndex:int;
		
		[Bindable]
		public function get filterIndex():int
		{
			return _filterIndex;
		}
		
		public function set filterIndex(value:int):void
		{
			_filterIndex = value;
		}
		
		[SkinPart(required="false", type="mx.core.IVisualElement")]
		
		/**
		 * A skin part that defines the first button.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 1.5
		 *  @productversion Flex 4
		 */
		public var collaborationsButton:IFactory;
		
		//----------------------------------
		//  lastButton
		//---------------------------------- 
		
		[SkinPart(required="false", type="mx.core.IVisualElement")]
		
		/**
		 * A skin part that defines the last button.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 1.5
		 *  @productversion Flex 4
		 */
		public var annotationsButton:IFactory;
		
		
		//--------------------------------------------------------------------------
		//
		//  Overridden Properties
		//
		//--------------------------------------------------------------------------
		
		//----------------------------------
		//  dataProvider
		//----------------------------------
		
		[Inspectable(category="Data")]
		
		/**
		 *  @private
		 */    
		override public function set dataProvider(value:IList):void
		{
			if (dataProvider)
				dataProvider.removeEventListener(CollectionEvent.COLLECTION_CHANGE, resetCollectionChangeHandler);
			
			// not really a default handler, we just want it to run after the datagroup
			if (value)
				value.addEventListener(CollectionEvent.COLLECTION_CHANGE, resetCollectionChangeHandler, false, EventPriority.DEFAULT_HANDLER, true);
			
			super.dataProvider = value;
		}
		
		/**
		 *  @private
		 */
		private function resetCollectionChangeHandler(event:Event):void
		{
			if (event is CollectionEvent)
			{
				var ce:CollectionEvent = CollectionEvent(event);
				
				if (ce.kind == CollectionEventKind.ADD || 
					ce.kind == CollectionEventKind.REMOVE)
				{
					// force reset here so first/middle/last skins
					// get reassigned
					if (dataGroup)
					{
						dataGroup.layout.useVirtualLayout = true;
						dataGroup.layout.useVirtualLayout = false;
					}
				}
			}
		}
		
		/**
		 *  @private
		 *  button bar always keeps something under the caret so don't let it
		 *  become -1
		 */
		override mx_internal function setCurrentCaretIndex(value:Number):void
		{
			if (value == -1)
				return;
			
			super.setCurrentCaretIndex(value);
		}
		
		
		private function defaultButtonBarItemRendererFunction(data:Object):IFactory
		{
			var i:int = dataProvider.getItemIndex(data);
			
			if (i == 0)
				return collaborationsButton;
			
			var n:int = dataProvider.length - 1;
			
			if (i == 1)
				return annotationsButton;
			
			return collaborationsButton;
		}
		
		
	}
}
