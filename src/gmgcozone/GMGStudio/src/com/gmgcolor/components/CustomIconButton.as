package com.gmgcolor.components
{
import spark.components.Button;

public class CustomIconButton extends Button
	{
		
		[Bindable] public var iconUp:Class;
		[Bindable] public var iconOver:Class;
		[Bindable] public var iconDown:Class;
		[Bindable] public var iconDisabled:Class;
		
		public function CustomIconButton()
		{
			super();
			height = 30;
			width = 35;
			minWidth = 0;
			
		}
		
		
	}
}
