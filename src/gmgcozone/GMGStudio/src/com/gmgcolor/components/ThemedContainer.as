package com.gmgcolor.components
{
import spark.components.SkinnableContainer;

[Style(name="gradientColor1", type="uint", format="Color", inherit="yes", theme="spark")]
	[Style(name="gradientColor2", type="uint", format="Color", inherit="yes", theme="spark")]
	[Style(name="gradientAngle", type="Number", format="Number", inherit="yes", theme="spark")]
	[Style(name="border", type="uint", format="Color", inherit="yes", theme="spark")]
	[Style(name="showBorder", type="Boolean", format="Boolean", inherit="yes", theme="spark")]
	[Style(name="showShadow", type="Boolean", format="Boolean", inherit="yes", theme="spark")]
	[Style(name="radius", type="Number", format="Number", inherit="no", theme="spark")]
	[Style(name="corners", type="String", format="String", enumeration="all,top,bottom,left,right,tl,tr,bl,br,none", inherit="yes", theme="spark")]
	
	public class ThemedContainer extends SkinnableContainer
	{
		public function ThemedContainer()
		{
			super();
		}
	}
}
