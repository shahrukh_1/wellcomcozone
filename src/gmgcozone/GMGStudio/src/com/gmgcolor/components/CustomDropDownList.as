package com.gmgcolor.components
{
import flash.events.MouseEvent;

import mx.events.FlexEvent;
import mx.events.FlexMouseEvent;

import spark.components.Group;

public class CustomDropDownList extends Group
	{
		
		[Bindable]
		public var lstSelectedIndex:int = -1;
		
		[Bindable]
		public var toggleSelected:Boolean;
		
		
		public function CustomDropDownList()
		{
			super();
		}
		
		public function state1_enterStateHandler(event:FlexEvent):void
		{
			
		}
		
		public function togglebutton1_clickHandler(event:MouseEvent):void
		{
			var toggleBtn:CustomIconToggleButton = CustomIconToggleButton(event.target);
			if(toggleBtn.selected)
			{
				currentState = 'popUpOpen';
				lstSelectedIndex = -1;
			}
			else
				currentState = 'normal';
		}
		
		public function vgroup1_mouseDownOutsideHandler(event:FlexMouseEvent):void
		{
			

			if(event.relatedObject is CustomIconToggleButton)
				return;
			currentState = 'normal';
			
			toggleSelected = false;
			
		}
	}
}
