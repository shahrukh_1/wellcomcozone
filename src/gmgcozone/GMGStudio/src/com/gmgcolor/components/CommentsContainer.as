package com.gmgcolor.components {
import com.gmgcolor.annotations.presentation.CommentsPM;

import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.core.UIComponent;
import mx.events.SandboxMouseEvent;

import spark.components.Group;
import spark.components.SkinnableContainer;
import spark.components.VGroup;

public class CommentsContainer extends SkinnableContainer {
		public function CommentsContainer()
		{
			super();
		}

		public var header:Group;
		public var comments:Group;
		public var inputNote:Group;
		public var creatorNote:VGroup;
		public var commentsList:VGroup;

		private var prevWidth:Number;
		private var prevHeight:Number;

		[Bindable]
		public var commentsPM:CommentsPM;

		private var clickOffset:Point;

		[SkinPart(required="true")]
		public var resizeHandle:UIComponent;

		public function updateHeight():void
		{
			if (currentState == "reply") {
				comments.height = height - header.height - inputNote.height;
			}
			else if (currentState == "post") {
				comments.height = 0;
				inputNote.height = height - header.height;
			}

		}

		public function checkHeight():void
		{
			validateNow();
			if (height >= parent.height - y - 1) {
				height = parent.height - y - 1;
				comments.height = height - header.height - inputNote.height;
				if (commentsList) {
					if (commentsList.height < comments.height) {
						height = NaN;
						updateHeight();
					}
				}
			}
			else {
				height = NaN;
				updateHeight();
			}


			validateNow();
		}

		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);

			if (instance == resizeHandle) {
				resizeHandle.addEventListener(MouseEvent.MOUSE_DOWN, resizeHandle_mouseDownHandler);
			}
		}

		override protected function partRemoved(partName:String, instance:Object):void
		{
			if (instance == resizeHandle) {
				resizeHandle.removeEventListener(MouseEvent.MOUSE_DOWN, resizeHandle_mouseDownHandler);
			}

			super.partRemoved(partName, instance);
		}


		protected function resizeHandle_mouseDownHandler(event:MouseEvent):void
		{
			if (enabled && !clickOffset) {
				clickOffset = new Point(event.stageX, event.stageY);
				prevWidth = width;
				prevHeight = height;

				var sbRoot:DisplayObject = systemManager.getSandboxRoot();

				sbRoot.addEventListener(
						MouseEvent.MOUSE_MOVE, resizeHandle_mouseMoveHandler, true);
				sbRoot.addEventListener(
						MouseEvent.MOUSE_UP, resizeHandle_mouseUpHandler, true);
				sbRoot.addEventListener(
						SandboxMouseEvent.MOUSE_UP_SOMEWHERE, resizeHandle_mouseUpHandler)
			}
		}

		protected function resizeHandle_mouseMoveHandler(event:MouseEvent):void
		{
			// during a resize, only the TitleWindow should get mouse move events
			// we don't check the target since this is on the systemManager and the target
			// changes a lot -- but this listener only exists during a resize.
			event.stopImmediatePropagation();

			if (!clickOffset) {
				return;
			}

			var tempWidth:Number = prevWidth + (event.stageX - clickOffset.x);
			var tempHeight:Number = prevHeight + (event.stageY - clickOffset.y);

			if (currentState == "reply") {
				minHeight = header.height + inputNote.height + Math.min(creatorNote.height, 75) + 8;
			}
			else if (currentState == "post") {
				minHeight = header.height + 70;
			}

			width = Math.max(minWidth, tempWidth);

			if (width > parent.width - x - 1)
				width = parent.width - x - 1;

			height = Math.max(minHeight, tempHeight);

			updateHeight()

			if (height > parent.height - y - 1) {
				height = parent.height - y - 1;
				comments.height = height - header.height - inputNote.height;
			}


			event.updateAfterEvent();
		}

		public function setMinimumHeight():void
		{
			inputNote.height = 150;
			inputNote.validateNow();
			if (currentState == "reply") {
				//inputNote.height = NaN;
				validateNow();
				comments.validateNow();
				creatorNote.validateNow();
				comments.height = creatorNote.height + 8
				height = header.height + inputNote.height + creatorNote.height + 8;
			} else if (currentState == "post") {
				comments.height = 0;
				comments.validateNow();
				inputNote.validateNow();
				height = header.height + inputNote.height;
				validateNow();
			}


		}

		protected function resizeHandle_mouseUpHandler(event:Event):void
		{
			clickOffset = null;
			prevWidth = NaN;
			prevHeight = NaN;

			var sbRoot:DisplayObject = systemManager.getSandboxRoot();

			sbRoot.removeEventListener(
					MouseEvent.MOUSE_MOVE, resizeHandle_mouseMoveHandler, true);
			sbRoot.removeEventListener(
					MouseEvent.MOUSE_UP, resizeHandle_mouseUpHandler, true);
			sbRoot.removeEventListener(
					SandboxMouseEvent.MOUSE_UP_SOMEWHERE, resizeHandle_mouseUpHandler);
		}

	}
}
