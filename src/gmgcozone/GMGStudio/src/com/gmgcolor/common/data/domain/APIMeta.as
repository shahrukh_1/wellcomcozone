package com.gmgcolor.common.data.domain
{
	[RemoteClass(alias="au.com.tabcorp.neo.api.common.vo.APIMeta")]
	public class APIMeta
	{
		public var requestId:String = "";

		public var jurisdictionId:uint = 1;
		
		public var deviceId:uint = 10000001;
	
		public var requestChannel:uint = 5;
		
		public var usernamePasswordToken:String;
	}
}