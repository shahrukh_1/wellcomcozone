package com.gmgcolor.common.event
{
	public class LoadReferenceDataMessage
	{
		private var _categoryName:String;
		public function LoadReferenceDataMessage(categoryName:String)
		{
			_categoryName = categoryName;
			 
		}

		public function get categoryName():String
		{
			return _categoryName;
		}

	}
}