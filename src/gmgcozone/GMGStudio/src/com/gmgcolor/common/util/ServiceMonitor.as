package com.gmgcolor.common.util {
import flash.events.EventDispatcher;

import mx.collections.ArrayCollection;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.rpc.http.HTTPService;
import mx.rpc.remoting.RemoteObject;

import org.spicefactory.parsley.core.context.Context;

/**
	 * Monitor for trafic on the service layer.
	 */
	[Event(name="result", type="mx.rpc.events.FaultEvent")]
	[Event(name="fault", type="mx.rpc.events.ResultEvent")]
	public class ServiceMonitor extends EventDispatcher {

		private var configuredServices:ArrayCollection = new ArrayCollection();

		[Inject]
		public var context:Context;

		[Init]
		public function onWiringComplete():void
		{
			configureAllRemoteObjects();
			configureAllHttpServices();
		}

		private function configureAllHttpServices():void
		{
			var allhttpServices:Array = getHttpServices();
			for each(var service:HTTPService in allhttpServices) {
				configureService(service);
			}

		}

		private function configureAllRemoteObjects():void
		{
			var allRemoteObjects:Array = getRemoteObjects();
			for each(var remoteObject:RemoteObject in allRemoteObjects) {
				configureService(remoteObject);
			}
		}

		private function configureService(service:*):void
		{
			if (!configuredServices.contains(service)) {
				service.addEventListener(FaultEvent.FAULT, onFault, false, 0, true);
				service.addEventListener(ResultEvent.RESULT, onResult, false, 0, true);
				configuredServices.addItem(service);
			}
		}

		protected function onFault(event:FaultEvent):void
		{
			dispatchEvent(event);
		}

		protected function onResult(event:ResultEvent):void
		{
			dispatchEvent(event);
		}

		[Observe]
		public function onServiceInitialized(service:RemoteObject):void
		{
			configureService(service);
		}

		[Observe]
		public function onHttpServiceInitialized(service:HTTPService):void
		{
			configureService(service);
		}

		private function getRemoteObjects():Array
		{
			return context.getAllObjectsByType(RemoteObject);
		}

		private function getHttpServices():Array
		{
			return context.getAllObjectsByType(HTTPService);
		}


	}
}
