package com.gmgcolor.common.util
{
import com.gmgcolor.application.enums.ResourceBundle;
import com.gmgcolor.common.errornotifications.models.ErrorVO;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;

/**
	 * The APIFaultMessage class is a util class to process server side fault objects
	 */
	public class APIFaultMessage
	{
		private static const DEFAULT_ERROR_MESSAGE:String = "Internet-00001";
		
		public static var resourceManager:IResourceManager = ResourceManager.getInstance();
		
		/**
		 * This method processes the APIException object thrown by server and sends back a list of error messages. 
		 * 
		 * @param fault The exception object received when the remote API throws an exception. 
		 * @param useFaultString If there is no matching API message, then this flag indicates if we can use the value passed in the faultString of the FaultEvent.
		 * 			Note - The faultString can contain API specific fault content (eg., stack traces) so should only be used if the faultString is guranteed to be sanitized.
		 * @return String List of error messages
		 *
		 */
		public static function getAPIErrors(fault:Object,useFaultString:Boolean=false):Array
		{
			var messageMap:Object = getMessageMap(fault);
			
			var errorList:Array = new Array();
			for(var prop:String in messageMap)
			{
				var apiErrorMessage:String = messageMap[prop];
				if(!apiErrorMessage || apiErrorMessage == "")
				{
					apiErrorMessage = resourceManager.getString(ResourceBundle.ERRORS_BUNDLE, DEFAULT_ERROR_MESSAGE);
				}
				var error:ErrorVO = new ErrorVO(ErrorVO.SERVICE_FAULT, prop, apiErrorMessage);
				errorList.push(error);
			}
			return errorList;
		}
		
		
		public static function isErrorSuppressed(errorCode:String):Boolean
		{
			// e.g. "API-3036"
			return resourceManager.getBoolean(ResourceBundle.CONFIGURATION_BUNDLE, errorCode + ".suppress");
		}		
		
		private static function getMessageMap(fault:Object):Object
		{
			var result:Object = new Object();
			if (!(fault is FaultEvent))
				return result;
			if (fault && fault.message.rootCause && fault.message.rootCause.hasOwnProperty('messageMap'))
			{
				result = getProcessedMessageMap(fault.message.rootCause.messageMap);
			}
			return result;
		}
		
		private static function getProcessedMessageMap(messageMap:Object) :Object
		{
			var result:Object = overrideAPIMessages(messageMap);
			return filterSuppressedMessages(result);
		}
		
		private static function filterSuppressedMessages(messageMap:Object):Object
		{
			var result:Object = new Object();
			var prop:String;
			for (prop in messageMap)
			{
				if(!isErrorSuppressed(prop))
					result[prop] = messageMap[prop];
			}
			return result;
		}
		
		private static function overrideAPIMessages(messageMap:Object) :Object
		{
			var result:Object = new Object();
			var prop:String;
			for (prop in messageMap)
			{
				// e.g. "API-3036"
				var propertyParts:Array = prop.split("-");
				if(propertyParts.length > 1)
				{
					var flexAPIProperty:String = "FLEX-" + propertyParts[1];
					var messageKey:String = resourceManager.getString(ResourceBundle.ERRORS_BUNDLE, flexAPIProperty + ".messageKey");
					if(messageKey)						
					{
						var message:String = resourceManager.getString(ResourceBundle.ERRORS_BUNDLE, messageKey);
						result[flexAPIProperty] = message;
					}
					else
					{
						result[prop] = messageMap[prop];
					}
				}
				else
				{
					result[prop] = messageMap[prop];
				}
			}
			return result;

		}
			
		/**
		 * Calls to get weather fault obj has perticular error id or not. 
		 * @param fault
		 * @param errorId
		 * @return 
		 * 
		 */		
		public static function hasAPIErrorID(fault:Object, errorId:String):Boolean
		{
			var messageMap:Object = new Object();
			if(fault.message && fault.message.rootCause && fault.message.rootCause.messageMap)
			{
				messageMap = fault.message.rootCause.messageMap;
				var prop:String;
				var hasAPIErrorId:Boolean;
				for (prop in messageMap)
				{
					if(prop == errorId)						
					{
						hasAPIErrorId = true;
						break;
					}
				}
			}
			else
			{
				hasAPIErrorId = false;
			}
			return hasAPIErrorId;
		}
	}
}
