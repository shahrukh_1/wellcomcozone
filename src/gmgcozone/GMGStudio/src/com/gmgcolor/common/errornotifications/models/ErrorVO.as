package com.gmgcolor.common.errornotifications.models
{
	[Bindable]
	public class ErrorVO
	{
		public static const SERVICE_FAULT:String = "Service Error";
		public static const NETWORK_FAULT:String = "Network Error";
		public static const FLEX_FAULT:String = "UI Error";
		
		private var _type:String;
		private var _code:String;
		private var _message:String;
		
		public function ErrorVO(type:String, code:String, message:String)
		{
			this._type = type;
			this._code = code;
			this._message = message;
		}
		
		public function get type():String
		{
			return this._type;
		}
		
		public function get code():String
		{
			return this._code;
		}
		
		public function get message():String
		{
			return this._message;
		}
		
	}
}