package com.gmgcolor.common.errornotifications.models {

import com.gmgcolor.annotations.domain.AccountUserInfo;
import com.gmgcolor.annotations.domain.ErrorFO;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.enums.ResourceBundle;
import com.gmgcolor.application.event.RecordErrorEvent;
import com.gmgcolor.common.errornotifications.messages.ConsolidatedErrorMessage;
import com.gmgcolor.common.errornotifications.messages.ISuppressOnNetworkError;
import com.gmgcolor.common.errornotifications.messages.ShowErrorNotificationView;
import com.gmgcolor.common.util.APIFaultMessage;
import com.gmgcolor.common.util.Dispatcher;
import com.gmgcolor.common.util.ServiceMonitor;

import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.resources.ResourceManager;
import mx.rpc.Fault;
import mx.rpc.events.ResultEvent;

public class ErrorNotificationPM extends Dispatcher {

		private static const TIME_TO_SUPPRESS:Number = (5 * 60 * 1000); // 5 mins

		private var lastNetworkErrorRecievedTime:Number = 0;

		private var _accountUserInfo:AccountUserInfo;

		[Bindable]
		public var errorList:ArrayCollection = new ArrayCollection();

		[Inject]
		public var serviceMonitor:ServiceMonitor;

		private var _title:String;

		[Bindable(event="titleChange")]
		[Bindable("localeChanged")]
		public function get title():String
		{
			return _title;
		}

		public function set title(value:String):void
		{
			if (_title !== value) {
				_title = value;
				dispatchEvent(new Event("titleChange"));
			}
		}

		[Init]
		public function onWiringComplete():void
		{
			//title = ResourceManager.getInstance().getString(ConfigEnum.LABELS_BUNDLE, "common.errornotification.title.text");
			serviceMonitor.addEventListener(ResultEvent.RESULT, onServiceResult, false, 0, true);
		}

		private function onServiceResult(event:ResultEvent):void
		{
			this.lastNetworkErrorRecievedTime = 0;
		}

		[CommandError]
		public function onServiceFault(faultEvent:Fault, request:Object):void
		{
			processNetworkErrors(faultEvent, request);
		}

		private function processNetworkErrors(faultEvent:Fault, request:Object):void
		{
			var errorMessage:String = getErrorMessage(faultEvent);
			var error:ErrorVO = new ErrorVO(ErrorVO.NETWORK_FAULT, faultEvent.faultCode, errorMessage);
			addErrorToList(error);
		}

		private function suppressNetworkError(fault:Fault, request:Object):Boolean
		{
			if (!suppressTimeExpired())
				return true;

			if (isRequestSuppressed(request))
				return true;

			if (!fault)
				return true;
			if (isSuppressedFault(fault.faultCode))
				return true;

			var event:Event = fault.rootCause as Event;
			if (event && isSuppressedErrorEvent(event.type))
				return true;

			return false;
		}

		/**
		 * Checks for the current time against the first error message recieved since last
		 * successful response.
		 */
		private function suppressTimeExpired():Boolean
		{
			var currentTime:Number = new Date().getTime();
			if ((currentTime - lastNetworkErrorRecievedTime) > TIME_TO_SUPPRESS)
				return true;
			else
				return false;
		}

		private function isRequestSuppressed(request:Object):Boolean
		{
			return request && request is ISuppressOnNetworkError;
		}

		private function isSuppressedFault(faultCode:String):Boolean
		{
			if (!faultCode)
				return false;
			return ResourceManager.getInstance().getBoolean(ResourceBundle.CONFIGURATION_BUNDLE, "faultCode." + faultCode + ".suppress");
			;
		}

		private function getErrorMessage(fault:Fault):String
		{

			return fault.faultDetail;
		}

		private function isSuppressedErrorEvent(type:String):Boolean
		{
			return ResourceManager.getInstance().getBoolean(ResourceBundle.CONFIGURATION_BUNDLE, "rootCause." + type + ".suppress");
		}

		private function processAPIErrors(faultEvent:Fault):void
		{
			var errors:Array = APIFaultMessage.getAPIErrors(faultEvent, true);
			for each(var error:ErrorVO in errors) {
				addErrorToList(error);
			}
		}

		private function isAPIError(fault:Object):Boolean
		{
			return fault && fault.message.rootCause && fault.message.rootCause.hasOwnProperty('messageMap');
		}

		[MessageHandler]
		public function onUIErrorMessage(message:ConsolidatedErrorMessage):void
		{
			if (message.title != null)
				this.title = message.title;
			var error:ErrorVO = new ErrorVO(ErrorVO.FLEX_FAULT, null, message.message);
			addErrorToList(error);
		}


		[Subscribe]
		[Bindable]
		public function get accountUserInfo():AccountUserInfo
		{
			return _accountUserInfo;
		}

		public function set accountUserInfo(accountUserInfo:AccountUserInfo):void
		{
			_accountUserInfo = accountUserInfo;
		}

		[Inject]
		public var securityManager:SecurityManager;


		private function addErrorToList(error:ErrorVO):void
		{

			var errorFO:ErrorFO = new ErrorFO;
			if (accountUserInfo)
				errorFO.AccountId = String(accountUserInfo.AccountID);
			errorFO.Component = ""
			errorFO.SessionId = String(securityManager.sessionId);
			errorFO.Source = ""
			errorFO.StackTrace = error.message;
			if (accountUserInfo)
				errorFO.UserId = String(accountUserInfo.UserID);

			if (error.message != "Client.Error.MessageSend" && error.message != "500" && error.message != "details not available")
				dispatcher(new RecordErrorEvent(RecordErrorEvent.RECORD, errorFO));
			if (duplicateMessageExists(error))
				return;
			if (errorList.length == 0)
				showErrorNotificationView();
			errorList.addItemAt(error, 0);
		}

		private function duplicateMessageExists(newError:ErrorVO):Boolean
		{
			for each(var error:ErrorVO in errorList) {
				if (error.message == newError.message)
					return true;
			}
			return false;
		}

		private function showErrorNotificationView():void
		{
			dispatcher(new ShowErrorNotificationView());
		}

		public function clear():void
		{
			errorList.removeAll();
			//title = ResourceManager.getInstance().getString(ConfigEnum.LABELS_BUNDLE, 'common.errornotification.title.text');
		}
	}
}
