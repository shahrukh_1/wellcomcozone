package com.gmgcolor.common.errornotifications.messages
{
	/**
	 * Broadcast this message to show a error message on the Error consilidation UI
	 */
	public class ConsolidatedErrorMessage
	{
		private var _message:String;
		private var _title:String;
		
		public function ConsolidatedErrorMessage(message:String, title:String = '')
		{
			this._message = message;
			this._title = title;
		}
		
		public function get message():String
		{
			return this._message;
		}

		public function get title():String
		{
			return this._title;
		}
	}
}