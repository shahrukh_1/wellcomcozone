package com.gmgcolor.utils {
	public class FXGUtil {

		static public const FXG_TEMPLATE:XML = <Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			<Library/>
			<Private/>
		</Graphic>;
		static public const SVG_TEMPLATE:XML = <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
			<g></g>
		</svg>;

		namespace svgns = "http://www.w3.org/2000/svg";

		use namespace svgns;

		public function FXGUtil()
		{
		}

		static public function createFXGTemplate():XML
		{
			return new XML(FXG_TEMPLATE.toXMLString());
		}

		static public function createSVGTemplate():XML
		{
			return new XML(SVG_TEMPLATE.toXMLString());
		}

		static public function offsetFXGContent(value:XML, xOffset:Number, yOffset:Number):XML
		{
			var fxg:XML = value;
			if (fxg) {
				fxg = new XML(value.toXMLString()); // duplicate XML Object so we don't override what's there.
				for each(var node:XML in fxg.children()) {
					offsetElement(node, xOffset, yOffset);
				}
			}
			return fxg; // return new FXG
		}

		static public function setSVGSize(value:XML, width:Number, height:Number):XML
		{
			// Set instrinsic size of svg
			var svg:XML = value;
			if (svg) {
				svg = new XML(value.toXMLString());
				svg.@width = width+"px";
				svg.@height = height + "px"
				svg.@viewBox = "0 0 " + width + " " + height;
			}
			return svg;
		}

		static private function offsetElement(element:XML, xOffset:Number, yOffset:Number):void
		{
			switch (QName(element.name()).localName) {
				case 'Rect':
				case 'Line':
				case 'Ellipse':
					element.@x += xOffset;
					element.@y += yOffset;
				case 'Path':
					var data:Array = element.@data.split(' ');
					for (var i:uint = 0, len:uint = data.length; i < len; i++) {
						switch (data[i].toLowerCase()) {
							case 'c':
								//cubic bezier (C 45 50 20 30 10 20)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								data[i + 2] = Number(data[i + 2]) + yOffset;
								data[i + 3] = Number(data[i + 3]) + xOffset;
								data[i + 4] = Number(data[i + 4]) + yOffset;
								data[i + 5] = Number(data[i + 5]) + xOffset;
								data[i + 6] = Number(data[i + 6]) + yOffset;
								i += 6;
								break;
							case 's':
								//cubic bezier without control points (S 20 30 10 20)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								data[i + 2] = Number(data[i + 2]) + yOffset;
								data[i + 3] = Number(data[i + 3]) + xOffset;
								data[i + 4] = Number(data[i + 4]) + yOffset;
								i += 4;
								break;
							case 'h':
								//horizontal line (H 100)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								i++;
								break;
							case 'l':
								//line (L 50 30)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								data[i + 2] = Number(data[i + 2]) + yOffset;
								i += 2;
								break;
							case 'm':
								//move (M 10 20)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								data[i + 2] = Number(data[i + 2]) + yOffset;
								i += 2;
								break;
							case 'q':
								//quadratic bezier (Q 110 45 90 30)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								data[i + 2] = Number(data[i + 2]) + yOffset;
								data[i + 3] = Number(data[i + 3]) + xOffset;
								data[i + 4] = Number(data[i + 4]) + yOffset;
								i += 4;
								break;
							case 't':
								//quadratic bezier without control points (T 90 30)
								data[i + 1] = Number(data[i + 1]) + xOffset;
								data[i + 2] = Number(data[i + 2]) + yOffset;
								i += 2;
								break;
							case 'v':
								//vertical line (V 100)
								data[i + 1] = Number(data[i + 1]) + yOffset;
								i++;
								break;
							case 'z':
								//end
								break;
						}
					}

					element.@data = data.join(' ');
					break;
				case 'Group':
					for each(var child:XML in element.children()) {
						offsetElement(child, xOffset, yOffset);
					}
					break;
			}
		}

		static public function toSVG(fxg:XML):XML
		{
			var svg:XML = createSVGTemplate();
			if (fxg) {
				for each(var node:XML in fxg.children()) {
					node = convertElement(node);
					if (node) {
						svg.svgns::g[0].appendChild(node);
					}
				}
			}
			return svg;
		}

		static private function convertElement(element:XML):XML
		{
			var svg:XML, child:XML;
			switch (QName(element.name()).localName) {
				case 'Rect':
					svg = new XML('<rect stroke="none" fill="none" stroke-width="1" stroke-linecap="square" stroke-linejoin="square"></rect>');
					break;
				case 'Path':
					svg = new XML('<path stroke="none" fill="none" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"></path>');
					break;
				case 'Line':
					svg = new XML('<line></line>');
					break;
				case 'Ellipse':
					svg = new XML('<ellipse></ellipse>');
					break;
				case 'Group':
					svg = new XML('<g></g>');
					for each(child in element.children()) {
						svg.appendChild(convertElement(child));
					}
					break;
			}
			if (svg) {
				copyAttributes(element, svg);
				if (element.children().length() != 0) {
					for each(child in element.children()) {
						convertChildren(child, svg);
					}
				}
			}
			return svg;
		}

		static private function copyAttributes(fxg:XML, svg:XML):void
		{
			for each(var attribute:XML in fxg.@*) {
				var attributeName:String = QName(attribute.name()).localName;
				switch (attributeName.toLowerCase()) {
					case 'data':
						svg.@d = fxg.@data;
						break;
					case 'color':
						svg.@color = convertColor(fxg.@color);
						break;
					default:
						svg.@[attributeName] = fxg.@[attributeName];
						break;
				}
			}
		}

		static private function convertChildren(fxg:XML, svg:XML):void
		{
			switch (QName(fxg.name()).localName) {
				case "fill":
					if (fxg.children().length() != 0) {
						svg.@fill = convertColor(fxg.children()[0].@color);
					}
					break;
				case "stroke":
					if (fxg.children().length() != 0) {
						svg.@stroke = convertColor(fxg.children()[0].@color);
						svg.@['stroke-width'] = fxg.children()[0].@weight;
						svg.@['stroke-linecap'] = fxg.children()[0].@caps;
						svg.@['stroke-linejoin'] = fxg.children()[0].@joints;
					}
					break;
			}
		}

		static private function convertColor(color:String):String
		{
			return color.toLowerCase().indexOf('0x') == 0 ? '#' + color.slice(2) : color;
		}
	}
}
