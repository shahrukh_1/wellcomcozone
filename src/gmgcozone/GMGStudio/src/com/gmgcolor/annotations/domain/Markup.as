package com.gmgcolor.annotations.domain
{
import flash.utils.ByteArray;

[Bindable]
	public class Markup
	{
		public function Markup()
		{
			
		}
		
		public var dataByteArray:ByteArray;
		public var width:Number;
		public var height:Number;
		public var x:Number;
		public var y:Number;

	}
}
