package com.gmgcolor.annotations.domain
{
import mx.collections.ArrayCollection;

[Bindable]
	public class AnnotationGroupByName
	{
		public function AnnotationGroupByName()
		{
			list = new ArrayCollection();
		}
		
		/** The value is used for header text */
		public var groupInfo:String;
		
		public var groupName:String;
		
		public var statusId:int;
		
		public var groupLevel:int=1;
		
		public var PrimaryDecisionMaker:Boolean;
		
		public var groupCollaborator:ApprovalCollaborator;
		
		/** The value is used for runner list */
		public var list:ArrayCollection;
		
	}
}
