package com.gmgcolor.annotations.domain {
import com.gmgcolor.application.presentation.FXGConvertor;
import com.greensock.transform.TransformItem;

[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalAnnotationMarkupFO")]
	public class ApprovalAnnotationMarkupFO {
		public function ApprovalAnnotationMarkupFO()
		{
			_Shape = new ShapeFO();
		}

		private var _ID:int

		private var _ApprovalAnnotation:int

		private var _X:Number;

		private var _Y:Number;

		private var _ScaleX:Number;

		private var _ScaleY:Number;

		private var _BackgroundColor:String;

		private var _Shape:ShapeFO;

		private var _Rotation:Number;

		private var _isLocked:Boolean;

		private var _createdItem:FXGConvertor;

		private var _transformItem:TransformItem;

		public var xml:XML;


		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}

		public function get ApprovalAnnotation():int
		{
			return _ApprovalAnnotation;
		}

		public function set ApprovalAnnotation(value:int):void
		{
			_ApprovalAnnotation = value;
		}

		public function get X():Number
		{
			return _X;
		}

		public function set X(value:Number):void
		{
			_X = value;
		}

		public function get Y():Number
		{
			return _Y;
		}

		public function set Y(value:Number):void
		{
			_Y = value;
		}

		public function get ScaleX():Number
		{
			return _ScaleX;
		}

		public function set ScaleX(value:Number):void
		{
			_ScaleX = value;
		}

		public function get ScaleY():Number
		{
			return _ScaleY;
		}

		public function set ScaleY(value:Number):void
		{
			_ScaleY = value;
		}


		public function get BackgroundColor():String
		{
			return _BackgroundColor;
		}

		public function set BackgroundColor(value:String):void
		{
			_BackgroundColor = value;
		}


		public function get isLocked():Boolean
		{
			return _isLocked;
		}

		public function set isLocked(value:Boolean):void
		{
			_isLocked = value;
		}


		public function get Shape():ShapeFO
		{
			return _Shape;
		}

		public function set Shape(value:ShapeFO):void
		{
			_Shape = value;
		}

		public function get Rotation():Number
		{
			return _Rotation;
		}

		public function set Rotation(value:Number):void
		{
			_Rotation = value;
		}

		[Transient]
		public function get createdItem():FXGConvertor
		{
			return _createdItem;
		}

		public function set createdItem(value:FXGConvertor):void
		{
			_createdItem = value;
		}

		[Transient]
		public function get transformItem():TransformItem
		{
			return _transformItem;
		}

		public function set transformItem(value:TransformItem):void
		{
			_transformItem = value;
		}


	}
}
