package com.gmgcolor.annotations.domain
{
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ErrorFO")]
	public class ErrorFO
	{
		private var _AccountId:String;
		private var _SessionId:String;
		private var _UserId:String;
		private var _Component:String;
		private var _Source:String;
		private var _StackTrace:String;
		
		public function ErrorFO()
		{
		}

		public function get AccountId():String
		{
			return _AccountId;
		}

		public function set AccountId(value:String):void
		{
			_AccountId = value;
		}

		public function get SessionId():String
		{
			return _SessionId;
		}

		public function set SessionId(value:String):void
		{
			_SessionId = value;
		}

		public function get UserId():String
		{
			return _UserId;
		}

		public function set UserId(value:String):void
		{
			_UserId = value;
		}

		public function get Component():String
		{
			return _Component;
		}

		public function set Component(value:String):void
		{
			_Component = value;
		}

		public function get Source():String
		{
			return _Source;
		}

		public function set Source(value:String):void
		{
			_Source = value;
		}

		public function get StackTrace():String
		{
			return _StackTrace;
		}

		public function set StackTrace(value:String):void
		{
			_StackTrace = value;
		}


	}
}