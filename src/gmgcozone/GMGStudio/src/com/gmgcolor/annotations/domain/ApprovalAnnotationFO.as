package com.gmgcolor.annotations.domain
{
import com.gmgcolor.annotations.enum.AnnotationEnum;

import org.osmf.metadata.CuePoint;
import org.osmf.metadata.CuePointType;

[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalAnnotationFO")]
	public class ApprovalAnnotationFO
	{
		public function ApprovalAnnotationFO()
		{

		}

		public static const ANNOTATION_NORMAL:String = "normal";
		public static const ANNOTATION_BY_NAME:String = "byName";
		public static const ANNOTATION_BY_STATUS:String = "byStatus";
		public static const ANNOTATION_BY_PAGE:String = "byPage";

		public static const currentState:String = ANNOTATION_NORMAL;

		private var _GivenName:String;
		private var _FamilyName:String;

		//Server properties

		public var isEditMode:String;

		public var ApprovalAnnotationAttachment:ApprovalAnnotationAttachmentFO;

		public var ApprovalAnnotationMarkups:Array = [];

		public var ApprovalAnnotationPrivateCollaborators:Array;

		public var ID:int;

		public var Creator:int;

		public var Modifier:int;

		public var ReferenceFilePath:String;

		public var Comment:String;

		public var AnnotatedDate:Date;

		public var CommentType:int = AnnotationEnum.ANNOTATION_TYPE_MARKER;

		public var attachmentFilename:String;

		public var ModifiedDate:Date;

		public var CommentNumber:int;

		public var Parent:int;

		public var ExternalUser:int;

		public var Page:int;

		public var PageNumber:int;

		public var Status:int;

		public var StartIndex:int;

		public var EndIndex:int;

		public var HighlightType:int;

		public var CrosshairXCoord:Number;

		public var CrosshairYCoord:Number;

		public var IsPrivate:Boolean;

		public var HighlightData:String;

		//Local Properties

		public var submitted:Boolean;

		public var annotationWidth:Number;

		public var annotationHeight:Number;

		public var approverId:int;

		public var approverFirstName:String;

		public var approverLastName:String;

		public var approvalPage:Number;

		public var width:Number;

		public var height:Number;

		public var x:Number;

		public var y:Number;

		public var GivenName:String;

		public var FamilyName:String;
		public var IsExternalUser:Boolean;

        private var _TimeFrame:Number;

        public var cuePoint:CuePoint;

		private function createRandomNumber():int
		{
			var rand:Number = Math.random();
			var max:Number = 99999999;
			var randInRange:Number = Math.round(rand * max);
			return randInRange;

		}

        public function get TimeFrame():Number
        {
            return _TimeFrame;
        }

        public function set TimeFrame(value:Number):void
        {
            _TimeFrame = value;
            cuePoint = new CuePoint(CuePointType.ACTIONSCRIPT, _TimeFrame / 1000, GivenName, this);
        }
    }
}
