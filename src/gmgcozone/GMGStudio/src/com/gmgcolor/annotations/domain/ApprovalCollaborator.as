package com.gmgcolor.annotations.domain
{
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalCollaboratorFO")]
	public class ApprovalCollaborator
	{
		private var _ID:int;
		private var _Approval:int;
		private var _AssignedDate:Date;
		private var _Collaborator:int;
		private var _FamilyName:String;
		private var _GivenName:String;
		private var _CollaboratorRoleName:String;
		private var _CollaboratorRoleID:int;
		private var _isChecked:Boolean;
		private var _isPrivateSelected:Boolean;
		private var _CollaboratorAvatar:String;
		private var _isPrimaryDecisionMaker:Boolean;
		public var IsExternal:Boolean;
		public var IsExpired:Boolean;
		public var ExpireDate:Date;

		public function ApprovalCollaborator()
		{
		}

		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}
		
		public function get Approval():int
		{
			return _Approval;
		}
		
		public function set Approval(value:int):void
		{
			_Approval = value;
		}
		
		public function get AssignedDate():Date
		{
			return _AssignedDate;
		}
		
		public function set AssignedDate(value:Date):void
		{
			_AssignedDate = value;
		}
		
		public function get Collaborator():int
		{
			return _Collaborator;
		}
		
		public function set Collaborator(value:int):void
		{
			_Collaborator = value;
		}
		
		public function get FamilyName():String
		{
			return _FamilyName;
		}
		
		public function set FamilyName(value:String):void
		{
			_FamilyName = value;
		}
		
		public function get GivenName():String
		{
			return _GivenName;
		}
		
		public function set GivenName(value:String):void
		{
			_GivenName = value;
		}


		public function get isChecked():Boolean
		{
			return _isChecked;
		}

		public function set isChecked(value:Boolean):void
		{
			_isChecked = value;
		}

		public function get isPrivateSelected():Boolean
		{
			return _isPrivateSelected;
		}

		public function set isPrivateSelected(value:Boolean):void
		{
			_isPrivateSelected = value;
		}

		public function get CollaboratorAvatar():String
		{
			return _CollaboratorAvatar;
		}

		public function set CollaboratorAvatar(value:String):void
		{
			_CollaboratorAvatar = value;
		}


		public function get CollaboratorRoleName():String
		{
			return _CollaboratorRoleName;
		}

		public function set CollaboratorRoleName(value:String):void
		{
			_CollaboratorRoleName = value;
		}


		public function get CollaboratorRoleID():int
		{
			return _CollaboratorRoleID;
		}

		public function set CollaboratorRoleID(value:int):void
		{
			_CollaboratorRoleID = value;
		}

		public function get isPrimaryDecisionMaker():Boolean
		{
			return _isPrimaryDecisionMaker;
		}

		public function set isPrimaryDecisionMaker(value:Boolean):void
		{
			_isPrimaryDecisionMaker = value;
		}
	}
}