package com.gmgcolor.annotations.domain
{
	
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalCollaboratorDecisionFO")]
	public class ApprovalCollaboratorDecisionFO
	{
		
		  	private  var _ID:int;
		
		       private  var _Approval:int;
		
		       private  var _Collaborator:int;
		
		       private var _AssignedDate:Date;
		
		       private var _OpenedDate:Date;
		
		       private var _CompletedDate:Date;
		
		       private var _Decision:int;
		
		
		public function ApprovalCollaboratorDecisionFO()
		{
		}


		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}

		public function get Approval():int
		{
			return _Approval;
		}

		public function set Approval(value:int):void
		{
			_Approval = value;
		}

		public function get Collaborator():int
		{
			return _Collaborator;
		}

		public function set Collaborator(value:int):void
		{
			_Collaborator = value;
		}

		public function get AssignedDate():Date
		{
			return _AssignedDate;
		}

		public function set AssignedDate(value:Date):void
		{
			_AssignedDate = value;
		}

		public function get OpenedDate():Date
		{
			return _OpenedDate;
		}

		public function set OpenedDate(value:Date):void
		{
			_OpenedDate = value;
		}

		public function get CompletedDate():Date
		{
			return _CompletedDate;
		}

		public function set CompletedDate(value:Date):void
		{
			_CompletedDate = value;
		}

		public function get Decision():int
		{
			return _Decision;
		}

		public function set Decision(value:int):void
		{
			_Decision = value;
		}

	}
}