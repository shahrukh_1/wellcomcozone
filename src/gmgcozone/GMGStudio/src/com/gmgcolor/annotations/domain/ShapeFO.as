package com.gmgcolor.annotations.domain {
[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ShapeFO")]
	public class ShapeFO {
		private var _ID:int;
		private var _FXG:String;
		private var _SVG:String;
		private var _IsCustom:Boolean;
		private var _isDrawing:Boolean;

		public function ShapeFO()
		{
		}

		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}

		public function get FXG():String
		{
			return _FXG;
		}

		public function set FXG(value:String):void
		{
			_FXG = value;
		}

		public function get SVG():String
		{
			return _SVG;
		}

		public function set SVG(value:String):void
		{
			_SVG = value;
		}


		public function get IsCustom():Boolean
		{
			return _IsCustom;
		}

		public function set IsCustom(value:Boolean):void
		{
			_IsCustom = value;
		}

		[Transient]
		public function get isDrawing():Boolean
		{
			return _isDrawing;
		}

		public function set isDrawing(value:Boolean):void
		{
			_isDrawing = value;
		}
	}
}
