package com.gmgcolor.annotations.domain
{
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalDecisionFO")]
	public class ApprovalDecision
	{
		
		private var _ID:int;
		
		private var _Key:String;
		
		private var _Name:String;
		
		private var _Priority:int;

		public function get Key():String
		{
			return _Key;
		}

		public function set Key(value:String):void
		{
			_Key = value;
		}

		public function get Name():String
		{
			return _Name;
		}

		public function set Name(value:String):void
		{
			_Name = value;
		}

		public function get Priority():int
		{
			return _Priority;
		}

		public function set Priority(value:int):void
		{
			_Priority = value;
		}

		
		public function ApprovalAnnotationStatus():void
		{
			
		}

		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}
		
		/*public function get iconClass():Class
		{
			switch(Key)
			{
				case result:
				{
					
					break;
				}
					
				default:
				{
					break;
				}
			}
		}*/
		
		/*decisionType.iconClass = ApplicationEnum.ICON_PENDING;
		decisionType.name = "Pending"
		decisionTypes.addItem(decisionType);
		
		decisionType = new DecisionType;
		decisionType.iconClass = ApplicationEnum.ICON_APPROVED;
		decisionType.name = "Approved"
		decisionTypes.addItem(decisionType);
		
		decisionType = new DecisionType;
		decisionType.iconClass = ApplicationEnum.ICON_APPROVED_WITH_CHANGES;
		decisionType.name = "Approved with changes"
		decisionTypes.addItem(decisionType);
		
		decisionType = new DecisionType;
		decisionType.iconClass = ApplicationEnum.ICON_CHANGES_REQUIRED;
		decisionType.name = "Changes required"
		decisionTypes.addItem(decisionType);
	
		*/
	}
		
}