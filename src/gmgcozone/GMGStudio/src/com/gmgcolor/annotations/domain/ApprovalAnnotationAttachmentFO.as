package com.gmgcolor.annotations.domain
{
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalAnnotationAttachmentFO")]
	public class ApprovalAnnotationAttachmentFO
	{
		
		private var _ID:int
		
		private var  _ApprovalAnnotation:int;
		
		private var _DisplayName:String;
		
		private var _FileType:String;
		
		private var _Name:String;
		
		
		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}

		public function get ApprovalAnnotation():int
		{
			return _ApprovalAnnotation;
		}

		public function set ApprovalAnnotation(value:int):void
		{
			_ApprovalAnnotation = value;
		}

		public function get DisplayName():String
		{
			return _DisplayName;
		}

		public function set DisplayName(value:String):void
		{
			_DisplayName = value;
		}

		public function get FileType():String
		{
			return _FileType;
		}

		public function set FileType(value:String):void
		{
			_FileType = value;
		}

		public function get Name():String
		{
			return _Name;
		}

		public function set Name(value:String):void
		{
			_Name = value;
		}


		
		public function ApprovalAnnotationAttachmentFO()
		{
			
		}

	

	}
}