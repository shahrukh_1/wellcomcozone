package com.gmgcolor.annotations.domain {
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalFO")]
	public class Approval {

		private var _ID:int;
		private var _Account:int;
		private var _CreatedDate:Date
		private var _Creator:int;
		private var _Deadline:Date;
		private var _Decision:int;
		private var _Description:String;
		private var _DocumentPath:String;
		private var _Guid:String;
		private var _IsDeleted:Boolean;
		private var _IsPageSpreads:Boolean;
		private var _ModifiedDate:Date;
		private var _Modifier:int;
		private var _Parent:int;
		private var _SelectedAccessMode:int;
		private var _Title:String;
		private var _Version:int;
		private var _LockTheProof:Boolean;
		private var _LoginRequired:Boolean;
		private var _AllowDownloadOriginal:Boolean;
		private var _Pages:Array;
		private var _Collaborators:Array;
		private var _Versions:Array;
		private var _Status:int;
		private var _VersionNumber:Number;
		private var _IsLocked:Boolean;
		private var _OnlyOneDecisionRequired:Boolean;
		private var _PrimaryDecisionMaker:int;

		private var _LockProofWhenAllDecisionsMade:Boolean;
		private var _Annotations:Array;
		private var _ApprovalFolder:String
		private var _OriginalFileType:String;
		private var _ApprovalType:String;

		private var _DownloadOriginalFileLink:String;
		private var _MovieFilePath:String;

		/*	private var Job:
		 private var ErrorMessage:
		 private var Owner:
		 private var CollaboratorRole:*/


		private var _isEnabled:Boolean;
        public static const IMAGE_TYPE:String = 'image';
        public static const VIDEO_TYPE:String = 'video';
        public static const SWF_TYPE:String = 'swf';

		public function Approval()
		{

		}


		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}

		public function get Account():int
		{
			return _Account;
		}

		public function set Account(value:int):void
		{
			_Account = value;
		}

		public function get CreatedDate():Date
		{
			return _CreatedDate;
		}

		public function set CreatedDate(value:Date):void
		{
			_CreatedDate = value;
		}

		public function get Creator():int
		{
			return _Creator;
		}

		public function set Creator(value:int):void
		{
			_Creator = value;
		}

		public function get Deadline():Date
		{
			return _Deadline;
		}

		public function set Deadline(value:Date):void
		{
			_Deadline = value;
		}

		public function get Decision():int
		{
			return _Decision;
		}

		public function set Decision(value:int):void
		{
			_Decision = value;
		}

		public function get Description():String
		{
			return _Description;
		}

		public function set Description(value:String):void
		{
			_Description = value;
		}

		public function get DocumentPath():String
		{
			return _DocumentPath;
		}

		public function set DocumentPath(value:String):void
		{
			_DocumentPath = value;
		}

		public function get Guid():String
		{
			return _Guid;
		}

		public function set Guid(value:String):void
		{
			_Guid = value;
		}

		public function get IsDeleted():Boolean
		{
			return _IsDeleted;
		}

		public function set IsDeleted(value:Boolean):void
		{
			_IsDeleted = value;
		}

		public function get IsPageSpreads():Boolean
		{
			return _IsPageSpreads;
		}

		public function set IsPageSpreads(value:Boolean):void
		{
			_IsPageSpreads = value;
		}

		public function get ModifiedDate():Date
		{
			return _ModifiedDate;
		}

		public function set ModifiedDate(value:Date):void
		{
			_ModifiedDate = value;
		}

		public function get Modifier():int
		{
			return _Modifier;
		}

		public function set Modifier(value:int):void
		{
			_Modifier = value;
		}

		public function get Parent():int
		{
			return _Parent;
		}

		public function set Parent(value:int):void
		{
			_Parent = value;
		}

		public function get SelectedAccessMode():int
		{
			return _SelectedAccessMode;
		}

		public function set SelectedAccessMode(value:int):void
		{
			_SelectedAccessMode = value;
		}

		public function get Title():String
		{
			return _Title;
		}

		public function set Title(value:String):void
		{
			_Title = value;
		}

		public function get Version():int
		{
			return _Version;
		}

		public function set Version(value:int):void
		{
			_Version = value;
		}

		public function get LockTheProof():Boolean
		{
			return _LockTheProof;
		}

		public function set LockTheProof(value:Boolean):void
		{
			_LockTheProof = value;
		}

		public function get LoginRequired():Boolean
		{
			return _LoginRequired;
		}

		public function set LoginRequired(value:Boolean):void
		{
			_LoginRequired = value;
		}

		public function get AllowDownloadOriginal():Boolean
		{
			return _AllowDownloadOriginal;
		}

		public function set AllowDownloadOriginal(value:Boolean):void
		{
			_AllowDownloadOriginal = value;
		}


		public function get Pages():Array
		{
			return _Pages;
		}

		public function set Pages(value:Array):void
		{
			_Pages = value;
		}


		public function get Collaborators():Array
		{
			return _Collaborators;
		}

		public function set Collaborators(value:Array):void
		{
			_Collaborators = value;
		}

		public function get Versions():Array
		{
			return _Versions;
		}

		public function set Versions(value:Array):void
		{
			_Versions = value;
		}


		public function get Status():int
		{
			return _Status;
		}

		public function set Status(value:int):void
		{
			_Status = value;
		}

		public function get VersionNumber():Number
		{
			return _VersionNumber;
		}

		public function set VersionNumber(value:Number):void
		{
			_VersionNumber = value;
		}

		public function get IsLocked():Boolean
		{
			return _IsLocked;
		}

		public function set IsLocked(value:Boolean):void
		{
			_IsLocked = value;
		}

		public function get OnlyOneDecisionRequired():Boolean
		{
			return _OnlyOneDecisionRequired;
		}

		public function set OnlyOneDecisionRequired(value:Boolean):void
		{
			_OnlyOneDecisionRequired = value;
		}


		public function get isEnabled():Boolean
		{
			return _isEnabled;
		}

		public function set isEnabled(value:Boolean):void
		{
			_isEnabled = value;
		}


		public function get PrimaryDecisionMaker():int
		{
			return _PrimaryDecisionMaker;
		}

		public function set PrimaryDecisionMaker(value:int):void
		{
			_PrimaryDecisionMaker = value;
		}


		public function get LockProofWhenAllDecisionsMade():Boolean
		{
			return _LockProofWhenAllDecisionsMade;
		}

		public function set LockProofWhenAllDecisionsMade(value:Boolean):void
		{
			_LockProofWhenAllDecisionsMade = value;
		}

		public function get Annotations():Array
		{
			return _Annotations;
		}

		public function set Annotations(value:Array):void
		{
			_Annotations = value;
		}


		public function get ApprovalFolder():String
		{
			return _ApprovalFolder;
		}

		public function set ApprovalFolder(value:String):void
		{
			_ApprovalFolder = value;
		}


		public function get OriginalFileType():String
		{
			return _OriginalFileType;
		}

		public function set OriginalFileType(value:String):void
		{
			_OriginalFileType = value;
		}

        [Bindable]
        public function get ApprovalType():String
        {
            return _ApprovalType;
        }

        public function set ApprovalType(value:String):void
        {
            _ApprovalType = value;
        }

		public function get DownloadOriginalFileLink():String
		{
			return _DownloadOriginalFileLink;
		}

		public function set DownloadOriginalFileLink(value:String):void
		{
			_DownloadOriginalFileLink = value;
		}


        public function get MovieFilePath():String
        {
            return _MovieFilePath;
        }

        public function set MovieFilePath(value:String):void
        {
            _MovieFilePath = value;
        }
    }
}
