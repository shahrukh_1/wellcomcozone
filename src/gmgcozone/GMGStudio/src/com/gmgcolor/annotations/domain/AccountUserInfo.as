package com.gmgcolor.annotations.domain
{
import com.gmgcolor.model.enums.ApprovalRole;

[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.AccountUserInfoFO")]
	public class AccountUserInfo
	{
		private var _UserID:int;
		private var _UserGivenName:String;
		private var _UserFamilyName:String;
		private var _UserIsActive:Boolean;
		private var _UserIsDeleted:Boolean;
		private var _UserPhotoPath:String;
		private var _UserStatusName:String;
		private var _Username:String;
		private var _UserHomeTelephoneNumber:String;	
		private var _UserEmailAddress:String;
		private var _UserMobileTelephoneNumber:String;
		private var _AccountID:int;
		private var _AccountName:String;
		private var _AccountTypeName:String;
		private var _AccountStatusName:String;
		private var _AccountContactEmailAddress:String;
		private var _AccountCurrencyName:String;
		private var _AccountCurrencyCode:String;
		private var _AccountDateFormat:String;
		private var _AccountTimeFormat:String;	
		private var _AccountTimeZone:String;
		private var _AccountThemeName:String;
		private var _AccountCustomColor:String;
		private var _AccountIsNeedPDFsToBeColorManaged:Boolean;
		private var _AccountIsNeedProfileManagement:Boolean;
		private var _AccountIsRemoveAllGMGCollaborateBranding:Boolean;
		private var _AccountLocaleName:String;
		private var _UserAvatar:String;
		private var _AccountCYMK_RGBIndicator:Boolean;
		private var _UserRole:int;
		private var _ApprovalRole:int;
		private var _UserIsExternal:Boolean;

		public function getDateTimeFormat():String
		{
			var a:String = AccountDateFormat;
			var b:String = AccountTimeFormat;

			switch(b)
			{
				case "12-hour clock":
				{
					return a + " h:mm:ss a";
					break;
				}
				case "24-hour clock":
				{
					return a + " H:mm";
					break;
				}
				default:
				{
					return a + " h:mm a";
					break;
				}
			}
			
		/*	MM/dd/yyyy
			1 12-hour clock 4:20 PM
			2 24-hour clock 16:20*/
		}
		
		

		public function get UserID():int 
		{
			return _UserID;
		}

		public function set UserID(value:int):void
		{
			_UserID = value;
		}

		public function get UserGivenName():String
		{
			return _UserGivenName;
		}

		public function set UserGivenName(value:String):void
		{
			_UserGivenName = value;
		}

		public function get UserFamilyName():String
		{
			return _UserFamilyName;
		}

		public function set UserFamilyName(value:String):void
		{
			_UserFamilyName = value;
		}

		public function get UserIsActive():Boolean
		{
			return _UserIsActive;
		}

		public function set UserIsActive(value:Boolean):void
		{
			_UserIsActive = value;
		}

		public function get UserIsDeleted():Boolean
		{
			return _UserIsDeleted;
		}

		public function set UserIsDeleted(value:Boolean):void
		{
			_UserIsDeleted = value;
		}

		public function get UserPhotoPath():String
		{
			return _UserPhotoPath;
		}

		public function set UserPhotoPath(value:String):void
		{
			_UserPhotoPath = value;
		}

		public function get UserStatusName():String
		{
			return _UserStatusName;
		}

		public function set UserStatusName(value:String):void
		{
			_UserStatusName = value;
		}

		public function get Username():String
		{
			return _Username;
		}

		public function set Username(value:String):void
		{
			_Username = value;
		}

		public function get UserHomeTelephoneNumber():String
		{
			return _UserHomeTelephoneNumber;
		}

		public function set UserHomeTelephoneNumber(value:String):void
		{
			_UserHomeTelephoneNumber = value;
		}

		public function get UserEmailAddress():String
		{
			return _UserEmailAddress;
		}

		public function set UserEmailAddress(value:String):void
		{
			_UserEmailAddress = value;
		}

		public function get UserMobileTelephoneNumber():String
		{
			return _UserMobileTelephoneNumber;
		}

		public function set UserMobileTelephoneNumber(value:String):void
		{
			_UserMobileTelephoneNumber = value;
		}

		public function get AccountID():int
		{
			return _AccountID;
		}

		public function set AccountID(value:int):void
		{
			_AccountID = value;
		}

		public function get AccountName():String
		{
			return _AccountName;
		}

		public function set AccountName(value:String):void
		{
			_AccountName = value;
		}

		public function get AccountTypeName():String
		{
			return _AccountTypeName;
		}

		public function set AccountTypeName(value:String):void
		{
			_AccountTypeName = value;
		}

		public function get AccountStatusName():String
		{
			return _AccountStatusName;
		}

		public function set AccountStatusName(value:String):void
		{
			_AccountStatusName = value;
		}

		public function get AccountContactEmailAddress():String
		{
			return _AccountContactEmailAddress;
		}

		public function set AccountContactEmailAddress(value:String):void
		{
			_AccountContactEmailAddress = value;
		}

		public function get AccountCurrencyName():String
		{
			return _AccountCurrencyName;
		}

		public function set AccountCurrencyName(value:String):void
		{
			_AccountCurrencyName = value;
		}

		public function get AccountCurrencyCode():String
		{
			return _AccountCurrencyCode;
		}

		public function set AccountCurrencyCode(value:String):void
		{
			_AccountCurrencyCode = value;
		}

		public function get AccountDateFormat():String
		{
			return _AccountDateFormat;
		}

		public function set AccountDateFormat(value:String):void
		{
			_AccountDateFormat = value;
		}

		public function get AccountTimeFormat():String
		{
			return _AccountTimeFormat;
		}

		public function set AccountTimeFormat(value:String):void
		{
			_AccountTimeFormat = value;
		}

		public function get AccountTimeZone():String
		{
			return _AccountTimeZone;
		}

		public function set AccountTimeZone(value:String):void
		{
			_AccountTimeZone = value;
		}

		public function get AccountThemeName():String
		{
			return _AccountThemeName;
		}

		public function set AccountThemeName(value:String):void
		{
			_AccountThemeName = value;
		}

		public function get AccountCustomColor():String
		{
			return _AccountCustomColor;
		}

		public function set AccountCustomColor(value:String):void
		{
			_AccountCustomColor = value;
		}

		public function get AccountIsNeedPDFsToBeColorManaged():Boolean
		{
			return _AccountIsNeedPDFsToBeColorManaged;
		}

		public function set AccountIsNeedPDFsToBeColorManaged(value:Boolean):void
		{
			_AccountIsNeedPDFsToBeColorManaged = value;
		}

		public function get AccountIsNeedProfileManagement():Boolean
		{
			return _AccountIsNeedProfileManagement;
		}

		public function set AccountIsNeedProfileManagement(value:Boolean):void
		{
			_AccountIsNeedProfileManagement = value;
		}

		public function get AccountIsRemoveAllGMGCollaborateBranding():Boolean
		{
			return _AccountIsRemoveAllGMGCollaborateBranding;
		}

		public function set AccountIsRemoveAllGMGCollaborateBranding(value:Boolean):void
		{
			_AccountIsRemoveAllGMGCollaborateBranding = value;
		}

		public function get AccountLocaleName():String
		{
			return _AccountLocaleName;
		}

		public function set AccountLocaleName(value:String):void
		{
			_AccountLocaleName = value;
		}


		public function get UserAvatar():String
		{
			return _UserAvatar;
		}

		public function set UserAvatar(value:String):void
		{
			_UserAvatar = value;
		}


		public function get AccountCYMK_RGBIndicator():Boolean
		{
			return true;
			return _AccountCYMK_RGBIndicator;
		}

		public function set AccountCYMK_RGBIndicator(value:Boolean):void
		{
			_AccountCYMK_RGBIndicator = value;
		}

		public function get UserRole():int
		{
			return _UserRole;
		}

		public function set UserRole(value:int):void
		{
			_UserRole = value;
		}

		public function get ApprovalRole():int
		{
			return _ApprovalRole;
		}

		public function set ApprovalRole(value:int):void
		{
			_ApprovalRole = value;
		}

		public function get UserIsExternal():Boolean
		{
			return _UserIsExternal;
		}

		public function set UserIsExternal(value:Boolean):void
		{
			_UserIsExternal = value;
		}

		public function getApprovalRole():com.gmgcolor.model.enums.ApprovalRole {
			return com.gmgcolor.model.enums.ApprovalRole.findById(this._ApprovalRole);
		}

		
		public function AccountUserInfo()
		{
		}
	}
}
