package com.gmgcolor.annotations.model {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;

import mx.collections.ArrayCollection;

[Event(name="childAnnotationsChange", type="flash.events.Event")]
	[Event(name="annotationChange", type="flash.events.Event")]
	[Event(name="annotationsChange", type="flash.events.Event")]
	[Event(name="inputTextChange", type="flash.events.Event")]
	[Event(name="submittableChange", type="flash.events.Event")]
	public class CommentsModel extends BaseUIModel {


		public static const ISSUBMITTED_CHANGE_EVENT:String = "isSubmittedChange";
		public static const CHILDANNOTATIONS_CHANGE_EVENT:String = "childAnnotationsChange";
		public static const ANNOTATION_CHANGE_EVENT:String = "annotationChange";
		public static const ANNOTATIONS_CHANGE_EVENT:String = "annotationsChange";
		public static const INPUTTEXT_CHANGE_EVENT:String = "inputTextChange";
		public static const SUBMITTABLE_CHANGE_EVENT:String = "submittableChange";

		private var _inputText:String;

		private var _isSubmitted:Boolean;

		private var _childAnnotations:ArrayCollection;

		private var _annotation:ApprovalAnnotationFO;

		private var _annotations:ArrayCollection;
		private var _submittable:Boolean;
		private var _selectedIndex:int;

		public function CommentsModel()
		{
			super();
		}


		[Bindable(event="submittableChange")]
		public function get submittable():Boolean
		{
			return _submittable;
		}

		public function set submittable(value:Boolean):void
		{
			_submittable = value;
			dispatchEvent(new Event(SUBMITTABLE_CHANGE_EVENT));
		}

		override public function get isOn():Boolean
		{
			return super.isOn;
		}

		override public function set isOn(value:Boolean):void
		{
			super.isOn = value;
			if (!value) {
				submittable = false;
				height = NaN;
			}
		}


		[Bindable(event="annotationsChange")]
		public function get annotations():ArrayCollection
		{
			return _annotations;
		}

		public function set annotations(value:ArrayCollection):void
		{
			if (_annotations != value) {
				_annotations = value;
				dispatchEvent(new Event(ANNOTATIONS_CHANGE_EVENT));
			}
		}

		[Bindable(event="childAnnotationsChange")]
		public function get childAnnotations():ArrayCollection
		{
			return _childAnnotations;
		}

		public function set childAnnotations(value:ArrayCollection):void
		{

			_childAnnotations = value;
			dispatchEvent(new Event(CHILDANNOTATIONS_CHANGE_EVENT));

		}

		public function get isSubmitted():Boolean
		{
			return _isSubmitted;
		}

		public function set isSubmitted(value:Boolean):void
		{
			if (_isSubmitted != value) {
				_isSubmitted = value;
				dispatchEvent(new Event(ISSUBMITTED_CHANGE_EVENT));
			}
		}

		[Bindable(event="annotationChange")]
		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public function set annotation(value:ApprovalAnnotationFO):void
		{
			_annotation = value;
			dispatchEvent(new Event(ANNOTATION_CHANGE_EVENT));
		}


		[Bindable(event="inputTextChange")]
		public function get inputText():String
		{
			return _inputText;
		}

		public function set inputText(value:String):void
		{
			if (_inputText != value) {
				_inputText = value;
				dispatchEvent(new Event(INPUTTEXT_CHANGE_EVENT));
			}
		}

		public function get selectedIndex():int
		{
			return _selectedIndex;
		}

		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;
		}
	}
}
