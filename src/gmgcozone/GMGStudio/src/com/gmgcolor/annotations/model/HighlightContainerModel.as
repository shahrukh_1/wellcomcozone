package com.gmgcolor.annotations.model
{
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;

import mx.collections.ArrayCollection;

[Event(name="textMetricsCollectionChange",type="flash.events.Event")]
	public class HighlightContainerModel extends BaseUIModel
	{
		
		public static const TEXTMETRICSCOLLECTION_CHANGE_EVENT:String = "textMetricsCollectionChange";
		
		
		private var _StartIndex:int;
		private var _EndIndex:int;
		private var _hightlightType:int;
		private var _SVG:String;
		
		private var _CrosshairXCoord:Number;
		
		private var _CrosshairYCoord:Number;
		
		
		public function HighlightContainerModel()
		{
			
		}
		
		private var _textMetricsCollection:ArrayCollection;
		

		[Bindable(event="textMetricsCollectionChange")]
		public function get textMetricsCollection():ArrayCollection
		{
			return _textMetricsCollection;
		}

		public function set textMetricsCollection(value:ArrayCollection):void
		{
			if (_textMetricsCollection != value)
			{
				_textMetricsCollection = value;
				dispatchEvent(new Event(TEXTMETRICSCOLLECTION_CHANGE_EVENT));
			}
		}

		public function get StartIndex():int
		{
			return _StartIndex;
		}

		public function set StartIndex(value:int):void
		{
			_StartIndex = value;
		}

		public function get EndIndex():int
		{
			return _EndIndex;
		}

		public function set EndIndex(value:int):void
		{
			_EndIndex = value;
		}

		public function get hightlightType():int
		{
			return _hightlightType;
		}

		public function set hightlightType(value:int):void
		{
			_hightlightType = value;
		}


		public function get CrosshairXCoord():Number
		{
			return _CrosshairXCoord;
		}

		public function set CrosshairXCoord(value:Number):void
		{
			_CrosshairXCoord = value;
		}

		public function get CrosshairYCoord():Number
		{
			return _CrosshairYCoord;
		}

		public function set CrosshairYCoord(value:Number):void
		{
			_CrosshairYCoord = value;
		}


		public function get SVG():String
		{
			return _SVG;
		}

		public function set SVG(value:String):void
		{
			_SVG = value;
		}


	}
}
