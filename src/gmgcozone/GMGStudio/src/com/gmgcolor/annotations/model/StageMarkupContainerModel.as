package com.gmgcolor.annotations.model
{
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;

/**
	 * 
	 * @author nathanvale
	 * 
	 */
	public class StageMarkupContainerModel extends BaseUIModel
	{
		
		public static const MARKUP_CHANGE_EVENT:String = "markUpChange";
		private var _markUp:Array;
		
		
		/**
		 * 
		 * 
		 */
		public function StageMarkupContainerModel()
		{
			super();
		}
		
		public function get markUp():Array
		{
			return _markUp;
		}
		
		public function set markUp(value:Array):void
		{
			if (_markUp != value)
			{
				_markUp = value;
				dispatchEvent(new Event(MARKUP_CHANGE_EVENT));
			}
		}
		
	}
}
