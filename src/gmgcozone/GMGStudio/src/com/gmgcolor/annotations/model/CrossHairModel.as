package com.gmgcolor.annotations.model
{
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;
import flash.geom.Point;

[Event(name="isPlacingChange",type="flash.events.Event")]
	[Event(name="localPointChange",type="flash.events.Event")]
	/**
	 * 
	 * @author nathanvale
	 * 
	 */
	public class CrossHairModel extends BaseUIModel
	{
		
		public static const ISPLACING_CHANGE_EVENT:String = "isPlacingChange";
		public static const LOCALPOINT_CHANGE_EVENT:String = "localPointChange";
		
		private var _isPlacing:Boolean;
		
		private var _globalPoint:Point;
		
		private var _localPoint:Point;
		
		/**
		 * 
		 * 
		 */
		public function CrossHairModel()
		{
			
		}
		
		[Bindable(event="isPlacingChange")]
		public function get isPlacing():Boolean
		{
			return _isPlacing;
		}

		public function set isPlacing(value:Boolean):void
		{
			if (_isPlacing != value)
			{
				_isPlacing = value;
				dispatchEvent(new Event(ISPLACING_CHANGE_EVENT));
			}
		}


		public function get globalPoint():Point
		{
			return _globalPoint;
		}

		public function set globalPoint(value:Point):void
		{
			_globalPoint = value;
		}


		[Bindable(event="localPointChange")]
		public function get localPoint():Point
		{
			return _localPoint;
		}

		public function set localPoint(value:Point):void
		{
			if (_localPoint != value)
			{
				_localPoint = value;
				xPos = value.x;
				yPos = value.y;
				dispatchEvent(new Event(LOCALPOINT_CHANGE_EVENT));
			}
		}


	}
}
