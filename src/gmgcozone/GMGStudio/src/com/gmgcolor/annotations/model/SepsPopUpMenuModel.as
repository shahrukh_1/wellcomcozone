package com.gmgcolor.annotations.model {
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;

import mx.collections.ArrayCollection;

[Event(name="sepsChange", type="flash.events.Event")]
	public class SepsPopUpMenuModel extends BaseUIModel {

		public static const SEPS_CHANGE_EVENT:String = "sepsChange";
		public static const SELECTED_SEPS_CHANGE_EVENT:String = "selectedSepsChange";
		private var _separations:ArrayCollection;
		private var _selectedSeparation:int;

		public function SepsPopUpMenuModel():void
		{
			super();
		}

		[Bindable(event="sepsChange")]
		public function get separations():ArrayCollection
		{
			return _separations;
		}

		public function set separations(value:ArrayCollection):void
		{
			if (_separations != value) {
				_separations = value;
				dispatchEvent(new Event(SEPS_CHANGE_EVENT));
			}
		}
		[Bindable(event="selectedSepsChange")]
		public function get selectedSeparation():int
		{
			return _selectedSeparation;
		}

		public function set selectedSeparation(value:int):void
		{
			if (_selectedSeparation != value) {
				_selectedSeparation = value;
				dispatchEvent(new Event(SELECTED_SEPS_CHANGE_EVENT));
			}
		}

	}
}
