package com.gmgcolor.annotations.model
{

import com.gmgcolor.annotations.presentation.CustomVideoPlayer;
import com.gmgcolor.application.domain.ApprovalPage;
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;

public class DocumentModel extends BaseUIModel
	{
		

		public static const HANDMODEON_CHANGE_EVENT:String = "handModeOnChange";
		public static const MARKUP_CHANGE_EVENT:String = "markupChange";
		public static const ISSELECTED_CHANGE_EVENT:String = "isSelectedChange";

        public static const CURRENTTIME_CHANGE_EVENT:String = "currentTimeChange";
        public static const VIDEOPLAYING_CHANGE_EVENT:String = "videoPlayingChange";
        public static const VIDEO_DURATION_CHANGE_EVENT:String = "videoDurationChange";
        public static const VIDEO_PLAYER_CHANGE_CHANGE_EVENT:String = "videoPlayerChange";

		private var _page:ApprovalPage;
        private var _isSelected:Boolean = true;
        private var _handModeOn:Boolean;
        private var _isMainViewSelected:Boolean;
        private var _isUserLocked:Boolean;
        private var _currentTimeFrame:Number = 0;
        private var _isVideoPlaying:Boolean = false;
        private var _videoDuration:Number = 0;
        private var _totalFrames:Number = 0;
        private var _videoPlayer:CustomVideoPlayer;

		public function DocumentModel()
		{
			
		}

		public function get handModeOn():Boolean
		{
			return _handModeOn;
		}

		public function set handModeOn(value:Boolean):void
		{
			if (_handModeOn != value)
			{
				_handModeOn = value;
				dispatchEvent(new Event(HANDMODEON_CHANGE_EVENT));
			}
		}


		public function get isSelected():Boolean
		{
			return _isSelected;
		}

		public function set isSelected(value:Boolean):void
		{
			if (_isSelected != value)
			{
				_isSelected = value;
				dispatchEvent(new Event(ISSELECTED_CHANGE_EVENT));
			}
		}

		public function get page():ApprovalPage
		{
			return _page;
		}

		public function set page(value:ApprovalPage):void
		{
			_page = value;
		}


		public function get isMainViewSelected():Boolean
		{
			return _isMainViewSelected;
		}

		public function set isMainViewSelected(value:Boolean):void
		{
			_isMainViewSelected = value;
		}


		public function get isUserLocked():Boolean
		{
			return _isUserLocked;
		}

		public function set isUserLocked(value:Boolean):void
		{
			_isUserLocked = value;
		}

        public function get currentTimeFrame():Number
        {
            return _currentTimeFrame;
        }

        public function set currentTimeFrame(value:Number):void
        {
            if(_currentTimeFrame != value)
            {
                _currentTimeFrame = value;
                dispatchEvent(new Event(CURRENTTIME_CHANGE_EVENT));
            }
        }

        public function get isVideoPlaying():Boolean
        {
            return _isVideoPlaying;
        }

        public function set isVideoPlaying(value:Boolean):void
        {
            if(_isVideoPlaying != value)
            {
                _isVideoPlaying = value;
                dispatchEvent(new Event(VIDEOPLAYING_CHANGE_EVENT));
            }
        }

        [Bindable(event="videoDurationChange")]
        public function get videoDuration():Number
        {
            return _videoDuration;
        }

        public function set videoDuration(value:Number):void
        {
            _videoDuration = value;
            dispatchEvent(new Event(VIDEO_DURATION_CHANGE_EVENT));
        }

        public function get videoPlayer():CustomVideoPlayer
        {
            return _videoPlayer;
        }

        public function set videoPlayer(value:CustomVideoPlayer):void
        {
            if(_videoPlayer != value)
            {
                _videoPlayer = value;
                dispatchEvent(new Event(VIDEO_PLAYER_CHANGE_CHANGE_EVENT));
            }
        }

        [Bindable]
        public function get totalFrames():Number
        {
            return _totalFrames;
        }

        public function set totalFrames(value:Number):void
        {
            _totalFrames = value;
        }
}
}
