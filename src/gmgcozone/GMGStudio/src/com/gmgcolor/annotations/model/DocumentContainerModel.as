package com.gmgcolor.annotations.model
{
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;
import flash.geom.Point;

[Event(name="localDocumentClickPointChange",type="flash.events.Event")]
	[Event(name="globalDocumentClickPointChange",type="flash.events.Event")]
	public class DocumentContainerModel extends BaseUIModel
	{
		public static const LOCALDOCUMENTCLICKPOINT_CHANGE_EVENT:String = "localDocumentClickPointChange";
		public static const GLOBALDOCUMENTCLICKPOINT_CHANGE_EVENT:String = "globalDocumentClickPointChange";
		
		private var _localDocumentClickPoint:Point;
		private var _globalDocumentClickPoint:Point;
		
		public function DocumentContainerModel()
		{
			super();
		}
		
		
		[Bindable(event="localDocumentClickPointChange")]
		public function get localDocumentClickPoint():Point
		{
			return _localDocumentClickPoint;
		}

		public function set localDocumentClickPoint(value:Point):void
		{
			if (_localDocumentClickPoint != value)
			{
				_localDocumentClickPoint = value;
				dispatchEvent(new Event(LOCALDOCUMENTCLICKPOINT_CHANGE_EVENT));
			}
		}

		[Bindable(event="globalDocumentClickPointChange")]
		public function get globalDocumentClickPoint():Point
		{
			return _globalDocumentClickPoint;
		}

		public function set globalDocumentClickPoint(value:Point):void
		{
			if (_globalDocumentClickPoint != value)
			{
				_globalDocumentClickPoint = value;
				dispatchEvent(new Event(GLOBALDOCUMENTCLICKPOINT_CHANGE_EVENT));
			}
		}

	}
}
