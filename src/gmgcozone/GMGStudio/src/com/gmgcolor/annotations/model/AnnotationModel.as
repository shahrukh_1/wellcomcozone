package com.gmgcolor.annotations.model {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;
import flash.events.EventDispatcher;

import mx.collections.ArrayCollection;

public class AnnotationModel extends EventDispatcher {
		public static const ALPHALISTS:Array = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
		public static const SELECTEDANNOTATION_CHANGE_EVENT:String = "selectedAnnotationChange";
		public static const ANNOTATIONS_CHANGE_EVENT:String = "annotationsChange";
		public static const SHOWALLANNOTATIONS_CHANGE_EVENT:String = "showAllAnnotationsChange";

		private var _selectedAnnotation:ApprovalAnnotationFO;

		private var _showAllAnnotations:Boolean;

		private var _annotations:ArrayCollection;

		/**
		 *
		 *
		 */
		public function AnnotationModel()
		{
			//annotations = dummyAnnotations;
		}


		[PublishSubscribe]
		[Bindable("selectedAnnotationChange")]
		public function get selectedAnnotation():ApprovalAnnotationFO
		{
			return _selectedAnnotation;
		}

		public function set selectedAnnotation(value:ApprovalAnnotationFO):void
		{
			_selectedAnnotation = value;
			dispatchEvent(new Event(SELECTEDANNOTATION_CHANGE_EVENT));
		}

		public function get childAnnotations():ArrayCollection
		{
			if (_selectedAnnotation)
				return new ArrayCollection();

			var i:int
			var annotations:Array = _annotations.source;
			var annotation:ApprovalAnnotationFO;
			var a:ArrayCollection = new ArrayCollection();

			for (i = 0; i < annotations.length; i++) {
				annotation = ApprovalAnnotationFO(annotations[i]);

				if (annotation.Parent > 0) {
					if (annotation.Parent == _selectedAnnotation.ID)
						a.addItem(annotation);
				}
			}
			return a;
		}


		public function updateCommentSequence(currentPageId:int):void
		{
			var i:int = 0;
			var parentAnnotation:ApprovalAnnotationFO;
			var count:int = 0;

			for (i = 0; i < annotations.source.length; i++) {
				parentAnnotation = ApprovalAnnotationFO(annotations.source[i]);

				if (parentAnnotation.Page == currentPageId && parentAnnotation.Parent == 0) {
					count++
					parentAnnotation.CommentNumber = count;
					annotations.itemUpdated(parentAnnotation);
				}
			}
			annotations.refresh();
		}

		[Bindable]
		/**
		 *
		 * @return
		 *
		 */
		public function get annotations():ArrayCollection
		{
			return _annotations;
		}

		/**
		 *
		 * @param value
		 *
		 */
		public function set annotations(value:ArrayCollection):void
		{
			if (_annotations != value) {
				_annotations = value;
				dispatchEvent(new Event(ANNOTATIONS_CHANGE_EVENT));
			}
		}

		/**
		 *
		 * @return
		 *
		 */
		public function get showAllAnnotations():Boolean
		{
			return _showAllAnnotations;
		}

		/**
		 *
		 * @param value
		 *
		 */
		public function set showAllAnnotations(value:Boolean):void
		{
			if (_showAllAnnotations != value) {
				_showAllAnnotations = value;
				dispatchEvent(new Event(SHOWALLANNOTATIONS_CHANGE_EVENT));
			}
		}


	}
}
