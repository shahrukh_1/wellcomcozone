package com.gmgcolor.annotations.enum
{
	public class AnnotationEnum
	{
		public static const ANNOTATION_TYPE_MARKER:int = 1;
		public static const ANNOTATION_TYPE_DRAW:int = 4;
		public static const ANNOTATION_TYPE_TEXT:int = 3;
		public static const ANNOTATION_TYPE_AREA:int = 2;
		public static const ANNOTATION_TYPE_ADD_TEXT:int = 5;
		
		public static const ANNOTATION_STATUS_APPROVED:int = 1;
		public static const ANNOTATION_STATUS_DECLINED:int = 2;
		public static const ANNOTATION_STATUS_NUETRAL:int = 3;
	}
}