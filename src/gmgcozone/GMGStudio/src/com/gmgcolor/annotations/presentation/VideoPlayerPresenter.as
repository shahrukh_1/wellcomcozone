package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.model.CrossHairModel;
import com.gmgcolor.annotations.model.DocumentContainerModel;

import flash.events.Event;
import flash.events.MouseEvent;

import mx.managers.CursorManager;

[InjectConstructor]
	public class VideoPlayerPresenter extends Presenter {


		[Bindable]
		[Embed(source="/assets/images/icons/37.png")]
		public var crosshairCursor:Class

		private var _cursorID:int;

		private var _videoPath:String;

        [Bindable]
        public var isPlacing:Boolean;

		[Bindable]
		public var colourPickerEnabled:Boolean;

        [Bindable]
        public var highlightEnabled:Boolean;

		[Bindable]
		public var markupEnabled:Boolean;

		private var _crossHairModel:CrossHairModel;

		public function VideoPlayerPresenter(model:DocumentContainerModel, crossHairModel:CrossHairModel)
		{
            //this._model = model;
            this._crossHairModel = crossHairModel;
            this._crossHairModel.addEventListener(CrossHairModel.ISPLACING_CHANGE_EVENT, onIsPlacingChange);
		}

        public function onMouseMove(event:Event):void
        {
            if (_crossHairModel.isPlacing) {
                CursorManager.removeAllCursors();
                _cursorID = CursorManager.setCursor(crosshairCursor, 2, -8, -8);
            }
        }

        public function onMouseOut(event:MouseEvent):void
        {
            CursorManager.removeAllCursors();
        }


		[MessageHandler(selector="disable", scope="global")]
		public function onDisableMarkup(value:MarkupEvent):void
		{
			markupEnabled = false;
		}

		[MessageHandler(selector="enable", scope="global")]
		public function onEnableMarkup(value:MarkupEvent):void
		{
			markupEnabled = true;
		}

		[MessageHandler(selector="newVideoDocument")]
		public function onNewSWF(event:DocumentEvent):void
		{
			videoPath = event.value;
		}

        private function onIsPlacingChange(event:Event):void
        {
            isPlacing = _crossHairModel.isPlacing;
        }

		[Bindable]
		public function get videoPath():String
		{
			return _videoPath;
		}

		public function set videoPath(value:String):void
		{
			if (value != _videoPath) {
                _videoPath = value;
			}
		}
	}
}
