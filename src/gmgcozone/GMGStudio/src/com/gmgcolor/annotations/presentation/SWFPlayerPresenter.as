package com.gmgcolor.annotations.presentation {

import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.model.CrossHairModel;
import com.gmgcolor.annotations.model.DocumentContainerModel;
import com.gmgcolor.annotations.model.DocumentModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.UploadRecordedSWFEvent;
import com.gmgcolor.application.pages.events.SWFPlayerEvent;
import com.gmgcolor.application.pages.events.VideoControlsEvent;
import com.gmgcolor.application.pages.events.VideoStateChangeEvent;
import com.gmgcolor.application.pages.events.VideoTimeChangeEvent;

import flash.display.AVM1Movie;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.ProgressEvent;
import flash.media.SoundTransform;
import flash.net.URLRequest;
import flash.system.LoaderContext;
import flash.system.Security;
import flash.utils.ByteArray;

import leelib.util.flvEncoder.ByteArrayFlvEncoder;

import mx.controls.SWFLoader;
import mx.core.mx_internal;
import mx.events.SliderEvent;
import mx.managers.CursorManager;

import org.libspark.utils.ForcibleLoader;

use namespace mx_internal;

[InjectConstructor]
	public class SWFPlayerPresenter extends Presenter {


		[Bindable]
		[Embed(source="/assets/images/icons/37.png")]
		public var crosshairCursor:Class;

		private var _cursorID:int;

		private var _swfPath:String;

        [Inject]
        [Bindable]
        public var securityManager:SecurityManager;

        [Inject]
        public var approvalModel:ApprovalModel;

        [Bindable]
        public var isPlacing:Boolean;

		[Bindable]
		public var colourPickerEnabled:Boolean;

        [Bindable]
        public var highlightEnabled:Boolean;

		[Bindable]
		public var markupEnabled:Boolean;

		private var _crossHairModel:CrossHairModel;

        private var _view:SWFPlayer;

        private var _movie:*;

        public var currentFrame:uint = 0;

        public var totalFrames:uint = 0;

        public var isPlaying:Boolean = false;

        [Bindable]
        public var currentTime:Number = 0;

        public var frameRate:Number = 0;

        [Bindable]
        public var totalTime:Number = 0;

        [Inject]
        public var documentModel:DocumentModel;

        [Bindable]
        public var loaderContext:LoaderContext;

        private var bitmapdata:BitmapData;
        private var baFlvEncoder:ByteArrayFlvEncoder;
        private var loader:Loader;
        private var count:int = 0;
        private var recording:Boolean = false;
        private var recorder:SWFLoader;
        private var container:SWFLoader;
        private var SWF:ByteArray;

        public static const SWFLOADER_FILE:String = 'SWFLoader.swf';
		public function SWFPlayerPresenter(model:DocumentContainerModel, crossHairModel:CrossHairModel)
		{
            //this._model = model;
            this._crossHairModel = crossHairModel;
            this._crossHairModel.addEventListener(CrossHairModel.ISPLACING_CHANGE_EVENT, onIsPlacingChange);
		}

        public function onMouseMove(event:Event):void
        {
            if (_crossHairModel.isPlacing) {
                CursorManager.removeAllCursors();
                _cursorID = CursorManager.setCursor(crosshairCursor, 2, -8, -8);
            }
        }

        public function onMouseOut(event:MouseEvent):void
        {
            CursorManager.removeAllCursors();
        }


		[MessageHandler(selector="disable", scope="global")]
		public function onDisableMarkup(value:MarkupEvent):void
		{
			markupEnabled = false;
		}

		[MessageHandler(selector="enable", scope="global")]
		public function onEnableMarkup(value:MarkupEvent):void
		{
			markupEnabled = true;
		}

		[MessageHandler(selector="newSWFDocument")]
		public function onNewSWF(event:DocumentEvent):void
		{
            loaderContext = new LoaderContext(true);
            loaderContext.checkPolicyFile = false;
            container = new SWFLoader();
            container.loaderContext = loaderContext;
            container.addEventListener(Event.COMPLETE, onContainerLoadComplete);
            container.load(getDomain(event.value) + '/' + SWFLOADER_FILE);
			swfPath = event.value;
		}

        private function onContainerLoadComplete(event:Event):void
        {
            var wrapperSWF:* = event.target.content;
            wrapperSWF.addEventListener(ProgressEvent.PROGRESS, onPlayerProgress);
            wrapperSWF.addEventListener('loadFileComplete', function(e:Event):void {
                SWF = e.target.getLoadedFile();
                view.swfLoader.loaderContext = loaderContext;
                view.swfLoader.source = SWF;
            });
            wrapperSWF.loadFile(swfPath);
        }

        private function onIsPlacingChange(event:Event):void
        {
            isPlacing = _crossHairModel.isPlacing;
        }

		[Bindable]
		public function get swfPath():String
		{
			return _swfPath;
		}

		public function set swfPath(value:String):void
		{
			if (value != _swfPath) {
                _swfPath = value;
			}
		}

        private static function getDomain(url:String):String
        {
            var parts:Array = url.split('/');
            return parts.slice(0, 3).join('/');
        }

        public function onPlayerLoadComplete(event:Event):void
        {
            if (event.target.content is AVM1Movie) {
                loader = new Loader();
                loader.contentLoaderInfo.addEventListener(Event.COMPLETE, as2swfComplete);
                var fLoader:ForcibleLoader = new ForcibleLoader(loader);
                var as2ByteArray:ByteArray = new ByteArray();
                SWF.readBytes(as2ByteArray);
                fLoader.loadBytes(as2ByteArray);
                view.as2SWFContainer.addChild(loader);
                view.as2SWFContainer.visible = true;
            }
            else
            {
                movie = event.target.content as MovieClip;
            }
        }

        private function as2swfComplete(event:Event):void
        {
            view.swfLoader.unloadAndStop(true);
            movie = event.currentTarget.content as MovieClip;
        }

        public function registerPlayer(value:SWFPlayer):void
        {
            view = value;
            view.swfLoader.addEventListener(Event.COMPLETE, onPlayerLoadComplete);
        }

        private function onPlayerProgress(event:ProgressEvent):void
        {
            view.scrubBar.loadedRangeArea.width = event.bytesLoaded / event.bytesTotal * view.scrubBar.loadedRangeArea.parent.width;
        }

        public function set view(player:SWFPlayer):void
        {
            _view = player;
        }

        public function get view():SWFPlayer
        {
            return _view;
        }

        [Bindable]
        public function get movie():*
        {
            return _movie;
        }

        public function set movie(value:*):void
        {
            _movie = value;
            var st:SoundTransform = new SoundTransform();
            st.volume = 0;
            _movie.soundTransform = st;
            totalFrames = _movie.totalFrames;
            currentFrame = 1;
            isPlaying = _movie.isPlaying;
            frameRate = _movie.loaderInfo.frameRate;
            totalTime = totalFrames / frameRate;
            documentModel.totalFrames = totalFrames;
            documentModel.videoDuration = totalTime;
            _movie.gotoAndStop(1);
            view.sizerRect.width = _movie.loaderInfo.width;
            view.sizerRect.height = _movie.loaderInfo.height;
            view.swfLoader.width = _movie.loaderInfo.width;
            view.swfLoader.height = _movie.loaderInfo.height;
            if(totalTime >= 1)
            {
                view.scrubBar.minimum = 0;
                view.scrubBar.maximum = totalTime;
                view.scrubBar.value = 0;

                _movie.addEventListener(Event.ENTER_FRAME, onEnterFrame);
                _movie.addEventListener(Event.ENTER_FRAME, onExitFrame);
                view.scrubBar.snapInterval = totalTime / 10 < 1 ? 0.1 : 1;
                view.scrubBar.addEventListener(SliderEvent.CHANGE, onScrubbarChange);
                view.scrubBar.addEventListener(SliderEvent.THUMB_DRAG, onScrubbarChange);
            }
            else
            {
                view.swfLoader.source = null;
                view.swfLoader.unloadAndStop(true);
                _movie = null;
                if(loader)
                {
                    loader.unloadAndStop(true);
                    view.as2SWFContainer.removeChild(loader);
                }
                view.playersContainer.removeElement(view.swfLoader);
                view.playersContainer.removeElement(view.as2SWFContainer);
                displayRecordControls();
            }
        }

        private function onExitFrame(event:Event):void
        {
            currentFrame = event.target.currentFrame;
            if(currentFrame == totalFrames)
            {
                movie.nextFrame();
            }
        }

        private function displayRecordControls():void
        {
            view.alert.visible = true;
            dispatcher(new VideoControlsEvent(VideoControlsEvent.HIDE_CONTROLS, null));
            dispatcher(new VideoControlsEvent(VideoControlsEvent.DISPLAY_RECORD_CONTROLS, null));
        }

        [MessageHandler(selector="swfRecordStart", scope="global")]
        public function onRecordStart(event:SWFPlayerEvent):void
        {
            reloadSWFLoader();
            view.alert.visible = false;
        }

        [MessageHandler(selector="swfRecordStop", scope="global")]
        public function onRecordStop(event:SWFPlayerEvent):void
        {
            recorder.content.removeEventListener(Event.ENTER_FRAME, onFrameGetBitmapData);
            baFlvEncoder.updateDurationMetadata();
            if(loader)
            {
                loader.unloadAndStop(true);
            }
            recorder.source = null;
            recorder.unloadAndStop(true);
            view.playersContainer.removeElement(recorder);
            view.recordingComplete.visible = true;
            dispatcher(new UploadRecordedSWFEvent(UploadRecordedSWFEvent.UPLOAD, securityManager.sessionId, approvalModel.currentApprovalId, baFlvEncoder.byteArray));
            baFlvEncoder.kill();
            view.alert.visible = false;
        }

        private function reloadSWFLoader():void
        {
            recorder = view.recorderLoader;
            recorder.width = view.sizerRect.width;
            recorder.height = view.sizerRect.height;
            recorder.addEventListener(Event.COMPLETE, function(event:Event):void {
                saveFLV();
            });
            recorder.source = SWF;
        }

        private function displayErrorAndUnload():void
        {
            if(loader)
            {
                loader.unloadAndStop(true);
            }
            view.swfLoader.unloadAndStop(true);

            dispatcher(new VideoControlsEvent(VideoControlsEvent.HIDE_CONTROLS, null));
            view.alert.visible = true;
        }

        [MessageHandler(selector="hideControls")]
        public function onHideControls(event:VideoControlsEvent):void
        {
            view.playerControls.visible = false;
        }

        private function saveFLV():void
        {
            bitmapdata = new BitmapData(recorder.parent.width, recorder.parent.height);
            baFlvEncoder = new ByteArrayFlvEncoder(frameRate);
            baFlvEncoder.setVideoProperties(recorder.parent.width, recorder.parent.height);
            baFlvEncoder.start();
            recorder.content.addEventListener(Event.ENTER_FRAME, onFrameGetBitmapData);
        }

        private function onFrameGetBitmapData(event:Event):void
        {
            if(baFlvEncoder.byteArray.length > 1000000000)
            {
                dispatcher(new SWFPlayerEvent(SWFPlayerEvent.SWF_RECORD_STOP));
            }
            else
            {
                bitmapdata.draw(recorder.content);
                baFlvEncoder.addFrame(bitmapdata, null);
            }
        }

        private function onScrubbarChange(event:Event):void
        {
            seek(event.target.value * frameRate);
        }

        private function onEnterFrame(event:Event):void
        {
            var previousTime:Number = currentTime;
            currentFrame = event.target.currentFrame;
            currentTime = currentFrame/totalFrames * totalTime;
            if(previousTime != currentTime)
            {
                dispatcher(new VideoTimeChangeEvent(VideoTimeChangeEvent.VIDEO_TIME_CHANGE, currentTime, null));
                dispatcher(new SWFPlayerEvent(SWFPlayerEvent.SWF_FRAME_CHANGE, currentFrame));
                updateThumbMarker();
            }
        }

        private function updateThumbMarker():void
        {
            view.scrubBar.value = currentTime;
        }

        public function dispatchPlayingChange():void
        {
            dispatcher(new VideoStateChangeEvent(VideoStateChangeEvent.VIDEO_STATE_CHANGE, movie.isPlaying ? 'playing' : 'paused', null));
        }

        public function dispatchSeek(frame:uint):void
        {
            dispatcher(new SWFPlayerEvent(SWFPlayerEvent.SWF_SEEK, frame));
        }

        public function play():void
        {
            if(movie && !movie.isPlaying)
            {
                movie.play();
                dispatchPlayingChange();
            }
        }

        public function stop():void
        {
            if(movie && movie.isPlaying)
            {
                movie.stop();
//                stopAllClips(movie, true);
                dispatchPlayingChange();
            }
        }

        public function goToStart():void
        {
            if(movie)
            {
                movie.gotoAndStop(1);
                dispatchSeek(1);
                dispatchPlayingChange();
            }
        }

        public function goToEnd():void
        {
            if(movie)
            {
                movie.gotoAndStop(movie.totalFrames);
                dispatchSeek(movie.totalFrames);
                dispatchPlayingChange();
            }
        }

        public function seek(frame:uint):void
        {
            if(movie)
            {
                if(movie.isPlaying)
                {
                    movie.gotoAndPlay(frame);
                }
                else
                {
                    movie.gotoAndStop(frame);
                }
                dispatchSeek(frame);
            }
        }

        public function formatTimeValue(value:Number):String
        {
            // default format: hours:minutes:seconds
            value = Math.round(value);

            var hours:uint = Math.floor(value/3600) % 24;
            var minutes:uint = Math.floor(value/60) % 60;
            var seconds:uint = value % 60;

            var result:String = "";
            if (hours != 0)
                result = hours + ":";

            if (result && minutes < 10)
                result += "0" + minutes + ":";
            else
                result += minutes + ":";

            if (seconds < 10)
                result += "0" + seconds;
            else
                result += seconds;

            return result;
        }

        public static function stopAllClips(mc:MovieClip,recursive:Boolean = false):void
        {
            var n:int = mc.numChildren;
            for (var i:int=0;i<n;i++) {
                var clip:MovieClip = mc.getChildAt(i) as MovieClip;
                if (clip)
                {
                    clip.gotoAndStop(1);
                    if(recursive) stopAllClips(clip,true);
                }
            }
        }

        public static function startAllClips(mc:MovieClip,recursive:Boolean = false):void
        {
            var n:int = mc.numChildren;
            for (var i:int=0;i<n;i++) {
                var clip:MovieClip = mc.getChildAt(i) as MovieClip;
                if (clip)
                {
                    clip.play();
                    if(recursive) startAllClips(clip,true);
                }
            }
        }
}
}
