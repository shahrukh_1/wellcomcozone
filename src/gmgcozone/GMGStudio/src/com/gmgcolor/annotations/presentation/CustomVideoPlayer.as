package com.gmgcolor.annotations.presentation {
import mx.core.mx_internal;

import org.osmf.media.MediaPlayer;

import spark.components.VideoPlayer;

use namespace mx_internal;

    public class CustomVideoPlayer extends VideoPlayer
    {

        [SkinPart(required="true")]
        public var pins:Pins;

        public function CustomVideoPlayer()
        {
            super();
        }

        public function get mediaPlayer():MediaPlayer
        {
            return videoDisplay.mx_internal::videoPlayer;
        }

    }
}
