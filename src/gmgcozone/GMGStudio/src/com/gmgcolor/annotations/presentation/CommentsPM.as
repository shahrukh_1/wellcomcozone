package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.domain.ApprovalCollaborator;
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.annotations.event.ReplyCommentEvent;
import com.gmgcolor.annotations.event.SideBarAnnotationSelectEvent;
import com.gmgcolor.annotations.event.UpdateCommentHeightEvent;
import com.gmgcolor.annotations.model.CommentsModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.enums.ApplicationEnum;
import com.gmgcolor.application.event.AnnotationSelectedIndexChange;
import com.gmgcolor.application.event.DeleteApprovalAnnotationEvent;
import com.gmgcolor.application.event.IsLockedChangeEvent;
import com.gmgcolor.application.event.UpdateApprovalAnnotationEvent;
import com.gmgcolor.application.model.CollaboratorModel;
import com.gmgcolor.components.CommentInputTextSkin;
import com.gmgcolor.components.GripSkin;
import com.gmgcolor.model.BaseUIModel;

import flash.display.DisplayObject;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.MouseEvent;
import flash.events.ProgressEvent;
import flash.geom.Rectangle;
import flash.net.FileReference;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import mx.collections.ArrayCollection;
import mx.collections.ListCollectionView;
import mx.controls.Alert;
import mx.events.MoveEvent;
import mx.events.ResizeEvent;

import spark.components.CheckBox;
import spark.components.SkinnableContainer;

[InjectConstructor]
	public class CommentsPM extends BaseUIPM {

		public static const REPLY_HEADER_STATE_NORMAL:String = "normal";
		public static const REPLY_HEADER_STATE_EDIT:String = "edit";

		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var collaboratorModel:CollaboratorModel;

		[Inject]
		[Bindable]
		public var securityManager:SecurityManager;

		[MessageDispatcher]
		public var dispatcher:Function;

		private var _repliesCollection:ArrayCollection;

		[Bindable]
		public var moveable:Boolean;

		private var _annotation:ApprovalAnnotationFO;

		[Bindable]
		public var labelComment:String = "Test Comment";

		private var _commentIndex:int;

		[Bindable]
		public var currentState:String = STATE_POST;

		[Bindable]
		public var popUpState:String = "normal";

		[Bindable]
		public var browseText:String;

		[Bindable]
		public var showFilenameBox:Boolean;

		[Bindable]
		public var enabledPrevBtn:Boolean;

		[Bindable]
		public var enabledNextBtn:Boolean;

		public static const STATE_POST:String = "post";
		public static const STATE_REPLY:String = "reply";


		public var line:SkinnableContainer;
		public var fileRef:FileReference;

		private var _view:Comments;

		[Bindable]
		public var minWidth:Number;

		[Bindable]
		public var commentLabelWidth:Number;

		[Bindable]
		public var commentReadWidth:Number;

		[Bindable]
		public var minHeight:Number;

		[Bindable]
		public var footerHeight:Number;

		[Bindable]
		public var headerHeight:Number = 32;

		[Bindable]
		public var inputNoteHeight:Number;

		[Bindable]
		public var inputComment:String;

		[Bindable]
		public var annotationsCollection:ListCollectionView = new ListCollectionView;

		[Bindable]
		public var privateCollaborators:ListCollectionView = new ListCollectionView;

		[Bindable]
		public var getDateTimeFormat:String;


		private var startDragging:Boolean;
		private var startMouseX:Number;
		private var startMouseY:Number;
		private var startWidth:Number;
		private var startHeight:Number;
		private var startX:Number;
		private var startY:Number;
		private var moving:Object;


		private var moveOverCommentUI:Boolean;

		[Bindable]
		public var submittable:Boolean

		public function CommentsPM(commentsModel:CommentsModel)
		{

			this._model = commentsModel;
			_model.addEventListener(BaseUIModel.ISON_CHANGE_EVENT, onIsOnChange);
			_model.addEventListener(CommentsModel.CHILDANNOTATIONS_CHANGE_EVENT, onChildAnnotationsChange);
			_model.addEventListener(CommentsModel.ANNOTATION_CHANGE_EVENT, onAnnotationChange);
			_model.addEventListener(CommentsModel.ANNOTATIONS_CHANGE_EVENT, onAnnotationsChange);
			_model.addEventListener(CommentsModel.INPUTTEXT_CHANGE_EVENT, onInputTextChange);
			_model.addEventListener(CommentsModel.SUBMITTABLE_CHANGE_EVENT, onSumbitChange);

			super();

		}

		override public function set height(value:Number):void
		{
			super.height = value;
		}


		public function onDeleteAnnotation(event:MouseEvent, id:int):void
		{
			dispatcher(new DeleteApprovalAnnotationEvent(DeleteApprovalAnnotationEvent.deleteApprovalAnnotation, securityManager.sessionId, id));

			var a:Array = repliesCollection.list.toArray();
			var index:int;

			if (repliesCollection && repliesCollection.length > 0) {
				/*				for each (var ap:ApprovalAnnotationFO in repliesCollection.list)
				 {
				 if (ap.ID == id)
				 {
				 index++
				 break;
				 }
				 index++
				 }
				 repliesCollection.list.removeItemAt(index);*/
			}
			else {
				dispatcher(new CommentsEvent(CommentsEvent.CANCEL));
			}
		}

		public function onUpdateAnnotation(event:MouseEvent, annotation:ApprovalAnnotationFO):void
		{
			tempComment = annotation.Comment;
			annotation.isEditMode = REPLY_HEADER_STATE_EDIT;
			currentEditAnnotation = annotation;
		}

		private var tempComment:String;

		public function onSaveEditAnnotation(event:Event, annotation:ApprovalAnnotationFO):void
		{

			annotation.isEditMode = REPLY_HEADER_STATE_NORMAL;
			dispatcher(new UpdateApprovalAnnotationEvent(UpdateApprovalAnnotationEvent.updateApprovalAnnoation, securityManager.sessionId, annotation));
		}

		public function onCancelEditAnnotation(event:Event, annotation:ApprovalAnnotationFO):void
		{
			annotation.Comment = tempComment;
			annotation.isEditMode = REPLY_HEADER_STATE_NORMAL;
		}

		public function getApprovalCollaboratorName(id:int):String
		{
			if (id == securityManager.accountUserInfo.UserID) {
				return securityManager.accountUserInfo.UserGivenName + " " + securityManager.accountUserInfo.UserFamilyName;
			}
			else {
				return collaboratorModel.getApprovalCollaboratorName(id);
			}
		}

		public function getApprovalCollaboratorAvatar(id:int):String
		{
			if (id == securityManager.accountUserInfo.UserID) {
				return securityManager.accountUserInfo.UserAvatar;
			}
			else {
				return collaboratorModel.getApprovalCollaboratorAvatar(id);
			}
		}

		[MessageHandler]
		public function onIsLockaedChangeEvent(event:IsLockedChangeEvent):void
		{
			submittable = CommentsModel(_model).submittable && !event.isLocked;
		}

		private function onSumbitChange(event:Event):void
		{
			submittable = CommentsModel(_model).submittable && !approvalModel.currentApproval.IsLocked;
		}

		private function onInputTextChange(event:Event):void
		{
			inputComment = CommentsModel(_model).inputText;
		}

		public function onPreviousBtnClick(event:MouseEvent):void
		{
			dispatcher(new CommentsEvent(CommentsEvent.PREVIOUS));
		}

		public function onNextBtnClick(event:MouseEvent):void
		{
			dispatcher(new CommentsEvent(CommentsEvent.NEXT));
		}

		public function onMouseDownOutsideHandler(event:Event):void
		{
			popUpState = 'normal';
		}

		private function filterAnnotations(item:ApprovalAnnotationFO):Boolean
		{

			return item.Parent == 0;
		}

		private function onAnnotationsChange(event:Event):void
		{
			var annotations:ArrayCollection;

			annotations = CommentsModel(_model).annotations;

			if (annotations) {
				annotationsCollection = new ListCollectionView(annotations);
				annotationsCollection.filterFunction = filterAnnotations;
				annotationsCollection.refresh();
			}
		}

		private function onAnnotationChange(event:Event):void
		{
			annotation = CommentsModel(_model).annotation;
			enableButtons(annotationsCollection.getItemIndex(annotation));
		}

		private function onChildAnnotationsChange(event:Event):void
		{
			repliesCollection = CommentsModel(_model).childAnnotations;
		}


		[Init]
		override public function init():void
		{
			minWidth = ApplicationEnum.DEFAULT_COMMENT_WIDTH;
			startWidth = ApplicationEnum.DEFAULT_COMMENT_WIDTH;
			width = ApplicationEnum.DEFAULT_COMMENT_WIDTH;

			collaboratorModel.addEventListener("collaboratorsChange", onCollaboratorsChange);
			privateCollaborators = new ListCollectionView(collaboratorModel.currentCollaborators);

			super.init();
		}

		[MessageHandler(scope="global")]
		public function onSideBarAnnotationClick(event:SideBarAnnotationSelectEvent):void
		{
			commentIndex = annotationsCollection.getItemIndex(event.annotation);
		}

		[MessageHandler(scope="global")]
		public function onAnnotationSelectedIndexChange(event:AnnotationSelectedIndexChange):void
		{
			commentIndex = event.selectedIndex;
			CommentsModel(_model).selectedIndex = commentIndex;
		}

		[MessageHandler(scope="global")]
		public function onShowCommentEvent(event:ShowCommentEvent):void
		{
			_view.setMinimumHeight();
		}

		private function onCollaboratorsChange(event:Event):void
		{
			privateCollaborators = new ListCollectionView(collaboratorModel.currentCollaborators);
		}

		public function registerView(view:Comments):void
		{
			this._view = view;
			this._view.addEventListener(MoveEvent.MOVE, onMove);
			this._view.addEventListener(ResizeEvent.RESIZE, onResize);
			onResize();
		}

		public function onResize(event:ResizeEvent = null):void
		{
			_model.width = _view.width;
			_model.height = _view.height;
		}

		private var currentEditAnnotation:ApprovalAnnotationFO;

		public function onCloseBtnClick(event:MouseEvent):void
		{
			_model.isOn = false;
			dispatcher(new CommentsEvent(CommentsEvent.CANCEL));

			if (currentEditAnnotation)
				currentEditAnnotation.isEditMode = REPLY_HEADER_STATE_NORMAL;
		}

		private var isPrivate:Boolean;

		public function checkbox1_clickHandler(event:MouseEvent):void
		{
			isPrivate = CheckBox(event.target).selected;

			if (isPrivate)
				popUpState = "open";
			else {
				for each (var approvalCollaborator:ApprovalCollaborator in privateCollaborators) {
					approvalCollaborator.isChecked = false;
				}
			}
		}

		[MessageHandler(scope="global")]
		public function onUpdateCommentHeightEvent(event:UpdateCommentHeightEvent):void
		{
			_view.updateHeight();
		}

		public function onBrowseClickHandler(event:MouseEvent):void
		{
			fileRef = new FileReference();
			fileRef.addEventListener(Event.SELECT, fileRef_select, false, 0, true);
			fileRef.addEventListener(Event.CANCEL, fileRef_cancel, false, 0, true);
			fileRef.addEventListener(IOErrorEvent.IO_ERROR, fileRef_error, false, 0, true);
			fileRef.addEventListener(ProgressEvent.PROGRESS, progressHandler, false, 0, true);
			fileRef.addEventListener(Event.COMPLETE, fileRef_complete, false, 0, true);
			fileRef.browse();
		}

		public function startDownload():void
		{
			var req:URLRequest = new URLRequest(annotation.attachmentFilename);
			trace("getURL", annotation.attachmentFilename);
			try {
				navigateToURL(req, "_blank");
			}
			catch (e:Error) {
				trace("Navigate to URL failed", e.message);
			}
		}

		public function fileRef_select(evt:Event):void
		{
			try {
				browseText = fileRef.name;
			}
			catch (err:Error) {
				Alert.show("ERROR: zero-byte file");
			}
		}

		public function fileRef_error(evt:ErrorEvent):void
		{
			Alert.show(evt.text);
		}

		public function fileRef_cancel(evt:Event):void
		{
			fileRef = null;
		}

		public function fileRef_progress(evt:ProgressEvent):void
		{
			// progressBar.setProgress(Number(evt.bytesLoaded), Number(evt.bytesTotal));
		}

		public function progressHandler(event:ProgressEvent):void
		{
			var file:FileReference = FileReference(event.target);
			//progressBar.setProgress( Number(event.bytesLoaded), Number(event.bytesTotal));
		}

		public function fileRef_complete(evt:Event):void
		{
			trace("loaded!");
		}

		public function onCancelClickHandler(event:MouseEvent):void
		{
			_model.isOn = false;
			dispatcher(new CommentsEvent(CommentsEvent.CANCEL));

			if (currentEditAnnotation)
				currentEditAnnotation.isEditMode = REPLY_HEADER_STATE_NORMAL;
		}

		public function removeView():void
		{
			this._view = null;
			_systemManager.removeEventListener(Event.RESIZE, onApplicationResize);
		}

		public function onPostClickHandler(event:MouseEvent):void
		{
			if (annotation) {
				dispatcher(new ReplyCommentEvent(ReplyCommentEvent.REPLY_COMMENT, inputComment));
			}
			else {
				_model.isOn = false;

				var a:Array = [];

				for each (var approvalCollaborator:ApprovalCollaborator in privateCollaborators) {
					if (approvalCollaborator.isPrivateSelected)
						a.push(approvalCollaborator);
				}

				dispatcher(new NewCommentEvent(NewCommentEvent.NEW_COMMENT, inputComment, isPrivate, a));
			}
			inputComment = "";
		}

		private function onApplicationResize(event:Event):void
		{
			//checkCommentInBounds()
		}

		public function onMouseDown(event:MouseEvent):void
		{
			if (event.target is GripSkin || event.target is CommentInputTextSkin)
				return;

			var obj:Object = Object(event.target);

			while (obj != null) {
				var name:String = obj["name"] ? obj["name"] : "";
				obj = obj.parent;
				if (obj is CommentInputTextSkin)
					return;
			}

			_systemManager.addEventListener(MouseEvent.MOUSE_UP, onSystemMouseUp);
			moveOverCommentUI = true;
			moving = this;
			_view.startDrag(false, new Rectangle(1, 1, _view.parent.width - width - 2, _view.parent.height - height - 2));

			startDragging = true;

			moving = DisplayObject(event.target);

			if (moving is GripSkin) {
				startMouseX = event.stageX;
				startMouseY = event.stageY;
				startWidth = _view.width;
				startHeight = _view.height;
				startX = _view.x;
				startY = _view.y;
			}
			else if (moving is Comments) {
				_model.xPos = _view.x;
				_model.yPos = _view.y;
			}

			_systemManager.addEventListener(MouseEvent.MOUSE_MOVE, onSystemMouseMove);
			_systemManager.addEventListener(MouseEvent.MOUSE_UP, onSystemMouseUp);
		}


		public function onSystemMouseUp(event:MouseEvent):void
		{
			startDragging = false;
			_systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, onSystemMouseMove, false);
			_systemManager.removeEventListener(MouseEvent.MOUSE_UP, onSystemMouseUp, false);
			_view.stopDrag();
		}

		public function onSystemMouseMove(event:MouseEvent):void
		{

			_view.dispatchEvent(new Event(Event.CHANGE));


			if (startDragging) {

				_model.xPos = _view.x;
				_model.yPos = _view.y;
				dispatcher(new CommentsEvent(CommentsEvent.MOVE));
			}
		}

		public function onMove(event:MoveEvent):void
		{
			_model.xPos = _view.x;
			_model.yPos = _view.y;
		}

		public function onFileDownloadButtonClick(event:MouseEvent):void
		{
			startDownload();
		}

		[Bindable]
		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public function set annotation(value:ApprovalAnnotationFO):void
		{
			if (value) {
				currentState = STATE_REPLY;
			}
			else {
				currentState = STATE_POST;
			}

			if (value)
				showFilenameBox = value.attachmentFilename == null ? false : true;

			_annotation = value;

		}


		[Bindable]
		public function get repliesCollection():ArrayCollection
		{
			return _repliesCollection;
		}

		public function set repliesCollection(value:ArrayCollection):void
		{
			if (value)
				currentState = STATE_REPLY;
			else
				currentState = STATE_POST;
			_repliesCollection = value;
		}

		private function onIsOnChange(event:Event):void
		{
			if (_model.isOn)
				inputComment = "";
		}

		[Bindable]
		public function get commentIndex():int
		{
			return _commentIndex;
		}

		public function set commentIndex(value:int):void
		{
			_commentIndex = value;
			enableButtons(value);
		}

		private function enableButtons(value:int):void
		{
			enabledPrevBtn = false;
			enabledNextBtn = false;

			if (value > 0)
				enabledPrevBtn = true;

			if (value < annotationsCollection.length - 1)
				enabledNextBtn = true;
		}


	}
}
