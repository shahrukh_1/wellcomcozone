package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.SepsChangeEvent;
import com.gmgcolor.annotations.model.SepsPopUpMenuModel;
import com.gmgcolor.application.domain.ApprovalPage;
import com.gmgcolor.application.domain.ApprovalSeparationPlateFO;

import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.collections.ListCollectionView;
import mx.core.FlexGlobals;

[InjectConstructor]
	public class SepsPopUpMenuPM extends BaseUIPM {

		[Bindable]
		public var selectedSeparationIndex:int = 0;

		private var _currentState:String;

		[Bindable]
		public var separations:ListCollectionView;

		[MessageDispatcher]
		public var dispatcher:Function;

		public function SepsPopUpMenuPM(sepsPopUpMenuModel:SepsPopUpMenuModel)
		{
			_model = sepsPopUpMenuModel;
			//_model.addEventListener(SepsPopUpMenuModel.SEPS_CHANGE_EVENT, onSepsChange);
			//_model.addEventListener(SepsPopUpMenuModel.SELECTED_SEPS_CHANGE_EVENT, onSelectedSepsChange);
			super();
		}

		[Init]
		override public function init():void
		{
			super.init();
			_systemManager = FlexGlobals.topLevelApplication.systemManager.getSandboxRoot();
		}

		[MessageHandler(selector="selected")]
		public function onDocumentSelected(e:DocumentEvent):void
		{
			var page:ApprovalPage = DocumentPM(e.value).page;
			if(page) {
				separations = new ArrayCollection(page.ApprovalSeparationPlates);
				selectedSeparationIndex = page.SelectedApprovalSeparationIndex;
			}
		}

		public function itemrenderer1_clickHandler(event:Event, approvalSeparationPlateFO:ApprovalSeparationPlateFO):void
		{
			selectedSeparationIndex = separations.getItemIndex(approvalSeparationPlateFO);
			dispatcher(new SepsChangeEvent(SepsChangeEvent.CHANGE, approvalSeparationPlateFO, selectedSeparationIndex));
		}

		[Bindable]
		public function get currentState():String
		{
			return _currentState;
		}

		public function set currentState(value:String):void
		{
			_currentState = value;
		}
	}
}
