package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.HighlightEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.event.ScaleEvent;
import com.gmgcolor.annotations.event.ZoomEvent;
import com.gmgcolor.annotations.model.CrossHairModel;
import com.gmgcolor.annotations.model.DocumentContainerModel;
import com.gmgcolor.application.event.EyeDropperToolChangeEvent;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;
import com.gmgcolor.common.errornotifications.messages.ConsolidatedErrorMessage;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.MouseEvent;

import mx.managers.CursorManager;

[Event(name='rotate')]
	[Event(name='fit')]
	[Event(name='scale', type="com.gmgcolor.annotations.event.ScaleEvent")]

	[InjectConstructor]
	public class ImageDocumentViewerPresenter extends Presenter {

		static public const ROTATE:String = 'rotate';
		static public const FIT:String = 'fit';

		[Bindable]
		[Embed(source="/assets/images/icons/37.png")]
		public var crosshairCursor:Class

		private var _cursorID:int;

		[Bindable]
		public var bytesLoaded:int;

		[Bindable]
		public var bytesTotal:int;

		[Bindable]
		public var progressIsVisible:Boolean;

		private var _imagePath:String;

		[Bindable]
		public var isPlacing:Boolean;

		[Bindable]
		public var highlightEnabled:Boolean;

		[Bindable]
		public var colourPickerEnabled:Boolean;

		[Bindable]
		public var markupEnabled:Boolean;
		private var _crossHairModel:CrossHairModel;

		public function ImageDocumentViewerPresenter(model:DocumentContainerModel, crossHairModel:CrossHairModel)
		{
			//this._model = model;
			this._crossHairModel = crossHairModel;
			this._crossHairModel.addEventListener(CrossHairModel.ISPLACING_CHANGE_EVENT, onIsPlacingChange);
		}

		public function onMouseMove(event:Event):void
		{
			if (_crossHairModel.isPlacing) {
				CursorManager.removeAllCursors();
				_cursorID = CursorManager.setCursor(crosshairCursor, 2, -8, -8);
			}
		}

		public function onMouseOut(event:MouseEvent):void
		{
			CursorManager.removeAllCursors();
		}

		[MessageHandler(selector="rotateButtonClick", scope="global")]
		public function onRotateMessage(event:DocumentControlsEvent):void
		{
			dispatchEvent(new Event(ROTATE));
		}

		[MessageHandler(selector="fitToContentButtonClick", scope="global")]
		public function onFitMessage(event:DocumentControlsEvent):void
		{
			dispatchEvent(new Event(FIT));
		}

		[MessageHandler(selector="oneToOneButtonClick", scope="global")]
		public function onOneToOneMessage(event:DocumentControlsEvent):void
		{
			// TODO: Scale = 1 means 1 tile on screen, not proper 1 to 1 zoom
			dispatchEvent(new ScaleEvent(ScaleEvent.SCALE, 1));
		}

		[MessageHandler(selector="scale", scope="global")]
		public function onScaleMessage(event:DocumentControlsEvent):void
		{
			dispatchEvent(new ZoomEvent(ZoomEvent.ZOOM, event.value));
		}

		[MessageHandler(selector="disable", scope="global")]
		public function onDisableColorPicker(value:EyeDropperToolChangeEvent):void
		{
			colourPickerEnabled = false;
		}

		[MessageHandler(selector="enable", scope="global")]
		public function onEnableColorPicker(value:EyeDropperToolChangeEvent):void
		{
			colourPickerEnabled = true;
		}


		[MessageHandler(selector="disable", scope="global")]
		public function onDisableMarkup(value:MarkupEvent):void
		{
			markupEnabled = false;
		}

		[MessageHandler(selector="enable", scope="global")]
		public function onEnableMarkup(value:MarkupEvent):void
		{
			markupEnabled = true;
		}

		[MessageHandler(selector="disable")]
		public function onDisableHighlight(value:HighlightEvent):void
		{
			highlightEnabled = false;
		}

		[MessageHandler(selector="enable")]
		public function onEnableHighlight(value:HighlightEvent):void
		{
			highlightEnabled = true;
		}

		public function onTileLoadFail(event:IOErrorEvent):void
		{
			dispatcher(new ConsolidatedErrorMessage(event.text));
		}

		[MessageHandler(selector="newTiledDocument")]
		public function onNewSWF(event:DocumentEvent):void
		{
			imagePath = event.value + 'ImageProperties.xml';
		}

		private function onIsPlacingChange(event:Event):void
		{
			isPlacing = _crossHairModel.isPlacing;
		}

		[Bindable]
		public function get imagePath():String
		{
			return _imagePath;
		}

		public function set imagePath(value:String):void
		{
			if (value != imagePath) {
				_imagePath = value;
			}
		}
	}
}
