package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.HighlightEvent;
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.annotations.event.TextToolsChangeEvent;
import com.gmgcolor.annotations.event.TextToolsPopUpMenuEvent;
import com.gmgcolor.annotations.model.TextToolsPopUpModel;

import flash.geom.Point;

import mx.core.FlexGlobals;

import spark.events.IndexChangeEvent;

[InjectConstructor]
	public class TextToolsPopUpMenuPM extends BaseUIPM {


		[Bindable]
		public var selectToolIndex:int = 0;


		private var _view:TextToolsPopUpMenu;

		private var _currentState:String;

		[MessageDispatcher]
		public var dispatcher:Function;


		public function TextToolsPopUpMenuPM(textToolsPopUpModel:TextToolsPopUpModel)
		{
			_model = textToolsPopUpModel;
			super();
		}

		[Init]
		override public function init():void
		{
			super.init();
			_systemManager = FlexGlobals.topLevelApplication.systemManager.getSandboxRoot();
		}


		public function registerView(view:TextToolsPopUpMenu):void
		{
			this._view = view;
		}

		[MessageHandler(selector="cancel", scope="global")]
		public function onCommentsCancel(event:CommentsEvent):void
		{
			isVisible = false;
		}

		[MessageHandler]
		public function onPostComment(event:NewCommentEvent):void
		{
			isVisible = false;
		}

		[MessageHandler(selector="disable")]
		public function onDisableMarkup(value:HighlightEvent):void
		{
			isVisible = false;
		}


		[MessageHandler(selector="clear")]
		public function onTextToolsClear(event:TextToolsPopUpMenuEvent):void
		{
			isVisible = false;
		}

		[MessageHandler(selector="mouseUp")]
		public function onTextToolsMouseUp(event:TextToolsPopUpMenuEvent):void
		{
			currentState = "active";
			var pt:Point = new Point(event.globalRect.right, event.globalRect.bottom);
			pt = _view.parent.globalToLocal(pt);
			width = event.globalRect.width;
			height = event.globalRect.height;
			xPos = pt.x;
			yPos = pt.y;
			isVisible = true;
			selectToolIndex = 0;
		}

		[MessageHandler(selector="mouseMove")]
		public function onTextToolsMouseMove(event:TextToolsPopUpMenuEvent):void
		{
			/*	var pt:Point = new Point(event.globalRect.right, event.globalRect.bottom);
			 pt = _view.parent.globalToLocal(pt);
			 width = event.globalRect.width;
			 height = event.globalRect.height;
			 xPos = pt.x;
			 yPos = pt.y;*/
		}

		public function onTextToolsChange(event:IndexChangeEvent):void
		{
			dispatcher(new TextToolsChangeEvent(TextToolsChangeEvent.CHANGE, event.newIndex));
		}


		[Bindable]
		public function get currentState():String
		{
			return _currentState;
		}

		public function set currentState(value:String):void
		{
			_currentState = value;
		}
	}
}
