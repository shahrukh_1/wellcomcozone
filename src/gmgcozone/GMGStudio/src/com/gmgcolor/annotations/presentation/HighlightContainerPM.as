package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.HighlightEvent;
import com.gmgcolor.annotations.event.HighlightTextEvent;
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.annotations.event.RedrawHighlightEvent;
import com.gmgcolor.annotations.event.TextToolsChangeEvent;
import com.gmgcolor.annotations.event.TextToolsPopUpMenuEvent;
import com.gmgcolor.annotations.model.HighlightContainerModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.domain.TetFont;
import com.gmgcolor.application.domain.TextMetrics;
import com.gmgcolor.common.errornotifications.messages.ConsolidatedErrorMessage;
import com.gmgcolor.utils.FXGUtil;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.ui.Mouse;
import flash.ui.MouseCursor;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;
import mx.events.ResizeEvent;

[InjectConstructor]
	public class HighlightContainerPM extends BaseUIPM {

		namespace tetns = "http://www.pdflib.com/XML/TET3/TET-3.0";

		use namespace tetns;

		[MessageDispatcher]
		public var dispatcher:Function;

		public static const HIGHLIGHT_TYPE_NORMAL:int = 1;
		public static const HIGHLIGHT_TYPE_INSERT:int = 2;
		public static const HIGHLIGHT_TYPE_REPLACE:int = 3;
		public static const HIGHLIGHT_TYPE_DELETE:int = 4;

		[Bindable]
		[Inject]
		public var approvalModel:ApprovalModel;
		public var textToolsXpos:Number = 0;
		public var textToolsYpos:Number = 0;
		private var _view:HighlightContainer;
		private var _drawHighlight:Boolean;
		private var _startX:Number;
		private var _startY:Number;
		private var _directionX:Number;
		private var _directionY:Number;
		private var _startRect:Rectangle;
		private var _highlightCount:int;
		private var _selectedStr:String;
		private var _lastRect:Rectangle;
		private var _xml:XML;
		private var _loader:URLLoader;
		private var xml_url:String;
		private var _enabled:Boolean;
		private var _lastFXG:XML;


		public function HighlightContainerPM(highlightContainerModel:HighlightContainerModel):void
		{
			this._model = highlightContainerModel;
			super();
		}

		[MessageHandler(selector="newHighlightXml")]
		public function onNewHighlightXML(event:DocumentEvent):void
		{
			xml_url = event.value;
		}

		private function onXMLLoadError(event:IOErrorEvent):void
		{
			dispatcher(new ConsolidatedErrorMessage(event.text));
		}

		private function xmlLoaded(event:Event):void
		{
			_loader.removeEventListener(Event.COMPLETE, xmlLoaded);
			_loader.removeEventListener(IOErrorEvent.IO_ERROR, onXMLLoadError);
			_xml = new XML(_loader.data);
			_loader = null;
			parseXML();
		}

		[MessageHandler(selector="cancel")]
		public function onCommentsCancel(event:CommentsEvent):void
		{
			clear();
			dispatcher(new HighlightEvent(HighlightEvent.DISABLE));
		}

		[MessageHandler]
		public function onPostComment(event:NewCommentEvent):void
		{
			clear();
			dispatcher(new HighlightEvent(HighlightEvent.DISABLE));
		}

		[MessageHandler(selector="disable")]
		public function onDisableMarkup(value:HighlightEvent):void
		{
			if (_enabled) {
				_view.graphics.clear();
				clear();
				Mouse.cursor = MouseCursor.AUTO;
				_view.mouseEnabled = _view.mouseChildren = false;
				_view.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false);
				_enabled = false;
			}
		}

		[MessageHandler(selector="enable")]
		public function onEnableMarkup(value:HighlightEvent):void
		{
			if (!_enabled) {
				_enabled = true;
				_view.mouseEnabled = _view.mouseChildren = true;
				_view.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				_view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			}
		}

		[MessageHandler]
		public function onRedrawHighlight(event:RedrawHighlightEvent):void
		{
			HighlightContainerModel(_model).StartIndex = event.startIndex;
			HighlightContainerModel(_model).EndIndex = event.endIndex;
			HighlightContainerModel(_model).hightlightType = event.highlightType;
			drawHighlight(event.highlightType);
			saveSVG();
		}

		private var _currentTextToolsIndex:int;

		[MessageHandler]
		public function onTextToolsChange(event:TextToolsChangeEvent):void
		{
			_currentTextToolsIndex = event.newIndex
			switch (event.newIndex) {
				case 0:
				{
					drawHighlight(HIGHLIGHT_TYPE_NORMAL);
					saveSVG();
					dispatcher(new HighlightTextEvent(HighlightTextEvent.ADDED, _selectedStr));
					break;
				}
				case 1:
				{
					drawHighlight(HIGHLIGHT_TYPE_REPLACE);
					saveSVG();
					dispatcher(new HighlightTextEvent(HighlightTextEvent.ADDED, "[REPLACE WITH]\n" + _selectedStr));
					break;
				}
				case 2:
				{
					drawHighlight(HIGHLIGHT_TYPE_NORMAL);
					saveSVG();
					dispatcher(new HighlightTextEvent(HighlightTextEvent.ADDED, "[INSERT]\n"));
					break;
					break;
				}
				case 3:
				{
					drawHighlight(HIGHLIGHT_TYPE_REPLACE);
					saveSVG();
					dispatcher(new HighlightTextEvent(HighlightTextEvent.ADDED, "[DELETE]\n" + _selectedStr));
					break;
				}
				default:
				{
					break;
				}
			}
		}


		public function registerView(view:HighlightContainer):void
		{
			this._view = view;
			this._view.addEventListener(ResizeEvent.RESIZE, parseXML);
			this._view.addEventListener(FlexEvent.CREATION_COMPLETE, parseXML);
			loadXML();
		}

		private function loadXML():void
		{
			if (xml_url) {
				_loader = new URLLoader();
				_loader.addEventListener(Event.COMPLETE, xmlLoaded);
				_loader.addEventListener(IOErrorEvent.IO_ERROR, onXMLLoadError);
				_loader.load(new URLRequest(xml_url + '?' + int(Math.random() * 9999999)));
			}
		}

		private var tetFonts:Object;

		private function getGlyphHeight(size:Number, tetFont:TetFont):Number
		{
			return (size * (tetFont.ascender) / 1000) + (size * (tetFont.descender * -1) / 1000);
		}

		private function convertY(size:Number, y:Number, tetFont:TetFont):Number
		{
			return y + (size * (tetFont.descender) / 1000);
		}

		private function parseXML(e:Event = null):void
		{
			if (_xml) {
				// set ratio
				var ratioX:Number = _view.width / Number(_xml.tetns::Document.Pages.Page[0].@width);
				var ratioY:Number = _view.height / Number(_xml.tetns::Document.Pages.Page[0].@height);
				if (ratioX != 0 && ratioY != 0) {
					var paras:XMLList = _xml.tetns::Document.Pages.Page[0].Content..Para; // Select all paragraphs
					var fonts:XMLList = _xml.tetns::Document.Pages.Resources.Fonts.Font;

					tetFonts = {};

					HighlightContainerModel(_model).textMetricsCollection = new ArrayCollection();

					var tm:TextMetrics;
					var para:XML;
					var box:XML;
					var count:int = 0;
					var glyph:XML;
					var font:XML;
					var tetFont:TetFont;

					for (var i:int = 0; i < fonts.length(); i++) {
						font = fonts[i];
						tetFont = new TetFont();
						tetFont.ascender = font.@ascender;
						tetFont.capheight = font.@capheight;
						tetFont.descender = font.@descender;
						tetFont.embedded = font.@embedded;
						tetFont.fullname = font.@fullname;
						tetFont.id = font.@id;
						tetFont.italicangle = font.@italicangle;
						tetFont.name = font.@name;
						tetFont.type = font.@type;
						tetFont.weight = font.@weight;
						tetFont.xheight = font.@xheight;
						tetFonts[tetFont.id] = tetFont;
					}

					for (i = 0; i < paras.length(); i++) {
						para = paras[i];

						var boxes:XMLList = para..Box;
						var lastX:Number;
						var lastY:Number;
						var lastHeight:Number;
						var createSpace:Boolean;
						var corner0x:Number;
						var corner0y:Number;

						for (var j:int = 0; j < boxes.length(); j++) {
							box = boxes[j];
							var glyphs:XMLList = box..Glyph;

							if (j > 0 && j < boxes.length())
								createSpace = true;

							for (var k:int = 0; k < glyphs.length(); k++) {
								glyph = glyphs[k];
								corner0x = convert(glyph.@x, ratioX);

								tm = new TextMetrics();
								tm.color = glyph.color;
								tm.font = glyph.@font;
								tm.size = glyph.@size;

								corner0y = convert(convertY(tm.size, glyph.@y, tetFonts[tm.font]), ratioY);

								if (createSpace) {
									createSpace = false;
									tm = new TextMetrics();
									tm.render = lastY == _view.height - corner0y
									tm.color = glyph.color;
									tm.corner0x = lastX
									tm.corner0y = lastY;
									tm.corner1x = corner0x;
									tm.height = lastHeight;
									tm.character = " ";
									tm.index = count;
									count++;
									HighlightContainerModel(_model).textMetricsCollection.addItem(tm);
								}

								tm = new TextMetrics();
								tm.color = glyph.color;
								tm.font = glyph.@font;
								tm.size = glyph.@size;
								tm.render = true;

								if (corner0x < lastX && lastY == _view.height - corner0y)
									tm.corner0x = lastX;
								else
									tm.corner0x = corner0x;

								tm.corner0y = lastY = _view.height - corner0y;
								tm.corner1x = lastX = convert(Number(glyph.@x) + Number(glyph.@width), ratioX);
								tm.height = lastHeight = convert(getGlyphHeight(tm.size, tetFonts[tm.font]), ratioY);
								tm.character = glyph.text();
								if (tm.character == "")
									tm.character = " ";
								tm.index = count;
								count++;
								HighlightContainerModel(_model).textMetricsCollection.addItem(tm);
							}
						}

						tm = new TextMetrics();
						tm.render = false;
						tm.color = glyph.color;
						tm.corner0x = lastX
						tm.corner0y = lastY;
						tm.corner1x = corner0x;
						tm.height = lastHeight;
						tm.render = false;
						tm.character = "\n\n";
						tm.index = count;
						count++;
						HighlightContainerModel(_model).textMetricsCollection.addItem(tm);
					}

					// DRAW TEXT HIGHLIGHTING AREA
					/*for each(var metric:TextMetrics in HighlightContainerModel(_model).textMetricsCollection) {
						_view.graphics.beginFill(0x00CCFF, 0.5);
						_view.graphics.drawRect(metric.corner0x, metric.corner0y - metric.height, metric.corner1x - metric.corner0x, metric.height);
						_view.graphics.endFill();
					}*/

					if (HighlightContainerModel(_model).textMetricsCollection.length > 0) {
						dispatcher(new HighlightEvent(HighlightEvent.HIGHLIGHTS_AVAILABLE));
					} else {
						dispatcher(new HighlightEvent(HighlightEvent.HIGHLIGHTS_UNAVAILABLE));
					}
				}
			}
		}

		private function convert(value:Number, ratio:Number):Number
		{
			return value * ratio;
		}

		private function onMouseDown(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			_systemManager.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);

			var obj:Object = Object(event.target);
			while (obj != null) {
				if (obj is TextToolsPopUpMenu) {
					return;
				}
				obj = obj.parent;
			}

			dispatcher(new TextToolsPopUpMenuEvent(TextToolsPopUpMenuEvent.CLEAR));

			clear();
			_drawHighlight = true;
			isReversing = false;

			_startX = _view.mouseX;
			_startY = _view.mouseY;

			var point:Point = new Point(_startX, _startY);

			// Set the I Beam if we have to.
			for each (var tm:TextMetrics in HighlightContainerModel(_model).textMetricsCollection) {
				var rect:Rectangle = new Rectangle(tm.corner0x,
						tm.corner0y - tm.height,
						tm.corner1x - tm.corner0x,
						tm.height);

				if (rect.containsPoint(point)) {
					_startRect = rect;
					HighlightContainerModel(_model).StartIndex = tm.index;
					HighlightContainerModel(_model).CrosshairXCoord = _startX;
					HighlightContainerModel(_model).CrosshairYCoord = _startY;
					break;
				}
			}
		}


		private function onMouseMove(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			var mouseX:int = _view.mouseX;
			var mouseY:int = _view.mouseY;

			var point:Point = new Point(mouseX, mouseY);
			var rect:Rectangle;
			var contains:Boolean = false;
			// Set the I Beam if we have to.
			for each (var tm:TextMetrics in HighlightContainerModel(_model).textMetricsCollection) {
				contains ||= new Rectangle(tm.corner0x, tm.corner0y - tm.height, tm.corner1x - tm.corner0x, tm.height).containsPoint(point);
				if (contains) {
					break;
				}
			}

			Mouse.cursor = contains ? MouseCursor.IBEAM : MouseCursor.AUTO;

			if (!_drawHighlight) // If mouse isn't down, don't continue
				return;

			_directionX = 1;
			_directionY = 1;

			if (mouseX < _startX) {
				_directionX = -1;
			}

			if (mouseY < _startY) {
				_directionY = -1;
			}


			for each (tm in HighlightContainerModel(_model).textMetricsCollection) {
				rect = new Rectangle(tm.corner0x,
						tm.corner0y - tm.height,
						tm.corner1x - tm.corner0x,
						tm.height);

				if (rect.containsPoint(point)) {
					if (_directionX == -1 || mouseY < _startY - rect.height) {
						if (!isReversing) {
							isReversing = true;
						}
						HighlightContainerModel(_model).EndIndex = tm.index;
					}
					else {
						isReversing = false;
						HighlightContainerModel(_model).EndIndex = tm.index;
					}
					break;
				}
			}

			if (HighlightContainerModel(_model).EndIndex == 0 && HighlightContainerModel(_model).StartIndex == 0)
				return;

			switch (_currentTextToolsIndex) {
				case 0:
				{
					drawHighlight(HIGHLIGHT_TYPE_NORMAL);
					break;
				}
				case 1:
				{
					drawHighlight(HIGHLIGHT_TYPE_REPLACE);
					break;
				}
				case 2:
				{
					drawHighlight(HIGHLIGHT_TYPE_NORMAL);
					break;
					break;
				}
				case 3:
				{
					drawHighlight(HIGHLIGHT_TYPE_REPLACE);
					break;
				}
				default:
				{
					break;
				}
			}


		}

		private var isReversing:Boolean;

		private function drawHighlight(type:int):void
		{
			HighlightContainerModel(_model).hightlightType = type;
			_view.graphics.clear();

			_highlightCount = 0;

			_selectedStr = "";
			_lastFXG = FXGUtil.createFXGTemplate();
			for each (var tm:TextMetrics in HighlightContainerModel(_model).textMetricsCollection) {
				var min:Number = Math.min(HighlightContainerModel(_model).StartIndex, HighlightContainerModel(_model).EndIndex);
				var max:Number = Math.max(HighlightContainerModel(_model).StartIndex, HighlightContainerModel(_model).EndIndex);

				if (tm.index >= min && tm.index <= max) {
					var x:Number, y:Number, w:Number, h:Number;

					_selectedStr += tm.character;
					var path:XML = <Path data="">
						<fill>
							<SolidColor color="#000000" />
						</fill>
					</Path>;
					if ((tm.corner1x - tm.corner0x) > 0) {

						switch (type) {
							case HIGHLIGHT_TYPE_NORMAL:
							{
								x = tm.corner0x;
								y = tm.corner0y - tm.height;
								w = tm.corner1x - tm.corner0x;
								h = tm.height;
								_view.graphics.beginFill(0x00CCFF, 0.5);
								_view.graphics.drawRect(x, y, w, h);
								_view.graphics.endFill();

								path.@data += "M " + x + " " + y; // move to x,y
								path.@data += "L " + (x + w) + " " + y; // line to top right
								path.@data += "L " + (x + w) + " " + (y + h); // line to bottom right
								path.@data += "L " + (x) + " " + (y + h); // line to bottom left
								path.@data += "L " + (x) + " " + (y); // line to top left
								path.fill.SolidColor[0].@color = '#00CCFF';
								_lastFXG.appendChild(path);

								break;
							}
							case HIGHLIGHT_TYPE_REPLACE:
							{
								x = tm.corner0x;
								y = tm.corner0y - (tm.height / 2);
								w = tm.corner1x - tm.corner0x;
								h = tm.height / 12;
								_view.graphics.beginFill(0xff0000, 1);
								_view.graphics.drawRect(x, y, w, h);
								_view.graphics.endFill();

								path.@x = tm.corner0x;
								path.@y = tm.corner0y - (tm.height / 2);
								path.@width = tm.corner1x - tm.corner0x;
								path.@height = tm.height / 12;

								path.@data += "M " + x + " " + y; // move to x,y
								path.@data += "L " + (x + w) + " " + y; // line to top right
								path.@data += "L " + (x + w) + " " + (y + h); // line to bottom right
								path.@data += "L " + (x) + " " + (y + h); // line to bottom left
								path.@data += "L " + (x) + " " + (y); // line to top left
								path.fill.SolidColor[0].@color = '#ff0000';
								_lastFXG.appendChild(path);
								break;
							}
							case HIGHLIGHT_TYPE_INSERT:
							{

								break;
							}
							case HIGHLIGHT_TYPE_DELETE:
							{
								break;
							}

							default:
							{
								break;
							}
						}

						_lastRect = new Rectangle(tm.corner0x, tm.corner0y - tm.height, tm.corner1x - tm.corner0x, tm.height);
					}
				}
			}
		}

		[MessageHandler(selector="erase")]
		public function onHighlightTextEvent(event:HighlightTextEvent):void
		{
			clear();
		}

		public function clear():void
		{
			_drawHighlight = false;
			if (_view) {
				_view.graphics.clear();
			}
			_lastRect = null;
			_startRect = null;
		}


		private function onMouseUp(event:MouseEvent):void
		{
			_drawHighlight = false;
			_systemManager.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);

			if (_lastRect) {
				textToolsXpos = _lastRect.bottomRight.x;
				textToolsYpos = _lastRect.bottomRight.y;
				dispatcher(new TextToolsPopUpMenuEvent(TextToolsPopUpMenuEvent.MOUSE_UP, getTextContainerGlobalRect(_lastRect)));
			}

			saveSVG();

			dispatcher(new HighlightTextEvent(HighlightTextEvent.ADDED, _selectedStr));
		}

		private function saveSVG():void
		{
			var svg:XML = FXGUtil.setSVGSize(FXGUtil.toSVG(_lastFXG), _view.width, _view.height);
			HighlightContainerModel(_model).SVG = svg.toXMLString();
		}

		private function getTextContainerGlobalRect(rect:Rectangle):Rectangle
		{
			var pt:Point = new Point(rect.x, rect.y);
			pt = _view.localToGlobal(pt);
			var pt2:Point = new Point(rect.right, rect.bottom);
			pt2 = _view.localToGlobal(pt2);
			var rect:Rectangle = new Rectangle(pt.x, pt.y, pt2.x - pt.x, pt2.y - pt.y);
			return rect;
		}
	}
}
