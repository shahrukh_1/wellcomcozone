package com.gmgcolor.annotations.presentation {
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;
import flash.events.EventDispatcher;

import mx.core.FlexGlobals;
import mx.managers.ISystemManager;

public class BaseUIPM extends EventDispatcher {

		protected var _model:BaseUIModel;

		[Bindable]
		public var isVisible:Boolean;

		private var _xPos:Number;

		[Bindable]
		public var yPos:Number;

		[Bindable]
		public var scaleX:Number = 1;

		[Bindable]
		public var scaleY:Number = 1;

		private var _width:Number;

		private var _height:Number;

		protected var _systemManager:ISystemManager;

		public function BaseUIPM()
		{
			_model.addEventListener(BaseUIModel.XPOS_CHANGE_EVENT, onXPosChange);
			_model.addEventListener(BaseUIModel.YPOS_CHANGE_EVENT, onYPosChange);
			_model.addEventListener(BaseUIModel.SCALEX_CHANGE_EVENT, onScaleXChange);
			_model.addEventListener(BaseUIModel.SCALEY_CHANGE_EVENT, onScaleYChange);
			_model.addEventListener(BaseUIModel.ISVISIBLE_CHANGE_EVENT, onIsVisibleChange);
			_model.addEventListener(BaseUIModel.WIDTH_CHANGE_EVENT, onWidthChange);
			_model.addEventListener(BaseUIModel.HEIGHT_CHANGE_EVENT, onHeightChange);
		}


		[Bindable]
		public function get height():Number
		{
			return _height;
		}

		public function set height(value:Number):void
		{
			_height = value;
		}

		[Init]
		public function init():void
		{
			// TODO: FIX THIS SHIT.
			_systemManager = FlexGlobals.topLevelApplication.systemManager.getSandboxRoot();
		}

		protected function onXPosChange(event:Event):void
		{
			xPos = _model.xPos;
		}

		protected function onYPosChange(event:Event):void
		{
			yPos = _model.yPos;
		}

		protected function onScaleYChange(event:Event):void
		{
			scaleX = _model.scaleX;
		}

		protected function onScaleXChange(event:Event):void
		{
			scaleY = _model.scaleY;
		}

		protected function onIsVisibleChange(event:Event):void
		{
			isVisible = _model.isVisible;
		}

		protected function onWidthChange(event:Event):void
		{
			width = _model.width;
		}

		protected function onHeightChange(event:Event):void
		{
			height = _model.height;
		}

		[Bindable]
		public function get width():Number
		{
			return _width;
		}

		public function set width(value:Number):void
		{
			_model.width = value;
			_width = value;
		}

		[Bindable]
		public function get xPos():Number
		{
			return _xPos;
		}

		public function set xPos(value:Number):void
		{
			_xPos = value;
		}
	}
}
