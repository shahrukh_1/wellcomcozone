package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.event.MouseMoveDrawingEvent;
import com.gmgcolor.annotations.event.MouseUpDrawingEvent;
import com.gmgcolor.annotations.event.SaveBitmapDataEvent;
import com.gmgcolor.annotations.event.StartDrawingEvent;
import com.gmgcolor.annotations.model.CrossHairModel;
import com.gmgcolor.annotations.model.DocumentModel;
import com.gmgcolor.annotations.model.MarkupContainerModel;

import flash.events.MouseEvent;
import flash.geom.Point;

[InjectConstructor]
	public class MarkupContainerPM extends BaseUIPM {

		[MessageDispatcher]
		public var dispatcher:Function;
		private var _documentModel:DocumentModel;
		private var _crossHairModel:CrossHairModel;
		private var _view:MarkupContainer;
		private var _enabled:Boolean;

		public function MarkupContainerPM(documentModel:DocumentModel, crossHairModel:CrossHairModel, markupContainerModel:MarkupContainerModel)
		{
			this._documentModel = documentModel;
			this._crossHairModel = crossHairModel;
			this._model = markupContainerModel;
			super();
		}

		public function registerView(view:MarkupContainer):void
		{
			this._view = view;
		}

		[MessageHandler(selector="cancel", scope="global")]
		public function onCancelComment(event:CommentsEvent):void
		{
			dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
		}

		[MessageHandler(selector="disable", scope="global")]
		public function onDisableMarkup(value:MarkupEvent):void
		{
			if (_enabled) {
				_enabled = false;
				_view.mouseEnabled = _view.mouseChildren = false;
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			}
		}

		[MessageHandler(selector="enable", scope="global")]
		public function onEnableMarkup(value:MarkupEvent):void
		{
			if (!_enabled) {
				_enabled = true;
				_view.mouseEnabled = _view.mouseChildren = true;
				_view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			}
		}

		[MessageHandler(selector="enable", scope="global")]
		public function onSaveBitmapData(event:SaveBitmapDataEvent):void
		{
			_view.addElement(event.fxg);
		}

		private function onMouseDown(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			if (_crossHairModel.isDragging || _documentModel.handModeOn)
				return;

			var obj:Object = Object(event.target);
			var parent:Object = obj.parent;

			while (obj != null) {
				if (obj is MarkerContainer)
					return;
				obj = obj.parent;
			}

			_view.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			_view.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			_view.addEventListener(MouseEvent.RELEASE_OUTSIDE, onMouseUp);

			dispatcher(new StartDrawingEvent(StartDrawingEvent.START, new Point(_view.mouseX, _view.mouseY)));
		}

		private function onMouseMove(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			dispatcher(new MouseMoveDrawingEvent(MouseMoveDrawingEvent.MOVE, new Point(_view.mouseX, _view.mouseY)));
		}

		private function onMouseUp(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			_view.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			_view.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			_view.removeEventListener(MouseEvent.RELEASE_OUTSIDE, onMouseUp);
			dispatcher(new MouseUpDrawingEvent(MouseUpDrawingEvent.mouseUp));
		}
	}
}
