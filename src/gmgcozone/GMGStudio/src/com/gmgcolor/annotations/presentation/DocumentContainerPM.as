
package com.gmgcolor.annotations.presentation
{
import com.gmgcolor.annotations.event.DocumentContainerEvent;
import com.gmgcolor.annotations.event.DocumentProgressEvent;
import com.gmgcolor.annotations.model.CrossHairModel;
import com.gmgcolor.annotations.model.DocumentContainerModel;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;

import mx.events.MoveEvent;
import mx.events.ResizeEvent;

[InjectConstructor]
	public class DocumentContainerPM extends BaseUIPM
	{
	
		[Bindable]
		public var bytesLoaded:int;
		
		[Bindable]
		public var bytesTotal:int;
		
		[Bindable]
		public var progressIsVisible:Boolean;
		
		private var _swfSource:String;
		
		private var _view:DocumentContainer;
		
		private var _crossHairModel:CrossHairModel;
		
		
		
		public function DocumentContainerPM(documentContainerModel:DocumentContainerModel,
											crossHairModel:CrossHairModel)
		{
			this._model = documentContainerModel;
			this._crossHairModel = crossHairModel;
			super();
		}
		
		[Init]
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 */
		[MessageDispatcher] 
		public var dispatcher:Function;
		
		
		public function swfloader_ioErrorHandler(event:IOErrorEvent):void
		{
			
		}
		
		public function swfloader_progressHandler(event:ProgressEvent):void
		{
			dispatcher(new DocumentProgressEvent(DocumentProgressEvent.PROGRESS, event.bytesLoaded, event.bytesTotal));
		}
		
		public function swfloader_completeHandler(event:Event):void
		{
			var movieClip:MovieClip = MovieClip(event.target.content);
			width = movieClip.width;
			height = movieClip.height;
			progressIsVisible = false; 
			dispatcher(new DocumentContainerEvent(DocumentContainerEvent.SWF_LOADED,movieClip));
		}
		
		public function registerView(view:DocumentContainer):void
		{
			this._view = view;
			this._view.addEventListener(MoveEvent.MOVE,onMove);
			this._view.addEventListener(ResizeEvent.RESIZE,onResize);
		}
		
		public function onResize(event:ResizeEvent):void
		{
			_model.width = _view.width;
			_model.height = _view.height;
		}
		
		public function onMove(event:MoveEvent):void
		{
			_model.xPos = _view.x;
			_model.yPos = _view.y;
		}
		
		[Bindable]
		public function get swfSource():String
		{
			return _swfSource;
		}
		
		public function set swfSource(value:String):void
		{
			_swfSource = value;
		}

	}
}
