package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.event.ActiveColorStageContainerEvent;
import com.gmgcolor.annotations.event.ColorPickerDragEvent;
import com.gmgcolor.annotations.event.ColourInfoPickerMoveEvent;
import com.gmgcolor.annotations.event.CreateStageColorContainerEvent;
import com.gmgcolor.annotations.model.CommentsModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.event.EyeDropperToolChangeEvent;
import com.gmgcolor.application.model.SettingsModel;
import com.gmgcolor.common.util.Dispatcher;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Matrix;
import flash.geom.Point;

import mx.collections.ListCollectionView;
import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.events.MoveEvent;
import mx.managers.ISystemManager;

public class ColorPickerStageContainerPM extends Dispatcher {

		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var commentsModel:CommentsModel;

		[Inject]
		public var settingsModel:SettingsModel;

		private var _registrationContainer:Sprite;

		private var _removePins:Boolean;

		private var currentColorInfoContainer:ColorInfoContainer;

		private var myBitmapData:BitmapData;

		//private var _currentZoomer:ZoomifyViewer;

		private var _currentDocument:Document;

		protected var _systemManager:ISystemManager;

		public function ColorPickerStageContainerPM()
		{

		}

		[Init]
		public function init():void
		{
			_systemManager = FlexGlobals.topLevelApplication.systemManager.getSandboxRoot();
			_systemManager.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}

		public var selectedAnnotations:ListCollectionView;

		private var _view:ColorPickerStageContainer;

		[MessageHandler]
		public function onCreateStageColorContainer(event:CreateStageColorContainerEvent):void
		{
			var pt:Point = event.pt;
			pt = _registrationContainer.localToGlobal(pt);
			pt = _view.globalToLocal(pt);
			var c:ColorInfoContainer = new ColorInfoContainer();
			c.colourPickerType = event.colorType.label;
			c.isPlaced = true;
			c.registrationPoint = event.pt;
			c.addEventListener(CloseEvent.CLOSE, onColorInfoContainerClose);
			c.addEventListener(ColourInfoPickerMoveEvent.MOVE, onColorInfoContainerMove);
			c.addEventListener(ActiveColorStageContainerEvent.CHANGE, onChange);
			c.addEventListener(ColorPickerDragEvent.START, onStart);
			c.addEventListener(ColorPickerDragEvent.END, onEnd);
			c.move(pt.x, pt.y);
			_view.addElement(c);
		}

		[MessageHandler(selector="disable", scope="global")]
		public function onDisableEyeDropper(event:EyeDropperToolChangeEvent):void
		{
			if (_view) {
				_view.removeAllElements();
			}
		}

		private function onEnd(event:ColorPickerDragEvent):void
		{
			dispatcher(event);
		}

		private function onStart(event:ColorPickerDragEvent):void
		{
			dispatcher(event);
		}

		/*[MessageHandler]
		 public function onZoomifyCacheEvent(event:ZoomifyCacheEvent):void
		 {
		 myBitmapData = event.bitmapData;
		 }*/

		[MessageHandler]
		public function onDocumentViewRegisteredEvent(event:DocumentViewRegisteredEvent):void
		{
			_currentDocument = event.view;
		}

		private function onChange(event:ActiveColorStageContainerEvent):void
		{
			//	if(currentColorInfoContainer)
			//	currentColorInfoContainer.isActive = false;

			if (event.active)
				currentColorInfoContainer = event.container;
			else
				currentColorInfoContainer = null;
		}

		private function onMouseDown(event:MouseEvent):void
		{
			/*if (event.target is ColorDeleteButton && currentColorInfoContainer) {
			 if (currentColorInfoContainer && !currentColorInfoContainer.isActive)
			 currentColorInfoContainer.isActive = true;
			 myBitmapData = _currentZoomer.getBitMapCache();
			 var pt1:Point = new Point(currentColorInfoContainer.x, currentColorInfoContainer.y);
			 pt1 = _view.localToGlobal(pt1);
			 pt1 = _currentZoomer.globalToLocal(pt1);
			 currentColorInfoContainer.setValues(uint("0x" + myBitmapData.getPixel(pt1.x, pt1.y).toString(16)));
			 currentColorInfoContainer.currentState = ColorInfoContainer.TOP_RIGHT;
			 return;
			 }*/
			//if(currentColorInfoContainer)
			//currentColorInfoContainer.isActive = false;

		}

		private function getContainerState(c:ColorInfoContainer):String
		{

			/*			var pt1:Point = new Point(c.x,c.y);
			 pt1 = _view.localToGlobal(pt1);
			 pt1 = _currentDocument.globalToLocal(pt1);

			 if(pt1.x > _currentDocument.width - c.container.hbox.width - c.iconArea.width &&

			 )
			 return ColorInfoContainer.TOP_LEFT;

			 if(pt1.x > _currentDocument.width - c.container.hbox.width - c.iconArea.width &&

			 )
			 return ColorInfoContainer.BOTTOM_LEFT;*/

			/*			switch(c)
			 {
			 case result:
			 {

			 break;
			 }

			 default:
			 {
			 break;
			 }
			 }*/

			return "";
		}

		private function onColorInfoContainerClose(event:CloseEvent):void
		{
			var markerContainer:MarkerContainer;
			if (event.target is ColorInfoContainer) {
				_view.removeElement(ColorInfoContainer(event.target));
			}
		}

		private function onColorInfoContainerMove(event:ColourInfoPickerMoveEvent):void
		{

			var obj:Object = Object(event.target);

			/*if (obj is ColorInfoContainer) {
			 var pt1:Point = event.pt;
			 pt1 = _view.localToGlobal(pt1);

			 var pt2:Point = _registrationContainer.globalToLocal(pt1);
			 ColorInfoContainer(obj).registrationPoint = pt2;

			 pt1 = _currentZoomer.globalToLocal(pt1);
			 obj.setValues(uint("0x" + myBitmapData.getPixel(pt1.x, pt1.y).toString(16)));

			 ColorInfoContainer(obj).currentState = ColorInfoContainer.TOP_RIGHT;

			 }*/
		}


		public function registerView(view:ColorPickerStageContainer):void
		{
			this._view = view;
			/*repositionContainers();*/
		}

		public function onPinClick(selectedItem:ApprovalAnnotationFO):void
		{

		}


		/*[MessageHandler(scope="global")]
		 public function onZoomifyChangeEvent(event:ZoomifyChangeEvent):void
		 {
		 _currentZoomer = event.zoomer;

		 repositionContainers();
		 }*/


		private function onTimerComplete(event:TimerEvent):void
		{
			/*repositionContainers();*/
		}

		private function repositionContainers():void
		{

			var c:ColorInfoContainer


			for (var i:int = 0; i < _view.numElements; i++) {
				c = ColorInfoContainer(_view.getElementAt(i));

				var pt:Point = c.registrationPoint;
				pt = _registrationContainer.localToGlobal(pt);
				pt = _view.globalToLocal(pt);
				c.move(pt.x, pt.y);
				c.validateNow();
			}

		}


		/*[MessageHandler]
		 public function onNewMarkerContainerEvent(event:NewMarkerContainerEvent):void
		 {
		 _registrationContainer = event.markerContainer;
		 }*/

		private function onMove(event:MoveEvent):void
		{
			/*	trace(new Point(event.oldX,event.oldY));*/
		}

		private function filterAnnotations(item:ApprovalAnnotationFO):Boolean
		{

			return item.Parent == 0 && approvalModel.currentPage.PageNumber == item.PageNumber;
		}


		public function skinnablecontainer1_clearHandler(event:Event, approvalAnnotationFO:ApprovalAnnotationFO):void
		{
			// TODO Auto-generated method stub

		}

		private function getBitmapData(target:UIComponent):BitmapData
		{
			var bd:BitmapData = new BitmapData(target.width, target.height);
			var m:Matrix = new Matrix();
			bd.draw(target, m);
			return bd;
		}

	}
}
