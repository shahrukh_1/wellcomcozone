package com.gmgcolor.annotations.presentation
{
import flash.events.Event;

public class ShowCommentEvent extends Event
	{
		public static const SHOW:String = "show";

		public function ShowCommentEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new ShowCommentEvent(type);
		}

		public override function toString():String
		{
			return formatToString("ShowCommentEvent");
		}
	}
}
