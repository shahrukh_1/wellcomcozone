package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.SideBarAnnotationSelectEvent;
import com.gmgcolor.annotations.model.CommentsModel;
import com.gmgcolor.annotations.model.DocumentModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.event.AddApprovalAnnotationEvent;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.model.SettingsModel;
import com.gmgcolor.model.BaseUIModel;

import flash.events.Event;

import mx.collections.ListCollectionView;

public class PinsPM {

		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var commentsModel:CommentsModel;

		[Inject]
		public var settingsModel:SettingsModel;

		[Inject]
		public var documentModel:DocumentModel;

		public function PinsPM()
		{
		}

		public var selectedAnnotations:ListCollectionView;

		private var _view:Pins;

		[MessageDispatcher]
		public var dispatcher:Function;
        private var reachedAnnotation:ApprovalAnnotationFO;

		[Init]
		public function init():void
		{
			BaseUIModel(commentsModel).addEventListener(BaseUIModel.ISON_CHANGE_EVENT, onIsOnChange);
			settingsModel.addEventListener(SettingsModel.SHOWPINS_CHANGE_EVENT, onShowPinsChange);
            documentModel.addEventListener(DocumentModel.CURRENTTIME_CHANGE_EVENT, onVideoTimeChange);
            documentModel.addEventListener(DocumentModel.VIDEOPLAYING_CHANGE_EVENT, onPlayingStateChange);
		}

		[MessageHandler(selector="selected")]
		public function onDocumentSelectedEvent(event:DocumentEvent):void
		{
			draw();
		}


		[MessageHandler]
		public function onAddApprovalAnnotationEvent(event:AddApprovalAnnotationEvent):void
		{
			draw();
		}

		private function onIsOnChange(event:Event):void
		{
			if (_view)
				_view.container.visible = !commentsModel.isOn

			if (!commentsModel.isOn) {
				draw();
			}
		}

		public function registerView(view:Pins):void
		{
			this._view = view;
			draw();
		}

		public function onPinClick(selectedItem:ApprovalAnnotationFO):void
		{
			dispatcher(new SideBarAnnotationSelectEvent(SideBarAnnotationSelectEvent.ANNOTATION_CHANGE, selectedItem));
		}

		[MessageHandler]
		public function onAnnotationsChange(event:AnnotationsChangeEvent):void
		{
			selectedAnnotations = new ListCollectionView(event.annotations);
		}

		private function onShowPinsChange(event:Event):void
		{
			draw();
		}

        private function onVideoTimeChange(event:Event):void
        {
            if(!documentModel.isVideoPlaying)
            {
                draw();
            }
        }

        private function onPlayingStateChange(event:Event):void
        {
            if(documentModel.isVideoPlaying)
            {
                _view.container.removeAllElements();
            }
            else
            {
                draw();
            }
        }

		private function draw():void
		{
            if(selectedAnnotations)
            {
                selectedAnnotations.filterFunction = filterAnnotations;
                selectedAnnotations.refresh();
                _view.container.removeAllElements();
                if (_view && selectedAnnotations && !commentsModel.isOn && settingsModel.showPins) {
                    var pin:Pin;
                    var count:int = 0;
                    for each (var approvalAnnotationFO:ApprovalAnnotationFO in selectedAnnotations) {
                        count++;
                        if([Approval.VIDEO_TYPE, Approval.SWF_TYPE].indexOf(approvalModel.currentApproval.ApprovalType) > -1
                                && Math.round(approvalAnnotationFO.TimeFrame / 1000) != Math.round(documentModel.currentTimeFrame))
                        {
                            continue;
                        }
                        pin = addPin(approvalAnnotationFO.CrosshairXCoord, approvalAnnotationFO.CrosshairYCoord, count);
                        pin.annotation = approvalAnnotationFO;
                    }
                }
            }
		}

		private function addPin(x:Number, y:Number, index:Number):Pin
		{
			var pin:Pin = new Pin();
			pin.pinsPM = this;
			pin.text = index.toString();
			pin.move(x, y);
			_view.container.addElement(pin);
			return pin;
		}

		private function filterAnnotations(item:ApprovalAnnotationFO):Boolean
		{
			return item.Parent == 0 && approvalModel.currentPage.PageNumber == item.PageNumber;
		}
	}
}
