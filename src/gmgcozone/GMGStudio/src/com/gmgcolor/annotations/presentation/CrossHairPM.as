package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.CrossHairEvent;
import com.gmgcolor.annotations.model.CrossHairModel;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.events.ResizeEvent;

[InjectConstructor]
	public class CrossHairPM extends BaseUIPM {
		[MessageDispatcher]
		public var dispatcher:Function
		private var _view:CrossHair;

		public function CrossHairPM(crossHairModel:CrossHairModel)
		{
			this._model = crossHairModel;
			CrossHairModel(_model).addEventListener("localPointChange", onLocalPointChange);
			super();
		}

		[Init]
		override public function init():void
		{
			super.init();
		}

		public function registerView(view:CrossHair):void
		{
			this._view = view;
			this._view.addEventListener(ResizeEvent.RESIZE, onResize);
		}

		public function onMouseDown(event:MouseEvent):void
		{
			if (_model.isOn) {
				if (_model.isMoveable) {
					_systemManager.addEventListener(MouseEvent.MOUSE_MOVE, onSystemMouseMove, false, 0, true);
					_systemManager.addEventListener(MouseEvent.MOUSE_UP, onSystemMouseUp, false, 0, true);
					_view.startDrag(false);
					_model.isDragging = true;
					event.stopImmediatePropagation();
				}

				dispatcher(new CrossHairEvent(CrossHairEvent.MOUSEDOWN));
			}
		}

		public function onSystemMouseUp(event:MouseEvent):void
		{
			if (_model.isMoveable) {
				_systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, onSystemMouseMove, false)
				_systemManager.removeEventListener(MouseEvent.MOUSE_UP, onSystemMouseUp, false);
				_view.stopDrag();
				_model.isDragging = false;
			}
		}

		public function onSystemMouseMove(event:MouseEvent):void
		{
			_model.xPos = _view.x;
			_model.yPos = _view.y;

			var pt:Point = new Point(_view.x, _view.y);

			CrossHairModel(_model).globalPoint = _view.localToGlobal(pt);


			if (_model.isMoveable) {
				dispatcher(new CrossHairEvent(CrossHairEvent.MOVE));
			}
		}

		private function onResize(event:ResizeEvent):void
		{
			_model.width = _view.width;
			_model.height = _view.height;
		}

		private function onLocalPointChange(event:Event):void
		{
			var pt:Point = new Point(_model.xPos, _model.yPos);

			CrossHairModel(_model).globalPoint = _view.localToGlobal(pt);
		}

	}
}
