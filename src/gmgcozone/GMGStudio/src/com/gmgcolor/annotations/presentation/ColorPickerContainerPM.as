package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.ColorPickerDragEvent;
import com.gmgcolor.annotations.event.CreateStageColorContainerEvent;
import com.gmgcolor.annotations.model.ColorPickerModel;
import com.gmgcolor.application.event.EyeDropperToolChangeEvent;
import com.gmgcolor.application.pages.enum.EyeDropperTypeEnum;

import flash.display.BitmapData;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.Mouse;
import flash.ui.MouseCursor;

import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.managers.CursorManager;
import mx.managers.PopUpManager;

[InjectConstructor]
	public class ColorPickerContainerPM extends BaseUIPM {

		[MessageDispatcher]
		public var dispatcher:Function;

		private var _view:ColorPickContainer;
		private var _scaleX:Number;
		private var _scaleY:Number;

		private var myBitmapData:BitmapData;

		private var pickers:Array;

		private var _enabled:Boolean;

		private var currentEyeDropperType:EyeDropperTypeEnum;

		//private var _currentZoomer:ZoomifyViewer;

		private var isDragging:Boolean;

		[Bindable]
		[Embed(source="/assets/images/icons/dropper.png")]
		public var dropperTool:Class;

		[Bindable]
		[Embed(source="/assets/images/icons/dropcross.png")]
		public var dropCrossTool:Class;

		public function ColorPickerContainerPM(colorPickerModel:ColorPickerModel):void
		{
			this._model = colorPickerModel;
			super();
		}

		[Init]
		override public function init():void
		{
			super.init();
		}


		[MessageHandler(selector="enable", scope="global")]
		public function onEnableEyeDropper(event:EyeDropperToolChangeEvent):void
		{
			if (!_enabled) {
				currentEyeDropperType = event.data;
				_view.mouseEnabled = _view.mouseChildren = true;
				_view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				_systemManager.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				createColorInfoContainer();
				//myBitmapData = _currentZoomer.getBitMapCache();
				_enabled = true;
			}

		}

		[MessageHandler(selector="disable", scope="global")]
		public function onDisableEyeDropper(event:EyeDropperToolChangeEvent):void
		{
			if (_enabled && _view) {
				_view.mouseEnabled = false;
				Mouse.cursor = MouseCursor.AUTO;
				_systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				_view.removeAllElements();
				CursorManager.removeAllCursors();
				_enabled = false;
			}
		}

		/*[MessageHandler(scope="global")]
		 public function onZoomifyChangeEvent(event:ZoomifyChangeEvent):void
		 {
		 _currentZoomer = event.zoomer;

		 }*/


		[MessageHandler(selector="start", scope="global")]
		public function onStart(event:ColorPickerDragEvent):void
		{
			isDragging = true;
		}


		[MessageHandler(selector="end", scope="global")]
		public function onEnd(event:ColorPickerDragEvent):void
		{
			isDragging = false;
		}


		private function createColorInfoContainer():void
		{

			currentColorInfoContainer = new ColorInfoContainer();
			currentColorInfoContainer.visible = false;
			currentColorInfoContainer.colourPickerType = currentEyeDropperType.label;

			PopUpManager.addPopUp(currentColorInfoContainer, Main(FlexGlobals.topLevelApplication.topLevelAplication), false);

		}


		public function registerView(view:ColorPickContainer):void
		{
			this._view = view;
		}

		public function clear():void
		{

		}

		private function convert(value:Number):Number
		{
			return 300 * value / 72;
		}

		private var currentColorInfoContainer:ColorInfoContainer;

		private function onMouseDown(event:MouseEvent):void
		{
			var pt:Point = new Point(event.localX, event.localY);

			currentColorInfoContainer.move(pt.x, pt.y);
			dispatcher(new CreateStageColorContainerEvent(CreateStageColorContainerEvent.CREATE, pt, currentEyeDropperType));
			currentColorInfoContainer.visible = false;

		}


		public function updateScale(sx:Number, sy:Number):void
		{
			_scaleX = sx;
			_scaleY = sy;

		}

		/*[MessageHandler]
		 public function onZoomifyCacheEvent(event:ZoomifyCacheEvent):void
		 {
		 myBitmapData = event.bitmapData;
		 }*/

		private function onMouseMove(event:MouseEvent):void
		{

			/*currentColorInfoContainer.visible = true;

			 var pt1:Point = new Point(_view.mouseX, _view.mouseY);
			 pt1 = _view.localToGlobal(pt1);
			 currentColorInfoContainer.move(pt1.x, pt1.y);
			 pt1 = _currentZoomer.globalToLocal(pt1);
			 currentColorInfoContainer.setValues(uint("0x" + myBitmapData.getPixel(pt1.x, pt1.y).toString(16)));

			 CursorManager.removeAllCursors();


			 var isTiledContainer:Boolean;

			 var obj:Object = event.target;

			 while (obj != null) {
			 obj = obj.parent;

			 if (obj is ZoomifyViewer) {
			 isTiledContainer = true;
			 break;
			 }
			 }

			 currentColorInfoContainer.visible = isTiledContainer && !isDragging;

			 if (isTiledContainer) {
			 CursorManager.setCursor(dropperTool, 2, 0, -16);
			 }*/

		}

		private function drawHighlight(type:int):void
		{

		}


		private function onMouseUp(event:MouseEvent):void
		{


		}


		private function getTextContainerGlobalRect(rect:Rectangle):Rectangle
		{

			var pt:Point = new Point(rect.x, rect.y);

			pt = _view.localToGlobal(pt);

			var pt2:Point = new Point(rect.right, rect.bottom);

			pt2 = _view.localToGlobal(pt2);

			var rect:Rectangle = new Rectangle(pt.x, pt.y, pt2.x - pt.x, pt2.y - pt.y);

			return rect;
		}

		private function getBitmapData(target:UIComponent):BitmapData
		{
			var bd:BitmapData = new BitmapData(target.width, target.height);
			var m:Matrix = new Matrix();
			bd.draw(target, m);
			return bd;
		}


	}
}
