package com.gmgcolor.annotations.presentation
{
import flash.events.Event;

public class DocumentViewRegisteredEvent extends Event
	{
		public static const REGISTERED:String = "registered";

		private var _view:Document;

		public function DocumentViewRegisteredEvent(type:String, view:Document)
		{
			super(type,false,false);

			this._view = view;
		}

		public function get view():Document
		{
			return _view;
		}

		public override function clone():Event
		{
			return new DocumentViewRegisteredEvent(type, view);
		}

		public override function toString():String
		{
			return formatToString("DocumentViewRegisteredEvent","view");
		}
	}
}
