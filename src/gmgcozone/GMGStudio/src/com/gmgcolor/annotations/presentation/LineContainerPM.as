package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.DrawLineEvent;
import com.gmgcolor.annotations.model.LineContainerModel;

import flash.display.Graphics;
import flash.geom.Point;

import mx.events.MoveEvent;
import mx.events.ResizeEvent;

import spark.components.SkinnableContainer;

[InjectConstructor]
	public class LineContainerPM extends BaseUIPM {

		private var _view:SkinnableContainer;

		public function LineContainerPM(lineModel:LineContainerModel)
		{
			_model = lineModel;
			super();
		}

		public function registerView(view:LineContainer):void
		{
			this._view = view;
			this._view.addEventListener(MoveEvent.MOVE, onMove);
			this._view.addEventListener(ResizeEvent.RESIZE, onResize);
		}

		[MessageHandler]
		public function onDrawLine(event:DrawLineEvent):void
		{
			var g:Graphics = _view.graphics;
			var pt1:Point = event.pt1;
			var pt2:Point = event.pt2;
			g.clear();
			g.lineStyle(3, 0xFFFFFF, .5);
			g.moveTo(pt1.x, pt1.y);
			g.lineTo(pt2.x, pt2.y);
			g.lineStyle(1, 0x000000, 1);
			g.moveTo(pt1.x, pt1.y);
			g.lineTo(pt2.x, pt2.y);
		}

		private function onResize(event:ResizeEvent):void
		{
			_model.width = _view.width;
			_model.height = _view.height;
		}

		private function onMove(event:MoveEvent):void
		{
			_model.xPos = _view.x;
			_model.yPos = _view.y;
		}

	}
}
