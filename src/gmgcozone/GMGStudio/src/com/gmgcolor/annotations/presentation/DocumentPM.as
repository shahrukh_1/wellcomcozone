package com.gmgcolor.annotations.presentation {

import com.gmgcolor.annotations.domain.*;
import com.gmgcolor.annotations.enum.AnnotationEnum;
import com.gmgcolor.annotations.event.*;
import com.gmgcolor.annotations.model.*;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.domain.ApprovalPage;
import com.gmgcolor.application.event.AddApprovalAnnotationEvent;
import com.gmgcolor.application.event.AnnotationToolsEvent;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.event.ApprovalAnnotationAddedEvent;
import com.gmgcolor.application.event.DeleteApprovalAnnotationEvent;
import com.gmgcolor.application.event.EyeDropperToolChangeEvent;
import com.gmgcolor.application.event.HandChangeEvent;
import com.gmgcolor.application.event.IsLockedChangeEvent;
import com.gmgcolor.application.model.AnnotationToolsModel;
import com.gmgcolor.application.pages.enum.EyeDropperTypeEnum;
import com.gmgcolor.application.pages.events.VideoControlsEvent;
import com.gmgcolor.common.errornotifications.messages.ConsolidatedErrorMessage;
import com.gmgcolor.messages.CommentPlacedMessage;

import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.managers.CursorManager;

import org.spicefactory.parsley.core.context.Context;

[InjectConstructor]
	public class DocumentPM extends BaseUIPM {

		[Inject]
		public var annotationsToolsModel:AnnotationToolsModel;

		[Inject]
		public var securityManager:SecurityManager;


		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var highlightContainerModel:HighlightContainerModel;

		[MessageDispatcher]
		public var dispatcher:Function;

		[Bindable]
		[Embed(source="/assets/images/hand_tool.png")]
		public var handTool:Class;

		[Bindable]
		[Embed(source="/assets/images/hand_tool_down.png")]
		public var handToolDown:Class;

		[Bindable]
		public var currentState:String = "normal";

		[Bindable]
		public var progress:Number;

		[Bindable]
		public var progressBarIsVisible:Boolean;

		protected var _page:ApprovalPage;

		protected var _context:Context;
        protected var _annotationModel:AnnotationModel;
        protected var _crossHairModel:CrossHairModel;
        protected var _commentsModel:CommentsModel;
        protected var _markupContainerModel:MarkupContainerModel;
        protected var _documentContainerModel:DocumentContainerModel;
        protected var _lineModel:LineContainerModel;
        protected var _documentContainer:DocumentContainer;

        protected var _selectedAnnotation:ApprovalAnnotationFO;

        protected var _isMainViewSelected:Boolean;
        protected var _isSyncMode:Boolean;
        protected var _syncing:Boolean;
        protected var _highlightTextFilename:String;
        protected var _documentModel:DocumentModel;

        protected var drawn:Boolean;

		public function DocumentPM(context:Context, documentModel:DocumentModel, annotationModel:AnnotationModel, crossHairModel:CrossHairModel, commentsModel:CommentsModel, markupContainerModel:MarkupContainerModel, documentContainerModel:DocumentContainerModel, lineModel:LineContainerModel)
		{
			this._context = context;
			this._model = documentModel;
			this._annotationModel = annotationModel;
			this._crossHairModel = crossHairModel;
			this._commentsModel = commentsModel;
			this._markupContainerModel = markupContainerModel;
			this._documentContainerModel = documentContainerModel;
			this._lineModel = lineModel;
			this._documentModel = documentModel;
			super();

		}

		[MessageHandler(scope="global")]
		public function onAnnotationToolsEvent(event:AnnotationToolsEvent):void
		{
			if (event.isToolBtnClick()) {
                dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_DISABLE_PLAY));
				var annotation:ApprovalAnnotationFO;
				switch (event.type) {
					case AnnotationToolsEvent.DRAW_BUTTON_CLICK:
					{
						annotation = new ApprovalAnnotationFO();
						annotation.CommentType = AnnotationEnum.ANNOTATION_TYPE_DRAW;
						_annotationModel.selectedAnnotation = annotation;
						break;
					}

					case AnnotationToolsEvent.MARKER_BUTTON_CLICK:
					{
						annotation = new ApprovalAnnotationFO();
						annotation.CommentType = AnnotationEnum.ANNOTATION_TYPE_MARKER;
						_annotationModel.selectedAnnotation = annotation;
						break;
					}

					case AnnotationToolsEvent.TEXT_BUTTON_CLICK:
					{
						annotation = new ApprovalAnnotationFO();
						annotation.CommentType = AnnotationEnum.ANNOTATION_TYPE_TEXT;
						_annotationModel.selectedAnnotation = annotation;
						break;
					}

					case AnnotationToolsEvent.EYE_DROPPER_BUTTON_CLICK:
					{
						clearAll();
						dispatcher(new EyeDropperToolChangeEvent(EyeDropperToolChangeEvent.ENABLE, EyeDropperTypeEnum(event.data)));
						break;
					}

					default:
					{
						break;
					}
				}

			}
			else if (event.type == AnnotationToolsEvent.RESET)
            {
                if(isMainViewSelected)
                {
                    dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_ENABLE_PLAY));
                }
				clearAll();
			}
		}


		[MessageHandler(scope="global")]
		public function onApprovalAnnotationAdded(event:ApprovalAnnotationAddedEvent):void
		{
			if (isMainViewSelected) {
				var annotations:ArrayCollection = _annotationModel.annotations;
                _commentsModel.childAnnotations = getChildAnnotations(annotations);
			}
		}

		[MessageHandler]
		public function onPostComment(event:NewCommentEvent):void
		{
			if (!securityManager.accountUserInfo)
				dispatcher(new ConsolidatedErrorMessage("User not logged in!"));

			var annotation:ApprovalAnnotationFO = _selectedAnnotation;
			annotation.Comment = event.comment;
			annotation.AnnotatedDate = new Date();
			annotation.Page = _page.ID;
			annotation.PageNumber = _page.PageNumber;
			annotation.submitted = true;
			annotation.Status = AnnotationEnum.ANNOTATION_STATUS_NUETRAL;
			annotation.Parent = 0;

			annotation.ReferenceFilePath = "";

			annotation.CrosshairXCoord = _crossHairModel.xPos;
			annotation.CrosshairYCoord = _crossHairModel.yPos;

			if (annotation.CommentType == AnnotationEnum.ANNOTATION_TYPE_TEXT) {
				annotation.StartIndex = highlightContainerModel.StartIndex;
				annotation.EndIndex = highlightContainerModel.EndIndex;
				annotation.HighlightType = highlightContainerModel.hightlightType;
				annotation.CrosshairXCoord = highlightContainerModel.CrosshairXCoord;
				annotation.CrosshairYCoord = highlightContainerModel.CrosshairYCoord;
				annotation.HighlightData = highlightContainerModel.SVG;
			}

			annotation.IsPrivate = event.isPrivate;
			annotation.ApprovalAnnotationPrivateCollaborators = event.collaboratorIds;
			annotation.Creator = annotation.Modifier = securityManager.accountUserInfo.UserID;
			annotation.ExternalUser = FlexGlobals.topLevelApplication.parameters.extuid ? FlexGlobals.topLevelApplication.parameters.extuid : 0;
			annotation.ModifiedDate = new Date();
			annotation.approverId = securityManager.accountUserInfo.UserID;
			annotation.AnnotatedDate = new Date();

            annotation.TimeFrame = _documentModel.currentTimeFrame * 1000;

			if (!_page.Annotations) {
				_page.Annotations = [];
			}

			var annotations:Array = _page.Annotations.concat();
			annotations.push(annotation);
			_page.Annotations = annotations;
			_annotationModel.annotations.addItem(annotation);

			dispatcher(new AddApprovalAnnotationEvent(AddApprovalAnnotationEvent.ADD, annotation, securityManager.sessionId));
			dispatcher(new AnnotationsChangeEvent(AnnotationsChangeEvent.CHANGE, annotationModel.annotations));
            dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_ENABLE_PLAY));
			clearAll();
		}

		[MessageHandler]
		public function onReplyComment(event:ReplyCommentEvent):void
		{

			if (!securityManager.accountUserInfo)
				dispatcher(new ConsolidatedErrorMessage("User not logged in!"));

			var annotation:ApprovalAnnotationFO = new ApprovalAnnotationFO();
			annotation.Comment = event.comment;
			annotation.AnnotatedDate = new Date();
			if (FlexGlobals.topLevelApplication.parameters.extuid > 0) {
				annotation.Creator = 0;
				annotation.Modifier = 0;
				annotation.ExternalUser = FlexGlobals.topLevelApplication.parameters.extuid;
			}
			else {
				annotation.Creator = securityManager.accountUserInfo.UserID;
				annotation.Modifier = securityManager.accountUserInfo.UserID;
				annotation.ExternalUser = 0;
			}
			annotation.ModifiedDate = new Date();
			annotation.Page = _page.ID;
			annotation.Parent = _selectedAnnotation.ID;
			annotation.IsPrivate = false;
			annotation.ApprovalAnnotationPrivateCollaborators = [];
			annotation.Status = AnnotationEnum.ANNOTATION_STATUS_NUETRAL;

			_annotationModel.annotations.addItem(annotation);

			dispatcher(new AnnotationsChangeEvent(AnnotationsChangeEvent.CHANGE, annotationModel.annotations));
			dispatcher(new AddApprovalAnnotationEvent(AddApprovalAnnotationEvent.ADD, annotation, securityManager.sessionId));
            dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_ENABLE_PLAY));
			clearAll();
		}

		[MessageHandler]
		public function onDocumentProgressEvent(event:DocumentProgressEvent):void
		{
			progress = event.bytesLoaded / event.bytesTotal;
			progressBarIsVisible = event.bytesLoaded != event.bytesTotal;
		}

		[MessageHandler(selector="cancel", scope="global")]
		public function onCancelComment(event:CommentsEvent):void
		{
			dispatcher(new MarkupEvent(MarkupEvent.ERASE));
            if(isMainViewSelected)
            {
                dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_ENABLE_PLAY));
            }
			clearAll();
		}

		[MessageHandler(selector="added")]
		public function onHighlightTextAdded(event:HighlightTextEvent):void
		{
			if (isMainViewSelected) {
				_commentsModel.isOn = true;
				_commentsModel.submittable = true;
				_commentsModel.inputText = event.text;

			}
		}

		public function get selectedAnnotation():ApprovalAnnotationFO
		{
			return _selectedAnnotation;
		}

		private function getChildAnnotations(annotations:ArrayCollection):ArrayCollection
		{
			var childAnnotations:ArrayCollection = new ArrayCollection;

			for each (var annotation:ApprovalAnnotationFO in annotations) {
				if (annotation.hasOwnProperty('Parent') && _selectedAnnotation && annotation.Parent == _selectedAnnotation.ID) {
					childAnnotations.addItem(annotation);
				}
			}
			return childAnnotations;
		}

		[MessageHandler]
		public function onDeleteApprovalAnnotationEvent(event:DeleteApprovalAnnotationEvent):void
		{
			var index:int;
			for each (var ap:ApprovalAnnotationFO in _annotationModel.annotations) {
				if (ap.ID == event.annotationId)
					break;
				index++
			}

			_annotationModel.annotations.removeItemAt(index);

            _commentsModel.childAnnotations = getChildAnnotations(_annotationModel.annotations);

			dispatcher(new AnnotationsChangeEvent(AnnotationsChangeEvent.CHANGE, _annotationModel.annotations));
		}

		[MessageHandler(selector="added")]
		public function onCommentsAdded(event:CommentsEvent):void
		{
			if (_commentsModel.isSubmitted) {
				DocumentModel(_model).handModeOn = true;
				_annotationModel.updateCommentSequence(1);
				dispatcher(new HandChangeEvent(HandChangeEvent.CHANGE, true));
			}
		}

		[MessageHandler]
		public function onTileContainerClicked(event:TileContainerClickedEvent):void
		{
			if (_crossHairModel.isPlacing) {
				_crossHairModel.xPos = _documentContainerModel.localDocumentClickPoint.x;
				_crossHairModel.yPos = _documentContainerModel.localDocumentClickPoint.y;
				_crossHairModel.isMoveable = true;
				_commentsModel.isMoveable = true;
				_commentsModel.isOn = true;

				_crossHairModel.globalPoint = _documentContainerModel.globalDocumentClickPoint;

				_crossHairModel.isPlacing = false;
				_crossHairModel.isOn = true;
				CursorManager.removeAllCursors();
				_crossHairModel.isPlacing = false;
				_commentsModel.submittable = true;
				dispatcher(new DocumentEvent(DocumentEvent.COMMENT_PLACED));
				dispatcher(new CommentPlacedMessage(_crossHairModel.xPos, _crossHairModel.yPos));
			}
			dispatcher(new DocumentEvent(DocumentEvent.MOUSE_DOWN));
		}

		private function get filename():String
		{
			return page.SelectedApprovalSeparationIndex == 0 ? page.PageFolder : page.ApprovalSeparationPlates[page.SelectedApprovalSeparationIndex].FolderPath + "/";
		}

		private function clearAll():void
		{
			_annotationModel.selectedAnnotation = null;
			dispatcher(new AnnotationSelectedEvent(AnnotationSelectedEvent.SELECTED, null));
			_crossHairModel.isPlacing = false;
			_crossHairModel.isOn = false;
			_lineModel.isOn = false;
			_commentsModel.isOn = false;
			_crossHairModel.isOn = false;
			_crossHairModel.xPos = 0;
			_crossHairModel.yPos = 0;
			DocumentModel(_model).handModeOn = false;

			dispatcher(new HighlightTextEvent(HighlightTextEvent.ERASE));
			dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
			dispatcher(new EyeDropperToolChangeEvent(MarkupEvent.DISABLE));
			dispatcher(new HighlightEvent(HighlightEvent.DISABLE));
		}

		public function get isMainViewSelected():Boolean
		{
			return _isMainViewSelected;
		}

		public function set isMainViewSelected(value:Boolean):void
		{
			_documentModel.isMainViewSelected = value;
			_isMainViewSelected = value;
			currentState = "normal";

			/*	if (viewer)
			 viewer.disableMouseDown = !value;*/

			if (_isMainViewSelected) {
				if (_isMainViewSelected && approvalModel.documents.length > 1) {
					currentState = "selected";
				}
				dispatcher(new DocumentEvent(DocumentEvent.SELECTED, this));
				dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.RESET));
				dispatcher(new IsLockedChangeEvent(IsLockedChangeEvent.CHANGE, _documentModel.isUserLocked));
                dispatcher(new DocumentEvent(DocumentEvent.SELECT_COMPLETED))
			}
			else {
				clearAll();
			}
		}

		public function get annotationModel():AnnotationModel
		{
			return _annotationModel;
		}

		[Bindable]
		public function get page():ApprovalPage
		{
			return _page;
		}

		public function set page(value:ApprovalPage):void
		{
			if (_page) {
				_page.removeEventListener(ApprovalPage.ISMAINVIEWSELECTED_CHANGE_EVENT, onIsMainViewSelectedChange);
			}

			var approval:Approval = approvalModel.getApprovalFromPage(value.Approval);

			_annotationModel.annotations = new ArrayCollection(approval.Annotations);

			_page = value;

			if (page) {
				_page.addEventListener(ApprovalPage.ISMAINVIEWSELECTED_CHANGE_EVENT, onIsMainViewSelectedChange);
				highlightTextFilename = page.highlightTextFilename;

				dispatcher(new AnnotationsChangeEvent(AnnotationsChangeEvent.START, annotationModel.annotations));
				dispatcher(new PageChangeEvent(PageChangeEvent.pageChange, value));

                isMainViewSelected = page.isMainViewSelected;
                switch(approval.ApprovalType)
                {
                    case Approval.IMAGE_TYPE:
                        dispatcher(new DocumentEvent(DocumentEvent.NEW_TILED_DOCUMENT, filename));
                        break;
                    case Approval.VIDEO_TYPE:
                        dispatcher(new DocumentEvent(DocumentEvent.NEW_VIDEO_DOCUMENT, approval.MovieFilePath));
                        break;
                    case Approval.SWF_TYPE:
                        dispatcher(new DocumentEvent(DocumentEvent.NEW_SWF_DOCUMENT, approval.DownloadOriginalFileLink));
                        break;
                }
            }
		}

		[MessageHandler(scope="global")]
		public function onSepsChangeEvent(event:SepsChangeEvent):void
		{
			if (isMainViewSelected) {
				drawn = false;
				page.SelectedApprovalSeparationIndex = event.separationIndex;
				dispatcher(new DocumentEvent(DocumentEvent.NEW_TILED_DOCUMENT, filename));
			}
		}

		private function onIsMainViewSelectedChange(event:Event):void
		{
			isMainViewSelected = _page.isMainViewSelected;
		}

		public function get isSyncMode():Boolean
		{
			return _isSyncMode;
		}

		public function set isSyncMode(value:Boolean):void
		{
			_isSyncMode = value;
		}

		public function get highlightTextFilename():String
		{
			return _highlightTextFilename;
		}

		public function set highlightTextFilename(value:String):void
		{
			_highlightTextFilename = value;
			if (_highlightTextFilename) {
				dispatcher(new DocumentEvent(DocumentEvent.NEW_HIGHLIGHT_XML, _highlightTextFilename));
			}
		}


	}
}

