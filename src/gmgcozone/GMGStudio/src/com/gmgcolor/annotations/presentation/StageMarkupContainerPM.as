package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.domain.ApprovalAnnotationMarkupFO;
import com.gmgcolor.annotations.domain.ShapeFO;
import com.gmgcolor.annotations.event.DrawToolsEvent;
import com.gmgcolor.annotations.event.MarkerContainerMouseEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.event.MouseMoveDrawingEvent;
import com.gmgcolor.annotations.event.MouseUpDrawingEvent;
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.annotations.event.SaveBitmapDataEvent;
import com.gmgcolor.annotations.event.StartDrawingEvent;
import com.gmgcolor.annotations.event.TileContainerClickedEvent;
import com.gmgcolor.annotations.model.AnnotationModel;
import com.gmgcolor.annotations.model.CrossHairModel;
import com.gmgcolor.annotations.model.DocumentContainerModel;
import com.gmgcolor.annotations.model.DocumentModel;
import com.gmgcolor.annotations.model.StageMarkupContainerModel;
import com.gmgcolor.application.event.AnnotationToolsEvent;
import com.gmgcolor.application.model.AnnotationToolsModel;
import com.gmgcolor.application.model.DrawingToolsModel;
import com.gmgcolor.application.presentation.AnnotationToolsPM;
import com.gmgcolor.application.presentation.FXGConvertor;
import com.gmgcolor.utils.FXGUtil;
import com.greensock.events.TransformEvent;
import com.greensock.events.TransformSelectedEvent;
import com.greensock.transform.TransformItem;
import com.greensock.transform.TransformManager;

import flash.display.CapsStyle;
import flash.display.GraphicsPathCommand;
import flash.display.JointStyle;
import flash.display.LineScaleMode;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.core.IVisualElement;

import org.openzoom.flex.components.MultiScaleContainer;
import org.openzoom.flex.components.MultiScaleImage;

[InjectConstructor]
	public class StageMarkupContainerPM extends BaseUIPM {
		[MessageDispatcher]
		public var dispatcher:Function;
		public var transformManager:TransformManager = new TransformManager({forceSelectionToFront: true, allowDelete: true, autoDeselect: true, allowMultiSelect: false});

		private var _view:StageMarkerContainer;
		private var _annotationModel:AnnotationModel;
		private var _selectedComment:ApprovalAnnotationFO;
		private var _currentTransformItem:TransformItem;
		private var _square_commands:Vector.<int> = new Vector.<int>;
		private var _square_coord:Vector.<Number> = new Vector.<Number>;
		private var _crossHairModel:CrossHairModel;
		private var _createdItem:FXGConvertor;
		private var _currentApprovalAnnotationMarkupFO:ApprovalAnnotationMarkupFO;
		private var _documentModel:DocumentModel;
		private var _documentContainerModel:DocumentContainerModel;
		private var _drawingToolsModel:DrawingToolsModel;
		private var _annotationToolsModel:AnnotationToolsModel;
		private var _penWidth:Number;
		private var _penColor:Number;
		private var _penAlpha:Number;
		private var _fillColor:Number;
		private var _fillAlpha:Number;
		private var _mousePoint:Point;
		private var _shiftPressed:Boolean;

		namespace svgns = "http://www.w3.org/2000/svg";

		use namespace svgns;

		public function StageMarkupContainerPM(stageMarkupContainerModel:StageMarkupContainerModel, documentModel:DocumentModel, documentContainerModel:DocumentContainerModel, annotationModel:AnnotationModel, crossHairModel:CrossHairModel, drawingToolsModel:DrawingToolsModel, annotationToolsModel:AnnotationToolsModel)
		{
			_model = stageMarkupContainerModel;
			_documentModel = documentModel;
			_documentContainerModel = documentContainerModel;
			_annotationModel = annotationModel;
			_crossHairModel = crossHairModel;
			this._drawingToolsModel = drawingToolsModel;
			this._annotationToolsModel = annotationToolsModel;
			this._drawingToolsModel.addEventListener(DrawingToolsModel.CURRENTDRAWINGTOOL_CHANGE_EVENT, onCurrentAnnotationToolChange);
			this._annotationToolsModel.addEventListener(AnnotationToolsModel.CURRENTANNOTATIONTOOL_CHANGE_EVENT, onCurrentAnnotationToolChange);
			super();
		}

		private function onCurrentAnnotationToolChange(event:Event):void
		{
			if (_view) {
				if (_annotationToolsModel.currentAnnotationTool == AnnotationToolsPM.ANNOTATION_TOOL_STATE_MARKER
				/*_annotationToolsModel.currentAnnotationTool == AnnotationToolsPM.ANNOTATION_TOOL_STATE_TEXT ||*/
				/*_annotationToolsModel.currentAnnotationTool == AnnotationToolsPM.ANNOTATION_TOOL_STATE_DRAW*/) {
					_view.clickHandler.visible = true;
				}
				else {
					_view.clickHandler.visible = false;
				}
			}
		}

		public function onMouseDown(event:Event):void
		{
			if (_annotationToolsModel.currentAnnotationTool != AnnotationToolsPM.ANNOTATION_TOOL_STATE_MARKER) {
				event.stopImmediatePropagation(); // stop it from triggering mouse event on
			} else {
				_mousePoint = new Point(_view.stage.mouseX, _view.stage.mouseY);
			}
		}

		public function onMouseUp(event:Event):void
		{
			if (_annotationToolsModel.currentAnnotationTool == AnnotationToolsPM.ANNOTATION_TOOL_STATE_MARKER && Point.distance(_mousePoint, new Point(_view.stage.mouseX, _view.stage.mouseY)) < 5) {
				_documentContainerModel.localDocumentClickPoint = new Point(event.currentTarget.mouseX, event.currentTarget.mouseY);
				_documentContainerModel.globalDocumentClickPoint = new Point(event.currentTarget.stage.mouseX, event.currentTarget.stage.mouseY);
				dispatcher(new TileContainerClickedEvent(TileContainerClickedEvent.CLICK, _documentContainerModel.localDocumentClickPoint, _documentContainerModel.globalDocumentClickPoint));
			}
		}

		[MessageHandler(selector="deselect", scope="global")]
		public function onDeselect(event:AnnotationToolsEvent):void
		{
			transformManager.deselectAll();
		}

		[MessageHandler(selector="updateSelection", scope="global")]
		public function onUpdateSelection(event:AnnotationToolsEvent):void
		{
			onTmFinishInteractive(new TransformEvent(TransformEvent.FINISH_INTERACTIVE_SCALE, [_currentTransformItem]));
		}

		[Init]
		override public function init():void
		{
			super.init();
			_systemManager = FlexGlobals.topLevelApplication.systemManager.getSandboxRoot();

			transformManager.addEventListener(TransformEvent.SELECTION_CHANGE, onTmSelectionChange);
			transformManager.addEventListener(TransformEvent.DELETE, onTmDelete);
			transformManager.addEventListener(TransformEvent.FINISH_INTERACTIVE_MOVE, onTmFinishInteractive);
			transformManager.addEventListener(TransformEvent.FINISH_INTERACTIVE_ROTATE, onTmFinishInteractive);
			transformManager.addEventListener(TransformEvent.FINISH_INTERACTIVE_SCALE, onTmFinishInteractive);
		}

		[Bindable]
		[Subscribe]
		public function get selectedComment():ApprovalAnnotationFO
		{
			return _selectedComment;
		}

		public function set selectedComment(value:ApprovalAnnotationFO):void
		{
			_selectedComment = value;
			eraseMarkup();

			if (!value)
				return;

			var markups:Array = _selectedComment.ApprovalAnnotationMarkups;
			var markup:ApprovalAnnotationMarkupFO;

			if (markups) {
				for (var i:int = 0; i < markups.length; i++) {
					markup = markups[i];
					markup.isLocked = true;
					var shape:ShapeFO = markup.Shape;
					if (shape) {
						XML.ignoreWhitespace = true;
						var f:FXGConvertor = new FXGConvertor();
						f.xml = new XML(shape.FXG);
						f.approvalAnnotationMarkup = markup;
						f.addEventListener(Event.COMPLETE, onAddFXGCreationComplete);
						_view.addElement(f);
					}
				}
			}
		}

		[MessageHandler(selector="erase")]
		public function onEraseMarkup(event:MarkupEvent):void
		{
			eraseMarkup();
		}

		private function eraseMarkup():void
		{
			if (_view) {
				transformManager.removeAllItems();
				var element:IVisualElement;
				for (var i:uint = _view.numElements - 1; i > 0; i--) {
					element = _view.getElementAt(i);
					if (element is FXGConvertor) {
						_view.removeElementAt(i);
					}
				}
			}
		}

		// When a shape gets selected from the annotation tools
		[MessageHandler(selector="shapeClick", scope="global")]
		public function onShapeSelect(event:AnnotationToolsEvent):void
		{
			if (_documentModel.isMainViewSelected && !_documentModel.isUserLocked) {
				var f:FXGConvertor = new FXGConvertor;
				f.addEventListener(Event.COMPLETE, onFXGShapeComplete);
				f.xml = new XML(event.data);
				_view.addElement(f);
			}
		}

		[MessageHandler]
		public function onPostComment(event:NewCommentEvent):void
		{
			saveComment();
			transformManager.removeAllItems();
		}

		private function onFXGShapeComplete(event:Event):void
		{
			var d:FXGConvertor = _createdItem = FXGConvertor(event.currentTarget);
			d.removeEventListener(Event.COMPLETE, onFXGShapeComplete);

			_currentTransformItem = transformManager.addItem(d);

            if(_view.parent instanceof MultiScaleContainer)
            {
                // Resize item to a ratio of view
                var container:MultiScaleContainer = MultiScaleContainer(_view.parent);
                var image:MultiScaleImage = MultiScaleImage(container.parent);
                var sizeRatio:Number = image.zoomPercent / 1.5;
                _currentTransformItem.width /= sizeRatio;
                _currentTransformItem.height /= sizeRatio;

                // Center on view
                var point:Point = _view.globalToLocal(image.localToGlobal(new Point(container.viewportWidth/2, container.viewportHeight/2)));
                d.x = point.x - _currentTransformItem.width/2;
                d.y = point.y - _currentTransformItem.height/2;
            }
            else
            {
                var point:Point = new Point(_view.parent.width/2, _view.parent.width/2);
                d.x = point.x - _currentTransformItem.width/2;
                d.y = point.y - _currentTransformItem.height/2;
            }

			saveDrawing();
		}

		private function onAddFXGCreationComplete(event:Event):void
		{
			var d:FXGConvertor = _createdItem = FXGConvertor(event.currentTarget);
			d.removeEventListener(Event.COMPLETE, onAddFXGCreationComplete);

			_currentTransformItem = transformManager.addItem(d);
			_currentTransformItem.scaleX = d.approvalAnnotationMarkup.ScaleX;
			_currentTransformItem.scaleY = d.approvalAnnotationMarkup.ScaleY;
			_currentTransformItem.x = d.approvalAnnotationMarkup.X;
			_currentTransformItem.y = d.approvalAnnotationMarkup.Y;
			_currentTransformItem.rotation = d.approvalAnnotationMarkup.Rotation;
			d.approvalAnnotationMarkup.transformItem = _currentTransformItem;
			_currentTransformItem.enabled = false;
		}

		private function onFXGDrawingCreationComplete(event:Event):void
		{
			var d:FXGConvertor = _createdItem = FXGConvertor(event.currentTarget);
			d.removeEventListener(Event.COMPLETE, onFXGDrawingCreationComplete);
			// Clear graphics
			_view.graphics.clear();

			_currentTransformItem = transformManager.addItem(d);

			saveDrawing();
		}

		private function selectItem():void
		{
			transformManager.selectItem(_currentTransformItem);
		}

		private function onTmSelectionChange(event:TransformEvent):void
		{
			if (event.items && event.items.length != 0 && event.items[0].selected) {
				_currentTransformItem = event.items[0];
				if (_currentTransformItem && _selectedComment) {
					for each (var approvalAnnotationMarkupFO:ApprovalAnnotationMarkupFO in _selectedComment.ApprovalAnnotationMarkups) {
						if (approvalAnnotationMarkupFO.transformItem == _currentTransformItem) {
							_currentApprovalAnnotationMarkupFO = approvalAnnotationMarkupFO;
							break;
						}
					}

					if (_currentApprovalAnnotationMarkupFO) {
						dispatcher(new TransformSelectedEvent(TransformSelectedEvent.SELECTED, _currentTransformItem, _currentApprovalAnnotationMarkupFO.Shape.isDrawing));
					}
				}
			}
		}

		private function onTmDelete(event:TransformEvent):void
		{
			for (var i:uint = 0, len:uint = _selectedComment.ApprovalAnnotationMarkups.length; i < len; i++) {
				if (_selectedComment.ApprovalAnnotationMarkups[i].transformItem == _currentTransformItem) {
					_selectedComment.ApprovalAnnotationMarkups.splice(i, 1);
					break;
				}
			}
		}

		private function onTmFinishInteractive(event:TransformEvent):void
		{
			var fxgConverter:FXGConvertor = FXGConvertor(event.items[0].targetObject);

			if (fxgConverter.approvalAnnotationMarkup) {
				fxgConverter.approvalAnnotationMarkup.X = _currentTransformItem.x;
				fxgConverter.approvalAnnotationMarkup.Y = _currentTransformItem.y;
				fxgConverter.approvalAnnotationMarkup.ScaleX = _currentTransformItem.scaleX;
				fxgConverter.approvalAnnotationMarkup.ScaleY = _currentTransformItem.scaleY;
				fxgConverter.approvalAnnotationMarkup.Rotation = _currentTransformItem.rotation;

				// Set crosshair x,y
				if (_selectedComment) {
					var xcoord:Number = _currentTransformItem.x + _currentTransformItem.width / 2;
					var ycoord:Number = _currentTransformItem.y + _currentTransformItem.height / 2;
					_crossHairModel.xPos = _selectedComment.CrosshairXCoord = xcoord < 0 ? 0 : xcoord;
					_crossHairModel.yPos = _selectedComment.CrosshairYCoord = ycoord < 0 ? 0 : ycoord;
				}
				fxgConverter.approvalAnnotationMarkup.Shape.SVG = convertToSVG(fxgConverter.approvalAnnotationMarkup);
			}
		}


		[MessageHandler]
		public function onStartDrawing(event:StartDrawingEvent):void
		{
			_view.stage.focus = null;

			var comments:ArrayCollection = _annotationModel.annotations;
			var exists:Boolean = comments.contains(_selectedComment);

			if (!exists && _selectedComment != null) {
				var pt:Point = event.pt;
				_view.graphics.clear();

				_square_commands = new Vector.<int>;
				_square_coord = new Vector.<Number>;

				_square_commands.push(GraphicsPathCommand.MOVE_TO);
				_square_coord.push(pt.x);
				_square_coord.push(pt.y);
			}
		}

		[MessageHandler]
		public function onMouseMoveDrawing(event:MouseMoveDrawingEvent):void
		{
			var pt:Point = event.pt;

			_square_commands.push(GraphicsPathCommand.LINE_TO);
			_square_coord.push(pt.x);
			_square_coord.push(pt.y);

			drawLine();
		}

		private function drawLine():void
		{
			if (_square_commands && _square_coord) {
				_view.graphics.clear();
				_view.graphics.lineStyle(_penWidth, _penColor, _penAlpha, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 3);

				if (_shiftPressed) {
					var commands:Vector.<int> = new Vector.<int>;
					var coord:Vector.<Number> = new Vector.<Number>;
					commands.push(GraphicsPathCommand.MOVE_TO);
					commands.push(GraphicsPathCommand.LINE_TO);
					coord.push(_square_coord[0]);
					coord.push(_square_coord[1]);
					coord.push(_square_coord[_square_coord.length - 2]);
					coord.push(_square_coord[_square_coord.length - 1]);
					_view.graphics.drawPath(commands, coord);
				} else {
					_view.graphics.drawPath(_square_commands, _square_coord);
				}
			}
		}

		[MessageHandler]
		public function onMouseUpDrawing(event:MouseUpDrawingEvent):void
		{
			if (_shiftPressed) {
				var commands:Vector.<int> = new Vector.<int>;
				var coord:Vector.<Number> = new Vector.<Number>;
				commands.push(GraphicsPathCommand.MOVE_TO);
				commands.push(GraphicsPathCommand.LINE_TO);
				coord.push(_square_coord[0]);
				coord.push(_square_coord[1]);
				coord.push(_square_coord[_square_coord.length - 2]);
				coord.push(_square_coord[_square_coord.length - 1]);
				_square_commands = commands;
				_square_coord = coord;
			}
			convertToFXGXML();

			_square_commands = null;
			_square_coord = null;

			if (_annotationModel.showAllAnnotations) {
				_annotationModel.showAllAnnotations = false;
			}
		}

		private function convertToFXGXML():void
		{
			var myColor:uint = _penColor;
			var myHEXColor:String = "#" + myColor.toString(16);
			XML.ignoreWhitespace = true;
			var template:XML = FXGUtil.createFXGTemplate();
			template.appendChild(
					<Path winding="nonZero" data="">
						<stroke>
							<SolidColorStroke weight="5" caps="round" joints="round" pixelHinting="true" scaleMode="normal" miterLimit="3" color="#231F20" alpha="1"/>
						</stroke>
					</Path>
			);

			var xmlfxgPaths:XMLList = template..*::SolidColorStroke;
			var xml:XML = xmlfxgPaths[0];

			xml.@weight = String(_penWidth);
			xml.@alpha = String(_penAlpha);
			xml.@color = myHEXColor;

			xmlfxgPaths = template..*::Path;
			xml = xmlfxgPaths[0];

			var startX:int = _square_coord[0];
			var startY:int = _square_coord[1];

			var count:int;

			for each (var i:int in _square_commands) {
				switch (i) {
					case GraphicsPathCommand.WIDE_MOVE_TO:
					case GraphicsPathCommand.MOVE_TO:
						xml.@data += "M "
						break;
					case GraphicsPathCommand.LINE_TO:
					case GraphicsPathCommand.WIDE_LINE_TO:
						xml.@data += "L "
						break;
					case GraphicsPathCommand.CURVE_TO:
					case GraphicsPathCommand.CUBIC_CURVE_TO:
						xml.@data += "C "
						break;
				}

				var spot:int = 2 * count;
				xml.@data += String(_square_coord[spot] - startX) + " ";
				xml.@data += String(_square_coord[spot + 1] - startY) + " ";
				count++;
			}

			var f:FXGConvertor = new FXGConvertor();
			f.addEventListener(Event.COMPLETE, onFXGDrawingCreationComplete);
			f.approvalAnnotationMarkup.Shape.isDrawing = true;
			f.xml = template;
			f.x = _square_coord[0];
			f.y = _square_coord[1];
			_view.addElement(f);
		}

		private function saveDrawing():void
		{
			if (!_selectedComment || !_currentTransformItem)
				return;

			var approvalAnnotationMarkup:ApprovalAnnotationMarkupFO = FXGConvertor(_currentTransformItem.targetObject).approvalAnnotationMarkup;

			approvalAnnotationMarkup.X = _currentTransformItem.x;
			approvalAnnotationMarkup.Y = _currentTransformItem.y;
			approvalAnnotationMarkup.ScaleX = _currentTransformItem.scaleX;
			approvalAnnotationMarkup.ScaleY = _currentTransformItem.scaleY;
			approvalAnnotationMarkup.Rotation = _currentTransformItem.rotation;
			approvalAnnotationMarkup.createdItem = _createdItem;
			approvalAnnotationMarkup.transformItem = _currentTransformItem;

			isMarkUpsVisible(false);

			approvalAnnotationMarkup.Shape.FXG = _createdItem.xml;
			approvalAnnotationMarkup.Shape.IsCustom = true;
			approvalAnnotationMarkup.Shape.SVG = convertToSVG(approvalAnnotationMarkup);

			isMarkUpsVisible(true);

			var exists:Boolean = _annotationModel.annotations.contains(_selectedComment);
			if (!exists && _selectedComment != null) {
				if (_selectedComment.ApprovalAnnotationMarkups == null)
					_selectedComment.ApprovalAnnotationMarkups = new Array();
				var xcoord:Number = _currentTransformItem.x + _currentTransformItem.width / 2;
				var ycoord:Number = _currentTransformItem.y + _currentTransformItem.height / 2;
				_crossHairModel.xPos = _selectedComment.CrosshairXCoord = xcoord < 0 ? 0 : xcoord;
				_crossHairModel.yPos = _selectedComment.CrosshairYCoord = ycoord < 0 ? 0 : ycoord;
			}

			if (_selectedComment) {
				_selectedComment.ApprovalAnnotationMarkups.push(approvalAnnotationMarkup);
			}

			StageMarkupContainerModel(_model).markUp = _selectedComment.ApprovalAnnotationMarkups;

			_annotationModel.showAllAnnotations = true;

			_view.callLater(selectItem);

			if (_selectedComment.ApprovalAnnotationMarkups.length == 1)
				dispatcher(new MarkupEvent(MarkupEvent.FIRST_MARKUP));
		}

		private function saveComment():void
		{
			dispatcher(new SaveBitmapDataEvent(SaveBitmapDataEvent.SAVE, _createdItem, new Point(_currentTransformItem.x, _currentTransformItem.y), _currentTransformItem.width, _currentTransformItem.height));
		}

		public function registerView(view:StageMarkerContainer):void
		{
			this._view = view

			// Prevent events from propagating to MultiScaleImage
			this._view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this._view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			this._view.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}

		private function onKeyDown(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.SHIFT) {
				_shiftPressed = true;
				drawLine();
			}
		}

		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.SHIFT) {
				_shiftPressed = false;
				drawLine();
			}
		}

		[MessageHandler(selector="mouseOver")]
		public function onMarkerOver(event:MarkerContainerMouseEvent):void
		{
			_view.validateNow();
			var pt:Point = new Point(event.globalRect.x, event.globalRect.y);
			pt = _view.parent.globalToLocal(pt);
			width = event.globalRect.width;
			height = event.globalRect.height;
			xPos = pt.x;
			yPos = pt.y;
		}

		[MessageHandler(selector="mouseOut")]
		public function onMarkerOut(event:MarkerContainerMouseEvent):void
		{
			_view.validateNow();
		}

		[MessageHandler(selector="toolChange", scope="global")]
		public function onDrawToolsEvent(event:DrawToolsEvent):void
		{
			_fillAlpha = event.value.fillAlpha;
			_fillColor = event.value.fillColor;
			_penColor = event.value.strokeColor;
			_penWidth = event.value.strokeWeight;
			_penAlpha = event.value.strokeAlpha;
		}

		private function isMarkUpsVisible(value:Boolean):void
		{
			for (var i:int = 0; i < _view.numElements; i++) {
				if (_view.getElementAt(i) is MarkerContainer) {
					var element:MarkerContainer = MarkerContainer(_view.getElementAt(i));
					element.visible = value;
				}
			}
		}

		private function convertToSVG(approvalAnnotationMarkup:ApprovalAnnotationMarkupFO):String
		{
			var svg:XML = FXGUtil.setSVGSize(FXGUtil.toSVG(new XML(approvalAnnotationMarkup.Shape.FXG)), _view.width, _view.height);
			var group:XML = svg.svgns::g[0];
			var item:TransformItem = approvalAnnotationMarkup.transformItem;
			group.@transform += 'translate(' + _currentTransformItem.x + ', ' + _currentTransformItem.y + ') '
			if (item.scaleX != 1 || item.scaleY != 1) {
                for (var index:* in svg..*::path.@transform)
                {
                    svg..*::path.@transform[index] += 'scale(' + item.scaleX + ', ' + item.scaleY + ') ';
                }
			}
			if (item.rotation != 0) {
				group.@transform += 'rotate(' + item.rotation + ', ' + item.width / 2 + ', ' + item.height / 2 + ') ';
			}

			return svg.toXMLString();
		}
	}
}
