package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.model.DocumentModel;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;

import flash.events.Event;

import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.events.ResizeEvent;

import org.osmf.metadata.CuePoint;
import org.osmf.metadata.TimelineMetadata;

public class AnnotationMarkersPM extends Presenter
    {

        [Inject]
        public var documentModel:DocumentModel;

        private var _view:AnnotationMarkers;
        private var _addedMarkers:Object = {};
        private var _videoPlayerInitialized:Boolean = false;
        private var _annotations:ArrayCollection;
        private var _addedAnnotations:Array = [];
        private var _dynamicTimelineMetadata:TimelineMetadata;
        private var _isVideo:Boolean = false;
        public function AnnotationMarkersPM()
        {

        }

        public function registerView(view:AnnotationMarkers):void
        {
            _view = view;
            _view.addEventListener(ResizeEvent.RESIZE, onResize);
        }

        [MessageHandler(selector="selectCompleted", scope="global")]
        public function onDocumentSelected(e:DocumentEvent):void
        {
            if(_videoPlayerInitialized && _view)
            {
                initMetadata();
            }
        }

        private function initMetadata():void
        {
            if(documentModel.videoPlayer)
            {
                _isVideo = true;
            }
            _addedMarkers = {};
            _addedAnnotations = [];
            _view.container.removeAllElements();

            if(_isVideo)
            {
                documentModel.videoPlayer.mediaPlayer.media.removeMetadata(CuePoint.DYNAMIC_CUEPOINTS_NAMESPACE);
                _dynamicTimelineMetadata = new TimelineMetadata(documentModel.videoPlayer.mediaPlayer.media);
                documentModel.videoPlayer.mediaPlayer.media.addMetadata
                        (CuePoint.DYNAMIC_CUEPOINTS_NAMESPACE, _dynamicTimelineMetadata);
            }
            draw();
        }

        [MessageHandler]
        public function onAnnotationsChange(event:AnnotationsChangeEvent):void
        {
            _annotations = event.annotations;
            if(_videoPlayerInitialized)
            {
                draw();
            }
            else
            {
                documentModel.addEventListener(DocumentModel.VIDEO_DURATION_CHANGE_EVENT, onVideoPlayerReady)
            }
        }

        private function onVideoPlayerReady(e:Event):void
        {
            if(!isNaN(documentModel.videoDuration) && documentModel.videoDuration > 0 && !_videoPlayerInitialized)
            {
                _videoPlayerInitialized = true;
                initMetadata();
            }
        }

        private function onResize(event:ResizeEvent):void
        {
            redraw();
        }

        private function draw():void
        {
            var count:uint = 1;
            var marker:AnnotationMarker;
            var deleteIndexes:Array = [];
            // loop over added annotations to remove deleted ones
            for each(var addedAnnotation:ApprovalAnnotationFO in _addedAnnotations)
            {
                if(_annotations.getItemIndex(addedAnnotation) < 0)
                {
                    var time:String = String(Math.round(addedAnnotation.TimeFrame / 1000));
                    marker = _addedMarkers[time];
                    if(marker.count > 1)
                    {
                        marker.count--;
                    }
                    else
                    {
                        if(_isVideo)
                        {
                            _dynamicTimelineMetadata.removeMarker(addedAnnotation.cuePoint);
                        }
                        _view.container.removeElement(marker);
                        delete _addedMarkers[time];
                    }
                    deleteIndexes.push(_addedAnnotations.indexOf(addedAnnotation));
                }
            }
            if(deleteIndexes.length)
            {
                for each(var index:uint in deleteIndexes)
                {
                    delete _addedAnnotations[index];
                }
            }

            for each (var annotation:ApprovalAnnotationFO in _annotations)
            {
                if(annotation.Parent)
                {
                    continue;
                }
                var timeString:String = String(Math.round(annotation.TimeFrame / 1000));
                if(_addedMarkers.hasOwnProperty(timeString))
                {
                    if(_addedAnnotations.indexOf(annotation) < 0)
                    {
                        _addedMarkers[timeString].count++;
                        _addedAnnotations.push(annotation);
                    }
                    continue;
                }
                else
                {
                    count = 1;
                }
                marker = new AnnotationMarker();
                marker.time = annotation.TimeFrame / 1000;
                marker.count = count;
                var duration:Number;
                if(_isVideo)
                {
                    duration = documentModel.videoDuration;
                }
                else
                {
                    duration = documentModel.totalFrames;
                }
                marker.x = _view.width * Number(timeString) / duration - marker.width / 2 ;
                marker.y = 0;
                marker.addEventListener(MouseEvent.CLICK, onMarkerClick);
                _view.container.addElement(marker);
                _addedMarkers[timeString] = marker;
                if(_isVideo)
                {
                    _dynamicTimelineMetadata.addMarker(annotation.cuePoint);
                }
                _addedAnnotations.push(annotation);
            }
        }

        private function redraw():void
        {
            for each (var marker:AnnotationMarker in _addedMarkers)
            {
                marker.x = _view.width * Math.round(marker.time) / documentModel.videoDuration - marker.width / 2 ;
            }
        }

        private function onMarkerClick(e:MouseEvent):void
        {
            dispatcher(new DocumentControlsEvent(DocumentControlsEvent.ANNOTATION_MARKER_CLICK, e.target.time));
        }
    }
}
