package com.gmgcolor.annotations.presentation
{
import com.gmgcolor.application.domain.ApprovalPage;

public interface IDocumentModel
	{
		 function get page():ApprovalPage
		 function set page(page:ApprovalPage):void
	}
	

}
