package com.gmgcolor.annotations.presentation {
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.model.AnnotationModel;
import com.gmgcolor.annotations.model.DocumentModel;

import flash.events.Event;

import mx.events.ResizeEvent;
import mx.graphics.SolidColorStroke;

import org.osmf.events.TimeEvent;

import spark.primitives.Line;

public class VideoTimelinePM extends Presenter
    {
        [Inject]
        public var _annotationModel:AnnotationModel;

        [Inject]
        public var documentModel:DocumentModel;

        private var _view:VideoTimeline;
        private var _videoPlayerInitialized:Boolean = false;

        public function VideoTimelinePM()
        {
        }

        public function registerView(view:VideoTimeline):void
        {
            _view = view;
            _view.addEventListener(ResizeEvent.RESIZE, onResize);
            if(_videoPlayerInitialized)
            {
                draw();
            }
        }

        private function onResize(e:Event):void
        {
            if(_videoPlayerInitialized)
            {
                draw();
            }
            else
            {
                documentModel.addEventListener(DocumentModel.VIDEO_DURATION_CHANGE_EVENT, onVideoPlayerReady)
            }
        }

        private function onVideoPlayerReady(e:Event):void
        {
            if(!isNaN(documentModel.videoDuration) && documentModel.videoDuration > 0 && !_videoPlayerInitialized)
            {
                _videoPlayerInitialized = true;
                draw();
            }
        }

        [MessageHandler(selector="selected")]
        public function onDocumentSelected(e:DocumentEvent):void
        {
            _videoPlayerInitialized = false;
        }

        private static function drawLine(x:Number, bottomY:Number, height:Number):Line
        {
            var stroke:SolidColorStroke = new SolidColorStroke(0x000000, 1);
            var line:Line = new Line();
            line.xFrom = line.xTo = x;
            line.yFrom = bottomY;
            line.yTo = bottomY - height;
            line.stroke = stroke;
            return line;
        }

        private function draw():void
        {
            _view.container.removeAllElements();
            var duration:Number = Math.round(documentModel.videoDuration);
            var width:Number = _view.width;
            var height:Number = _view.height;
            var distance:Number = width / duration;
            var multiplier:uint = 2;
            while(distance < 5)
            {
                distance = width / duration * multiplier;
                multiplier++;
            }
            var currentX:Number = 0;
            var counter:uint = 0;
            do
            {
                counter++;
                if(currentX < width - 1)
                {
                    _view.container.addElement(drawLine(Math.round(currentX), height, getLineHeight(counter)));
                }
                currentX += distance;
            } while(currentX < width);
            _view.container.addElement(drawLine(width - 1, height, getLineHeight(counter + 1)));
        }

        private static function getLineHeight(count:uint):uint
        {
            if(!(count % 11) || count == 1)
            {
                return 20
            }
            else if(!(count % 6))
            {
                return 13;
            }
            return 10;
        }
    }
}
