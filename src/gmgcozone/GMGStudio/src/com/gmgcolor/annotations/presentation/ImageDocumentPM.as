package com.gmgcolor.annotations.presentation {

import com.gmgcolor.annotations.domain.*;
import com.gmgcolor.annotations.enum.AnnotationEnum;
import com.gmgcolor.annotations.event.*;
import com.gmgcolor.annotations.model.*;
import com.gmgcolor.application.enums.ApplicationEnum;
import com.gmgcolor.application.event.EyeDropperToolChangeEvent;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;

import flash.events.Event;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;

import org.openzoom.flash.events.MultiScaleEvent;
import org.spicefactory.parsley.core.context.Context;

[InjectConstructor]
	public class ImageDocumentPM extends DocumentPM {

		private var _view:ImageDocument;

		public function get view():ImageDocument
		{
			return _view;
		}

		public function set view(value:ImageDocument):void
		{
			_view = value;
		}

		public function ImageDocumentPM(context:Context, documentModel:DocumentModel, annotationModel:AnnotationModel, crossHairModel:CrossHairModel, commentsModel:CommentsModel, markupContainerModel:MarkupContainerModel, documentContainerModel:DocumentContainerModel, lineModel:LineContainerModel)
		{
			super(
                context,
                documentModel,
                annotationModel,
                crossHairModel,
                commentsModel,
                markupContainerModel,
                documentContainerModel,
                lineModel
            );
		}

		public function registerView(view:ImageDocument):void
		{
			this.view = view;
			dispatcher(new DocumentViewRegisteredEvent(DocumentViewRegisteredEvent.REGISTERED, _view));
			view.imageViewer.addEventListener(MultiScaleEvent.SCALE_CHANGE, onSyncChange);
			view.imageViewer.addEventListener(MultiScaleEvent.ZOOM_CHANGE, onSyncChange);
			view.imageViewer.addEventListener(MultiScaleEvent.VIEWPORT_UPDATE, onSyncChange);
			view.imageViewer.addEventListener(MultiScaleEvent.SCENE_CHANGE, onSyncChange);
			view.imageViewer.addEventListener(MultiScaleEvent.SCENE_UPDATE, onSyncChange);
			view.imageViewer.addEventListener(MultiScaleEvent.SCENE_RESIZE, onSyncChange);
			view.imageViewer.addEventListener(MultiScaleEvent.SOURCE_CHANGE, onSyncChange);
			if (!view.initialized) {
				view.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
			} else {
				onCreationComplete();
			}
		}

		private function onCreationComplete(e:Event = null):void
		{
			view.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
			drawn = false;
			_commentsModel.xPos = _view.width - _commentsModel.width - ApplicationEnum.DEFAULT_COMMENTS_PADDING;
			_commentsModel.yPos = 0 + ApplicationEnum.DEFAULT_COMMENTS_PADDING;

			dispatcher(new DocumentEvent(DocumentEvent.READY, this));
		}

		private function onSyncChange(event:Event):void
		{
			drawLine();
			if (!_syncing && _isSyncMode) {
				_syncing = true;
				sync();
			}
		}

		private function sync():void
		{
			if (_isSyncMode && isMainViewSelected) {
				dispatchSync();
			}
			_syncing = false;
		}

		public function showComment():void
		{
			if (isMainViewSelected && !_commentsModel.isOn) {
				_commentsModel.isOn = true;
				_commentsModel.width = ApplicationEnum.DEFAULT_COMMENT_WIDTH;
				_commentsModel.xPos = _view.width - _commentsModel.width - ApplicationEnum.DEFAULT_COMMENTS_PADDING;
				_commentsModel.yPos = 0 + ApplicationEnum.DEFAULT_COMMENTS_PADDING;
				_commentsModel.height = NaN;
                drawLine();
				dispatcher(new ShowCommentEvent(ShowCommentEvent.SHOW));
			}
		}

		[MessageHandler(selector="syncButtonClick", scope="global")]
		public function onSyncButtonClick(event:DocumentControlsEvent):void
		{
			if (_isMainViewSelected) {
				dispatchSync();
			}
		}

		[MessageHandler(selector="lockButtonClick", scope="global")]
		public function onLockButtonClick(event:DocumentControlsEvent):void
		{
			_isSyncMode = event.value;
			if (_isMainViewSelected && _isSyncMode) {
				dispatchSync();
			}
		}

		private function dispatchSync():void
		{
			dispatcher(new DocumentSyncEvent(DocumentSyncEvent.SYNC,
					_view.imageViewer.zoom,
					new Point(_view.imageViewer.viewportX, _view.imageViewer.viewportY),
					_view.imageViewer.rotation,
					approvalModel.currentApprovalPages.getItemIndex(approvalModel.currentPage),
					page.SelectedApprovalSeparationIndex
			));
		}

		[MessageHandler(scope="global")]
		public function onDocumentSync(event:DocumentSyncEvent):void
		{
			if (!isMainViewSelected) {
				approvalModel.currentPageIndex = event.page;
				approvalModel.currentPage.SelectedApprovalSeparationIndex = page.SelectedApprovalSeparationIndex = event.selectedSeparationIndex;
				dispatcher(new DocumentEvent(DocumentEvent.NEW_TILED_DOCUMENT, filename));
				_view.zoomTo(event.zoom); // always zoom first
				_view.panTo(event.position.x, event.position.y);
				_view.rotateTo(event.rotate);
			}
		}

		[MessageHandler(selector="firstMarkup")]
		public function onFirstMarkupEvent(event:MarkupEvent):void
		{
			if (isMainViewSelected) {
				showComment()
				_commentsModel.submittable = true;
			}
		}

		[MessageHandler(selector="move")]
		public function onCrossHairMove(event:CrossHairEvent):void
		{
			if (isMainViewSelected) {
				drawLine();
			}
		}

		[Subscribe]
		public function set selectedAnnotation(value:ApprovalAnnotationFO):void
		{
			if (!isMainViewSelected)
				return;

			var isOn:Boolean = value && _commentsModel.isOn && value.ID > 0;

			_selectedAnnotation = value;


			clearAll();

			dispatcher(new AnnotationSelectedEvent(AnnotationSelectedEvent.SELECTED, value));

			_commentsModel.annotations = _annotationModel.annotations;

			if (_selectedAnnotation == null)
				return;

			var show:Boolean;


			var annotations:ArrayCollection = _annotationModel.annotations;

			//exisiting annotation
			if (annotations.contains(_selectedAnnotation)) {
				_commentsModel.isOn = isOn;

				_crossHairModel.scaleX /= _crossHairModel.scaleX;
				_crossHairModel.scaleY /= _crossHairModel.scaleY;
				_crossHairModel.localPoint = new Point(_selectedAnnotation.CrosshairXCoord, _selectedAnnotation.CrosshairYCoord);

				//Loop over annoations to find child annotation.

				var childAnnotations:ArrayCollection = getChildAnnotations(annotations);

				_commentsModel.childAnnotations = childAnnotations;

				_commentsModel.annotation = _selectedAnnotation;

				if (_selectedAnnotation.CommentType == AnnotationEnum.ANNOTATION_TYPE_MARKER) {
					_crossHairModel.isOn = true;
					_crossHairModel.isMoveable = _selectedAnnotation.ID == 0;
					drawLine();
				}

				/*	_commentsModel.xPos = _selectedAnnotation.annotationXCoord;
				 _commentsModel.yPos = _selectedAnnotation.annotationYCoord;*/
				_commentsModel.submittable = true;
				var markups:Array = _selectedAnnotation.ApprovalAnnotationMarkups;
				var markup:ApprovalAnnotationMarkupFO;

				if (markups) {
					for (var i:int = 0; i < markups.length; i++) {
						markup = markups[i];
						markup.isLocked = true;
						/*			image = new Image();
						 image.source = markup.dataByteArray;
						 image.width = markup.width;
						 image.height = markup.height;
						 image.move(markup.x + 12, markup.y + 20);*/
						dispatcher(new MarkupImageEvent(MarkupImageEvent.ADD, markup));
					}
				}

				if (_selectedAnnotation.CommentType == AnnotationEnum.ANNOTATION_TYPE_TEXT) {
					dispatcher(new RedrawHighlightEvent(RedrawHighlightEvent.REDRAW,
							_selectedAnnotation.StartIndex,
							_selectedAnnotation.EndIndex,
							_selectedAnnotation.HighlightType));
				}
			}
			else {
				_commentsModel.childAnnotations = null;
				_commentsModel.annotation = null;

				switch (_selectedAnnotation.CommentType) {
					case AnnotationEnum.ANNOTATION_TYPE_DRAW:
					{
						break;
					}

					case AnnotationEnum.ANNOTATION_TYPE_MARKER:
					{
						_crossHairModel.isPlacing = true;
						_crossHairModel.scaleX /= _markupContainerModel.scaleX;
						_crossHairModel.scaleY /= _markupContainerModel.scaleY;
						_crossHairModel.xPos = _view.mouseX;
						_crossHairModel.yPos = _view.mouseY;
						break;
					}

					case AnnotationEnum.ANNOTATION_TYPE_TEXT:
					{
						dispatcher(new HighlightEvent(HighlightEvent.ENABLE));
						break;
					}

					default:
					{
						break;
					}
				}
			}

			showComment();

			dispatcher(new AnnotationEvent(AnnotationEvent.CREATE));
		}

		private function getChildAnnotations(annotations:ArrayCollection):ArrayCollection
		{
			var childAnnotations:ArrayCollection = new ArrayCollection;

			for each (var annotation:ApprovalAnnotationFO in annotations) {
				if (annotation.Parent == _selectedAnnotation.ID) {
					childAnnotations.addItem(annotation);
				}
			}
			return childAnnotations;
		}

		[MessageHandler(selector="move")]
		public function onCommentsMove(event:CommentsEvent):void
		{
			drawLine();
		}

		[MessageHandler(selector="mouseDown")]
		public function onCrossHairMouseDown(event:CrossHairEvent):void
		{
			if (!_commentsModel.isOn) {
				_crossHairModel.isMoveable = true;
				var pt:Point = new Point(_crossHairModel.xPos + 50, _crossHairModel.yPos - 50);
				pt = _view.globalToLocal(pt);
				if (pt.y < 0) pt.y = 0;
				if (pt.x > _view.width - ApplicationEnum.DEFAULT_COMMENT_WIDTH) pt.y = pt.y + 120;
				_commentsModel.xPos = pt.x;
				_commentsModel.yPos = pt.y;
				_commentsModel.isMoveable = true;
				_commentsModel.isOn = true;
				_commentsModel.submittable = true;
				//checkCommentInBounds();
				drawLine();
				dispatcher(new MarkupEvent(MarkupEvent.ENABLE));
				DocumentModel(_model).handModeOn = true;
			}
		}

		private function get filename():String
		{
			return page.SelectedApprovalSeparationIndex == 0 ? page.PageFolder : page.ApprovalSeparationPlates[page.SelectedApprovalSeparationIndex].FolderPath + "/";
		}

		public function drawLine(e:Event = null):void
		{
			if (_selectedAnnotation && _selectedAnnotation.ID != 0 && _crossHairModel.isOn) {
				_lineModel.isOn = true;
				var pt1:Point = _view.lineContainer.globalToLocal(_view.pins.localToGlobal(_crossHairModel.localPoint));
				var pt2:Point = new Point(_commentsModel.xPos, _commentsModel.yPos + 16);
				dispatcher(new DrawLineEvent(DrawLineEvent.DRAW, pt1, pt2));
			}
		}

		public function updateApprovalsXY():void
		{
			_view.callLater(updateApprovalsXY2);
		}

		public function updateApprovalsXY2():void
		{
			_documentContainerModel.xPos = _documentContainer.x - (12 * _markupContainerModel.scaleX);
			_documentContainerModel.yPos = _documentContainer.y - (20 * _markupContainerModel.scaleY);

			_crossHairModel.scaleX = 1 / _markupContainerModel.scaleX;
			_crossHairModel.scaleY = 1 / _markupContainerModel.scaleY;
			drawLine();
		}

		public function checkCommentInBounds():void
		{
			if (_commentsModel.height > _model.height) {
				_commentsModel.height = height;
			}

			var x:Number = (_commentsModel.xPos + _commentsModel.width - ApplicationEnum.DEFAULT_COMMENTS_PADDING) > _view.width ? _view.width - _commentsModel.width - ApplicationEnum.DEFAULT_COMMENTS_PADDING : _commentsModel.xPos;
			var y:Number = (_commentsModel.yPos + _commentsModel.height - ApplicationEnum.DEFAULT_COMMENTS_PADDING) > _view.height ? _view.height - _commentsModel.height - -ApplicationEnum.DEFAULT_COMMENTS_PADDING : _commentsModel.yPos;

			_commentsModel.xPos = x;
			_commentsModel.yPos = y;

			var rectA:Rectangle = new Rectangle(_commentsModel.xPos,
					_commentsModel.yPos,
					_commentsModel.width,
					_commentsModel.height);

			if (_crossHairModel.isOn && !_commentsModel.isSubmitted) {
				var pt:Point = new Point(_crossHairModel.xPos, _crossHairModel.yPos);
				/*				pt = approvals.localToGlobal(pt)
				 pt = globalToLocal(pt);*/

				var rectB:Rectangle = new Rectangle(pt.x, pt.y, _crossHairModel.width, _crossHairModel.height);
				var rectAIntersectsB:Boolean = rectA.intersects(rectB);

				if (rectAIntersectsB) {
					if (pt.x < _model.width / 2) {
						_commentsModel.xPos = _model.width - _commentsModel.width;
						_commentsModel.yPos = 0;
					}
					else if (pt.x >= width / 2) {
						_commentsModel.xPos = 1;
						_commentsModel.yPos = 1;
					}
				}
			}

			//see if the cross hair intersects the comment on startup
			if (_crossHairModel.isOn)
				drawLine();
		}

		private function clearAll():void
		{
			_annotationModel.selectedAnnotation = null;
			dispatcher(new AnnotationSelectedEvent(AnnotationSelectedEvent.SELECTED, null));
			_crossHairModel.isPlacing = false;
			_crossHairModel.isOn = false;
			_lineModel.isOn = false;
			_commentsModel.isOn = false;
			_crossHairModel.isOn = false;
			_crossHairModel.xPos = 0;
			_crossHairModel.yPos = 0;
			DocumentModel(_model).handModeOn = false;

			dispatcher(new HighlightTextEvent(HighlightTextEvent.ERASE));
			dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
			dispatcher(new EyeDropperToolChangeEvent(MarkupEvent.DISABLE));
			dispatcher(new HighlightEvent(HighlightEvent.DISABLE));
		}

//        [MessageHandler]
//        public function onPostComment(event:NewCommentEvent):void
//        {
//            if(view && view.presenter.selectedDocument is ImageDocument)
//            {
//                doPostComment(event, 0);
//            }
//        }
    }
}

