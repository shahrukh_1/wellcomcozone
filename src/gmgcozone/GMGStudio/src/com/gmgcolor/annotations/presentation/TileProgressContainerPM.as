package com.gmgcolor.annotations.presentation {
	[InjectConstructor]
	public class TileProgressContainerPM {

		[MessageDispatcher]
		public var dispatcher:Function;

		[Bindable]
		public var bytesLoaded:int;

		[Bindable]
		public var bytesTotal:int;

		[Bindable]
		public var percent:int;

		[Bindable]
		public var progressIsVisible:Boolean = false;

		public function TileProgressContainerPM()
		{
			super();
		}

		[Init]
		public function init():void
		{

		}

		/*[MessageHandler]
		 public function onMessageHandler(event:TileProgressEvent):void
		 {
		 percent = int(event.bytesLoaded / event.bytesTotal * 100);
		 progressIsVisible = percent != 0;
		 }*/
	}
}