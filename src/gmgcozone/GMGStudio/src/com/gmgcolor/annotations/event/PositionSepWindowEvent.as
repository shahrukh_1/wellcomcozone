package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.presentation.SepsPopUpMenu;

import flash.events.Event;

public class PositionSepWindowEvent extends Event
	{
		public static const POSITION:String = "position";

		private var _sepsPopUpMenu:SepsPopUpMenu;

		public function PositionSepWindowEvent(type:String, sepsPopUpMenu:SepsPopUpMenu)
		{
			super(type,false,false);

			this._sepsPopUpMenu = sepsPopUpMenu;
		}

		public function get sepsPopUpMenu():SepsPopUpMenu
		{
			return _sepsPopUpMenu;
		}

		public override function clone():Event
		{
			return new PositionSepWindowEvent(type,sepsPopUpMenu);
		}

		public override function toString():String
		{
			return formatToString("PositionSepWindowEvent","sepsPopUpMenu");
		}
	}
}
