package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.presentation.ColorInfoContainer;

import flash.events.Event;

public class ActiveColorStageContainerEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _active:Boolean;
		private var _container:ColorInfoContainer;

		public function ActiveColorStageContainerEvent(type:String, active:Boolean, container:ColorInfoContainer)
		{
			super(type,false,false);

			this._active = active;
			this._container = container;
		}

		public function get active():Boolean
		{
			return _active;
		}

		public function get container():ColorInfoContainer
		{
			return _container;
		}


		public override function clone():Event
		{
			return new ActiveColorStageContainerEvent(type,active,container);
		}

		public override function toString():String
		{
			return formatToString("ActiveColorStageContainerEvent","active","container");
		}
	}
}
