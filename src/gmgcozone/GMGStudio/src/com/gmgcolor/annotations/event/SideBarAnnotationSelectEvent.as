package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class SideBarAnnotationSelectEvent extends Event
	{
		public static const ANNOTATION_CHANGE:String = "annotationChange";

		private var _annotation:ApprovalAnnotationFO;

		public function SideBarAnnotationSelectEvent(type:String, annotation:ApprovalAnnotationFO)
		{
			super(type,false,false);

			this._annotation = annotation;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public override function clone():Event
		{
			return new SideBarAnnotationSelectEvent(type,annotation);
		}

		public override function toString():String
		{
			return formatToString("SideBarEvent","annotation");
		}
	}
}
