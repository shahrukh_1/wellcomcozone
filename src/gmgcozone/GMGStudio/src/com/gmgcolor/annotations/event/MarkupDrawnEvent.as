package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class MarkupDrawnEvent extends Event
	{
		public static const DRAW:String = "draw";

		private var _annotation:ApprovalAnnotationFO;

		public function MarkupDrawnEvent(type:String, annotation:ApprovalAnnotationFO)
		{
			super(type,false,false);

			this._annotation = annotation;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public override function clone():Event
		{
			return new MarkupDrawnEvent(type,annotation);
		}

		public override function toString():String
		{
			return formatToString("MarkupDrawnEvent","annotation");
		}
	}
}
