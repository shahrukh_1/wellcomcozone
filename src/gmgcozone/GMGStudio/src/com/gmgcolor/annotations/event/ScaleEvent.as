package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class ScaleEvent extends Event
	{
		public static const SCALE:String = "scale";

		public var value:Number;

		public function ScaleEvent(type:String, scale:Number = 1)
		{
			super(type,false,false);
			value = scale;
		}

		public override function clone():Event
		{
			return new ScaleEvent(type, value);
		}
	}
}
