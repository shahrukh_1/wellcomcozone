package com.gmgcolor.annotations.event
{
import com.gmgcolor.application.domain.ApprovalPage;

import flash.events.Event;

public class PageChangeEvent extends Event
	{
		public static const pageChange:String = "pageChange";

		private var _page:ApprovalPage;

		public function PageChangeEvent(type:String, page:ApprovalPage)
		{
			super(type,false,false);

			this._page = page;
		}

		public function get page():ApprovalPage
		{
			return _page;
		}

		public override function clone():Event
		{
			return new PageChangeEvent(type,page);
		}

		public override function toString():String
		{
			return formatToString("PageChangeEvent","page");
		}
	}
}
