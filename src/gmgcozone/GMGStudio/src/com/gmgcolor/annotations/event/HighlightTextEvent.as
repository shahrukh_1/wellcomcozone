package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class HighlightTextEvent extends Event
	{
		public static const ADDED:String = "added";
		public static const ERASE:String = "erase";

		private var _text:String;

		public function HighlightTextEvent(type:String, text:String = null)
		{
			super(type,false,false);

			this._text = text;
		}

		public function get text():String
		{
			return _text;
		}

		public override function clone():Event
		{
			return new HighlightTextEvent(type,text);
		}

		public override function toString():String
		{
			return formatToString("HighlightTextEvent","text");
		}
	}
}
