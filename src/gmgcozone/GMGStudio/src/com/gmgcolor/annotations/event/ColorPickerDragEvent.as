package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class ColorPickerDragEvent extends Event
	{
		public static const START:String = "start";
		public static const END:String = "end";

		public function ColorPickerDragEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new ColorPickerDragEvent(type);
		}

		public override function toString():String
		{
			return formatToString("ColorPickerDragEvent");
		}
	}
}
