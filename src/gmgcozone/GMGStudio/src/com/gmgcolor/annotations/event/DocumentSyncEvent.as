package com.gmgcolor.annotations.event {
import flash.events.Event;
import flash.geom.Point;

public class DocumentSyncEvent extends Event {
		public static const SYNC:String = "sync";

		private var _zoom:Number;
		private var _position:Point;
		private var _rotate:Number;
		private var _page:uint;
		private var _selectedSeparationIndex:uint;

		public function DocumentSyncEvent(type:String, zoom:Number, position:Point, rotate:Number, page:uint, selectedSeparationIndex:int)
		{
			super(type, false, false);

			this._zoom = zoom;
			this._position = position;
			this._rotate = rotate;
			this._page = page;
			this._selectedSeparationIndex = selectedSeparationIndex;
		}

		public function get rotate():Number
		{
			return _rotate;
		}

		public function get zoom():Number
		{
			return _zoom;
		}

		public function get position():Point
		{
			return _position;
		}

		public function get page():uint
		{
			return _page;
		}

		public function get selectedSeparationIndex():uint
		{
			return _selectedSeparationIndex;
		}

		public override function clone():Event
		{
			return new DocumentSyncEvent(type, zoom, position, rotate, page, selectedSeparationIndex);
		}

		public override function toString():String
		{
			return formatToString("DocumentSyncEvent", "zoom", "position", "rotate", "page");
		}
	}
}
