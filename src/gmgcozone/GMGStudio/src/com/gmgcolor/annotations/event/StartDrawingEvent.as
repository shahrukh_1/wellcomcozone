package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Point;

public class StartDrawingEvent extends Event
	{
		public static const START:String = "start";
		
		private var _pt:Point;
		
		public function StartDrawingEvent(type:String, pt:Point)
		{
			super(type,false,false);
			
			this._pt = pt;
		}
		
		public function get pt():Point
		{
			return _pt;
		}
		
		public override function clone():Event
		{
			return new StartDrawingEvent(type,pt);
		}
		
		public override function toString():String
		{
			return formatToString("StartDrawingEvent","pt");
		}
	}
}
