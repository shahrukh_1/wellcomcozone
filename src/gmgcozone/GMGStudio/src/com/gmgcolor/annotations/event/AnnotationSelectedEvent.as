package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class AnnotationSelectedEvent extends Event
	{
		public static const SELECTED:String = "";

		private var _annotation:ApprovalAnnotationFO;

		public function AnnotationSelectedEvent(type:String, annotation:ApprovalAnnotationFO)
		{
			super(type,false,false);

			this._annotation = annotation;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public override function clone():Event
		{
			return new AnnotationSelectedEvent(type,annotation);
		}

		public override function toString():String
		{
			return formatToString("AnnotationSelectedEvent","annotation");
		}
	}
}
