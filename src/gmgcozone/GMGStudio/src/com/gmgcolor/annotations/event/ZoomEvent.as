package com.gmgcolor.annotations.event {
import flash.events.Event;

public class ZoomEvent extends Event {
		static public const ZOOM:String = 'zoom';

		public var value:Number;

		public function ZoomEvent(type:String, zoom:Number, bubbles:Boolean = false,cancelable:Boolean = false)
		{
			super(type,  bubbles, cancelable);
			value = zoom;
		}
	}
}
