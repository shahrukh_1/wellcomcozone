package com.gmgcolor.annotations.event
{
import com.gmgcolor.application.presentation.FXGConvertor;

import flash.events.Event;
import flash.geom.Point;

public class SaveBitmapDataEvent extends Event
	{
		public static const SAVE:String = "save";

		private var _fxg:FXGConvertor;
		private var _pt:Point;
		private var _width:Number;
		private var _height:Number;

		public function SaveBitmapDataEvent(type:String, fxg:FXGConvertor, pt:Point, width:Number, height:Number)
		{
			super(type,false,false);

			this._fxg = fxg;
			this._pt = pt;
			this._width = width;
			this._height = height;
		}
	
		public function get pt():Point
		{
			return _pt;
		}
		
		public function get width():Number
		{
			return _width;
		}
		
		public function get height():Number
		{
			return _height;
		}
		
		public function get fxg():FXGConvertor
		{
			return _fxg;
		}

		public override function clone():Event
		{
			return new SaveBitmapDataEvent(type,fxg,pt,width,height);
		}


	}
}
