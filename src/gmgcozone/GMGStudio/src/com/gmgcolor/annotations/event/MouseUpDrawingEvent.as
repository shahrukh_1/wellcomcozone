package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class MouseUpDrawingEvent extends Event
	{
		public static const mouseUp:String = "mouseup";
		
		public function MouseUpDrawingEvent(type:String)
		{
			super(type,false,false);
		}
		
		public override function clone():Event
		{
			return new MouseUpDrawingEvent(type);
		}
		
		public override function toString():String
		{
			return formatToString("MouseUpDrawingEvent");
		}
	}
}
