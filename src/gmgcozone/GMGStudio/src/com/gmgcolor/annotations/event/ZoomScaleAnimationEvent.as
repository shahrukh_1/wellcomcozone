package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Point;

public class ZoomScaleAnimationEvent extends Event
	{
		public static const EVENT_TYPE:String = "scaleAnimate";
		private var _n:Number;
		private var _zp:Point;
		private var _dispatchScaleEvent:Boolean;

		public function ZoomScaleAnimationEvent(type:String, n:Number, zp:Point, dispatchScaleEvent:Boolean=true)
		{
			super(type,false,false);
			this._n = n;
			this._zp = zp;
			this._dispatchScaleEvent = dispatchScaleEvent;
		}

		public override function clone():Event
		{
			return new ZoomScaleAnimationEvent(type,n,zp,dispatchScaleEvent);
		}

		public override function toString():String
		{
			return formatToString("ZoomScaleEvent");
		}


		public function get n():Number
		{
			return _n;
		}

		public function set n(value:Number):void
		{
			this._n = value;
		}

		public function get zp():Point
		{
			return _zp;
		}

		public function set zp(value:Point):void
		{
			this._zp = value;
		}

		public function get dispatchScaleEvent():Boolean
		{
			return _dispatchScaleEvent;
		}

		public function set dispatchScaleEvent(value:Boolean):void
		{
			this._dispatchScaleEvent = value;
		}

	}
}
