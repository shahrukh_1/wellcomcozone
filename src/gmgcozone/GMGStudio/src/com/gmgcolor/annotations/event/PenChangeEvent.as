package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class PenChangeEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _penWidth:Number;
		private var _drawColor:uint;
		private var _penAlpha:Number;

		public function PenChangeEvent(type:String, penWidth:Number=1, drawColor:uint = 0xFF0000, penAlpha:Number=1)
		{
			super(type,false,false);

			this._penWidth = penWidth;
			this._drawColor = drawColor;
			this._penAlpha = penAlpha;
		}

		public function get penWidth():Number
		{
			return _penWidth;
		}

		public function get drawColor():uint
		{
			return  _drawColor;
		}

		public function get penAlpha():Number
		{
			return _penAlpha;
		}

		public override function clone():Event
		{
			return new PenChangeEvent(type,penWidth,drawColor,penAlpha);
		}

		public override function toString():String
		{
			return formatToString("PenChangeEvent","penWidth","drawColor","penAlpha");
		}
	}
}
