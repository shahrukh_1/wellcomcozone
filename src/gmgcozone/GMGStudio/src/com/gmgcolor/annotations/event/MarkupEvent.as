package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class MarkupEvent extends Event
	{
		public static const ERASE:String = "erase";
		public static const ENABLE:String = "enable";
		public static const DISABLE:String = "disable";
		public static const FIRST_MARKUP:String = "firstMarkup";

		public function MarkupEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new MarkupEvent(type);
		}

		public override function toString():String
		{
			return formatToString("MarkupEvent");
		}
	}
}
