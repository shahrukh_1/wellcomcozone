package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class DocumentEvent extends Event
	{
		public static const MOUSE_DOWN:String = "mouseDown";
		public static const SELECTED:String = "selected";
		public static const NEW_TILED_DOCUMENT:String = "newTiledDocument";
		public static const NEW_VIDEO_DOCUMENT:String = "newVideoDocument";
        public static const NEW_SWF_DOCUMENT:String = "newSWFDocument";
		public static const READY:String = "ready";
		public static const COMMENT_PLACED:String = "commentPlaced";
		public static const RESIZE:String = "resize";
		public static const NEW_HIGHLIGHT_XML:String = "newHighlightXml";
        public static const SELECT_COMPLETED:String = 'selectCompleted';

        public var value:*;

		public function DocumentEvent(type:String,value:* = null)
		{
			super(type,false,false);
			this.value = value;
		}

		public override function clone():Event
		{
			return new DocumentEvent(type,value);
		}

		public override function toString():String
		{
			return formatToString("DocumentEvent");
		}
	}
}
