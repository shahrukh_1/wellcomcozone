package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class DisplaySepsEvent extends Event
	{
		public static const DISPLAY:String = "display";

		private var _display:Boolean;

		public function DisplaySepsEvent(type:String, display:Boolean)
		{
			super(type,false,false);

			this._display = display;
		}

		public function get display():Boolean
		{
			return _display;
		}

		public override function clone():Event
		{
			return new DisplaySepsEvent(type,display);
		}

		public override function toString():String
		{
			return formatToString("DisplaySepsEvent","display");
		}
	}
}
