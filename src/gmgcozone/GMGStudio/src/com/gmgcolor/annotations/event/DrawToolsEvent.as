package com.gmgcolor.annotations.event
{
import com.gmgcolor.application.domain.DrawTools;

import flash.events.Event;

public class DrawToolsEvent extends Event
	{
		public static const TOOL_CHANGE:String = "toolChange";

		private var _value:DrawTools;

		public function DrawToolsEvent(type:String, value:DrawTools)
		{
			super(type,false,false);

			this._value = value;
		}

		public function get value():DrawTools
		{
			return _value;
		}

		public override function clone():Event
		{
			return new DrawToolsEvent(type,value);
		}

		public override function toString():String
		{
			return formatToString("DrawToolsEvent","value");
		}
	}
}
