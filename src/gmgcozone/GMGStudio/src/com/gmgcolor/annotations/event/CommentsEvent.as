package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class CommentsEvent extends Event
	{
		public static const RESIZE:String = "resize";
		public static const MOVE:String = "move";
		public static const ADDED:String = "added";
		public static const CANCEL:String = "cancel";
		public static const NEXT:String = "next";
		public static const PREVIOUS:String = "previous";
		public static const SELECT:String = "select";
		
		private var _value:Object;

		public function CommentsEvent(type:String,value:Object = null)
		{
			super(type,false,false);
			this._value = value;
		}

		public override function clone():Event
		{
			return new CommentsEvent(type,value);
		}

		public override function toString():String
		{
			return formatToString("CommentsEvent");
		}


		public function get value():Object
		{
			return _value;
		}

	

	}
}
