package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Point;

public class DrawLineEvent extends Event
	{
		public static const DRAW:String = "draw";

		private var _pt1:Point;
		private var _pt2:Point;

		public function DrawLineEvent(type:String, pt1:Point, pt2:Point)
		{
			super(type,false,false);

			this._pt1 = pt1;
			this._pt2 = pt2;
		}

		public function get pt1():Point
		{
			return _pt1;
		}

		public function get pt2():Point
		{
			return _pt2;
		}

		public override function clone():Event
		{
			return new DrawLineEvent(type,pt1,pt2);
		}

		public override function toString():String
		{
			return formatToString("DrawLineEvent","pt1","pt2");
		}
	}
}
