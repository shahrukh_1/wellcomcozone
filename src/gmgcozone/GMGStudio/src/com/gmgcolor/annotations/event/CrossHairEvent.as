package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class CrossHairEvent extends Event
	{
		public static const SHOW:String = "show";
		public static const HIDE:String = "hide";
		public static const MOVE:String = "move";
		public static const MOUSEDOWN:String = "mouseDown";

		public function CrossHairEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new CrossHairEvent(type);
		}

		public override function toString():String
		{
			return formatToString("CrossHairEvent");
		}
	}
}
