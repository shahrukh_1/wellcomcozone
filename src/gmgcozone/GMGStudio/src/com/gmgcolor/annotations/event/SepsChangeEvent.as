package com.gmgcolor.annotations.event
{
import com.gmgcolor.application.domain.ApprovalSeparationPlateFO;

import flash.events.Event;

public class SepsChangeEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _separation:ApprovalSeparationPlateFO;
		private var _separationIndex:int;

		public function SepsChangeEvent(type:String, sep:ApprovalSeparationPlateFO, index:int)
		{
			super(type,false,false);

			this._separation = sep;
			this._separationIndex = index;
		}

		public function get separation():ApprovalSeparationPlateFO
		{
			return _separation;
		}

		public function get separationIndex():int
		{
			return _separationIndex;
		}

		public override function clone():Event
		{
			return new SepsChangeEvent(type,separation, separationIndex);
		}

		public override function toString():String
		{
			return formatToString("SepsChangeEvent","separation");
		}
	}
}
