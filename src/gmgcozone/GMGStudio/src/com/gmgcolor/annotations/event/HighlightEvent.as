package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class HighlightEvent extends Event
	{
		public static const ENABLE:String = "enable";
		public static const DISABLE:String = "disable";
		public static const HIGHLIGHTS_AVAILABLE:String = "available";
		public static const HIGHLIGHTS_UNAVAILABLE:String = "unavailable";

		public function HighlightEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new HighlightEvent(type);
		}

		public override function toString():String
		{
			return formatToString("HighlightEvent");
		}
	}
}
