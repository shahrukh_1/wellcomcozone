package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class DocumentProgressEvent extends Event
	{
		public static const PROGRESS:String = "progress";

		private var _bytesLoaded:int;
		private var _bytesTotal:int;

		public function DocumentProgressEvent(type:String, bytesLoaded:int, bytesTotal:int)
		{
			super(type,false,false);

			this._bytesLoaded = bytesLoaded;
			this._bytesTotal = bytesTotal;
		}

		public function get bytesLoaded():int
		{
			return _bytesLoaded;
		}

		public function get bytesTotal():int
		{
			return _bytesTotal;
		}

		public override function clone():Event
		{
			return new DocumentProgressEvent(type,bytesLoaded,bytesTotal);
		}

		public override function toString():String
		{
			return formatToString("DocumentProgressEvent","bytesLoaded","bytesTotal");
		}
	}
}
