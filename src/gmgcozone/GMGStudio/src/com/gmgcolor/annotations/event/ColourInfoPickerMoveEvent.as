package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Point;

public class ColourInfoPickerMoveEvent extends Event
	{
		public static const MOVE:String = "movePicker";

		private var _pt:Point;

		public function ColourInfoPickerMoveEvent(type:String, pt:Point)
		{
			super(type,false,false);

			this._pt = pt;
		}

		public function get pt():Point
		{
			return _pt;
		}

		public override function clone():Event
		{
			return new ColourInfoPickerMoveEvent(type,pt);
		}

		public override function toString():String
		{
			return formatToString("ColourInfoPickerMove","pt");
		}
	}
}
