package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class ReplyCommentEvent extends Event
	{
		public static const REPLY_COMMENT:String = "replyComment";

		private var _comment:String;

		public function ReplyCommentEvent(type:String, comment:String)
		{
			super(type,false,false);

			this._comment = comment;
		}

		public function get comment():String
		{
			return _comment;
		}

		public override function clone():Event
		{
			return new ReplyCommentEvent(type,comment);
		}

		public override function toString():String
		{
			return formatToString("ReplyCommentEvent","comment");
		}
	}
}
