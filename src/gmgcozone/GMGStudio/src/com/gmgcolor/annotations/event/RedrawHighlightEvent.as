package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class RedrawHighlightEvent extends Event
	{
		public static const REDRAW:String = "redraw";

		private var _startTM:int;
		private var _endTM:int;
		private var _highlightType:int;

		public function RedrawHighlightEvent(type:String, startIndex:int, endIndex:int, highlightType:int)
		{
			super(type,false,false);

			this._startTM = startIndex;
			this._endTM = endIndex;
			this._highlightType = highlightType;
		}

		public function get startIndex():int
		{
			return _startTM;
		}

		public function get endIndex():int
		{
			return _endTM;
		}

		public function get highlightType():int
		{
			return _highlightType;
		}

		public override function clone():Event
		{
			return new RedrawHighlightEvent(type,startIndex,endIndex,highlightType);
		}

		public override function toString():String
		{
			return formatToString("RedrawHighlight","startIndex","endIndex","highlightType");
		}
	}
}
