package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class TextToolsChangeEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _newIndex:int;

		public function TextToolsChangeEvent(type:String, newIndex:int)
		{
			super(type,false,false);

			this._newIndex = newIndex;
		}

		public function get newIndex():int
		{
			return _newIndex;
		}

		public override function clone():Event
		{
			return new TextToolsChangeEvent(type,newIndex);
		}

		public override function toString():String
		{
			return formatToString("TextToolsChangeEvent","newIndex");
		}
	}
}
