package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Rectangle;

public class TextToolsPopUpMenuEvent extends Event
	{
		public static const MOUSE_OVER:String = "mouseOver";
		public static const MOUSE_OUT:String = "mouseOut";
		public static const MOUSE_MOVE:String = "mouseMove";
		public static const MOUSE_UP:String = "mouseUp";
		public static const HIGHLIGHT_TYPE_NORMAL:String = "normal";
		public static const HIGHLIGHT_TYPE_REPLACE:String = "replace";
		public static const CLEAR:String = "clear";
		
		private var _globalRect:Rectangle;

		public function TextToolsPopUpMenuEvent(type:String, globalRect:Rectangle = null)
		{
			super(type,false,false);
			this._globalRect = globalRect;
		}

		public function get globalRect():Rectangle
		{
			return _globalRect;
		}

		public override function clone():Event
		{
			return new TextToolsPopUpMenuEvent(type,globalRect);
		}

		public override function toString():String
		{
			return formatToString("TextContainerMouseEvent");
		}
	}
}
