package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class UpdateCommentHeightEvent extends Event
	{
		public static const UPDATE:String = "update";

		public function UpdateCommentHeightEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new UpdateCommentHeightEvent(type);
		}

		public override function toString():String
		{
			return formatToString("UpdateCommentEvent");
		}
	}
}
