package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Point;

public class TileContainerClickedEvent extends Event
	{
		public static const CLICK:String = "click";

		private var _localPoint:Point;
		private var _globalPoint:Point;

		public function TileContainerClickedEvent(type:String, localPoint:Point, globalPoint:Point)
		{
			super(type,false,false);

			this._localPoint = localPoint;
			this._globalPoint = globalPoint;
		}

		public function get localPoint():Point
		{
			return _localPoint;
		}

		public function get globalPoint():Point
		{
			return _globalPoint;
		}

		public override function clone():Event
		{
			return new TileContainerClickedEvent(type,localPoint,globalPoint);
		}

		public override function toString():String
		{
			return formatToString("TileContainerClickedEvent","localPoint","globalPoint");
		}
	}
}
