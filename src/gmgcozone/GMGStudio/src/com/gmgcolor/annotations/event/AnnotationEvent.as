package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class AnnotationEvent extends Event
	{
		public static const CREATE:String = "create";
		public static const ADD:String = "add";
		public static const CANCEL:String = "cancel";
		public static const MOVE:String = "move";
		public static const DRAW:String = "draw";
		public static const REPLY:String = "reply";
        public static const REACHED:String = "reached";

		private var _annotation:ApprovalAnnotationFO;

		public function AnnotationEvent(type:String, annotation:ApprovalAnnotationFO=null)
		{
			super(type,false,false);

			this._annotation = annotation;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

//		public override function clone():Event
//		{
//			return new AnnotationEvent(type,annotation);
//		}

		public override function toString():String
		{
			return formatToString("AnnotationEvent","annotation");
		}
	}
}
