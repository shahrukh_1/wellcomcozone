package com.gmgcolor.annotations.event
{
import com.gmgcolor.application.pages.enum.EyeDropperTypeEnum;

import flash.events.Event;
import flash.geom.Point;

public class CreateStageColorContainerEvent extends Event
	{
		public static const CREATE:String = "create";

		private var _pt:Point;
		private var _colorType:EyeDropperTypeEnum;

		public function CreateStageColorContainerEvent(type:String, pt:Point, colorType:EyeDropperTypeEnum)
		{
			super(type,false,false);

			this._pt = pt;
			this._colorType = colorType;
		}

		public function get pt():Point
		{
			return _pt;
		}

		public override function clone():Event
		{
			return new CreateStageColorContainerEvent(type,pt,colorType);
		}

		public override function toString():String
		{
			return formatToString("CreateStageColorContainerEvent","pt");
		}


		public function get colorType():EyeDropperTypeEnum
		{
			return _colorType;
		}

	

	}
}
