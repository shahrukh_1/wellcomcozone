package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Rectangle;

public class StageMarkerContainerMouseEvent extends Event
	{
		public static const MOUSE_OVER:String = "mouseOver";
		public static const MOUSE_OUT:String = "mouseOut";
		public static const MOUSE_MOVE:String = "mouseMove";
		public static const REMOVE:String = "remove";
		
		private var _globalRect:Rectangle;

		public function StageMarkerContainerMouseEvent(type:String, globalRect:Rectangle = null)
		{
			super(type,false,false);
			this._globalRect = globalRect;
		}

		public function get globalRect():Rectangle
		{
			return _globalRect;
		}

		public override function clone():Event
		{
			return new StageMarkerContainerMouseEvent(type,globalRect);
		}

		public override function toString():String
		{
			return formatToString("StageMarkerContainerMouseEvent");
		}
	}
}
