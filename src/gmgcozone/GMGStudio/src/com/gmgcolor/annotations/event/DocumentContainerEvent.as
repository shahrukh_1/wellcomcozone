package com.gmgcolor.annotations.event
{
import flash.display.MovieClip;
import flash.events.Event;

public class DocumentContainerEvent extends Event
	{
		public static const SWF_LOADED:String = "swfLoaded";

		private var _value:MovieClip;

		public function DocumentContainerEvent(type:String, value:MovieClip)
		{
			super(type,false,false);

			this._value = value;
		}

		public function get value():MovieClip
		{
			return _value;
		}

		public override function clone():Event
		{
			return new DocumentContainerEvent(type,value);
		}

		public override function toString():String
		{
			return formatToString("DocumentContainerEvent","value");
		}
	}
}
