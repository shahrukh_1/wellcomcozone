package com.gmgcolor.annotations.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationMarkupFO;

import flash.events.Event;

public class MarkupImageEvent extends Event
	{
		public static const ADD:String = "add";

		private var _image:ApprovalAnnotationMarkupFO;

		public function MarkupImageEvent(type:String, image:ApprovalAnnotationMarkupFO)
		{
			super(type,false,false);

			this._image = image;
		}

		public function get image():ApprovalAnnotationMarkupFO
		{
			return _image;
		}

		public override function clone():Event
		{
			return new MarkupImageEvent(type,image);
		}

		public override function toString():String
		{
			return formatToString("MarkupImageEvent","image");
		}
	}
}
