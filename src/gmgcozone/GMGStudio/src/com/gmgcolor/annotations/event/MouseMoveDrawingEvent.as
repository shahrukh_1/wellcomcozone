package com.gmgcolor.annotations.event
{
import flash.events.Event;
import flash.geom.Point;

public class MouseMoveDrawingEvent extends Event
	{
		public static const MOVE:String = "move";
		
		private var _pt:Point;
		
		public function MouseMoveDrawingEvent(type:String, pt:Point)
		{
			super(type,false,false);
			
			this._pt = pt;
		}
		
		public function get pt():Point
		{
			return _pt;
		}
		
		public override function clone():Event
		{
			return new MouseMoveDrawingEvent(type,pt);
		}
		
		public override function toString():String
		{
			return formatToString("MouseMoveDrawingEvent","pt");
		}
	}
}
