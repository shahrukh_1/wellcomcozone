package com.gmgcolor.annotations.event
{
import flash.events.Event;

public class NewCommentEvent extends Event
	{
		public static const NEW_COMMENT:String = "newComment";

		private var _comment:String;
		private var _isPrivate:Boolean;
		private var _collaboratorIds:Array;

		public function NewCommentEvent(type:String, comment:String, isPrivate:Boolean = false, collaboratorIds:Array = null)
		{
			super(type,false,false);

			this._comment = comment;
			this._isPrivate = isPrivate;
			this._collaboratorIds = collaboratorIds;
		}

		public function get comment():String
		{
			return _comment;
		}

		public override function clone():Event
		{
			return new NewCommentEvent(type,comment,isPrivate,collaboratorIds);
		}

		public override function toString():String
		{
			return formatToString("NewCommentEvent","comment");
		}


		public function get isPrivate():Boolean
		{
			return _isPrivate;
		}

		public function set isPrivate(value:Boolean):void
		{
			this._isPrivate = value;
		}

		public function get collaboratorIds():Array
		{
			return _collaboratorIds;
		}

		public function set collaboratorIds(value:Array):void
		{
			this._collaboratorIds = value;
		}

	}
}
