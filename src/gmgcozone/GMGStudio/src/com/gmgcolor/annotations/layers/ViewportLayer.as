package com.gmgcolor.annotations.layers {
import flash.display.Sprite;

import org.openzoom.flash.viewport.INormalizedViewport;
import org.openzoom.flash.viewport.IViewportLayer;

public class ViewportLayer extends Sprite implements IViewportLayer{

		private var _viewport:INormalizedViewport;

		public function get viewport():INormalizedViewport {
			return this._viewport;
		}

		public function set viewport(value:INormalizedViewport):void {
			/*if(this._viewport) {
				this._viewport.removeEventListener(ViewportEvent.TRANSFORM_UPDATE, onUpdate);
			}*/
			this._viewport = value;

			/*_viewport.addEventListener(ViewportEvent.TRANSFORM_START,
					viewport_transformStartHandler,
					false, 0, true);
			_viewport.addEventListener(ViewportEvent.TRANSFORM_UPDATE,
					viewport_transformUpdateHandler,
					false, 0, true);
			_viewport.addEventListener(ViewportEvent.TRANSFORM_END,
					viewport_transformEndHandler,
					false, 0, true);*/
		}

		public function ViewportLayer()
		{
			this.draw();
		}

		private function draw():void {
			graphics.clear();
			//graphics.drawRect();
		}
	}
}
