package com.gmgcolor.model.enums {
	public class UserRole {

		// The possible enums for this class
		static public const SUPERADMIN:UserRole = new UserRole(1, 'HQA', 'Super Administrator');
		static public const SUPERMANAGER:UserRole = new UserRole(2, 'HQM', 'Super Manager');
		static public const ADMIN:UserRole = new UserRole(3, 'ADM', 'Administrator');
		static public const MANAGER:UserRole = new UserRole(4, 'MAN', 'Manager');
		static public const MODERATOR:UserRole = new UserRole(5, 'MOD', 'Moderator');
		static public const CONTRIBUTOR:UserRole = new UserRole(6, 'CON', 'Contributor');
		static public const VIEWER:UserRole = new UserRole(7, 'VIE', 'Viewer');


		// A list of all the enums for easy referencing if needed
		static public const roles:Array = [SUPERADMIN, SUPERMANAGER, ADMIN, MANAGER, MODERATOR, CONTRIBUTOR, VIEWER];

		// The static initializer needed to stop anyone else to an instance of this class
		static private var INIT:Boolean = init();

		// Class vars
		private var _id:int;
		private var _key:String;
		private var _name:String;

		// Class constructor
		public function UserRole(id:int, key:String, name:String)
		{
			if (INIT) {
				throw new Error('UserRole enums already created');
			}

			this._id = id;
			this._key = key;
			this._name = name;
		}

		// Returns the true weight of the class
		public function valueOf():int
		{
			return this._id;
		}

		// Returns a string when needed
		public function toString():String
		{
			return this._name;
		}

		public function get id():int
		{
			return this._id;
		}

		public function get key():String
		{
			return this._key;
		}

		public function get name():String
		{
			return this._name;
		}

		static public function findById(id:int):UserRole {
			for each(var role:UserRole in roles) {
				if(role.id == id) {
					return role;
				}
			}
			return null;
		}

		// Static initializer function
		static private function init():Boolean
		{
			return true;
		}
	}
}
