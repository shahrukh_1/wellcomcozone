package com.gmgcolor.model.enums {
	public class ApprovalRole {

		// The possible enums for this class
		static public const READ_ONLY:ApprovalRole = new ApprovalRole(1, 'RDO', 'Read Only');
		static public const REVIEWER:ApprovalRole = new ApprovalRole(2, 'RVW', 'Reviewer');
		static public const APPROVER:ApprovalRole = new ApprovalRole(3, 'ANR', 'Approver & Reviewer');


		// A list of all the enums for easy referencing if needed
		static public const roles:Array = [READ_ONLY, REVIEWER, APPROVER];

		// The static initializer needed to stop anyone else to an instance of this class
		static private var INIT:Boolean = init();

		// Class vars
		private var _id:int;
		private var _key:String;
		private var _name:String;

		// Class constructor
		public function ApprovalRole(id:int, key:String, name:String)
		{
			if (INIT) {
				throw new Error('ApprovalRole enums already created');
			}

			this._id = id;
			this._key = key;
			this._name = name;
		}

		// Returns the true weight of the class
		public function valueOf():int
		{
			return this._id;
		}

		// Returns a string when needed
		public function toString():String
		{
			return this._name;
		}

		public function get id():int
		{
			return this._id;
		}

		public function get key():String
		{
			return this._key;
		}

		public function get name():String
		{
			return this._name;
		}

		static public function findById(id:int):ApprovalRole
		{
			for each(var role:ApprovalRole in roles) {
				if (role.id == id) {
					return role;
				}
			}
			return null;
		}

		// Static initializer function
		static private function init():Boolean
		{
			return true;
		}
	}
}
