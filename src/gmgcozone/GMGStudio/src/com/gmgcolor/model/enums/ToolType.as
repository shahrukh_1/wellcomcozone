package com.gmgcolor.model.enums {
	public class ToolType {

		// The possible enums for this class
		static public const PAN:ToolType = new ToolType(1, 'PAN');
		static public const MOVE:ToolType = new ToolType(2, 'MOVE');
		static public const COMMENT:ToolType = new ToolType(3, 'COMMENT');
		static public const DRAW:ToolType = new ToolType(4, 'DRAW');
		static public const SHAPE:ToolType = new ToolType(5, 'SHAPE');
		static public const TEXT:ToolType = new ToolType(6, 'TEXT');


		// A list of all the enums for easy referencing if needed
		static public const roles:Array = [PAN, MOVE, COMMENT, DRAW, SHAPE, TEXT];

		// The static initializer needed to stop anyone else to an instance of this class
		static private var INIT:Boolean = init();

		// Class vars
		private var _id:int;
		private var _name:String;

		// Class constructor
		public function ToolType(id:int, name:String)
		{
			if (INIT) {
				throw new Error('ToolType enums already created');
			}

			this._id = id;
			this._name = name;
		}

		// Returns the true weight of the class
		public function valueOf():int
		{
			return this._id;
		}

		// Returns a string when needed
		public function toString():String
		{
			return this._name;
		}

		public function get id():int
		{
			return this._id;
		}

		public function get name():String
		{
			return this._name;
		}

		// Static initializer function
		static private function init():Boolean
		{
			return true;
		}
	}
}
