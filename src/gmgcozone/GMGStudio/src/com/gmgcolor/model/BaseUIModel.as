package com.gmgcolor.model
{
import flash.events.Event;
import flash.events.EventDispatcher;

public class BaseUIModel extends EventDispatcher
	{
		public static const ISMOVEABLE_CHANGE_EVENT:String = "isMoveableChange";
		public static const ISDRAGGING_CHANGE_EVENT:String = "isDraggingChange";
		public static const XPOS_CHANGE_EVENT:String = "xPosChange";
		public static const YPOS_CHANGE_EVENT:String = "yPosChange";
		public static const SCALEX_CHANGE_EVENT:String = "scaleXChange";
		public static const SCALEY_CHANGE_EVENT:String = "scaleYChange";
		public static const ISON_CHANGE_EVENT:String = "isOnChange";
		public static const ISVISIBLE_CHANGE_EVENT:String = "isVisibleChange";
		public static const WIDTH_CHANGE_EVENT:String = "widthChange";
		public static const HEIGHT_CHANGE_EVENT:String = "heightChange";
		
		private var _isMoveable:Boolean;
		private var _isDragging:Boolean;
		private var _isOn:Boolean;
		private var _isVisible:Boolean;
		private var _xPos:Number;
		private var _yPos:Number;
		private var _scaleX:Number = 1;
		private var _scaleY:Number = 1;
		private var _width:Number;
		private var _height:Number;

		public function get isMoveable():Boolean
		{
			return _isMoveable;
		}

		public function set isMoveable(value:Boolean):void
		{
			if (_isMoveable != value)
			{
				_isMoveable = value;
				dispatchEvent(new Event(ISMOVEABLE_CHANGE_EVENT));
			}
		}

		public function get isDragging():Boolean
		{
			return _isDragging;
		}

		public function set isDragging(value:Boolean):void
		{
			if (_isDragging != value)
			{
				_isDragging = value;
				dispatchEvent(new Event(ISDRAGGING_CHANGE_EVENT));
			}
		}

		public function get xPos():Number
		{
			return _xPos;
		}

		public function set xPos(value:Number):void
		{
			if (_xPos != value)
			{
				_xPos = value;
				dispatchEvent(new Event(XPOS_CHANGE_EVENT));
			}
		}

		public function get yPos():Number
		{
			return _yPos;
		}

		public function set yPos(value:Number):void
		{
			if (_yPos != value)
			{
				_yPos = value;
				dispatchEvent(new Event(YPOS_CHANGE_EVENT));
			}
		}

		public function get scaleX():Number
		{
			return _scaleX;
		}

		public function set scaleX(value:Number):void
		{
			if (_scaleX != value)
			{
				_scaleX = value;
				dispatchEvent(new Event(SCALEX_CHANGE_EVENT));
			}
		}

		public function get scaleY():Number
		{
			return _scaleY;
		}

		public function set scaleY(value:Number):void
		{
			if (_scaleY != value)
			{
				_scaleY = value;
				dispatchEvent(new Event(SCALEY_CHANGE_EVENT));
			}
		}

		public function get isOn():Boolean
		{
			return _isOn;
		}

		public function set isOn(value:Boolean):void
		{
			if (_isOn != value)
			{
				_isOn = value;
				isVisible = value;
				dispatchEvent(new Event(ISON_CHANGE_EVENT));
			}
		}

		public function get isVisible():Boolean
		{
			return _isVisible;
		}

		public function set isVisible(value:Boolean):void
		{
			if (_isVisible != value)
			{
				_isVisible = value;
				dispatchEvent(new Event(ISVISIBLE_CHANGE_EVENT));
			}
		}


		public function get width():Number
		{
			return _width;
		}

		public function set width(value:Number):void
		{
			if (_width != value)
			{
				_width = value;
				dispatchEvent(new Event(WIDTH_CHANGE_EVENT));
			}
		}

		public function get height():Number
		{
			return _height;
		}

		public function set height(value:Number):void
		{
			if (_height != value)
			{
				_height = value;
				dispatchEvent(new Event(HEIGHT_CHANGE_EVENT));
			}
		}

	}
}
