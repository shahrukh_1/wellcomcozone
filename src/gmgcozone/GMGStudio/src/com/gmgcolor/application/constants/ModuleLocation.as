package com.gmgcolor.application.constants
{
	// Location constants for modules
	public class ModuleLocation
	{
		// Only static public constants can be used here
		static public const style:String = /*CONFIG::modulePath+'/'+*/CONFIG::styleModule;
		static public const font1:String = /*CONFIG::modulePath+'/'+*/CONFIG::fontModule1;
		static public const font2:String = /*CONFIG::modulePath+'/'+*/CONFIG::fontModule2;
		static public const font3:String = /*CONFIG::modulePath+'/'+*/CONFIG::fontModule3;
		static public const home:String = CONFIG::modulePath+'/'+CONFIG::homeModule;
		static public const createAccount:String = CONFIG::modulePath+'/'+CONFIG::createAccountModule;
		static public const myAccount:String = CONFIG::modulePath+'/'+CONFIG::myAccountModule;
		static public const myBet:String = CONFIG::modulePath+'/'+CONFIG::myBetModule;
		static public const racing:String = CONFIG::modulePath+'/'+CONFIG::racingModule;
		static public const sports:String = CONFIG::modulePath+'/'+CONFIG::sportsModule;
	}
}