package com.gmgcolor.application.constants
{
import flash.net.SharedObject;

// Location constants for resource files
	public class ResourceLocation
	{
		// Only static public constants can be used here
		static public const bootstrap:String = CONFIG::resourcePath+'/'+CONFIG::bootstrapResource;
		static public const environment:String = SharedObject.getLocal('parameters', '/').data.configFile;
		static public const configuration:String = SharedObject.getLocal('parameters', '/').data.configPath +'/'+ CONFIG::configurationResource;

	}
}
