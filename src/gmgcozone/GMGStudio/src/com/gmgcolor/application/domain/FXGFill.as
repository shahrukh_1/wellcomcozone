package com.gmgcolor.application.domain
{
	public class FXGFill
	{
		
		private var _color:String;
		private var _alpha:Number;
		
		public function FXGFill()
		{
		}


		public function get color():String
		{
			return _color;
		}

		public function set color(value:String):void
		{
			_color = value;
		}

		public function get alpha():Number
		{
			return _alpha;
		}

		public function set alpha(value:Number):void
		{
			_alpha = value;
		}

	}
}