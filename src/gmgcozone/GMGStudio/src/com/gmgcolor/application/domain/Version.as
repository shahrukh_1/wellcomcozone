package com.gmgcolor.application.domain
{
	[Bindable]
	public class Version
	{
		
		public var name:String;
		public var isSelected:Boolean;
		public var isEnabled:Boolean;
		
		public function Version()
		{
		}
	}
}