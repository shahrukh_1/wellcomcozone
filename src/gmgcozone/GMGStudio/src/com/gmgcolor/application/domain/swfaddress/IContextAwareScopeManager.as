package com.gmgcolor.application.domain.swfaddress
{
	public interface IContextAwareScopeManager
	{
		
		function dispatchContextMessage(message:Object,selector:*=null):void;
	}
}