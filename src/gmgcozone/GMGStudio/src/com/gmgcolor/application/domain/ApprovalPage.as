package com.gmgcolor.application.domain {
import com.gmgcolor.application.enums.ResourceBundle;

import flash.events.Event;
import flash.events.EventDispatcher;

import mx.modules.IModuleInfo;
import mx.resources.ResourceManager;

[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalPageFO")]
	[Event(name="numberChange", type="flash.events.Event")]
	[Event(name="filenameChange", type="flash.events.Event")]
	[Event(name="isMainViewSelectedChange", type="flash.events.Event")]
	[Event(name="moduleInfoChange", type="flash.events.Event")]
	[Event(name="highlightTextFilenameChange", type="flash.events.Event")]
	[Event(name="IDChange", type="flash.events.Event")]
	[Event(name="ApprovalChange", type="flash.events.Event")]
	[Event(name="AssetFolderChange", type="flash.events.Event")]
	[Event(name="AnnotationsChange", type="flash.events.Event")]
	[Event(name="PageFolderChange", type="flash.events.Event")]
	[Event(name="SmallThumnailHeightChange", type="flash.events.Event")]
	[Event(name="LargeThumnailHeightChange", type="flash.events.Event")]
	[Event(name="SmallThumnailWidthChange", type="flash.events.Event")]
	[Event(name="LargeThumnailWidthChange", type="flash.events.Event")]
	[Event(name="DPIChange", type="flash.events.Event")]
	[Event(name="ApprovalSepartionPlatesChange", type="flash.events.Event")]
	public class ApprovalPage extends EventDispatcher {
		public static const NUMBER_CHANGE_EVENT:String = "numberChange";
		public static const FILENAME_CHANGE_EVENT:String = "filenameChange";
		public static const ISMAINVIEWSELECTED_CHANGE_EVENT:String = "isMainViewSelectedChange";
		public static const MODULEINFO_CHANGE_EVENT:String = "moduleInfoChange";
		public static const HIGHLIGHTTEXTFILENAME_CHANGE_EVENT:String = "highlightTextFilenameChange";
		public static const ID_CHANGE_EVENT:String = "IDChange";
		public static const APPROVAL_CHANGE_EVENT:String = "ApprovalChange";
		public static const ASSETFOLDER_CHANGE_EVENT:String = "AssetFolderChange";
		public static const ANNOTATIONS_CHANGE_EVENT:String = "AnnotationsChange";
		public static const PAGEFOLDER_CHANGE_EVENT:String = "PageFolderChange";
		public static const SMALLTHUMNAILHEIGHT_CHANGE_EVENT:String = "SmallThumnailHeightChange";
		public static const LARGETHUMNAILHEIGHT_CHANGE_EVENT:String = "LargeThumnailHeightChange";
		public static const SMALLTHUMNAILWIDTH_CHANGE_EVENT:String = "SmallThumnailWidthChange";
		public static const LARGETHUMNAILWIDTH_CHANGE_EVENT:String = "LargeThumnailWidthChange";
		public static const DPI_CHANGE_EVENT:String = "DPIChange";
		public static const APPROVALSEPARATIONPLATES_CHANGE_EVENT:String = "ApprovalSeparationPlatesChange";
		public static const APPROVALSEPARATIONINDEX_CHANGE_EVENT:String = "ApprovalSeparationIndexChange";

		public function ApprovalPage()
		{

		}

		private var _ID:int;
		private var _Approval:int
		private var _Annotations:Array;
		private var _PageNumber:int;
		private var _filename:String;
		private var _isMainViewSelected:Boolean;
		private var _moduleInfo:IModuleInfo;
		private var _highlightTextFilename:String;

		private var _SelectedApprovalSeparationIndex:int = 0;

		private var _PageFolder:String;

		private var _ApprovalSeparationPlates:Array;

		private var _DPI:int;

		private var _SmallThumbnailPath:String;
		private var _LargeThumbnailPath:String;
		private var _SmallThumbnailHeight:Number;
		private var _LargeThumbnailHeight:Number;
		private var _SmallThumbnailWidth:Number;
		private var _LargeThumbnailWidth:Number;

		public function get SmallThumbnailPath():String
		{
			return _SmallThumbnailPath;
		}

		public function set SmallThumbnailPath(value:String):void
		{
			_SmallThumbnailPath = value;
		}

		public function get LargeThumbnailPath():String
		{
			return _LargeThumbnailPath;
		}

		public function set LargeThumbnailPath(value:String):void
		{
			_LargeThumbnailPath = value;
		}


		[Bindable(event="numberChange")]
		public function get PageNumber():int
		{
			return _PageNumber;
		}

		public function set PageNumber(value:int):void
		{
			if (_PageNumber != value) {
				_PageNumber = value;
				dispatchEvent(new Event(NUMBER_CHANGE_EVENT));
			}
		}

		[Bindable(event="filenameChange")]
		public function get filename():String
		{
			return _filename;
		}

		public function set filename(value:String):void
		{
			if (_filename != value) {
				_filename = value;
				dispatchEvent(new Event(FILENAME_CHANGE_EVENT));
			}
		}

		[Bindable(event="isMainViewSelectedChange")]
		public function get isMainViewSelected():Boolean
		{
			return _isMainViewSelected;
		}

		public function set isMainViewSelected(value:Boolean):void
		{
			if (_isMainViewSelected != value) {
				_isMainViewSelected = value;
				dispatchEvent(new Event(ISMAINVIEWSELECTED_CHANGE_EVENT));
			}
		}


		[Bindable(event="moduleInfoChange")]
		public function get moduleInfo():IModuleInfo
		{
			return _moduleInfo;
		}

		public function set moduleInfo(value:IModuleInfo):void
		{
			if (_moduleInfo != value) {
				_moduleInfo = value;
				dispatchEvent(new Event(MODULEINFO_CHANGE_EVENT));
			}
		}


		[Bindable(event="highlightTextFilenameChange")]
		public function get highlightTextFilename():String
		{
			return _highlightTextFilename;
		}

		public function set highlightTextFilename(value:String):void
		{
			if (_highlightTextFilename != value) {
				_highlightTextFilename = value;
				dispatchEvent(new Event(HIGHLIGHTTEXTFILENAME_CHANGE_EVENT));
			}
		}


		[Bindable(event="IDChange")]
		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			if (_ID != value) {
				_ID = value;
				dispatchEvent(new Event(ID_CHANGE_EVENT));
			}
		}

		[Bindable(event="ApprovalChange")]
		public function get Approval():int
		{
			return _Approval;
		}

		public function set Approval(value:int):void
		{
			if (_Approval != value) {
				_Approval = value;
				dispatchEvent(new Event(APPROVAL_CHANGE_EVENT));
			}
		}

		[Bindable(event="AnnotationsChange")]
		public function get Annotations():Array
		{
			return _Annotations;
		}

		public function set Annotations(value:Array):void
		{
			if (_Annotations != value) {
				_Annotations = value;
				dispatchEvent(new Event(ANNOTATIONS_CHANGE_EVENT));
			}
		}

		[Bindable(event="PageFolderChange")]
		public function get PageFolder():String
		{
			return _PageFolder;
		}

		public function set PageFolder(value:String):void
		{
			if (_PageFolder != value) {
				_PageFolder = value;
				dispatchEvent(new Event(PAGEFOLDER_CHANGE_EVENT));
			}
		}

		[Bindable(event="DPIChange")]
		public function get DPI():int
		{
			return _DPI;
		}

		public function set DPI(value:int):void
		{
			if (_DPI != value) {
				_DPI = value;
				dispatchEvent(new Event(DPI_CHANGE_EVENT));
			}
		}


		[Bindable(event="ApprovalSeparationPlatesChange")]
		public function get ApprovalSeparationPlates():Array
		{
			var array:Array;
			if(_ApprovalSeparationPlates) {
				array = _ApprovalSeparationPlates.slice();
				var separation:ApprovalSeparationPlateFO = new ApprovalSeparationPlateFO();
				separation.ID = 0;
				separation.Name = ResourceManager.getInstance().getString(ResourceBundle.LABELS, "separations.all");
				array.unshift(separation);
			}

			return array;
		}

		public function set ApprovalSeparationPlates(value:Array):void
		{
			if (_ApprovalSeparationPlates != value) {
				_ApprovalSeparationPlates = value;
				dispatchEvent(new Event(APPROVALSEPARATIONPLATES_CHANGE_EVENT));
			}
		}

		public function get SmallThumbnailHeight():Number
		{
			return _SmallThumbnailHeight;
		}

		public function set SmallThumbnailHeight(value:Number):void
		{
			_SmallThumbnailHeight = value;
		}

		public function get LargeThumbnailHeight():Number
		{
			return _LargeThumbnailHeight;
		}

		public function set LargeThumbnailHeight(value:Number):void
		{
			_LargeThumbnailHeight = value;
		}

		public function get SmallThumbnailWidth():Number
		{
			return _SmallThumbnailWidth;
		}

		public function set SmallThumbnailWidth(value:Number):void
		{
			_SmallThumbnailWidth = value;
		}

		public function get LargeThumbnailWidth():Number
		{
			return _LargeThumbnailWidth;
		}

		public function set LargeThumbnailWidth(value:Number):void
		{
			_LargeThumbnailWidth = value;
		}

		[Bindable(event="ApprovalSeparationIndexChange")]
		public function get SelectedApprovalSeparationIndex():int
		{
			return _SelectedApprovalSeparationIndex;
		}

		public function set SelectedApprovalSeparationIndex(value:int):void
		{
			_SelectedApprovalSeparationIndex = value;
			dispatchEvent(new Event(APPROVALSEPARATIONINDEX_CHANGE_EVENT));
		}
	}
}
