package com.gmgcolor.application.domain {
	public class TextMetrics {
		public function TextMetrics()
		{

		}

		public var color:uint;
		public var corner0x:Number;
		public var corner0y:Number;
		public var corner1x:Number;
		public var font:String;
		public var height:Number;
		public var character:String;
		public var index:int;
		public var selected:Boolean;
		public var size:Number;
		public var alpha:Number;
		public var render:Boolean = true;

	}
}