package com.gmgcolor.application.domain
{
	
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.ApprovalSeparationPlateFO")]
	public class ApprovalSeparationPlateFO
	{
		
		private var _ID:int
		
		private var _Page:int;
		
		private var  _Name:String;
		
		private var _FolderPath:String
		
		
		public function ApprovalSeparationPlateFO()
		{
			
		}


		public function get ID():int
		{
			return _ID;
		}

		public function set ID(value:int):void
		{
			_ID = value;
		}

		public function get Page():int
		{
			return _Page;
		}

		public function set Page(value:int):void
		{
			_Page = value;
		}

		public function get Name():String
		{
			return _Name;
		}

		public function set Name(value:String):void
		{
			_Name = value;
		}

		public function get FolderPath():String
		{
			return _FolderPath;
		}

		public function set FolderPath(value:String):void
		{
			_FolderPath = value;
		}

	}
}