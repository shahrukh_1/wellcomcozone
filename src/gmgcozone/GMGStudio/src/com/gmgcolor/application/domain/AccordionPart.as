package com.gmgcolor.application.domain
{
	[Bindable]
	public class AccordionPart
	{
		
		public var name:String;
		public var clazz:Class;
		public var visible:Boolean;
		
		public function AccordionPart(name:String,clazz:Class,visible:Boolean)
		{
			this.name = name;
			this.clazz = clazz;
			this.visible = visible;
		}
	}
}