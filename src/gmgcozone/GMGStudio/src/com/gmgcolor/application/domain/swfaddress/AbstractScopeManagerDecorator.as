package com.gmgcolor.application.domain.swfaddress
{
import org.spicefactory.parsley.core.bootstrap.BootstrapInfo;
import org.spicefactory.parsley.core.bootstrap.InitializingService;
import org.spicefactory.parsley.core.context.Context;
import org.spicefactory.parsley.core.messaging.MessageRouter;
import org.spicefactory.parsley.core.scope.Scope;
import org.spicefactory.parsley.core.scope.ScopeInfoRegistry;
import org.spicefactory.parsley.core.scope.ScopeManager;

public class AbstractScopeManagerDecorator implements InitializingService
	{
		protected var delegate:ScopeManager;

		private var bootstrapInfo:BootstrapInfo;
		
		protected function get context():Context
		{
			return bootstrapInfo.context;
		}
		protected function get scopeInfoRegistry():ScopeInfoRegistry
		{
			return bootstrapInfo.scopeInfoRegistry;
		}
		protected function get messageRouter():MessageRouter
		{
			return bootstrapInfo.messageRouter;
		}
		
		public function AbstractScopeManagerDecorator(delegate:ScopeManager)
		{
			this.delegate = delegate;
		}
		
		public function hasScope(name:String):Boolean
		{
			return delegate.hasScope(name);
		}

		public function getScope(name:String=null):Scope
		{
			return delegate.getScope(name);
		}

		public function getAllScopes():Array
		{
			return delegate.getAllScopes();
		}

		public function dispatchMessage(message:Object, selector:*=null):void
		{
			delegate.dispatchMessage(message,selector);
		}

		public function observeCommand(command:Command):void
		{
			delegate.observeCommand(command);
		}

		public function init(info:BootstrapInfo):void
		{
			this.bootstrapInfo = info;
		}
	}
}
