package com.gmgcolor.application.domain.swfaddress
{
import org.spicefactory.parsley.core.messaging.MessageProcessor;

public class NavigationManager extends Broadcaster
	{
		private var pendingNavigationQueue:Array = [];
		
		[MessageHandler]
		public function onNavigationMessage(message:BaseNavigatorMessage,processor:MessageProcessor):void
		{
			if (message is ICompositeNavigationMessage)
			{
				queueChildMessages(ICompositeNavigationMessage(message).childMessages);
			} else {
				dequeuePendingMessage(message);
			}
			if (!navigationInProgress)
			{
				broadcast(new NavigationCompleteMessage());
			}
		
		}
		
		private function dequeuePendingMessage(message:BaseNavigatorMessage):void
		{
			var index:int = pendingNavigationQueue.indexOf(message)
			if (index != -1)
			{
				pendingNavigationQueue.splice(index,1);
			}
		}
		
		public function get navigationInProgress():Boolean
		{
			return pendingNavigationQueue.length != 0;
		}

		
		private function queueChildMessages(childMessages:Array):void
		{
			for each (var message:Object in childMessages)
			{
				pendingNavigationQueue.push(message);
			}
		}
	}
}
