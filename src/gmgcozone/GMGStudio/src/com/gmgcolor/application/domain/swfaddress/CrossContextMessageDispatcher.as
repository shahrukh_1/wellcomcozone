package com.gmgcolor.application.domain.swfaddress
{
/**
	 * A message dispatcher for a specific message within a specific
	 * context scope.
	 * 
	 * Used for redispatching messages annotated with [CrossContextMessage]
	 * which were broadcast from antoehr context
	 * potentially before this context was created.
	 * 
	 * Messages that are annotated with [CrossContextMessage]
	 * are queued until a target explicitly registers to recieve them.
	 * */
	public class CrossContextMessageDispatcher
	{
		private var scopeManager:IContextAwareScopeManager;
		
		public function CrossContextMessageDispatcher(messageClass:Class,scopeManager:IContextAwareScopeManager)
		{
			this._type = messageClass;
			this.scopeManager = scopeManager;
		}
		private var _type:Class;
		public function get type():Class
		{
			return _type;
		}

		public function set type(value:Class):void
		{
			_type = value;
		}

		public function redispatchMessage(message:Object):void
		{
			scopeManager.dispatchContextMessage(message);
		}
	}
}
