package com.gmgcolor.application.domain
{
	public class TetFont
	{
		public function TetFont()
		{
			
		}
		
		public var xheight:Number;	
		public var weight:Number;	
		public var descender:Number;	
		public var italicangle:Number;	
		public var capheight:Number;	
		public var ascender:Number;		
		public var embedded:Boolean;	
		public var type:String;	
		public var fullname:String;	
		public var name:String;
		public var id:String;	

	}
}