package com.gmgcolor.application.domain
{

	public class PageSpread
	{
		private var _leftPage:ApprovalPage;
		private var _rightPage:ApprovalPage;
		
		private var _isSpread:Boolean;
		
		private var _hasLeft:Boolean;
		private var _hasRight:Boolean;
		
		public function PageSpread()
		{
			
		}
		
		public function get singlePage():ApprovalPage
		{
			if(hasLeft)
				return leftPage;
			if(hasRight)
				return rightPage;
			
			return null;
		}

		public function get hasLeft():Boolean
		{
			return _leftPage;
		}
		
		public function get hasRight():Boolean
		{
			return _rightPage;
		}

		public function get isSpread():Boolean
		{
			return _leftPage && _rightPage;
		}

		[Bindable]
		public function get leftPage():ApprovalPage
		{
			return _leftPage;
		}

		public function set leftPage(value:ApprovalPage):void
		{
			_leftPage = value;
		}
		[Bindable]
		public function get rightPage():ApprovalPage
		{
			return _rightPage;
		}

		public function set rightPage(value:ApprovalPage):void
		{
			_rightPage = value;
		}
		

	}
}