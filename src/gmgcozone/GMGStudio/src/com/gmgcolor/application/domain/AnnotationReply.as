package com.gmgcolor.application.domain
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

[Bindable]
	public class AnnotationReply
	{
		public function AnnotationReply()
		{
		}
		
		public var annotation:ApprovalAnnotationFO;
		public var repliedText:String;
	}
}
