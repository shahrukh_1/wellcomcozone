package com.gmgcolor.application.domain.swfaddress
{
import org.spicefactory.parsley.core.bootstrap.BootstrapConfig;

public class CrossContextMessageTarget implements BootstrapConfigProcessor
	{
		public var messageTypes:Array = [];
		
		public function set messageType(value:Class):void
		{
			messageTypes = [value];
		}
		public function processConfig(config:BootstrapConfig):void
		{
			config.services.scopeManager.addDecorator(ContextScopeManagerDecorator,messageTypes);
		}
	}
}
