package com.gmgcolor.application.domain
{
	public class DrawTools
	{
		
		private var _strokeWeight:int;
		
		private var _strokeColor:uint;
		
		private var _fillColor:uint;
		
		private var _strokeAlpha:Number;
		
		private var _fillAlpha:Number;
		
		public function DrawTools()
		{
			
		}


		public function get strokeWeight():int
		{
			return _strokeWeight;
		}

		public function set strokeWeight(value:int):void
		{
			_strokeWeight = value;
		}

		public function get strokeColor():uint
		{
			return _strokeColor;
		}

		public function set strokeColor(value:uint):void
		{
			_strokeColor = value;
		}

		public function get fillColor():uint
		{
			return _fillColor;
		}

		public function set fillColor(value:uint):void
		{
			_fillColor = value;
		}


		public function get strokeAlpha():Number
		{
			return _strokeAlpha;
		}

		public function set strokeAlpha(value:Number):void
		{
			_strokeAlpha = value;
		}

		public function get fillAlpha():Number
		{
			return _fillAlpha;
		}

		public function set fillAlpha(value:Number):void
		{
			_fillAlpha = value;
		}


	}
}