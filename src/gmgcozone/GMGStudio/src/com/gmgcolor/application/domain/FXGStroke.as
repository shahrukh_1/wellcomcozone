package com.gmgcolor.application.domain
{
	public class FXGStroke
	{
		
		private var _weight:Number;
		private var _caps:String;
		private var _joints:String;
		private var _miterLimit:Number;

		public function FXGStroke()
		{
		}
		

		public function get weight():Number
		{
			return _weight;
		}

		public function set weight(value:Number):void
		{
			_weight = value;
		}

		public function get caps():String
		{
			return _caps;
		}

		public function set caps(value:String):void
		{
			_caps = value;
		}

		public function get joints():String
		{
			return _joints;
		}

		public function set joints(value:String):void
		{
			_joints = value;
		}

		public function get miterLimit():Number
		{
			return _miterLimit;
		}

		public function set miterLimit(value:Number):void
		{
			_miterLimit = value;
		}

	}
}