package com.gmgcolor.application.domain.swfaddress
{
import avmplus.getQualifiedClassName;

import flash.utils.Dictionary;

import org.spicefactory.lib.logging.LogContext;
import org.spicefactory.lib.logging.Logger;
import org.spicefactory.lib.reflect.ClassInfo;
import org.spicefactory.lib.reflect.Metadata;

public class CrossContextMessageQueue
	{
		private static var metadataInitialized:Boolean;
		private var log:Logger = LogContext.getLogger(CrossContextMessageQueue)
		private var messageQueue:Array = [];
		private var listeners:Dictionary = new Dictionary(); // of key: MessageClass, value: CrossContextMessageTarget
		public function CrossContextMessageQueue()
		{
			initializeMetadata();
		}
		
		private static function initializeMetadata():void
		{
			if (!metadataInitialized)
			{
				Metadata.registerMetadataClass(CrossContextMessageMetadata);
				metadataInitialized = true;
			}
		}
		
		[MessageHandler]
		public function handleMessage(message:Object):void
		{
			var type:ClassInfo = ClassInfo.forInstance(message);
			if (!isCrossContextMessage(type))
				return;
			
			if (!messageHasTarget(type))
				queueMessage(message);
		}
		
		public function registerHandler(messageTarget:CrossContextMessageDispatcher):void
		{
			listeners[messageTarget.type] = messageTarget;
			sendQueuedMessages();
		}
		
		private function sendQueuedMessages():void
		{
			for each (var message:Object in messageQueue)
			{
				var messageType:ClassInfo = ClassInfo.forInstance(message);
				if (messageHasTarget(messageType))
				{
					resendMessage(message,messageType);
				}
			}
		}
		
		private function resendMessage(message:Object,messageType:ClassInfo):void
		{
			log.debug("Resending message of type {0}",messageType.name);
			var listener:CrossContextMessageDispatcher = getTargetForMessage(messageType);
			listener.redispatchMessage(message);
			var index:int = messageQueue.indexOf(message);
			messageQueue.splice(index,1);
		}
		private function queueMessage(message:Object):void
		{
			log.debug("Queued message of type {0}",getQualifiedClassName(message));
			messageQueue.push(message);
		}
		
		public function get size():int
		{
			return messageQueue.length;
		}
		
		private function getTargetForMessage(messageType:ClassInfo):CrossContextMessageDispatcher
		{
			return listeners[messageType.getClass()] as CrossContextMessageDispatcher;
		}
		private function messageHasTarget(type:ClassInfo):Boolean
		{
			return getTargetForMessage(type) != null;
		}
		
		private function isCrossContextMessage(type:ClassInfo):Boolean
		{
			return type.hasMetadata(CrossContextMessageMetadata) || type.isType(ICrossContextMessage);
		}
	}
}
