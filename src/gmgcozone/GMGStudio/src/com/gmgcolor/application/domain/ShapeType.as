package com.gmgcolor.application.domain
{
	
	[Bindable]
	public class ShapeType
	{
		public var xmlPath:Class
		
		public function ShapeType(xmlPath:Class)
		{
			this.xmlPath = xmlPath;
		}
	}
}