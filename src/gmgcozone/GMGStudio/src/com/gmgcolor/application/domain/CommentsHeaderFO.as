package com.gmgcolor.application.domain
{
	[Bindable]
	public class CommentsHeaderFO
	{
		
		public var data:Boolean;
		public var label:String;
		
		public function CommentsHeaderFO(data:Boolean,label:String)
		{
			this.data = data;
			this.label = label;
		}
		
	}
}