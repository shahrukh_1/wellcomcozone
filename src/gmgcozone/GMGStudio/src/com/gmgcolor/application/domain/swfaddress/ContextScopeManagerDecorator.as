package com.gmgcolor.application.domain.swfaddress
{

import org.spicefactory.lib.logging.LogContext;
import org.spicefactory.lib.logging.Logger;
import org.spicefactory.lib.reflect.ClassInfo;
import org.spicefactory.parsley.core.bootstrap.BootstrapInfo;
import org.spicefactory.parsley.core.context.Context;
import org.spicefactory.parsley.core.events.ContextEvent;
import org.spicefactory.parsley.core.messaging.Message;
import org.spicefactory.parsley.core.messaging.MessageReceiverCache;
import org.spicefactory.parsley.core.messaging.impl.DefaultMessage;
import org.spicefactory.parsley.core.scope.ScopeInfo;
import org.spicefactory.parsley.core.scope.ScopeManager;

/**
	 * This class decorates the default ScopeManager, adding the ability
	 * to send a message directly to objects within a context.
	 * This feature is not available out-of-the-box in Parsley, where there are
	 * two concepts of Scope : GLOBAL and LOCAL.
	 * However, LOCAL scope does not receive messages broadcast within the local context
	 * at a GLOBAL scope.  Therefore, sending messages targeted directly to a specific
	 * context, without explicilty specifying the scope, is not possible.
	 * 
	 * This class modifies this behaviour, adding the ability to directly target a scope.
	 * Only message receivers that are decalred directly within the specified context will receive
	 * the message, regardless of scope.
	 * 
	 * This is typcially useful when a GLOBAL message was dispatched, but the recieving context
	 * was not instantiated at the time to receive it (ie., a module being loaded, or a context 
	 * being created deferred).
	 * */
	public class ContextScopeManagerDecorator extends AbstractScopeManagerDecorator implements ScopeManager, IContextAwareScopeManager
	{
		private static const log:Logger = LogContext.getLogger(ContextScopeManagerDecorator);
		private var messageClasses:Array;
		public function ContextScopeManagerDecorator(delegate:ScopeManager,messageClasses:Array)
		{
			super(delegate);
			this.messageClasses = messageClasses;
		}
		
		public function dispatchContextMessage(instance:Object,selector:*=null):void
		{
			var type:ClassInfo = ClassInfo.forInstance(instance, context.domain);
			var cache:MessageReceiverCache = getMessageRecieverCache(type,context);
			if (!selector) {
				selector = cache.getSelectorValue(instance);
			}
			var message:Message = new DefaultMessage(instance, type, selector, context);
			if (cache.getReceivers(message, MessageReceiverKind.TARGET).length == 0) {
				if (log.isDebugEnabled()) {
					log.debug("Discarding message '{0}': no matching receiver in any scope", instance);
				}
				return;
			}
			messageRouter.dispatchMessage(message, cache);
		}
		
		
		private function getMessageRecieverCache(type:ClassInfo,context:Context):MessageReceiverCache
		{
			var caches:Array = [];
			for each (var scope:ScopeInfo in scopeInfoRegistry.activeScopes) {
				caches.push(scope.getMessageReceiverCache(type));
			}
			return new ContextFilteredMessageRecieverCache(caches,context);
		}
		
		override public function init(info:BootstrapInfo):void
		{
			super.init(info);
			if (!context.initialized)
			{
				context.addEventListener(ContextEvent.INITIALIZED,onContextInitialized);
			} else {
				onContextInitialized();
			}
		}

		private function onContextInitialized(event:ContextEvent=null):void
		{
			registerMessageDispatchers(messageClasses,context);			
		}
		
		private function registerMessageDispatchers(messageClasses:Array, context:Context):void
		{
			var messageQueue:CrossContextMessageQueue = context.getObjectByType(CrossContextMessageQueue) as CrossContextMessageQueue;
			for each (var messageClass:Class in messageClasses)
			{
				var messageHandler:CrossContextMessageDispatcher = new CrossContextMessageDispatcher(messageClass,this);
				messageQueue.registerHandler(messageHandler);
			}
		}		
		
	}
}

import org.spicefactory.parsley.core.context.Context;
import org.spicefactory.parsley.core.context.provider.ObjectProvider;
import org.spicefactory.parsley.core.messaging.Message;
import org.spicefactory.parsley.core.messaging.MessageReceiverCache;
import org.spicefactory.parsley.core.state.GlobalState;

/**
 * An implementation of Parsley's MessageReceiverCache
 * which returns only the message receivers defined in a specific context
 * */
class ContextFilteredMessageRecieverCache implements MessageReceiverCache
{
	private var caches:Array;
	private var context:Context;
	public function ContextFilteredMessageRecieverCache(caches:Array,context:Context)
	{
		this.caches = caches;
		this.context = context;
	}
	
	private var _recievers:Array;
	public function getReceivers(message:Message, kind:MessageReceiverKind):Array
	{
		if (!_recievers)
			buildRecievers(message,kind);
		return _recievers;	
	}
	
	private function buildRecievers(message:Message, kind:MessageReceiverKind):void
	{
		var result:Array = [];
		for each (var cache:MessageReceiverCache in caches)
		{
			var recievers:Array = cache.getReceivers(message,MessageReceiverKind.TARGET);
			for each (var handler:MessageHandler in recievers)
			{
				var handlerContext:Context = getHandlerContext(handler.provider);
				if (handlerContext == context)
					result.push(handler);
			}
		}
		_recievers = result;
	}
	private function getHandlerContext(provider:ObjectProvider):Context
	{
		var instance:Object = provider.instance;
		return GlobalState.objects.getContext(provider.instance);
	}
	
	public function getSelectorValue(message:Object):*
	{
		var defaultCache:MessageReceiverCache = caches[0];
		return defaultCache.getSelectorValue(message);
	}
}
