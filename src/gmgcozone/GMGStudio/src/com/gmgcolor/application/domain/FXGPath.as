package com.gmgcolor.application.domain
{
import mx.collections.ArrayCollection;

[Bindable]
	public class FXGPath
	{
		
		private var _x:Number;
		private var _y:Number;
		private var _winding:String;
		private var _data:String;
		private var _stroke:FXGStroke;
		private var _fill:FXGFill
		private var _xml:XML;


		private var _vector_commands:Vector.<int> = new Vector.<int>; 
		private var _vector_coord:Vector.<Number> = new Vector.<Number>; 
		
		
		public function FXGPath()
		{
		}


		public function get x():Number
		{
			return _x;
		}

		public function set x(value:Number):void
		{
			_x = value;
		}

		public function get y():Number
		{
			return _y;
		}

		public function set y(value:Number):void
		{
			_y = value;
		}

		public function get winding():String
		{
			return _winding;
		}

		public function set winding(value:String):void
		{
			_winding = value;
		}

		public function get data():String
		{
			return _data;
		}

		public function set data(value:String):void
		{
			
			var a:ArrayCollection = new ArrayCollection(value.split(" "));
			
			a.removeItemAt(0);
			
			var count:int;
			var movedTo:Boolean;
			var data:int;
			
			for each (var str:String in a) 
			{
			
				_vector_coord.push(Number(str))
				count++;
				
				if(count==2)
				{
					if(!movedTo)
					{
						movedTo = true;
						data = 0
					}
					else
					{
						data = 1;
					}
					_vector_commands.push(data)
					count = 0
				}
				
			}
			
			
	/*		data="M139.048 " +
				"23.0158C139.048 23.0158 " +
				"5.95456 -31.9139 " +
				"1.02487 27.9455" +
				" -3.90482 87.8049" +
				" 7.35739 117.382" +
				" 70.0361 83.5793 " +
				"132.715 49.7765 " +
				"143.161 30.0588 " +
				"106.952 20.1994"*/
			
				
			_data = value;
		}
		
		public function get vector_commands():Vector.<int>
		{
			return _vector_commands;
		}
		
		public function get vector_coord():Vector.<Number>
		{
			return _vector_coord;
		}


		public function get stroke():FXGStroke
		{
			return _stroke;
		}

		public function set stroke(value:FXGStroke):void
		{
			_stroke = value;
		}


		public function get fill():FXGFill
		{
			return _fill;
		}

		public function set fill(value:FXGFill):void
		{
			_fill = value;
		}


		public function get xml():XML
		{
			return _xml;
		}

		public function set xml(value:XML):void
		{
			_xml = value;
		}


	}
}

