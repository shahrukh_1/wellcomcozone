package com.gmgcolor.application.model
{
import com.gmgcolor.annotations.domain.ApprovalCollaborator;

import flash.events.Event;
import flash.events.EventDispatcher;

import mx.collections.ArrayCollection;

[Event(name="collaboratorsChange",type="flash.events.Event")]
	public class CollaboratorModel extends EventDispatcher
	{
		
		public static const COLLABORATORS_CHANGE_EVENT:String = "collaboratorsChange";
		
		private var _collaborators:ArrayCollection;
		
		public function CollaboratorModel()
		{
			super()
		}
		
		[Bindable(event="collaboratorsChange")]
		public function get currentCollaborators():ArrayCollection
		{
			return _collaborators;
		}

		public function set currentCollaborators(value:ArrayCollection):void
		{
			if (_collaborators != value)
			{
				_collaborators = value;
				dispatchEvent(new Event(COLLABORATORS_CHANGE_EVENT));
			}
		}
		
		public function getApprovalCollaborator(id:int):ApprovalCollaborator
		{
			for each (var approvalCollaborator:ApprovalCollaborator in currentCollaborators) 
			{
				if(approvalCollaborator.Collaborator == id)
				{
					return approvalCollaborator;
				}
			}
			return null;
		}
		
		public function getApprovalCollaboratorAvatar(id:int):String
		{
			for each (var approvalCollaborator:ApprovalCollaborator in currentCollaborators) 
			{
				if(approvalCollaborator.Collaborator == id)
				{
					return approvalCollaborator.CollaboratorAvatar;
				}
			}
			return "";
		}
		
		public function getApprovalCollaboratorName(id:int):String
		{
			for each (var approvalCollaborator:ApprovalCollaborator in currentCollaborators) 
			{
				if(approvalCollaborator.Collaborator == id)
				{
					return approvalCollaborator.GivenName + " " + approvalCollaborator.FamilyName;
				}
			}
			return "";
		}

	}
}
