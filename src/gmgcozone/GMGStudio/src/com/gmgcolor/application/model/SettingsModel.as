package com.gmgcolor.application.model
{
import flash.events.Event;
import flash.events.EventDispatcher;

[Event(name="allowMultiplePanelsOpenChange",type="flash.events.Event")]
	[Event(name="userslOpenChange",type="flash.events.Event")]
	[Event(name="annotationsOpenChange",type="flash.events.Event")]
	[Event(name="showPinsChange",type="flash.events.Event")]
	public class SettingsModel extends EventDispatcher
	{
		public static const ALLOWMULTIPLEPANELSOPEN_CHANGE_EVENT:String = "allowMultiplePanelsOpenChange";
		public static const USERSLOPEN_CHANGE_EVENT:String = "userslOpenChange";
		public static const ANNOTATIONSOPEN_CHANGE_EVENT:String = "annotationsOpenChange";
		public static const SHOWPINS_CHANGE_EVENT:String = "showPinsChange";
		private var _allowMultiplePanelsOpen:Boolean;
		private var _userslOpen:Boolean;
		private var _annotationsOpen:Boolean;
		
		private var _showPins:Boolean = true;



		[Bindable(event="allowMultiplePanelsOpenChange")]
		public function get allowMultiplePanelsOpen():Boolean
		{
			return _allowMultiplePanelsOpen;
		}

		public function set allowMultiplePanelsOpen(value:Boolean):void
		{
			if (_allowMultiplePanelsOpen != value)
			{
				_allowMultiplePanelsOpen = value;
				dispatchEvent(new Event(ALLOWMULTIPLEPANELSOPEN_CHANGE_EVENT));
			}
		}

		[Bindable(event="userslOpenChange")]
		public function get userslOpen():Boolean
		{
			return _userslOpen;
		}

		public function set userslOpen(value:Boolean):void
		{
			if (_userslOpen != value)
			{
				_userslOpen = value;
				dispatchEvent(new Event(USERSLOPEN_CHANGE_EVENT));
			}
		}

		[Bindable(event="annotationsOpenChange")]
		public function get annotationsOpen():Boolean
		{
			return _annotationsOpen;
		}

		public function set annotationsOpen(value:Boolean):void
		{
			if (_annotationsOpen != value)
			{
				_annotationsOpen = value;
				dispatchEvent(new Event(ANNOTATIONSOPEN_CHANGE_EVENT));
			}
		}

		[Bindable(event="showPinsChange")]
		public function get showPins():Boolean
		{
			return _showPins;
		}

		public function set showPins(value:Boolean):void
		{
			if (_showPins != value)
			{
				_showPins = value;
				dispatchEvent(new Event(SHOWPINS_CHANGE_EVENT));
			}
		}

		
		public function SettingsModel()
		{
		}
	}
}
