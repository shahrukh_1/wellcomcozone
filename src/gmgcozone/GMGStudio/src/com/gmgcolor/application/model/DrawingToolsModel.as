package com.gmgcolor.application.model
{
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;

[Event(name="currentAnnotationToolChange",type="flash.events.Event")]
	[Event(name="moveToolSelectedChange",type="flash.events.Event")]
	[Event(name="pencilToolSelectedChange",type="flash.events.Event")]
	[Event(name="shapesToolSelectedChange",type="flash.events.Event")]
	[Event(name="currentDrawingToolChange",type="flash.events.Event")]
	public class DrawingToolsModel extends EventDispatcher
	{
		
		public static const CURRENTANNOTATIONTOOL_CHANGE_EVENT:String = "currentAnnotationToolChange";
		public static const MOVETOOLSELECTED_CHANGE_EVENT:String = "moveToolSelectedChange";
		public static const PENCILTOOLSELECTED_CHANGE_EVENT:String = "pencilToolSelectedChange";
		public static const SHAPESTOOLSELECTED_CHANGE_EVENT:String = "shapesToolSelectedChange";
		public static const CURRENTDRAWINGTOOL_CHANGE_EVENT:String = "currentDrawingToolChange";

		
		private var _moveToolSelected:Boolean;
		private var _pencilToolSelected:Boolean;
		private var _shapesToolSelected:Boolean;
		
		private var _currentDrawingTool:String;
		
		
		public function DrawingToolsModel(target:IEventDispatcher=null)
		{
			super(target);
		}



		[Bindable(event="moveToolSelectedChange")]
		public function get moveToolSelected():Boolean
		{
			return _moveToolSelected;
		}

		public function set moveToolSelected(value:Boolean):void
		{
			if (_moveToolSelected != value)
			{
				_moveToolSelected = value;
				dispatchEvent(new Event(MOVETOOLSELECTED_CHANGE_EVENT));
			}
		}

		[Bindable(event="pencilToolSelectedChange")]
		public function get pencilToolSelected():Boolean
		{
			return _pencilToolSelected;
		}

		public function set pencilToolSelected(value:Boolean):void
		{
			if (_pencilToolSelected != value)
			{
				_pencilToolSelected = value;
				dispatchEvent(new Event(PENCILTOOLSELECTED_CHANGE_EVENT));
			}
		}

		[Bindable(event="shapesToolSelectedChange")]
		public function get shapesToolSelected():Boolean
		{
			return _shapesToolSelected;
		}

		public function set shapesToolSelected(value:Boolean):void
		{
			if (_shapesToolSelected != value)
			{
				_shapesToolSelected = value;
				dispatchEvent(new Event(SHAPESTOOLSELECTED_CHANGE_EVENT));
			}
		}


		[Bindable(event="currentDrawingToolChange")]
		public function get currentDrawingTool():String
		{
			return _currentDrawingTool;
		}

		public function set currentDrawingTool(value:String):void
		{
			if (_currentDrawingTool != value)
			{
				_currentDrawingTool = value;
				dispatchEvent(new Event(CURRENTDRAWINGTOOL_CHANGE_EVENT));
			}

	}
	}
}
