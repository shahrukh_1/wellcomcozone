package com.gmgcolor.application.model
{
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;

[Event(name="currentAnnotationToolChange",type="flash.events.Event")]
	public class AnnotationToolsModel extends EventDispatcher
	{
		
		public static const HANDBUTTONSELECTED_CHANGE_EVENT:String = "handButtonSelectedChange";
		public static const ANNOTATEBUTTONSELECTED_CHANGE_EVENT:String = "annotateButtonSelectedChange";
		public static const DRAWBUTTONSELECTED_CHANGE_EVENT:String = "drawButtonSelectedChange";
		public static const TEXTBUTTONSELECTED_CHANGE_EVENT:String = "textButtonSelectedChange";
		public static const CURRENTANNOTATIONTOOL_CHANGE_EVENT:String = "currentAnnotationToolChange";
		
		private var _handButtonSelected:Boolean;
		private var _annotateButtonSelected:Boolean;
		private var _drawButtonSelected:Boolean;
		private var _textButtonSelected:Boolean;
		
		private var _currentAnnotationTool:String;
		
		public function AnnotationToolsModel(target:IEventDispatcher=null)
		{
			super(target);
		}

		public function get textButtonSelected():Boolean
		{
			return _textButtonSelected;
		}

		public function set textButtonSelected(value:Boolean):void
		{
			if( _textButtonSelected !== value)
			{
				_textButtonSelected = value;
				dispatchEvent(new Event("TEXTBUTTONSELECTED_CHANGE_EVENT"));
			}
		}

		public function get handButtonSelected():Boolean
		{
			return _handButtonSelected;
		}

		public function set handButtonSelected(value:Boolean):void
		{
			if (_handButtonSelected != value)
			{
				_handButtonSelected = value;
				dispatchEvent(new Event(HANDBUTTONSELECTED_CHANGE_EVENT));
			}
		}

		public function get annotateButtonSelected():Boolean
		{
			return _annotateButtonSelected;
		}

		public function set annotateButtonSelected(value:Boolean):void
		{
			if (_annotateButtonSelected != value)
			{
				_annotateButtonSelected = value;
				dispatchEvent(new Event(ANNOTATEBUTTONSELECTED_CHANGE_EVENT));
			}
		}

		public function get drawButtonSelected():Boolean
		{
			return _drawButtonSelected;
		}

		public function set drawButtonSelected(value:Boolean):void
		{
			if (_drawButtonSelected != value)
			{
				_drawButtonSelected = value;
				dispatchEvent(new Event(DRAWBUTTONSELECTED_CHANGE_EVENT));
			}
		}


		[Bindable(event="currentAnnotationToolChange")]
		public function get currentAnnotationTool():String
		{
			return _currentAnnotationTool;
		}

		public function set currentAnnotationTool(value:String):void
		{
			if (_currentAnnotationTool != value)
			{
				_currentAnnotationTool = value;
				dispatchEvent(new Event(CURRENTANNOTATIONTOOL_CHANGE_EVENT));
			}
		}
	}
}
