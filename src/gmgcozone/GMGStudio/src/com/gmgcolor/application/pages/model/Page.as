package com.gmgcolor.application.pages.model
{
	[Bindable]
	public class Page
	{
		public function Page()
		{
		}
		
		public var number:int;
		public var filename:String;
		public var isMainViewSelected:Boolean;
	}
}