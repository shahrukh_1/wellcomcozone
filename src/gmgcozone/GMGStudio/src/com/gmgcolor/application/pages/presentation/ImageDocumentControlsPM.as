package com.gmgcolor.application.pages.presentation {
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.DisplaySepsEvent;
import com.gmgcolor.annotations.event.HighlightEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.event.PositionSepWindowEvent;
import com.gmgcolor.annotations.event.SepsChangeEvent;
import com.gmgcolor.annotations.presentation.SepsPopUpMenu;
import com.gmgcolor.application.event.AnnotationToolsEvent;
import com.gmgcolor.application.event.DocumentControlsCreated;
import com.gmgcolor.application.event.NavigatorCloseEvent;
import com.gmgcolor.application.event.PixelBlendBitmapsEvent;
import com.gmgcolor.application.event.VersionCompareEvent;
import com.gmgcolor.application.pages.enum.PageSizeTypeEnum;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;
import com.gmgcolor.application.pages.model.ImageDocumentControlsModel;
import com.gmgcolor.application.presentation.ApplicationContainerPM;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.events.FlexMouseEvent;
import mx.managers.PopUpManager;

import org.apache.flex.spark.utils.ColorPickerUtil;

import spark.components.ToggleButton;
import spark.events.IndexChangeEvent;

public class ImageDocumentControlsPM {

		[Inject]
		public var documentControlsModel:ImageDocumentControlsModel;

        [Inject]
        public var documentControlsPM:DocumentControlsPM;

		[Bindable]
		public var compareState:String = "normal";

		[Bindable]
		public var pageSizeTypes:ArrayCollection;

		[Bindable]
		public var hSliderValue:int;
		[Bindable]
		public var labelDropDownList:String;
		private var _navigatorBtnSelected:Boolean = false;

		[Bindable]
		public var selectedIndexDropDownList:int;

		public var thumbnailsHeight:Number = 100;

		[Bindable]
		public var popUpState:String = "normal";

		[Bindable]
		public var pagesLength:int;

		private var sepsPopUpMenu:SepsPopUpMenu;

		private var _documentScale:Number;

        [Inject]
        public var applicationPresenter:ApplicationContainerPM;

		//private var _view:SingleDocumentControls;

		/*	private var btnNavigate:ToggleButton;

		 private var _closeIt:Boolean;*/

		/*private var _navigatorOpen:Boolean;*/

		public function ImageDocumentControlsPM()
		{

			pageSizeTypes = new ArrayCollection();
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_25_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_50_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_75_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_100_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_125_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_150_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_200_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_300_PERCENT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_FIT_WIDTH);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_FIT_HEIGHT);
			pageSizeTypes.addItem(PageSizeTypeEnum.PAGE_SIZE_TYPE_FIT_PAGE);

		}


		private function onClose(event:CloseEvent):void
		{
            documentControlsPM.dispatcher(new DisplaySepsEvent(SepsChangeEvent.CHANGE, false));
		}

		[MessageHandler]
		public function onSepsDisplayEvent(event:DisplaySepsEvent):void
		{
			if (event.display && !sepsPopUpMenu) {
				sepsPopUpMenu = new SepsPopUpMenu();
				sepsPopUpMenu.addEventListener(CloseEvent.CLOSE, onClose);
                documentControlsPM.context.viewManager.addViewRoot(sepsPopUpMenu);
				PopUpManager.addPopUp(sepsPopUpMenu, Main(FlexGlobals.topLevelApplication));
                documentControlsPM.dispatcher(new PositionSepWindowEvent(PositionSepWindowEvent.POSITION, sepsPopUpMenu));
                documentControlsPM.sepsSelected = true;
			} else if (sepsPopUpMenu) {
                documentControlsPM.context.viewManager.removeViewRoot(sepsPopUpMenu);
				PopUpManager.removePopUp(sepsPopUpMenu);
				sepsPopUpMenu = null;
                documentControlsPM.sepsSelected = false;
			}
		}


		public function emailPopUp_mouseDownOutsideHandler(event:FlexMouseEvent):void
		{
			popUpState = 'normal';
		}

		public function group1_creationCompleteHandler(event:FlexEvent):void
		{
            documentControlsPM.dispatcher(new DocumentControlsCreated(DocumentControlsCreated.CREATED));
		}


		public function onShowPagesBtnClick(event:MouseEvent):void
		{
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.SINGLE_PAGE_BUTTON_CLICK));

		}

		private var versions:Array;

		[MessageHandler]
		public function onVersionButtonCompare(event:VersionCompareEvent):void
		{
			if (event.versions.length == 1) {
                documentControlsPM.currentState = documentControlsPM.approvalModel.currentApproval.ApprovalType;
				documentControlsModel.isCompareMode = false;
                applicationPresenter.documentState = "horizontal";
			} else {
                documentControlsPM.currentState = 'compare';
				documentControlsModel.isCompareMode = true;
			}
            if(documentControlsPM.view)
            {
                documentControlsPM.view.setCurrentState(documentControlsPM.currentState);
            }
            documentControlsPM.approvalModel.setPageCollection(event.versions);

			versions = event.versions;
		}

		public function onCompareModeBtnClick(event:MouseEvent):void
		{
            documentControlsPM.currentState = ToggleButton(event.target).selected ? 'compare' : 'normal';
			documentControlsModel.isCompareMode = ToggleButton(event.target).selected;
            documentControlsPM.dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
            documentControlsPM.dispatcher(new HighlightEvent(HighlightEvent.DISABLE));
            documentControlsPM.dispatcher(new CommentsEvent(CommentsEvent.CANCEL));
		}

		public function onSyncModeBtnClick(event:MouseEvent):void
		{
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.SYNC_BUTTON_CLICK));
		}

		public function onRotateBtnClick(event:Event):void
		{
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.ROTATE_BUTTON_CLICK));
		}

		public function get documentScale():Number
		{
			return _documentScale;
		}

		public function set documentScale(value:Number):void
		{
			_documentScale = value;
			hSliderValue = value * 100;
			selectedIndexDropDownList = -1;
			labelDropDownList = String(Math.round(value * 100)) + "%";
		}


		public function onShowSepsBtnClick(event:MouseEvent):void
		{
            documentControlsPM.dispatcher(new DisplaySepsEvent(DisplaySepsEvent.DISPLAY, ToggleButton(event.target).selected));
		}

		[MessageHandler(scope="global")]
		public function onNavigatorClose(e:NavigatorCloseEvent):void
		{
			navigatorBtnSelected = false;
		}

		public function onFitToContentBtnClick():void
		{
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.FIT_TO_CONTENT_BUTTON_CLICK));
		}

		public function onOneToOneBtnClick():void
		{
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.ONE_TO_ONE_BUTTON_CLICK));
		}

		public function onNavigatorWindowBtnClick(event:MouseEvent):void
		{
			navigatorBtnSelected = event.target.selected;
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.NAVIGATOR_BUTTON_CLICK, event.target));
		}


		public function onButtonBarChange(event:IndexChangeEvent):void
		{
            documentControlsPM.dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.RESET));
            documentControlsPM.dispatcher(new CommentsEvent(CommentsEvent.CANCEL));
			switch (event.newIndex) {
				case 0:
					compareState = "normal";
                    documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.COMPARE_BUTTON_CLICK));
					changeSplit();
					break;
				case 1:
					// Need to lock views together
					compareState = "pixelCompare";
                    documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.PIXEL_DIFFERENCE_BUTTON_CLICK));
                    documentControlsPM.lockTimer = new Timer(100, 1);
                    documentControlsPM.lockTimer.addEventListener(TimerEvent.TIMER_COMPLETE, documentControlsPM.onLockTimer);
                    documentControlsPM.lockTimer.start();
					break;
			}
		}

		private var _splitIndex:int = 0;

		public function onSplitChange(event:IndexChangeEvent):void {
			_splitIndex = event.newIndex;
			changeSplit();
		}

		private function changeSplit():void {
			switch (_splitIndex) {
				case 0:
                    documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.HORIZONTAL_TILE_BUTTON_CLICK));
					break;
				case 1:
                    documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.VERTICAL_TILE_BUTTON_CLICK));
					break;
			}
		}

		[Bindable]
		public function get navigatorBtnSelected():Boolean
		{
			return _navigatorBtnSelected;
		}

		public function set navigatorBtnSelected(value:Boolean):void
		{
			_navigatorBtnSelected = value;
		}

		public function onDoneClick():void
		{
			compareState = documentControlsPM.currentState = "normal";
			changeSplit();
            documentControlsPM.dispatcher(new DocumentControlsEvent(DocumentControlsEvent.DONE_BUTTON_CLICK));
            documentControlsPM.dispatcher(new VersionCompareEvent(VersionCompareEvent.COMPARE, [documentControlsPM.approvalModel.currentApprovalVersions.getItemAt(documentControlsPM.approvalModel.currentApprovalVersions.length - 1)]));
		}

		public function onPixelCompareSensitivity(value:Number):void
		{
            documentControlsPM.dispatcher(new PixelBlendBitmapsEvent(PixelBlendBitmapsEvent.SENSITIVITY, [value / 100]));
		}

		public function onPixelCompareBlend(value:Number):void
		{
            documentControlsPM.dispatcher(new PixelBlendBitmapsEvent(PixelBlendBitmapsEvent.BLEND, [value / 100]));
		}

		public function onPixelCompareColor(value:int):void
		{
            documentControlsPM.dispatcher(new PixelBlendBitmapsEvent(PixelBlendBitmapsEvent.COLOR, ColorPickerUtil.uintToRGB(value)));
		}

        public function registerView(value:PageControls):void
        {
            documentControlsPM.registerView(value);
        }
	}
}
