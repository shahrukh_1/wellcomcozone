package com.gmgcolor.application.pages.presentation {

import com.gmgcolor.application.pages.events.VideoControlsEvent;
import com.gmgcolor.application.pages.events.VideoStateChangeEvent;
import com.gmgcolor.application.pages.events.VideoTimeChangeEvent;
import com.gmgcolor.application.pages.model.VideoDocumentControlsModel;

import flash.events.Event;

public class VideoDocumentControlsPM {

        [Inject]
        public var model:VideoDocumentControlsModel;

        [Inject]
        public var documentControlsPM:DocumentControlsPM;
    
        public function VideoDocumentControlsPM()
		{

		}

        public function onGoToStart(event:Event):void
        {
            event.target.enabled = false;
            documentControlsPM.dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_GOTOSTART));
        }

        public function onBackward():void
        {
            documentControlsPM.dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_GOBACK))
        }

        public function onForward():void
        {
            documentControlsPM.dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_GOFORWARD))
        }

        public function onGoToEnd(event:Event, forwardButton:*):void
        {
            event.target.enabled = false;
            if(forwardButton)
            {
                forwardButton.enabled = false;
            }
            documentControlsPM.dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_GOTOEND));
        }

        public function onPlay():void {
            documentControlsPM.dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_PLAY));
        }

        public function onPause():void {
            documentControlsPM.dispatcher(new VideoControlsEvent(VideoControlsEvent.VIDEO_PAUSE));
        }

        [MessageHandler(selector="videoDisablePlay", scope="global")]
        public function onDisablePlay(e:VideoControlsEvent):void
        {
            model.isPlayEnabled = false;
        }

        [MessageHandler(selector="videoEnablePlay", scope="global")]
        public function onEnablePlay(e:VideoControlsEvent):void
        {
            model.isPlayEnabled = true;
        }

        [MessageHandler(scope="global")]
        public function onStateChange(e:VideoStateChangeEvent):void
        {
            model.isPlaying = e.state == 'playing';
            if(e.videoPlayer)
            {
                model.totalTime = e.videoPlayer.duration;
            }
        }

        [MessageHandler(scope="global")]
        public function onTimeChange(e:VideoTimeChangeEvent):void
        {
            model.currentTime = e.time;
//            var newState:Boolean = e.videoPlayer.mediaPlayerState == 'playing';
//            if(model.isPlaying !== newState)
//            {
//                model.isPlaying = newState;
//            }
        }
    
        public function registerView(value:PageControls):void
        {
            documentControlsPM.registerView(value);
        }
    }
}
