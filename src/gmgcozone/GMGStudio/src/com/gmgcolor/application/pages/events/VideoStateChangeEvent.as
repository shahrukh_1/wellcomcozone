package com.gmgcolor.application.pages.events {
import com.gmgcolor.annotations.presentation.CustomVideoPlayer;

import flash.events.Event;

public class VideoStateChangeEvent extends Event
{
    private var _videoPlayer:CustomVideoPlayer;
    private var _state:String;
    public static const VIDEO_STATE_CHANGE:String = 'videoStateChange';

    public function VideoStateChangeEvent(type:String, state:String, videoPlayer:*)
    {
        super(type);
        this.state = state;
        this.videoPlayer = videoPlayer;
    }

    public override function clone():Event
    {
        return new VideoControlsEvent(type, videoPlayer);
    }

    public override function toString():String
    {
        return formatToString("VideoStateChangeEvent");
    }

    public function get videoPlayer():CustomVideoPlayer {
        return _videoPlayer;
    }

    public function set videoPlayer(value:CustomVideoPlayer):void {
        _videoPlayer = value;
    }

    public function get state():String {
        return _state;
    }

    public function set state(value:String):void {
        _state = value;
    }
}
}
