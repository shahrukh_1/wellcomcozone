package com.gmgcolor.application.pages.events
{
import flash.events.Event;

public class CollaboratorSelectEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _collaboratorIds:Object;

		public function CollaboratorSelectEvent(type:String, collaboratorIds:Object)
		{
			super(type,false,false);

			this._collaboratorIds = collaboratorIds;
		}

		public function get collaboratorIds():Object
		{
			return _collaboratorIds;
		}

		public override function clone():Event
		{
			return new CollaboratorSelectEvent(type,collaboratorIds);
		}

		public override function toString():String
		{
			return formatToString("CollaboratorSelectEvent","collaboratorIds");
		}
	}
}
