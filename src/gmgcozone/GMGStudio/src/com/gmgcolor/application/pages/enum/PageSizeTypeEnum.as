package com.gmgcolor.application.pages.enum
{
	public class PageSizeTypeEnum
	{
		
		static public const PAGE_SIZE_TYPE_25_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('25%', 0.25);
		static public const PAGE_SIZE_TYPE_50_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('50%', 0.50);
		static public const PAGE_SIZE_TYPE_75_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('75%', 0.75);
		static public const PAGE_SIZE_TYPE_100_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('100%', 1);
		static public const PAGE_SIZE_TYPE_125_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('125%', 1.25);
		static public const PAGE_SIZE_TYPE_150_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('150%', 1.50);
		static public const PAGE_SIZE_TYPE_200_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('200%', 2);
		static public const PAGE_SIZE_TYPE_300_PERCENT:PageSizeTypeEnum = new PageSizeTypeEnum('300%', 3);
		static public const PAGE_SIZE_TYPE_FIT_WIDTH:PageSizeTypeEnum = new PageSizeTypeEnum(PAGE_SIZE_LABEL_FIT_WIDTH, 0);
		static public const PAGE_SIZE_TYPE_FIT_HEIGHT:PageSizeTypeEnum = new PageSizeTypeEnum(PAGE_SIZE_LABEL_FIT_HEIGHT, 0);
		static public const PAGE_SIZE_TYPE_FIT_PAGE:PageSizeTypeEnum = new PageSizeTypeEnum(PAGE_SIZE_LABEL_FIT_PAGE, 0);
		
		static public const PAGE_SIZE_LABEL_FIT_WIDTH:String = "Fit Width";
		static public const PAGE_SIZE_LABEL_FIT_HEIGHT:String = "Fit Height";
		static public const PAGE_SIZE_LABEL_FIT_PAGE:String = "Fit Page";
		

		private var _label:String;
		private var _data:Number;
		
		public function PageSizeTypeEnum(label:String,data:Number)
		{
			this._label = label;
			this._data = data;
		}
		
		public function get label():String
		{
			return _label;
		}
		
		public function get data():Number
		{
			return _data;
		}
	}
}

