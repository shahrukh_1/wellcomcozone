package com.gmgcolor.application.pages.enum
{
	public class EyeDropperTypeEnum
	{
		
		static public const COLOUR_HEX:EyeDropperTypeEnum = new EyeDropperTypeEnum('HEX', COLOUR_HEX_DATA);
		static public const COLOUR_RGB:EyeDropperTypeEnum = new EyeDropperTypeEnum('RGB', COLOUR_RGB_DATA);
		static public const COLOUR_CMYK:EyeDropperTypeEnum = new EyeDropperTypeEnum('CMYK', COLOUR_CMYK_DATA);
		
		static public const COLOUR_HEX_DATA:int = 1;
		static public const COLOUR_RGB_DATA:int = 2;
		static public const COLOUR_CMYK_DATA:int = 3;

		private var _label:String;
		private var _data:Number;
		
		public function EyeDropperTypeEnum(label:String,data:Number)
		{
			this._label = label;
			this._data = data;
		}
		
		public function get label():String
		{
			return _label;
		}
		
		public function get data():Number
		{
			return _data;
		}
	}
}

