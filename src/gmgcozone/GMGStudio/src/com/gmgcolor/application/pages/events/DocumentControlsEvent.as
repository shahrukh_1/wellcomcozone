package com.gmgcolor.application.pages.events
{
import flash.events.Event;

public class DocumentControlsEvent extends Event
	{
		public static const ROTATE_BUTTON_CLICK:String = "rotateButtonClick";
		public static const SINGLE_PAGE_BUTTON_CLICK:String = "singlePageButtonClick";
		public static const PIXEL_DIFFERENCE_BUTTON_CLICK:String = "pixelCompareButtonClick";
		public static const COMPARE_BUTTON_CLICK:String = "compareButtonClick";
		public static const DONE_BUTTON_CLICK:String = "doneButtonClick";
		public static const SYNC_BUTTON_CLICK:String = "syncButtonClick";
		public static const LOCK_BUTTON_CLICK:String = "lockButtonClick";
		public static const FIT_TO_CONTENT_BUTTON_CLICK:String = "fitToContentButtonClick";
		public static const ONE_TO_ONE_BUTTON_CLICK:String = "oneToOneButtonClick";
		public static const FIT_TO_WIDTH_BUTTON_CLICK:String = "fitToWidthButtonClick";
		public static const FIT_TO_HEIGHT_BUTTON_CLICK:String = "fitToHeightButtonClick";
		public static const ZOOM_SLIDER_CHANGE:String = "zoomSliderChange";
		public static const ZOOM:String = "zoom";
		public static const SCALE:String = "scale";
		public static const CENTRESCALE:String = "centreScale";
		public static const HORIZONTAL_TILE_BUTTON_CLICK:String = "horizontalTileButtonClick";
		public static const VERTICAL_TILE_BUTTON_CLICK:String = "verticalTileButtonClick";
		public static const NAVIGATOR_BUTTON_CLICK:String = "navigatorButtonClick";

        public static const ANNOTATION_MARKER_CLICK:String = "annotationMarkerClick";

        public var value:*;

		public function DocumentControlsEvent(type:String, value:* = null)
		{
			super(type,false,false);
			this.value = value;
		}

		public override function clone():Event
		{
			return new DocumentControlsEvent(type, value);
		}

		public override function toString():String
		{
			return formatToString("DocumentControlsEvent");
		}
	}
}
