package com.gmgcolor.application.pages.presentation
{
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.domain.ApprovalPage;

import flash.events.Event;
import flash.events.MouseEvent;

public class DocumentPagerContainerPM
	{
		[Inject][Bindable]
		public var approvalModel:ApprovalModel;
		
		[Bindable]
		public var pagesLength:int;
		
		[Bindable]
		public var currentPage:int;
		
		[Bindable]
		public var leftArrowEnabled:Boolean;
		
		[Bindable]
		public var rightArrowEnabled:Boolean;
		
		public function DocumentPagerContainerPM()
		{
		}
		
		[Init]
		public function init():void
		{
			approvalModel.addEventListener(ApprovalModel.CURRENT_PAGE_INDEX_CHANGE_EVENT, updatePage);
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_PAGE_CHANGE_EVENT, updatePage);
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_PAGES_CHANGE_EVENT, updatePage);
			updatePage();
		}

		private function updatePage(event:Event = null):void
		{
			currentPage = approvalModel.currentPageIndex;
			
			if(approvalModel.currentApprovalPages)
			{
				pagesLength = approvalModel.currentApprovalPages.length;
				leftArrowEnabled = true;
				rightArrowEnabled = true;
				if(approvalModel.currentPageIndex==0)
					leftArrowEnabled = false;
				if(approvalModel.currentPageIndex==pagesLength-1)
					rightArrowEnabled = false;
			}
		}

		public function onPreviousBtnClick(event:MouseEvent):void
		{
			approvalModel.currentPageIndex--;
				approvalModel.gotoPage(ApprovalPage(approvalModel.currentApprovalPages.getItemAt(approvalModel.currentPageIndex)));
		}
		
		public function onNextPageBtnClick(event:MouseEvent):void
		{
			approvalModel.currentPageIndex++;
				approvalModel.gotoPage(approvalModel.currentPage = ApprovalPage(approvalModel.currentApprovalPages.getItemAt(approvalModel.currentPageIndex)));
		}
	}
}
