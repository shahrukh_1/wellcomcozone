package com.gmgcolor.application.pages.model
{
import flash.events.Event;
import flash.events.EventDispatcher;

[Event(name="isCompareModeChange",type="flash.events.Event")]
	[Event(name="isSyncModeChange",type="flash.events.Event")]
	public class ImageDocumentControlsModel extends EventDispatcher
	{
		public static const ISCOMPAREMODE_CHANGE_EVENT:String = "isCompareModeChange";
		public static const ISSYNCMODE_CHANGE_EVENT:String = "isSyncModeChange";
		private var _isCompareMode:Boolean;
		private var _isSyncMode:Boolean;
		
		public function ImageDocumentControlsModel()
		{
			
		}

		[Bindable(event="isCompareModeChange")]
		public function get isCompareMode():Boolean
		{
			return _isCompareMode;
		}

		public function set isCompareMode(value:Boolean):void
		{
			if (_isCompareMode != value)
			{
				_isCompareMode = value;
				dispatchEvent(new Event(ISCOMPAREMODE_CHANGE_EVENT));
			}
		}

		[Bindable(event="isSyncModeChange")]
		public function get isSyncMode():Boolean
		{
			return _isSyncMode;
		}

		public function set isSyncMode(value:Boolean):void
		{
			if (_isSyncMode != value)
			{
				_isSyncMode = value;
				dispatchEvent(new Event(ISSYNCMODE_CHANGE_EVENT));
			}
		}

	}
}
