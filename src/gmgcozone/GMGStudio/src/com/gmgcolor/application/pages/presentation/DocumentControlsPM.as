package com.gmgcolor.application.pages.presentation {
import com.gmgcolor.annotations.domain.AccountUserInfo;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;
import com.gmgcolor.application.pages.events.VideoControlsEvent;
import com.gmgcolor.components.CustomIconToggleButton;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import org.spicefactory.parsley.core.context.Context;

public class DocumentControlsPM {

        public static const PAGE_THUMBNAIL_PADDING:Number = 10;
        public static const PAGE_THUMBNAIL_FOOTER:Number = 40;

        [Bindable]
        public var sepsSelected:Boolean;

        [Bindable]
        public var lockSelected:Boolean;

        private var _currentState:String = "normal";

        [Inject]
        [Bindable]
        public var approvalModel:ApprovalModel;

        [MessageDispatcher]
        public var dispatcher:Function;

        [Inject]
        public var context:Context;

        //private var _view:SingleDocumentControls;

        /*	private var btnNavigate:ToggleButton;

         private var _closeIt:Boolean;*/

        /*private var _navigatorOpen:Boolean;*/

        private var _accountUserInfo:AccountUserInfo;
        public var view:PageControls;

        public static function getThumbnailHeightMetrics():Number
        {
            return  (DocumentControlsPM.PAGE_THUMBNAIL_PADDING * 2) - PAGE_THUMBNAIL_FOOTER;
        }

        public function DocumentControlsPM()
        {

        }

        public function registerView(value:PageControls):void
        {
            if(! view)
            {
                view = value;
                approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_CHANGE_EVENT, onCurrentApprovalChange);
            }
        }

        private function onCurrentApprovalChange(event:Event):void
        {
            event.stopPropagation();
            if(view.currentState != 'compare')
            {
                view.setCurrentState(approvalModel.currentApproval.ApprovalType);
            }
        }

        [Subscribe]
        [Bindable]
        public function get accountUserInfo():AccountUserInfo
        {
            return _accountUserInfo;
        }

        public function set accountUserInfo(accountUserInfo:AccountUserInfo):void
        {
            _accountUserInfo = accountUserInfo;
        }


        public function onLockModeBtnClick(event:MouseEvent):void
        {
            lockDocuments(CustomIconToggleButton(event.target).selected);
        }

        protected function lockDocuments(value:Boolean):void
        {
            lockSelected = value;
            dispatcher(new DocumentControlsEvent(DocumentControlsEvent.LOCK_BUTTON_CLICK, value));
        }

        public var lockTimer:Timer;

        public function onLockTimer(e:TimerEvent):void
        {
            lockTimer.stop();
            lockTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onLockTimer);
            lockTimer = null;
            lockDocuments(true);
        }

        [Bindable]
        public function get currentState():String {
            return _currentState;
        }

        public function set currentState(value:String):void {
            _currentState = value;
        }

        [MessageHandler(selector="hideControls")]
        public function onHideControls(event:VideoControlsEvent):void
        {
            view.setCurrentState('normal');
        }

        [MessageHandler(selector="displayRecordControls")]
        public function onDisplayRecordControls(event:VideoControlsEvent):void
        {
            view.setCurrentState('record');
        }
    }
}
