package com.gmgcolor.application.pages.events {
import com.gmgcolor.annotations.presentation.CustomVideoPlayer;

import flash.events.Event;

public class VideoControlsEvent extends Event
    {
        public static const VIDEO_PLAY:String = 'videoPlay';
        public static const VIDEO_PAUSE:String = 'videoPause';
        public static const VIDEO_GOTOSTART:String = 'videoGoToStart';
        public static const VIDEO_GOTOEND:String = 'videoGoToEnd';
        public static const VIDEO_GOBACK:String = 'videoGoBack';
        public static const VIDEO_GOFORWARD:String = 'videoGoForward';

        public static const VIDEO_DISABLE_PLAY:String = 'videoDisablePlay';
        public static const VIDEO_ENABLE_PLAY:String = 'videoEnablePlay';
        public static const VIDEO_PLAYING_CHANGE:String = 'videoPlayingChange';

        public static const VIDEO_TIME_CHANGE:String = 'videoTimeChange';
        public static const HIDE_CONTROLS:String = 'hideControls';
        public static const DISPLAY_RECORD_CONTROLS:String = 'displayRecordControls';

        private var _videoPlayer:CustomVideoPlayer;

        public function VideoControlsEvent(type:String, videoPlayer:* = null)
        {
            super(type);
            if(videoPlayer)
            {
                this.videoPlayer = videoPlayer;
            }
        }

        public function get videoPlayer():CustomVideoPlayer
        {
            return _videoPlayer;
        }

        public function set videoPlayer(value:CustomVideoPlayer):void
        {
            _videoPlayer = value;
        }

        public override function clone():Event
        {
            return new VideoControlsEvent(type, videoPlayer);
        }

        public override function toString():String
        {
            return formatToString("VideoControlsEvent");
        }
    }
}
