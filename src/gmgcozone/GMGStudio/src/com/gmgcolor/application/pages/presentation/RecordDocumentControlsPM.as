package com.gmgcolor.application.pages.presentation {

import com.gmgcolor.application.pages.events.SWFPlayerEvent;

import flash.events.Event;

public class RecordDocumentControlsPM {

        [Inject]
        public var documentControlsPM:DocumentControlsPM;

        [Bindable]
        public var recording:Boolean = false;

        public function RecordDocumentControlsPM()
		{

		}

        public function onStartStop(event:Event):void
        {
            if(!recording)
            {
                documentControlsPM.dispatcher(new SWFPlayerEvent(SWFPlayerEvent.SWF_RECORD_START));
                recording = !recording;
            }
            else
            {
                documentControlsPM.dispatcher(new SWFPlayerEvent(SWFPlayerEvent.SWF_RECORD_STOP));
                event.target.enabled = false;
            }
        }

        public function registerView(value:PageControls):void
        {
            documentControlsPM.registerView(value);
        }
    }
}
