package com.gmgcolor.application.pages.events {
import flash.events.Event;

public class SWFPlayerEvent extends Event
    {
        public static const SWF_SEEK:String = 'swfSeek';
        public static const SWF_FRAME_CHANGE:String = 'swfFrameChange';
        public static const SWF_RECORD_START:String = 'swfRecordStart';
        public static const SWF_RECORD_STOP:String = 'swfRecordStop';
        private var _value:*;

        public function SWFPlayerEvent(type:String, value:* = null)
        {
            super(type);
            if(value)
            {
                this.value = value;
            }
        }

        public override function clone():Event
        {
            return new VideoControlsEvent(type, _value);
        }

        public override function toString():String
        {
            return formatToString("VideoControlsEvent");
        }

        public function get value():* {
            return _value;
        }

        public function set value(value:*):void {
            _value = value;
        }
}
}
