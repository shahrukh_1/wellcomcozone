package com.gmgcolor.application.pages.events {
import com.gmgcolor.annotations.presentation.CustomVideoPlayer;

import flash.events.Event;

public class VideoTimeChangeEvent extends Event
{
    private var _videoPlayer:CustomVideoPlayer;
    private var _time:Number;
    public static const VIDEO_TIME_CHANGE:String = 'videoTimeChange';

    public function VideoTimeChangeEvent(type:String, time:Number, videoPlayer:*)
    {
        super(type);
        this.time = time;
        this.videoPlayer = videoPlayer;
    }

    public override function clone():Event
    {
        return new VideoControlsEvent(type, videoPlayer);
    }

    public override function toString():String
    {
        return formatToString("VideoTimeChangeEvent");
    }

    public function get videoPlayer():CustomVideoPlayer {
        return _videoPlayer;
    }

    public function set videoPlayer(value:CustomVideoPlayer):void {
        _videoPlayer = value;
    }

    public function get time():Number {
        return _time;
    }

    public function set time(value:Number):void {
        _time = value;
    }
}
}
