package com.gmgcolor.application.pages.presentation {
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.domain.ApprovalPage;
import com.gmgcolor.application.pages.model.PageSpread;

import flash.events.Event;

import mx.collections.ArrayCollection;

public class PagesContainerPM {
		public static const PAGE_THUMBNAIL_PADDING:Number = 10;
		public static const PAGE_THUMBNAIL_FOOTER:Number = 23;

		[MessageDispatcher]
		public var dispatcher:Function;

		[Bindable]
		public var pages:ArrayCollection;

		[Bindable]
		[Inject]
		public var approvalModel:ApprovalModel;

		[Init]
		public function init():void
		{
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_PAGES_CHANGE_EVENT, createPages);
			createPages();
		}

		public function onPagesClickHandler(page:ApprovalPage):void
		{
			approvalModel.gotoPage(page);
		}

		private function createPages(event:Event = null):void
		{
			var pageSpread:PageSpread;
			var array:Array = [];

			for each (var approvalPage:ApprovalPage in approvalModel.currentApprovalPages) {
				pageSpread = new PageSpread();
				pageSpread.leftPage = null;
				pageSpread.rightPage = approvalPage;
				array.push(pageSpread);
			}

			pages = new ArrayCollection(array);
		}
	}
}
