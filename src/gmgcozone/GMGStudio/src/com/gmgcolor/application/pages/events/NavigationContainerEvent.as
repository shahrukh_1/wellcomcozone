package com.gmgcolor.application.pages.events
{
import flash.events.Event;

public class NavigationContainerEvent extends Event
	{
		public static const PANEL_TOGGLE_BUTTON_SELECTED:String = "panelToggleButtonSelected";
		public static const PANEL_TOGGLE_BUTTON_DESELECTED:String = "panelToggleButtonDeselected";

		public function NavigationContainerEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new NavigationContainerEvent(type);
		}

		public override function toString():String
		{
			return formatToString("NavigationContainerEvent");
		}
	}
}
