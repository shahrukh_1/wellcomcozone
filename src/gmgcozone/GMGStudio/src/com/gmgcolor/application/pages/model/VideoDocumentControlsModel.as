package com.gmgcolor.application.pages.model
{
import flash.events.EventDispatcher;

public class VideoDocumentControlsModel extends EventDispatcher
	{
		private var _isPlaying:Boolean = false;
		private var _isPlayEnabled:Boolean = true;
        private var _currentTime:Number = 0;
        private var _totalTime:Number;

		public function VideoDocumentControlsModel()
		{
			
		}

		[Bindable]
		public function get isPlaying():Boolean
		{
			return _isPlaying;
		}

		public function set isPlaying(value:Boolean):void
		{
				_isPlaying = value;
                isPlayEnabled = !value;
		}

        [Bindable]
        public function get isPlayEnabled():Boolean
        {
            return _isPlayEnabled;
        }

        public function set isPlayEnabled(value:Boolean):void
        {
            _isPlayEnabled = value;
        }

        [Bindable]
        public function get currentTime():Number
        {
            return _currentTime;
        }

        public function set currentTime(value:Number):void
        {
            _currentTime = value;
        }

        [Bindable]
        public function get totalTime():Number
        {
            return _totalTime;
        }

        public function set totalTime(value:Number):void
        {
            _totalTime = value;
        }
}
}
