package com.gmgcolor.application.event {
import mx.rpc.Fault;

public class GetUsePrePressFunctionalityTask extends ConcurrentTaskGroup {

		[MessageDispatcher]
		public var dispatcher:Function;

		public function GetUsePrePressFunctionalityTask()
		{
			super();
			setName("GetUsePrePressFunctionalityTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}

		override protected function doStart():void
		{
			dispatcher(new GetUsePrePressFunctionalityEvent(GetUsePrePressFunctionalityEvent.GET_USE_PRE_PRESS_FUNCTIONALITY_EVENT));
		}

		[CommandComplete]
		public function onComplete(message:GetSessionEvent):void
		{
			complete();
		}

		[CommandError]
		public function onError(fault:Fault, message:GetSessionEvent):void
		{
			complete();
		}

		override protected function doTimeout():void
		{
			skip();
		}
	}
}


