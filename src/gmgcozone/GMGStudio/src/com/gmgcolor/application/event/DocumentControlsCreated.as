package com.gmgcolor.application.event
{
import flash.events.Event;

public class DocumentControlsCreated extends Event
	{
		public static const CREATED:String = "created";

		public function DocumentControlsCreated(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new DocumentControlsCreated(type);
		}

		public override function toString():String
		{
			return formatToString("DocumentControlsCreated");
		}
	}
}
