package com.gmgcolor.application.event
{
import flash.events.Event;

public class AnnotationToolSelect extends Event
	{
		
		public static const ANNOTATE_BUTTON_SELECT:String = "annotateButtonSelect";
		public static const DRAW_BUTTON_SELECT:String= "drawButtonSelect";
		public static const MARKER_BUTTON_SELECT:String= "markerButtonSelect";
		public static const EYE_DROPPER_BUTTON_SELECT:String = "eyeDropperButtonSelect";
		public static const TEXT_BUTTON_SELECT:String= "textButtonSelect";
		
		
		public function AnnotationToolSelect(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new AnnotationToolSelect(type);
		}

		public override function toString():String
		{
			return formatToString("AnnotationToolSelect");
		}
	}
}
