package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetPreDefinedShapesEvent extends Event
	{
		public static const GET_PREDEFINED_SHAPES:String = "getPreDefinedShapesEvent";
		
		private var _sessionId:String;
		
		public function GetPreDefinedShapesEvent(type:String, sessionId:String)
		{
			super(type,false,false);
			
			this._sessionId = sessionId;
		}
		
		public function get sessionId():String
		{
			return _sessionId;
		}
		
		public override function clone():Event
		{
			return new GetPreDefinedShapesEvent(type,sessionId);
		}
		
		public override function toString():String
		{
			return formatToString("GetPreDefinedShapesEvent","sessionId");
		}
	}
}
