package com.gmgcolor.application.event
{
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.core.Application;
import mx.rpc.Fault;

public class LoginTask extends ConcurrentTaskGroup {
		
		[MessageDispatcher] public var dispatcher:Function;
		
		[Inject]
		public var securityManager:SecurityManager;
		
		public function LoginTask() 
		{
			super();
			setName("LoginTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}
		
		override protected function doStart():void
		{
			start();
			dispatcher(new LoginEvent(LoginEvent.LOGIN,securityManager.sessionId,Application.application.parameters.LoggedUser));
		}
		
		[Init]
		public function init():void
		{

		}
		
		[CommandComplete]
		public function onComplete(message:LoginEvent):void 
		{
			complete();
		}
		
		[CommandError]
		public function onError(fault:Fault, message:LoginEvent):void 
		{
			complete();
		}
		
		override protected function doTimeout():void
		{
			skip();
		}
	}
}


