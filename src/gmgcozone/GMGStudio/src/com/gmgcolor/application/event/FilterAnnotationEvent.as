package com.gmgcolor.application.event
{
import flash.events.Event;

public class FilterAnnotationEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _index:int;

		public function FilterAnnotationEvent(type:String, index:int)
		{
			super(type,false,false);

			this._index = index;
		}

		public function get index():int
		{
			return _index;
		}

		public override function clone():Event
		{
			return new FilterAnnotationEvent(type,index);
		}

		public override function toString():String
		{
			return formatToString("FilterAnnotationEvent","index");
		}
	}
}
