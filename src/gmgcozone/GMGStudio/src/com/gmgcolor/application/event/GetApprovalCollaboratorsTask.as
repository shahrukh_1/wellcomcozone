package com.gmgcolor.application.event
{
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetApprovalCollaboratorsTask extends ConcurrentTaskGroup {
		
		[MessageDispatcher] public var dispatcher:Function;
		
		[Inject]
		public var applicationModel:ApplicationModel;
		
		[Inject]
		public var securityManager:SecurityManager;
		
		public function GetApprovalCollaboratorsTask() 
		{
			super();
			setName("GetApprovalCollaboratorsTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}
		
		override protected function doStart():void
		{
			start();
			dispatcher(new GetApprovalCollaboratorsEvent(GetApprovalCollaboratorsEvent.GET_APPROVAL_COLLABORATORS,securityManager.sessionId, applicationModel.currentApprovalId));
		}
		
		[Init]
		public function init():void
		{
			
		}
		
		[CommandComplete]
		public function onComplete(message:GetApprovalCollaboratorsEvent):void 
		{
			complete();
		}
		
		[CommandError]
		public function onError(fault:Fault, message:GetApprovalCollaboratorsEvent):void 
		{
			complete();
		}
		
		override protected function doTimeout():void
		{
			skip();
		}
	}
}


