package com.gmgcolor.application.event
{
import flash.events.Event;

public class NavigationEvent extends Event
	{
		public static const ANNOTATE_BUTTON_CLICK:String = "annotateButtonClick";

		public function NavigationEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new NavigationEvent(type);
		}

		public override function toString():String
		{
			return formatToString("NavigationEvent");
		}
	}
}
