package com.gmgcolor.application.event
{
import flash.events.Event;

public class HandChangeEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _handModeOn:Boolean;

		public function HandChangeEvent(type:String, handModeOn:Boolean)
		{
			super(type,false,false);

			this._handModeOn = handModeOn;
		}

		public function get handModeOn():Boolean
		{
			return _handModeOn;
		}

		public override function clone():Event
		{
			return new HandChangeEvent(type,handModeOn);
		}

		public override function toString():String
		{
			return formatToString("HandChangeEvent","handModeOn");
		}
	}
}
