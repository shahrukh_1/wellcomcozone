package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetApprovalCollaboratorRoleEvent extends Event
	{
		public static const GetApprovalCollaboratorRole:String = "getapprovalcollaboratorroletask";

		private var _sessionId:String;

		public function GetApprovalCollaboratorRoleEvent(type:String, sessionId:String)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetApprovalCollaboratorRoleEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetApprovalCollaboratorRoleEvent","sessionId");
		}
	}
}
