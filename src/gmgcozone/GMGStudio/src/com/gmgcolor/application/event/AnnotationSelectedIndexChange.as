package com.gmgcolor.application.event
{
import flash.events.Event;

public class AnnotationSelectedIndexChange extends Event
	{
		public static const CHANGE:String = "change";

		private var _selectedIndex:int;

		public function AnnotationSelectedIndexChange(type:String, selectedIndex:int)
		{
			super(type,false,false);

			this._selectedIndex = selectedIndex;
		}

		public function get selectedIndex():int
		{
			return _selectedIndex;
		}

		public override function clone():Event
		{
			return new AnnotationSelectedIndexChange(type,selectedIndex);
		}

		public override function toString():String
		{
			return formatToString("AnnotationSelectedIndexChange","selectedIndex");
		}
	}
}
