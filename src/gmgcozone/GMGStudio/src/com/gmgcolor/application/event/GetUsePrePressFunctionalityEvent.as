package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetUsePrePressFunctionalityEvent extends Event
	{
		public static const GET_USE_PRE_PRESS_FUNCTIONALITY_EVENT:String = "getUsePrePressFunctionalityEvent";

		public function GetUsePrePressFunctionalityEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new GetUsePrePressFunctionalityEvent(type);
		}

		public override function toString():String
		{
			return formatToString("getUsePrePressFunctionalityEvent");
		}
	}
}
