package com.gmgcolor.application.event {
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetDecisionsTask extends Task {

		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var securityManager:SecurityManager;

		public function GetDecisionsTask()
		{
			super();
			setName("GetDecisionsTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);

		}

		override protected function doStart():void
		{
			dispatcher(new GetDecisionsEvent(GetDecisionsEvent.getDecisionsEvent, securityManager.sessionId));
		}

		[CommandComplete]
		public function onComplete(message:GetDecisionsEvent):void
		{
			complete();
		}

		[CommandError]
		public function onError(fault:Fault, message:GetDecisionsEvent):void
		{
			complete();
		}

		override protected function doTimeout():void
		{
			skip();
		}
	}
}


