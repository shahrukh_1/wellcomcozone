package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetSessionEvent extends Event
	{
		public static const GET_SESSIONS_EVENT:String = "getSessionsEvent";

		public function GetSessionEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new GetSessionEvent(type);
		}

		public override function toString():String
		{
			return formatToString("GetSessionEvent");
		}
	}
}
