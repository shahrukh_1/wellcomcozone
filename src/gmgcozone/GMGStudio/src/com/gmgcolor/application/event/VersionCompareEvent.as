package com.gmgcolor.application.event
{
import flash.events.Event;

public class VersionCompareEvent extends Event
	{
		public static const COMPARE:String = "compare";
		
		private var _versions:Array

		public function VersionCompareEvent(type:String,versions:Array)
		{
			super(type,false,false);
			_versions = versions;
		}

		public function get versions():Array
		{
			return _versions;
		}

		public override function clone():Event
		{
			return new VersionCompareEvent(type,versions);
		}

		public override function toString():String
		{
			return formatToString("VersionCompareEvent");
		}
	}
}
