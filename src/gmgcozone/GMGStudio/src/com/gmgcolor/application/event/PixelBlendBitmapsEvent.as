package com.gmgcolor.application.event
{
import flash.events.Event;

public class PixelBlendBitmapsEvent extends Event
	{
		public static const RENDER_BITMAPS:String = "render";
		public static const BLEND:String = "blend";
		public static const COLOR:String = "color";
		public static const SENSITIVITY:String = "sensitivity";

		private var _value:Array;

		public function PixelBlendBitmapsEvent(type:String, value:Array)
		{
			super(type,false,false);
			this._value = value;
		}

		public function get value():Array
		{
			return _value;
		}

		public override function clone():Event
		{
			return new PixelBlendBitmapsEvent(type,value);
		}

		public override function toString():String
		{
			return formatToString("PixelBlendBitmapsEvent","value");
		}
	}
}
