package com.gmgcolor.application.event
{
import flash.events.Event;

public class UpdateApprovalDecisionEvent extends Event
	{
		public static const UpdateApprovalDecision:String = "updateapprovaldecision";

		private var _sessionId:String;
		private var _approvalId:int;
		private var _decisionId:int;
		private var _collaboratorId:int;

		public function UpdateApprovalDecisionEvent(type:String, sessionId:String, approvalId:int, decisionId:int, collaboratorId:int)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._approvalId = approvalId;
			this._decisionId = decisionId;
			this._collaboratorId = collaboratorId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get approvalId():int
		{
			return _approvalId;
		}

		public function get decisionId():int
		{
			return _decisionId;
		}

		public function get collaboratorId():int
		{
			return _collaboratorId;
		}
		
		public function set collaboratorId(value:int):void
		{
			this._collaboratorId = value;
		}


		public override function clone():Event
		{
			return new UpdateApprovalDecisionEvent(type,sessionId,approvalId,decisionId,collaboratorId);
		}

		public override function toString():String
		{
			return formatToString("UpdateApprovalDecisionEvent","sessionId","approvalId","decisionId","collaboratorId");
		}
	}
}
