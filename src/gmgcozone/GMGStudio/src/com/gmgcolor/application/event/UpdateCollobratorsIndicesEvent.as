package com.gmgcolor.application.event
{
import flash.events.Event;

public class UpdateCollobratorsIndicesEvent extends Event
	{
		public static const UPDATE:String = "update";

		public function UpdateCollobratorsIndicesEvent(type:String)
		{
			super(type,false,false);
		}

		public override function clone():Event
		{
			return new UpdateCollobratorsIndicesEvent(type);
		}

		public override function toString():String
		{
			return formatToString("UpdateCollobratorsIndicesEvent");
		}
	}
}
