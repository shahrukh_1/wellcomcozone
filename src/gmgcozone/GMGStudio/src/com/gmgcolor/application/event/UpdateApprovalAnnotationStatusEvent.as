package com.gmgcolor.application.event
{
import flash.events.Event;

public class UpdateApprovalAnnotationStatusEvent extends Event
	{
		public static const UpdateApprovalAnnotationStatus:String = "updateapprovalannotationstatus";

		private var _sessionId:String;
		private var _approvalId:int;
		private var _statusId:int;

		public function UpdateApprovalAnnotationStatusEvent(type:String, sessionId:String, approvalId:int, statusId:int)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._approvalId = approvalId;
			this._statusId = statusId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get approvalId():int
		{
			return _approvalId;
		}

		public function get statusId():int
		{
			return _statusId;
		}

		public override function clone():Event
		{
			return new UpdateApprovalAnnotationStatusEvent(type,sessionId,approvalId,statusId);
		}

		public override function toString():String
		{
			return formatToString("UpdateApprovalAnnotationStatusEvent","sessionId","approvalId","statusId");
		}
	}
}
