package com.gmgcolor.application.event {
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetPreDefinedShapesTask extends Task {

		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var securityManager:SecurityManager;

		public function GetPreDefinedShapesTask()
		{
			super();
			setName("GetPreDefinedShapesTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}

		override protected function doStart():void
		{
			dispatcher(new GetPreDefinedShapesEvent(GetPreDefinedShapesEvent.GET_PREDEFINED_SHAPES, securityManager.sessionId));
		}

		[CommandComplete]
		public function onComplete(message:GetPreDefinedShapesEvent):void
		{
			complete();
		}

		[CommandError]
		public function onError(fault:Fault, message:GetPreDefinedShapesEvent):void
		{
			complete();
		}

		override protected function doTimeout():void
		{
			skip();
		}
	}
}


