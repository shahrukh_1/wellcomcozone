package com.gmgcolor.application.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class UpdateApprovalAnnotationEvent extends Event
	{
		public static const updateApprovalAnnoation:String = "updateApprovalAnnoation";

		private var _sessionId:String;
		private var _annotation:ApprovalAnnotationFO;

		public function UpdateApprovalAnnotationEvent(type:String, sessionId:String, annotation:ApprovalAnnotationFO)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._annotation = annotation;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public override function clone():Event
		{
			return new UpdateApprovalAnnotationEvent(type,sessionId,annotation);
		}

		public override function toString():String
		{
			return formatToString("UpdateApprovalAnnotationEvent","sessionId","annotation");
		}
	}
}
