package com.gmgcolor.application.event
{
import flash.events.Event;

public class AnnotationToolsEvent extends Event
	{
		public static const ANNOTATE_BUTTON_CLICK:String = "annotateButtonClick";
		public static const DRAW_BUTTON_CLICK:String= "drawButtonClick";
		public static const MARKER_BUTTON_CLICK:String= "markerButtonClick";
		public static const EYE_DROPPER_BUTTON_CLICK:String = "eyeDropperButtonClick";
		public static const TEXT_BUTTON_CLICK:String= "textButtonClick";
		public static const SHAPE_CLICK:String= "shapeClick";
		public static const RESET:String= "reset";
		public static const DESELECT:String= "deselect";
		public static const UPDATE_SELECTION:String= "updateSelection";
		public static const DONE:String= "done";


		private var _data:Object;


		public function AnnotationToolsEvent(type:String, data:Object = null)
		{
			super(type,false,false);
			this._data = data;
		}

		public function isToolBtnClick():Boolean
		{
			return type == DRAW_BUTTON_CLICK || type == MARKER_BUTTON_CLICK || type == TEXT_BUTTON_CLICK || type == EYE_DROPPER_BUTTON_CLICK;
		}
		
		public override function clone():Event
		{
			return new AnnotationToolsEvent(type, data);
		}

		public override function toString():String
		{
			return formatToString("AnnotationToolsEvent");
		}


		public function get data():Object
		{
			return _data;
		}
	}
}
