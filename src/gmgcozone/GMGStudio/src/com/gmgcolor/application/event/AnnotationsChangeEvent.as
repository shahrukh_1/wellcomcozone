package com.gmgcolor.application.event
{
import flash.events.Event;

import mx.collections.ArrayCollection;

public class AnnotationsChangeEvent extends Event
	{
		public static const CHANGE:String = "change";
		public static const START:String = "start";
		
		private var _annotations:ArrayCollection;
		
		public function AnnotationsChangeEvent(type:String,annotations:ArrayCollection)
		{
			this._annotations = annotations;
			super(type,false,false);
		}
		
		public function get annotations():ArrayCollection
		{
			return _annotations;
		}
		
		public override function clone():Event
		{
			return new AnnotationsChangeEvent(type,annotations);
		}
		
		public override function toString():String
		{
			return formatToString("AnnotationsChangeEvent", "annotations");
		}
	}
}
