package com.gmgcolor.application.event
{
public class ShowRightPanelContainerEvent
	{
		private var _name:String;
		private var _show:Boolean;

		public function ShowRightPanelContainerEvent(name:String, show:Boolean)
		{
			this._name = name;
			this._show = show;
		}

		public function get name():String
		{
			return _name;
		}

		public function get show():Boolean
		{
			return _show;
		}

	}
}
