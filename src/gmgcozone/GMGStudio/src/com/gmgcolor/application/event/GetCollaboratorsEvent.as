package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetCollaboratorsEvent extends Event
	{
		public static const getCollaborators:String = "getcollaborators";

		private var _sessionId:Object;

		public function GetCollaboratorsEvent(type:String, sessionId:Object)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():Object
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetCollaboratorsEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetCollaboratorsEvent","sessionId");
		}
	}
}
