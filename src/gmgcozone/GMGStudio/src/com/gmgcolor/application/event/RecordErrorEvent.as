package com.gmgcolor.application.event
{
import com.gmgcolor.annotations.domain.ErrorFO;

import flash.events.Event;

public class RecordErrorEvent extends Event
	{
		public static const RECORD:String = "record";

		private var _errorFO:ErrorFO;

		public function RecordErrorEvent(type:String, errorFO:ErrorFO)
		{
			super(type,false,false);

			this._errorFO = errorFO;
		}

		public function get errorFO():ErrorFO
		{
			return _errorFO;
		}

		public override function clone():Event
		{
			return new RecordErrorEvent(type,errorFO);
		}

		public override function toString():String
		{
			return formatToString("RecordErrorEvent","errorFO");
		}
	}
}
