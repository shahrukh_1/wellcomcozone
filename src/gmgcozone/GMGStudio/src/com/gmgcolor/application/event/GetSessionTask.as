package com.gmgcolor.application.event {
import mx.rpc.Fault;

public class GetSessionTask extends Task {

		[MessageDispatcher]
		public var dispatcher:Function;

		public function GetSessionTask()
		{
			super();
			setName("GetSessionTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}

		override protected function doStart():void
		{
			dispatcher(new GetSessionEvent(GetSessionEvent.GET_SESSIONS_EVENT));
		}

		[CommandComplete]
		public function onComplete(message:GetSessionEvent):void
		{
			complete();
		}

		[CommandError]
		public function onError(fault:Fault, message:GetSessionEvent):void
		{
			complete();
		}

		override protected function doTimeout():void
		{
			skip();
		}
	}
}


