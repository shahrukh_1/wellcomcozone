package com.gmgcolor.application.event
{
import com.gmgcolor.annotations.domain.Approval;

import flash.events.Event;

public class UpdateApproval extends Event
	{
		public static const UpdateApproval:String = "updateapproval";

		private var _sessionId:String;
		private var _approval:Approval;

		public function UpdateApproval(type:String, sessionId:String, approval:Approval)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._approval = approval;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get approval():Approval
		{
			return _approval;
		}

		public override function clone():Event
		{
			return new UpdateApproval(type,sessionId,approval);
		}

		public override function toString():String
		{
			return formatToString("UpdateApproval","sessionId","approval");
		}
	}
}
