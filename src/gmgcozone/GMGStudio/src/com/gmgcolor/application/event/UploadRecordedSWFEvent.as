package com.gmgcolor.application.event
{
import flash.events.Event;
import flash.utils.ByteArray;

public class UploadRecordedSWFEvent extends Event
	{
		public static const UPLOAD:String = "uploadRecordedSWF";

		private var _sessionId:String;
		private var _approvalId:int;
		private var _data:ByteArray;

		public function UploadRecordedSWFEvent(type:String, sessionId:String, approvalId:int, data:ByteArray)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._approvalId = approvalId;
			this._data = data;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get approvalId():int
		{
			return _approvalId;
		}

		public override function clone():Event
		{
			return new UploadRecordedSWFEvent(type,sessionId,approvalId, data);
		}

		public override function toString():String
		{
			return formatToString("UploadRecordedSWFEvent","sessionId","approvalId","data");
		}


		public function get data():ByteArray
		{
			return _data;
		}

		public function set data(value:ByteArray):void
		{
			this._data = value;
		}
	}
}
