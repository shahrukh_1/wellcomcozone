package com.gmgcolor.application.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class AddApprovalEvent extends Event
	{
		public static const AddApproval:String = "addapproval";

		private var _sessionId:String;
		private var _annotation:ApprovalAnnotationFO;

		public function AddApprovalEvent(type:String, sessionId:String, annotation:ApprovalAnnotationFO)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._annotation = annotation;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public override function clone():Event
		{
			return new AddApprovalEvent(type,sessionId,annotation);
		}

		public override function toString():String
		{
			return formatToString("AddApprovalEvent","sessionId","annotation");
		}
	}
}
