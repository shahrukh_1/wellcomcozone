package com.gmgcolor.application.event
{
public class GetAnnotationTypesEvent
	{


		private var _sessionId:String;

		public function GetAnnotationTypesEvent( sessionId:String)
		{
	
			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

	
	}
}
