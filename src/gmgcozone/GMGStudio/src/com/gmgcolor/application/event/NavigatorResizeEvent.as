package com.gmgcolor.application.event {
import flash.events.Event;

public class NavigatorResizeEvent extends Event {
		public static const RESIZE:String = "resize";

		public function NavigatorResizeEvent(type:String)
		{
			super(type, false, false);
		}

		public override function clone():Event
		{
			return new NavigatorResizeEvent(type);
		}

		public override function toString():String
		{
			return formatToString("NavigatorResizeEvent");
		}
	}
}
