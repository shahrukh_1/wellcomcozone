package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetCollaboratorGroupsEvent extends Event
	{
		public static const GetCollaborator:String = "getcollaborator";

		private var _sessionId:String;

		public function GetCollaboratorGroupsEvent(type:String, sessionId:String)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetCollaboratorGroupsEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetCollaboratorGroupsEvent","sessionId");
		}
	}
}
