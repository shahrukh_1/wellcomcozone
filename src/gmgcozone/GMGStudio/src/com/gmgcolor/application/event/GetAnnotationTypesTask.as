package com.gmgcolor.application.event
{
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetAnnotationTypesTask extends ConcurrentTaskGroup {
		
		[MessageDispatcher] public var dispatcher:Function;
		
		[Inject]
		public var securityManager:SecurityManager;
		
		public function GetAnnotationTypesTask() 
		{
			super();
			setName("GetAnnotationTypesTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}
		
		override protected function doStart():void
		{
			start();
			dispatcher(new GetAnnotationTypesEvent(securityManager.sessionId));
		}
		
		[Init]
		public function init():void
		{

		}
		
		[CommandComplete]
		public function onComplete(message:GetAnnotationTypesEvent):void 
		{
			complete();
		}
		
		[CommandError]
		public function onError(fault:Fault, message:GetAnnotationTypesEvent):void 
		{
			complete();
		}
		
		override protected function doTimeout():void
		{
			skip();
		}
	}
}


