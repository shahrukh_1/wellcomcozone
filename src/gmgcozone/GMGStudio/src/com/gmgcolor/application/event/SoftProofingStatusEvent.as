package com.gmgcolor.application.event
{
import com.gmgcolor.application.data.domain.SoftProofingStatus;

import flash.events.Event;

public class SoftProofingStatusEvent extends Event
	{
		public static const STATUS:String = "softProofingStatus";

        private var _status:SoftProofingStatus;

		public function SoftProofingStatusEvent(eventType:String, softProofingStatus:SoftProofingStatus)
		{
			super(eventType,false,false);
            status = softProofingStatus;
		}
		
		public override function clone():Event
		{
			return new SoftProofingStatusEvent(type,status);
		}
		
		public override function toString():String
		{
			return formatToString("SoftProofingStatusEvent");
		}

        public function get status():SoftProofingStatus {
            return _status;
        }

        public function set status(value:SoftProofingStatus):void {
            _status = value;
        }
}
}
