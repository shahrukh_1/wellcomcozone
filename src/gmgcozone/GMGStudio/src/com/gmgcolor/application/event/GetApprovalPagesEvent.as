package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetApprovalPagesEvent extends Event
	{
		public static const GET_APPROVAL_PAGES:String = "getApprovalPages";

		private var _sessionId:String;
		private var _approvalId:int;

		public function GetApprovalPagesEvent(type:String, sessionId:String, approvalId:int)
		{
			super(type,false,false);
			this._sessionId = sessionId;
			this._approvalId = approvalId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get approvalId():int
		{
			return _approvalId;
		}

		public override function clone():Event
		{
			return new GetApprovalPagesEvent(type,sessionId,approvalId);
		}

		public override function toString():String
		{
			return formatToString("GetApprovalPagesEvent","sessionId","approvalId");
		}
	}
}
