package com.gmgcolor.application.event
{
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetApprovalPagesTask extends ConcurrentTaskGroup {
		
		[MessageDispatcher] public var dispatcher:Function;
		
		[Inject]
		public var applicationModel:ApplicationModel;
		
		[Inject]
		public var securityManager:SecurityManager;
		
		public function GetApprovalPagesTask() 
		{
			super();
			setName("GetApprovalPagesTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
		}
		
		override protected function doStart():void
		{
			start();
			dispatcher(new GetApprovalPagesEvent(GetApprovalPagesEvent.GET_APPROVAL_PAGES,securityManager.sessionId, applicationModel.currentApprovalId));
		}
		
		[Init]
		public function init():void
		{
			
		}
		
		[CommandComplete]
		public function onComplete(message:GetApprovalPagesEvent):void 
		{
			complete();
		}
		
		[CommandError]
		public function onError(fault:Fault, message:GetApprovalPagesEvent):void 
		{
			complete();
		}
		
		override protected function doTimeout():void
		{
			skip();
		}
	}
}


