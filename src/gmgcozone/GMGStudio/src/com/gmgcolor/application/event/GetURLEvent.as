package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetURLEvent extends Event
	{
		public static const ADD_NEW_VERSION:String = "add";
		public static const DOWNLOAD:String = "download";
		public static const PRINT:String = "print";

		private var _sessionId:String;
		private var _approvalId:int;
		private var _versionId:int;

		public function GetURLEvent(type:String, sessionId:String, approvalId:int, versionId:int=0)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._approvalId = approvalId;
			this._versionId = versionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get approvalId():int
		{
			return _approvalId;
		}

		public override function clone():Event
		{
			return new GetURLEvent(type,sessionId,approvalId);
		}

		public override function toString():String
		{
			return formatToString("GetURLEvent","sessionId","approvalId","versionId");
		}


		public function get versionId():int
		{
			return _versionId;
		}

		public function set versionId(value:int):void
		{
			this._versionId = value;
		}

	}
}
