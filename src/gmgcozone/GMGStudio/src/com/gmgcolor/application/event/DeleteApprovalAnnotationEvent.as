package com.gmgcolor.application.event
{
import flash.events.Event;

public class DeleteApprovalAnnotationEvent extends Event
	{
		public static const deleteApprovalAnnotation:String = "deleteapprovalannotation";

		private var _sessionId:String;
		private var _annotationId:int;

		public function DeleteApprovalAnnotationEvent(type:String, sessionId:String, annotationId:int)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._annotationId = annotationId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public function get annotationId():int
		{
			return _annotationId;
		}

		public override function clone():Event
		{
			return new DeleteApprovalAnnotationEvent(type,sessionId,annotationId);
		}

		public override function toString():String
		{
			return formatToString("DeleteApprovalAnnotation","sessionId","annotationId");
		}
	}
}
