package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetApprovalCollaboratorsByCollaboratorEvent extends Event
	{
		public static const GetApprovalCollaboratorsByCollaborator:String = "getapprovalcollaboratorsbycollaborator";

		private var _sessionId:Object;
		private var _collaboratorId:int;

		public function GetApprovalCollaboratorsByCollaboratorEvent(type:String, sessionId:Object, collaboratorId:int)
		{
			super(type,false,false);

			this._sessionId = sessionId;
			this._collaboratorId = collaboratorId;
		}

		public function get sessionId():Object
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetApprovalCollaboratorsByCollaboratorEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetApprovalCollaboratorsByCollaboratorEvent","sessionId");
		}


		public function get collaboratorId():int
		{
			return _collaboratorId;
		}

		public function set collaboratorId(value:int):void
		{
			this._collaboratorId = value;
		}

	}
}
