package com.gmgcolor.application.event
{
import com.gmgcolor.application.pages.enum.EyeDropperTypeEnum;

import flash.events.Event;

public class EyeDropperToolChangeEvent extends Event
	{
		public static const CHANGE:String = "change";
		public static const ENABLE:String = "enable";
		public static const DISABLE:String = "disable";

		private var _data:EyeDropperTypeEnum;

		public function EyeDropperToolChangeEvent(type:String, data:EyeDropperTypeEnum = null)
		{
			super(type,false,false);

			this._data = data;
		}

		public function get data():EyeDropperTypeEnum
		{
			return _data;
		}

		public override function clone():Event
		{
			return new EyeDropperToolChangeEvent(type,data);
		}

		public override function toString():String
		{
			return formatToString("EyeDropperToolChangeEvent","data");
		}
	}
}
