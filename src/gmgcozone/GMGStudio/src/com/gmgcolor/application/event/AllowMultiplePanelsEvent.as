package com.gmgcolor.application.event
{
import flash.events.Event;

public class AllowMultiplePanelsEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _allow:Boolean;

		public function AllowMultiplePanelsEvent(type:String, allow:Boolean)
		{
			super(type,false,false);

			this._allow = allow;
		}

		public function get allow():Boolean
		{
			return _allow;
		}

		public override function clone():Event
		{
			return new AllowMultiplePanelsEvent(type,allow);
		}

		public override function toString():String
		{
			return formatToString("AllowMultiplePanelsEvent","allow");
		}
	}
}
