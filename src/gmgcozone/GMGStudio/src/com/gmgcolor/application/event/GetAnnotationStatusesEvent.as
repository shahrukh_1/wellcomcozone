package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetAnnotationStatusesEvent extends Event
	{
		public static const GetAnnotationStatuses:String = "getannotationstatuses";

		private var _sessionId:String;

		public function GetAnnotationStatusesEvent(type:String, sessionId:String)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetAnnotationStatusesEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetAnnotationStatusesEvent","sessionId");
		}
	}
}
