package com.gmgcolor.application.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class ApprovalAnnotationAddedEvent extends Event
	{
		public static const ADDED:String = "added";

		private var _approvalAnnotation:ApprovalAnnotationFO;

		public function ApprovalAnnotationAddedEvent(type:String, approvalAnnotation:ApprovalAnnotationFO)
		{
			super(type,false,false);

			this._approvalAnnotation = approvalAnnotation;
		}

		public function get approvalAnnotation():ApprovalAnnotationFO
		{
			return _approvalAnnotation;
		}

		public override function clone():Event
		{
			return new ApprovalAnnotationAddedEvent(type,approvalAnnotation);
		}

		public override function toString():String
		{
			return formatToString("ApprovalAnnotationAddedEvent","approvalAnnotation");
		}
	}
}
