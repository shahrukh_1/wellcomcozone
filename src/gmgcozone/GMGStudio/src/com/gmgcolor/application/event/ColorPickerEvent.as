package com.gmgcolor.application.event
{
import flash.events.Event;

public class ColorPickerEvent extends Event
	{
		public static const COLOR_CHANGE:String = "colorChange";

		private var _color:uint;

		public function ColorPickerEvent(type:String, color:uint)
		{
			super(type,false,false);

			this._color = color;
		}

		public function get color():uint
		{
			return _color;
		}

		public override function clone():Event
		{
			return new ColorPickerEvent(type,color);
		}

		public override function toString():String
		{
			return formatToString("ColorPickerEvent","color");
		}
	}
}
