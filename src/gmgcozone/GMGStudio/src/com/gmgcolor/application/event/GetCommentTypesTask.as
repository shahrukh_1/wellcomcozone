package com.gmgcolor.application.event {
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetCommentTypesTask extends Task {


		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var securityManager:SecurityManager;

		public function GetCommentTypesTask()
		{
			super();
			setName("GetCommentTypesTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);

		}

		override protected function doStart():void
		{
			dispatcher(new GetCommentTypesEvent(GetCommentTypesEvent.GET_COMMENT_TYPES, securityManager.sessionId));
		}

		[CommandComplete]
		public function onComplete(message:GetCommentTypesEvent):void
		{
			complete();
		}

		[CommandError]
		public function onError(fault:Fault, message:GetCommentTypesEvent):void
		{
			// Don't create fault because SSO is not required
			complete();
		}

		override protected function doTimeout():void
		{
			skip();
		}
	}
}


