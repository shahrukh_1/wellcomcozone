package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetDecisionsEvent extends Event
	{
		public static const getDecisionsEvent:String = "getdecisionsevent";

		private var _sessionId:String;

		public function GetDecisionsEvent(type:String, sessionId:String)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetDecisionsEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetDecisionsEvent","sessionId");
		}
	}
}
