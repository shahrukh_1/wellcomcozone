package com.gmgcolor.application.event
{
import flash.events.Event;

public class ShowRightPanelEvent extends Event
	{
		private var _name:String;
		private var _show:Boolean;

        public static const SHOW_RIGHT_PANEL = 'showRightPanel';

		public function ShowRightPanelEvent(name:String, show:Boolean)
		{
			this._name = name;
            this._show = show;
            super(name);
        }

		public function get name():String
		{
			return _name;
		}

		public function get show():Boolean
		{
			return _show;
		}

	}
}
