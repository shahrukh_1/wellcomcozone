package com.gmgcolor.application.event {
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.Fault;

public class GetRolesTask extends Task {

		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var securityManager:SecurityManager;

		public function GetRolesTask()
		{
			super();
			setName("GetRolesTask");
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);

		}

		override protected function doStart():void
		{
			dispatcher(new GetRolesEvent(GetRolesEvent.GET_ROLES, securityManager.sessionId));
		}

		[CommandComplete]
		public function onComplete(message:GetRolesEvent):void
		{
			complete();
		}

		[CommandError]
		public function onError(fault:Fault, message:GetRolesEvent):void
		{
			complete();
		}

		override protected function doTimeout():void
		{
			skip();
		}
	}
}


