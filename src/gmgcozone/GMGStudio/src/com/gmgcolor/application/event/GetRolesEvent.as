package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetRolesEvent extends Event
	{
		public static const GET_ROLES:String = "getRoles";

		private var _sessionId:String;

		public function GetRolesEvent(type:String, sessionId:String)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetRolesEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetRolesEvent","sessionId");
		}
	}
}
