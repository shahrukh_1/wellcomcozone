package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetCommentTypesEvent extends Event
	{
		public static const GET_COMMENT_TYPES:String = "getCommentTypes";

		private var _sessionId:String;

		public function GetCommentTypesEvent(type:String, sessionId:String)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetCommentTypesEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetCommentTypes","sessionId");
		}
	}
}
