package com.gmgcolor.application.event
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;

import flash.events.Event;

public class AddApprovalAnnotationEvent extends Event
	{
		public static const ADD:String = "add";

		private var _annotation:ApprovalAnnotationFO;
		private var _sessionId:String;

		public function AddApprovalAnnotationEvent(type:String, annotation:ApprovalAnnotationFO, sessionId:String)
		{
			super(type,false,false);

			this._annotation = annotation;
			this._sessionId = sessionId;
		}

		public function get annotation():ApprovalAnnotationFO
		{
			return _annotation;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}


		public override function clone():Event
		{
			return new AddApprovalAnnotationEvent(type,annotation,sessionId);
		}

		public override function toString():String
		{
			return formatToString("AddApprovalAnnotationEvent","annotation","sessionId");
		}
	}
}
