package com.gmgcolor.application.event {
import flash.events.Event;

public class GetUserAndAccountInfoEvent extends Event {
		public static const GetUserAndAccountInfo:String = "getuserandaccountinfo";

		private var _sessionId:String;
		private var _approvalId:int;
		private var _userId:int;
		private var _isExternal:Boolean;

		public function GetUserAndAccountInfoEvent(type:String, sessionId:String, userId:int, isExternal:Boolean, approvalId:int)
		{
			super(type, false, false);

			this._sessionId = sessionId;
			this._userId = userId;
			this._isExternal = isExternal;
			this._approvalId = approvalId;
		}

		public function get sessionId():String
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetUserAndAccountInfoEvent(type, sessionId, userId, isExternal, approvalId);
		}

		public override function toString():String
		{
			return formatToString("GetUserAndAccountInfo", "sessionId");
		}

		public function get userId():int
		{
			return _userId;
		}

		public function get approvalId():int
		{
			return _approvalId;
		}

		public function get isExternal():Boolean
		{
			return _isExternal;
		}
	}
}
