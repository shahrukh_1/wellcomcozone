package com.gmgcolor.application.event
{
import flash.events.Event;

public class IsLockedChangeEvent extends Event
	{
		public static const CHANGE:String = "change";

		private var _isLocked:Boolean;

		public function IsLockedChangeEvent(type:String, isLocked:Boolean)
		{
			super(type,false,false);

			this._isLocked = isLocked;
		}

		public function get isLocked():Boolean
		{
			return _isLocked;
		}

		public override function clone():Event
		{
			return new IsLockedChangeEvent(type,isLocked);
		}

		public override function toString():String
		{
			return formatToString("isLockedChangeEvent","isLocked");
		}
	}
}
