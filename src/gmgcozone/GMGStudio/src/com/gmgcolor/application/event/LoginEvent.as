package com.gmgcolor.application.event
{
import flash.events.Event;

public class LoginEvent extends Event
	{
		public static const LOGIN:String = "login";
		private var _sessionId:String;
		private var _userId:int;

		public function LoginEvent(type:String, sessionId:String, userId:int)
		{
			super(type,false,false);
			this._sessionId = sessionId;
			this._userId = userId;
		}

		public override function clone():Event
		{
			return new LoginEvent(type,sessionId,userId);
		}

		public override function toString():String
		{
			return formatToString("LoginEvent");
		}

		public function get sessionId():String
		{
			return _sessionId;
		}


		public function get userId():int
		{
			return _userId;
		}


	}
}
