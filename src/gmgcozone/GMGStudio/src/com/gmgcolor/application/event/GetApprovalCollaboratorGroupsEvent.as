package com.gmgcolor.application.event
{
import flash.events.Event;

public class GetApprovalCollaboratorGroupsEvent extends Event
	{
		public static const GetApprovalCollaboratorGroups:String = "getapprovalcollaboratorgroups";

		private var _sessionId:Object;

		public function GetApprovalCollaboratorGroupsEvent(type:String, sessionId:Object)
		{
			super(type,false,false);

			this._sessionId = sessionId;
		}

		public function get sessionId():Object
		{
			return _sessionId;
		}

		public override function clone():Event
		{
			return new GetApprovalCollaboratorGroupsEvent(type,sessionId);
		}

		public override function toString():String
		{
			return formatToString("GetApprovalCollaboratorGroupsEvent","sessionId");
		}
	}
}
