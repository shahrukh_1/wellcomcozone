package com.gmgcolor.application.presentation {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.domain.ApprovalDecision;
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.PageChangeEvent;
import com.gmgcolor.annotations.presentation.DocumentPM;
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.data.domain.SoftProofingStatus;
import com.gmgcolor.application.enums.LanguageEnum;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.event.GetURLEvent;
import com.gmgcolor.application.event.IsLockedChangeEvent;
import com.gmgcolor.application.event.NavigationEvent;
import com.gmgcolor.application.event.SoftProofingStatusEvent;
import com.gmgcolor.application.event.UpdateApprovalDecisionEvent;
import com.gmgcolor.application.event.VersionCompareEvent;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;
import com.gmgcolor.global.managers.LanguageManager;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import mx.collections.ArrayCollection;
import mx.collections.ListCollectionView;
import mx.core.FlexGlobals;

import spark.collections.Sort;

import spark.components.CheckBox;
import spark.components.DropDownList;
import spark.events.IndexChangeEvent;

public class NavigationPM {

		[Bindable]
		public var toggleSelected:Boolean;

		[Bindable]
		public var currentState:String;

		[Bindable]
		public var showVersions:Boolean;


		[Bindable]
		public var showCompareButton:Boolean;

		[Inject]
		[Bindable]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var languageManager:LanguageManager;

		[Inject]
		[Bindable]
		public var applicationModel:ApplicationModel;

		[Inject]
		[Bindable]
		public var securityManager:SecurityManager;

		[Bindable]
		public var disableCompareButton:Boolean;

		[Bindable]
		public var displayPrintBtn:Boolean;

		[Bindable]
		public function get popUpState():String
		{
			return _popUpState;
		}

		public function set popUpState(value:String):void
		{
			_popUpState = value;
		}

		[MessageDispatcher]
		public var dispatcher:Function;

		[Bindable]
		public var versionTypes:ListCollectionView;

		[Bindable]
		public var versionPrompt:String = "Compare";

		[Bindable]
		public var descisionSelectedItem:ApprovalDecision;

		[Bindable]
		public var commentTypes:ArrayCollection;

		private var _popUpState:String = "normal";

		[Bindable]
		public var isLocked:Boolean;
		[Bindable]
		public var isPixelCompare:Boolean;

        [Bindable]
        public var softProofingEnabled:Boolean = false;

        [Bindable]
        public var softProofingMonitorName:String = "";

        [Bindable]
        public var softProofingLevel:Number = 0;

        [Init]
		public function init():void
		{
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_VERSIONS_CHANGE_EVENT, onCurrentApprovalChange);
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_CHANGE_EVENT, onCurrentApprovalChange);
			setVersionProperties();
		}

		[MessageHandler]
		public function onPageChange(event:PageChangeEvent):void
		{
			var approval:Approval = approvalModel.getApprovalFromPage(event.page.Approval);
			displayPrintBtn = approval.Annotations && approval.Annotations.length > 0;
		}

		[MessageHandler]
		public function onAnnotationsChange(event:AnnotationsChangeEvent):void
		{
			var annotations:ArrayCollection = event.annotations;
			displayPrintBtn = annotations && annotations.length > 0;
		}

		[MessageHandler(selector="selected")]
		public function onDocumentSelectedEvent(event:DocumentEvent):void
		{
			var approval:Approval = approvalModel.getApprovalFromPage(DocumentPM(event.value).page.Approval);
			isLocked = approval.IsLocked;
		}

		[MessageHandler(scope="global")]
		public function onIsLockedChangeEvent(event:IsLockedChangeEvent):void
		{
			isLocked = event.isLocked;
		}

		[MessageHandler(scope="global")]
		public function onDocumentEvent(event:DocumentControlsEvent):void {
			switch(event.type) {
				case DocumentControlsEvent.PIXEL_DIFFERENCE_BUTTON_CLICK:
					isPixelCompare = true;
					break;
				case DocumentControlsEvent.COMPARE_BUTTON_CLICK:
				case DocumentControlsEvent.DONE_BUTTON_CLICK:
					isPixelCompare = false;
					break;
			}
		}

        [MessageHandler(scope="global")]
        public function onSoftProofingStatus(event:SoftProofingStatusEvent):void
        {
            var status:SoftProofingStatus = event.status;
            if(status.Enabled)
            {
                softProofingEnabled = true;
                softProofingMonitorName = status.MonitorName;
                softProofingLevel = status.Level;
            }
        }
		public function onDecisionBtnClick(event:IndexChangeEvent):void
		{
			var dropDownList:DropDownList = DropDownList(event.target);
			var approvalDecision:ApprovalDecision = ApprovalDecision(dropDownList.selectedItem);

			dispatcher(new UpdateApprovalDecisionEvent(UpdateApprovalDecisionEvent.UpdateApprovalDecision,
					securityManager.sessionId,
					approvalModel.currentApprovalId,
					approvalDecision.ID,
					securityManager.accountUserInfo.UserID));
		}


		public function onOpenPopUp():void
		{
			for each (var version:Approval in versionTypes) {
				version.isEnabled = false;
			}
			updateButtons();
		}


		public static function onExitButtonClick(event:MouseEvent):void
		{
			var urlRequest:URLRequest = new URLRequest(FlexGlobals.topLevelApplication.parameters.URL);
			navigateToURL(urlRequest, "_self");
		}

		public function onDownloadOriginal(event:MouseEvent):void
		{
			var urlRequest:URLRequest = new URLRequest(String(approvalModel.currentApproval.DownloadOriginalFileLink));
			navigateToURL(urlRequest, "_blank");

		}

		public function onAddNewVersion(event:MouseEvent):void
		{
			dispatcher(new GetURLEvent(GetURLEvent.ADD_NEW_VERSION, securityManager.sessionId, approvalModel.currentApprovalId));
		}

		public function onPrint(event:MouseEvent):void
		{
			dispatcher(new GetURLEvent(GetURLEvent.PRINT, securityManager.sessionId, approvalModel.currentApprovalId, approvalModel.currentApproval.Version));
		}

		private function onCurrentApprovalChange(e:Event = null):void
		{
			if (approvalModel && approvalModel.currentApproval) {
				setVersionProperties();

				for each (var approvalDecision:ApprovalDecision in applicationModel.decisions) {
					if (approvalDecision && approvalDecision.ID == approvalModel.currentApproval.Decision)
						descisionSelectedItem = approvalDecision;
				}
			}
		}

		public function onCheckBoxClick(event:Event, version:Approval):void
		{
			var checkBox:CheckBox = CheckBox(event.target);

            if(checkBox.selected && approvalModel.currentApproval.ApprovalType == Approval.SWF_TYPE)
            {
                for each (var approvalVersion:Approval in versionTypes) {
                    if (approvalVersion.isEnabled) {
                        approvalVersion.isEnabled = false;
                    }
                }
            }
			version.isEnabled = checkBox.selected;

			versionTypes.refresh();

			showCompareButton = false;

			updateButtons();
		}

		private function updateButtons():void
		{
			var count:int;

			for each (var version:Approval in versionTypes) {
				if (version.isEnabled) {
					count++
				}
			}

			if (count > 1)
				showCompareButton = true;

			disableCompareButton = count == 0;
		}


		public function setVersionProperties():void
		{
			versionTypes = new ListCollectionView(approvalModel.currentApprovalVersions);
            var sort:Sort = new Sort();
            sort.compareFunction = function(a:Object, b:Object, array:Array = null):int {
                if(a.VersionNumber == b.VersionNumber)
                    return 0;
                if(a.VersionNumber > b.VersionNumber)
                    return -1;
                return 1;
            };
            versionTypes.sort = sort;
            versionTypes.refresh();
			showVersions = versionTypes.length > 1;
		}

		public function NavigationPM()
		{
		}

		private function filterVersions(item:Approval):Boolean
		{

			return item.VersionNumber != 0;
		}

		public function onCompareModeBtnClick(event:MouseEvent):void
		{

			var a:Array = [];

			for each (var version:Approval in versionTypes) {
				if (version.isEnabled)
					a.push(version);
			}

			currentState = "normal";

			toggleSelected = false;

			dispatcher(new VersionCompareEvent(VersionCompareEvent.COMPARE, a));
		}

		public function onMouseDownOutsideHandler(event:MouseEvent):void
		{
			toggleSelected = false;
			currentState = "normal";
		}

		public function onAnnotateBtnClick(event:MouseEvent):void
		{
			dispatcher(new NavigationEvent(NavigationEvent.ANNOTATE_BUTTON_CLICK));
		}

		public function ddLanguages_changeHandler(event:IndexChangeEvent):void
		{
			var selectedItem:LanguageEnum = DropDownList(event.target).selectedItem;
			languageManager.selectedLanguage = selectedItem;
		}
	}
}
