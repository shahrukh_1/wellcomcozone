/**
 * a rotatable rectangle
 * 
 * all rotations are in radians
 */
package com.gmgcolor.application.presentation
{
import flash.geom.Matrix;
import flash.geom.Point;

public class OrientedRectangle
	{
		private var _mat:Matrix;
		
		public function OrientedRectangle( ix:Number, iy:Number, width:Number, height:Number, rot:Number=0)
		{
			_mat = new Matrix(width, 0, 0, height, ix, iy);
			if(rot)
			{
				_mat.rotate(rot);
			}
		}
		
		public function get x():Number { return _mat.tx; }
		public function set x(value:Number):void { _mat.tx = value; }
		
		public function get y():Number { return _mat.ty; }
		public function set y(value:Number):void { _mat.ty = value; }
		
		public function get width():Number
		{
			return Math.sqrt(_mat.a*_mat.a + _mat.b*_mat.b);
		}
		
		public function set width(value:Number):void
		{
			var scx:Number = this.width;
			
			if (scx)
			{
				var ratio:Number = value / scx;
				
				_mat.a *= ratio;
				_mat.b *= ratio;
			}
			else
			{
				//if tmp was 0, set scaleX from skewY
				var sky:Number = Math.atan2(_mat.b, _mat.a);
				_mat.a = Math.cos(sky) * value;
				_mat.b = Math.sin(sky) * value;
			}
		}
		
		
		public function get height():Number
		{
			return Math.sqrt(_mat.c*_mat.c + _mat.d*_mat.d);
		}
		
		public function set height(value:Number):void
		{
			var scy:Number = this.height;
			
			if (scy)
			{
				var ratio:Number = value / scy;
				
				_mat.c *= ratio;
				_mat.d *= ratio;
			}
			else
			{
				//if tmp was 0, set scaleY from skewX
				var skx:Number = Math.atan2(-_mat.c, _mat.d);
				
				_mat.c = -Math.sin(skx) * value;
				_mat.d =  Math.cos(skx) * value;
			}
		}
		
		public function get rotation():Number
		{
			return Math.atan2(_mat.b, _mat.a);
		}
		
		public function set rotation(value:Number):void
		{
			_mat.rotate(value - this.rotation);
		}
		
		public function get topLeft():Point
		{
			return new Point(_mat.tx, _mat.ty);
		}
		
		public function get topRight():Point
		{
			return new Point(_mat.tx + _mat.a, _mat.ty + _mat.b);
		}
		
		public function get bottomLeft():Point
		{
			return new Point(_mat.tx + _mat.c, _mat.ty + _mat.d);
		}
		
		public function get bottomRight():Point
		{
			return new Point(_mat.tx + _mat.a + _mat.c, _mat.ty + _mat.b + _mat.d);
		}
	}
}
