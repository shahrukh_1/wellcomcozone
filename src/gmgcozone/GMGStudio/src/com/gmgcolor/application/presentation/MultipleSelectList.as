package com.gmgcolor.application.presentation
{
import flash.events.MouseEvent;

import mx.core.mx_internal;

import spark.components.List;

use namespace mx_internal;
	
	public class MultipleSelectList extends List
	{
		public function MultipleSelectList()
		{
			super();
		}
		

		public override function get selectedIndex():int
		{
			//TODO Auto-generated method stub
			return super.selectedIndex;
		}

		public override function set selectedIndex(value:int):void
		{
			//TODO Auto-generated method stub
			super.selectedIndex = value;
		}

		mx_internal function setSelectedIndex(value:int, dispatchChangeEvent:Boolean=false, changeCaret:Boolean=true):void
		{
			//TODO Auto-generated method stub
			super.setSelectedIndex(value,dispatchChangeEvent,changeCaret);
		}

		/**
		 *  @private
		 *  Handles <code>MouseEvent.MOUSE_DOWN</code> events from any of the 
		 *  item renderers. This method handles the updating and commitment 
		 *  of selection as well as remembers the mouse down point and
		 *  attaches <code>MouseEvent.MOUSE_MOVE</code> and
		 *  <code>MouseEvent.MOUSE_UP</code> listeners in order to handle
		 *  drag gestures.
		 *
		 *  @param event The MouseEvent object.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 1.5
		 *  @productversion Flex 4
		 */
		override protected function item_mouseDownHandler(event:MouseEvent):void
		{
			event.ctrlKey = true;
			event.shiftKey = false;
			super.item_mouseDownHandler(event);
		}
		
	}
}
