package com.gmgcolor.application.presentation {
import com.gmgcolor.annotations.domain.ShapeFO;
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.DrawToolsEvent;
import com.gmgcolor.annotations.event.HighlightEvent;
import com.gmgcolor.annotations.event.MarkupEvent;
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.annotations.event.PenChangeEvent;
import com.gmgcolor.annotations.event.SideBarAnnotationSelectEvent;
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.domain.DrawTools;
import com.gmgcolor.application.event.AnnotationToolSelect;
import com.gmgcolor.application.event.AnnotationToolsEvent;
import com.gmgcolor.application.model.AnnotationToolsModel;
import com.gmgcolor.application.model.DrawingToolsModel;
import com.gmgcolor.application.pages.enum.EyeDropperTypeEnum;
import com.gmgcolor.components.Swatch;
import com.greensock.events.TransformEvent;
import com.greensock.events.TransformSelectedEvent;
import com.greensock.transform.TransformItem;

import flash.display.BitmapData;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Matrix;

import mx.collections.ArrayCollection;
import mx.core.UIComponent;
import mx.events.FlexMouseEvent;

import org.apache.flex.spark.utils.ColorPickerUtil;

import spark.components.HSlider;
import spark.components.ToggleButton;
import spark.events.IndexChangeEvent;

public class AnnotationToolsPM {
		private var _currentAnnotationState:String = ANNOTATION_TOOL_STATE_NORMAL;

		private var _currentDrawingState:String = DRAWING_STATE_MOVE;

		public static const MARKER_COMMENT:String = "Marker Comment";
		public static const AREA_COMMENT:String = "Area Comment";
		public static const TEXT_COMMENT:String = "Text Comment";

		public static const ANNOTATION_TOOL_STATE_NORMAL:String = "normal";
		public static const ANNOTATION_TOOL_STATE_MARKER:String = "marker";
		public static const ANNOTATION_TOOL_STATE_DRAW:String = "draw";
		public static const ANNOTATION_TOOL_STATE_TEXT:String = "text";

		public static const DRAWING_STATE_MOVE:String = "drawing_state_move";
		public static const DRAWING_STATE_PENCIL:String = "drawing_state_pencil";
		public static const DRAWING_STATE_SHAPES:String = "drawing_state_shapes";

		[Embed(source="/assets/ballon.fxg", mimeType="application/octet-stream")]
		protected const ballonFXG:Class;

		[Bindable]
		public var commentTypes:ArrayCollection;
		[Bindable]
		public var selectedDrawToolIndex:int = 0;

		[Bindable]
		[Inject]
		public var applicationModel:ApplicationModel;

		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var annotationToolModel:AnnotationToolsModel;

		[Inject]
		public var drawToolsModel:DrawingToolsModel;

		[Inject]
		public var securityManager:SecurityManager;

		[Inject]
		public var approvalModel:ApprovalModel;

		[Bindable]
		public var shapesToggleSelected:Boolean;

		[Bindable]
		public var eyeDropperTypeSizes:ArrayCollection;

		[Bindable]
		public var annotateButtonSelected:Boolean;

		[Bindable]
		public var drawButtonSelected:Boolean;


		[Bindable]
		public var selectedSwatchColor:uint = 0xf99e3f;

		[Bindable]
		public var selectedOpacityAlpha:Number = 1;

		[Bindable]
		public var selectedPenSize:Number = 5;

		private var _opacitySliderValue:int = 100;

		[Bindable]
		public var enableTextButton:Boolean;

		[Bindable]
		public var popUpState:String;

		[Bindable]
		public var eyePopUpState:String;

		[Bindable]
		public var toggleSelected:Boolean;
		private var _strokeWeight:int = 8;
		private var _strokeColor:uint = 0xFF0000;
		private var _fillColor:uint = 0xFF0000;
		private var _strokeAlpha:Number = 1;
		private var _fillAlpha:Number = 1;
		private var _currentTransformItem:TransformItem;

		[MessageHandler(scope="global")]
		public function onTransformSelectEvent(event:TransformSelectedEvent):void
		{
			if (event.currentTransformItem != _currentTransformItem) {
				dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
				currentAnnotationState = ANNOTATION_TOOL_STATE_DRAW;
				currentDrawingState = event.isDrawing ? DRAWING_STATE_PENCIL : DRAWING_STATE_SHAPES;
				selectedDrawToolIndex = 0;
				_currentTransformItem = event.currentTransformItem;
				_currentTransformItem.addEventListener(TransformEvent.DESELECT, onTransformDeselectEvent);
			}
		}

		public function onTransformDeselectEvent(event:TransformEvent):void
		{
			_currentTransformItem.removeEventListener(TransformEvent.DESELECT, onTransformDeselectEvent);
			currentDrawingState = DRAWING_STATE_MOVE;
			_currentTransformItem = null;
		}

		[Bindable]
		public function get strokeAlpha():Number
		{
			return _strokeAlpha;
		}

		public function set strokeAlpha(value:Number):void
		{
			_strokeAlpha = value;
			dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
			updateSelected();
		}

		[Bindable]
		public function get fillAlpha():int
		{
			return _fillAlpha;
		}

		public function set fillAlpha(value:int):void
		{
			_fillAlpha = value;
			dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
			updateSelected();
		}

		[Bindable]
		public function get fillColor():uint
		{
			return _fillColor;
		}

		public function set fillColor(value:uint):void
		{
			_fillColor = value;
			dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
			updateSelected();
		}

		[Bindable]
		public function get strokeWeight():int
		{
			return _strokeWeight;
		}

		public function set strokeWeight(value:int):void
		{
			_strokeWeight = value;
			dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
			updateSelected();
		}

		[Bindable]
		public function get strokeColor():uint
		{
			return _strokeColor;
		}

		public function set strokeColor(value:uint):void
		{
			_strokeColor = value;
			dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
			updateSelected();
		}

		private function updateSelected():void
		{
			if (_currentTransformItem && _currentTransformItem.targetObject is FXGConvertor) {
				var xml:XML = FXGConvertor(_currentTransformItem.targetObject).xml;
				switch (currentDrawingState) {
					case DRAWING_STATE_PENCIL:
						xml..*::SolidColorStroke.@color = ColorPickerUtil.uintToHex(strokeColor);
						xml..*::SolidColorStroke.@weight = ColorPickerUtil.uintToHex(strokeWeight);
						break;
					case DRAWING_STATE_SHAPES:
                        for (var index:* in xml..*::SolidColor.@color)
                        {
                            xml..*::SolidColor.@color[index] = ColorPickerUtil.uintToHex(fillColor);
                        }
						break;
				}
				FXGConvertor(_currentTransformItem.targetObject).xml = xml;
				FXGConvertor(_currentTransformItem.targetObject).approvalAnnotationMarkup.Shape.FXG = xml;
				dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.UPDATE_SELECTION))
			}
		}

		private function getDrawTools():DrawTools
		{
			var d:DrawTools = new DrawTools;
			d.fillColor = fillColor;
			d.fillAlpha = fillAlpha;
			d.strokeAlpha = strokeAlpha;
			d.strokeColor = strokeColor;
			d.strokeWeight = strokeWeight;
			return d;
		}

		public function AnnotationToolsPM()
		{
			commentTypes = new ArrayCollection();

			commentTypes.addItem(MARKER_COMMENT);
			commentTypes.addItem(AREA_COMMENT);
			commentTypes.addItem(TEXT_COMMENT);

			eyeDropperTypeSizes = new ArrayCollection();
			eyeDropperTypeSizes.addItem(EyeDropperTypeEnum.COLOUR_HEX);
			eyeDropperTypeSizes.addItem(EyeDropperTypeEnum.COLOUR_RGB);
			eyeDropperTypeSizes.addItem(EyeDropperTypeEnum.COLOUR_CMYK);

		}

		public function onShapeSelected(shapeType:ShapeFO, color:uint):void
		{
			shapesToggleSelected = false;

			// change shape color
			var xml:XML = new XML(shapeType.FXG);
            for (var index:* in xml..*::SolidColor.@color)
            {
                xml..*::SolidColor.@color[index] = ColorPickerUtil.uintToHex(color);
            }

			if (_currentTransformItem && currentDrawingState == DRAWING_STATE_SHAPES) {
				FXGConvertor(_currentTransformItem.targetObject).xml = xml;
			} else {
				dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.SHAPE_CLICK, xml.toXMLString()));
			}
		}

		public function onMouseDownOutsideHandler(event:MouseEvent):void
		{
			shapesToggleSelected = false;
		}

		[Init]
		public function init():void
		{
			annotationToolModel.addEventListener(AnnotationToolsModel.ANNOTATEBUTTONSELECTED_CHANGE_EVENT, onAnnotateButtonSelectedChange);
			annotationToolModel.addEventListener(AnnotationToolsModel.DRAWBUTTONSELECTED_CHANGE_EVENT, onDrawButtonSelectedChange);

			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_PAGES_CHANGE_EVENT, cancelTool);
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_PAGE_CHANGE_EVENT, cancelTool);
		}

		public function eyeDropperClick(data:Object):void
		{
			eyePopUpState = 'normal';
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.EYE_DROPPER_BUTTON_CLICK, EyeDropperTypeEnum(data)));
		}

		public function emailPopUp_mouseDownOutsideHandler(event:FlexMouseEvent):void
		{
			popUpState = 'normal';
		}

		public function eyePopUp_mouseDownOutsideHandler(event:FlexMouseEvent):void
		{
			eyePopUpState = 'normal';
		}

		private function onDrawButtonSelectedChange(event:Event):void
		{
			drawButtonSelected = annotationToolModel.drawButtonSelected;
		}

		private function onAnnotateButtonSelectedChange(event:Event):void
		{
			annotateButtonSelected = annotationToolModel.annotateButtonSelected;
		}

		[MessageHandler(selector="reset", scope="global")]
		public function onReset(event:AnnotationToolsEvent):void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_NORMAL;
		}

		[MessageHandler(selector="available", scope="global")]
		public function onHighlightsAvailable(event:HighlightEvent):void
		{
			enableTextButton = true;
		}

		[MessageHandler(selector="unavailable", scope="global")]
		public function onHighlightsUnavailable(event:HighlightEvent):void
		{
			enableTextButton = false;
		}

		[MessageHandler(selector="cancel", scope="global")]
		public function onCommentsCancel(event:CommentsEvent):void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_NORMAL;
			currentDrawingState = DRAWING_STATE_MOVE;
			selectedDrawToolIndex = 0;
		}

		[MessageHandler]
		public function onPostComment(event:NewCommentEvent):void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_NORMAL;
		}

		[MessageHandler]
		public function onSideBarAnnotationSelect(event:SideBarAnnotationSelectEvent):void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_NORMAL;
		}

		public function onAnnotateButtonClick(event:MouseEvent):void
		{
			annotationToolModel.annotateButtonSelected = ToggleButton(event.target).selected;
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.ANNOTATE_BUTTON_CLICK));
		}

		[MessageHandler]
		public function onAnnotationToolChange(event:AnnotationToolSelect):void
		{
			switch (event.type) {

				case AnnotationToolSelect.DRAW_BUTTON_SELECT:
				{
					drawSelect();
					break;
				}
				case AnnotationToolSelect.EYE_DROPPER_BUTTON_SELECT:
				{

					break;
				}
				case AnnotationToolSelect.MARKER_BUTTON_SELECT:
				{
					markerSelect();
					break;
				}
				case AnnotationToolSelect.TEXT_BUTTON_SELECT:
				{
					textSelect();
					break;
				}

				default:
				{
					break;
				}
			}
		}

		public function onAnnotationButtonClick(data:Object):void
		{
			dispatcher(new CommentsEvent(CommentsEvent.CANCEL));
			if (data) {
				switch (data.buttonId) {
					case 'marker':
						markerSelect();
						break;
					case 'text':
						textSelect();
						break;
					case 'draw':
						drawSelect();
						break;
				}
			}
		}

		private function markerSelect():void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_MARKER;
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.MARKER_BUTTON_CLICK));
		}

		private function drawSelect():void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_DRAW;
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.DRAW_BUTTON_CLICK));
		}

		private function textSelect():void
		{
			currentAnnotationState = ANNOTATION_TOOL_STATE_TEXT;
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.TEXT_BUTTON_CLICK));
		}

		public function cancelTool(e:Event = null):void
		{
			dispatcher(new CommentsEvent(CommentsEvent.CANCEL));
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.RESET));
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.DONE));
		}

		public function onSwatchClick(event:MouseEvent):void
		{
			var swatch:Swatch = Swatch(event.currentTarget);
			selectedSwatchColor = swatch.swatchColor;
			dispatcher(new PenChangeEvent(PenChangeEvent.CHANGE, selectedPenSize, selectedSwatchColor, selectedOpacityAlpha));
		}

		public function onSwatchAlphaChange(event:Event):void
		{
			selectedOpacityAlpha = HSlider(event.target).value / 100;
			dispatcher(new PenChangeEvent(PenChangeEvent.CHANGE, selectedPenSize, selectedSwatchColor, selectedOpacityAlpha));
		}

		public function onPenSizeChange(event:Event):void
		{
			selectedPenSize = HSlider(event.target).value;
			dispatcher(new PenChangeEvent(PenChangeEvent.CHANGE, selectedPenSize, selectedSwatchColor, selectedOpacityAlpha));
		}

		private function getBitmapData(target:UIComponent):BitmapData
		{
			var bd:BitmapData = new BitmapData(target.width, target.height);
			var m:Matrix = new Matrix();
			bd.draw(target, m);
			return bd;
		}

		[Bindable]
		public function get currentTransformItem():TransformItem
		{
			return _currentTransformItem;
		}

		public function set currentTransformItem(value:TransformItem):void
		{
			_currentTransformItem = value;
		}

		[Bindable]
		public function get currentAnnotationState():String
		{
			return _currentAnnotationState;
		}

		public function set currentAnnotationState(value:String):void
		{
			_currentAnnotationState = value;
			annotationToolModel.currentAnnotationTool = value == ANNOTATION_TOOL_STATE_NORMAL ? null : value;
		}

		public function onDrawToolsEvent(event:IndexChangeEvent):void
		{
			dispatcher(new AnnotationToolsEvent(AnnotationToolsEvent.DESELECT));
			selectedDrawToolIndex = event.newIndex;
			switch (selectedDrawToolIndex) {
				case 0:
				{
					dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
					currentDrawingState = DRAWING_STATE_MOVE;
					break;
				}
				case 1:
				{
					dispatcher(new MarkupEvent(MarkupEvent.ENABLE));
					dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
					currentDrawingState = DRAWING_STATE_PENCIL;
					break;
				}
				case 2:
				{
					dispatcher(new MarkupEvent(MarkupEvent.DISABLE));
					dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
					currentDrawingState = DRAWING_STATE_SHAPES;
					break;
				}
				default:
				{
					break;
				}
			}
		}


		[Bindable]
		public function get opacitySliderValue():int
		{
			return _opacitySliderValue;
		}

		public function set opacitySliderValue(value:int):void
		{
			_opacitySliderValue = value;
			strokeAlpha = value / 100;
			dispatcher(new DrawToolsEvent(DrawToolsEvent.TOOL_CHANGE, getDrawTools()));
		}

		[Bindable]
		public function get currentDrawingState():String
		{
			return _currentDrawingState;
		}

		public function set currentDrawingState(value:String):void
		{
			_currentDrawingState = value;
			drawToolsModel.currentDrawingTool = value;
		}
	}
}
