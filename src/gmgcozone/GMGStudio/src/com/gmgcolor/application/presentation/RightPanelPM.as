package com.gmgcolor.application.presentation {
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.application.event.AllowMultiplePanelsEvent;
import com.gmgcolor.application.event.ShowRightPanelContainerEvent;
import com.gmgcolor.application.event.ShowRightPanelEvent;

public class RightPanelPM {
		static public const CONTAINER_ANNOTATIONS:String = "annotations";
		static public const CONTAINER_COLLABORATIONS:String = "collaborations";

		[Bindable]
		public var allowMultipleExpanded:Boolean = true;

        private var _show:Boolean = false;

		[MessageDispatcher]
		public var dispatcher:Function;

		public function RightPanelPM()
		{
		}

		[MessageHandler]
		public function onAllowMultiplePanelsEvent(event:AllowMultiplePanelsEvent):void
		{
			allowMultipleExpanded = event.allow;
		}

		[MessageHandler]
		public function showPanelMessage(event:ShowRightPanelContainerEvent):void
		{
			show = event.show;
		}

		[MessageHandler]
		public function onPostComment(m:NewCommentEvent):void {
            show = true;
		}

        [Bindable]
        public function get show():Boolean
        {
            return _show;
        }

        public function set show(value:Boolean):void
        {
            _show = value;
            dispatcher(new ShowRightPanelEvent(ShowRightPanelEvent.SHOW_RIGHT_PANEL, _show));
        }
    }
}
