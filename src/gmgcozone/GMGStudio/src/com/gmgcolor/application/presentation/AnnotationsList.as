package com.gmgcolor.application.presentation
{
import flash.events.MouseEvent;

import spark.components.List;

public class AnnotationsList extends List
	{
		public function AnnotationsList()
		{
			super();
		}
		
		override protected function item_mouseDownHandler(event:MouseEvent):void
		{
			
			var obj:Object = Object(event.target);
			
			while (obj != null)
			{
				var name:String = obj["name"] ? obj["name"] : "";
				obj = obj.parent;
				if(obj is AnnotationStatusPopUpMenu)
					return;
			} 
			
			super.item_mouseDownHandler(event);
			
		}
	}
}
