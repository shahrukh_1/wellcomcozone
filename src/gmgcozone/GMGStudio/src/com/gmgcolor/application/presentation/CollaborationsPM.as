package com.gmgcolor.application.presentation
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.domain.ApprovalCollaborator;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.event.UpdateCollobratorsIndicesEvent;
import com.gmgcolor.application.model.CollaboratorModel;
import com.gmgcolor.application.pages.events.CollaboratorSelectEvent;

import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.ListCollectionView;

public class CollaborationsPM
	{
		
		[Bindable]
		public var selectedIndices:Vector.<int> = new Vector.<int>;
		
		[Bindable]
		public var selectedIndex:int;
		
		[Bindable]
		public var collaborators:ListCollectionView;
		
		[Inject]
		public var securityManger:SecurityManager;
		
		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var collaboratorModel:CollaboratorModel;

		[MessageDispatcher] public var dispatcher:Function;

		public function CollaborationsPM()
		{
		}

		[Init]
		public function onInjectComplete():void
		{
			collaboratorModel.addEventListener(CollaboratorModel.COLLABORATORS_CHANGE_EVENT,onCollaboratorsChange);
			sendSelectedCollaborators();
		}
		
		[MessageHandler(selector="start")]
		public function onAnnotationsChange(event:AnnotationsChangeEvent):void
		{
			
		}
		
		public function itemrenderer1_clickHandler(event:MouseEvent,data:ApprovalCollaborator):void
		{
			data.isChecked = data.isChecked ? false : true;
			collaborators.itemUpdated(data);
			
			var a:Object = {};
			
			for each (var collaborator:ApprovalCollaborator in collaborators) 
			{
				if(collaborator.isChecked)
					a[collaborator.Collaborator] = collaborator.GivenName + " " + collaborator.FamilyName;
			}
			
			dispatcher(new CollaboratorSelectEvent(CollaboratorSelectEvent.CHANGE,a));
		}
		
		[Init]
		public function init():void
		{
			collaboratorModel.addEventListener("collaboratorsChange",onCollaboratorsChange);
		}

		private function onCollaboratorsChange(event:Event):void
		{
			
			var annotations:Array = approvalModel.currentApproval.Annotations;
			
			if(!approvalModel.currentApproval.Annotations)
				return;
			
			for each (var collaborator:ApprovalCollaborator in collaboratorModel.currentCollaborators)
			{
				
				if(!securityManger.accountUserInfo)
					return;
				
				if(collaborator.Collaborator == securityManger.accountUserInfo.UserID)
					collaborator.isChecked = true;
				for(var index:int = 0; index < annotations.length; index ++)
				{
					var annotation:ApprovalAnnotationFO = annotations[index] as ApprovalAnnotationFO;
					if(annotation.Modifier == collaborator.Collaborator || collaborator.Collaborator == securityManger.accountUserInfo.UserID)
					{
						collaborator.isChecked = true;
						collaboratorModel.currentCollaborators.itemUpdated(collaborator);
						break;
					}
					
				}
			}
			
			collaborators = new ListCollectionView(collaboratorModel.currentCollaborators);
			
			sendSelectedCollaborators();
			
		}

		private function sendSelectedCollaborators():void
		{
			var a:Object = {};
		
			for each (var collaborator:ApprovalCollaborator in collaborators) 
			{
				if(collaborator.isChecked)
					a[collaborator.Collaborator] = collaborator.GivenName + " " + collaborator.FamilyName;
			}
		
			dispatcher(new CollaboratorSelectEvent(CollaboratorSelectEvent.CHANGE,a));
		
			updateIndices();
		}

		
		[MessageHandler]
		public function onUpdateCollobratorsIndicesEvent(event:UpdateCollobratorsIndicesEvent):void
		{
			updateIndices();
		}
		
		private function updateIndices():void
		{
 			var a:Vector.<int> = new Vector.<int>;
			var count:int
			selectedIndices = a;
			for each (var approvalCollaborator:ApprovalCollaborator in collaborators) 
			{
				if(approvalCollaborator.isChecked)
				selectedIndices.push(count);
				count++
			}
		}
		

	}
}
