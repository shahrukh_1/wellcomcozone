package com.gmgcolor.application.presentation
{
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.core.mx_internal;

import spark.components.PopUpAnchor;
import spark.components.PopUpPosition;

use namespace mx_internal;
	
	public class CustomPopUpAnchor extends PopUpAnchor
	{
		public function CustomPopUpAnchor()
		{
			super();
		}


		override mx_internal function determinePosition(placement:String,
														popUpWidth:Number, popUpHeight:Number,matrix:Matrix,
														registrationPoint:Point, bounds:Rectangle):void
		{
			switch(placement)
			{
				case PopUpPosition.BELOW:
					registrationPoint.x = -popUpWidth + unscaledWidth;
					registrationPoint.y = unscaledHeight;
					break;
				case PopUpPosition.ABOVE:
					registrationPoint.x = 0;
					registrationPoint.y = -popUpHeight;
					break;
				case PopUpPosition.LEFT:
					registrationPoint.x = -popUpWidth;
					registrationPoint.y = 0;
					break;
				case PopUpPosition.RIGHT:
					registrationPoint.x = unscaledWidth;
					registrationPoint.y = 0;
					break;            
				case PopUpPosition.CENTER:
					registrationPoint.x = (unscaledWidth - popUpWidth) / 2;
					registrationPoint.y = (unscaledHeight - popUpHeight) / 2;
					break;            
				case PopUpPosition.TOP_LEFT:
					// already 0,0
					break;
			}
		}

	}
}
