package com.gmgcolor.application.presentation {
import com.gmgcolor.annotations.domain.AnnotationGroupByName;
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.domain.ApprovalAnnotationStatus;
import com.gmgcolor.annotations.domain.ApprovalCollaborator;
import com.gmgcolor.annotations.event.AnnotationEvent;
import com.gmgcolor.annotations.event.AnnotationSelectedEvent;
import com.gmgcolor.annotations.event.CommentsEvent;
import com.gmgcolor.annotations.event.SideBarAnnotationSelectEvent;
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.AnnotationSelectedIndexChange;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.event.FilterAnnotationEvent;
import com.gmgcolor.application.event.UpdateApprovalAnnotationEvent;
import com.gmgcolor.application.model.CollaboratorModel;
import com.gmgcolor.application.pages.events.CollaboratorSelectEvent;

import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.collections.ListCollectionView;

public class AnnotationsPM {

		[Inject]
		public var collaboratorModel:CollaboratorModel;

		[Inject]
		[Bindable]
		public var securityManager:SecurityManager;

		[Inject]
		public var applicationModel:ApplicationModel;


		[Inject]
		public var approvalModel:ApprovalModel;

		[Bindable]
		public var selectedAnnotations:ListCollectionView;

		[Bindable]
		public var annotationsByName:ListCollectionView;

		[Bindable]
		public var annotationsByPage:ListCollectionView;

		[Bindable]
		public var annotationsByStatus:ListCollectionView;

		private var _selectedItem:ApprovalAnnotationFO;

		[Bindable]
		public var currentState:String = BY_NAME;

		[Bindable]
		public var statusPopUpState:String = "byName";


		public static const BY_NAME:String = "byName";
		public static const BY_STATUS:String = "byStatus";
		public static const BY_PAGE:String = "byPage";

		private var _selectedIndex:int;

		private var collaboratorsIds:Object = {};

		private var annotations:ArrayCollection;

		private var _filterIndex:int;

		public function annotationFilter(index:int):void
		{
			dispatcher(new FilterAnnotationEvent(FilterAnnotationEvent.CHANGE, index));
		}

		[Bindable]
		public function get filterIndex():int
		{
			return _filterIndex;
		}

		public function set filterIndex(value:int):void
		{
			_filterIndex = value;
		}

		[Bindable]
		public function get selectedIndex():int
		{
			return _selectedIndex;
		}

		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;

			var e:AnnotationSelectedIndexChange = new AnnotationSelectedIndexChange(AnnotationSelectedIndexChange.CHANGE, value);
			dispatcher(e);
		}

		public function updateAnnotationStatus(annotation:ApprovalAnnotationFO):void
		{
			selectedAnnotations.itemUpdated(annotation);
			if (selectedAnnotations) {
				annotationsByStatus = new ListCollectionView(groupByStatus(selectedAnnotations, collaboratorsIds));
			}
			dispatcher(new UpdateApprovalAnnotationEvent(UpdateApprovalAnnotationEvent.updateApprovalAnnoation, securityManager.sessionId, annotation));
		}

		/**
		 *
		 */
		[MessageDispatcher]
		public var dispatcher:Function;

		public function AnnotationsPM()
		{

		}

		[MessageHandler]
		public function onFilterAnnotationEvent(event:FilterAnnotationEvent):void
		{
			var index:int = event.index;

			switch (index) {
				case 0:
				{
					currentState = BY_NAME;
					break;
				}

				case 1:
				{
					currentState = BY_STATUS;
					break;
				}

				case 2:
				{
					currentState = BY_PAGE;
					break;
				}

				default:
				{
					break;
				}
			}
		}


		[MessageHandler]
		public function onCollaboratorSelect(event:CollaboratorSelectEvent):void
		{
			collaboratorsIds = event.collaboratorIds;

			/*annotationsByName = null;
			 annotationsByStatus= null;
			 annotationsByPage = null;*/

			var timer:Timer = new Timer(0, 1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete2);
			timer.start();

		}

		public function onAnnotationsClick(approvalAnnotation:ApprovalAnnotationFO):void
		{
			dispatcher(new CommentsEvent(CommentsEvent.CANCEL));
			selectedItem = approvalAnnotation;
			selectAnnotation();
		}

		private function selectAnnotation():void
		{
			var timer:Timer = new Timer(40, 1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			timer.start();
		}

		private function onTimerComplete(event:TimerEvent):void
		{
			dispatcher(new SideBarAnnotationSelectEvent(SideBarAnnotationSelectEvent.ANNOTATION_CHANGE, selectedItem));
		}

		private function onTimerComplete2(event:TimerEvent):void
		{
			if (selectedAnnotations) {
				annotationsByName = new ListCollectionView(groupByName(selectedAnnotations, collaboratorsIds));
				annotationsByPage = new ListCollectionView(groupByPage(selectedAnnotations, collaboratorsIds));
				annotationsByStatus = new ListCollectionView(groupByStatus(selectedAnnotations, collaboratorsIds));
			}
		}


		[MessageHandler(selector="cancel", scope="global")]
		public function onCancelComment(event:CommentsEvent):void
		{
			selectedIndex = -1;
		}

		[MessageHandler(selector="create", scope="global")]
		public function onCreateAnnotation(event:AnnotationEvent):void
		{
			//selectedIndex = -1;
		}

		[MessageHandler]
		public function onAnnotationsChange(event:AnnotationsChangeEvent):void
		{

			collaboratorsIds = {};

			annotations = event.annotations;

			for each (var collaborator:ApprovalCollaborator in collaboratorModel.currentCollaborators) {
				if (collaborator.isChecked) {
					collaboratorsIds[collaborator.Collaborator] = collaborator.GivenName + " " + collaborator.FamilyName;
				}
			}

			selectedAnnotations = new ListCollectionView(event.annotations);
			selectedAnnotations.filterFunction = filterAnnotations;
			selectedAnnotations.refresh();

			if (selectedAnnotations) {
				annotationsByName = new ListCollectionView(groupByName(selectedAnnotations, collaboratorsIds));
				annotationsByPage = new ListCollectionView(groupByPage(selectedAnnotations, collaboratorsIds));
				annotationsByStatus = new ListCollectionView(groupByStatus(selectedAnnotations, collaboratorsIds));
			}

			annotationsByName.refresh();
			annotationsByStatus.refresh();
			annotationsByPage.refresh();

//			selectedItem = tempSelectedAnnotation;
		}

		public function groupByName(annotations:ListCollectionView, collaboratorsIds:Object):ArrayCollection
		{
			var groups:ArrayCollection = new ArrayCollection;

			var dict:Object = collaboratorsIds;

			var pointIndex:int = 0;
			for (var approverId:String in dict) {
				var groupItem:AnnotationGroupByName = new AnnotationGroupByName();

				for (var index:int = 0; index < annotations.length; index++) {
					var annotation:ApprovalAnnotationFO = annotations.getItemAt(index) as ApprovalAnnotationFO;
					var firstChar:String = "";
					if (annotation.Creator == int(approverId)) {
						groupItem.groupName = dict[approverId];
						groupItem.groupCollaborator = collaboratorModel.getApprovalCollaborator(int(approverId));
						if (groupItem.groupCollaborator) {
							if (int(approverId) == approvalModel.currentApproval.PrimaryDecisionMaker)
								groupItem.groupCollaborator.isPrimaryDecisionMaker = true;
						}
						groupItem.list.addItem(annotation);
					}
				}
				if (groupItem.list.length > 0) {
					groups.addItem(groupItem);
				}
			}
			return groups;
		}

		public function groupByStatus(annotations:ListCollectionView, collaboratorsIds:Object):ArrayCollection
		{
			var groups:ArrayCollection = new ArrayCollection;

			var statuses:ArrayCollection = applicationModel.annotationStatuses;

			var dict:Object = {};

			for each (var status:ApprovalAnnotationStatus in statuses) {
				dict[status.ID] = status.Name;
			}

			for (var statusId:String in dict) {

				var groupItem:AnnotationGroupByName = new AnnotationGroupByName();
				groupItem.groupName = dict[statusId];

				for (var approverId:String in collaboratorsIds) {

					for (var index:int = 0; index < annotations.length; index++) {
						var annotation:ApprovalAnnotationFO = annotations.getItemAt(index) as ApprovalAnnotationFO;
						var firstChar:String = "";
						if (annotation.Status == int(statusId) && annotation.Creator == int(approverId)) {
							groupItem.groupName = dict[int(statusId)];
							groupItem.statusId = int(statusId);
							groupItem.groupCollaborator = collaboratorModel.getApprovalCollaborator(int(approverId));
							groupItem.list.addItem(annotation);
						}
					}

				}

				if (groupItem.list.length > 0) {
					groups.addItem(groupItem);
				}

			}
			return groups;
		}

		public function groupByPage(annotations:ListCollectionView, collaboratorsIds:Object):ArrayCollection
		{
			var groups:ArrayCollection = new ArrayCollection;

			var dict:Object = {};


			for each (var annotation:ApprovalAnnotationFO in annotations) {
				dict[annotation.Page] = "Page " + annotation.PageNumber;
			}

			var pointIndex:int = 0;

			for (var page:String in dict) {
				var groupItem:AnnotationGroupByName = new AnnotationGroupByName();

				for (var index:int = 0; index < annotations.length; index++) {
					annotation = annotations.getItemAt(index) as ApprovalAnnotationFO;
					var firstChar:String = "";
					if (annotation.Page == int(page)) {
						groupItem.groupName = dict[page];
						groupItem.groupCollaborator = collaboratorModel.getApprovalCollaborator(annotation.Modifier);


						if (groupItem.groupCollaborator) {
							annotation.GivenName = groupItem.groupCollaborator.GivenName;
							annotation.FamilyName = groupItem.groupCollaborator.FamilyName;

							for (var approverId:String in collaboratorsIds) {
								if (annotation.Modifier == int(approverId)) {
									groupItem.list.addItem(annotation);
									break;
								}
							}
						}

					}
				}
				if (groupItem.list.length > 0) {
					groups.addItem(groupItem);
				}
			}


			return groups;
		}


		[MessageHandler]
		public function onAnnotationSelectedEvent(event:AnnotationSelectedEvent):void
		{
			selectedItem = ApprovalAnnotationFO(event.annotation);
		}

		[MessageHandler(selector="select")]
		public function onSelectAnnotation(event:CommentsEvent):void
		{
			selectedItem = ApprovalAnnotationFO(event.value);
		}


		[MessageHandler(selector="next")]
		public function onNextComment(event:CommentsEvent):void
		{

			var list:ArrayCollection = new ArrayCollection();

			switch (currentState) {
				case BY_NAME:
				{

					for each (var i:AnnotationGroupByName in annotationsByName) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					selectedIndex++
					selectedItem = ApprovalAnnotationFO(list.getItemAt(selectedIndex));
					selectAnnotation();
					break;
				}

				case BY_PAGE:
				{
					for each (i in annotationsByPage) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					selectedIndex++;
					selectedItem = ApprovalAnnotationFO(list.getItemAt(selectedIndex));
					selectAnnotation();
					break;
				}

				case BY_STATUS:
				{
					for each (i in annotationsByStatus) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					selectedIndex++;
					selectedItem = ApprovalAnnotationFO(list.getItemAt(selectedIndex));
					selectAnnotation();
					break;
				}

				default:
				{
					break;
				}
			}
		}


		[MessageHandler(selector="previous")]
		public function onPreviousComment(event:CommentsEvent):void
		{

			var list:ArrayCollection = new ArrayCollection();

			switch (currentState) {
				case BY_NAME:
				{

					for each (var i:AnnotationGroupByName in annotationsByName) {
						list.addAll(i.list);
					}

					selectedIndex = list.getItemIndex(selectedItem);
					selectedIndex--
					selectedItem = ApprovalAnnotationFO(list.getItemAt(selectedIndex));
					selectAnnotation();
					break;
				}

				case BY_PAGE:
				{
					for each (i in annotationsByPage) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					selectedIndex--;
					selectedItem = ApprovalAnnotationFO(list.getItemAt(selectedIndex));
					selectAnnotation();
					break;
				}

				case BY_STATUS:
				{
					for each (i in annotationsByStatus) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					selectedIndex--;
					selectedItem = ApprovalAnnotationFO(list.getItemAt(selectedIndex));
					selectAnnotation();
					break;
				}

				default:
				{
					break;
				}
			}


			/*			selectedIndex--;
			 selectedItem = ApprovalAnnotationFO(selectedAnnotations.getItemAt(selectedIndex));
			 selectAnnotation();*/
		}

		private function filterAnnotations(item:ApprovalAnnotationFO):Boolean
		{

			return item.Parent == 0;
		}


		[Bindable]
		public function get selectedItem():ApprovalAnnotationFO
		{
			return _selectedItem;
		}

		public function set selectedItem(value:ApprovalAnnotationFO):void
		{
			_selectedItem = value;

			var list:ArrayCollection = new ArrayCollection();

			switch (currentState) {
				case BY_NAME:
				{

					for each (var i:AnnotationGroupByName in annotationsByName) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					break;
				}

				case BY_PAGE:
				{
					for each (i in annotationsByPage) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					break;
				}

				case BY_STATUS:
				{
					for each (i in annotationsByStatus) {
						list.addAll(i.list);
					}
					selectedIndex = list.getItemIndex(selectedItem);
					break;
				}

				default:
				{
					break;
				}
			}

		}
	}
}
