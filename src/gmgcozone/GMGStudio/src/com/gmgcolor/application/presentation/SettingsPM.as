package com.gmgcolor.application.presentation {
import com.gmgcolor.application.event.AllowMultiplePanelsEvent;
import com.gmgcolor.application.model.SettingsModel;

import spark.components.List;
import spark.events.IndexChangeEvent;

public class SettingsPM {
		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var settingsModel:SettingsModel;

		public function onListChange(event:IndexChangeEvent, data:Object):void
		{
			// Inverse selection
			data.selected = !data.selected;
			switch (event.newIndex) {
				case 0:
				{
					dispatcher(new AllowMultiplePanelsEvent(AllowMultiplePanelsEvent.CHANGE, data.selected));
					break;
				}
				case 1:
				{
					settingsModel.showPins = data.selected;
					break;
				}
			}

			var list:List = List(event.currentTarget);
			list.dataProvider.setItemAt(data, event.newIndex); // reset data to force binding
			list.selectedIndex = -1; // removing selection
		}
	}
}
