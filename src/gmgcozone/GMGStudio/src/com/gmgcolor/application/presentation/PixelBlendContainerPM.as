package com.gmgcolor.application.presentation {
import com.gmgcolor.application.event.PixelBlendBitmapsEvent;

import flash.display.BitmapData;
import flash.display.Shader;
import flash.display.ShaderJob;
import flash.utils.ByteArray;

public class PixelBlendContainerPM {
		// Embed the shader
		[Embed(source='/assets/ImageDifferencer.pbj', mimeType='application/octet-stream')]
		private var ImageDifferencerShader:Class;
		private var myShader:Shader;
		private var _view:PixelBlendContainer;
		private var _color:Array = [1.0, 0.0, 1.0];
		private var _blend:Array = [1.0];
		private var _sensitivity:Array = [0.95];

		public function PixelBlendContainerPM()
		{
		}

		public function registerView(view:PixelBlendContainer):void
		{
			_view = view;
		}

		[MessageHandler(selector="render")]
		public function onRenderBitmaps(event:PixelBlendBitmapsEvent):void
		{
			// Set up the Shader.
			myShader = new Shader(new ImageDifferencerShader() as ByteArray);
			// Set original image as background
			var data:BitmapData = event.value[0];
			//data.applyFilter( data, data.rect, data.rect.topLeft, colorMatrix.filter );
			_view.bitmapOriginal.source = data;
			//_view.bitmapOriginal.filters = [ColorMatrixUtil.setContrast(-0.5), ColorMatrixUtil.setSaturation(0)]
			myShader.data.srcOne.input = event.value[0];
			myShader.data.srcTwo.input = event.value[1];
			// The 'sensitivity' parameter scales sensitivity of the shader (dah).
			myShader.data.sensitivity.value = _sensitivity;
			// The 'blend' parameter allows you to set the alpha level of the differenced image.
			myShader.data.blend.value = _blend;
			// The 'colour' parameter allows you to set RGB colour of the differenced image.
			myShader.data.colour.value = _color;
			// Display the differenced output.
			update();
		}

		[MessageHandler(selector="color")]
		public function setHexColour(event:PixelBlendBitmapsEvent):void
		{
			//_color = hexToRGB(c);
			_color = event.value;
		}

		[MessageHandler(selector="sensitivity")]
		public function setSensitivity(event:PixelBlendBitmapsEvent):void
		{
			_sensitivity = event.value;
		}

		[MessageHandler(selector="blend")]
		public function setBlend(event:PixelBlendBitmapsEvent):void
		{
			_blend = event.value;
		}

		protected function update():void
		{
			// Run the shader - This only needs to be called when changes
			// have been made to the shaders parameters or the source images.
			var bitmapData:BitmapData = new BitmapData(_view.bitmapDifference.width, _view.bitmapDifference.height, true, 0);
			_view.bitmapDifference.source = bitmapData;
			var sJob:ShaderJob = new ShaderJob(myShader, bitmapData);
			sJob.start(true);
		}
	}
}
