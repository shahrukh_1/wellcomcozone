package com.gmgcolor.application.presentation
{
import com.gmgcolor.application.domain.CommentsHeaderFO;
import com.gmgcolor.application.enums.ResourceBundle;
import com.gmgcolor.application.event.AllowMultiplePanelsEvent;
import com.gmgcolor.application.event.ShowRightPanelContainerEvent;
import com.gmgcolor.application.pages.events.NavigationContainerEvent;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;
import mx.events.FlexMouseEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import spark.components.List;
import spark.components.ToggleButton;
import spark.events.IndexChangeEvent;

public class CommentsHeaderPM
	{
		
		[Bindable]
		public var commentTypes:ArrayCollection;
		
		private var _currentState:String = "normal";
		
		[Bindable]
		public var toggleSelected:Boolean;
		
		
		[Bindable]
		public var lstSelectedIndex:int = -1;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		/**
		 * 
		 */
		[MessageDispatcher] public var dispatcher:Function;
		
		
		public function CommentsHeaderPM()
		{
			commentTypes = expandedCollection;
			
		}
		
		public function get normalCollection():ArrayCollection
		{
			
			var commentTypes:ArrayCollection = new ArrayCollection();
			commentTypes.addItem(new CommentsHeaderFO(false,_resourceManager.getString(ResourceBundle.LABELS,"rightpanel.settings.lblAllowMultiplePanels")));
			commentTypes.addItem(new CommentsHeaderFO(false,_resourceManager.getString(ResourceBundle.LABELS,"rightpanel.settings.lblClosePanel")));
			return commentTypes;
		}
		
		public function get expandedCollection():ArrayCollection
		{
			var commentTypes:ArrayCollection = new ArrayCollection();
			commentTypes.addItem(new CommentsHeaderFO(true,_resourceManager.getString(ResourceBundle.LABELS,"rightpanel.settings.lblAllowMultiplePanels")));
			commentTypes.addItem(new CommentsHeaderFO(true,_resourceManager.getString(ResourceBundle.LABELS,"rightpanel.settings.lblUsers")));
			commentTypes.addItem(new CommentsHeaderFO(true,_resourceManager.getString(ResourceBundle.LABELS,"rightpanel.settings.lblAnnotations")));
			commentTypes.addItem(new CommentsHeaderFO(false,_resourceManager.getString(ResourceBundle.LABELS,"rightpanel.settings.lblClosePanel")));
			return commentTypes;
		}
		
		public function dd_changeHandler(event:IndexChangeEvent):void
		{
			currentState = 'normal';
			
			var list:List = List(event.target);
			
			var commentsHeader:CommentsHeaderFO = CommentsHeaderFO(list.selectedItem);
			

			switch(event.newIndex)
			{
				case 0:
				{
					commentsHeader.data = commentsHeader.data ? false : true ;
					
					var isSelected: Boolean = commentsHeader.data;
					
					
					if(isSelected)
						commentTypes = expandedCollection;
					else
						commentTypes = normalCollection;
					
					commentsHeader = CommentsHeaderFO(commentTypes.getItemAt(0));
					commentsHeader.data = isSelected;
					
					dispatcher(new AllowMultiplePanelsEvent(AllowMultiplePanelsEvent.CHANGE,isSelected));
					
					break;
				}
				case 1:
				{
					commentsHeader.data = commentsHeader.data ? false : true ;
					dispatcher(new ShowRightPanelContainerEvent(RightPanelPM.CONTAINER_COLLABORATIONS, commentsHeader.data));
					break;
				}
				case 2:
				{
					commentsHeader.data = commentsHeader.data ? false : true ;
					dispatcher(new ShowRightPanelContainerEvent(RightPanelPM.CONTAINER_ANNOTATIONS, commentsHeader.data));
					break;
				}
				case 3:
				{
					dispatcher(new NavigationContainerEvent(NavigationContainerEvent.PANEL_TOGGLE_BUTTON_DESELECTED));
				break;
				}
					
				default:
				{
					break;
				}
			}
			
		}
		
		public function vgroup1_mouseDownOutsideHandler(event:FlexMouseEvent):void
		{
			// TODO Auto-generated method stub
			if(event.relatedObject is ToggleButton)
				return;
			currentState = 'normal';
		}
		
		public function injectionComplete():void
		{
			
		}	
		
		public function state1_enterStateHandler(event:FlexEvent):void
		{
			// TODO Auto-generated method stub
			lstSelectedIndex = -1;
		}


		[Bindable]
		public function get currentState():String
		{
			return _currentState;
		}

		public function set currentState(value:String):void
		{
			_currentState = value;
			if(value == 'normal')
				toggleSelected = false;
			else
				toggleSelected = true;
		}

		
	}
}
