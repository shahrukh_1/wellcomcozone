package com.gmgcolor.application.presentation {
import com.gmgcolor.application.event.AnnotationToolsEvent;
import com.gmgcolor.application.event.NavigationEvent;

public class NavigationContainerPM {

		[Bindable]
		public var currentState:String = "navigation";

		[MessageDispatcher]
		public var dispatcher:Function;


		public function NavigationContainerPM()
		{
		}

		[MessageHandler(selector="annotateButtonClick")]
		public function onAnnotateBtnClick(event:NavigationEvent):void
		{
			currentState = "annotationTools";
		}


		[MessageHandler(selector="done")]
		public function onAnnotateCancelBtnClick(event:AnnotationToolsEvent):void
		{
			currentState = "navigation";
		}
	}
}
