package com.gmgcolor.application.presentation {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.event.DocumentEvent;
import com.gmgcolor.annotations.event.DocumentSyncEvent;
import com.gmgcolor.annotations.event.NewCommentEvent;
import com.gmgcolor.annotations.event.PositionSepWindowEvent;
import com.gmgcolor.annotations.event.SideBarAnnotationSelectEvent;
import com.gmgcolor.annotations.model.AnnotationModel;
import com.gmgcolor.annotations.presentation.DocumentItemRenderer;
import com.gmgcolor.annotations.presentation.DocumentPM;
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.domain.ApprovalPage;
import com.gmgcolor.application.enums.ApplicationEnum;
import com.gmgcolor.application.event.AnnotationsChangeEvent;
import com.gmgcolor.application.event.PixelBlendBitmapsEvent;
import com.gmgcolor.application.model.CollaboratorModel;
import com.gmgcolor.application.pages.events.DocumentControlsEvent;
import com.gmgcolor.application.pages.events.NavigationContainerEvent;
import com.gmgcolor.common.util.Dispatcher;

import flash.display.ColorCorrection;
import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Point;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.containers.BoxDirection;
import mx.events.SandboxMouseEvent;
import mx.managers.CursorManager;
import mx.managers.CursorManagerPriority;

import org.spicefactory.parsley.core.context.Context;

import spark.components.ToggleButton;

public class ApplicationContainerPM extends Dispatcher {

		[Bindable]
		[Embed(source="Assets.swf", symbol="mx.skins.cursor.HBoxDivider")]
		private var horizontalDividerCursor:Class;

		[Bindable]
		[Embed(source="Assets.swf", symbol="mx.skins.cursor.VBoxDivider")]
		private var verticalDividerCursor:Class;

		[Bindable]
		public var thumbnailsHeight:Number = 0;

		[Bindable]
		public var documentState:String = "horizontal";

		[Bindable]
		public var documentsCollectionView:ArrayCollection;

		[Bindable]
		public var panelWidth:Number = 0;

		[Bindable]
		public var hSliderValue:Number;

		[Bindable]
		public var pageSizeTypes:ArrayCollection;

		[Bindable]
		public var labelDropDownList:String;

		[Bindable]
		public var selectedIndexDropDownList:int;

		[Inject]
		public var securityManager:SecurityManager;

		[Inject]
		public var context:Context;

		[Inject]
		public var applicationModel:ApplicationModel;

		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var collaboratorModel:CollaboratorModel;

		private var _view:ApplicationContainer;
		public var direction:String = BoxDirection.VERTICAL;
		private var dragStartPosition:Number;
		private var dragDelta:Number;
		private var _state:String;
		private var thumbnailsHeightStart:Number;
		private var cursorID:int = CursorManager.NO_CURSOR;
		private var dragging:Boolean;
		private var _selectedDocument:DocumentPM;

		public function ApplicationContainerPM():void
		{
		}

		[MessageHandler(scope="global")]
		public function onPositionSepWindowEvent(event:PositionSepWindowEvent):void
		{
			var x:Number = 0;
			var y:Number = 0;


			x = _view.stage.stageWidth - panelWidth - 200 - 20;
			y = 65;

			event.sepsPopUpMenu.move(x, y);

		}

		[Init]
		public function onApplicationReady():void
		{
			approvalModel.addEventListener(ApprovalModel.PAGES_CHANGE_EVENT, onPagesChange);
			approvalModel.addEventListener(ApprovalModel.CURRENT_APPROVAL_CHANGE_EVENT, onCurrentApprovalChange);
			approvalModel.addEventListener(ApprovalModel.GLOBAL_APPROVAL_CHANGE_EVENT, onGlobalApprovalChange);

			if (this.approvalModel && approvalModel.globalApproval) {
				onGlobalApprovalChange();
			}
		}

		private function onGlobalApprovalChange(e:Event = null):void
		{
			if (this.approvalModel && approvalModel.globalApproval) {
				if (approvalModel.documents) {
					documentsCollectionView = approvalModel.documents;
				}

				for each (var a:Approval in approvalModel.globalApproval.Versions) {
					if (a.ID == approvalModel.globalApproval.ID) {
						approvalModel.currentApproval = a;
					}
				}

				approvalModel.currentApprovalVersions = new ArrayCollection(approvalModel.globalApproval.Versions);

				var timer:Timer = new Timer(100, 1);
				timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
				timer.start();
			}
		}

		[MessageHandler(scope="global")]
		public function onSideBarAnnotationClick(event:SideBarAnnotationSelectEvent):void
		{
			var annotations:ArrayCollection = new ArrayCollection(approvalModel.currentApproval.Annotations);
			if (approvalModel.currentPage.PageNumber == event.annotation.PageNumber) {
				if (annotations.contains(event.annotation))
					selectedDocument.annotationModel.selectedAnnotation = event.annotation;
			}
			else {
				approvalModel.selectedAnnotationOnANewPage = event.annotation
				approvalModel.gotoPage(approvalModel.getPage(event.annotation.PageNumber));
			}
		}

		private function onTimerComplete(event:TimerEvent):void
		{
			approvalModel.setPageCollection([approvalModel.currentApproval]);
		}

		private function onCurrentApprovalChange(event:Event):void
		{
			collaboratorModel.currentCollaborators = new ArrayCollection(approvalModel.currentApproval.Collaborators);
		}

		public function onPagesChange(event:Event):void
		{
			documentsCollectionView = approvalModel.documents;
		}

		public function onToggleBtnClick(event:MouseEvent):void
		{
			var toggleButton:ToggleButton = ToggleButton(event.currentTarget);
			if (toggleButton.selected)
				dispatcher(new NavigationContainerEvent(NavigationContainerEvent.PANEL_TOGGLE_BUTTON_SELECTED));
			else
				dispatcher(new NavigationContainerEvent(NavigationContainerEvent.PANEL_TOGGLE_BUTTON_DESELECTED));
		}


		public function registerView(view:ApplicationContainer):void
		{
			this._view = view;

			_view.stage.colorCorrection = ColorCorrection.ON;
		}

		private function onZoomSliderChange(event:DocumentControlsEvent):void
		{
			dispatcher(event);
		}

		[Bindable]
		public function get selectedDocument():DocumentPM
		{
			return _selectedDocument;
		}

		public function set selectedDocument(value:DocumentPM):void
		{
			_selectedDocument = value;
			_selectedDocument.annotationModel.addEventListener(AnnotationModel.ANNOTATIONS_CHANGE_EVENT, onSelectedDocumentAnnotationsChange);
			approvalModel.currentPage = value.page;
			dispatcher(new AnnotationsChangeEvent(AnnotationsChangeEvent.START, selectedDocument.annotationModel.annotations));
		}

		private function onSelectedDocumentAnnotationsChange(event:Event):void
		{
			dispatcher(new AnnotationsChangeEvent(AnnotationsChangeEvent.CHANGE, selectedDocument.annotationModel.annotations));
		}

		[MessageHandler]
		public function onPostComment(event:NewCommentEvent):void
		{
			if (panelWidth == 0)
				panelWidth = ApplicationEnum.DEFAULT_PANEL_WIDTH;
		}

		[MessageHandler(selector="singlePageButtonClick")]
		public function onSinglePageButtonClick(event:DocumentControlsEvent):void
		{
			thumbnailsHeight = thumbnailsHeight > 0 ? 0 : ApplicationEnum.DEFAULT_THUMBNAILS_HEIGHT;
		}

		[MessageHandler(selector="horizontalTileButtonClick")]
		public function onHorizontalTileButtonClick(event:DocumentControlsEvent):void
		{
			_view.removeEventListener(Event.ENTER_FRAME, pixelCompare);
			documentState = "horizontal";
		}

		[MessageHandler(selector="verticalTileButtonClick")]
		public function onVerticalTileButtonClick(event:DocumentControlsEvent):void
		{
			_view.removeEventListener(Event.ENTER_FRAME, pixelCompare);
			documentState = "vertical";
		}

		[MessageHandler(selector="pixelCompareButtonClick")]
		public function onPixelCompareButtonClick(event:DocumentControlsEvent):void
		{
			documentState = "pixelCompare";
			_view.addEventListener(Event.ENTER_FRAME, pixelCompare);
		}

		private function pixelCompare(e:Event = null):void
		{
			if(this._view.documentList.dataGroup.numElements >= 2) {
				// Get bitmapdata of last 2 item renderers to compare
				var bitmaps:Array = [], child:DocumentItemRenderer;
				for (var i:uint = 0; i < 2; i++) {
					child = this._view.documentList.dataGroup.getElementAt(i) as DocumentItemRenderer;
					bitmaps.unshift(child.image.imageViewer.getBitmapData());
				}
				dispatcher(new PixelBlendBitmapsEvent(PixelBlendBitmapsEvent.RENDER_BITMAPS, bitmaps));
			}
		}

		[MessageHandler(selector="panelToggleButtonSelected")]
		public function onPanelToggleButtonSelected(event:NavigationContainerEvent):void
		{
			panelWidth = ApplicationEnum.DEFAULT_PANEL_WIDTH;
		}

		[MessageHandler(selector="panelToggleButtonDeselected")]
		public function onPanelToggleButtonDeselected(event:NavigationContainerEvent):void
		{
			panelWidth = 0;
		}

		[MessageHandler(selector="selected")]
		public function onDocumentSelectedEvent(event:DocumentEvent):void
		{
			selectedDocument = event.value;
		}

		[Bindable("stateChange")]
		public function get currentState():String
		{
			return this._state;
		}

		private function set state(value:String):void
		{
			this._state = value;
			dispatchEvent(new Event("stateChange"));
		}

		private var dividerContainer:DisplayObject;

		public function selectDocument(data:ApprovalPage):void
		{
			approvalModel.selectDocument(data);
		}

		public function onHorizontalDividerMouseDown(event:MouseEvent):void
		{
			dividerContainer = event.currentTarget.parent;
			startDividerDrag(event);
			var sbRoot:DisplayObject = _view.systemManager.getSandboxRoot();
			sbRoot.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, true);
			sbRoot.addEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE, mouseUpHandler);
		}

		public function startDividerDrag(trigger:MouseEvent):void
		{
			dragging = true;
			dragStartPosition = getMousePosition(trigger);
			dragDelta = 0;
			thumbnailsHeightStart = thumbnailsHeight;
			_view.systemManager.getSandboxRoot().addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
			_view.systemManager.deployMouseShields(true);
		}

		public function stopDividerDrag(trigger:MouseEvent):void
		{

			dragging = false;
			if (trigger)
				dragDelta = getMousePosition(trigger) - dragStartPosition;
			_view.systemManager.getSandboxRoot().removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
			_view.systemManager.deployMouseShields(false);
		}

		private function mouseMoveHandler(event:MouseEvent):void
		{
			dragDelta = getMousePosition(event) - dragStartPosition;
			thumbnailsHeight = thumbnailsHeightStart - dragDelta;
			if (thumbnailsHeight < ApplicationEnum.DEFAULT_THUMBNAILS_HEIGHT)
				thumbnailsHeight = ApplicationEnum.DEFAULT_THUMBNAILS_HEIGHT;
		}

		/**
		 *  @private
		 */
		private function getMousePosition(event:MouseEvent):Number
		{
			var point:Point = new Point(event.stageX, event.stageY);
			point = dividerContainer.globalToLocal(point);
			return isVertical() ? point.y : point.x;
		}

		private function isVertical():Boolean
		{
			return direction != BoxDirection.HORIZONTAL;
		}

		private function mouseUpHandler(event:Event):void
		{
			stopDividerDrag(event as MouseEvent);

			var sbRoot:DisplayObject = _view.systemManager.getSandboxRoot();
			sbRoot.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, true);
			sbRoot.removeEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE, mouseUpHandler);

			restoreCursor();
		}


		public function mouseOverHandler(event:MouseEvent):void
		{
			changeCursor();
		}

		/**
		 *  @private
		 */
		public function mouseOutHandler(event:MouseEvent):void
		{
			if (!dragging)
				restoreCursor();
		}

		private function changeCursor():void
		{
			if (cursorID == CursorManager.NO_CURSOR) {
				// If a cursor skin has been set for the specified BoxDivider,
				// use it. Otherwise, use the cursor skin for the DividedBox.
				var cursorClass:Class = isVertical() ?
						verticalDividerCursor as Class :
						horizontalDividerCursor as Class;

				cursorID = _view.cursorManager.setCursor(cursorClass,
						CursorManagerPriority.HIGH, 0, 0);
			}
		}

		private function restoreCursor():void
		{
			if (cursorID != CursorManager.NO_CURSOR) {
				_view.cursorManager.removeCursor(cursorID);
				cursorID = CursorManager.NO_CURSOR;
			}
		}


	}
}
