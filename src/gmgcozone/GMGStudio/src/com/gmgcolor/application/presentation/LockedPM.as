package com.gmgcolor.application.presentation {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.event.PageChangeEvent;
import com.gmgcolor.annotations.model.DocumentModel;
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.IsLockedChangeEvent;

import flash.events.MouseEvent;

import spark.components.ToggleButton;

public class LockedPM {


		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var securityManager:SecurityManager;

		[Inject]
		public var applicationModel:ApplicationModel;

		[Inject]
		public var documentModel:DocumentModel;

		private var _showLock:Boolean;
		private var _view:LockedContainer;


		[Bindable]
		public function get showLockBtn():Boolean
		{
			return _showLock;
		}

		public function set showLockBtn(value:Boolean):void
		{
			_showLock = value;
		}

		[MessageDispatcher]
		public var dispatcher:Function;

		public function registerView(view:LockedContainer):void
		{
			_view = view;
			_view.btn.visible = showLockBtn
		}


		public function LockedPM()
		{

		}


		[MessageHandler]
		public function onPageChange(event:PageChangeEvent):void
		{
			var approval:Approval = approvalModel.getApprovalFromPage(event.page.Approval);

			showLockBtn = documentModel.isUserLocked = approval.IsLocked

			if (_view && _view.btn)
				_view.btn.visible = approval.IsLocked;

			//dispatcher(new IsLockedChangeEvent(IsLockedChangeEvent.CHANGE, showLockBtn));
		}

		public function btnCompareModeNavigate_clickHandler(event:MouseEvent):void
		{
			if (securityManager.accountUserInfo.UserRole == 3 || securityManager.accountUserInfo.UserRole == 4) {
				approvalModel.currentApproval.IsLocked = documentModel.isUserLocked = !ToggleButton(event.target).selected;
				dispatcher(new IsLockedChangeEvent(IsLockedChangeEvent.CHANGE, documentModel.isUserLocked));
			}
		}
	}
}
