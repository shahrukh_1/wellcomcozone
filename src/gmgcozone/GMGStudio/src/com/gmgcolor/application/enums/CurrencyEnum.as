package com.gmgcolor.application.enums
{
	public class CurrencyEnum
	{
		// The possible enums for this class
		static public const AUD:CurrencyEnum = new CurrencyEnum('AUD', '$');
		static public const GBP:CurrencyEnum = new CurrencyEnum('GBP', '£');
		static public const JPY:CurrencyEnum = new CurrencyEnum('JPY', '¥');
		static public const HKD:CurrencyEnum = new CurrencyEnum('HKD', 'HK$');
		static public const EUR:CurrencyEnum = new CurrencyEnum('EUR', '€');
		static public const USD:CurrencyEnum = new CurrencyEnum('USD', 'US$');
		
		// A list of all the enums for easy referencing if needed
		static public const list:Array = [AUD, GBP, JPY, HKD, EUR, USD];
		
		// The static initializer needed to stop anyone else to an instance of this class
		static private var INIT:Boolean = init();
		
		// Class vars
		private var _isoCode:String;
		private var _symbol:String;
		
		// Class constructor
		public function CurrencyEnum(code:String, symbol:String)
		{
			if(INIT)
			{
				throw new Error('CurrencyEnum enums already created');
			}
			
			this._isoCode = code;
			this._symbol = symbol;
		}
		
		// Returns the ISO Code of the currency
		public function get isoCode():String
		{
			return this._isoCode;
		}
		
		// Returns the symbol for the currency
		public function get symbol():String
		{
			return this._symbol;
		}
		
		// Returns a string when needed
		public function toString():String
		{
			return this._isoCode;
		}
		
		// Static initializer function
		static private function init():Boolean
		{
			return true;
		}
	}
}