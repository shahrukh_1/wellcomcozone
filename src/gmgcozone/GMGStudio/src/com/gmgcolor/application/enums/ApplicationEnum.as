package com.gmgcolor.application.enums
{
	public class ApplicationEnum
	{
		static public const DEFAULT_COMMENT_WIDTH:Number = 270;
		static public const DEFAULT_THUMBNAILS_HEIGHT:Number = 125;
		static public const DEFAULT_PANEL_WIDTH:Number = 300;
		
		static public const DEFAULT_COMMENTS_PADDING:Number = 20;
		
		[Embed('/assets/images/icons/approvedwithchanges.png')]
		public static const ICON_APPROVED_WITH_CHANGES:Class;
		
		[Embed('/assets/images/icons/approved2.png')]
		public static const ICON_APPROVED:Class;
		
		[Embed('/assets/images/icons/pending.png')]
		public static const ICON_PENDING:Class;
		
		[Embed('/assets/images/icons/changesrequired.png')]
		public static const ICON_CHANGES_REQUIRED:Class;
		
	}
}