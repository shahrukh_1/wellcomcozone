
package com.gmgcolor.application.enums
{
	/**
	 * Class to get Resource Bundle names.
	 */
	public class ResourceBundle
	{
		public static const LABELS:String = "labels";
	}
}