package com.gmgcolor.application.enums {
	public class LanguageEnum {
		// The possible enums for this class
		static public const ENGLISH:LanguageEnum = new LanguageEnum('English', 'en_US', 'en-US');
		static public const GERMAN:LanguageEnum = new LanguageEnum('German', 'de_DE', 'de-DE');
		static public const SPANISH:LanguageEnum = new LanguageEnum('Español', 'es_ES', 'es-ES');
		static public const FRENCH:LanguageEnum = new LanguageEnum('Français', 'fr_FR', 'fr-FR');
		static public const ITALIAN:LanguageEnum = new LanguageEnum('Italiano', 'it_IT', 'it-IT');
		static public const JAPANESE:LanguageEnum = new LanguageEnum('日本語', 'ja_JP', 'ja-JP');
		static public const KOREAN:LanguageEnum = new LanguageEnum('한국의', 'ko_KR', 'ko-KR');
		static public const PORTUGESE:LanguageEnum = new LanguageEnum('Português', 'pt_PT', 'pt-PT');
		static public const CANTONESE:LanguageEnum = new LanguageEnum('粤语', 'zh_CN', 'zh-HANS');
		static public const MANDARIN:LanguageEnum = new LanguageEnum('國語', 'zh_TW', 'zh-HANT');

		// A list of all the enums for easy referencing if needed
		static public const AVAILABLE_LANGUAGES:Array = [ENGLISH, GERMAN, SPANISH, FRENCH, ITALIAN, JAPANESE, KOREAN, PORTUGESE, CANTONESE, MANDARIN];

		// The static initializer needed to stop anyone else to an instance of this class
		static private var INIT:Boolean = init();

		// Class vars
		private var _name:String;
		private var _locale:String;
		private var _mappedLocale:String;

		// Class constructor
		public function LanguageEnum(name:String, locale:String, mappedLocale:String)
		{
			if (INIT) {
				throw new Error('LanguageEnum enums already created');
			}

			this._name = name;
			this._locale = locale;
			this._mappedLocale = mappedLocale;
		}

		// Returns the true weight of the class
		public function get name():String
		{
			return this._name;
		}

		// Returns the true weight of the class
		public function get locale():String
		{
			return this._locale;
		}

		// Returns the true weight of the class
		public function get mappedLocale():String
		{
			return this._mappedLocale;
		}

		// Returns a string when needed
		public function toString():String
		{
			return this._name;
		}

		// Static initializer function
		static private function init():Boolean
		{
			return true;
		}
	}
}