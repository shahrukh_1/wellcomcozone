package com.gmgcolor.application.service
{
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.domain.ErrorFO;

import flash.utils.ByteArray;

import mx.rpc.AsyncToken;

public interface IApplicationService
	{
		
		function GetPreDefinedShapes(sessionId:String):AsyncToken
			
		function GetRoles(sessionId:String, localeName:String):AsyncToken
			
		function GetApproval(sessionId:String, approvalId:int):AsyncToken
		
		function GetCommentTypes(sessionId:String, localeName:String):AsyncToken
		
		function GetApprovalPages(sessionId:String, approvalId:int):AsyncToken
		
		function GetAnnotationStatuses(sessionId:String):AsyncToken
			
		function GetSession():AsyncToken
			
		function Login(sessionId:String, userId:int):AsyncToken
		
		function GetAnnotationTypes(sessionId:String):AsyncToken
		
		function GetCollaboratorGroups(sessionId:String):AsyncToken
		
		function GetCollaborators(sessionId:String):AsyncToken
		
		function GetApprovalCollaboratorGroups( sessionId:String, approvalId:int):AsyncToken
		
		function GetApprovalCollaborators( sessionId:String, approvalId:int):AsyncToken
		
		function GetApprovalCollaboratorsByCollaborator( sessionId:String, collaboratorId:int):AsyncToken
		
		function GetDecisions(sessionId:String, localeName:String):AsyncToken
		
		function GetApprovalDecisionHistorys( sessionId:String, approvalId:int):AsyncToken
		
		function GetAnnotations( sessionId:String, approvalId:int):AsyncToken
			
		function AddApprovalAnnotation( sessionId:String, annotation:ApprovalAnnotationFO):AsyncToken
			
		function UpdateApprovalAnnoation( sessionId:String, annotation:ApprovalAnnotationFO):AsyncToken
			
		function DeleteApprovalAnnotation( sessionId:String, annotationId:int):AsyncToken
			
		function UpdateApproval( sessionId:String, approval:Approval):AsyncToken
			
		function UpdateApprovalAnnotationStatus( sessionId:String, approvalId:int, statusId:int):AsyncToken
			
		function UpdateApprovalUserDecision(sessionId:String,  approvalId:int,  collaboratorId:int,  decisionId:int):AsyncToken
			
		function GeneratePrintOutput( sessionId:String, approvalId:int):AsyncToken
			
		function GetUserAndAccountInfo(sessionId:String, userId:int, isExternal:Boolean, approvalId:int):AsyncToken

		function RecordError(errorFO:ErrorFO):AsyncToken
			
		function UsePrePressFunctionality():AsyncToken

		function AddNewVersion(sessionId:String, approvalId:int):AsyncToken
			
		function DownloadOriginal(sessionId:String, approvalId:int):AsyncToken
			
		function PrintAnnotation(sessionId:String, approvalId:int, versionId:int):AsyncToken
			
		function GetApprovalCollaboratorRoles(sessionId:String, localeName:String):AsyncToken

		function GetHighlightTypes(sessionId:String, localeName:String):AsyncToken

		function GetTimeFormats(sessionId:String, localeName:String):AsyncToken

        function UploadRecordedSWF(sessionId:String, approvalId:int, data:ByteArray):AsyncToken

        function GetSoftProofingStatus(sessionId:String):AsyncToken

}
}
