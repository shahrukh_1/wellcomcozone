package com.gmgcolor.application.service {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.annotations.domain.ErrorFO;

import flash.utils.ByteArray;

import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

[InjectConstructor]
	public class ApplicationService implements IApplicationService {
		protected var _remote:RemoteObject;

		public function ApplicationService(remote:RemoteObject) {
			this._remote = remote;
		}

		public function GetPreDefinedShapes(sessionId:String):AsyncToken
		{
			return _remote.GetPreDefinedShapes(sessionId);
		}

		public function UsePrePressFunctionality():AsyncToken
		{
			return _remote.UsePrePressFunctionality();
		}

		public function GetSession():AsyncToken
		{
			return _remote.GetSession();
		}

		public function RecordError(errorFO:ErrorFO):AsyncToken
		{
			return _remote.RecordError(errorFO);
		}

		public function Login(sessionId:String, userId:int):AsyncToken
		{
			return _remote.Login(sessionId, userId);
		}

		public function GetUserAndAccountInfo(sessionId:String, userId:int, isExternal:Boolean, approvalId:int):AsyncToken
		{
			return _remote.GetUserAndAccountInfo(sessionId, userId, isExternal, approvalId);
		}

		public function GetRoles(sessionId:String, localeName:String):AsyncToken
		{
			return _remote.GetRoles(sessionId, localeName);
		}

		public function GetApproval(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GetApproval(sessionId, approvalId);
		}

		public function GetCommentTypes(sessionId:String, localeName:String):AsyncToken
		{
			return _remote.GetCommentTypes(sessionId, localeName);
		}

		public function GetApprovalPages(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GetApprovalPages(sessionId, approvalId);
		}

		public function GetAnnotationStatuses(sessionId:String):AsyncToken
		{
			return _remote.GetAnnotationStatuses(sessionId);
		}

		public function GetAnnotationTypes(sessionId:String):AsyncToken
		{
			return _remote.GetAnnotationTypes(sessionId);
		}

		public function GetCollaboratorGroups(sessionId:String):AsyncToken
		{
			return _remote.GetCollaboratorGroups(sessionId);
		}

		public function GetCollaborators(sessionId:String):AsyncToken
		{
			return _remote.GetCollaborators(sessionId);
		}

		public function GetApprovalCollaboratorGroups(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GetApprovalCollaboratorGroups(sessionId, approvalId);
		}

		public function GetApprovalCollaborators(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GetApprovalCollaborators(sessionId, approvalId);
		}

		public function GetApprovalCollaboratorsByCollaborator(sessionId:String, collaboratorId:int):AsyncToken
		{
			return _remote.GetApprovalCollaboratorsByCollaborator(sessionId, collaboratorId);
		}

		public function GetDecisions(sessionId:String, localeName:String):AsyncToken
		{
			return _remote.GetDecisions(sessionId, localeName);
		}

		public function AddNewVersion(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.AddNewVersion(sessionId, approvalId);
		}

		public function GetApprovalDecisionHistorys(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GetApprovalDecisionHistorys(sessionId, approvalId);
		}

		public function GetAnnotations(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GetAnnotations(sessionId, approvalId);
		}

		public function AddApprovalAnnotation(sessionId:String, annotation:ApprovalAnnotationFO):AsyncToken
		{
			return _remote.AddApprovalAnnotation(sessionId, annotation);
		}

		public function UpdateApprovalAnnoation(sessionId:String, annotation:ApprovalAnnotationFO):AsyncToken
		{
			return _remote.UpdateApprovalAnnotation(sessionId, annotation);
		}

		public function DeleteApprovalAnnotation(sessionId:String, annotationId:int):AsyncToken
		{
			return _remote.DeleteApprovalAnnotation(sessionId, annotationId);
		}

		public function UpdateApproval(sessionId:String, approval:Approval):AsyncToken
		{
			return _remote.UpdateApproval(sessionId, approval);
		}

		public function UpdateApprovalAnnotationStatus(sessionId:String, approvalId:int, statusId:int):AsyncToken
		{
			return _remote.UpdateApprovalAnnotationStatus(sessionId, String(approvalId), String(statusId));
		}

		public function UpdateApprovalUserDecision(sessionId:String, approvalId:int, collaboratorId:int, decisionId:int):AsyncToken
		{
			return _remote.UpdateApprovalUserDecision(sessionId, approvalId, collaboratorId, decisionId);
		}

		public function GeneratePrintOutput(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.GeneratePrintOutput(sessionId, approvalId);
		}

		public function DownloadOriginal(sessionId:String, approvalId:int):AsyncToken
		{
			return _remote.DownloadOriginal(sessionId, approvalId);
		}

		public function PrintAnnotation(sessionId:String, approvalId:int, versionId:int):AsyncToken
		{
			return _remote.PrintAnnotation(sessionId, approvalId, versionId);
		}

		public function GetApprovalCollaboratorRoles(sessionId:String, localeName:String):AsyncToken
		{
			return _remote.GetApprovalCollaboratorRoles(sessionId, localeName);
		}

		public function GetHighlightTypes(sessionId:String, localeName:String):AsyncToken
		{
			return _remote.GetHighlightTypes(sessionId, localeName);
		}

		public function GetTimeFormats(sessionId:String, localeName:String):AsyncToken
		{
			return _remote.GetTimeFormats(sessionId, localeName);
		}

        public function UploadRecordedSWF(sessionId:String, approvalId:int, data:ByteArray):AsyncToken
		{
			return _remote.UploadRecordedSWF(sessionId, approvalId, data);
		}

        public function GetSoftProofingStatus(sessionId:String):AsyncToken
        {
            return _remote.GetSoftProofingStatus(sessionId);
        }
}
}
