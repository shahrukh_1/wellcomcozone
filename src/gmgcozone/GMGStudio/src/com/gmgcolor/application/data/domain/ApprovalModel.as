package com.gmgcolor.application.data.domain {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.application.domain.ApprovalPage;

import flash.events.Event;
import flash.events.EventDispatcher;

import mx.collections.ArrayCollection;

[Event(name="pagesChange", type="flash.events.Event")]
	[Event(name="globalApprovalChange", type="flash.events.Event")]
	public class ApprovalModel extends EventDispatcher {

		public static const CURRENT_APPROVAL_PAGES_CHANGE_EVENT:String = "currentApprovalPagesChange";
		public static const CURRENT_APPROVAL_PAGE_CHANGE_EVENT:String = "currentApprovalPageChange";
		public static const CURRENT_APPROVAL_VERSIONS_CHANGE_EVENT:String = "currentApprovalVersionChange";
		public static const PAGES_CHANGE_EVENT:String = "pagesChange";
		public static const GLOBAL_APPROVAL_CHANGE_EVENT:String = "globalApprovalChange";
		public static const CURRENT_APPROVAL_CHANGE_EVENT:String = "currentApprovalChange";
		public static const CURRENT_PAGE_INDEX_CHANGE_EVENT:String = "currentPageIndexChange";


		private var _currentApproval:Approval;

		private var _currentApprovalId:int;

		private var _documents:ArrayCollection;

		private var _currentApprovalPages:ArrayCollection;

		private var _currentApprovalVersions:ArrayCollection;

		private var _currentPage:ApprovalPage;

		private var _currentPageIndex:int;

		private var _annotations:ArrayCollection;

		private var _selectedAnnotationOnANewPage:ApprovalAnnotationFO;

		private var _xPosOnANewPage:Number;

		private var _yPosOnANewPage:Number;

		private var _zoomOnANewPage:Number;

		private var _currentZoomScale:Number;

		private var _currentZoomX:Number;

		private var _currentZoomY:Number;

		private var _rotateOnANewPage:Number;

		private var _globalApproval:Approval;

		private var _selectedDocumentIndex:int = 0;

		private var _indexOnANewPage:Number;


		public function ApprovalModel()
		{

		}
        [Bindable]
		public function get selectedDocumentIndex():int
		{
			return _selectedDocumentIndex;
		}

		public function set selectedDocumentIndex(value:int):void
		{
			_selectedDocumentIndex = value;
		}

		public function get currentZoomScale():Number
		{
			return _currentZoomScale;
		}

		public function set currentZoomScale(value:Number):void
		{
			_currentZoomScale = value;
		}

		public function get currentZoomX():Number
		{
			return _currentZoomX;
		}

		public function set currentZoomX(value:Number):void
		{
			_currentZoomX = value;
		}

		public function get currentZoomY():Number
		{
			return _currentZoomY;
		}

		public function set currentZoomY(value:Number):void
		{
			_currentZoomY = value;
		}

		public function getPage(page:int):ApprovalPage
		{
			for each (var approvalPage:ApprovalPage in currentApprovalPages) {
				if (page == approvalPage.PageNumber)
					return approvalPage;
			}
			return null;
		}

		public function getApprovalFromPage(approvalID:int):Approval
		{
			for each (var approval:Approval in currentApprovalVersions) {
				if (approval.ID == approvalID)
					return approval;
			}
			return null;
		}

		public function get documents():ArrayCollection
		{
			return _documents;
		}

		public function set documents(value:ArrayCollection):void
		{
			if (_documents != value) {
				_documents = value;
				dispatchEvent(new Event(PAGES_CHANGE_EVENT));
			}
		}

		public function gotoPage(page:ApprovalPage):void
		{
			for each (var document:ApprovalPage in documents) {
				document.isMainViewSelected = false;
			}
			page.isMainViewSelected = true;
			documents.setItemAt(page, selectedDocumentIndex);
			currentPage = page;
		}

		public function selectDocument(data:ApprovalPage):void
		{
			if (data && currentPage != data) {
				selectedDocumentIndex = documents.getItemIndex(data);
				currentPage = data;

				for each (var page:ApprovalPage in documents) {
					page.isMainViewSelected = page == data;
					documents.itemUpdated(page);
				}

				var approvalId:int = data.Approval;
				var versions:Array = globalApproval.Versions;
				if (versions) {
					for each (var approval:Approval in versions) {
						if (approval.ID == approvalId) {
							currentApproval = approval;
							break;
						}
					}
				}
			}
		}

		public function setPageCollection(versions:Array, pageIndexes:Array = null):void
		{
			if (!versions)
				return;

			var a:Array = [];
			var page:int;

			var approval:Approval;

			for (var i:uint = 0, len:uint = versions.length; i < len; i++) {
				approval = versions[i];
				if (approval && approval.Pages && approval.Pages.length != 0) {
                    for each(var approvalPage:ApprovalPage in approval.Pages) {
                        approvalPage.isMainViewSelected = false;
                    }
					if (!pageIndexes)
						page = 0;
					else
						page = i

					a.push(approval.Pages[page]);
				}
			}
			documents = new ArrayCollection(a.reverse());
			selectedDocumentIndex = a.length - 1;
            if(versions.length && versions[0].Pages.length) {
                gotoPage(versions[0].Pages[0]);
            }
		}

		[Bindable(event="currentApprovalChange")]
		public function get original():String
		{
			return currentApproval ? currentApproval.Title : '';
		}

		[Bindable]
		public function get currentApprovalPages():ArrayCollection
		{
			return _currentApprovalPages;
		}

		public function set currentApprovalPages(value:ArrayCollection):void
		{
			if (_currentApprovalPages != value) {
				if (value && currentPage)
					currentPageIndex = value.getItemIndex(currentPage);
				_currentApprovalPages = value;
				dispatchEvent(new Event(CURRENT_APPROVAL_PAGES_CHANGE_EVENT));
			}
		}


		[Bindable]
		public function get currentApprovalVersions():ArrayCollection
		{
			return _currentApprovalVersions;
		}

		public function set currentApprovalVersions(value:ArrayCollection):void
		{
			if (_currentApprovalVersions != value) {

				_currentApprovalVersions = value;
				dispatchEvent(new Event(CURRENT_APPROVAL_VERSIONS_CHANGE_EVENT));
			}
		}

		[Bindable]
		public function get currentPage():ApprovalPage
		{
			return _currentPage;
		}

		public function set currentPage(value:ApprovalPage):void
		{
			if (value != _currentPage) {
				currentPageIndex = currentApprovalPages.getItemIndex(value);
				_currentPage = value;
				dispatchEvent(new Event(CURRENT_APPROVAL_PAGE_CHANGE_EVENT));
			}
		}

		[Bindable]
		public function get currentPageIndex():int
		{
			return _currentPageIndex;
		}

		public function set currentPageIndex(value:int):void
		{
			if (value != _currentPageIndex && value >= 0 && value < currentApprovalPages.length) {
				_currentPageIndex = value;
				currentPage = currentApprovalPages.getItemAt(value) as ApprovalPage;
				dispatchEvent(new Event(CURRENT_PAGE_INDEX_CHANGE_EVENT));
			}
		}

		public function get currentAnnotations():ArrayCollection
		{
			return _annotations;
		}

		public function set currentAnnotations(value:ArrayCollection):void
		{
			_annotations = value;
		}

		[Bindable]
		public function get currentApproval():Approval
		{
			return _currentApproval;
		}

		public function set currentApproval(value:Approval):void
		{
			if (value != _currentApproval && (_currentApproval ? value.ID != _currentApproval.ID : true)) {
				currentApprovalPages = new ArrayCollection(value.Pages);
				_currentApproval = value;
				var pages:Array = value.Pages;
				var a:ArrayCollection = new ArrayCollection;

				for each (var approvalPage:ApprovalPage in pages) {
					a.addAll(new ArrayCollection(approvalPage.Annotations));
				}
				currentAnnotations = a;

				dispatchEvent(new Event(CURRENT_APPROVAL_CHANGE_EVENT));
			}
		}

		[Bindable]
		public function get currentApprovalId():int
		{
			return _currentApprovalId;
		}

		public function set currentApprovalId(value:int):void
		{
			_currentApprovalId = value;
		}


		public function get selectedAnnotationOnANewPage():ApprovalAnnotationFO
		{
			return _selectedAnnotationOnANewPage;
		}

		public function set selectedAnnotationOnANewPage(value:ApprovalAnnotationFO):void
		{
			_selectedAnnotationOnANewPage = value;
		}


		[Bindable(event="globalApprovalChange")]
		public function get globalApproval():Approval
		{
			return _globalApproval;
		}

		public function set globalApproval(value:Approval):void
		{
			if (_globalApproval != value) {
				_globalApproval = value;

				//_globalApproval.Annotations = new ArrayCollection;

				var p:ApprovalPage

				for each (var approval:Approval in _globalApproval.Versions) {
					if (!approval.Annotations)
						approval.Annotations = []

					for each (p in approval.Pages) {
						p.highlightTextFilename = p.PageFolder + "TextDescriptor.xml";

						for each (var a:ApprovalAnnotationFO in p.Annotations) {
							approval.Annotations.push(a);
						}
					}
				}

				for each (p in _globalApproval.Pages) {
					p.highlightTextFilename = p.PageFolder + "TextDescriptor.xml";
				}

				dispatchEvent(new Event(GLOBAL_APPROVAL_CHANGE_EVENT));
			}
		}


		public function get xPosOnANewPage():Number
		{
			return _xPosOnANewPage;
		}

		public function set xPosOnANewPage(value:Number):void
		{
			_xPosOnANewPage = value;
		}

		public function get yPosOnANewPage():Number
		{
			return _yPosOnANewPage;
		}

		public function set yPosOnANewPage(value:Number):void
		{
			_yPosOnANewPage = value;
		}

		public function get zoomOnANewPage():Number
		{
			return _zoomOnANewPage;
		}

		public function set zoomOnANewPage(value:Number):void
		{
			_zoomOnANewPage = value;
		}


		public function get rotateOnANewPage():Number
		{
			return _rotateOnANewPage;
		}

		public function set rotateOnANewPage(value:Number):void
		{
			_rotateOnANewPage = value;
		}

		public function get indexOnANewPage():Number
		{
			return _indexOnANewPage;
		}

		public function set indexOnANewPage(value:Number):void
		{
			_indexOnANewPage = value;
		}
	}
}
