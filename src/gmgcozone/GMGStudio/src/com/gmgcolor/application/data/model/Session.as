package com.gmgcolor.application.data.model
{
import mx.utils.StringUtil;

public class Session
	{
		private static const SERVICE_TICKET:String = "ServiceTicket";
		private static const JSESSIONID:String = "JSESSIONID";
		private var _jsessionId:String;
		private var _serviceTicket:String;
		
		public function get jsessionId():String
		{
			return _jsessionId;
		}
		public function get serviceTicket():String
		{
			return _serviceTicket;
		}
		public function Session(casResponse:String)
		{
			parseToken(casResponse);
		}
		
		public function get hasServiceTicket():Boolean
		{
			return serviceTicket != null;
		}
		private function parseToken(casResponse:String):void
		{
			try
			{
				var tokens:Array = casResponse.split("\n");
				var response:Object = new Object();
				for each (var token:String in tokens)
				{
					var keyValuePair:Array = token.split("=");
					response[keyValuePair[0]] = StringUtil.trim(keyValuePair[1] as String)
				}
				this._jsessionId = response[JSESSIONID];
				if (response.hasOwnProperty(SERVICE_TICKET))
					this._serviceTicket = response[SERVICE_TICKET];
			} catch (e:Error)
			{
			}
		}
	}
}
