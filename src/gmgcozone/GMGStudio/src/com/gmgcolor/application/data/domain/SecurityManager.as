package com.gmgcolor.application.data.domain {
import com.gmgcolor.annotations.domain.AccountUserInfo;
import com.gmgcolor.common.util.Dispatcher;
import com.gmgcolor.global.presenters.GlobalPM;

import flash.events.Event;

[Event(name="accountChange", type="flash.events.Event")]
	public class SecurityManager extends Dispatcher {
		public static const ACCOUNT_CHANGE:String = "accountChange";

		[Inject]
		public var globalPM:GlobalPM;

		[Bindable]
		public var sessionId:String;

		private var _userloggedin:Boolean;

		private var _accountUserInfo:AccountUserInfo;


		[Bindable("accountChange")]
		public function get isLoggedIn():Boolean
		{
			return _userloggedin;
		}

		[Publish]
		[Bindable("accountChange")]
		public function get accountUserInfo():AccountUserInfo
		{
			return _accountUserInfo;
		}

		public function logUserIn(accountUserInfo:AccountUserInfo):void
		{
			_userloggedin = true;
			_accountUserInfo = accountUserInfo;
			dispatchEvent(new Event(ACCOUNT_CHANGE))
		}
	}
}
