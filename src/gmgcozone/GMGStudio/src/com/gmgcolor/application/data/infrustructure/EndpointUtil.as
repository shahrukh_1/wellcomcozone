package com.gmgcolor.application.data.infrustructure
{
import mx.utils.UIDUtil;

public class EndpointUtil
	{
		/**
		 * Appends a unique identifier to the provided url.
		 * When urls are common across multiple endpoints, calls to these
		 * endpoints become subject to batching.  ie - All calls sent to this url
		 * within a frame may be batched at the flash Players discretion.
		 * 
		 * This means that all calls within the batch must have responded before
		 * the response is sent back to the client, allowing a long-running call
		 * to delay the response of quicker calls.
		 * 
		 * Using unique urls prevents this behaviour
		 * */
		public static function generateNonBatchingUrl(url:String):String
		{
			return url + "?a=" + UIDUtil.createUID();
		}
	}
}
