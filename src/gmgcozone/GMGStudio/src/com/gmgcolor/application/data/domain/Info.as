package com.gmgcolor.application.data.domain
{
	[Bindable]
	public class Info
	{
		public var id:String;
		public var name:String;
	}
}