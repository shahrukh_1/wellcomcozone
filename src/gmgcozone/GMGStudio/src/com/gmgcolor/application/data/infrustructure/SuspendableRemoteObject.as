package com.gmgcolor.application.data.infrustructure
{
import flash.events.Event;
import flash.utils.Dictionary;

import mx.core.mx_internal;
import mx.rpc.AbstractOperation;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.rpc.remoting.mxml.RemoteObject;

use namespace mx_internal;
	
	/**
	 * An extension to the RemoteObject, which supports 
	 * suspending calls.
	 * When remoteCallsSuspended = true, any calls to a remote service
	 * are queued up, and sent to the back-end service when remoteCallsSuspended is set to false.
	 * 
	 * While suspended, AsyncTokens are still returned from calls, so consumers of the class
	 * are unaware that the call has been suspended.
	 * */
	public class SuspendableRemoteObject extends RemoteObject
	{
		public static const ALL_ACTIVE_CALLS_COMPLETED:String = "allActiveCallsCompleted";
		
		
		private var _remoteCallsSuspended:Boolean;
		private var operationManagers:Dictionary = new Dictionary(); // of Operation,OperationManager
		
		public function SuspendableRemoteObject():void
		{
			addEventListener(FaultEvent.FAULT,onRemoteCallCompleted);
			addEventListener(ResultEvent.RESULT,onRemoteCallCompleted);
		}
		
		private function onRemoteCallCompleted(event:Event):void
		{
			if (hasEventListener(ALL_ACTIVE_CALLS_COMPLETED))
			{
				if (!hasActiveCalls())
					dispatchEvent(new Event(ALL_ACTIVE_CALLS_COMPLETED));
			}
		}
		
		public function get remoteCallsSuspended():Boolean
		{
			return _remoteCallsSuspended;
		}
		
		override public function getOperation(name:String):AbstractOperation
		{
			var operation:AbstractOperation = super.getOperation(name);
			if (operationManagers[operation] == null)
			{
				var operationManager:OperationManager = new OperationManager(operation,this);
				operationManagers[operation] = operationManager;
				operation.operationManager = operationManager.doManagedSend;
			}
			return operation;
		}
		
		public function suspend():void
		{
			_remoteCallsSuspended = true;
		}
		public function resume():void
		{
			_remoteCallsSuspended = false;
			sendQueuedCalls();
		}
		[MessageHandler]
		public function onSuspendRemoteCalls(message:SuspendRemoteCallsEvent):void
		{
			suspend();
		}
		[MessageHandler]
		public function onResumeRemoteCalls(message:ResumeRemoteCallsEvent):void
		{
			resume();
		}
		
		private function sendQueuedCalls():void
		{
			for each (var operationManager:OperationManager in operationManagers)
			{
				operationManager.sendQueuedCalls();
			}
		}
		
		public function hasActiveCalls():Boolean
		{
			for each ( var operation:AbstractOperation in operations)
			{
				if (operation.activeCalls.hasActiveCalls())
					return true;
			}
			return false;
		}
	}
}

