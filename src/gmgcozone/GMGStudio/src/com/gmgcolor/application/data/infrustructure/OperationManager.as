package com.gmgcolor.application.data.infrustructure
{
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;

public class OperationManager
	{
		private var operation:AbstractOperation;
		private var remoteObject:SuspendableRemoteObject;
		private var queuedTokens:Array = [];
		
		public function OperationManager(operation:AbstractOperation,remoteObject:SuspendableRemoteObject)
		{
			this.operation = operation;
			this.remoteObject = remoteObject;
		}
		
		public function doManagedSend(args:Array=null):AsyncToken
		{
			if (remoteObject.remoteCallsSuspended)
			{
				var suspendedToken:SuspendedAsyncToken = buildNewAsyncToken(args);
				queueCall(suspendedToken);
				return suspendedToken;
			} else {
				operation.operationManager = null;
				var token:AsyncToken = operation.send.apply(null,args);
				operation.operationManager = doManagedSend;
				return token;
			}
		}
		
		private function queueCall(token:SuspendedAsyncToken):void
		{
			queuedTokens.push(token);		
		}
		
		private function buildNewAsyncToken(args:Array):SuspendedAsyncToken
		{
			var token:SuspendedAsyncToken = new SuspendedAsyncToken(args);
			return token;
		}
		
		public function sendQueuedCalls():void
		{
			while (queuedTokens.length > 0)
			{
				var queuedToken:SuspendedAsyncToken = queuedTokens.shift();
				sendQueuedCall(queuedToken);
			}
		}
		
		private function sendQueuedCall(queuedToken:SuspendedAsyncToken):void
		{
			operation.operationManager = null;
			var realToken:AsyncToken = operation.send.apply(null,queuedToken.args);
			queuedToken.setDelegateToken(realToken);
			operation.operationManager = this.doManagedSend;
		}
	}
}

import mx.rpc.AsyncToken;
import mx.rpc.IResponder;

class SuspendedAsyncToken extends AsyncToken
{
	private var delegate:AsyncToken;
	private var _args:Array;
	public function SuspendedAsyncToken(args:Array)
	{
		this._args = args;
	}
	
	
	public function get args():Array
	{
		return _args;
	}
	
	public function setDelegateToken(token:AsyncToken):void
	{
		this.delegate = token;
		for each (var responder:IResponder in this.responders)
		{
			token.addResponder(responder);
		}
	}
	
	override public function addResponder(responder:IResponder):void
	{
		if (delegate)
		{
			delegate.addResponder(responder)
		} else {
			super.addResponder(responder);
		}
	}
}
