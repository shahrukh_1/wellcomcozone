package com.gmgcolor.application.data.infrustructure
{
import com.gmgcolor.common.data.domain.APIMeta;

import mx.rpc.events.FaultEvent;
import mx.utils.ObjectUtil;

/**
	 * An extension to the SuspendableRemoteObject, which supports 
	 * authenticating calls.
	 * 
	 * When sessionToken is populated from an authenticated account, any calls to a 
	 * remote service have the session token set in APIMeta.
	 * 
	 * All Remote Object declarations must use AuthenticatableRemoteObject to ensure
	 * calls over non-secure channels do not expose the session token.
	 * 
	 * */
	public class AuthenticatableRemoteObject extends SuspendableRemoteObject
	{
		
		private var _sessionToken:String;
		
		public function AuthenticatableRemoteObject()
		{
			super();
			this.convertParametersHandler = convertParameterHandler;
			addEventListener(FaultEvent.FAULT,onFault);
		}

		private function onFault(event:FaultEvent):void
		{
			
		}
		
		public function set sessionToken(value:String):void
		{
			_sessionToken = value;
		}
		
		private function convertParameterHandler(params:Array):Array
		{
			if ( params == null || params.length == 0 ) return params;
			
			var apiMeta:APIMeta = params[0] as APIMeta;
			if ( apiMeta == null ) return params;
			
			// We clone APIMeta to ensure subsequent calls
			// do not reset the sessionToken to null on the
			// singleton APIMeta object before the secure
			// call is sent.
			var clone:APIMeta = ObjectUtil.clone(apiMeta) as APIMeta; 
			clone.usernamePasswordToken = _sessionToken;
			
			params[0] = clone;
			
			
			return params;
			return [];
		}
	}
}
