package com.gmgcolor.application.data.domain
{
import com.gmgcolor.account.domain.AccountUserModel;
import com.gmgcolor.annotations.presentation.MarkupContainer;

import flash.events.EventDispatcher;

import mx.collections.ArrayCollection;

public class ApplicationModel extends EventDispatcher
	{
		[Inject]public var accountModel:AccountUserModel;
		[Inject]public var securityManager:SecurityManager;		
		
		[MessageDispatcher]public var dispatcher:Function;
		

		public var buildNumber:String = "DEV";
		
		public var buildTime:String = "";
		
		private var fonts:ArrayCollection;
		
		private var _selectedApprovalsView:MarkupContainer;
		
		[Bindable]
		public var annotationTypes:ArrayCollection;
		
		[Bindable]
		public var annotationStatuses:ArrayCollection;
		
		[Bindable]
		public var decisions:ArrayCollection;
		
		[Bindable]
		public var commentTypes:ArrayCollection;
		
		[Bindable]
		public var approvalCollaboratorRoles:ArrayCollection;
		
		[Bindable]
		public var roles:ArrayCollection;
		
		[Bindable]
		public var predefinedShapes:ArrayCollection;

		public function ApplicationModel()
		{
			fonts = new ArrayCollection();
		
		}

		public function get selectedApprovalsView():MarkupContainer
		{
			return _selectedApprovalsView;
		}
		
		[Publish]
		public function set selectedApprovalsView(value:MarkupContainer):void
		{
			_selectedApprovalsView = value;
		}

	}
}
