package com.gmgcolor.application.data.domain
{
	[Bindable]
	[RemoteClass(alias="GMGColor.FlexObjects.SoftProofingSessionFO")]
	public class SoftProofingStatus
	{
		
		private var _Enabled:Boolean;
		private var _MonitorName:String;
		private var _Level:Number;

        public function get Enabled():Boolean {
            return _Enabled;
        }

        public function set Enabled(value:Boolean):void {
            _Enabled = value;
        }

        public function get MonitorName():String {
            return _MonitorName;
        }

        public function set MonitorName(value:String):void {
            _MonitorName = value;
        }

        public function get Level():Number {
            return _Level;
        }

        public function set Level(value:Number):void {
            _Level = value;
        }
    }
		
}