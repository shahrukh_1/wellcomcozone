package com.gmgcolor.skins
{
import flash.events.Event;

public class FilterIndexChangeEvent extends Event
	{
		public static const FILTER_CHANGE:String = "filterChange";

		private var _oldIndex:int;
		private var _newIndex:int;

		public function FilterIndexChangeEvent(type:String, oldIndex:int=-1, newIndex:int=-1)
		{
			super(type,false,false);

			this._oldIndex = oldIndex;
			this._newIndex = newIndex;
		}

		public function get oldIndex():int
		{
			return _oldIndex;
		}

		public function get newIndex():int
		{
			return _newIndex;
		}

		public override function clone():Event
		{
			return new FilterIndexChangeEvent(type,oldIndex,newIndex);
		}

		public override function toString():String
		{
			return formatToString("FilterIndexChangeEvent","oldIndex","newIndex");
		}
	}
}
