package com.gmgcolor.skins
{
import spark.components.ButtonBarButton;
import spark.components.DropDownList;
import spark.events.IndexChangeEvent;

[Event(name="filterChange", type="com.gmgcolor.skins.FilterIndexChangeEvent")]
	public class AccordionsButtonBar extends ButtonBarButton
	{
		
		[SkinPart(required="true")]
		public var dropDownList:DropDownList;
		
		private var _selectedIndex:int;
		
		private var _filterIndex:int;
		
		[Bindable]
		public function get filterIndex():int
		{
			return _filterIndex;
		}
		
		public function set filterIndex(value:int):void
		{
			_filterIndex = value;
			dropDownList.selectedIndex = value
			selectedIndex = value;
		}
		
		
		public function AccordionsButtonBar()
		{
			super();
			mouseChildren = true;
		}
		
		/**
		 *  @private
		 */
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			
			if (instance == dropDownList)
			{
				dropDownList.addEventListener(IndexChangeEvent.CHANGE,onChange);
			}
		}

		private function onChange(event:IndexChangeEvent):void
		{
			dispatchEvent(new FilterIndexChangeEvent(FilterIndexChangeEvent.FILTER_CHANGE,event.oldIndex,event.newIndex));
			selectedIndex = event.newIndex;
		}

		[Bindable]
		public function get selectedIndex():int
		{
			return _selectedIndex;
		}

		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;
			filterIndex = value;
		}

	}
}
