package com.gmgcolor.skins
{
	public class AnnotationToolsEnum
	{
		public function AnnotationToolsEnum()
		{
		}
		
		[Embed('/assets/graphics/icons/pincomment.png')]
		public static const ICON_PIN_COMMENT:Class;
		
		[Embed('/assets/graphics/icons/areacomment.png')]
		public static const ICON_AREA_COMMENT:Class;
		
		[Embed('/assets/graphics/icons/textcomment.png')]
		public static const ICON_TEXT_COMMENT:Class;
		
		[Embed('/assets/images/icons/pincomment_blk.png')]
		public static const ICON_BLACK_PIN_COMMENT:Class;
		
		[Embed('/assets/images/icons/areacomment_blk.png')]
		public static const ICON_BLACK_AREA_COMMENT:Class;
		
		[Embed('/assets/images/icons/textcomment_blk.png')]
		public static const ICON_BLACK_TEXT_COMMENT:Class;
		
		[Embed('/assets/images/icons/drawcomment_blk.png')]
		public static const ICON_BLACK_DRAW_COMMENT:Class;
		
		[Embed('/assets/images/icons/textinputcomment_blk.png')]
		public static const ICON_BLACK_TEXT_INPUT_COMMENT:Class;
		
		[Embed('/assets/images/icons/status_approved.png')]
		public static const ICON_STATUS_APPROVED:Class;
		
		[Embed('/assets/images/icons/status_declined.png')]
		public static const ICON_STATUS_DECLINED:Class;
		
		[Embed('/assets/images/icons/status_nuetral.png')]
		public static const ICON_STATUS_NUETRAL:Class;
		
		[Embed('/assets/images/icons/status_arrow.png')]
		public static const ICON_STATUS_ARROW:Class;
		
	}
}