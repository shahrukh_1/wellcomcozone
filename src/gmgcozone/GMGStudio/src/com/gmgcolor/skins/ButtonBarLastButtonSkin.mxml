<?xml version="1.0" encoding="utf-8"?>
<!--

  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

-->


<!--- The default skin class for the last button in a Spark ButtonBar component.

 @see spark.components.ButtonBar
 @see spark.components.ButtonBarButton

 @langversion 3.0
 @playerversion Flash 10
 @playerversion AIR 1.5
 @productversion Flex 4
-->
<s:SparkButtonSkin xmlns:fx="http://ns.adobe.com/mxml/2009"
                   xmlns:s="library://ns.adobe.com/flex/spark"
                   xmlns:fb="http://ns.adobe.com/flashbuilder/2009"
                   minWidth="21" minHeight="21"
                   alpha.disabled="0.5">

	<!-- host component -->
	<fx:Metadata>
        <![CDATA[
		/**
		 * @copy spark.skins.spark.ApplicationSkin#hostComponent
		 */
		[HostComponent("spark.components.ButtonBarButton")]
		]]>
    </fx:Metadata>

	<fx:Script fb:purpose="styling">
		/* Define the skin elements that should not be colorized.
		 For toggle button, the graphics are colorized but the label is not. */
		static private const exclusions:Array = ["labelDisplay"];

		/**
		 *  @private
		 */
		override public function get colorizeExclusions():Array {return exclusions;}

		/**
		 *  @private
		 */
		override protected function initializationComplete():void
		{
			useChromeColor = true;
			super.initializationComplete();
		}

		/**
		 *  @private
		 */
		override protected function updateDisplayList(unscaledWidth:Number, unscaleHeight:Number):void
		{
			var cr:Number = getStyle("cornerRadius");

			if (cornerRadius != cr)
			{
				cornerRadius = cr;
				fill.topRightRadiusX = cornerRadius;
				fill.bottomRightRadiusX = cornerRadius;
				highlightStroke.topRightRadiusX = cornerRadius;
				highlightStroke.bottomRightRadiusX = cornerRadius;
				border.topRightRadiusX = cornerRadius;
				border.bottomRightRadiusX = cornerRadius;

				if(hldownstroke1) {
					hldownstroke1.topRightRadiusX = cornerRadius;
					hldownstroke1.bottomRightRadiusX = cornerRadius;
				}
			}

			super.updateDisplayList(unscaledWidth, unscaledHeight);
		}

		private var cornerRadius:Number = 2;
	</fx:Script>

	<!-- states -->
	<s:states>
		<s:State name="up" />
		<s:State name="over" stateGroups="overStates" />
		<s:State name="down" stateGroups="downStates" />
		<s:State name="disabled" stateGroups="disabledStates" />
		<s:State name="upAndSelected" stateGroups="selectedStates, selectedUpStates" />
		<s:State name="overAndSelected" stateGroups="overStates, selectedStates" />
		<s:State name="downAndSelected" stateGroups="downStates, selectedStates" />
		<s:State name="disabledAndSelected" stateGroups="selectedUpStates, disabledStates, selectedStates" />
	</s:states>

	<s:Group left="0" right="-1" top="-1" bottom="-1">

		<!-- layer 2: fill -->
		<!--- @private -->
		<s:Rect id="fill" left="1" right="2" top="2" bottom="2" topRightRadiusX="2" bottomRightRadiusX="2">
			<s:fill>
				<s:LinearGradient rotation="90">
					<s:GradientEntry color="0xFFFFFF"
					                 color.over="0xF2F2F2"
					                 color.downStates="0xE6E6E6"
					                 color.overAndSelected="0xE6E6E6"
					                 color.selectedUpStates="0xE4E4E4"
					                 ratio="0"
					                 alpha="1" />
					<s:GradientEntry color="0xE6E6E6"
					                 color.over="0xE4E4E4"
					                 color.overAndSelected="0xE6E6E6"
					                 color.downStates="0xE6E6E6"
					                 color.selectedUpStates="0xE6E6E6"
					                 ratio="1"
					                 alpha="1" />
				</s:LinearGradient>
			</s:fill>
		</s:Rect>

		<!-- layer 5: highlight stroke (all states except down) -->
		<s:Rect id="highlightStroke" left="0" right="0" top="1" bottom="0" topRightRadiusX="2" bottomRightRadiusX="2" excludeFrom="downStates">
			<s:stroke>
				<s:LinearGradientStroke rotation="90" weight="1">
					<s:GradientEntry color="0xFFFFFF" alpha="0.25" />
				</s:LinearGradientStroke>
			</s:stroke>
		</s:Rect>

		<s:Rect id="hldownstroke1" left="1" right="2" top="2" bottom="2" topRightRadiusX="2"  bottomRightRadiusX="2"
		        includeIn="downStates, selectedUpStates, overAndSelected">
			<s:filters>
				<s:DropShadowFilter
						blurX="10"
						blurY="10"
						alpha=".15"
						inner="true"
						distance="1"
						color="#000000"
						angle="90"
						knockout="true"/>
			</s:filters>
			<s:fill>
				<s:SolidColor alpha="1" color="#F5F5F5"/>
			</s:fill>
		</s:Rect>

		<!-- layer 7: border - put on top of the fill so it doesn't disappear when scale is less than 1 -->
		<!--- @private -->
		<s:Rect id="border" left="0" right="1" top="1" bottom="1" topRightRadiusX="2" bottomRightRadiusX="2">
			<s:stroke>
				<s:LinearGradientStroke rotation="90" weight="1">
					<s:GradientEntry color="0x000000"
					                 alpha="0.15" />
					<s:GradientEntry color="0x000000"
					                 alpha="0.25"  />
				</s:LinearGradientStroke>
			</s:stroke>
		</s:Rect>
	</s:Group>

	<!-- layer 8: text -->
	<!---  @copy spark.components.supportClasses.ButtonBase#labelDisplay -->
	<s:Label id="labelDisplay"
	         textAlign="center"
	         verticalAlign="middle"
	         maxDisplayedLines="1"
	         horizontalCenter="0" verticalCenter="1"
	         left="10" right="10" top="2" bottom="2">
	</s:Label>

</s:SparkButtonSkin>