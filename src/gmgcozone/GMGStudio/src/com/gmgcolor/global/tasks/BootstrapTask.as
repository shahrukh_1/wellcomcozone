package com.gmgcolor.global.tasks
{
import flash.utils.Dictionary;

import org.spicefactory.parsley.core.context.Context;
import org.spicefactory.parsley.core.context.DynamicObject;

[Event(name="progress", type="flash.events.ProgressEvent")]
	// Need to extend ConcurrentTaskGroup as creating sequencial before inject causes bug
	public class BootstrapTask extends ConcurrentTaskGroup
	{
		
		private var _tasks:Dictionary = new Dictionary();
		private var _sequence:SequentialTaskGroup = new SequentialTaskGroup();
		
		[Inject] public var context:Context;
		
		override public function get data():*
		{
			return this._xmlTask?this._xmlTask.data:null;
		}
		
		public function BootstrapTask() 
		{
			super("BootstrapTask");
			ignoreChildErrors = false;
			setCancelable(false);
			setSkippable(false);
			setSuspendable(true);
			
		}
		
		override protected function doStart():void
		{
			//suspend();
			complete();
		}
		
		[Init]
		public function init():void
		{
			
			// Load bootstrap xml
			//add(this._xmlTask);
			// Parse and load swfs
			//(new PreloadTask());
			// Add the sequence
			//addTask(this._sequence);
			//resume();
			
			
		}
		
		[Destroy]
		public function destroy():void
		{
			this.removeAllTasks();
			this._sequence = null;
			this._xmlTask = null;
			for(var task:Object in this._tasks)
			{
				remove(Task(task));
			}
			this._tasks = null;
		}
		
		private function add(task:Task):void
		{
			this._tasks[task] = context.addDynamicObject(task);
			task.addEventListener(TaskEvent.COMPLETE, onTaskComplete);
			this._sequence.addTask(task);
		}
		
		private function onTaskComplete(e:TaskEvent):void
		{
			remove(Task(e.currentTarget));
		}
		
		private function remove(task:Task):void
		{
			task.removeEventListener(TaskEvent.COMPLETE, onTaskComplete);
			DynamicObject(this._tasks[task]).remove();
			delete this._tasks[task];
		}
	}
}
