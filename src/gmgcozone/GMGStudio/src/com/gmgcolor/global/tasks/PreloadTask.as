package com.gmgcolor.global.tasks
{
import flash.net.URLLoader;
import flash.utils.Dictionary;

import org.spicefactory.parsley.core.context.Context;
import org.spicefactory.parsley.core.context.DynamicObject;

[Event(name="progress", type="flash.events.ProgressEvent")]

	public class PreloadTask extends ConcurrentTaskGroup
	{
		
		private var _loader:URLLoader = new URLLoader();
		private var _tasks:Dictionary = new Dictionary();
		
		[Inject] public var context:Context;
		
		public function PreloadTask() 
		{
			super("PreloadTask");
			ignoreChildErrors = false;
			setCancelable(false);
			setSkippable(false);
			setSuspendable(false);
		}
		
		[Destroy]
		public function destroy():void
		{
			this.removeAllTasks();
			for(var task:Object in this._tasks)
			{
				remove(Task(task));
			}
			this._tasks = null;
		}
		
		override protected function doStart():void
		{
			var xml:XML = XML(parent.data);
			
			if(xml)
			{
				var child:XML;
				// Find style tags
				for each(child in xml.style)
				{
					if(child.@url != null)
					{
						//addTask(new LoadStyleTask(child.@url));
					}
				}
				
				// Find modules tag
				/*for each(child in xml.modules[0].module)
				{
					if(child.@url =! null && child.@key == navigation.activeNavigationRequest.nextNavigationDirective.currentNode)
					{
						addTask(new LoadModuleTask(child.@url));
					}
				}*/
			}
		}
		
		override public function addTask(task:Task):Boolean
		{
			// Adding to context to be aware and reflected
			this._tasks[task] = context.addDynamicObject(task); 
			task.addEventListener(TaskEvent.COMPLETE, onTaskComplete);
			return super.addTask(task);
		}
		
		private function onTaskComplete(e:TaskEvent):void 
		{
			remove(Task(e.currentTarget));
		}
		
		private function remove(task:Task):void
		{
			DynamicObject(this._tasks[task]).remove();
			delete this._tasks[task];
		}
	}
}
