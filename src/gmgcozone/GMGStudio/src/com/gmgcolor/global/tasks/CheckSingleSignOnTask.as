package com.gmgcolor.global.tasks
{
import com.gmgcolor.application.data.domain.SecurityManager;

import mx.rpc.events.FaultEvent;

public class CheckSingleSignOnTask extends Task
	{
		
		[MessageDispatcher] public var dispatcher:Function;
		
		[Inject]
		public var securityManager:SecurityManager;
		
		public function CheckSingleSignOnTask() 
		{
			super();
			setName("CheckSingleSignOnTask");
			setCancelable(true);
			setSuspendable(true);
			setSkippable(true);
			setTimeout(5000);
		}
		
		override protected function doStart():void
		{
			suspend();
		}
		
		[Init]
		public function init():void
		{
			resume();
			dispatcher(new CheckSingleSignOnMessage());
		}
		
		[CommandComplete]
		public function onComplete(message:CheckSingleSignOnMessage):void 
		{
			complete();
		}
		
		[CommandError]
		public function onError(fault:FaultEvent, message:CheckSingleSignOnMessage):void 
		{
			// Don't create fault because SSO is not required
			complete();
		}
		
		override protected function doTimeout():void
		{
			securityManager.performSSOCheckOnInitialization();
			skip();
		}
	}
}
