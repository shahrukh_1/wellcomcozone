package com.gmgcolor.global.tasks
{
import com.gmgcolor.global.messages.LoadStyle;

import mx.rpc.events.FaultEvent;

public class LoadStyleTask extends Task
	{
		private var _url:String;
		
		[MessageDispatcher] public var dispatcher:Function;
		
		public function LoadStyleTask(url:String) 
		{
			super();
			this._url = url;
			setCancelable(false);
			setSkippable(false);
			setSuspendable(false);
		}
		
		[Init]
		public function init():void
		{
			dispatcher(new LoadStyle(this._url));
		}
		
		[CommandComplete]
		public function onComplete(message:LoadStyle):void 
		{
			if(message.url == this._url)
			{
				complete();
			}
		}
		
		[CommandError]
		public function onError(fault:FaultEvent, message:LoadStyle):void 
		{
		}
	}
}
