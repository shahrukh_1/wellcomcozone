package com.gmgcolor.global.tasks
{
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;

[Event(name="progress", type="flash.events.ProgressEvent")]
	public class LoadFileTask extends Task
	{
		
		protected var _loader:URLLoader = new URLLoader();
		protected var _url:String;
		protected var _data:*;
		
		override public function get data():*
		{
			return this._data;
		}
		
		public function LoadFileTask(url:String) 
		{
			super();
			this._url = url;
			setName("LoadFileTask "+url);
			setCancelable(false);
			setSuspendable(false);
			setSkippable(false);	
			this._loader.dataFormat = URLLoaderDataFormat.TEXT;
		}
		
		override protected function doStart(): void 
		{
			complete();
	/*		this._loader.addEventListener(Event.COMPLETE, onComplete);
			this._loader.addEventListener(ProgressEvent.PROGRESS, dispatchEvent);
			this._loader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			this._loader.load(new URLRequest(this._url));*/
		}
		
		private function onComplete(e:Event):void 
		{
			this._data = this._loader.data;
			complete();
		}
		
		private function onError(e:IOErrorEvent):void 
		{
			this._loader.removeEventListener(Event.COMPLETE, onComplete);
			this._loader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
		}
		
		override protected function complete():Boolean
		{
			this._loader.removeEventListener(Event.COMPLETE, onComplete);
			this._loader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			this._loader.close();
			this._loader = null;
			return super.complete();
		}
	}
}
