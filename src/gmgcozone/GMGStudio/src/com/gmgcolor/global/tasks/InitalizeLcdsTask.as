package com.gmgcolor.global.tasks
{
import mx.rpc.events.FaultEvent;

/**
	 * Triggers intiailization of LCDS and DSid.
	 * See IntitizliaeLcdsCommand for more details
	 * */
	public class InitalizeLcdsTask extends Task
	{
		
		[MessageDispatcher] public var dispatcher:Function;
		
		public function InitalizeLcdsTask() 
		{
			super();
			setName("IntializeLcdsTask");
			setCancelable(false);
			setSuspendable(true);
			setSkippable(true);
			setTimeout(5000);
		}
		override protected function doStart():void
		{
			suspend();
		}
		
		[Init]
		public function init():void
		{
			resume();
			dispatcher(new InitializeLcdsMessage());
			// Complete straight away, allowing other tasks to continue.
			// We don't care about the result of the
			// call, just that it happened first.
			complete();
		}
		
		[CommandError]
		public function onError(fault:FaultEvent, message:CheckSingleSignOnMessage):void 
		{
			// Don't create fault.  This task is an optimization.  Failure only indicates
			// that further queuing may result, rather than a critical failure.
			skip();
		}
		
		override protected function doTimeout():void
		{
			skip();
		}
	}
}
