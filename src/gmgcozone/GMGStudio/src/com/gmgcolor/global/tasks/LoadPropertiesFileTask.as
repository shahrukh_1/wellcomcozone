package com.gmgcolor.global.tasks
{
import mx.resources.IResourceManager;
import mx.resources.ResourceBundle;
import mx.resources.ResourceManager;
import mx.utils.StringUtil;

public class LoadPropertiesFileTask extends LoadFileTask
	{
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		private var _name:String;
		private var _locale:String;
		
		public function LoadPropertiesFileTask(url:String, bundleName:String, locale:String) 
		{
			super(url);
			this._name = bundleName;
			this._locale = locale;
		}
		
		override protected function complete():Boolean
		{
			var data:Array = String(this._loader.data).split("\n");
			var bundle:ResourceBundle = new ResourceBundle(this._locale, this._name);
			
			var values:Array, key:String, value:String;
			for each(var line:String in data)
			{
				if(line.charAt(0) != '#')
				{
					values = line.split('=');
					if(values.length > 1)
					{
						bundle.content[StringUtil.trim(values.shift())] = StringUtil.trim(values.join('='));
					}
				}
			}
			
			// Add bundle to manager, update
			this._resourceManager.addResourceBundle(bundle);
			this._resourceManager.update();
			
			return super.complete();
		}
	}
}
