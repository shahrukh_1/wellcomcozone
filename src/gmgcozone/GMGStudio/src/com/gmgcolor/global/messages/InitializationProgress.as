package com.gmgcolor.global.messages
{
	public class InitializationProgress
	{
		private var _total:uint = 0;
		private var _completed:uint = 0;
		
		public function get completed():uint
		{
			return this._completed;
		}
		
		public function get total():uint
		{
			return this._total;
		}
		
		public function InitializationProgress(completed:uint, total:uint)
		{
			this._completed = completed;
			this._total = total;
		}
	}
}