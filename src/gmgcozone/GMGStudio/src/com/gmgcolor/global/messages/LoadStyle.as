package com.gmgcolor.global.messages
{
	public class LoadStyle
	{
		private var _url:String;
		
		public function get url():String
		{
			return this._url;
		}
		
		public function LoadStyle(url:String)
		{
			this._url = url;
		}
	}
}