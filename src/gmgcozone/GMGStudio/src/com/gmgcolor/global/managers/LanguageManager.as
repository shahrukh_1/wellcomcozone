package com.gmgcolor.global.managers {
import com.gmgcolor.application.enums.LanguageEnum;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

public class LanguageManager {
		private const _resourceManager:IResourceManager = ResourceManager.getInstance();
		private var _selectedLanguage:LanguageEnum = LanguageEnum.ENGLISH;

		[MessageDispatcher]
		public var dispatcher:Function;


		public function set selectedLanguage(lang:LanguageEnum):void
		{
			_selectedLanguage = lang;
			setChain();
		}

		public function get selectedLanguage():LanguageEnum
		{
			return this._selectedLanguage;
		}

		public function LanguageManager()
		{
		}

		public function selectLanguageByLocale(locale:String):void
		{
			for (var i:uint = 0, len:uint = LanguageEnum.AVAILABLE_LANGUAGES.length; i < len; i++) {
				if (locale == LanguageEnum.AVAILABLE_LANGUAGES[i].locale || locale == LanguageEnum.AVAILABLE_LANGUAGES[i].mappedLocale) {
					selectedLanguage = LanguageEnum.AVAILABLE_LANGUAGES[i];
					break;
				}
			}
		}

		private function setChain():void
		{
			var chain:Array = [this._selectedLanguage.locale];

			if (this._selectedLanguage != LanguageEnum.ENGLISH) {
				chain.push(LanguageEnum.ENGLISH.locale);
			}

			this._resourceManager.localeChain = chain;
		}
	}
}
