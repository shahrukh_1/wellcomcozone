package com.gmgcolor.global.preloader {
import flash.events.Event;
import flash.events.ProgressEvent;
import flash.net.SharedObject;

import mx.events.RSLEvent;
import mx.preloaders.SparkDownloadProgressBar;

public class PreLoader extends SparkDownloadProgressBar {
		private var _preloadDisplay:PreloaderDisplay;
		private var _rslLoaded:Number = 0;
		private var _applicationLoaded:Number = 0;
		private var _initializePercent:Number = 0;
		private var _initProgressCount:uint = 0;

		public function PreLoader()
		{
			super();
			this._preloadDisplay = new PreloaderDisplay();
			addChild(this._preloadDisplay);
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(Event.RESIZE, draw);

			// Store FlashVars
			var so:SharedObject = SharedObject.getLocal("parameters", "/");
			for (var prop:String in loaderInfo.parameters) {
				so.data[prop] = loaderInfo.parameters[prop];
			}

			so.flush();
			draw();
		}

		private function draw(e:Event = null):void
		{
			if (this._preloadDisplay) {
				this._preloadDisplay.draw();
			}
		}

		private function updateProgress():void
		{
			var percent:Number = (this._rslLoaded * 0.35) + (this._applicationLoaded * 0.35) + (this._initializePercent * 0.30);
			var handler:Function;
			if (percent == 1)
				handler = onComplete;
			this._preloadDisplay.setProgress(percent /** 0.5*/, handler);
		}

		// Progress handler for the application SWF
		override protected function progressHandler(e:ProgressEvent):void
		{
			this._applicationLoaded = e.bytesLoaded / e.bytesLoaded;
			updateProgress();
		}

		override protected function completeHandler(e:Event):void
		{
			this._applicationLoaded = 1;
			updateProgress();
		}

		// Progress handler for the RSLs
		override protected function rslProgressHandler(e:RSLEvent):void
		{
			this._rslLoaded = e.bytesLoaded / e.bytesLoaded;
			updateProgress();
		}

		override protected function rslCompleteHandler(e:RSLEvent):void
		{
			this._rslLoaded = 1;
			updateProgress();
		}

		override protected function rslErrorHandler(e:RSLEvent):void
		{
			try {
				setError("Unable to download Runtimes Shared Libraries: " + e.url.url);
			} catch (err:Error) {}
		}

		// Progress handler for application initialization
		override protected function initProgressHandler(e:Event):void
		{
			this._initProgressCount++;
			this._initializePercent = this._initProgressCount / initProgressTotal;
			updateProgress();
		}

		override protected function initCompleteHandler(e:Event):void
		{
			this._initializePercent = 1;
			updateProgress();
		}

		private function onComplete():void
		{
			stage.removeEventListener(Event.RESIZE, draw);
			dispatchEvent(new Event(Event.COMPLETE));
		}

		private function setError(text:String):void
		{
			this._preloadDisplay.showError();
			// TODO: log this to a server
		}
	}
}
