package com.gmgcolor.global.preloader {
import com.greensock.TweenNano;
import com.greensock.easing.Quad;

import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.filters.DropShadowFilter;
import flash.text.AntiAliasType;
import flash.text.TextField;
import flash.text.TextFormat;

public class PreloaderDisplay extends Sprite {
		[Embed(source='/assets/fonts/Arial.ttf', fontFamily="EmbedArial", fontName="EmbedArial", advancedAntiAliasing="true", fontWeight="bold", embedAsCFF="false", unicodeRange="U+0021-U+002f,U+003a-U+007e")]
		private const font:Class;

		[Embed(source='/assets/images/bg.jpg')]
		private var bgTile:Class;

		private const BAR_WIDTH:int = 200;
		private const BAR_HEIGHT:int = 4;

		private var _progressBarSprite:Sprite;
		private var _progressBarSpriteBackground:Sprite;
		private var _errorDisplay:Sprite;
		private var _errorText:TextField;

		public function PreloaderDisplay(initialProgress:Number = 0)
		{
			super();

			_progressBarSpriteBackground = new Sprite();
			_progressBarSpriteBackground.graphics.beginFill(0xFFFFFF, 0.5);
			_progressBarSpriteBackground.graphics.drawRect(0, 0, BAR_WIDTH, BAR_HEIGHT);
			_progressBarSpriteBackground.graphics.endFill();
			addChild(_progressBarSpriteBackground);

			_progressBarSprite = new Sprite();
			_progressBarSprite.graphics.beginFill(0x999999);
			_progressBarSprite.graphics.drawRect(0, 0, Math.max(1, initialProgress * BAR_WIDTH), BAR_HEIGHT);
			_progressBarSprite.graphics.endFill();
			addChild(_progressBarSprite);

			// Create error display
			this._errorDisplay = new Sprite();
			this._errorDisplay.visible = false;
			this._errorDisplay.alpha = 0;
			this._errorDisplay.filters = [new DropShadowFilter(1, 45, 0xFFFFFF, 0.2, 0, 0, 1, 1)];

			var sadface:TextField = new TextField();
			sadface.text = ":(";
			sadface.setTextFormat(new TextFormat('EmbedArial', 112, 0x999999, true));
			sadface.embedFonts = true;
			sadface.antiAliasType = AntiAliasType.ADVANCED;
			sadface.height = 200;
			sadface.width = 85;
			sadface.selectable = false;
			this._errorDisplay.addChild(sadface);
			var error:TextField = new TextField();
			error.text = "Oops!";
			error.setTextFormat(new TextFormat('EmbedArial', 42, 0x999999, true));
			error.embedFonts = true;
			error.antiAliasType = AntiAliasType.ADVANCED;
			error.selectable = false;
			error.width = 350;
			error.x = 100;
			error.y = 12;
			this._errorDisplay.addChild(error);
			this._errorText = new TextField();
			this._errorText.text = "An error has occured while loading.\nPlease confirm that your internet is active\nand refresh to continue."
			this._errorText.setTextFormat(new TextFormat('EmbedArial', 18, 0x999999, true));
			this._errorText.embedFonts = true;
			this._errorText.antiAliasType = AntiAliasType.ADVANCED;
			this._errorText.multiline = true;
			this._errorText.wordWrap = true;
			this._errorText.selectable = false;
			this._errorText.width = 350;
			this._errorText.x = 100;
			this._errorText.y = 65;
			this._errorDisplay.addChild(this._errorText);
			addChild(this._errorDisplay);
		}

		public function setProgress(percent:Number, completeHandler:Function = null):void
		{
			if (_progressBarSprite) {
				TweenNano.to(_progressBarSprite, 0.4, {width: 200 * percent, overwrite: true, ease: Quad.easeOut, onComplete: completeHandler});
			}
		}

		public function draw():void
		{
			if (stage) {
				// draw background
				var g:Graphics = this.graphics;
				var bmd:BitmapData = (new bgTile()).bitmapData;
				g.clear();
				g.beginBitmapFill(bmd);
				g.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
				g.endFill();

				_progressBarSpriteBackground.x = (stage.stageWidth - BAR_WIDTH) / 2;
				_progressBarSpriteBackground.y = ((stage.stageHeight - BAR_HEIGHT) / 2)

				_progressBarSprite.x = (stage.stageWidth - BAR_WIDTH) / 2;
				_progressBarSprite.y = ((stage.stageHeight - BAR_HEIGHT) / 2)
				// Center error text
				this._errorDisplay.x = (stage.stageWidth - this._errorDisplay.width) / 2;
				this._errorDisplay.y = (stage.stageHeight - this._errorDisplay.height) / 2;
			}
		}

		public function showError():void
		{
			this._errorDisplay.visible = true;
			draw();
			TweenNano.to(this._progressBarSprite, 0.5, {alpha:0});
			TweenNano.to(this._errorDisplay, 0.5, {alpha: 1});
		}
	}
}
