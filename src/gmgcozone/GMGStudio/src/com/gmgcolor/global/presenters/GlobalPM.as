package com.gmgcolor.global.presenters {
import com.gmgcolor.common.util.Dispatcher;
import com.gmgcolor.global.messages.InitializeApplicationMessage;

import flash.events.ContextMenuEvent;

import mx.rpc.Fault;
import mx.rpc.events.FaultEvent;

import org.spicefactory.lib.command.light.LightCommandAdapter;

public class GlobalPM extends Dispatcher {
		private var __loaded:Boolean = false;
		private var __initProgress:Number = 0;
		private var _menuItems:Array = [];
		private var _appLoaded:Boolean = false;
		private var _appConfigured:Boolean = false;
		private var _appInitialized:Boolean = false;


		public function get menuItems():Array
		{
			return _menuItems;
		}

		public function GlobalPM()
		{
		}

		[Init]
		public function init():void
		{
			LightCommandAdapter.addErrorType(Fault);
			LightCommandAdapter.addErrorType(FaultEvent);

			dispatcher(new InitializeApplicationMessage());
		}

		/*[MessageHandler]
		 public function onInitializeProgress(message:InitializationProgress):void
		 {
		 var progress:Number = message.completed / message.total;
		 this._loaded = progress == 1;
		 this._initProgress = progress;
		 }*/

		private function createMenuItems():void
		{
			/*if (ApplicationInfo.debug) {
			 var item:ContextMenuItem = new ContextMenuItem(ApplicationInfo.appName, false, true)
			 item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onMenuItemClicked);
			 menuItems.push(item);
			 menuItems.push(new ContextMenuItem("Build Time: " + ApplicationInfo.buildTime, false, false));
			 }*/
		}

		private function onMenuItemClicked(e:ContextMenuEvent):void
		{
			//System.setClipboard("Soft proof 1.0 Build #"+ApplicationInfo.buildNumber+" Built "+ApplicationInfo.buildTime.toString());
		}
	}
}
