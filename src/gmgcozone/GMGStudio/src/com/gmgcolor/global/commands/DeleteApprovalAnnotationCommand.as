package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.DeleteApprovalAnnotationEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.rpc.AsyncToken;
import mx.rpc.events.FaultEvent;

public class DeleteApprovalAnnotationCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		public function execute(event:DeleteApprovalAnnotationEvent):AsyncToken
		{			
			return service.DeleteApprovalAnnotation(event.sessionId, event.annotationId);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:FaultEvent):void
		{			
			
		}
	}
}
