package com.gmgcolor.global.commands {
import com.gmgcolor.annotations.domain.Approval;
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.service.IApplicationService;

import mx.core.FlexGlobals;
import mx.rpc.AsyncToken;

public class GetApprovalCommand extends AsyncTokenCommand {

		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var securityManager:SecurityManager;

		public function execute():AsyncToken
		{
			approvalModel.currentApprovalId = FlexGlobals.topLevelApplication.parameters.appid;
			return service.GetApproval(securityManager.sessionId, approvalModel.currentApprovalId);
		}

		public function result(result:Approval):Approval
		{
			approvalModel.globalApproval = result;
			return result;
		}
	}
}
