package com.gmgcolor.global.commands
{
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.event.GetApprovalPagesEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class GetApprovalPagesCommand
	{
		[Inject] 
		public var service:IApplicationService;
		
		[Inject]
		public var approvalModel:ApprovalModel;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:GetApprovalPagesEvent):AsyncToken
		{			
			return service.GetApprovalPages(event.sessionId,event.approvalId);
		}
		
		public function result(result:Object):void
		{
			approvalModel.currentApprovalPages = new ArrayCollection(result as Array);
		}
		
		public function error(fault:Fault):void
		{
			
		}
	}
}
