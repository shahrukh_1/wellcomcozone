package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.GetURLEvent;
import com.gmgcolor.application.service.IApplicationService;

import flash.net.URLRequest;
import flash.net.navigateToURL;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class GetURLCommand
	{

		[Inject] 
		public var service:IApplicationService;
		
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		

		public function execute(message:GetURLEvent):AsyncToken
		{			
			
			switch(message.type)
			{
				case GetURLEvent.ADD_NEW_VERSION:
				{
					return service.AddNewVersion(message.sessionId,message.approvalId);
					break;
				}
				case GetURLEvent.PRINT:
				{
					return service.PrintAnnotation(message.sessionId,message.approvalId,message.versionId);
					break;
				}
				case GetURLEvent.DOWNLOAD:
				{
					return service.DownloadOriginal(message.sessionId,message.approvalId);
					break;
				}
				default:
				{
					break;
				}
			}
			
			return null
		}

		public function result(result:Object):void
		{
		 
		 var urlRequest:URLRequest = new URLRequest(String(result));
		 navigateToURL(urlRequest, "_blank");
		 
		}
		
		public function error(fault:Fault):void
		{
			
		}
	}
}
