package com.gmgcolor.global.commands {
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.AddApprovalAnnotationEvent;
import com.gmgcolor.application.event.ApprovalAnnotationAddedEvent;
import com.gmgcolor.application.service.IApplicationService;
import com.gmgcolor.common.errornotifications.messages.ConsolidatedErrorMessage;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class AddApprovalAnnotationCommand {

		[Inject]
		public var service:IApplicationService;

		[MessageDispatcher]
		public var dispatcher:Function;

		[Inject]
		public var securityManager:SecurityManager;

		public var approvalAnnotationFO:ApprovalAnnotationFO;

		private var _resourceManager:IResourceManager = ResourceManager.getInstance();

		public function execute(event:AddApprovalAnnotationEvent):AsyncToken
		{
			approvalAnnotationFO = event.annotation;
			approvalAnnotationFO.IsExternalUser = securityManager.accountUserInfo.UserIsExternal;
			return service.AddApprovalAnnotation(event.sessionId, event.annotation);
		}

		public function result(result:Object):void
		{
			if (result is ApprovalAnnotationFO) {
				approvalAnnotationFO.ID = ApprovalAnnotationFO(result).ID;
				approvalAnnotationFO.ModifiedDate = ApprovalAnnotationFO(result).ModifiedDate;
				approvalAnnotationFO.AnnotatedDate = ApprovalAnnotationFO(result).AnnotatedDate;
				approvalAnnotationFO.Parent = ApprovalAnnotationFO(result).Parent;
				approvalAnnotationFO.PageNumber = ApprovalAnnotationFO(result).PageNumber;
			}
			else
				dispatcher(new ConsolidatedErrorMessage("ApprovalAnnotationFO not returned from AddApprovalAnnotation method."));

			dispatcher(new ApprovalAnnotationAddedEvent(ApprovalAnnotationAddedEvent.ADDED, approvalAnnotationFO));
		}

		public function error(fault:Fault):void
		{

		}
	}
}
