package com.gmgcolor.global.commands
{
    import com.gmgcolor.application.event.UploadRecordedSWFEvent;
    import com.gmgcolor.application.service.IApplicationService;

    import flash.net.URLRequest;
    import flash.net.navigateToURL;

    import mx.core.FlexGlobals;

    import mx.resources.IResourceManager;
    import mx.resources.ResourceManager;
    import mx.rpc.AsyncToken;
    import mx.rpc.Fault;

    public class UploadRecordedSWFCommand
    {
        [Inject]
        public var service:IApplicationService;


        private var _resourceManager:IResourceManager = ResourceManager.getInstance();


        public function execute(message:UploadRecordedSWFEvent):AsyncToken
        {
            return service.UploadRecordedSWF(message.sessionId, message.approvalId, message.data);
        }

        public function result(result:Boolean):Boolean
        {
            if(result)
            {
                var urlRequest:URLRequest = new URLRequest(FlexGlobals.topLevelApplication.parameters.URL);
                navigateToURL(urlRequest, "_self");
            }
            return result;
        }

        public function error(fault:Fault):void
        {

        }
    }
}
