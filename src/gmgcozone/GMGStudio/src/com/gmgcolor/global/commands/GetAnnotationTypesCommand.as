package com.gmgcolor.global.commands
{
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.event.GetAnnotationTypesEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.collections.ArrayCollection;
import mx.core.mx_internal;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

import org.spicefactory.lib.logging.LogContext;
import org.spicefactory.lib.logging.Logger;

use namespace mx_internal;
	

	public class GetAnnotationTypesCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		[Inject]
		public var applicationModel:ApplicationModel;
		
		private const logger:Logger = LogContext.getLogger(GetAnnotationTypesCommand);
		
		
		public function execute(event:GetAnnotationTypesEvent):AsyncToken
		{			
			return service.GetAnnotationTypes(event.sessionId);
		}
		
		public function result(result:Object):void
		{
			applicationModel.annotationTypes = new ArrayCollection(result as Array);
		}
		
		public function error(fault:Fault):void
		{
			
		}
		
	}
}
