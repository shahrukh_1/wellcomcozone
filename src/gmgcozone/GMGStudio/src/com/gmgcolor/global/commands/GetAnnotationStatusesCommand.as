package com.gmgcolor.global.commands {
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.service.IApplicationService;

import mx.collections.ArrayCollection;
import mx.rpc.AsyncToken;

public class GetAnnotationStatusesCommand extends AsyncTokenCommand {
		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var applicationModel:ApplicationModel;

		public function execute():AsyncToken
		{
			return service.GetAnnotationStatuses(applicationModel.securityManager.sessionId);
		}

		public function result(result:Array):Array
		{
			applicationModel.annotationStatuses = new ArrayCollection(result);
			return result;
		}
	}
}
