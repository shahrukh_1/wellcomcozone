package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.GetCollaboratorGroupsEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.events.FaultEvent;

public class GetCollaboratorGroupsCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		
		public function execute(event:GetCollaboratorGroupsEvent):AsyncToken
		{			
			return service.GetCollaboratorGroups(event.sessionId);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:FaultEvent):void
		{			
			
		}
	}
}
