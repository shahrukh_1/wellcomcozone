package com.gmgcolor.global.commands {
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.service.IApplicationService;

import mx.collections.ArrayCollection;
import mx.rpc.AsyncToken;

public class GetCommentTypesCommand extends AsyncTokenCommand {

		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var applicationModel:ApplicationModel;

		public function execute():AsyncToken
		{
			return service.GetCommentTypes(applicationModel.securityManager.sessionId, applicationModel.securityManager.accountUserInfo.AccountLocaleName);
		}

		public function result(result:Array):Array
		{
			applicationModel.commentTypes = new ArrayCollection(result);
			return result;
		}

	}
}
