package com.gmgcolor.global.commands {
import com.gmgcolor.annotations.domain.AccountUserInfo;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.enums.LanguageEnum;
import com.gmgcolor.application.service.IApplicationService;
import com.gmgcolor.global.managers.LanguageManager;

import mx.core.FlexGlobals;
import mx.rpc.AsyncToken;

public class GetUserAndAccountInfoCommand extends AsyncTokenCommand {

		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var securityManager:SecurityManager;

		[Inject]
		public var languageManager:LanguageManager;

		public function execute():AsyncToken
		{
			return service.GetUserAndAccountInfo(securityManager.sessionId, FlexGlobals.topLevelApplication.parameters.LoggedUser, FlexGlobals.topLevelApplication.parameters.IsExternal == '1', FlexGlobals.topLevelApplication.parameters.appid);
		}

		public function result(result:AccountUserInfo):AccountUserInfo
		{
			securityManager.logUserIn(result);

			for each (var language:LanguageEnum in LanguageEnum.AVAILABLE_LANGUAGES) {
				if (language.mappedLocale.toLowerCase() == result.AccountLocaleName.toLowerCase()) {
					languageManager.selectedLanguage = language;
					break;
				}
			}
			return result;
		}
	}
}
