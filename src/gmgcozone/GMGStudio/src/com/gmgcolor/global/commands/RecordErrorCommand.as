package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.RecordErrorEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class RecordErrorCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:RecordErrorEvent):AsyncToken
		{			
			return service.RecordError(event.errorFO);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:Fault):void
		{			
			
		}
	}
}
