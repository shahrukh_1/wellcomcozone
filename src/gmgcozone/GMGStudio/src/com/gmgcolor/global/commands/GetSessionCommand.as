package com.gmgcolor.global.commands {
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.service.IApplicationService;

import mx.rpc.AsyncToken;

public class GetSessionCommand extends AsyncTokenCommand {

		[Inject]
		public var service:IApplicationService;

		[Inject]
		 public var securityManager:SecurityManager;

		public function execute():AsyncToken
		{
			return service.GetSession();
		}

		public function result(result:String):String
		{
			securityManager.sessionId = result;
			return result;
		}
	}
}
