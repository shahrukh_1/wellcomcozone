package com.gmgcolor.global.commands
{
import flash.display.DisplayObject;

import mx.core.FlexGlobals;
import mx.managers.PopUpManager;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import org.spicefactory.lib.logging.LogContext;
import org.spicefactory.lib.logging.Logger;

public class IdleTimeoutCommand extends Broadcaster
	{
		private const log:Logger = LogContext.getLogger(IdleTimeoutCommand);
		
		public var resourceManager:IResourceManager = ResourceManager.getInstance();
		
		[Inject]
		public var subscriptionModel:SubscriptionModel;
		
		public function execute(message:IdleThresholdExceededMessage):void
		{
			log.info("Idle timeout exceeded.  Cleaning up");
			
			subscriptionModel.unsubscribeAllConsumers();
			logOutUser();
			showTimeoutMessage();
		}
		
		private function logOutUser():void
		{
			broadcast(new AccountEvent(AccountEvent.LOGOUT_ACCOUNT_EVENT));
		}
		
		private function showTimeoutMessage():void
		{
			var title:String = resourceManager.getString(ConfigEnum.NEO_APP_MESSAGES_BUNDLE,'idleManager.idleTimeoutTitle');
			var message:String = resourceManager.getString(ConfigEnum.NEO_APP_MESSAGES_BUNDLE,'idleManager.idleTimeoutMessage');
			var popup:ModalMessagePopup = new ModalMessagePopup();
			popup.closeVisible = false;
			popup.message = message;
			popup.title = title;
			PopUpManager.addPopUp(popup,FlexGlobals.topLevelApplication as DisplayObject,true);
			PopUpManager.centerPopUp(popup);
		}
	}
}
