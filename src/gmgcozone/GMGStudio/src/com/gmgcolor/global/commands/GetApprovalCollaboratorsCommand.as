
package com.gmgcolor.global.commands
{
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.event.GetApprovalCollaboratorsEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class GetApprovalCollaboratorsCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		[Inject]
		public var approvalModel:ApprovalModel;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:GetApprovalCollaboratorsEvent):AsyncToken
		{			
			return service.GetApprovalCollaborators(event.sessionId, approvalModel.currentApprovalId);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:Fault):void
		{
			
		}
	}
}
