package com.gmgcolor.global.commands
{
import com.gmgcolor.global.messages.LoadStyle;

import flash.events.IEventDispatcher;
import flash.system.ApplicationDomain;

import mx.core.FlexGlobals;
import mx.events.StyleEvent;
import mx.styles.IStyleManager2;

public class LoadStyleCommand extends AsyncCommand
	{
		
		public function execute(e:LoadStyle):void
		{
			var styleManager:IStyleManager2 = FlexGlobals.topLevelApplication.styleManager;
			var dispatcher:IEventDispatcher = styleManager.loadStyleDeclarations2(e.url, true, ApplicationDomain.currentDomain);
			dispatcher.addEventListener(StyleEvent.COMPLETE, onComplete);
			dispatcher.addEventListener(StyleEvent.ERROR, onError);
		}
		
		private function onComplete(e:StyleEvent):void
		{
			e.currentTarget.removeEventListener(StyleEvent.COMPLETE, onComplete);
			e.currentTarget.removeEventListener(StyleEvent.ERROR, onError);
			callback(true);
		}
		
		private function onError(e:StyleEvent):void
		{
			e.currentTarget.removeEventListener(StyleEvent.COMPLETE, onComplete);
			e.currentTarget.removeEventListener(StyleEvent.ERROR, onError);
			callback();
		}
	}
}
