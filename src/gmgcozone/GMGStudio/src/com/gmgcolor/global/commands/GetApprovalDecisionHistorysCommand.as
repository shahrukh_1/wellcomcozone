package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.GetApprovalDecisionHistorysEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.events.FaultEvent;

public class GetApprovalDecisionHistorysCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:GetApprovalDecisionHistorysEvent):AsyncToken
		{			
			return service.GetApprovalDecisionHistorys(event.sessionId,event.approvalId);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:FaultEvent):void
		{			
			
		}
	}
}
