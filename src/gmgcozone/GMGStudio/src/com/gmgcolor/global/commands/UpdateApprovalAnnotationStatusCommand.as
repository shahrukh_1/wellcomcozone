package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.UpdateApprovalAnnotationStatusEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class UpdateApprovalAnnotationStatusCommand
	{
		[Inject] 
		public var service:IApplicationService;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:UpdateApprovalAnnotationStatusEvent):AsyncToken
		{			
			return service.UpdateApprovalAnnotationStatus(event.sessionId, event.approvalId, event.statusId);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:Fault):void
		{			
			
		}
	}
}
