package com.gmgcolor.global.commands
{
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.GetUsePrePressFunctionalityEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class UsePrePressFunctionalityCommand
	{
		
		[Inject] 
		public var service:IApplicationService;

		
		[Inject]
		public var securityManager:SecurityManager;

		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		
		public function execute(event:GetUsePrePressFunctionalityEvent):AsyncToken
		{			
			return service.UsePrePressFunctionality();
		}
		
		public function result(result:Object):void
		{
			securityManager.sessionId = String(result);
		}
		
		public function error(fault:Fault):void
		{
			
		}
	}
}
