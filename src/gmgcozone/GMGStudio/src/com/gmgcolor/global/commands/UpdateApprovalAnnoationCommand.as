package com.gmgcolor.global.commands
{
import com.gmgcolor.annotations.domain.ApprovalAnnotationFO;
import com.gmgcolor.application.event.UpdateApprovalAnnotationEvent;
import com.gmgcolor.application.service.IApplicationService;
import com.gmgcolor.common.errornotifications.messages.ConsolidatedErrorMessage;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class UpdateApprovalAnnoationCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		[MessageDispatcher] public var dispatcher:Function;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		private var annotation:ApprovalAnnotationFO
		
		public function execute(event:UpdateApprovalAnnotationEvent):AsyncToken
		{			 
			annotation = event.annotation;
			return service.UpdateApprovalAnnoation(event.sessionId,event.annotation);
		}
		
		public function result(result:Object):void
		{
			
			if(result is ApprovalAnnotationFO)
			{
				
			}
			else
				dispatcher(new ConsolidatedErrorMessage("ApprovalAnnotationFO not returned from AddApprovalAnnotation method."));
		}
		
		public function error(fault:Fault):void
		{			
			
		}
	}
}
