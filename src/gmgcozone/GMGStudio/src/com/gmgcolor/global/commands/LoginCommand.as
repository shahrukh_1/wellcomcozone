package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.LoginEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class LoginCommand
	{
		
		[Inject] 
		public var service:IApplicationService;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:LoginEvent):AsyncToken
		{			
			return service.Login(event.sessionId, event.userId);
		}
		
		public function result(result:Object):void
		{
			
		}
		
		public function error(fault:Fault):void
		{
			
		}
	}
}
