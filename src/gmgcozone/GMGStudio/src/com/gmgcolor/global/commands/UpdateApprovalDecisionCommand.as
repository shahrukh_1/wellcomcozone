package com.gmgcolor.global.commands
{
import com.gmgcolor.application.event.UpdateApprovalDecisionEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class UpdateApprovalDecisionCommand
	{
		[Inject] 
		public var service:IApplicationService;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		public function execute(event:UpdateApprovalDecisionEvent):AsyncToken
		{			
			return service.UpdateApprovalUserDecision(event.sessionId, event.approvalId, event.collaboratorId,event.decisionId);
		}
		
		public function result(result:Object):void
		{
		}
		
		public function error(fault:Fault):void
		{			
			
		}
	}
}
