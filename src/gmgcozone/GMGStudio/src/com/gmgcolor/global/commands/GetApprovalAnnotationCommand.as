package com.gmgcolor.global.commands {
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.event.AddApprovalAnnotationEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.AsyncToken;
import mx.rpc.Fault;

public class GetApprovalAnnotationCommand
	{

		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var securityManager:SecurityManager;


		private var _resourceManager:IResourceManager = ResourceManager.getInstance();


		public function execute(event:AddApprovalAnnotationEvent):AsyncToken
		{
			return service.AddApprovalAnnotation(event.sessionId, event.annotation);
		}

		public function result(result:Object):void
		{
		}

		public function error(fault:Fault):void
		{

		}
	}
}
