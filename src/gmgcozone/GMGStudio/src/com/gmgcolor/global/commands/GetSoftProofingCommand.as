package com.gmgcolor.global.commands {
import com.gmgcolor.application.data.domain.ApprovalModel;
import com.gmgcolor.application.data.domain.SecurityManager;
import com.gmgcolor.application.data.domain.SoftProofingStatus;
import com.gmgcolor.application.event.SoftProofingStatusEvent;
import com.gmgcolor.application.service.IApplicationService;

import mx.rpc.AsyncToken;

public class GetSoftProofingCommand extends AsyncTokenCommand {

		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var approvalModel:ApprovalModel;

		[Inject]
		public var securityManager:SecurityManager;

		public function execute():AsyncToken
		{
			return service.GetSoftProofingStatus(securityManager.sessionId);
		}

		public function result(result:SoftProofingStatus):SoftProofingStatus
		{
            dispatcher(new SoftProofingStatusEvent(SoftProofingStatusEvent.STATUS, result));
			return result;
		}
	}
}
