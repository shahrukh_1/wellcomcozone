package com.gmgcolor.global.commands {
import com.gmgcolor.application.data.domain.ApplicationModel;
import com.gmgcolor.application.service.IApplicationService;

import mx.collections.ArrayCollection;
import mx.rpc.AsyncToken;

public class GetApprovalCollaboratorRolesCommand extends AsyncTokenCommand {

		[Inject]
		public var service:IApplicationService;

		[Inject]
		public var applicationModel:ApplicationModel;

		public function execute():AsyncToken
		{
			return service.GetApprovalCollaboratorRoles(applicationModel.securityManager.sessionId, applicationModel.securityManager.accountUserInfo.AccountLocaleName);
		}

		public function result(result:Array):Array
		{
			applicationModel.approvalCollaboratorRoles = new ArrayCollection(result);
			return result;
		}
	}
}
