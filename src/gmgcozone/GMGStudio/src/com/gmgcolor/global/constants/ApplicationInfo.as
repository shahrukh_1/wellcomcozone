package com.gmgcolor.global.constants {
import mx.core.FlexGlobals;
import mx.utils.URLUtil;


// Information about the application
	public class ApplicationInfo {
		// Only static public constants can be used here
		static public const appName:String = CONFIG::applicationName;
		static public const debug:Boolean = CONFIG::debug;
		static public const buildTime:String = CONFIG::buildTime;

		//Environmental Properties
        static public const serviceHandler:String = CONFIG::serviceHandler;
		static public const requestTimeout:int = int(CONFIG::requestTimeout);
		static public const applicationDestination:String = CONFIG::applicationDestination;

        static public function get httpsEnabled():Boolean {
            return URLUtil.isHttpsURL(FlexGlobals.topLevelApplication.parameters.URL);
        }

        static public function get channelUri():String {
            return URLUtil.getProtocol(FlexGlobals.topLevelApplication.parameters.URL) + '://' +
                   URLUtil.getServerNameWithPort(FlexGlobals.topLevelApplication.parameters.URL) + '/' + serviceHandler;
        }

		//static public const buildTime:Date = DateFormatter.parseDateString(CONFIG::buildTime);
	}
}