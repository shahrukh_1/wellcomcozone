package com.gmgcolor.account.event
{
import flash.events.Event;

public class AccountChangeEvent extends Event
	{
		public static const CHANGE:String = "change";

		public function AccountChangeEvent(type:String, bubbles:Boolean, cancelable:Boolean)
		{
			super(type,bubbles,cancelable);
		}

		public override function clone():Event
		{
			return new AccountChangeEvent(type,bubbles,cancelable);
		}

		public override function toString():String
		{
			return formatToString("AccountChangeEvent","bubbles","cancelable");
		}
	}
}
