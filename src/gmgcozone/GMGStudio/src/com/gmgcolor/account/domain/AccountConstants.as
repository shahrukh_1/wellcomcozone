package com.gmgcolor.account.domain
{	

	public class AccountConstants
	{

		/* CURRENCY LOV_CODE(S)
		 * AUD, EUR, GBP, HKD, JPY, USD
		 */
		public static const LCODE_CURRENCY_AUD:String = "AUD";
		public static const LCODE_CURRENCY_EUR:String = "EUR";
		public static const LCODE_CURRENCY_GBP:String = "GBP";
		public static const LCODE_CURRENCY_HKD:String = "HKD";
		public static const LCODE_CURRENCY_JPY:String = "JPY";
		public static const LCODE_CURRENCY_USD:String = "USD";
		
		/* CURRENCY LOV_CODE(S)
		* AUD, EUR, GBP, HKD, JPY, USD
		*/
		public static const CURRENCY_SYMBOL_AUD:String = "$";
		public static const CURRENCY_SYMBOL_EUR:String = "€";
		public static const CURRENCY_SYMBOL_GBP:String = "£";
		public static const CURRENCY_SYMBOL_HKD:String = "HK$";
		public static const CURRENCY_SYMBOL_JPY:String = "¥";
		public static const CURRENCY_SYMBOL_USD:String = "US$";
		
		public static const ACCOUNT_STATE_CHANGED:String = "myAccountCurrentStateChanged";
		
	
	}
}
