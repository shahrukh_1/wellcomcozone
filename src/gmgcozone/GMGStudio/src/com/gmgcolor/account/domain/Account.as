package com.gmgcolor.account.domain
{
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;

public class Account extends EventDispatcher
	{
		public function Account(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public var accountId:Number;
		public var language:String;
		
	}
}
