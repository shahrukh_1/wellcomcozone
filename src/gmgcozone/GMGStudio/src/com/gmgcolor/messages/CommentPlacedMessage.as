package com.gmgcolor.messages {
	public class CommentPlacedMessage {
		public var x:Number;
		public var y:Number;
		public function CommentPlacedMessage(x:Number,  y:Number)
		{
			this.x = x;
			this.y = y;
		}
	}
}
