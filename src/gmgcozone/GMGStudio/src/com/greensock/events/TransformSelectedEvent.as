package com.greensock.events {
import com.greensock.transform.TransformItem;

import flash.events.Event;

public class TransformSelectedEvent extends Event {
		public static const SELECTED:String = "selected";

		private var _currentTransformItem:TransformItem;
		private var _isDrawing:Boolean;

		public function TransformSelectedEvent(type:String, currentTransformItem:TransformItem, isDrawing:Boolean = false)
		{
			super(type, false, false);

			this._currentTransformItem = currentTransformItem;
			this._isDrawing = isDrawing;
		}

		public function get currentTransformItem():TransformItem
		{
			return _currentTransformItem;
		}

		public function get isDrawing():Boolean
		{
			return _isDrawing;
		}

		public override function clone():Event
		{
			return new TransformSelectedEvent(type, currentTransformItem, isDrawing);
		}

		public override function toString():String
		{
			return formatToString("com.greensock.events.TransformSelectedEvent", "currentTransformItem");
		}
	}
}
