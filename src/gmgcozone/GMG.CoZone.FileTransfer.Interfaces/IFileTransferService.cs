﻿using GMG.CoZone.FileTransfer.DomainModels;
using GMGColorBusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CoZone.FileTransfer.Interfaces
{
    public interface IFileTransferService
    {
        int SaveTransfer(FileTransferModel model);
        void DeleteFileTransfer(int id);
        List<int> GetExternalUsersForTransfer(int fileTransferId);
        Dictionary<int, string> GetDownloadPathsForApprovalsFromFolders(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals);
        void SetDownloadDateForExternal(int fileTransferExternalId);
        void SetDownloadDateForInternal(int fileTransferInternalID);

        void SendDownloadNotification(int fileTransferId, int internalrecipient, int eventCreatorUserId, DbContextBL context,bool isExternal);
    }
}
