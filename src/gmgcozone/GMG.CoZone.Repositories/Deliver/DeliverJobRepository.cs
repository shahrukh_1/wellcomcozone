﻿using GMG.CoZone.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMG.CoZone.Repositories.Interfaces.Deliver;
using GMGColorDAL;
using GMG.CoZone.Deliver.DomainModels;

namespace GMG.CoZone.Repositories.Deliver
{
    public class DeliverJobRepository : GenericRepository<DeliverJob>, IDeliverJobRepository
    {
        public DeliverJobRepository(GMGColorContext context) : base(context)
        {
        }

        public List<DeliverFileModel> GetDeliverJobsByIds(List<int> deliverJobIdss)
        {
            return Get(dj => deliverJobIdss.Contains(dj.ID) && !dj.IsDeleted).Select(dj => new DeliverFileModel()
            {
                Id = dj.ID,
                Guid = dj.Guid,
                FileName = dj.FileName
            }).ToList();
        }
    }
}
