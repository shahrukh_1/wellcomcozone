﻿using System;

namespace GMG.CoZone.Repositories.Models
{
    public class VersionPhaseModel : IComparable
    {
        public int Version { get; set; }
        public int? Phase { get; set; }

        public int CompareTo(object obj)
        {
            var rightObj = (VersionPhaseModel)obj;
            if (this.Version < rightObj.Version)
                return -1;
            if (this.Version > rightObj.Version)
                return 1;

            if ((this.Phase == null) && (rightObj != null))
                return -1;

            if ((this.Phase != null) && (rightObj == null))
                return 1;


            if ((this.Phase == null) && (rightObj == null))
                return 0;


            if (this.Phase < rightObj.Phase)
                return -1;
            if (this.Phase > rightObj.Phase)
                return 1;

            return 0;
        }
    }
}
