﻿using System;
using GMG.CoZone.Common.Repositories;
using GMG.CoZone.Common.Repositories.ApprovalRepositories;
using GMG.CoZone.Repositories.Common;
using GMG.CoZone.Repositories.Common.ApprovalRepositories;
using GMG.CoZone.Repositories.Deliver;
using GMG.CoZone.Repositories.FileTransfer;
using GMG.CoZone.Repositories.Interfaces;
using GMG.CoZone.Repositories.Interfaces.Common;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMG.CoZone.Repositories.Interfaces.Deliver;
using GMG.CoZone.Repositories.Interfaces.FileTransfer;
using GMG.CoZone.Repositories.Interfaces.Settings;
using GMG.CoZone.Repositories.Settings;
using GMGColorDAL;
using System.Data.SqlClient;

namespace GMG.CoZone.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed = false;
        private readonly GMGColorContext _context = new GMGColorContext();

        private IApprovalRepository _approvalRepository;
        private IApprovalCollaboratorRepository _approvalCollaboratorRepository;
        private IApprovalAnnotationRepository _approvalAnnotationRepository;
        private IApprovalUserViewInfoRepository _approvalUserViewInfoRepository;
        private IApprovalCollaboratorRoleRepository _approvalCollaboratorRoleoRepository;
        private IGenericRepository<SharedApproval> _sharedApprovalRepository;
        private IApprovalCollaboratorDecisionRepository _approvalCollaboratorDecisionRepository;
        private IApprovalPageRepository _approvalPageRepository;
        private IGenericRepository<ApprovalJobPhaseApproval> _approvalJobPhaseApprovalRepository;
        private IApprovalJobPhaseRepository _approvalJobPhaseRepository;
        private IAccountSettingsRepository _accountSettingsRepository;
        private IGenericRepository<ValueDataType> _valueDataTypeRepository;
        private IAccountRepository _accountRepository;
        private IUserRepository _userRepository;
        private ISoftProofingSessionParamRepository _softProofingSessionParamRepository;
        private IAccountSettingsValueTypeRepository _accountSettingsValueTypeRepository;
        private IGenericRepository<FTPPresetJobDownload> _ftpPresetJobDownloadRepository;
        private IDeliverJobRepository _deliverJobRepository;
        private IFileTransferRepository _fileTransferRepository;

        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (new UserRepository(_context)); }
        }
        public IGenericRepository<FTPPresetJobDownload> FtpPresetJobDownloadRepository
        {
            get { return _ftpPresetJobDownloadRepository ?? (new GenericRepository<FTPPresetJobDownload>(_context)); }
        }

        public IDeliverJobRepository DeliverJobRepository
        {
            get { return _deliverJobRepository ?? (new DeliverJobRepository(_context)); }
        }

        public IAccountRepository AccountRepository
        {
            get { return _accountRepository ?? (new AccountRepository(_context)); }
        }

        public IGenericRepository<ValueDataType> ValueDataTypeRepository
        {
            get { return _valueDataTypeRepository ?? (new GenericRepository<ValueDataType>(_context)); }
        }

        public IAccountSettingsRepository AccountSettingsRepository
        {
            get { return _accountSettingsRepository ?? (new AccountSettingsRepository(_context)); }
        }

        public IApprovalRepository ApprovalRepository
        {
            get { return _approvalRepository ?? (new ApprovalRepository(_context)); }
        }

        public IApprovalCollaboratorRepository ApprovalCollaboratorRepository
        {
            get { return _approvalCollaboratorRepository ?? new ApprovalCollaboratorRepository(_context); }
        }

        public IApprovalAnnotationRepository ApprovalAnnotationRepository
        {
            get { return _approvalAnnotationRepository ?? new ApprovalAnnotationRepository(_context); }
        }

        public IApprovalUserViewInfoRepository ApprovalUserViewInfoRepository
        {
            get { return _approvalUserViewInfoRepository ?? new ApprovalUserViewInfoRepository(_context); }
        }

        public IApprovalCollaboratorRoleRepository ApprovalCollaboratorRoleRepository
        {
            get { return _approvalCollaboratorRoleoRepository ?? new ApprovalCollaboratorRoleRepository(_context); }
        }

        public IGenericRepository<SharedApproval> SharedApprovalRepository
        {
            get { return _sharedApprovalRepository ?? new GenericRepository<SharedApproval>(_context); }
        }
        public IApprovalCollaboratorDecisionRepository ApprovalCollaboratorDecisionRepository
        {
            get { return _approvalCollaboratorDecisionRepository ?? new ApprovalCollaboratorDecisionRepository(_context); }
        }

        public IApprovalPageRepository ApprovalPageRepository
        {
            get { return _approvalPageRepository ?? new ApprovalPageRepository(_context); }
        }
        public IGenericRepository<ApprovalJobPhaseApproval> ApprovalJobPhaseApprovalRepository
        {
            get { return _approvalJobPhaseApprovalRepository ?? new GenericRepository<ApprovalJobPhaseApproval>(_context); }
        }
        public IApprovalJobPhaseRepository ApprovalJobPhaseRepository
        {
            get { return _approvalJobPhaseRepository ?? new ApprovalJobPhaseRepository(_context); }
        }

        public ISoftProofingSessionParamRepository SoftProofingSessionParamRepository
        {
            get { return _softProofingSessionParamRepository ?? new SoftProofingSessionParamRepository(_context); }
        }

        public IAccountSettingsValueTypeRepository AccountSettingsValueTypeRepository
        {
            get { return _accountSettingsValueTypeRepository ?? new AccountSettingsValueTypeRepository(_context); }
        }

        public IFileTransferRepository FileTransferRepository
        {
            get { return _fileTransferRepository ?? new FileTransferRepository(_context); }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// parameters name should same as defined in query.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        public int UpdateApprovalState(string query, int state, int approval)
        {
            _context.Database.ExecuteSqlCommand(query, new SqlParameter("state", state), new SqlParameter("approval", approval));
            return _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
