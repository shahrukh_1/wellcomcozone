﻿using GMG.CoZone.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMGColorDAL;
using GMG.CoZone.FileTransfer.DomainModels;
using GMG.CoZone.Repositories.Interfaces.FileTransfer;
using GMGColorDAL.CustomModels;
using GMGColorBusinessLogic;
using GMG.CoZone.Collaborate.DomainModels;

namespace GMG.CoZone.Repositories.FileTransfer
{
    public class FileTransferRepository : GenericRepository<GMGColorDAL.FileTransfer>, IFileTransferRepository
    {
        public FileTransferRepository(GMGColorContext context) : base(context)
        {
        }


        public void SoftDeleteFileTransfer(int id)
        {
            //get coresponding file transfer
            GMGColorDAL.FileTransfer fileTransfer = (from ft in context.FileTransfers
                                                     where ft.ID == id
                                                     select ft).SingleOrDefault();

            fileTransfer.IsDeleted = true;
        }

        public int SaveTransfer(FileTransferModel model)
        {
            GMGColorDAL.FileTransfer fileTransfer = new GMGColorDAL.FileTransfer()
            {
                AccountID = model.AccountID,
                FileName = model.FileName,
                FolderPath = model.FolderPath,
                Guid = model.Guid,
                Creator = model.Creator,
                CreatedDate = model.CreatedDate,
                Modifier = model.Modifier,
                ModifiedDate = model.ModifiedDate,
                Message = model.Message,
                ExpirationDate = model.ExpirationDate,
                IndividualFileNames = (String.IsNullOrEmpty(model.IndividualFileNames)) ? null : model.IndividualFileNames
            };
            var fileTransferUserInternals = context.Users.Where(c => model.InternalUsers.Contains(c.ID)).ToList().Select(d => new FileTransferUserInternal() { User = d, FileTransfer = fileTransfer });
            var fileTransferUserExternals = context.ExternalCollaborators.Where(c => model.ExternalUsers.Contains(c.ID)).ToList().Select(d => new FileTransferUserExternal() { ExternalCollaborator = d, FileTransfer = fileTransfer });

            // Add objects to database
            context.FileTransfers.Add(fileTransfer);
            context.FileTransferUserInternals.AddRange(fileTransferUserInternals);
            context.FileTransferUserExternals.AddRange(fileTransferUserExternals);

            // Add external users
            string externalEmails = PermissionsBL.GetFormatedExternalUsersDetails(model.ExternalEmails);
            List<NewExternalUserPermissionsPerf> newExternalUsers = PermissionsBL.GetNewExternalUsersAndEmailsPerf(externalEmails, null);

            foreach (var newExternalUser in newExternalUsers)
            {
                string[] givenfamilyNames = newExternalUser.FullName.Split(' ');
                var externalCollaborator = new ExternalCollaborator();
                externalCollaborator.Account = model.AccountID;
                externalCollaborator.Guid = Guid.NewGuid().ToString();
                externalCollaborator.Creator = model.Creator;
                externalCollaborator.CreatedDate = DateTime.UtcNow;
                externalCollaborator.IsDeleted = false;
                externalCollaborator.GivenName = givenfamilyNames[0].Trim();
                externalCollaborator.FamilyName = givenfamilyNames[1].Trim();
                externalCollaborator.EmailAddress = newExternalUser.Email.ToLower();
                externalCollaborator.Locale = newExternalUser.Locale;
                externalCollaborator.Modifier = model.Modifier;
                externalCollaborator.ModifiedDate = DateTime.UtcNow;

                FileTransferUserExternal fileTransferUserExternal = new FileTransferUserExternal()
                {
                    ExternalCollaborator = externalCollaborator,
                    FileTransfer = fileTransfer
                };

                // Add objects to database
                context.ExternalCollaborators.Add(externalCollaborator);
                context.FileTransferUserExternals.Add(fileTransferUserExternal);

            }

            //save changes
            context.SaveChanges();
            return fileTransfer.ID;
        }

        public List<int> GetExternalUsersForTransfer(int fileTransferId)
        {

            var externalUserIds = context.FileTransferUserExternals.Where(f => f.FileTransferID == fileTransferId).Select(s => s.UserExternalID).ToList();
            return externalUserIds;


        }

        public List<FileTransferModel> GetFileTransfersByIds(List<int> fileTransferIdss, int accountId)
        {
            DateTime today = DateTime.UtcNow;
            return Get(dj => fileTransferIdss.Contains(dj.ID) && !dj.IsDeleted && dj.ExpirationDate>today && dj.AccountID == accountId).Select(dj => new FileTransferModel()
            {
                ID = dj.ID,
                Guid = dj.Guid,
                FileName = dj.FileName,
                AccountID = dj.AccountID,
                FolderPath = dj.FolderPath,
                ExpirationDate = dj.ExpirationDate,
                Message = dj.Message,
                Creator = dj.Creator,
                CreatedDate = dj.CreatedDate,
                ModifiedDate = dj.ModifiedDate,
                Modifier = dj.Modifier
            }).ToList();


        }
        public List<ApprovalFileModel> GetMaxApprovalVersionsByFolderIds(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals)
        {
            var approvalIds = GetApprovalIdsFromFolders(folderIds);

            var foldersApprovals = (from a in context.Approvals
                                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                    where approvalIds.Contains(a.ID) && a.AllowDownloadOriginal && (a.IsDeleted == false && a.DeletePermanently == false) && (adminCanViewAllApprovals || ac.Collaborator == loggedUserId)
                                    select new
                                    {
                                        Id = a.ID,
                                        Guid = a.Guid,
                                        FileName = a.FileName,
                                        a.Version,
                                        a.Job
                                    }).Distinct().ToList();

            if (foldersApprovals.Count > 0)
            {
                var currentVersions = foldersApprovals.GroupBy(gr => gr.Job, (key, g) => g.OrderByDescending(e => e.Version).FirstOrDefault()).ToList();

                return currentVersions.Select(foldersApproval => new ApprovalFileModel()
                {
                    Id = foldersApproval.Id,
                    Guid = foldersApproval.Guid,
                    FileName = foldersApproval.FileName
                }).Distinct().ToList();
            }
            return new List<ApprovalFileModel>();
        }

        private IEnumerable<int> GetApprovalIdsFromFolders(List<int> folderIds)
        {
            var x = (from f in context.Folders
                     where folderIds.Contains(f.ID)
                     select f.Approvals.Select(a => a.ID));
            var list = x.ToList();
            var newList = new List<int>();
            foreach (var col in list)
            {
                newList.AddRange(col);
            }
           
            return newList;
        }

        public void SetDownloadDateForExternal(int fileTransferExternalId)
        {
            var externalTransfer = context.FileTransferUserExternals.Where(f => f.ID == fileTransferExternalId).SingleOrDefault();
            var currentDownloadDate = DateTime.UtcNow;
            externalTransfer.DownloadDate = currentDownloadDate;
        }
      
        public void SetDownloadDateForInternal(int fileTransferInternalId)
        {
            var interanlTransfer = context.FileTransferUserInternals.Where(f => f.ID == fileTransferInternalId).SingleOrDefault();
            var currentDownloadDate = DateTime.UtcNow;
            interanlTransfer.DownloadDate = currentDownloadDate;
        }
    }
}
