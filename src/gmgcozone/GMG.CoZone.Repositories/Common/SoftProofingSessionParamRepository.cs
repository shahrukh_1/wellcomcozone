﻿using System;
using System.Linq;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMG.CoZone.Repositories.Interfaces.Common;
using GMGColorDAL;
using System.Collections.Generic;

namespace GMG.CoZone.Repositories.Common
{
    public class SoftProofingSessionParamRepository : GenericRepository<SoftProofingSessionParam>, ISoftProofingSessionParamRepository
    {
        public SoftProofingSessionParamRepository(GMGColorContext context) : base(context)
        {
        }

        public SoftProofingSessionParamsModel GetExistingProofingSessionParameters(int approvalId, InternalUseJobViewingConditionsModel jobViewingConditions)
        {
            var softProofingSessions = (from sp in context.SoftProofingSessionParams
                                        join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                        where
                                            sp.Approval == approvalId &&
                                            sp.ActiveChannels == null && vwcnd.PaperTint == jobViewingConditions.PaperTint &&
                                            vwcnd.SimulationProfile == jobViewingConditions.SimulationProfile
                                        select new SoftProofingSessionParamsModel
                                        {
                                            ID = sp.ID,
                                            ActiveChannels = string.Empty,
                                            OutputRgbProfileName = sp.OutputRGBProfileName,
                                            SessionGuid = sp.SessionGuid,
                                            Version = sp.Approval,
                                            SimulationProfileId = vwcnd.SimulationProfile,
                                            EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                            CustomProfileId = vwcnd.CustomProfile,
                                            PaperTintId = vwcnd.PaperTint,
                                            DefaultSession = true,
                                            HighResolution = sp.HighResolution
                                        }).ToList();
            if (softProofingSessions.Any(s => s.HighResolution == true))
            {
                return softProofingSessions.Find(s => s.HighResolution);
            }

            return softProofingSessions.Take(1).FirstOrDefault();
        }

        public SoftProofingSessionParamsModel GetExistingProofingSessionParameters(int approvalId, int? paperTintId, InternalUseApprovalViewingConditionsModel viewingConditions)
        {
            return (from sp in context.SoftProofingSessionParams
                    join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                    where
                        sp.Approval == approvalId &&
                        sp.ActiveChannels == null && vwcnd.PaperTint == paperTintId &&
                        vwcnd.SimulationProfile == viewingConditions.SimulationProfile && vwcnd.CustomProfile == viewingConditions.CustomProfile && vwcnd.EmbeddedProfile == viewingConditions.EmbeddedProfile
                    select new SoftProofingSessionParamsModel
                    {
                        ID = sp.ID,
                        ActiveChannels = string.Empty,
                        OutputRgbProfileName = sp.OutputRGBProfileName,
                        SessionGuid = sp.SessionGuid,
                        Version = sp.Approval,
                        SimulationProfileId = vwcnd.SimulationProfile,
                        EmbeddedProfileId = vwcnd.EmbeddedProfile,
                        CustomProfileId = vwcnd.CustomProfile,
                        PaperTintId = vwcnd.PaperTint,
                        DefaultSession = true,
                        HighResolution = sp.HighResolution
                    }).Take(1).FirstOrDefault();
        }

        public SoftProofingSessionParamsModel GetDefaultCMYKProofingSessionParameters(int approvalId)
        {
            var defaultSessionParams = context.SoftProofingSessionParams.Where(sp => sp.Approval == approvalId && sp.DefaultSession).FirstOrDefault();
            var firstPage = context.ApprovalPages.Where(ap => ap.Approval == approvalId && ap.Number == 1).FirstOrDefault();
            if (defaultSessionParams == null || !defaultSessionParams.HighResolutionInProgress || firstPage == null || firstPage.Progress == 100)
            {
                return (from sp in context.SoftProofingSessionParams
                        join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                        where
                            sp.Approval == approvalId &&
                            sp.ActiveChannels == null &&
                            sp.DefaultSession
                        select new SoftProofingSessionParamsModel
                        {
                            ID = sp.ID,
                            ActiveChannels = string.Empty,
                            OutputRgbProfileName = sp.OutputRGBProfileName,
                            SessionGuid = sp.SessionGuid,
                            Version = sp.Approval,
                            SimulationProfileId = vwcnd.SimulationProfile,
                            CustomProfileId = vwcnd.CustomProfile,
                            EmbeddedProfileId = vwcnd.EmbeddedProfile,
                            PaperTintId = vwcnd.PaperTint,
                            DefaultSession = true,
                            HighResolution = sp.HighResolution
                        }).Take(1).FirstOrDefault();
            }
            else
            {
                return (from sp in context.SoftProofingSessionParams
                        join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                        where
                            sp.Approval == approvalId &&
                            sp.ActiveChannels == null &&
                            sp.ViewingCondition == defaultSessionParams.ViewingCondition &&
                            !sp.DefaultSession
                        select new SoftProofingSessionParamsModel
                        {
                            ID = sp.ID,
                            ActiveChannels = string.Empty,
                            OutputRgbProfileName = sp.OutputRGBProfileName,
                            SessionGuid = sp.SessionGuid,
                            Version = sp.Approval,
                            SimulationProfileId = vwcnd.SimulationProfile,
                            CustomProfileId = vwcnd.CustomProfile,
                            EmbeddedProfileId = vwcnd.EmbeddedProfile,
                            PaperTintId = vwcnd.PaperTint,
                            DefaultSession = true,
                            HighResolution = sp.HighResolution,
                            HighResolutionInProgress = true
                        }).Take(1).FirstOrDefault();
            }
        }

        public SoftProofingSessionParamsModel GetDefaultRGBProofingSessionParameters(int approvalId)
        {
            var defaultSessionParams = context.SoftProofingSessionParams.Where(sp => sp.Approval == approvalId && sp.DefaultSession).FirstOrDefault();
            var firstPage = context.ApprovalPages.Where(ap => ap.Approval == approvalId && ap.Number == 1).FirstOrDefault();
            if (defaultSessionParams == null || !defaultSessionParams.HighResolutionInProgress || firstPage == null || firstPage.Progress == 100)
            {
                return (from sp in context.SoftProofingSessionParams
                        where
                            sp.Approval == approvalId &&
                            sp.ActiveChannels == null &&
                            sp.DefaultSession
                        select new SoftProofingSessionParamsModel
                        {
                            ID = sp.ID,
                            ActiveChannels = string.Empty,
                            OutputRgbProfileName = sp.OutputRGBProfileName,
                            SessionGuid = sp.SessionGuid,
                            Version = sp.Approval,
                            DefaultSession = true,
                            HighResolution = sp.HighResolution
                        }).Take(1).FirstOrDefault();
            }
            else
            {
                return (from sp in context.SoftProofingSessionParams
                        where
                            sp.Approval == approvalId &&
                            sp.ActiveChannels == null &&
                            sp.ViewingCondition == defaultSessionParams.ViewingCondition &&
                            !sp.DefaultSession
                        select new SoftProofingSessionParamsModel
                        {
                            ID = sp.ID,
                            ActiveChannels = string.Empty,
                            OutputRgbProfileName = sp.OutputRGBProfileName,
                            SessionGuid = sp.SessionGuid,
                            Version = sp.Approval,
                            DefaultSession = true,
                            HighResolution = sp.HighResolution,
                            HighResolutionInProgress = true
                        }).Take(1).FirstOrDefault();
            }
        }

        public SoftProofingSessionParamsModel CreateNewSoftproofingParameters(int approvalId, InternalUseJobViewingConditionsModel viewingConditions, bool highRes = false)
        {
            //create new session if not existging and also send generate tiles
            var newSession = new SoftProofingSessionParam
            {
                Approval = approvalId,
                LastAccessedDate = DateTime.UtcNow,
                SessionGuid = Guid.NewGuid().ToString(),
                ViewingCondition = viewingConditions.ID,
                SimulatePaperTint = true,
                HighResolution = highRes
            };
            context.SoftProofingSessionParams.Add(newSession);

            return new SoftProofingSessionParamsModel
                {
                    ID = newSession.ID,
                    ActiveChannels = string.Empty,
                    OutputRgbProfileName = newSession.OutputRGBProfileName,
                    SessionGuid = newSession.SessionGuid,
                    Version = newSession.Approval,
                    SimulationProfileId = viewingConditions.SimulationProfile,
                    PaperTintId = viewingConditions.PaperTint,
                    DefaultSession = true,
                    HighResolution = highRes
            };
                        
        }

        public SoftProofingSessionParamsModel CreateNewSoftproofingParameters(int approvalId, int? paperTintId, InternalUseApprovalViewingConditionsModel viewingConditions)
        {
            var defaultwithPaperTintCond = new ViewingCondition()
            {
                SimulationProfile = viewingConditions.SimulationProfile,
                EmbeddedProfile = viewingConditions.EmbeddedProfile,
                CustomProfile = viewingConditions.CustomProfile,
                PaperTint = paperTintId,
            };
            
            var newSessionWithPaperTint = new SoftProofingSessionParam
            {
                Approval = approvalId,
                LastAccessedDate = DateTime.UtcNow,
                SessionGuid = Guid.NewGuid().ToString(),
                ViewingCondition1 = defaultwithPaperTintCond,
                SimulatePaperTint = true
            };

            context.ViewingConditions.Add(defaultwithPaperTintCond);
            context.SoftProofingSessionParams.Add(newSessionWithPaperTint);

            return new SoftProofingSessionParamsModel
            {
                ID = newSessionWithPaperTint.ID,
                ActiveChannels = string.Empty,
                OutputRgbProfileName = newSessionWithPaperTint.OutputRGBProfileName,
                SessionGuid = newSessionWithPaperTint.SessionGuid,
                Version = newSessionWithPaperTint.Approval,
                SimulationProfileId = defaultwithPaperTintCond.SimulationProfile,
                EmbeddedProfileId = defaultwithPaperTintCond.EmbeddedProfile,
                CustomProfileId = defaultwithPaperTintCond.CustomProfile,
                PaperTintId = defaultwithPaperTintCond.PaperTint,
                DefaultSession = true
            };
        }

        public InternalUseApprovalViewingConditionsModel GetApprovalViewingCondition(int approvalId)
        {
            return (from sp in context.SoftProofingSessionParams
                    join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                    where
                        sp.Approval == approvalId &&
                        sp.DefaultSession
                    select new InternalUseApprovalViewingConditionsModel
                    {
                        CustomProfile = vwcnd.CustomProfile,
                        EmbeddedProfile = vwcnd.EmbeddedProfile,
                        SimulationProfile = vwcnd.SimulationProfile
                    }).FirstOrDefault();
        }

        public InternalUseJobViewingConditionsModel GetJobViewingCondition(int jobId)
        {
            return (from vwcn in context.ViewingConditions
                    join jvwcn in context.ApprovalJobViewingConditions on vwcn.ID equals jvwcn.ViewingCondition
                    where jvwcn.Job == jobId
                    select new InternalUseJobViewingConditionsModel
                    {
                        ID = vwcn.ID,
                        SimulationProfile = vwcn.SimulationProfile,
                        PaperTint = vwcn.PaperTint
                    }).FirstOrDefault();
        }

        public int GetApprovalIdByProofStudioSession(string sessionId)
        {
            return context.SoftProofingSessionParams.Where(p => p.SessionGuid == sessionId).FirstOrDefault().Approval;
        }

        public void ResetDefaultForNonHighResSessionParams(int approvalId)
        {
            var sessionParams = context.SoftProofingSessionParams.Where(p => p.Approval == approvalId).ToList();

            foreach (var sessionParam in sessionParams)
            {
                if (!sessionParam.HighResolution)
                {
                    sessionParam.DefaultSession = false;
                }
            }

            context.SaveChanges();
        }

        public SoftProofingSessionParam GetDefaultSessionParamsByApprovalId(int approvalId)
        {
            return context.SoftProofingSessionParams.FirstOrDefault(t => t.Approval == approvalId && t.DefaultSession == true);
        }

        public ViewingCondition GetViewingConditionByJobId(int jobId)
        {
            return (from vwcn in context.ViewingConditions
                    join jvwcn in context.ApprovalJobViewingConditions on vwcn.ID equals jvwcn.ViewingCondition
                    where jvwcn.Job == jobId
                    select vwcn).FirstOrDefault();
        }

        public string GetApprovalHighResSoftProofingSessionInProgress(int approvalId)
        {
            return (from sps in context.SoftProofingSessionParams
                    where sps.Approval == approvalId && sps.HighResolutionInProgress == true
                    select sps.SessionGuid).FirstOrDefault();
        }
    }
}
