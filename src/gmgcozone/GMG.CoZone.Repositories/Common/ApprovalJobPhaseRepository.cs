﻿using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Common.Repositories;
using GMG.CoZone.Repositories.Interfaces.Common;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Common
{
    public class ApprovalJobPhaseRepository : GenericRepository<ApprovalJobPhase>, IApprovalJobPhaseRepository
    {
        public ApprovalJobPhaseRepository(GMGColorContext context) : base(context)
        {
        }

        public List<int> GetInternalUserVisiblePhases(int approvalId, int? currentPhase)
        {
            if (currentPhase == null)
            {
                return new List<int>();
            }

            return (from aph in context.ApprovalJobPhases
                    join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                    where ajpa.Approval == approvalId && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == currentPhase)
                    select aph.ID).ToList();

        }
        public List<int> GetExternalUserVisiblePhases(int approvalId, int? currentPhase)
        {
            if (currentPhase == null)
            {
                return new List<int>();
            }

            return (from aph in context.ApprovalJobPhases
                    join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                    where ajpa.Approval == approvalId && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == currentPhase)
                    select aph.ID).ToList();

        }

    }
}
