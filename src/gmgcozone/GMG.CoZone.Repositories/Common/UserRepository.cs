﻿using System.Linq;
using GMG.CoZone.Repositories.Interfaces.Common;
using GMGColorDAL;
using GMGColorDAL.CustomModels.Api;
using System.Collections.Generic;

namespace GMG.CoZone.Repositories.Common
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(GMGColorContext context) : base(context)
        {
        }

        // TODO: check why this function returns always true
        public bool UserIsAdministrator(string userKey)
        {
            return (from u in context.Users
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where
                        u.Guid.ToLower() == userKey.ToLower() &&
                        us.Key.ToUpper() == "A" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate &&
                        r.Key.ToUpper() == "ADM"
                    select true).FirstOrDefault();
        }

        public bool UserIsManager(string userKey)
        {
            string showAllFilesToManagers = (from acs in context.AccountSettings
                                             join u in context.Users on acs.Account equals u.Account
                                             where u.Guid == userKey.ToLower() && acs.Name == "ShowAllFilesToManagers"
                                             select acs.Value).FirstOrDefault();

            if (showAllFilesToManagers == "True")
            {
                return (from u in context.Users
                        join us in context.UserStatus on u.Status equals us.ID
                        join ur in context.UserRoles on u.ID equals ur.User
                        join r in context.Roles on ur.Role equals r.ID
                        join am in context.AppModules on r.AppModule equals am.ID
                        where u.Guid.ToLower() == userKey.ToLower() &&
                        us.Key.ToUpper() == "A" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate &&
                        r.Key.ToUpper() == "MAN"
                        select true).FirstOrDefault();
            }
            else
            {
                return false;
            }
        }

        public bool InternalUserCanViewAllPhasesAnnotations(string userKey)
        {
            var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
            return (from us in context.UserSettings
                    join u in context.Users on us.User equals u.ID
                    where (u.Guid.ToLower() == userKey.ToLower()) && us.Setting == showAnnotationsFromAllPhases
                    select us.Value.ToLower() == "true").FirstOrDefault();
        }

        public bool ExternalUserCanViewAllPhasesAnnotations(string userKey)
        {
            return (from ec in context.ExternalCollaborators
                    where (ec.Guid.ToLower() == userKey.ToLower())
                    select ec.ShowAnnotationsOfAllPhases).FirstOrDefault();
        }

        public int GetUserIdByGuid(string userKey)
        {
            return (from u in context.Users
                    join us in context.UserStatus on u.Status equals us.ID
                    where u.Guid.ToLower() == userKey.ToLower() && us.Key == "A"
                    select u.ID).FirstOrDefault();
        }

        public bool IsApprovalFromCurrentAccount(List<int> approvalIds, List<int> folderIds, int accountId)
        {
            if (approvalIds.Count > 0)
            {
                return ((from j in context.Jobs
                         join a in context.Approvals on j.ID equals a.Job
                         where approvalIds.Contains(a.ID) && j.Account == accountId
                         select j.ID).Count()) == approvalIds.Count ? true : false;
            }
            else
            {
                return ((from f in context.Folders
                         where folderIds.Contains(f.ID) && f.Account == accountId
                         select f.ID).Count()) == folderIds.Count ? true : false;
            }
        }

        public bool IsFromCurrentAccount(List<int> IdList, string userGuid, string typeOf)
        {
            bool result = false;
            try
            {
                if (typeOf == "USER")
                {
                    result =
                        (from u in context.Users
                         where u.Guid == userGuid
                         select u.ID).Any();
                }
                else if (typeOf == "APPROVAL")
                {
                    result =
                        ((from a in context.Approvals
                          join j in context.Jobs on a.Job equals j.ID
                          let accountId = (from u in context.Users
                                           where u.Guid == userGuid
                                           select u.Account).FirstOrDefault()

                          let accountId1 = accountId == 0 ? (from ec in context.ExternalCollaborators
                                                             where ec.Guid == userGuid
                                                             select ec.Account).FirstOrDefault() : accountId
                          where IdList.Contains(a.ID) && j.Account == accountId1
                          select a.ID).Count()) == IdList.Count() ? true : false;
                }

                else if (typeOf == "JOB")
                {
                    result = ((from j in context.Jobs
                               let accountId = (from u in context.Users
                                                where u.Guid == userGuid
                                                select u.Account).FirstOrDefault()
                               let accountId1 = accountId == 0 ? (from ec in context.ExternalCollaborators
                                                                  where ec.Guid == userGuid
                                                                  select ec.Account).FirstOrDefault() : accountId
                               where IdList.Contains(j.ID) && j.Account == accountId1
                               select j.ID).Count()) == IdList.Count() ? true : false;
                }
                else if (typeOf == "FOLDER")
                {
                    result =
                        ((from f in context.Folders
                          let accountId = (from u in context.Users
                                           where u.Guid == userGuid
                                           select u.Account).FirstOrDefault()
                          where IdList.Contains(f.ID) && f.Account == accountId
                          select f.ID).Count()) == IdList.Count() ? true : false;
                }

                else if (typeOf == "APPROVALPHASE")
                {
                    result =
                       ((from ap in context.ApprovalPhases
                         join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                         let accountId = (from u in context.Users
                                          where u.Guid == userGuid
                                          select u.Account).FirstOrDefault()
                         let accountId1 = accountId == 0 ? (from ec in context.ExternalCollaborators
                                                            where ec.Guid == userGuid
                                                            select ec.Account).FirstOrDefault() : accountId
                         where IdList.Contains(ap.ID) && aw.Account == accountId1
                         select ap.ID).Count()) == IdList.Count() ? true : false;
                }
                else if (typeOf == "APPROVALPAGE")
                {
                    result =
                        ((from a in context.Approvals
                          join j in context.Jobs on a.Job equals j.ID
                          join ap in context.ApprovalPages on a.ID equals ap.Approval

                          let accountId = (from u in context.Users
                                           where u.Guid == userGuid
                                           select u.Account).FirstOrDefault()
                          let accountId1 = accountId == 0 ? (from ec in context.ExternalCollaborators
                                                             where ec.Guid == userGuid
                                                             select ec.Account).FirstOrDefault() : accountId
                          where IdList.Contains(ap.ID) && j.Account == accountId1
                          select a.ID).Count()) == IdList.Count() ? true : false;
                }
                else if (typeOf == "APPROVALANNOTAION")
                {
                    result =
                        ((from a in context.Approvals
                          join j in context.Jobs on a.Job equals j.ID
                          join ap in context.ApprovalPages on a.ID equals ap.Approval
                          join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                          let accountId = (from u in context.Users
                                           where u.Guid == userGuid
                                           select u.Account).FirstOrDefault()
                          let accountId1 = accountId == 0 ? (from ec in context.ExternalCollaborators
                                                             where ec.Guid == userGuid
                                                             select ec.Account).FirstOrDefault() : accountId
                          where IdList.Contains(aa.ID) && j.Account == accountId1
                          select a.ID).Count()) == IdList.Count() ? true : false;
                }

                else
                {
                    result = false;
                }

                return result;
            }
            finally
            {
            }
        }

        public bool IsUserHasAccess(List<int> approvalIds, int userId)
        {
            var showAllFilesToManagers = (from acs in context.AccountSettings
                                             join u in context.Users on acs.Account equals u.Account
                                             where u.ID == userId && acs.Name == "ShowAllFilesToManagers"
                                             select acs.Value).Any();

            bool hasRole =
                 (from u in context.Users
                  join us in context.UserStatus on u.Status equals us.ID
                  join ur in context.UserRoles on u.ID equals ur.User
                  join r in context.Roles on ur.Role equals r.ID
                  join am in context.AppModules on r.AppModule equals am.ID
                  where u.ID == userId && us.Key.ToUpper() == "A" &&
                      am.Key == 0 &&
                      (r.Key == "ADM" || (showAllFilesToManagers && r.Key == "MAN"))
                  select u.ID).Any();

            if (hasRole) return true;

            return ((from ac in context.ApprovalCollaborators
                     where approvalIds.Contains(ac.Approval) && ac.Collaborator == userId
                     select ac.ID).Count()) == approvalIds.Count ? true : false;

        }
    }
}
