﻿using GMG.CoZone.Collaborate.DomainModels;
using GMG.CoZone.Repositories.Interfaces.Common;
using GMGColorDAL;
using System.Linq;

namespace GMG.CoZone.Repositories.Common
{
    public class AccountRepository: GenericRepository<Account>, IAccountRepository
    {
        public AccountRepository(GMGColorContext context) : base(context)
        {
        }

        public ApprovalInfoModel GetApprovalInfo(int approvalId)
        {
            return (from ap in context.Approvals
                    join u in context.Users on ap.Creator equals u.ID
                    join ac in context.Accounts on u.Account equals ac.ID
                    where ap.ID == approvalId
                    select new ApprovalInfoModel
                    {
                        Guid = ap.Guid,
                        Region = ac.Region,
                        Domain = ac.Domain
                    }).FirstOrDefault();
        }
    }
}
