﻿using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMGColorDAL;
using System.Collections.Generic;
using System.Linq;

namespace GMG.CoZone.Repositories.Common.ApprovalRepositories
{
    class ApprovalCollaboratorRoleRepository : GenericRepository<ApprovalCollaboratorRole>, IApprovalCollaboratorRoleRepository
    {
        public ApprovalCollaboratorRoleRepository(GMGColorContext context) : base(context)
        {

        }

        public ApprovalCollaboratorRole GetApprovalCollaboratorRole(string approvalCollaboratorRoleKey)
        {
            ApprovalCollaboratorRole approvalCollaboratorRole = Get(apcr => apcr.Key == approvalCollaboratorRoleKey).FirstOrDefault();

            return approvalCollaboratorRole;
        }

        public List<ApprovalCollaboratorRole> GetApprovalCollaboratorRolesList(string[] approvalCollaboratorRolesKeys)
        {
            List<ApprovalCollaboratorRole> approvalCollaboratorRole = Get(apcr => approvalCollaboratorRolesKeys.Contains(apcr.Key)).ToList();

            return approvalCollaboratorRole;
        }

        public int GetApprovalCollaboratorRoleID(string approvalCollaboratorRoleKey)
        {
            return GetApprovalCollaboratorRole(approvalCollaboratorRoleKey).ID;
        }

        public int[] GetApprovalCollaboratorRolesListID(string[] approvalCollaboratorRolesKeys)
        {
            return GetApprovalCollaboratorRolesList(approvalCollaboratorRolesKeys).Select(s => s.ID).ToArray();
        }

    }
}
