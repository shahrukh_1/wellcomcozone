﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Common.Repositories;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMG.CoZone.Repositories.Models;
using GMGColorDAL;
using GMG.CoZone.Collaborate.DomainModels;
using System.Data.SqlClient;

namespace GMG.CoZone.Repositories.Common.ApprovalRepositories
{
    public class ApprovalCollaboratorRepository : GenericRepository<ApprovalCollaborator>, IApprovalCollaboratorRepository
    {
        public ApprovalCollaboratorRepository(GMGColorContext context) : base(context)
        {
        }

        public List<UserGroupModel> GetJobPrimayGroups(int jobId, string userKey, bool isExternalUser)
        {
            var groups = (from acg in context.ApprovalCollaboratorGroups
                          join ug in context.UserGroups on acg.CollaboratorGroup equals ug.ID
                          join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                          join a in context.Approvals on acg.Approval equals a.ID

                          let ShowAnnotationsOfAllPhases = !isExternalUser ? (from u in context.Users
                                                                              join uus in context.UserSettings on u.ID equals uus.User
                                                                              where u.Guid == userKey && uus.Setting == "ShowAnnotationsOfAllPhases"
                                                                              select uus.Value).FirstOrDefault() : (from ec in context.ExternalCollaborators
                                                                                                                    join uus in context.UserSettings on ec.ID equals uus.User
                                                                                                                    where ec.Guid == userKey && uus.Setting == "ShowAnnotationsOfAllPhases"
                                                                                                                    select uus.Value).FirstOrDefault()

                          where ugu.IsPrimary && a.Job == jobId && (a.CurrentPhase == acg.Phase || ShowAnnotationsOfAllPhases == "True")
                          select new UserGroupModel
                          {
                              ID = ug.ID,
                              Name = ug.Name
                          }).ToList();

            return groups.GroupBy(o => o.ID).Select(o => o.FirstOrDefault()).ToList();
        }

        public List<UserGroupDecisionModel> GetJobPrimaryGroupDecisions(string userGuid, int jobId)
        {
            string query = @"SELECT
                       [Project16].[ID] AS [approval_version],
                       [Project16].[CollaboratorGroup] AS [user_group],
                       CASE
                          WHEN
                             (
                                EXISTS 
                                (
                                   SELECT
                                      TOP (1) 1 AS [C1] 
                                   FROM
                                      (
                                         SELECT
                                            [Extent21].[Priority] AS [Priority] 
                                         FROM
                                            (
                                               SELECT
                                                  [Extent18].[Approval] AS [Approval1],
                                                  [Extent18].[Decision] AS [Decision],
                                                  [Extent18].[Phase] AS [Phase1],
                                                  [Extent20].[UserGroup] AS [UserGroup] 
                                               FROM
                                                  [dbo].[ApprovalCollaboratorDecision] AS [Extent18] 
                                                  INNER JOIN
                                                     [dbo].[ApprovalCollaborator] AS [Extent19] 
                                                     ON [Extent18].[Collaborator] = [Extent19].[Collaborator] 
                                                  INNER JOIN
                                                     [dbo].[UserGroupUser] AS [Extent20] 
                                                     ON [Extent19].[Collaborator] = [Extent20].[User] 
                                               WHERE
                                                  [Extent20].[IsPrimary] = 1 
                                            )
                                            AS [Filter10] 
                                            INNER JOIN
                                               [dbo].[ApprovalDecision] AS [Extent21] 
                                               ON [Filter10].[Decision] = [Extent21].[ID] 
                                         WHERE
                                            (
                                               [Filter10].[Approval1] = [Project16].[ID] 
                                            )
                                            AND 
                                            (
                                               [Filter10].[UserGroup] = [Project16].[CollaboratorGroup] 
                                            )
                                            AND 
                                            (
                    ( [Filter10].[Phase1] = [Project16].[CurrentPhase] ) 
                                               OR 
                                               (
                    ([Filter10].[Phase1] IS NULL) 
                                                  AND 
                                                  (
                                                     [Project16].[CurrentPhase] IS NULL 
                                                  )
                                               )
                                            )
                                      )
                                      AS [Project17] 
                                   ORDER BY
                                      [Project17].[Priority] ASC 
                                )
                             )
                          THEN
                             CASE
                                WHEN
                                   (
                                      [Project16].[C1] IS NULL
                                   )
                                THEN
                                   0 
                                ELSE
                                   [Project16].[C2] 
                             END
                       END
                       AS [decision] 
                    FROM
                       (
                          SELECT
                             [Project15].[ID] AS [ID],
                             [Project15].[CurrentPhase] AS [CurrentPhase],
                             [Project15].[CollaboratorGroup] AS [CollaboratorGroup],
                             [Project15].[C1] AS [C1],
                             [Project15].[C2] AS [C2] 
                          FROM
                             (
                                SELECT
                                   [Project12].[ID] AS [ID],
                                   [Project12].[CurrentPhase] AS [CurrentPhase],
                                   [Project12].[CollaboratorGroup] AS [CollaboratorGroup],
                                   [Project12].[C1] AS [C1],
                                   (
                                      SELECT
                                         TOP (1) [top].[ID] AS [ID] 
                                      FROM
                                         (
                                            SELECT
                                               TOP (1) [Project13].[ID] AS [ID] 
                                            FROM
                                               (
                                                  SELECT
                                                     [Extent17].[ID] AS [ID],
                                                     [Extent17].[Priority] AS [Priority] 
                                                  FROM
                                                     (
                                                        SELECT
                                                           [Extent14].[Approval] AS [Approval2],
                                                           [Extent14].[Decision] AS [Decision],
                                                           [Extent14].[Phase] AS [Phase2],
                                                           [Extent16].[UserGroup] AS [UserGroup] 
                                                        FROM
                                                           [dbo].[ApprovalCollaboratorDecision] AS [Extent14] 
                                                           INNER JOIN
                                                              [dbo].[ApprovalCollaborator] AS [Extent15] 
                                                              ON [Extent14].[Collaborator] = [Extent15].[Collaborator] 
                                                           INNER JOIN
                                                              [dbo].[UserGroupUser] AS [Extent16] 
                                                              ON [Extent15].[Collaborator] = [Extent16].[User] 
                                                        WHERE
                                                           [Extent16].[IsPrimary] = 1 
                                                     )
                                                     AS [Filter8] 
                                                     INNER JOIN
                                                        [dbo].[ApprovalDecision] AS [Extent17] 
                                                        ON [Filter8].[Decision] = [Extent17].[ID] 
                                                  WHERE
                                                     (
                                                        [Filter8].[Approval2] = [Project12].[ID] 
                                                     )
                                                     AND 
                                                     (
                                                        [Filter8].[UserGroup] = [Project12].[CollaboratorGroup] 
                                                     )
                                                     AND 
                                                     (
                    ( [Filter8].[Phase2] = [Project12].[CurrentPhase] ) 
                                                        OR 
                                                        (
                    ([Filter8].[Phase2] IS NULL) 
                                                           AND 
                                                           (
                                                              [Project12].[CurrentPhase] IS NULL 
                                                           )
                                                        )
                                                     )
                                               )
                                               AS [Project13] 
                                            ORDER BY
                                               [Project13].[Priority] ASC 
                                         )
                                         AS [top] 
                                   )
                                   AS [C2] 
                                FROM
                                   (
                                      SELECT
                                         [Project11].[ID] AS [ID],
                                         [Project11].[CurrentPhase] AS [CurrentPhase],
                                         [Project11].[CollaboratorGroup] AS [CollaboratorGroup],
                                         [Project11].[C1] AS [C1] 
                                      FROM
                                         (
                                            SELECT
                                               [Project8].[ID] AS [ID],
                                               [Project8].[CurrentPhase] AS [CurrentPhase],
                                               [Project8].[CollaboratorGroup] AS [CollaboratorGroup],
                                               (
                                                  SELECT
                                                     TOP (1) [top].[ID] AS [ID] 
                                                  FROM
                                                     (
                                                        SELECT
                                                           TOP (1) [Project9].[ID] AS [ID] 
                                                        FROM
                                                           (
                                                              SELECT
                                                                 [Extent13].[ID] AS [ID],
                                                                 [Extent13].[Priority] AS [Priority] 
                                                              FROM
                                                                 (
                                                                    SELECT
                                                                       [Extent10].[Approval] AS [Approval3],
                                                                       [Extent10].[Decision] AS [Decision],
                                                                       [Extent10].[Phase] AS [Phase3],
                                                                       [Extent12].[UserGroup] AS [UserGroup] 
                                                                    FROM
                                                                       [dbo].[ApprovalCollaboratorDecision] AS [Extent10] 
                                                                       INNER JOIN
                                                                          [dbo].[ApprovalCollaborator] AS [Extent11] 
                                                                          ON [Extent10].[Collaborator] = [Extent11].[Collaborator] 
                                                                       INNER JOIN
                                                                          [dbo].[UserGroupUser] AS [Extent12] 
                                                                          ON [Extent11].[Collaborator] = [Extent12].[User] 
                                                                    WHERE
                                                                       [Extent12].[IsPrimary] = 1 
                                                                 )
                                                                 AS [Filter6] 
                                                                 INNER JOIN
                                                                    [dbo].[ApprovalDecision] AS [Extent13] 
                                                                    ON [Filter6].[Decision] = [Extent13].[ID] 
                                                              WHERE
                                                                 (
                                                                    [Filter6].[Approval3] = [Project8].[ID] 
                                                                 )
                                                                 AND 
                                                                 (
                                                                    [Filter6].[UserGroup] = [Project8].[CollaboratorGroup] 
                                                                 )
                                                                 AND 
                                                                 (
                    ( [Filter6].[Phase3] = [Project8].[CurrentPhase] ) 
                                                                    OR 
                                                                    (
                    ([Filter6].[Phase3] IS NULL) 
                                                                       AND 
                                                                       (
                                                                          [Project8].[CurrentPhase] IS NULL 
                                                                       )
                                                                    )
                                                                 )
                                                           )
                                                           AS [Project9] 
                                                        ORDER BY
                                                           [Project9].[Priority] ASC 
                                                     )
                                                     AS [top] 
                                               )
                                               AS [C1] 
                                            FROM
                                               (
                                                  SELECT
                                                     [Project5].[ID] AS [ID],
                                                     [Project5].[Job] AS [Job],
                                                     [Project5].[CurrentPhase] AS [CurrentPhase],
                                                     [Project5].[CollaboratorGroup] AS [CollaboratorGroup],
                                                     [Project5].[Phase] AS [Phase],
                                                     [Project5].[C1] AS [C1],
                                                     CASE
                                                        WHEN
                                                           (
                    ( EXISTS 
                                                              (
                                                                 SELECT
                                                                    1 AS [C1] 
                                                                 FROM
                                                                    [dbo].[ApprovalCollaborator] AS [Extent5] 
                                                                    INNER JOIN
                                                                       [dbo].[User] AS [Extent6] 
                                                                       ON [Extent5].[Collaborator] = [Extent6].[ID] 
                                                                 WHERE
                                                                    (
                                                                       [Project5].[ID] = [Extent5].[Approval] 
                                                                    )
                                                                    AND 
                                                                    (
                                                                       [Extent6].[Guid] = @p__linq__0
                                                                    )
                                                                    AND 
                                                                    (
                                                                       [Extent6].[Status] = 1
                                                                    )
                                                                    AND 
                                                                    (
                    ( [Project5].[CurrentPhase] = [Extent5].[Phase] ) 
                                                                       OR 
                                                                       (
                    ( [Project5].[CurrentPhase] IS NULL ) 
                                                                          AND 
                                                                          (
                                                                             [Extent5].[Phase] IS NULL
                                                                          )
                                                                       )
                                                                    )
                                                              )
                    ) 
                                                              OR 
                                                              (
                                                                 EXISTS 
                                                                 (
                                                                    SELECT
                                                                       1 AS [C1] 
                                                                    FROM
                                                                       [dbo].[SharedApproval] AS [Extent8] 
                                                                       INNER JOIN
                                                                          [dbo].[ExternalCollaborator] AS [Extent9] 
                                                                          ON [Extent8].[ExternalCollaborator] = [Extent9].[ID] 
                                                                    WHERE
                                                                       (
                                                                          [Extent8].[Approval] = [Project5].[ID] 
                                                                       )
                                                                       AND 
                                                                       (
                                                                          [Extent9].[Guid] = @p__linq__0
                                                                       )
                                                                       AND 
                                                                       (
                                                                          0 = [Extent9].[IsDeleted]
                                                                       )
                                                                       AND 
                                                                       (
                    ( [Extent8].[Phase] = [Project5].[CurrentPhase] ) 
                                                                          OR 
                                                                          (
                    ([Extent8].[Phase] IS NULL) 
                                                                             AND 
                                                                             (
                                                                                [Project5].[CurrentPhase] IS NULL 
                                                                             )
                                                                          )
                                                                       )
                                                                 )
                                                              )
                                                           )
                                                        THEN
                                                           cast(1 as bit) 
                                                        ELSE
                                                           cast(0 as bit) 
                                                     END
                                                     AS [C2] 
                                                  FROM
                                                     (
                                                        SELECT
                                                           [Project4].[ID] AS [ID],
                                                           [Project4].[Job] AS [Job],
                                                           [Project4].[CurrentPhase] AS [CurrentPhase],
                                                           [Project4].[CollaboratorGroup] AS [CollaboratorGroup],
                                                           [Project4].[Phase] AS [Phase],
                                                           CASE
                                                              WHEN
                                                                 (
                                                                    [Project4].[C1] IS NULL
                                                                 )
                                                              THEN
                                                                 cast(0 as bit) 
                                                              ELSE
                                                                 [Project4].[C2] 
                                                           END
                                                           AS [C1] 
                                                        FROM
                                                           (
                                                              SELECT
                                                                 [Project2].[ID] AS [ID],
                                                                 [Project2].[Job] AS [Job],
                                                                 [Project2].[CurrentPhase] AS [CurrentPhase],
                                                                 [Project2].[CollaboratorGroup] AS [CollaboratorGroup],
                                                                 [Project2].[Phase] AS [Phase],
                                                                 [Project2].[C1] AS [C1],
                                                                 (
                                                                    SELECT
                                                                       TOP (1) [Extent4].[IsPrimary] AS [IsPrimary] 
                                                                    FROM
                                                                       [dbo].[UserGroupUser] AS [Extent4] 
                                                                    WHERE
                                                                       (
                                                                          [Extent4].[UserGroup] = [Project2].[CollaboratorGroup] 
                                                                       )
                                                                       AND 
                                                                       (
                                                                          [Extent4].[IsPrimary] = 1
                                                                       )
                                                                 )
                                                                 AS [C2] 
                                                              FROM
                                                                 (
                                                                    SELECT
                                                                       [Extent1].[ID] AS [ID],
                                                                       [Extent1].[Job] AS [Job],
                                                                       [Extent1].[CurrentPhase] AS [CurrentPhase],
                                                                       [Extent2].[CollaboratorGroup] AS [CollaboratorGroup],
                                                                       [Extent2].[Phase] AS [Phase],
                                                                       (
                                                                          SELECT
                                                                             TOP (1) [Extent3].[IsPrimary] AS [IsPrimary] 
                                                                          FROM
                                                                             [dbo].[UserGroupUser] AS [Extent3] 
                                                                          WHERE
                                                                             (
                                                                                [Extent3].[UserGroup] = [Extent2].[CollaboratorGroup] 
                                                                             )
                                                                             AND 
                                                                             (
                                                                                [Extent3].[IsPrimary] = 1
                                                                             )
                                                                       )
                                                                       AS [C1] 
                                                                    FROM
                                                                       [dbo].[Approval] AS [Extent1] 
                                                                       INNER JOIN
                                                                          [dbo].[ApprovalCollaboratorGroup] AS [Extent2] 
                                                                          ON [Extent1].[ID] = [Extent2].[Approval] 																		--Where
                                                                          --([Extent1] .[Job] = 131350) 
                                                                 )
                                                                 AS [Project2] 
                                                           )
                                                           AS [Project4] 
                                                     )
                                                     AS [Project5] 
                                               )
                                               AS [Project8] 
                                            WHERE
                                               (
                                                  [Project8].[Job] = @p__linq__1
                                               )
                                               AND 
                                               (
                                                  [Project8].[C1] = 1
                                               )
                                               AND 
                                               (
                                                  [Project8].[C2] = 1
                                               )
                                               AND 
                                               (
                    ( [Project8].[Phase] = [Project8].[CurrentPhase] ) 
                                                  OR 
                                                  (
                    ([Project8].[Phase] IS NULL) 
                                                     AND 
                                                     (
                                                        [Project8].[CurrentPhase] IS NULL 
                                                     )
                                                  )
                                               )
                                         )
                                         AS [Project11] 
                                   )
                                   AS [Project12] 
                             )
                             AS [Project15] 
                       )
                       AS [Project16]";

            var links = context.Database.SqlQuery<UserGroupDecisionModel>(query,
                new SqlParameter("p__linq__0", userGuid),
                new SqlParameter("p__linq__1", jobId)
                ).ToList();

            

            int index = 1;
            foreach (var link in links)
            {
                link.ID = (jobId * 10000) + index++;
            }
            return links;
        }

        public List<InternalUseCollaboratorModel> GetInternalUserCollaborators(List<InternalUsePartialCollaboratorModel> internalUserPartialCollaborators)
        {
            var collaborators = new List<InternalUseCollaboratorModel>();
            foreach (var pc in internalUserPartialCollaborators)
            {
                collaborators.AddRange((from acc in context.Accounts                                       
                                            where
                                                pc.AccountID == acc.ID
                                        from ur in context.UserRoles
                                        join r in context.Roles on ur.Role equals r.ID
                                        join am in context.AppModules on r.AppModule equals am.ID
                                            where 
                                                pc.Collaborator == ur.User && am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate
                                        from c in context.ProofStudioUserColors
                                            where 
                                                pc.ProofStudioColor == c.ID
                                        select new InternalUseCollaboratorModel
                                        {
                                            ID = pc.ApprovalCollaboratorID,
                                            FamilyName = pc.FamilyName,
                                            GivenName = pc.GivenName,
                                            IsExternal = false,
                                            CollaboratorAvatar = "",
                                            CollaboratorRole = r.ID,
                                            Collaborator = pc.Collaborator,
                                            AssignedDate = pc.UserCreatedDate,
                                            UserColor = c.HEXValue,
                                            AccountID = pc.AccountID,
                                            AccountRegion = acc.Region,
                                            AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                            UserPhotoPath = pc.UserPhotoPath,
                                            UserGuid = pc.UserGuid,
                                            IsAcessRevoked = pc.IsAcessRevoked,
                                            SubstituteUserName = pc.SubstituteUserName,
                                            UserStatus = pc.UserStatus == ((int)UserStatu.Status.Inactive + 1) ? true : false
                                        }));
            }

            return collaborators.Distinct().ToList();
        }

        [Obsolete("This method is obsolete. Call GetInternalUserPartialCollaborators and GetInternalUserCollaborators instead.", false)]
        public List<InternalUseCollaboratorModel> GetInternalUserCollaborators(IEnumerable<int> versions, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            return (from a in context.Approvals
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    join u in context.Users on ac.Collaborator equals u.ID
                    join acc in context.Accounts on u.Account equals acc.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    join c in context.ProofStudioUserColors on u.ProofStudioColor equals c.ID

                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                 where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()
                    where
                        versions.Contains(a.ID) &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate &&
                        ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                    select new InternalUseCollaboratorModel
                    {
                        ID = ac.ID,
                        FamilyName = u.FamilyName,
                        GivenName = u.GivenName,
                        IsExternal = false,
                        CollaboratorAvatar = "",
                        CollaboratorRole = r.ID,
                        Collaborator = u.ID,
                        AssignedDate = u.CreatedDate,
                        UserColor = c.HEXValue,
                        AccountID = u.Account,
                        AccountRegion = acc.Region,
                        AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                        UserPhotoPath = u.PhotoPath,
                        UserGuid = u.Guid
                    }).Distinct().ToList();
        }

        public List<InternalUsePartialCollaboratorModel> GetExternalUserPartialCollaborators(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            var listOfExternalUsersInSharedApproval = (from sa in context.SharedApprovals
                     join a in context.Approvals on sa.Approval equals a.ID
                     where versionIds.Contains(a.ID)
                     select sa.ExternalCollaborator).Distinct().ToList();

            var externalUsersNotInSharedApprovalHasMadeAnnotation = (from aa in context.ApprovalAnnotations
                     join ap in context.ApprovalPages on aa.Page equals ap.ID
                     join a in context.Approvals on ap.Approval equals a.ID
                     join ec in context.ExternalCollaborators on aa.ExternalCreator equals ec.ID
                     where versionIds.Contains(a.ID)&& !listOfExternalUsersInSharedApproval.Contains(ec.ID)
                     select new InternalUsePartialCollaboratorModel
                     {
                         ApprovalCollaboratorID = ec.ID,
                         FamilyName = ec.FamilyName,
                         GivenName = ec.GivenName,
                         IsExternal = true,
                         Collaborator = ec.ID,
                         UserCreatedDate = ec.CreatedDate,
                         UserPhotoPath = string.Empty,
                         UserGuid = ec.Guid,
                         UserColor = ec.ProofStudioColor,
                         ExpireDate = DateTime.UtcNow,
                         IsExpired = false,
                         ApprovalCollaboratorRole = 1,
                         Approval_version = a.ID,
                         IsLocked = a.IsLocked,
                         CurrentPhase = a.CurrentPhase,
                         Phase = aa.Phase,
                         Version = a.Version,
                         Owner = a.Owner,
                         Job = a.Job,
                         ApprovalId = a.ID,
                         IsAcessRevoked = true
                     }).Distinct().ToList();

            var externalUsersInSharedApproval = (from sa in context.SharedApprovals
                    join a in context.Approvals on sa.Approval equals a.ID                    
                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID

                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                 where ap.Approval == a.ID && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()
                    where
                        versionIds.Contains(a.ID) && a.IsDeleted == false && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                    select new InternalUsePartialCollaboratorModel
                    {                    
                        ApprovalCollaboratorID = ec.ID,// = sa.ExternalCollaborator   
                        FamilyName = ec.FamilyName,
                        GivenName = ec.GivenName,
                        IsExternal = true,
                        Collaborator = ec.ID, // = sa.ExternalCollaborator                     
                        UserCreatedDate = ec.CreatedDate,
                        //AccountID = ac.Account,
                        UserPhotoPath = string.Empty,
                        UserGuid = ec.Guid,
                        UserColor = ec.ProofStudioColor,

                        ExpireDate = sa.ExpireDate,
                        IsExpired = sa.IsExpired,
                        ApprovalCollaboratorRole = sa.ApprovalCollaboratorRole,
                        Approval_version = sa.Approval,
                        IsLocked = a.IsLocked,
                        CurrentPhase = a.CurrentPhase,
                        Phase = sa.Phase,
                        Version = a.Version,
                        Owner = a.Owner,
                        Job = a.Job,
                        ApprovalId = a.ID,
                        IsAcessRevoked = false
                    }).Distinct().ToList();

            if(externalUsersNotInSharedApprovalHasMadeAnnotation.Count > 0)
            {
                externalUsersInSharedApproval.AddRange(externalUsersNotInSharedApprovalHasMadeAnnotation);
            }

            return externalUsersInSharedApproval;
        }


        public List<InternalUseCollaboratorModel> GetExternalUserCollaborators(List<InternalUsePartialCollaboratorModel> partialExternalCollaborates)
        {
            var internalUseCollaborators = new List<InternalUseCollaboratorModel>();
            foreach (var partialExternalCollaborate in partialExternalCollaborates)
            {
                internalUseCollaborators.AddRange(
                    (from j in context.Jobs
                    join ac in context.Accounts on j.Account equals ac.ID
                    where j.ID == partialExternalCollaborate.Job
                    select new InternalUseCollaboratorModel
                            {
                                ID = partialExternalCollaborate.Collaborator,
                                FamilyName = partialExternalCollaborate.FamilyName,
                                GivenName = partialExternalCollaborate.GivenName,
                                IsExternal = true,
                                CollaboratorAvatar = "",
                                CollaboratorRole = partialExternalCollaborate.ApprovalCollaboratorRole,
                                Collaborator = partialExternalCollaborate.Collaborator,
                                AssignedDate = partialExternalCollaborate.UserCreatedDate,
                                UserColor = partialExternalCollaborate.UserColor,
                                AccountID = ac.ID,
                                AccountRegion = ac.Region,
                                AccountDomain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                UserPhotoPath = partialExternalCollaborate.UserPhotoPath,
                                UserGuid = partialExternalCollaborate.UserGuid,
                                IsAcessRevoked = partialExternalCollaborate.IsAcessRevoked
                            })
                );
            }

            return internalUseCollaborators.Distinct().ToList();
        }

        [Obsolete("This method is obsolete. Call GetExternalUserPartialCollaborators and GetExternalUserCollaborators instead.", false)]
        public List<InternalUseCollaboratorModel> GetExternalUserCollaborators(IEnumerable<int> versions, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            return (from sa in context.SharedApprovals
                    join a in context.Approvals on sa.Approval equals a.ID
                    join appp in context.ApprovalPages on a.ID equals appp.Approval
                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals
                        ec.ID
                    join j in context.Jobs on a.Job equals j.ID
                    join ac in context.Accounts on j.Account equals ac.ID

                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 where appp.ID == aan.Page && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()
                    where
                        versions.Contains(a.ID) && a.IsDeleted == false && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                    select new InternalUseCollaboratorModel
                    {
                        ID = ec.ID,
                        FamilyName = ec.FamilyName,
                        GivenName = ec.GivenName,
                        IsExternal = true,
                        CollaboratorAvatar = "",
                        CollaboratorRole = sa.ApprovalCollaboratorRole,
                        Collaborator = ec.ID,
                        AssignedDate = ec.CreatedDate,
                        UserColor = ec.ProofStudioColor,
                        AccountID = ac.ID,
                        AccountRegion = ac.Region,
                        AccountDomain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                        UserPhotoPath = string.Empty,
                        UserGuid = ec.Guid
                    }).Distinct().ToList();
        }

        public List<string> GetPredefinedUserColors(int accountID, List<string> avoidColors)
        {
            return (from c in context.ProofStudioUserColors select c.HEXValue)
                                            .Concat(from e in context.ExternalCollaborators where e.Account == accountID select e.ProofStudioColor)
                                            .Concat(avoidColors)
                                            .ToList();
        }

        public void AttachColorToExternalCollaborator(int externalCollaboratorID, string userColor)
        {
            context.ExternalCollaborators.Where(c => c.ID == externalCollaboratorID).FirstOrDefault().ProofStudioColor = userColor;
        }

        public List<InternalUseCollaboratorLinkModel> GetInternalUserCollaboratorLinksForUpdateApprovalDecision(int jobId, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            return (from ac in context.ApprovalCollaborators
                    join u in context.Users on ac.Collaborator equals u.ID
                    join a in context.Approvals on ac.Approval equals a.ID
                    let decision = (from acd in context.ApprovalCollaboratorDecisions
                                    where
                                        acd.Collaborator == u.ID && acd.Approval == a.ID &&
                                        acd.Phase == a.CurrentPhase
                                    select acd.Decision).FirstOrDefault()
                    let primaryGroup = (from ug in context.UserGroups
                                        join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                        join acg in context.ApprovalCollaboratorGroups on ug.ID equals
                                            acg.CollaboratorGroup
                                        where
                                            ugu.User == u.ID && ugu.IsPrimary && acg.Approval == a.ID &&
                                            acg.Phase == a.CurrentPhase
                                        select ug.ID).FirstOrDefault()

                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                 where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()

                    where
                        jobId == a.Job && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                    select new InternalUseCollaboratorLinkModel
                    {
                        ID = ac.Collaborator,
                        ExpireDate = u.CreatedDate,
                        IsExpired = false,
                        approval_role = ac.ApprovalCollaboratorRole,
                        approval_version = ac.Approval,
                        PrimaryGroup = primaryGroup,
                        Decision = decision,
                        CurrentPhase = a.CurrentPhase,
                        IsLocked = a.IsLocked,
                        Phase = ac.Phase,
                        Version = a.Version
                    }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new VersionPhaseModel
                                                                                                        {
                                                                                                            Version = e.Version,
                                                                                                            Phase = e.Phase
                                                                                                        }).FirstOrDefault()).ToList();
        }

        public List<InternalUseCollaboratorLinkModel> GetExternalUserCollaboratorLinksForUpdateApprovalDecision(int jobId, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            return (from sa in context.SharedApprovals
                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                    join a in context.Approvals on sa.Approval equals a.ID
                    let decision = (from acd in context.ApprovalCollaboratorDecisions
                                    where
                                        acd.ExternalCollaborator == ec.ID && acd.Approval == a.ID &&
                                        acd.Decision != null && acd.Phase == a.CurrentPhase
                                    select acd.Decision).Take(1).FirstOrDefault()

                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                 where ap.Approval == a.ID && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()
                    where
                        jobId == a.Job && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                    select new InternalUseCollaboratorLinkModel
                    {
                        ID = sa.ExternalCollaborator,
                        ExpireDate = sa.ExpireDate,
                        IsExpired = sa.IsExpired,
                        approval_role = sa.ApprovalCollaboratorRole,
                        approval_version = sa.Approval,
                        PrimaryGroup = 0,
                        Decision = decision,
                        CurrentPhase = a.CurrentPhase,
                        IsLocked = a.IsLocked,
                        Phase = sa.Phase,
                        Version = a.Version
                    }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new VersionPhaseModel
                                                                                                        {
                                                                                                            Version = e.Version,
                                                                                                            Phase = e.Phase
                                                                                                        }).FirstOrDefault()).ToList();
        }

        public List<InternalUsePartialCollaboratorModel> GetInternalUserPartialCollaborators(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
           
            var listOfUsersInApprovalCollaborator = (from ac in context.ApprovalCollaborators
                     join a in context.Approvals on ac.Approval equals a.ID
                     where versionIds.Contains(a.ID)
                     select ac.Collaborator).Distinct().ToList();

            var usersNotInCollaboratorHasMadeAnnotation = (from aa in context.ApprovalAnnotations
                     join ap in context.ApprovalPages on aa.Page equals ap.ID
                     join a in context.Approvals on ap.Approval equals a.ID
                     join u in context.Users on aa.Creator equals u.ID
                     where versionIds.Contains(a.ID) && !listOfUsersInApprovalCollaborator.Contains((int)aa.Creator)
                     select new InternalUsePartialCollaboratorModel
                     {
                         ApprovalCollaboratorID = 0,
                         FamilyName = u.FamilyName,
                         GivenName = u.GivenName,
                         IsExternal = false,
                         Collaborator = u.ID,
                         UserCreatedDate = u.CreatedDate,
                         AccountID = u.Account,
                         UserPhotoPath = u.PhotoPath,
                         UserGuid = u.Guid,
                         ProofStudioColor = u.ProofStudioColor,
                         ApprovalCollaboratorRole = 1,
                         Approval_version = a.ID,
                         IsLocked = a.IsLocked,
                         CurrentPhase = a.CurrentPhase,
                         Phase = aa.Phase,
                         Version = a.Version,
                         Owner = a.Owner,
                         Job = a.Job,
                         ApprovalId = a.ID,
                         IsAcessRevoked = true,
                         UserStatus = u.Status
                     }).Distinct().ToList();
                     
                                

            var usersInApprovalCollaborator = (from ac in context.ApprovalCollaborators
                    join a in context.Approvals on ac.Approval equals a.ID
                    join u in context.Users on ac.Collaborator equals u.ID
                    //join us in context.UserStatus on u.Status equals us.ID
                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                 where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()

                    let substituteuser = (from u in context.Users
                                          where u.ID == ac.SubstituteUser
                                          select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                        ).FirstOrDefault()
                                               where
                       versionIds.Contains(a.ID) && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                    select new InternalUsePartialCollaboratorModel
                    {
                        ApprovalCollaboratorID = ac.ID,
                        FamilyName = u.FamilyName,
                        GivenName = u.GivenName,
                        IsExternal = false,                        
                        Collaborator = u.ID, // = ac.Collaborator                        
                        UserCreatedDate = u.CreatedDate,
                        AccountID = u.Account,
                        UserPhotoPath = u.PhotoPath,
                        UserGuid = u.Guid,
                        ProofStudioColor = u.ProofStudioColor,

                        ApprovalCollaboratorRole = ac.ApprovalCollaboratorRole,
                        Approval_version = ac.Approval,
                        IsLocked = a.IsLocked,
                        CurrentPhase = a.CurrentPhase,
                        Phase = ac.Phase,
                        Version = a.Version,
                        Owner = a.Owner,
                        Job = a.Job,
                        ApprovalId = a.ID,
                        IsAcessRevoked = false,
                        SubstituteUserName = substituteuser == null ? "" : substituteuser,
                        UserStatus = u.Status
                    }).Distinct().ToList();
            if(usersNotInCollaboratorHasMadeAnnotation.Count > 0)
            {
                usersInApprovalCollaborator.AddRange(usersNotInCollaboratorHasMadeAnnotation);
            }
           return usersInApprovalCollaborator;
        }

        public List<InternalUseCollaboratorLinkModel> GetInternalUserCollaboratorLinks(List<InternalUsePartialCollaboratorModel> internalUserPartialCollaborators)
        {
            var collaboratorLinks = new List<InternalUseCollaboratorLinkModel>();

            foreach(var pc in internalUserPartialCollaborators)
            {
                collaboratorLinks.AddRange(
                        from acr in context.ApprovalCollaboratorRoles
                            where 
                                pc.ApprovalCollaboratorRole == acr.ID
                        let decision = (from acd in context.ApprovalCollaboratorDecisions
                                        where
                                            acd.Collaborator == pc.Collaborator && acd.Approval == pc.ApprovalId &&
                                            acd.Phase == pc.CurrentPhase
                                        select acd.Decision).Take(1).FirstOrDefault()
                        let primaryGroup = (from ug in context.UserGroups
                                            join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                            join acg in context.ApprovalCollaboratorGroups on ug.ID equals
                                                acg.CollaboratorGroup
                                            where ugu.User == pc.Collaborator && ugu.IsPrimary && acg.Approval == pc.ApprovalId
                                            select ug.ID).FirstOrDefault()


                        let jobOwner = (from j in context.Jobs where j.ID == pc.Job select j.JobOwner).FirstOrDefault()

                        select new InternalUseCollaboratorLinkModel
                        {
                            ID = pc.Collaborator,
                            ExpireDate = pc.UserCreatedDate,
                            IsExpired = false,
                            approval_role = pc.ApprovalCollaboratorRole,
                            approval_version = pc.Approval_version,
                            PrimaryGroup = primaryGroup,
                            Decision = decision,
                            Permission = acr.Key,
                            IsLocked = pc.IsLocked,
                            CurrentPhase = pc.CurrentPhase,
                            Phase = pc.Phase,
                            Version = pc.Version,
                            Owner = pc.Owner,
                            Job = pc.Job,
                            JobOwner = jobOwner,
                            ApprovalId = pc.ApprovalId
                        });
                    
            }

            return collaboratorLinks.GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new VersionPhaseModel
            {
                Version = e.Version,
                Phase = e.Phase
            }).FirstOrDefault()).ToList();
        }

        [Obsolete("This method is obsolete. Call GetInternalUserPartialCollaborators and GetInternalUserCollaboratorLinks instead.", false)]
        public List<InternalUseCollaboratorLinkModel> GetInternalUserCollaboratorLinks(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            return (from ac in context.ApprovalCollaborators
                   join acr in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole equals
                       acr.ID
                   join u in context.Users on ac.Collaborator equals u.ID
                   join us in context.UserStatus on u.Status equals us.ID
                   join a in context.Approvals on ac.Approval equals a.ID
                   let decision = (from acd in context.ApprovalCollaboratorDecisions
                                   where
                                       acd.Collaborator == u.ID && acd.Approval == a.ID &&
                                       acd.Phase == a.CurrentPhase
                                   select acd.Decision).Take(1).FirstOrDefault()
                   let primaryGroup = (from ug in context.UserGroups
                                       join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                       join acg in context.ApprovalCollaboratorGroups on ug.ID equals
                                           acg.CollaboratorGroup
                                       where ugu.User == u.ID && ugu.IsPrimary && acg.Approval == a.ID
                                       select ug.ID).FirstOrDefault()

                   let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                select aan.ID).Any()

                   let jobOwner = (from j in context.Jobs where j.ID == a.Job select j.JobOwner).FirstOrDefault()

                   where
                       versionIds.Contains(a.ID) && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                   select new InternalUseCollaboratorLinkModel
                   {
                       ID = ac.Collaborator, 
                       ExpireDate = u.CreatedDate,
                       IsExpired = false,
                       approval_role = ac.ApprovalCollaboratorRole,
                       approval_version = ac.Approval,
                       PrimaryGroup = primaryGroup,
                       Decision = decision,
                       Permission = acr.Key,
                       IsLocked = a.IsLocked,
                       CurrentPhase = a.CurrentPhase,
                       Phase = ac.Phase,
                       Version = a.Version,
                       Owner = a.Owner,
                       Job = a.Job,
                       JobOwner = jobOwner,
                       ApprovalId = a.ID
                   }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new VersionPhaseModel
                                                                                                       {
                                                                                                           Version = e.Version,
                                                                                                           Phase = e.Phase
                                                                                                       }).FirstOrDefault()).ToList();
        }
        public List<InternalUseCollaboratorLinkModel> GetExternalUserCollaboratorLinks(List<InternalUsePartialCollaboratorModel> partialExternalCollaborates)
        {
            var collaborators = new List<InternalUseCollaboratorLinkModel>();

            foreach (var pc in partialExternalCollaborates)
            {
                collaborators.AddRange(
                    (from acr in context.ApprovalCollaboratorRoles
                     where
                         pc.ApprovalCollaboratorRole == acr.ID

                     let decision = (from acd in context.ApprovalCollaboratorDecisions
                                     where
                                         acd.ExternalCollaborator == pc.Collaborator && acd.Approval == pc.ApprovalId &&
                                         acd.Decision != null && acd.Phase == pc.CurrentPhase
                                     select acd.Decision).Take(1).FirstOrDefault()

                     let jobOwner = (from j in context.Jobs where j.ID == pc.Job select j.JobOwner).FirstOrDefault()

                     select new InternalUseCollaboratorLinkModel
                     {
                         ID = pc.Collaborator,
                         ExpireDate = pc.ExpireDate,
                         IsExpired = pc.IsExpired,
                         approval_role = pc.ApprovalCollaboratorRole,
                         approval_version = pc.Approval_version,
                         PrimaryGroup = 0,
                         Decision = decision,
                         Permission = acr.Key,
                         IsLocked = pc.IsLocked,
                         CurrentPhase = pc.CurrentPhase,
                         Phase = pc.Phase,
                         Version = pc.Version,
                         Owner = pc.Owner,
                         Job = pc.Job,
                         JobOwner = jobOwner,
                         ApprovalId = pc.ApprovalId
                     }));
            }

            return collaborators.GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new VersionPhaseModel
                                                                                                        {
                                                                                                            Version = e.Version,
                                                                                                            Phase = e.Phase
                                                                                                        }).FirstOrDefault()).ToList();
        }

        [Obsolete("This method is obsolete. Call GetExternalUserPartialCollaborators and GetExternalUserCollaboratorLinks instead.", false)]
        public List<InternalUseCollaboratorLinkModel> GetExternalUserCollaboratorLinks(List<int> versionIds, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            return (from sa in context.SharedApprovals
                    join acr in context.ApprovalCollaboratorRoles on sa.ApprovalCollaboratorRole equals acr.ID
                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                    join a in context.Approvals on sa.Approval equals a.ID
                    let decision = (from acd in context.ApprovalCollaboratorDecisions
                                    where
                                        acd.ExternalCollaborator == ec.ID && acd.Approval == a.ID &&
                                        acd.Decision != null && acd.Phase == a.CurrentPhase
                                    select acd.Decision).Take(1).FirstOrDefault()

                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                 where ap.Approval == a.ID && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                 select aan.ID).Any()

                    let jobOwner = (from j in context.Jobs where j.ID == a.Job select j.JobOwner).FirstOrDefault()

                    where
                        versionIds.Contains(a.ID) && ((userCanViewAllPhasesAnnotations && userHasAnnotations &&
                            visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                    select new InternalUseCollaboratorLinkModel
                    {
                        ID = sa.ExternalCollaborator,
                        ExpireDate = sa.ExpireDate,
                        IsExpired = sa.IsExpired,
                        approval_role = sa.ApprovalCollaboratorRole,
                        approval_version = sa.Approval,
                        PrimaryGroup = 0,
                        Decision = decision,
                        Permission = acr.Key,
                        IsLocked = a.IsLocked,
                        CurrentPhase = a.CurrentPhase,
                        Phase = sa.Phase,
                        Version = a.Version,
                        Owner = a.Owner,
                        Job = a.Job,
                        JobOwner = jobOwner,
                        ApprovalId = a.ID
                    }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new VersionPhaseModel
                                                                                                    {
                                                                                                        Version = e.Version,
                                                                                                        Phase = e.Phase
                                                                                                    }).FirstOrDefault()).ToList();
        }

        public ApprovalCollaboratorsAndDecisionsModel GetAllCollaboratorsAndDecisions(int approvalId, int? phaseId, int approverAndReviewerRoleID)
        {
            var approvalCollaboratorsAndDecisions = (from a in context.Approvals
                                                     where a.ID == approvalId

                    let internalCollaboratorsDecision = (from
                                                            acd in context.ApprovalCollaboratorDecisions
                                                        where
                                                            acd.Phase == phaseId &&
                                                            acd.Collaborator.HasValue &&
                                                            acd.Decision != null &&
                                                            acd.Approval == approvalId
                                                         select 
                                                            acd.Collaborator.Value).Distinct()

                    let externalCollaboratorsDecision = (from 
                                                            acd in context.ApprovalCollaboratorDecisions
                                                        where 
                                                            acd.Phase == phaseId && 
                                                            acd.ExternalCollaborator.HasValue && 
                                                            acd.Decision != null &&
                                                            acd.Approval == approvalId
                                                         select
                                                            acd.ExternalCollaborator.Value).Distinct()
                                                                  
                    let internalCollaborators = (from 
                                                    ac in context.ApprovalCollaborators
                                                where 
                                                    ac.Phase == phaseId &&
                                                    ac.ApprovalCollaboratorRole == approverAndReviewerRoleID &&
                                                    ac.Approval == approvalId
                                                 select 
                                                    ac.Collaborator).Distinct()
                                                         
                    let externalCollaborators = (from 
                                                    sa in context.SharedApprovals
                                                where 
                                                    sa.Phase == phaseId &&
                                                    sa.ApprovalCollaboratorRole == approverAndReviewerRoleID &&
                                                    sa.Approval == approvalId
                                                 select 
                                                    sa.ExternalCollaborator).Distinct()

                    select new ApprovalCollaboratorsAndDecisionsModel {
                        InternalCollaboratorsDecision = internalCollaboratorsDecision.ToList(),
                        ExternalCollaboratorsDecision = externalCollaboratorsDecision.ToList(),
                        InternalCollaborators = internalCollaborators.ToList(),
                        ExternalCollaborators = externalCollaborators.ToList()
                    });

            return approvalCollaboratorsAndDecisions.FirstOrDefault();
        }

        public ApprovalCollaboratorsAndAnnotationsModel GetAllCollaboratorsAndAnnotations(int approvalId, int? phaseId, int[] approverAndReviewerRolesIDs)
        {
            var collaboratorsAndAnnotations = (from a in context.Approvals
                                               where a.ID == approvalId

                                               let internalCollaborators = (from
                                                                                ac in context.ApprovalCollaborators
                                                                            where
                                                                                ac.Phase == phaseId &&
                                                                                approverAndReviewerRolesIDs.Contains(ac.ApprovalCollaboratorRole) &&
                                                                                ac.Approval == approvalId
                                                                            select
                                                                                ac.Collaborator).Distinct()

                                               let externalCollaborators = (from
                                                                                sa in context.SharedApprovals
                                                                            where
                                                                                sa.Phase == phaseId &&
                                                                                approverAndReviewerRolesIDs.Contains(sa.ApprovalCollaboratorRole) &&
                                                                                sa.Approval == approvalId
                                                                            select
                                                                                sa.ExternalCollaborator).Distinct()
                                                                                
                                                let internalCollaboratorsAnnotations = (from 
                                                                                            a in context.Approvals
                                                                                        join 
                                                                                            ap in context.ApprovalPages on a.ID equals ap.Approval
                                                                                        join 
                                                                                            an in context.ApprovalAnnotations on ap.ID equals an.Page
                                                                                        where 
                                                                                            an.Phase == phaseId && 
                                                                                            an.Creator.HasValue &&
                                                                                            a.ID == approvalId
                                                                                        select
                                                                                            an.Creator.Value).Distinct()

                                               let externalCollaboratorsAnnotations = (from
                                                                                           a in context.Approvals
                                                                                       join
                                                                                           ap in context.ApprovalPages on a.ID equals ap.Approval
                                                                                       join
                                                                                           an in context.ApprovalAnnotations on ap.ID equals an.Page
                                                                                       where
                                                                                           an.Phase == phaseId &&
                                                                                           an.ExternalCreator.HasValue &&
                                                                                           a.ID == approvalId
                                                                                       select
                                                                                           an.ExternalCreator.Value).Distinct()

                                               select new ApprovalCollaboratorsAndAnnotationsModel {
                                                    InternalCollaborators = internalCollaborators.ToList(),
                                                    ExternalCollaborators = externalCollaborators.ToList(), 
                                                    InternalAnnotationsCreators = internalCollaboratorsAnnotations.ToList(),
                                                    ExternalAnnotationsCreators = externalCollaboratorsAnnotations.ToList()
                                                }).FirstOrDefault();

            return collaboratorsAndAnnotations;
        }
    }
}
