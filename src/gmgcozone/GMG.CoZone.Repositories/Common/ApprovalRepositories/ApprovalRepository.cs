﻿using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Common;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMGColorDAL;
using GMG.CoZone.Collaborate.DomainModels;
using System;
using System.Data.Entity.Core.Common.CommandTrees;

namespace GMG.CoZone.Repositories.Common.ApprovalRepositories
{
    public class ApprovalRepository : GenericRepository<Approval>, IApprovalRepository
    {
        public ApprovalRepository(GMGColorContext context) : base(context)
        {
        }

        public Approval.ApprovalTypeEnum GetApprovalType(int approvalId)
        {
            var approvalTypeKey = Get(ap => ap.ID == approvalId).Join(context.ApprovalTypes, a => a.Type, at => at.ID, (a,at) => new { atKey = at.Key }).FirstOrDefault();

            if (approvalTypeKey == null) // it should not be null
                return Approval.ApprovalTypeEnum.Image;

            return (Approval.ApprovalTypeEnum)approvalTypeKey.atKey;

        }

        public string GetApprovalTimezone(int approvalId)
        {
            return (from aj in context.Approvals
                    join j in context.Jobs on aj.Job equals j.ID
                    join a in context.Accounts on j.Account equals a.ID
                    where aj.ID == approvalId && aj.IsDeleted == false
                    select a.TimeZone).Take(1).FirstOrDefault();
        }

        public ApprovalInternalUseModel GetApprovalForExternalUser(int approvalId, string userKey)
        {
            return (from a in context.Approvals
             join at in context.ApprovalTypes on a.Type equals at.ID
             join j in context.Jobs on a.Job equals j.ID
             join sa in context.SharedApprovals on a.ID equals sa.Approval
             join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
             where
                 a.ID == approvalId &&
                 a.IsDeleted == false &&
                 ec.Guid.ToLower() == userKey.ToLower() && !ec.IsDeleted && !sa.IsExpired
             orderby a.Version ascending
             select new ApprovalInternalUseModel
             {
                 ID = a.ID,
                 ErrorMessage = a.ErrorMessage,
                 Creator = a.Creator,
                 ApprovalTypeName = at.Name,
                 Title = j.Title,
                 CurrentPhase = a.CurrentPhase,
                 Account = j.Account,
                 Job = j.ID,
             }).Take(1).FirstOrDefault();
        }

        public ApprovalInternalUseModel GetApprovalForInternalUser(int approvalId, int loggedUserId, bool userIsAdmin, bool userIsManager)
        { 
            List<int> collaborators = (from ac in context.ApprovalCollaborators
                                       where ac.Approval == approvalId
                                       select ac.Collaborator).ToList();
            if (userIsAdmin)
            {
                collaborators.Add(loggedUserId);
            }
            if(userIsManager)
            {
                collaborators.Add(loggedUserId);
            }

            return (from a in context.Approvals
                    join at in context.ApprovalTypes on a.Type equals at.ID
                    join j in context.Jobs on a.Job equals j.ID
                    where
                        a.ID == approvalId &&
                        a.IsDeleted == false &&
                        (collaborators.Contains(loggedUserId) || j.JobOwner == loggedUserId)
                    orderby a.Version ascending
                    select new ApprovalInternalUseModel
                    {
                        ID = a.ID,
                        ErrorMessage = a.ErrorMessage,
                        Creator = a.Creator,
                        ApprovalTypeName = at.Name,
                        Title = j.Title,
                        Job = j.ID,
                    }).FirstOrDefault();
        }

        public int GetJobId(int approvalId)
        {
            return (from a in context.Approvals
                        where a.ID == approvalId
                        select a.Job).Take(1).FirstOrDefault();
        }

        public List<InternalUseVersionModel> GetExternalUserVersions(int jobId, int approvalId, int? currentPhase, string userKey)
        {
            return (from aap in context.Approvals
                    join j in context.Jobs on aap.Job equals j.ID
                    join at in context.ApprovalTypes on aap.Type equals at.ID
                    join sa in context.SharedApprovals on aap.ID equals sa.Approval
                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                    join acc in context.Accounts on ec.Account equals acc.ID

                    where
                        aap.Job == jobId && aap.IsDeleted == false && !aap.DeletePermanently &&
                        ec.Guid.ToLower() == userKey.ToLower() && sa.IsExpired == false &&
                        ec.IsDeleted == false  && aap.ID != approvalId
                    select new InternalUseVersionModel
                    {
                        ID = aap.ID,
                        Title = j.Title,
                        Guid = aap.Guid,
                        AccountRegion = acc.Region,
                        AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                        VersionNumber = aap.Version,
                        IsVideo = at.Key == (int)GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                        VersionLabel = aap.VersionSufix,
                    }).GroupBy(x => x.ID, (key, g) => g.FirstOrDefault()).ToList();
        }
        public List<InternalUseVersionModel> GetInternalUserVersions(int jobId, int approvalId, int? currentPhase, string userKey, bool isAdministrator)
        {
            return (from aap in context.Approvals
                    join j in context.Jobs on aap.Job equals j.ID
                    join at in context.ApprovalTypes on aap.Type equals at.ID
                    join ac in context.ApprovalCollaborators on aap.ID equals ac.Approval
                    join u in context.Users on ac.Collaborator equals u.ID
                    join acc in context.Accounts on u.Account equals acc.ID

                    where
                        aap.Job == jobId && aap.IsDeleted == false && !aap.DeletePermanently &&
                        u.Guid.ToLower() == userKey.ToLower() && aap.ID != approvalId
                    select new InternalUseVersionModel
                    {
                        ID = aap.ID,
                        Title = aap.FileName,
                        Guid = aap.Guid,
                        AccountRegion = acc.Region,
                        AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                        VersionNumber = aap.Version,
                        IsVideo = at.Key == (int)GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                        VersionLabel = aap.VersionSufix
                    }).GroupBy(x => x.ID, (key, g) => g.FirstOrDefault()).ToList();
        }

        public int? GetVersionParent(int versionId)
        {
            return (from pa in context.Approvals
                    join j in context.Jobs on pa.Job equals j.ID
                    where pa.ID == versionId && pa.Version == 1
                    select pa.ID).Take
                            (1).FirstOrDefault();
        }

        public InternalUseDetailedVersionModel GetDetailedVersion(int versionId)
        {
            return (from a in context.Approvals
                    join at in context.ApprovalTypes on a.Type equals at.ID
                    join j in context.Jobs on a.Job equals j.ID
                    join js in context.JobStatus on j.Status equals js.ID
                    join acc in context.Accounts on j.Account equals acc.ID
                    join ft in context.FileTypes on a.FileType equals ft.ID

                    let totalPagesCount = (from ap in context.ApprovalPages
                                           where ap.Approval == a.ID
                                           select ap.ID).Count()

                    let docEmbeddedProfile = (from apep in context.ApprovalEmbeddedProfiles where apep.Approval == a.ID select apep).FirstOrDefault()
                    let firstPageId = (from ap in context.ApprovalPages where ap.Approval == a.ID select ap.ID).Take(1).FirstOrDefault()

                    let phaseName = (from ajp in context.ApprovalJobPhases
                                     where ajp.ID == a.CurrentPhase
                                     select ajp.Name).FirstOrDefault()
                    let phaseCompleted = (from apja in context.ApprovalJobPhaseApprovals
                                          where apja.Phase == a.CurrentPhase
                                          orderby apja.ID descending
                                          select apja.PhaseMarkedAsCompleted).FirstOrDefault()
                    let shouldBeViewedByAll = (from ajp in context.ApprovalJobPhases
                                               join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
                                               where ajp.ID == a.CurrentPhase
                                               select aph.ApprovalShouldBeViewedByAll).FirstOrDefault()
                    where versionId == a.ID
                    select new InternalUseDetailedVersionModel
                    {
                        ID = a.ID,
                        Guid = a.Guid,
                        ErrorMessage = a.ErrorMessage,
                        IsDeleted = a.IsDeleted,
                        IsLocked = a.IsLocked,
                        Creator = a.Creator,
                        ApprovalTypeName = at.Name,
                        AllowDownloadOriginal = a.AllowDownloadOriginal,
                        RestrictedZoom = a.RestrictedZoom,
                        VersionNumber = a.Version,
                        FileName = a.FileName,
                        Extention = ft.Extention,
                        AccountRegion = acc.Region,
                        ApprovalType = at.Key,
                        AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                        isVideo = at.Key == (int)GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                        FirstPageNumber = 1,
                        TotalPagesCount = totalPagesCount,
                        JobStatus = js.Key,
                        IsRGBColorSpace = (docEmbeddedProfile != null ? docEmbeddedProfile.IsRGB : (from asp in context.ApprovalSeparationPlates
                                                                                                    where
                                                                                                        asp.Page == firstPageId &&
                                                                                                        (asp.Name.ToLower() == Constants.REDPLATE ||
                                                                                                        asp.Name.ToLower() == Constants.GREENPLATE ||
                                                                                                        asp.Name.ToLower() == Constants.BLUEPLATE)
                                                                                                    select asp.ID).Count() == 3),
                        CurrentPhase = a.CurrentPhase,
                        PhaseName = phaseName,
                        VersionLabel = a.VersionSufix,
                        IsPhaseCompleted = phaseCompleted,
                        shouldBeViewedByAll = shouldBeViewedByAll
                    }).FirstOrDefault();
        }

        public bool IsLastVersion(int approvalId)
        {
            var jobId = context.Approvals.Where(a => a.ID == approvalId).Select(a => a.Job).FirstOrDefault();
            var maxVersion = context.Approvals.Where(a => a.Job == jobId).OrderByDescending(v => v.Version).FirstOrDefault();
            return maxVersion != null && approvalId == maxVersion.ID;
        }

        public List<ApprovalBasicDataModel> GetApprovalsBasics(List<int> approvalsIds)
        {

            return (from a in context.Approvals
                    join ap in context.ApprovalPages on a.ID equals ap.Approval
                    let privateAnnotations = (from aa in context.Approvals where approvalsIds.Contains(aa.ID) && aa.PrimaryDecisionMaker > 0 select
                                              aa.PrivateAnnotations).FirstOrDefault()
                    where approvalsIds.Contains(a.ID) && ap.Number == 1// first page                
                    select new ApprovalBasicDataModel() {
                        ID = a.ID,
                        ApprovalId = a.ID,
                        JobId = a.Job,
                        CurrentPhaseId = a.CurrentPhase,
                        FirstPageId = ap.ID,
                        PDM = privateAnnotations ? a.PrimaryDecisionMaker : null
                }).ToList();
        }

        public int? GetApprovalIdForTranscoding(string transcodingJobId)
        {
            return (from ap in context.Approvals where ap.TranscodingJobId == transcodingJobId select ap.ID).FirstOrDefault();
        }

        public string GetVideoThumbnailPath(ApprovalInfoModel approvalInfo)
        {
            return Approval.GetVideoImagePath(approvalInfo.Guid, approvalInfo.Region, approvalInfo.Domain);
        }

        public List<ApprovalFileModel> GetApprovalsByIdAndCollaborator(List<int> approvalIds, int loggedUserId, bool adminCanViewAllApprovals)
        {
            return (from a in context.Approvals
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    where approvalIds.Contains(a.ID) && a.AllowDownloadOriginal && (a.IsDeleted == false && a.DeletePermanently == false) && (adminCanViewAllApprovals || ac.Collaborator == loggedUserId)
                    select new ApprovalFileModel
                    {
                        Id = a.ID,
                        Guid = a.Guid,
                        FileName = (a.FileType == 30) ? a.FileName.Replace(".pdf",".doc") : (a.FileType == 31) ? a.FileName.Replace(".pdf",".docx") : (a.FileType == 32) ? a.FileName.Replace(".pdf", ".ppt"): (a.FileType == 33) ? a.FileName.Replace(".pdf", ".pptx") :  a.FileName
                    }).Distinct().ToList();
        }

        public List<int> GetApprovalJobIds(List<int> approvalIds)
        {
            return Get(a => approvalIds.Contains(a.ID)).Select(a => a.Job).Distinct().ToList();
        }

        public List<ApprovalFileModel> GetApprovalVersionsByJobIdAndCollaborator(List<int> jobIds, int loggedUserId, bool adminCanViewAllApprovals)
        {
            return (from a in context.Approvals
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    where jobIds.Contains(a.Job) && a.AllowDownloadOriginal && (a.IsDeleted == false && a.DeletePermanently == false) && (adminCanViewAllApprovals || ac.Collaborator == loggedUserId)
                    select new ApprovalFileModel
                    {
                        Id = a.ID,
                        Guid = a.Guid,
                        FileName = a.FileName
                    }).Distinct().ToList();
        }
   
        public List<ApprovalFileModel> GetApprovalVersionsByFolderIds(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals)
        {
            var approvalIds = GetApprovalIdsFromFolders(folderIds);
            var foldersApprovals = (from a in context.Approvals
                                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                    where approvalIds.Contains(a.ID) && a.AllowDownloadOriginal && (a.IsDeleted == false && a.DeletePermanently == false) && (adminCanViewAllApprovals || ac.Collaborator == loggedUserId)
                                    select new ApprovalFileModel()
                                    {
                                        Id = a.ID,
                                        Guid = a.Guid,
                                        FileName = a.FileName
                                    }).Distinct().ToList();

            return foldersApprovals;
        }

        public List<ApprovalFileModel> GetMaxApprovalVersionsByFolderIds(List<int> folderIds, int loggedUserId, bool adminCanViewAllApprovals)
        {
            var approvalIds = GetApprovalIdsFromFolders(folderIds);
            var foldersApprovals = (from a in context.Approvals
                                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                    where approvalIds.Contains(a.ID) && a.AllowDownloadOriginal && (a.IsDeleted == false && a.DeletePermanently == false) && (adminCanViewAllApprovals || ac.Collaborator == loggedUserId)
                                    select new {
                                                   Id = a.ID,
                                                   Guid = a.Guid,
                                                   FileName = a.FileName,
                                                   a.Version,
                                                   a.Job
                                               }).Distinct().ToList();

            if (foldersApprovals.Count > 0)
            {
                var currentVersions = foldersApprovals.GroupBy(gr => gr.Job, (key, g) => g.OrderByDescending(e => e.Version).FirstOrDefault()).ToList();

                return currentVersions.Select(foldersApproval => new ApprovalFileModel()
                {
                    Id = foldersApproval.Id,
                    Guid = foldersApproval.Guid,
                    FileName = foldersApproval.FileName
                }).Distinct().ToList();
            }
            return new List<ApprovalFileModel>();
        }

        private IEnumerable<int> GetApprovalIdsFromFolders(List<int> folderIds)
        {
            return (from f in context.Folders
                    where folderIds.Contains(f.ID)
                    select f.Approvals.Select(a => a.ID)).FirstOrDefault();
        }

        public void ResetApprovalProcessingDate(int approvalId)
        {
            var approval = context.Approvals.Find(approvalId);
            approval.StartProcessingDate = null;
            context.SaveChanges();
        }


        public bool CheckIfApprovalIsNotDeleted(int approvalId)
        {
            return (from f in context.Approvals
                    where f.ID == approvalId
                    select f).Any(t => t.IsDeleted == false);
        }
    }
}
