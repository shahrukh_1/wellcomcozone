﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMGColorDAL;
using GMG.CoZone.Collaborate.DomainModels.Annotation;
using GMG.CoZone.Repositories.Common;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;


namespace GMG.CoZone.Common.Repositories.ApprovalRepositories
{
    public class ApprovalAnnotationRepository : GenericRepository<ApprovalAnnotation>, IApprovalAnnotationRepository
    {
        public ApprovalAnnotationRepository(GMGColorContext context) : base(context)
        {
        }

        public IEnumerable<IGrouping<int, int>> GetAnnotationsOrderedbyCreatedDay(int[] annotationIdsToLoad)
        {

            var approvalsIds = (from ap in context.Approvals
                                join app in context.ApprovalPages on ap.ID equals app.Approval
                                join aa in context.ApprovalAnnotations on app.ID equals aa.Page
                                where annotationIdsToLoad.Contains(aa.ID)
                                select ap.ID).ToList();

            var approvalAnnotations = (from ap in context.Approvals
                                       join app in context.ApprovalPages on ap.ID equals app.Approval
                                       join an in context.ApprovalAnnotations on app.ID equals an.Page
                                       where approvalsIds.Contains(ap.ID) && an.Parent == null
                                       select new
                                       {
                                           ID = an.ID,
                                           ApprovalID = ap.ID,
                                           CreationDate = an.AnnotatedDate
                                       }).ToList();

            //order list by annotated date to get annotation index number for all document(s)
            var orderedByCreatedDate = (from approval in approvalAnnotations.OrderBy(i => i.CreationDate)
                                        group approval by approval.ApprovalID into g
                                        select new GroupedAnnotationIdsByApprovalIdModel
                                        {
                                            Key = g.Key,
                                            Annotations = g.Select(a => a.ID)
                                        });

            return orderedByCreatedDate;
        }

        public IEnumerable<IndividualAnnotationModel> GetShapeAndMarkerAnnotations(int[] annotationIds)
        {
            return (from ann in context.ApprovalAnnotations
                    join p in context.ApprovalPages on ann.Page equals p.ID
                    join a in context.Approvals on p.Approval equals a.ID
                    join act in context.ApprovalCommentTypes on ann.CommentType equals act.ID
                    let svg = (from am in context.ApprovalAnnotationMarkups
                               join s in context.Shapes on am.Shape equals s.ID
                               where am.ApprovalAnnotation == ann.ID
                               select s.SVG).ToList()
                    where annotationIds.Contains(ann.ID) && act.Key != "TXT"
                    select new IndividualAnnotationModel
                    {
                        UTCCreatedDate = ann.AnnotatedDate,
                        ImageWidth = p.OutputRenderWidth,
                        ImageHeight = p.OutputRenderHeight,
                        PositionX = ann.CrosshairXCoord.Value,
                        PositionY = ann.CrosshairYCoord.Value,
                        AnnotatedOnImageWidth = ann.AnnotatedOnImageWidth,
                        SVG = svg,
                        ID = ann.ID,
                        Comment = ann.Comment,
                        CommentType = svg.Count > 0 ? "shape" : "marker",
                        AnnotationTimeFrame = ann.TimeFrame,
                        IsUserStatusInactive = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                          where u.ID == ann.Creator && u.Status == (int)UserStatus.Inactive
                                                                                          select u.Status).Any()
                                                                                          :
                                                                                          false,
            CreatorDisplayName = ann.Creator.HasValue && ann.Creator > 0 ?( (from u in context.Users
                                                                                        where u.ID == ann.Creator
                                                                                        select u.GivenName + " " + u.FamilyName
                                                                                    ).FirstOrDefault() +

                                                                                    (

                                                                                        ((from ac in context.ApprovalCollaborators
                                                                                          join u in context.Users
                                                                                          on ac.SubstituteUser equals u.ID
                                                                                          where ac.Approval == a.ID && ac.SubstituteUser == u.ID && ac.Collaborator == ann.Creator
                                                                                          select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                        ).FirstOrDefault()) ?? ""

                                                                                    )

                                                                                    )
                                                                                    :
                                                                                    (from exc in context.ExternalCollaborators
                                                                                     where exc.ID == ann.ExternalCreator
                                                                                     select exc.GivenName + " " + exc.FamilyName
                                                                                    ).FirstOrDefault(),
                        Replies = (from rep in context.ApprovalAnnotations
                                   where rep.Parent == ann.ID
                                   orderby rep.AnnotatedDate ascending
                                   select new AnnotationReplyModel
                                   {
                                       Comment = rep.Comment,
                                       UTCReplyDate = rep.AnnotatedDate,
                                       CreatorDisplayName = rep.Creator.HasValue && rep.Creator > 0 ? (from u in context.Users
                                                                                                       where u.ID == rep.Creator
                                                                                                       select u.GivenName + " " + u.FamilyName
                                                                                                       ).FirstOrDefault()
                                                                                                   :
                                                                                                   (from exc in context.ExternalCollaborators
                                                                                                    where exc.ID == rep.ExternalCreator
                                                                                                    select exc.GivenName + " " + exc.FamilyName
                                                                                                   ).FirstOrDefault(),
                                       IsUserStatusInactive = rep.Creator.HasValue && rep.Creator > 0 ? (from u in context.Users
                                                                                                         where u.ID == rep.Creator && u.Status == (int)UserStatus.Inactive
                                                                                                         select u.Status).Any()
                                                                                          :
                                                                                          false,
                                       PhaseName = (from apjp in context.ApprovalJobPhases
                                                    where rep.Phase == apjp.ID
                                                    select apjp.Name).FirstOrDefault()
                                   }).ToList(),

                        PhaseName = (from apjp in context.ApprovalJobPhases
                                     where ann.Phase == apjp.ID
                                     select apjp.Name).FirstOrDefault(),
                        AnnotationAttachment = (from annatt in context.ApprovalAnnotationAttachments
                                                where ann.ID == annatt.ApprovalAnnotation || ann.Parent == annatt.ID
                                                select annatt.DisplayName).ToList(),

                        AnnotationChecklist = (from anncl in context.ApprovalAnnotationCheckListItems
                                               join cli in context.CheckListItem on anncl.CheckListItem equals cli.ID 
                                               join cl in context.CheckList on cli.CheckList equals cl.ID
                                               where ann.ID == anncl.ApprovalAnnotation
                                               select new AnnotationChecklistModel
                                               {
                                                   Checklist = cl.Name,
                                                   ChecklistItem = cli.Name,
                                                   TextValue = anncl.TextValue
                                               }).ToList()

                    });
        }

        public IEnumerable<IndividualAnnotationModel> GetMarkerAnnotations(int[] annotationIds)
        {
            return (from ann in context.ApprovalAnnotations
                    join p in context.ApprovalPages on ann.Page equals p.ID
                    join a in context.Approvals on p.Approval equals a.ID
                    join act in context.ApprovalCommentTypes on ann.CommentType equals act.ID
                    where annotationIds.Contains(ann.ID) && act.Key == "TXT"
                    select new IndividualAnnotationModel
                    {
                        UTCCreatedDate = ann.AnnotatedDate,
                        ImageWidth = p.OutputRenderWidth,
                        ImageHeight = p.OutputRenderHeight,
                        PositionX = ann.CrosshairXCoord.HasValue ? ann.CrosshairXCoord.Value : 0,
                        PositionY = ann.CrosshairYCoord.HasValue ? ann.CrosshairYCoord.Value : 0,
                        AnnotatedOnImageWidth = ann.AnnotatedOnImageWidth,
                        SVG = new List<string> { ann.HighlightData },
                        ID = ann.ID,
                        Comment = ann.Comment,
                        CommentType = "text",
                        IsUserStatusInactive = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                          where u.ID == ann.Creator && u.Status == (int)UserStatus.Inactive
                                                                                          select u.Status).Any()
                                                                                          :
                                                                                          false,
                        CreatorDisplayName = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                        where u.ID == ann.Creator
                                                                                        select u.GivenName + " " + u.FamilyName
                                                                                    ).FirstOrDefault()
                                                                                    :
                                                                                    (from exc in context.ExternalCollaborators
                                                                                     where exc.ID == ann.ExternalCreator
                                                                                     select exc.GivenName + " " + exc.FamilyName
                                                                                    ).FirstOrDefault(),
                        Replies = (from rep in context.ApprovalAnnotations
                                   where rep.Parent == ann.ID
                                   select new AnnotationReplyModel
                                   {
                                       Comment = rep.Comment,
                                       UTCReplyDate = rep.AnnotatedDate,
                                       CreatorDisplayName = rep.Creator.HasValue && rep.Creator > 0 ? (from u in context.Users
                                                                                                       where u.ID == rep.Creator
                                                                                                       select u.GivenName + " " + u.FamilyName
                                                                                                       ).FirstOrDefault()
                                                                                                   :
                                                                                                   (from exc in context.ExternalCollaborators
                                                                                                    where exc.ID == rep.ExternalCreator
                                                                                                    select exc.GivenName + " " + exc.FamilyName
                                                                                                   ).FirstOrDefault(),
                                       IsUserStatusInactive = rep.Creator.HasValue && rep.Creator > 0 ? (from u in context.Users
                                                                                                         where u.ID == rep.Creator && u.Status == (int)UserStatus.Inactive
                                                                                                         select u.Status).Any()
                                                                                                         : false,
                                       PhaseName = (from apjp in context.ApprovalJobPhases
                                                    where rep.Phase == apjp.ID
                                                    select apjp.Name).FirstOrDefault()
                                   }).ToList(),
                        PhaseName = (from apjp in context.ApprovalJobPhases
                                     where ann.Phase == apjp.ID
                                     select apjp.Name).FirstOrDefault(),

                        AnnotationAttachment = (from annatt in context.ApprovalAnnotationAttachments
                                                where ann.ID == annatt.ApprovalAnnotation || ann.Parent == annatt.ID
                                                select annatt.DisplayName).ToList(),

                        AnnotationChecklist = (from anncl in context.ApprovalAnnotationCheckListItems
                                               join cli in context.CheckListItem on anncl.CheckListItem equals cli.ID
                                               join cl in context.CheckList on cli.CheckList equals cl.ID
                                               where ann.ID == anncl.ApprovalAnnotation
                                               select new AnnotationChecklistModel
                                               {
                                                   Checklist = cl.Name,
                                                   ChecklistItem = cli.Name,
                                                   TextValue = anncl.TextValue
                                               }).ToList()

                    });
        }
    }
}
