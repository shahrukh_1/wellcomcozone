﻿using System.Linq;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Common.ApprovalRepositories
{
    public class ApprovalCollaboratorDecisionRepository : GenericRepository<ApprovalCollaboratorDecision>, IApprovalCollaboratorDecisionRepository
    {
        public ApprovalCollaboratorDecisionRepository(GMGColorContext context) : base(context)
        {
        }

        public int? GetInternalUserApprovalDecision(int approvalId, string userGuid)
        {
            var approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
                                join col in context.Users on apcd.Collaborator equals col.ID
                                join a in context.Approvals on apcd.Approval equals a.ID
                                where apcd.Approval == approvalId && col.Guid == userGuid && apcd.Phase == a.CurrentPhase
                                select apcd.Decision).FirstOrDefault();

            return approvalDecision.GetValueOrDefault() > 0 ? approvalDecision : null;
        }

        public int? GetExternalUserApprovalDecision(int approvalId, string userGuid)
        {
            var approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
                                join ec in context.ExternalCollaborators on apcd.ExternalCollaborator equals ec.ID
                                join a in context.Approvals on apcd.Approval equals a.ID
                                where apcd.Approval == approvalId && ec.Guid == userGuid && apcd.Phase == a.CurrentPhase
                                select apcd.Decision).FirstOrDefault();

            return approvalDecision.GetValueOrDefault() > 0 ? approvalDecision : null;
        }
    }
}
