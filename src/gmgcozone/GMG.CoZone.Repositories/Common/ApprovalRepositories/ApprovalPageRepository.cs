﻿using System.Linq;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Common.ApprovalRepositories
{
    public class ApprovalPageRepository : GenericRepository<ApprovalPage>, IApprovalPageRepository
    {
        public ApprovalPageRepository(GMGColorContext context) : base(context)
        {
        }

        public int GetApprovalFirstPageId(int approvalId)
        {
            var approvalPage = Get(ap => ap.Approval == approvalId && ap.Number == 1).FirstOrDefault();
            if (approvalPage == null)
                return 0;

            return approvalPage.ID;
        }

        public void ResetApprovalPagesProgress(int approvalId)
        {
            var pages = Get(page => page.Approval == approvalId).ToList();
            pages.ForEach(page => page.Progress = page.Number == 1 ? 5 : 0);

            context.SaveChanges();
        }
    }
}
