﻿using System.Linq;
using GMG.CoZone.Common.Repositories;
using GMG.CoZone.Repositories.Interfaces.Common.ApprovalRepositories;
using GMGColorDAL;
using System.Data.SqlClient;

namespace GMG.CoZone.Repositories.Common.ApprovalRepositories
{
    public class ApprovalUserViewInfoRepository : GenericRepository<ApprovalUserViewInfo>, IApprovalUserViewInfoRepository
    {
        public ApprovalUserViewInfoRepository(GMGColorContext context) : base(context)
        {
        }
        public int? GetLatestVisitedApproval(int userId)
        {
            string query = @"SELECT Top 1 [Extent1].[Approval] AS [Approval]
                            FROM [dbo].[ApprovalUserViewInfo] AS [Extent1] 
                            WHERE [Extent1].[User] = @userId
                            Order by ViewedDate Desc ";

            return context.Database.SqlQuery<int?>(query, new SqlParameter("userId", userId)).FirstOrDefault();
        }
    }
}
