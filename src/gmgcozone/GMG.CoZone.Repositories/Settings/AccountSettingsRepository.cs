﻿using System;
using System.Linq;
using GMG.CoZone.Repositories.Common;
using GMG.CoZone.Repositories.Interfaces.Settings;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Settings
{
    public class AccountSettingsRepository : GenericRepository<AccountSetting>, IAccountSettingsRepository
    {
        public AccountSettingsRepository(GMGColorContext context) : base(context)
        {

        }

        public bool IsShowAllFilesToAdminsChecked(int accountId, string ShowAllFilesToAdminSetting)
        {
            return ((from acs in context.AccountSettings
                     where acs.Account == accountId && acs.Name == ShowAllFilesToAdminSetting
                     select acs.Value).FirstOrDefault() ?? string.Empty).ToLower() == Boolean.TrueString.ToLower();
        }
    }
}
