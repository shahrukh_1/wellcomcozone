﻿using GMG.CoZone.Repositories.Common;
using GMG.CoZone.Repositories.Interfaces.Settings;
using GMGColorDAL;

namespace GMG.CoZone.Repositories.Settings
{
    public class AccountSettingsValueTypeRepository : GenericRepository<ValueDataType>, IAccountSettingsValueTypeRepository
    {
        public AccountSettingsValueTypeRepository(GMGColorContext context): base(context)
        {
        }
    }
}
