﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ColorProofATPConnector.AppCache;
using ColorProofATPConnector.ColorProofServer;
using System.Threading;
using System.Diagnostics;
using GMG.CoZone.Common;
using GMG.CoZone.Component;

namespace ColorProofATPConnector
{
    /// <summary>
    /// Jobs cache class
    /// </summary>
    public class JobsCache : CacheBase
    {

        #region Const Data

        private const string CACHE_NAME = "DeliverJobsCache";

        #endregion
        
        #region Ctors

        public JobsCache() :
            base(CACHE_NAME, GMGColorDAL.GMGColorConfiguration.AppConfiguration.DeliverJobCacheServerAddress)
        {
            Connect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add ColorProof instance job to cache (username + password) as key
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool AddCPInstanceJob(ColorProofJob job)
        {
            bool success = true;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Wait for set operation to complete
            while (Get(GetJobNamePasswordLockKey(job.CPInstanceGuid)) != null)
            {
                // skip if waiting time exceeds 3 seconds
                if (stopwatch.ElapsedMilliseconds > 3 * 1000)
                    break;
                Thread.Sleep(200);
            }

            // lock cache entry to avoid posting the CP jobs multiple times (in case the CP instance ask for the jobs in the same time an append is performed)
            string lockKey = GetJobNamePasswordLockKey(job.CPInstanceGuid);
            Set(lockKey, "locked");

            List<ColorProofJob> jobs = GetJobsForCPInstance(job.CPInstanceGuid, false);
            jobs.Add(job);

            if (!Set(GetJobNamePasswordKey(job.CPInstanceGuid), jobs))
            {
                ColorProofComponentInstance.Log("Adding ColorProof instance job to jobs cache failed", Severity.Error);
                success = false;
            }
            else
            {
                ColorProofComponentInstance.Log("Job added to cache: " + job.JobGUID, Severity.Info);
                success = true;
            }

            // unlock the CP jobs entry
            Delete(lockKey);

            return success;
        }

        /// <summary>
        /// Add ColorProof instance job to cache (username + password) as key
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool AddCPInstanceCommand(CommandTicket command)
        {
            bool success = true;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Wait for set operation to complete
            while (Get(GetCommandsPasswordLockKey(command.CPGuid)) != null)
            {
                // skip if waiting time exceeds 3 seconds
                if (stopwatch.ElapsedMilliseconds > 3 * 1000)
                    break;
                Thread.Sleep(200);
            }

            // lock cache entry to avoid posting the CP jobs multiple times (in case the CP instance ask for the jobs in the same time an append is performed)
            string lockKey = GetCommandsPasswordLockKey(command.CPGuid);
            Set(lockKey, "locked");

            List<CommandTicket> commands = GetCommandsForCPInstance(command.CPGuid, false);
            commands.Add(command);

            if (!Set(GetCommandsPasswordKey(command.CPGuid), commands))
            {
                ColorProofComponentInstance.Log("Adding ColorProof instance command to jobs cache failed", Severity.Error);
                success = false;
            }
            else
            {
                ColorProofComponentInstance.Log("Command added to cache: " + command.JobticketId, Severity.Info);
                success = false;
            }

            // unlock the CP jobs entry
            Delete(lockKey);

            return success;
        }

        /// <summary>
        /// Add job to cache using job GUID as key
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool AddJob(ColorProofJob job)
        {   
            if (Set(job.JobGUID, job))
            {
                return true;
            }
            else
            {
                ColorProofComponentInstance.Log("Adding color proof job to jobs cache failed", Severity.Error);
                return false;
            }
        }

        /// <summary>
        /// Add job to cache using job GUID as key
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool AddJobs(List<ColorProofJob> jobs)
        {
            bool success = true;
            foreach (ColorProofJob job in jobs)
            {
                if (!Set(job.JobGUID, job))
                {
                    ColorProofComponentInstance.Log("Adding ColorProof instance job to jobs cache in bulk operation failed", Severity.Error);
                }
            }
            return success;
        }

        /// <summary>
        /// Remove job by job GUID
        /// </summary>
        /// <param name="jobGUID"></param>
        /// <returns></returns>
        public bool DeleteJobByGUID(string jobGUID)
        {

            if (Delete(jobGUID))
            {
                return true;
            }
            else
            {
                ColorProofComponentInstance.Log("Delete job by GUID from jobs cache  failed", Severity.Error);
                return false;
            }
        }

        /// <summary>
        /// Remove job by username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool DeleteJobByInstanceGuid(string CPinstanceGuid)
        {
            if (Delete(GetJobNamePasswordKey(CPinstanceGuid)))
            {
                return true;
            }
            else
            {
                ColorProofComponentInstance.Log("Delete job by user and pass from jobs cache failed", Severity.Error);
                return false;
            }
        }

        /// <summary>
        /// Remove job by username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool DeleteCommandsByInstanceGuid(string CPinstanceGuid)
        {
            if (Delete(GetCommandsPasswordKey(CPinstanceGuid)))
            {
                return true;
            }
            else
            {
                ColorProofComponentInstance.Log("Delete commands by user and pass from jobs cache failed", Severity.Error);
                return false;
            }
        }

        /// <summary>
        /// Get color proof instance job (by username and password)
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public List<ColorProofJob> GetJobsForCPInstance(string CPInstanceGuid, bool waitForUnlock = true, bool deleteAfterGet = false)
        {
            List<ColorProofJob> jobs = new List<ColorProofJob>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Wait for set operation to complete
            while (waitForUnlock && Get(GetJobNamePasswordLockKey(CPInstanceGuid)) != null)
            {
                // skip if waiting time exceeds 10 seconds
                if (stopwatch.ElapsedMilliseconds > 10 * 1000)
                    return jobs;
                Thread.Sleep(200);
            }

            // lock cache entry to avoid adding new jobs that will be removed
            string lockKey = GetJobNamePasswordLockKey(CPInstanceGuid);
            if (deleteAfterGet)
            {                
                Set(lockKey, "locked");
            }

            object jobObj = Get(GetJobNamePasswordKey(CPInstanceGuid));

            if (jobObj != null)
            {
                if (deleteAfterGet)
                {
                    if (DeleteJobByInstanceGuid(CPInstanceGuid))
                    {
                        ColorProofComponentInstance.Log("Jobs delete from cache. ColorProof Instance: " + CPInstanceGuid, Severity.Info);
                    }
                }

                if (jobObj is ColorProofJob)
                {
                    jobs.Add(jobObj as ColorProofJob);
                }
                else if (jobObj is List<ColorProofJob>)
                {
                    jobs = jobObj as List<ColorProofJob>;                    
                }
                else
                {
                    ColorProofComponentInstance.Log("Get job by user and pass failed: unrecognized object. ColorProof Instance: " + CPInstanceGuid, Severity.Error);
                }
            }

            // unlock the CP jobs entry
            if (deleteAfterGet)
            {
                Delete(lockKey);
            }

            return jobs;
        }

        /// <summary>
        /// Get color proof instance job (by username and password)
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public List<CommandTicket> GetCommandsForCPInstance(string CPInstanceGuid, bool waitForUnlock = true, bool deleteAfterGet = false)
        {
            List<CommandTicket> jobs = new List<CommandTicket>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Wait for set operation to complete
            while (waitForUnlock && Get(GetCommandsPasswordLockKey(CPInstanceGuid)) != null)
            {
                // skip if waiting time exceeds 10 seconds
                if (stopwatch.ElapsedMilliseconds > 10 * 1000)
                    return jobs;
                Thread.Sleep(200);
            }

            // lock cache entry to avoid adding new jobs that will be removed
            string lockKey = GetCommandsPasswordLockKey(CPInstanceGuid);
            if (deleteAfterGet)
            {
                Set(lockKey, "locked");
            }

            object jobObj = Get(GetCommandsPasswordKey(CPInstanceGuid));

            if (jobObj != null)
            {
                if (deleteAfterGet)
                {
                    if (DeleteCommandsByInstanceGuid(CPInstanceGuid))
                    {
                        ColorProofComponentInstance.Log("Commands delete from cache. ColorProof Instance: " + CPInstanceGuid, Severity.Info);
                    }
                }

                if (jobObj is CommandTicket)
                {
                    jobs.Add(jobObj as CommandTicket);
                }
                else if (jobObj is List<CommandTicket>)
                {
                    jobs = jobObj as List<CommandTicket>;
                }
                else
                {
                    ColorProofComponentInstance.Log("Get commands by user and pass failed: unrecognized object. ColorProof Instance: " + CPInstanceGuid, Severity.Error);
                }
            }

            // unlock the CP jobs entry
            if (deleteAfterGet)
            {
                Delete(lockKey);
            }

            return jobs;
        }

        /// <summary>
        /// Get job by GUID
        /// </summary>
        /// <param name="jobGUID"></param>
        /// <returns></returns>
        public ColorProofJob GetJobByGUID(string jobGUID)
        {
            object jobObj = Get(jobGUID);
            if (jobObj != null && jobObj is ColorProofJob)
            {
                return jobObj as ColorProofJob;
                
            }
            else
            {
                ColorProofComponentInstance.Log("Get job by GUID failed: unrecognized object", Severity.Info);
                return null;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Retrieve cache registration key
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private string GetJobNamePasswordKey(string CPInstanceGuid)
        {
            return CPInstanceGuid;
        }

        /// <summary>
        /// Retrieve cache registration key
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private string GetJobNamePasswordLockKey(string CPInstanceGuid)
        {
            return CPInstanceGuid + "_lock";
        }

        /// <summary>
        /// Retrieve cache registration key
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private string GetCommandsPasswordKey(string CPInstanceGuid)
        {
            return CPInstanceGuid + "_command";
        }

        /// <summary>
        /// Retrieve cache registration key
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private string GetCommandsPasswordLockKey(string CPInstanceGuid)
        {
            return CPInstanceGuid + "_command_lock";
        }

        #endregion
    }
}
