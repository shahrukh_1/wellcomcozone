﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BeIT.MemCached;
using GMG.CoZone.Common;
using GMG.CoZone.Component;

namespace ColorProofATPConnector.AppCache
{
    /// <summary>
    /// Base cache class
    /// </summary>
    public class CacheBase
    {
        #region Private Members
        
        private MemcachedClient _cacheClient;
        private string _cacheName;
        private List<string> _servers = new List<string>();
        #endregion

        #region Ctors

        protected CacheBase(string cacheName, string server)
        {
            _cacheName = cacheName;
            AddServer(server);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Connect to cache server
        /// </summary>
        public void Connect()
        {
            try
            {
                MemcachedClient.Setup(_cacheName, _servers.ToArray());
                _cacheClient = MemcachedClient.GetInstance(_cacheName);
            }
            catch (System.Exception ex)
            {
                ColorProofComponentInstance.Log("Failed to connect to cache server!", ex);
                throw ex;
            }
            ColorProofComponentInstance.Log("Server successfully connected to memcached server " + _servers[0], Severity.Info);
        }

        /// <summary>
        /// Add a new server to list
        /// </summary>
        /// <param name="server"></param>
        public void AddServer(string server)
        {
            _servers.Add(server);
        }

        /// <summary>
        ///  Add a new entry to cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected bool Set(string key, object value)
        {
            return _cacheClient.Set(key, value);
        }

        /// <summary>
        /// Get a value by the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected object Get(string key)
        {
            return _cacheClient.Get(key);
        }

        /// <summary>
        /// Permanently remove item from cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected bool Delete(string key)
        {
            return _cacheClient.Delete(key);
        }

        #endregion
    }
}
