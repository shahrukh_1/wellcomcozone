﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel;
using GMG.Cz.Network.Bind;
using ColorProofATPConnector.ColorProofServer;
using GMG.Cz.Network.Interfaces;
using Amazon.SQS.Model;
using GMGColor.AWS;
using System.Reflection;
using System.Messaging;
using System.Threading;
using GMG.CoZone.Common;
using System.Xml.Linq;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;
using System.Threading.Tasks;
using GMGColorDAL;

namespace ColorProofATPConnector
{
    /// <summary>
    /// Connector component worker instance
    /// </summary>
    public class ColorProofComponentInstance
    {
        private static readonly Logger _log = new Logger(GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.ActiveConfiguration + " " + GMGColorConfiguration.AppConfiguration.AWSRegion
                                                                                : String.Empty
                                                        , "DBErrorLogger", "ColorProofConnectorLogger");
        private int _timePeriodDeliveryJobQueue = 0;
        private static JobsCache _jobCache = new JobsCache();
        public MessageQueue _mq;
        private static ColorProofComponentInstance _instance = null;
        private CancellationTokenSource _jobTaskCancellationSource = null;
        private CancellationTokenSource _instanceStatusCancellationSource  = null;

        #region Ctors
        public ColorProofComponentInstance()
        {
            _jobTaskCancellationSource = new CancellationTokenSource();
            _instanceStatusCancellationSource = new CancellationTokenSource();
        }
        #endregion

        /// <summary>
        /// Singleton Method
        /// </summary>
        public static ColorProofComponentInstance Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ColorProofComponentInstance();
                return _instance;
            }
        }

        #region Properties

        /// <summary>
        /// Gets deliver job queue visibility timeout
        /// </summary>
        private int DeliveryJobQueueVisibilityTimePeriod
        {
            get
            {
                if (_timePeriodDeliveryJobQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorDAL.GMGColorConfiguration.AppConfiguration.DeliverJobQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSRegion);

                        _timePeriodDeliveryJobQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        Log("Media Processor unable to get the SQS DeliverQueueName attributes. Exception", ex);
                    }

                    if (_timePeriodDeliveryJobQueue < 60)
                    {
                        _timePeriodDeliveryJobQueue = 60; // 60 Seconds
                    }
                }

                return _timePeriodDeliveryJobQueue;
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Log Method
        /// </summary>
        /// <param name="message">Info Message</param>
        /// <param name="ex">InnerException</param>
        public static void Log(string message, Exception ex)
        {
            _log.Log(new LoggingException(message, ex));
        }

        /// <summary>
        /// Log Method
        /// </summary>
        /// <param name="message">Info Message</param>
        /// <param name="ex">InnerException</param>
        public static void Log(string message, Severity Severity)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)Severity)
            {
                _log.Log(new LoggingNotification(message, Severity));
            }
        }

        /// <summary>
        /// Log Method
        /// </summary>
        /// <param name="notification">Notification for the log message</param>
        public static void Log(LoggingNotification notification)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)notification.Severity)
            {
                _log.Log(notification);
            }
        }

        /// <summary>
        /// ATP component startup
        /// </summary>
        public void StartJobsTask()
        {
            try
            {
                while (!_jobTaskCancellationSource.Token.IsCancellationRequested)
                {
                    try
                    {
                        bool queueIsEmpty = false;
                        string deliverQueueName = GMGColorDAL.GMGColorConfiguration.AppConfiguration.DeliverJobQueueName;

                        if (GMGColorDAL.GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                        {
                            ReceiveMessageResponse oResponse = AWSSimpleQueueService.CheckQueueForMessages(deliverQueueName, 10, Instance.DeliveryJobQueueVisibilityTimePeriod,
                                                                                                            GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                                                            GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                                                            GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSRegion);
                            if (oResponse.Messages.Count > 0)
                            {
                                foreach (Amazon.SQS.Model.Message processMsg in oResponse.Messages)
                                {
                                    try
                                    {
                                        Instance.AddNewJobOrCommand(processMsg.Body);

                                        // Delete the message from queue
                                        // TODO - delete only in case AddNewJobToATP succeeded
                                        AWSSimpleQueueService.DeleteMessage(deliverQueueName, processMsg,
                                                                            GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                            GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                            GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSRegion);
                                    }
                                    catch (Exception ex)
                                    {
                                        _log.Log(new LoggingEvent(new LoggingException(MessageConstants.CPAddNewJobOrCommandFailed, ex)));
                                    }
                                }
                            }
                            else
                            {
                                queueIsEmpty = true;
                            }
                        }
                        else
                        {
                            string message;
                            try
                            {
                                Instance._mq = new MessageQueue(deliverQueueName);
                                System.Messaging.Message msg = Instance._mq.Receive(new TimeSpan(0, 0, 3));
                                msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                                message = msg.Body.ToString();

                                Instance.AddNewJobOrCommand(message);
                            }
                            catch (MessageQueueException msqsEx)
                            {
                                if (msqsEx.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                                {
                                    queueIsEmpty = true;
                                }
                                else
                                {
                                    _log.Log(new LoggingException(MessageConstants.ReadFromQueueFail, msqsEx));
                                }
                            }
                        }

                        // Sleep thread for 10 seconds
                        if (queueIsEmpty)
                        {
                            Thread.Sleep(500 * 10); // wait 5 seconds between syncs
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Log(new LoggingEvent(new LoggingException(MessageConstants.CheckForNewDeliverJobsError, ex)));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log("Failed to initialize component! Execution ended!", ex);
            }
        }

        /// <summary>
        /// Checks for instances that are no longer connected and set their online status to false
        /// </summary>
        public void StartInstanceStatusTask()
        {
            while (!_instanceStatusCancellationSource.Token.IsCancellationRequested)
            {
                try
                {
                    bool needToSave = false;
                    using (GMGColorContext context = new GMGColorContext())
                    {
                        foreach (ColorProofInstance cpInstance in context.ColorProofInstances.Where(t => t.IsOnline == true))
                        {
                            if (cpInstance.IsOnline.GetValueOrDefault() && cpInstance.LastSeen != null && TimeSpan.FromTicks((DateTime.UtcNow.Ticks - cpInstance.LastSeen.Value)).TotalSeconds > GMGColorDAL.GMGColorConfiguration.AppConfiguration.ColorProofInstanceTimeoutInSec)
                            {
                                cpInstance.IsOnline = false;
                                needToSave = true;
                            }
                        }
                        if (needToSave)
                        {
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Log(new LoggingEvent(new LoggingException(MessageConstants.CPCheckInstancesConnectivityFailed, ex)));
                }
                finally
                {
                    //Sleep 20 sec
                    Thread.Sleep(20 * 1000);
                }
            }
        }

        public void StopTasks()
        {
            _jobTaskCancellationSource.Cancel();
            _instanceStatusCancellationSource.Cancel();
        }

        /// Delete Job from ATP and from local cache
        /// </summary>
        /// <param name="ATPJobGuid">ATP JobGuid</param>
        /// <param name="jobticketId">CoZone job GUID</param>
        public void DeleteJob(Guid jobticketId)
        {
            Log(new LoggingNotification(String.Format("DeleteJob guid {0} !", jobticketId), Severity.Info));
            // Delete from Cache
            _jobCache.DeleteJobByGUID(jobticketId.ToString());
        }

        /// <summary>
        /// Add new Job Or Command read from the queue to MemCache
        /// </summary>
        /// <param name="message"></param>
        private void AddNewJobOrCommand(string message)
        {
            XDocument newMsg = XDocument.Parse(message);

            string actionType = newMsg.Descendants(GMG.CoZone.Common.Constants.DeliverQueueActionType).FirstOrDefault().Value;

            if (actionType.Equals(ActionType.AddNewJob.ToString()))
            {
                ColorProofJob cpJob = new ColorProofJob(message);

                //Save checksum to database For TimeStamp 0
                Instance.SetJobCheckSum(cpJob.JobGUID, cpJob.CheckSum);
                // Add job with key = (CPInstanceGuid) to be able to get the CP pending jobs by username and password
                _jobCache.AddCPInstanceJob(cpJob);

                Log(new LoggingNotification(String.Format("New job with guid {0} added!", cpJob.JobGUID), Severity.Info));
            }
            else if (actionType.Equals(ActionType.AddNewCommand.ToString()))
            {
                CommandTicket command = new CommandTicket();
                command.FromXML(newMsg);
                _jobCache.AddCPInstanceCommand(command);

                Log(new LoggingNotification(String.Format("New {0} command for job with guid {1} added!", command.Type.ToString(), command.JobticketId.ToString()), Severity.Info));
            }
        }

        /// <summary>
        /// Save file checksum to database
        /// </summary>
        /// <param name="jobGUID">deliver job guid</param>
        /// <param name="fileCheckSum">file checksum to be saved</param>
        public void SetJobCheckSum(string jobGUID, string fileCheckSum)
        {
            using (GMGColorDAL.GMGColorContext context = new GMGColorDAL.GMGColorContext())
            {
                GMGColorDAL.DeliverJob deliverJob = (from dj in context.DeliverJobs
                                                       where dj.Guid == jobGUID
                                                       select dj).Take(1).SingleOrDefault();

                if (deliverJob != null)
                {
                    deliverJob.FileCheckSum = fileCheckSum;
                    context.SaveChanges();
                }
                else
                {
                    Log(new LoggingNotification(String.Format("SetJobCheckSum() method failed, job with guid: {0} not found in database", jobGUID), Severity.Error));
                }
            }
        }

        /// <summary>
        /// Removes ColorProof jobs added with (CP username + CP password) as key and add them with as items with key = job GUID
        /// </summary>
        /// <returns></returns>
        public void SetCPJobsAsInProgressInCache(List<ColorProofJob> jobs)
        {
            _jobCache.AddJobs(jobs);
        }

        /// <summary>
        /// Get Deliver Jobs from CoZone By ColorProof Username And Password and delete the retrieved jobs
        /// </summary>
        /// <returns></returns>
        public List<ColorProofJob> GetJobsToSendByInstanceGuid(string CPInstanceGuid)
        {
            List<ColorProofJob> jobs = _jobCache.GetJobsForCPInstance(CPInstanceGuid, deleteAfterGet: true);
            return jobs;
        }

        /// <summary>
        /// Get Deliver Jobs from CoZone By ColorProof Username And Password and delete the retrieved jobs
        /// </summary>
        /// <returns></returns>
        public List<CommandTicket> GetCommandsToSendByInstanceGuid(string CPInstanceGuid)
        {
            List<CommandTicket> commands = _jobCache.GetCommandsForCPInstance(CPInstanceGuid, deleteAfterGet: true);
            return commands;
        }

        /// <summary>
        /// Get filename By job guid
        /// </summary>
        /// <param name="jobGuid"></param>
        /// <returns></returns>
        public ColorProofJob GetJobByJobGUID(string jobGuid)
        {
            return _jobCache.GetJobByGUID(jobGuid);
        }

        /// <summary>
        /// Adds a new job in MemCached
        /// </summary>
        /// <param name="job"></param>
        public void AddJobInCache(ColorProofJob job)
        {
            _jobCache.AddJob(job);
        }
        
        #endregion
    }
}
