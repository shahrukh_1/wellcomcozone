﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ColorProofATPConnector;
using GMG.CoZone.ColorProofConnector.ColorProofServer;
using GMG.CoZone.Common;
using GMG.CoZone.Component;
using GMG.Cz.Network.Data;
using GMGColorDAL;

namespace GMG.CoZone.ColorProofConnector.Utils
{
    public class Utilities
    {
        /// <summary>
        /// Retrieve deliver job status based on
        /// </summary>
        /// <param name="cpJobStatus"></param>
        /// <returns></returns>
        public static DeliverJobStatus GetDeliverJobStatusFromColorProofJobStatus(ColorProofJobStatus cpJobStatus, DeliverJob job)
        {
            DeliverJobStatus status = DeliverJobStatus.Submitting;
            switch (cpJobStatus)
            {
                case ColorProofJobStatus.Created:
                    {
                        status = DeliverJobStatus.Created;
                        break;
                    }

                case ColorProofJobStatus.Downloading:
                    {
                        status = DeliverJobStatus.Downloading;
                        break;
                    }
                case ColorProofJobStatus.Waiting:
                    {
                        status = DeliverJobStatus.Waiting;
                        break;
                    }

                case ColorProofJobStatus.Processing:
                    {
                        status = DeliverJobStatus.Processing;
                        break;
                    }
                case ColorProofJobStatus.Cancelled:
                    {
                        status = DeliverJobStatus.Cancelled;
                        break;
                    }
                case ColorProofJobStatus.DownloadFailed:
                    {
                        status = DeliverJobStatus.DownloadToCPFailed;
                        break;
                    }
                case ColorProofJobStatus.ValidationFailed:
                    {
                        status = DeliverJobStatus.ValidationInCpFailed;
                        break;
                    }
                case ColorProofJobStatus.ProcessingError:
                    {
                        status = DeliverJobStatus.ProcessingError;
                        break;
                    }
                case ColorProofJobStatus.Completed:
                    {
                        status = DeliverJobStatus.Completed;
                        break;
                    }
                case ColorProofJobStatus.Deleted:
                    {
                    status = IsJobCompletedAndDeleted(job);
                    break;
                    }
            }

            ColorProofComponentInstance.Log(String.Format(MessageConstants.DeliverJobStatusFromColorProofStatusInfo, cpJobStatus, job.Status, job.Guid), Severity.Info);

            return status;
        }

        /// <summary>
        /// Check if job that is deleted is completed or it has errors
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private static DeliverJobStatus IsJobCompletedAndDeleted(DeliverJob job)
        {
            PagesSelectionEnum pageType = PagesSelectionEnum.Single;
            string rangeValue;
            Common.Utils.StringToPage(job.Pages ?? string.Empty, out pageType, out rangeValue);

            ColorProofComponentInstance.Log(String.Format(MessageConstants.JobCompletedAndDeletedStatusInfo, pageType, job.DeliverJobPages.Count, job.TotalNrOfPages, job.Guid), Severity.Info);

            switch (pageType)
            {
                case PagesSelectionEnum.Single:
                    {
                        var deliverJobPage = job.DeliverJobPages.SingleOrDefault();

                        if (job.DeliverJobPages.Count != 1 || deliverJobPage == null)
                        {
                            return DeliverJobStatus.ErrorDeleted;
                        }
                       
                        var pageCurrentStatus = deliverJobPage.DeliverJobPageStatu.Key;

                        return (pageCurrentStatus == (int)DeliverPageStatus.Deleted) ? DeliverJobStatus.CompletedDeleted : DeliverJobStatus.ErrorDeleted;
                    }
                case PagesSelectionEnum.All:
                    {
                        if (job.DeliverJobPages.Count != job.TotalNrOfPages)
                        {
                            return DeliverJobStatus.ErrorDeleted;
                        }

                        return CalculateJobStatus(job);
                    }
                case PagesSelectionEnum.Range:
                    {
                        if (job.DeliverJobPages.Count != Common.Utils.ReturnRangePages(rangeValue).Count)
                        {
                            return DeliverJobStatus.ErrorDeleted;
                        }

                        return CalculateJobStatus(job);
                    }
                default:
                    return DeliverJobStatus.ErrorDeleted;
            }

        }

        private static DeliverJobStatus CalculateJobStatus(DeliverJob job)
        {
            return job.DeliverJobPages.Any(
                    t =>
                    t.DeliverJobPageStatu.Key != (int)DeliverPageStatus.Completed &&
                    t.DeliverJobPageStatu.Key != (int)DeliverPageStatus.Deleted)
                    ? DeliverJobStatus.ErrorDeleted
                    : DeliverJobStatus.CompletedDeleted;
        }

        public static string GetSupportedSpottedColorString(ICollection<string> colors)
        {
            string supportedColors = String.Empty;
            foreach (var color in colors)
                supportedColors += color + Environment.NewLine;

            return supportedColors;
        }

        public static void CreateWorkflowBOObject(ColorProofWorkflow workflow, CpWorkflow baseData)
        {
            workflow.IsActivated = true; //this is "false" for true CoZone Cloud service, and Admin needs to Activate Published Workflow
            workflow.IsInlineProofVerificationSupported = baseData.IsInlineProofVerificationSupported;
            workflow.MaximumUsablePaperWidth = baseData.MaximumUsablePaperWidth;
            workflow.Name = baseData.Name;
            workflow.OldName = baseData.OldName;
            workflow.ProofStandard = baseData.ProofStandard;

            workflow.SupportedSpotColors = GetSupportedSpottedColorString(baseData.SupportedSpotColors);
        }

        //Check only properties important to proofing
        public static bool IsDifferentProofCapabilities(ColorProofWorkflow workflow, CpWorkflow wf)
        {
            if (workflow.IsInlineProofVerificationSupported != wf.IsInlineProofVerificationSupported)
            {
                return true;
            }

            if (workflow.MaximumUsablePaperWidth != wf.MaximumUsablePaperWidth)
            {
                return true;
            }

            if (workflow.ProofStandard != wf.ProofStandard)
            {
                return true;
            }

            if (SupportedSpotColorsFromString(workflow.SupportedSpotColors).Count() != wf.SupportedSpotColors.Count)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < SupportedSpotColorsFromString(workflow.SupportedSpotColors).Count(); i++)
                {
                    if (SupportedSpotColorsFromString(workflow.SupportedSpotColors).ElementAt(i) != wf.SupportedSpotColors.ElementAt(i))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static string[] SupportedSpotColorsFromString(string SupportedSpotColors)
        {
            string[] words = (SupportedSpotColors ?? String.Empty).Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            return words;
        }
    }
}