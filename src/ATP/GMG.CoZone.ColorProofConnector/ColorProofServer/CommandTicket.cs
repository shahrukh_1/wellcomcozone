﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.Cz.Network.Data;
using GMG.CoZone.Common;
using System.Xml.Linq;

namespace ColorProofATPConnector.ColorProofServer
{
    [Serializable]
    public class CommandTicket : CzCommandTicket
    {
        #region Properties

        public CommandType Type { get; set; }

        public string CPGuid { get; set; }

        #endregion

        #region Public Methods 

        /// <summary>
        /// Create a Command Ticket from XML
        /// </summary>
        /// <param name="message"></param>
        public void FromXML(XDocument message)
        {
            Guid guid = new Guid();
            Guid.TryParse(message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobGuid).FirstOrDefault().Value, out  guid);
            JobticketId = guid;

            CommandType type;
            Enum.TryParse(message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueCommandType).FirstOrDefault().Value, out type);
            Type = type;

            CPGuid = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueCPServerGuid).FirstOrDefault().Value;
        }

        /// <summary>
        /// Create a CzCommandTicket 
        /// </summary>
        /// <returns></returns>
        public CzCommandTicket CloneToCzCommandTicket()
        {
            CzCommandTicket clone = null;

            if (Type == CommandType.Cancel)
            {
                clone = new CzCancelTicket();
            }
            else if (Type == CommandType.Delete)
            {
                clone = new CzDeleteTicket();
            }
            else
            {
                clone = new CzCommandTicket();
            }

            clone.JobticketId = JobticketId;

            return clone;
        }

        #endregion

    }

}
