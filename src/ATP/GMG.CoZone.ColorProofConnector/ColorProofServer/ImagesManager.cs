﻿// Microsoft
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Security.Cryptography;
using GMG.Cz.Network.Bind;
using System.Configuration;
using System.Net;
using System.Web;
using System.Xml.Linq;
using GMGColor.AWS;
using GMG.Cz.Network.Utility;
using GMG.CoZone.Common;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;

namespace ColorProofATPConnector.ColorProofServer
{
    public static class ImagesManager
    {
        #region Constants

        private const int _downloadTimeout = 2; //if FileDownload do not receive message 2 min - FileStream is closed and FileDownload removed from s_downloadFiles
        private const int _cleanupThreadPeriod = 10000;
        private const string _coZoneSimulatorImageFolder = "CoZoneSimulatorImages";

        #endregion

        #region Members

        private static int s_imageChunkSize = Convert.ToInt32(ConfigurationManager.AppSettings[MessageConstants.ImageChunkSize]);
        private static Dictionary<Guid, FileDownload> s_downloadFiles = new Dictionary<Guid, FileDownload>();
        private static object s_downloadFilesLock = new object();


        #endregion

        #region Public methods

        /// <summary>
        /// Gets the filepath by job GUID
        /// </summary>
        /// <param name="jobGUID"></param>
        /// <param name="region"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string GetJobFilePath(string jobGUID, string filename)
        {
            string filePath = String.Empty;

            string approvalFolderRelativePath = "deliver/" + jobGUID + "/"; // TODO - use config value for "deliver" folder 
            string approvalFileRelativePath = approvalFolderRelativePath + filename;
            if (GMGColorDAL.GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                filePath = approvalFileRelativePath;
            }
            else
            {
                string pathtoDataFolder = GMGColorDAL.GMGColorConfiguration.AppConfiguration.PathToDataFolder(String.Empty);
                filePath = pathtoDataFolder + approvalFileRelativePath;
            }

            return filePath;
        }

        public static void StartCleanupThread()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback((sender) =>
            {
                while (true)
                {
                    Thread.Sleep(_cleanupThreadPeriod);

                    lock (s_downloadFilesLock)
                    {
                        try
                        {
                            List<Guid> timedOutDownloads = new List<Guid>();
                            foreach (KeyValuePair<Guid, FileDownload> download in s_downloadFiles)
                            {
                                TimeSpan timeElapsed = DateTime.Now - download.Value.LastActivity;
                                if (timeElapsed.Minutes > _downloadTimeout)
                                {
                                    download.Value.File.Close();
                                    timedOutDownloads.Add(download.Key);
                                }
                            }
                            foreach (Guid fileId in timedOutDownloads)
                            {
                                s_downloadFiles.Remove(fileId);
                            }
                        }
                        catch (Exception ex)
                        {
                            ColorProofComponentInstance.Log(MessageConstants.czImagesCleanUpThreadFiled, ex);
                        }
                    }
                }
            }));
        }

        public static bool GetImageChunkLocal(Guid jobticketId, string filePath, long chunkNumberIn, out long chunkNumberOut, out byte[] data)
        {
            data = new byte[s_imageChunkSize];
            chunkNumberOut = 0;
            lock (s_downloadFilesLock)
            {
                if (chunkNumberIn == 0)
                {
                    OpenFileStream(jobticketId, filePath);
                }

                if (chunkNumberIn == s_downloadFiles[jobticketId].File.Length) //use chunkNumberIn equal to file size (this is set in ColorProof when download is finished or canceled) to close file.
                {
                    CloseFileStream(jobticketId);

                    ColorProofComponentInstance.Instance.DeleteJob(jobticketId);

                    ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.ColorProofJobFinished, jobticketId), Severity.Info));

                    return true;
                }

                if (chunkNumberIn != s_downloadFiles[jobticketId].File.Position)
                {
                    return false;
                }

                int bytesRead = s_downloadFiles[jobticketId].File.Read(data, 0, s_imageChunkSize);
                s_downloadFiles[jobticketId].LastActivity = DateTime.Now;

                //Remove empty part of last chunk
                if (bytesRead < s_imageChunkSize)
                {
                    byte[] withoutEmpty = new byte[bytesRead];
                    for (int n = 0; n < bytesRead; n++)
                        withoutEmpty[n] = data[n];

                    data = withoutEmpty;
                }

                chunkNumberOut = s_downloadFiles[jobticketId].File.Position;
            }

            return true;
        }

        /// <summary>
        /// Get Requested chunk from the specified file
        /// </summary>
        /// <param name="jobticketId">job GUID</param>
        /// <param name="filePath">job Relative to bucket file path</param>
        /// <param name="fileSize">file Size</param>
        /// <param name="ATPJobGuid">job Guid in ATP</param>
        /// <param name="chunkNumberIn">start byte of the requested sentence</param>
        /// <param name="chunkNumberOut">end byte of the requested sentence</param>
        /// <param name="data">data chunk that will be requested</param>
        /// <returns>Wheter the request succeedes or not</returns>
        public static bool GetImageChunkFromS3(Guid jobticketId, string filePath, long fileSize, long chunkNumberIn, out long chunkNumberOut, out byte[] data)
        {
            data = new byte[s_imageChunkSize];
            chunkNumberOut = 0;
            try
            {
                lock (s_downloadFilesLock)
                {
                    // Temporary log
                    ColorProofComponentInstance.Log(new LoggingNotification(String.Format("Get image chunk for job {0}, chunknumberin: {1}, chunknumberout {2}, filesize: {3}, filepath {4}", jobticketId, chunkNumberIn, chunkNumberIn + s_imageChunkSize, fileSize, filePath), Severity.Info));

                    if (chunkNumberIn == fileSize) //use chunkNumberIn equal to file size (this is set in ColorProof when download is finished or canceled) to close file.
                    {
                        ColorProofComponentInstance.Instance.DeleteJob(jobticketId);

                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.ColorProofJobFinished, jobticketId), Severity.Info));

                        return true;
                    }

                    data = AWSSimpleStorageService.GetFileStreamByteRange(GMGColorDAL.GMGColorConfiguration.AppConfiguration.DataFolderName(GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSRegion), filePath, GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                                  GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSSecretKey, chunkNumberIn, chunkNumberIn + s_imageChunkSize);

                    int bytesRead = data.Length;

                    //Remove empty part of last chunk
                    if (bytesRead < s_imageChunkSize)
                    {
                        byte[] withoutEmpty = new byte[bytesRead];
                        for (int n = 0; n < bytesRead; n++)
                            withoutEmpty[n] = data[n];

                        data = withoutEmpty;
                    }

                    chunkNumberOut = chunkNumberIn + bytesRead;
                }

                return true;
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log("Get Image Chunk From S3 failed", ex);
                return false;
            }
        }

        public static void DeleteJobticketImages(Guid jobticketId)
        {
            string imagesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _coZoneSimulatorImageFolder);

            string jobticketFolder = Path.Combine(imagesFolder, jobticketId.ToString());

            if (Directory.Exists(jobticketFolder))
            {
                try
                {
                    Directory.Delete(jobticketFolder, true);
                }
                catch { }
            }
        }

        /// <summary>
        /// Gets the checksum that will be needed to validate the job in ColorProof and saves it to database to avoid downloading the file again whenever the job is sent to ColorProof
        /// </summary>
        /// <param name="jobGUID">Deliver job guid</param>
        /// <param name="approvalFileRelativePath">file relative path to Bucket for Amazon files and absolute path for local files</param>
        /// <param name="fileName">Deliver Job FileName</param>
        /// <returns>CheckSum that will be sent to ColorProof</returns>
        public static string GetJobFileCheckSum(string jobGUID, string approvalFileRelativePath, string fileName)
        {
            string checkSum = String.Empty;
            string pathtoDataFolder = String.Empty;
            if (GMGColorDAL.GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                pathtoDataFolder = GMGColorDAL.GMGColorConfiguration.AppConfiguration.PathToDataFolder(GMGColorDAL.GMGColorConfiguration.AppConfiguration.AWSRegion);
                string processingFolderPath = GMGColorDAL.GMGColorConfiguration.AppConfiguration.PathToProcessingFolder + jobGUID;
                CreateLocalFolder(processingFolderPath);

                string filePath = processingFolderPath + "/" + fileName;
                WebClient client = new WebClient();
                try
                {
                    client.DownloadFile(pathtoDataFolder + HttpUtility.UrlEncode(approvalFileRelativePath), filePath);
                    if (File.Exists(filePath))
                    {
                        checkSum = Checksum.GetCheckSumForFile(filePath);
                        ColorProofComponentInstance.Log(MessageConstants.AWSImageDownloaded + "to " + filePath, Severity.Info);
                        string guidFolder = Path.GetDirectoryName(filePath);
                        Directory.Delete(guidFolder, true);
                    }
                    else
                        ColorProofComponentInstance.Log(String.Format(MessageConstants.AWSImageDownloadedFileNotFound, filePath), Severity.Error);
                }
                catch (Exception ex)
                {
                    ColorProofComponentInstance.Log(String.Format(MessageConstants.AWSImageDownloadedFailed, filePath, ex.Message, ex.StackTrace), Severity.Error);
                  
                }
                finally
                {
                    client.Dispose();
                }

            }
            else
            {
                checkSum = Checksum.GetCheckSumForFile(approvalFileRelativePath);
            }

            return checkSum;
        }

        #endregion

        #region Private Methods

        private static void CloseFileStream(Guid jobTicketId)
        {
            lock (s_downloadFilesLock)
            {
                if (s_downloadFiles.ContainsKey(jobTicketId))
                {
                    s_downloadFiles[jobTicketId].File.Close();
                    s_downloadFiles.Remove(jobTicketId);
                }
            }
        }

        private static void OpenFileStream(Guid jobTicketId, string filePath)
        {
            lock (s_downloadFilesLock)
            {
                FileDownload download = new FileDownload();
                download.File = new FileStream(filePath, FileMode.Open);
                download.LastActivity = DateTime.Now;
                s_downloadFiles.Add(jobTicketId, download);
            }
        }

        /// <summary>
        /// Delete and create job file folder
        /// </summary>
        /// <param name="folderPath"></param>
        private static void CreateLocalFolder(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                return;
            }
            Directory.CreateDirectory(folderPath);
        }

        #endregion
    }

    public class FileDownload
    {
        public FileStream File { get; set; }

        public DateTime LastActivity { get; set; }
    }
}

