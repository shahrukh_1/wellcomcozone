﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ColorProofATPConnector.ColorProofServer
{
    /// <summary>
    /// Enum for ColorProof Server Messages
    /// </summary>
    internal enum ColorProofInternalStatus
    {
        InstanceAlreadyExists = 9,
        InstanceNotFound,
        Error,
        SQLError
    }
}
