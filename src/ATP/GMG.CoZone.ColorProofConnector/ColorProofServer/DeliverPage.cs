﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GMG.CoZone.ColorProofConnector.ColorProofServer
{
    public class DeliverPage
    {
        public int Status { get; internal set; }
        public string UserFriendlyName { get; internal set; }
    }
}