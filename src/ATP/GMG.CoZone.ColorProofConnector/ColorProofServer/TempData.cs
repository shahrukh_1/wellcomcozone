﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GMG.Cz.Network.Data;

namespace ColorProofATPConnector.ColorProofServer
{
    public class TempData
    {
        public static List<CpImageStatusUpdate> CreateTestFiles()
        {
            List<CpImageStatusUpdate> testList = new List<CpImageStatusUpdate>();

            CpProofVerificationResult prooftest = new CpProofVerificationResult();
            CpImageStatusUpdate update = new CpImageStatusUpdate();
            Guid result;
            Guid.TryParse("a58dc103-be5d-4266-bbff-52ead9da841a", out result);
            update.CzJobticketId = result;
            update.NewStatus = CzImageState.Failed;
            update.Message = "Test-fatal-error";
            update.UserFriendlyName = "test 1";
            CpSpotColorVerificationResult spot = new CpSpotColorVerificationResult()
            {
                MaxDeltaEInGamut = 4.23,
                AverageDeltaEInGamut = 9.99
            };
            prooftest.SpotColorVerificationResults.Add(spot);
            CpVerificationResult verifm = new CpVerificationResult(ProofVerificationType.Maximum, "asd", "asd", 12.22, 4.33, true);
            CpVerificationResult verifa = new CpVerificationResult(ProofVerificationType.Average, "asd", "asd", 13.33, 9.99, false);
            prooftest.VerificationResults.Add(verifm);
            prooftest.VerificationResults.Add(verifa);
            update.ProofVerificationResults.Add(prooftest);

            CpProofVerificationResult prooftest1 = new CpProofVerificationResult();
            CpImageStatusUpdate update1 = new CpImageStatusUpdate();
            Guid.TryParse("a730b4fa-756f-4f1d-899f-961008b8d23d", out result);
            update1.CzJobticketId = result;
            update1.NewStatus = CzImageState.Completed;
            update1.Message = "Test-fatal-success";
            update1.UserFriendlyName = "test 2";
            CpSpotColorVerificationResult spot1 = new CpSpotColorVerificationResult()
            {
                MaxDeltaEInGamut = 4.23,
                AverageDeltaEInGamut = 7.99
            };
            prooftest1.SpotColorVerificationResults.Add(spot1);
            CpVerificationResult verifm1 = new CpVerificationResult(ProofVerificationType.Maximum, "asd", "asd", 2.22, 4.33, true);
            CpVerificationResult verifa1 = new CpVerificationResult(ProofVerificationType.Average, "asd", "asd", 2.22, 9.99, false);
            prooftest1.VerificationResults.Add(verifm1);
            prooftest1.VerificationResults.Add(verifa1);
            update1.ProofVerificationResults.Add(prooftest1);

            testList.Add(update1);

            return testList;
        }

    }
}