﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ColorProofATPConnector.ColorProofServer
{
    public class ImageProofVerificationResult
    {
        #region Properties 

        public string ProofResult { get; set; }
        public double MaxDelta { get; set; }
        public double AvgDelta { get; set; }
        public string StripName { get; set; }
        public double AllowedTolleranceForMax { get; set; }
        public double AllowedTolleranceForAvg { get; set; }
        public bool IsSpotColorVerification { get; set; }

        #endregion
    }
}