﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;

namespace ColorProofATPConnector.ColorProofServer
{
    public class JobStateUpdate
    {
        #region Properties

        /// <summary>
        /// ATO job GUID
        /// </summary>
        public string JobGUID { get; set; }

        /// <summary>
        /// job color proof status
        /// </summary>
        public ColorProofJobStatus JobStatus { get; set; }

        #endregion
    }
}
