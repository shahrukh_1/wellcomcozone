﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GMG.CoZone.Common;
using System.Collections.ObjectModel;
using System.IO;
using GMGColorDAL;
using GMG.CoZone.Component;

namespace ColorProofATPConnector.ColorProofServer
{
    [Serializable]
    public class ColorProofJob
    {
        #region Properties

        public string JobGUID { get; set; }

        public string CPInstanceGuid { get; set; }

        public string FileName { get; set; }

        public string JobName { get; set; }

        public long FileSize { get; set; }

        public string WorkflowName { get; set; }

        public string CoZoneUsername { get; set; }

        public string CheckSum { get; set; }

        public string BacklinkUrl { get; set; }

        public string FilePath { get; set; }

        public ColorProofJobStatus? JobStatus { get; set; }

        public string Pages { get; set; }

        public bool IncludeMetaProofInformation { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for CPJob from AUjob
        /// </summary>
        /// <param name="auJob"></param>
        public ColorProofJob(string messageJob)
        {
            XDocument message = XDocument.Parse(messageJob);

            JobName = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobTitle).FirstOrDefault().Value;
            JobGUID = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobGuid).FirstOrDefault().Value;
            CPInstanceGuid = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueCPServerGuid).FirstOrDefault().Value;
            WorkflowName = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueCPWorkflowName).FirstOrDefault().Value;
            CoZoneUsername = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueCoZoneUsername).FirstOrDefault().Value;
            FileName = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobFilename).FirstOrDefault().Value;
            FileSize = Convert.ToInt64(Convert.ToDouble(message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobFileSize).FirstOrDefault().Value));
            Pages = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobPages).FirstOrDefault().Value;
            FilePath = ImagesManager.GetJobFilePath(JobGUID, FileName);
            IncludeMetaProofInformation = Convert.ToBoolean(message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobIncludeMetaInfo).FirstOrDefault().Value);
            BacklinkUrl = message.Descendants(GMG.CoZone.Common.Constants.DeliverQueueJobBackLinkURL).FirstOrDefault().Value;
            CheckSum = ImagesManager.GetJobFileCheckSum(JobGUID, FilePath, FileName);
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ColorProofJob()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clone method for CPJob
        /// </summary>
        /// <returns></returns>
        public ColorProofJob Clone()
        {
            ColorProofJob clone = new ColorProofJob();

            clone.FileSize = FileSize;
            clone.FilePath = FilePath;
            clone.FileName = FileName;
            clone.JobGUID = JobGUID;
            clone.JobName = JobName;
            clone.JobStatus = JobStatus;
            clone.WorkflowName = WorkflowName;
            clone.CoZoneUsername = CoZoneUsername;
            clone.BacklinkUrl = BacklinkUrl;
            clone.CPInstanceGuid = CPInstanceGuid;
            clone.Pages = Pages;
            clone.IncludeMetaProofInformation = IncludeMetaProofInformation;
            clone.BacklinkUrl = BacklinkUrl;

            return clone;
        }

        /// <summary>
        /// Get Pages For CzJobTicket
        /// </summary>
        /// <returns>The Collection of Pages to be sent to ColorProof for this Job</returns>
        public Collection<int> GetPages()
        {
            Collection<int> pages = new Collection<int>();
            try
            {
                pages = Utils.GetPages(Pages);
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log(MessageConstants.CPGetPages, ex);
            }

            return pages;
        }

        public static ColorProofJob GetColorProfJobIfExists(string guid)
        {
            using (GMGColorContext context = new GMGColorContext())
            {
                DeliverJob deliverJob = (from jobs in context.DeliverJobs
                            where jobs.Guid == guid
                            select jobs).FirstOrDefault();

                if (deliverJob != null)
                {
                    ColorProofJob cpJob = new ColorProofJob();
                    cpJob.JobGUID = guid;
                    cpJob.FileName = deliverJob.FileName;
                    cpJob.FilePath = ImagesManager.GetJobFilePath(guid, deliverJob.FileName);
                    cpJob.FileSize = Convert.ToInt64(Convert.ToDouble(deliverJob.Size));

                    return cpJob;
                }
            }

            return null;
        }

        #endregion
    }

}
