﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GMG.CoZone.Common;

namespace ColorProofATPConnector.ColorProofServer
{
    public class ImageStateUpdate
    {
        #region Properties

        public string UserFriendlyName { get; set; }
        public string JobGuid { get; set; }
        public ImageState ImageState { get; set; }
        public string ErrMessage { get; set; }

        #endregion

        #region Public Data

        public List<ImageProofVerificationResult> proofVerificationResult = new List<ImageProofVerificationResult>();

        #endregion
    }
}