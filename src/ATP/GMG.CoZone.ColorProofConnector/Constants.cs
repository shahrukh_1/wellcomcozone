﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ColorProofATPConnector
{
    class Constants
    {
        #region Config Settings

        public const string ControllerIP = "ATPControllerIP";
        public const string ControllerPort = "ATPControllerPort";        

        #endregion
    }
}
