﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading.Tasks;
using ColorProofATPConnector;
using System.Threading;
using GMGColor.AWS;
using GMGColorDAL;

namespace GMG.CoZone.ColorProofConnector
{
    public class Global : System.Web.HttpApplication
    {
        private Task _AUComponentInstanceTask = null;
        private Task _checkForInvalidInstancesTask  = null;
        private Task _checkInstancesConnectivity = null;
        private ColorProofComponentInstance colorProofComponentInstance = null;
        private CPServer cpServer = null;

        protected void Application_Start(object sender, EventArgs e)
        {
            colorProofComponentInstance = new ColorProofComponentInstance();
            cpServer =  new CPServer();
            log4net.Config.XmlConfigurator.Configure();
            _AUComponentInstanceTask = new Task(() => colorProofComponentInstance.StartJobsTask());
            _AUComponentInstanceTask.Start();

            _checkInstancesConnectivity = new Task(() => colorProofComponentInstance.StartInstanceStatusTask());
            _checkInstancesConnectivity.Start();

            _checkForInvalidInstancesTask = new Task(() => cpServer.LoadInvalidInstancesTask());
            _checkForInvalidInstancesTask.Start();

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // TODO - log error
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            colorProofComponentInstance.StopTasks();
            cpServer.StopTasks();
        }
    }
}