﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Threading;
using ColorProofATPConnector.ColorProofServer;
using GMG.Cz.Network.Data;
using GMG.Cz.Network.Interfaces;
using ColorProofATPConnector;
using GMG.CoZone.Common;
using System.Collections.ObjectModel;
using GMG.CoZone.ColorProofConnector.ColorProofServer;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;
using GMGColorBusinessLogic;

namespace GMG.CoZone.ColorProofConnector
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class CPServer : ICzCloud
    {
        #region Private Data

        private CancellationTokenSource _checkForInvalidCancellationSource = null;
        private static Dictionary<string, string> _invalidInstances = new Dictionary<string, string>();
        private const string SECTION_NAME = "UnknownInstances";

        #endregion

        #region

        public CPServer()
        {
            _checkForInvalidCancellationSource = new CancellationTokenSource();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Main Method for Exchange data with ColorProof Instance
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public CzResultData ExchangeData(CpDataObject data)
        {
            ColorProofComponentInstance.Log(new LoggingNotification(String.Format("ExchangeData: username {0}, password {1}", data.Username, data.Password), Severity.Info));
            var rez = new CzResultData();
            try
            {
                //check the username against a local cache of invalid instances, that are still trying to connect to Deliver service.
                if (_invalidInstances.ContainsKey(data.Username))
                {
                    rez.Result = CzResult.WrongUsername;
                    ColorProofComponentInstance.Log(new LoggingNotification(String.Format("Invalid instance: Username {0} is wrong", data.Username), Severity.Warning));
                    return rez;
                }

                using (var dbContext = new DbContextBL())
                {
                    if (data is CpPairingStartup)
                    {
                        CpPairingStartup pairingData = data as CpPairingStartup;

                        ColorProofInstance instance = ColorProofInstance.Login(pairingData.PairingCode, dbContext);
                        ColorProofComponentInstance.Log(
                                   new LoggingNotification(
                                       String.Format("Instance: {0} is trying to perform pairing with code {1} ",
                                           instance.Guid, pairingData.PairingCode), Severity.Info));
                        if (instance != null)
                        {
                            if (!SettingsBL.IsValidPairingConnection(instance))
                            {
                                ColorProofComponentInstance.Log(
                                    new LoggingNotification(
                                        String.Format("Instance: {0}, PairingCode {1} was expired, connection rejected",
                                            instance.Guid, pairingData.PairingCode), Severity.Warning));
                                rez.Result = CzResult.WrongUsername;
                            }
                            else
                            {
                                rez = new CzUserVerification();
                                (rez as CzUserVerification).Username = instance.UserName;
                                (rez as CzUserVerification).Password = instance.Password;
                                ColorProofComponentInstance.Log(
                                   new LoggingNotification(
                                       String.Format("Instance: {0} has associated username {1} ",
                                           instance.Guid, instance.UserName), Severity.Info));
                            }
                        }
                        else
                        {
                            ColorProofComponentInstance.Log(
                                new LoggingNotification(
                                    String.Format("Instance: {0}, Instance with PairingCode {1} not found", instance.Guid, pairingData.PairingCode),
                                    Severity.Warning));
                            rez.Result = CzResult.WrongUsername;
                        }

                        return rez;
                    }

                    ColorProofInstance colorProofInstance = null;
                    CzResult result = TryGetRegistration(data.Username, data.Password, out colorProofInstance, dbContext);

                    if (result != CzResult.NoError)
                    {
                        ColorProofComponentInstance.Log(
                            new LoggingNotification(
                                String.Format("Instance with user {0} and password {1} not found", data.Username,
                                    data.Password), Severity.Warning));

                        rez.Result = result;
                        return rez;
                    }

                    //Set connection status
                    ColorProofComponentInstance.Log(new LoggingNotification(String.Format("Set connection status for Instance: {0}", colorProofInstance.Guid), Severity.Info));
                    SetColorProofConnectionStatus(data, colorProofInstance);

                    if (data is CpGetImageChunkDataObject)
                    {
                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format("CpGetImageChunkDataObject Instance: {0}", colorProofInstance.Guid), Severity.Info));
                        CpGetImageChunkDataObject getImageChunkData = (CpGetImageChunkDataObject)data;

                        rez = GetImageChunk(getImageChunkData.JobticketId, getImageChunkData.FileId,
                            getImageChunkData.ChunkNumber, getImageChunkData.Username, getImageChunkData.Password);
                    }

                    if (data is CpAliveDataObject)
                    {
                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format("CpAliveDataObject Instance: {0}", colorProofInstance.Guid), Severity.Info));
                        rez = new CzTicketsResultData();
                        CheckForTickets((CzTicketsResultData)rez, data, dbContext, colorProofInstance);
                    }

                    if (data is CpWorkflowDataObject)
                    {
                        //Check published workflows
                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format("CpWorkflowDataObject Instance: {0}", colorProofInstance.Guid), Severity.Info));
                        CpWorkflowDataObject workflowSenderData = (CpWorkflowDataObject)data;
                        SynchronizePublishedWorkflows(workflowSenderData, colorProofInstance, dbContext);
                    }

                    if (data is CpStartupDataObject)
                    {
                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format("CpStartupDataObject Instance: {0}", colorProofInstance.Guid), Severity.Info));
                        UpdateColorProofInstance(data, colorProofInstance);
                    }

                    if (data is CpProcessingDataObject)
                    {
                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format("CpProcessingDataObject Instance: {0}", colorProofInstance.Guid), Severity.Info));
                        SetJobticketAndImageStateUpdates(data, dbContext);
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log(String.Format(MessageConstants.CPExchangeDataFailed, data.Username, data.Password), ex);
                rez.Result = CzResult.UnspecifiedError;
            }

            return rez;
        }

        public void LoadInvalidInstancesTask()
        {
            while (!_checkForInvalidCancellationSource.Token.IsCancellationRequested)
            {
                try
                {
                    using (var context = new GMGColorContext())
                    {
                        // Get time span to next iteration
                        DateTime currentDate = DateTime.UtcNow;
                        DateTime nextDate = currentDate.AddDays(1);
                        DateTime desiredTime = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, GMGColorConfiguration.AppConfiguration.UTCHourForUpdatingInstancesList, 0, 0);

                        TimeSpan span = desiredTime - currentDate;
                        Thread.Sleep(span);

                        _invalidInstances = (from cpi in context.ColorProofInstances
                                             join u in context.Users on cpi.Owner equals u.ID
                                             join ac in context.Accounts on u.Account equals ac.ID
                                             join acs in context.AccountStatus on ac.Status equals acs.ID
                                             where acs.Key != "A" || (cpi.Password == null && cpi.PairingCode == null)
                                             select new
                                             {
                                                 cpi.UserName,
                                                 cpi.Password
                                             }).ToDictionary(t => t.UserName, t => t.Password);

                        var instances = (NameValueCollection)ConfigurationManager.GetSection(SECTION_NAME);
                        foreach (var key in instances.AllKeys)
                        {
                            _invalidInstances.Add(key, instances[key]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ColorProofComponentInstance.Log("Error on updating instances list: ", ex);
                }
            }
        }

        public void StopTasks()
        {
            _checkForInvalidCancellationSource.Cancel();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Try to get current ColorProof Instance From DataBase
        /// </summary>
        /// <param name="username">CP Username</param>
        /// <param name="password">CP Password</param>
        /// <param name="registration">ColorProof Instance</param>
        /// <returns></returns>
        private static CzResult TryGetRegistration(string username, string password, out ColorProofInstance colorProofInstance, DbContextBL context)
        {
            colorProofInstance = null;

            List<ColorProofInstance> CPInstances = context.ColorProofInstances.Include("ColorProofWorkflows").Where(t => t.UserName == username && t.IsDeleted == false).ToList();

            if (CPInstances == null || CPInstances.Count == 0)
            {
                ColorProofComponentInstance.Log(new LoggingNotification(String.Format("Instance Login: {0}, Username {1} and WrongUsername", colorProofInstance.Guid, username), Severity.Info));
                return CzResult.WrongUsername;
            }

            colorProofInstance = ColorProofInstance.Login(username, password, context);

            if (colorProofInstance == null)
            {
                ColorProofComponentInstance.Log(new LoggingNotification(String.Format("Instance Login: {0}, Username {1} and WrongPassword", colorProofInstance.Guid, username), Severity.Info));
                return CzResult.WrongPassword;
            }
            else
            {
                ColorProofComponentInstance.Log(new LoggingNotification(String.Format("Instance Login: {0}, Username {1}", colorProofInstance.Guid, username), Severity.Info));
                return CzResult.NoError;
            }
        }

        /// <summary>
        /// Updates CP Instance as connected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="registration"></param>
        private static void SetColorProofConnectionStatus(CpDataObject sender, ColorProofInstance registration)
        {
            registration.LastSeen = DateTime.UtcNow.Ticks;
            registration.IsOnline = true;
            if (registration.State == ((int)ColorProofInstance.ColorProofInstanceStateEnum.Pending) && !String.IsNullOrEmpty(registration.Version) && !String.IsNullOrEmpty(registration.SerialNumber))
            {
                registration.State = (int)ColorProofInstance.ColorProofInstanceStateEnum.Ready;
                ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.CPInstanceActivated, registration.ComputerName), Severity.Info));
            }
        }

        /// <summary>
        /// UpdateColorProof Instance Command
        /// </summary>
        /// <param name="data"></param>
        private void UpdateColorProofInstance(CpDataObject data, ColorProofInstance colorProofInstance)
        {
            try
            {
                CpStartupDataObject startupSenderData = (CpStartupDataObject)data;
                if (!String.IsNullOrEmpty(startupSenderData.ColorProofSerialNumber))
                {
                    colorProofInstance.SerialNumber = startupSenderData.ColorProofSerialNumber;
                }
                if (!String.IsNullOrEmpty(startupSenderData.ColorProofVersion))
                {
                    colorProofInstance.Version = startupSenderData.ColorProofVersion;
                }
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log(String.Format(MessageConstants.CPUpdateInfoFail, data.Username, data.Password), ex);
            }
        }

        /// <summary>
        /// Check For Jobtickets
        /// </summary>
        /// <param name="data"></param>
        private void CheckForTickets(CzTicketsResultData res, CpDataObject data, DbContextBL context, ColorProofInstance colorProofInstance)
        {
            CpAliveDataObject aliveSenderData = (CpAliveDataObject)data;

            if (aliveSenderData.JobTicketsTimestampToken == 0)
            {
                try
                {
                    res.Jobtickets = GetListofJobsforTimeStamp0(colorProofInstance.Guid);
                }
                catch (Exception ex)
                {
                    ColorProofComponentInstance.Log(MessageConstants.CPCheckForTicketsTm0, ex);
                }

            }
            else
            {
                List<ColorProofJob> jobList = ColorProofComponentInstance.Instance.GetJobsToSendByInstanceGuid(colorProofInstance.Guid);
                try
                {
                    List<JobStateUpdate> jobsToUpdate = new List<JobStateUpdate>();
                    foreach (ColorProofJob job in jobList)
                    {
                        CzJobTicket jobTicket = CreateJobTicket(job);

                        CzFile jobticketImage = new CzFile();
                        jobticketImage.ImageId = Guid.NewGuid();
                        jobticketImage.CheckSum = job.CheckSum;

                        jobticketImage.TotalSize = job.FileSize;
                        jobticketImage.FileName = job.FileName;
                        jobticketImage.Pages = job.GetPages();

                        jobTicket.Files.Add(jobticketImage);
                        res.Jobtickets.Add(jobTicket);

                        jobsToUpdate.Add(new JobStateUpdate()
                        {
                            JobGUID = job.JobGUID,
                            JobStatus = ColorProofJobStatus.Created
                        });

                        ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.ColorProofJobSentToColorProof, job.JobGUID), Severity.Info));
                    }

                    // Move jobs from CP key to job GUID key in cache
                    ColorProofComponentInstance.Instance.SetCPJobsAsInProgressInCache(jobList);

                    UpdateJobsStaus(jobsToUpdate, context);

                }
                catch (Exception ex)
                {
                    ColorProofComponentInstance.Log(String.Format(MessageConstants.CPCheckForTicketsFailed, data.Username, data.Password), ex);
                }
            }

            List<CommandTicket> commands = ColorProofComponentInstance.Instance.GetCommandsToSendByInstanceGuid(colorProofInstance.Guid);

            if (commands.Count > 0)
            {
                foreach (CommandTicket command in commands)
                {
                    res.Commands.Add(command.CloneToCzCommandTicket());
                }
            }

            res.TicketsTimestampToken = DateTime.UtcNow.Ticks;
        }

        /// <summary>
        /// Creates a new JobTicket from a AUJob
        /// </summary>
        /// <param name="job">The job from which the jobticket will be created</param>
        /// <returns>A new jobTicket from CPJob</returns>
        private CzJobTicket CreateJobTicket(ColorProofJob job)
        {
            CzJobTicket jobTicket = new CzJobTicket();

            if (job.IncludeMetaProofInformation)
            {
                jobTicket.CoZoneLabelInfo = job.JobName;
            }
            Guid guid = new Guid();
            Guid.TryParse(job.JobGUID, out guid);
            jobTicket.JobticketId = guid;
            jobTicket.WorkflowName = job.WorkflowName;
            jobTicket.CoZoneUserName = job.CoZoneUsername;
            jobTicket.BacklinkUrl = job.BacklinkUrl;

            return jobTicket;
        }

        /// <summary>
        /// Get Image Chunk Method
        /// </summary>
        /// <param name="jobticketId">ID of the JobTicket</param>
        /// <param name="fileId">ID of the Image</param>
        /// <param name="chunkNumber">chunk Number</param>
        /// <param name="username">ColorProof Instance Username</param>
        /// <param name="password">ColorProof Instance Password</param>
        /// <returns></returns>
        private CzResultData GetImageChunk(Guid jobticketId, Guid fileId, long chunkNumber, string username, string password)
        {
            CzImageChunkResultData result = new CzImageChunkResultData();
            result.Result = CzResult.NoError;

            byte[] data;
            long chunkNumberOut;

            if (chunkNumber == 0)
                ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.ColorProofJobDownloading, jobticketId), Severity.Info));

            ColorProofJob cpJob = ColorProofComponentInstance.Instance.GetJobByJobGUID(jobticketId.ToString());

            if (cpJob == null)
            {
                cpJob = ColorProofJob.GetColorProfJobIfExists(jobticketId.ToString());
                if (cpJob != null)
                {
                    ColorProofComponentInstance.Instance.AddJobInCache(cpJob);
                }
            }

            if (cpJob != null && (GMGColorDAL.GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? ImagesManager.GetImageChunkFromS3(jobticketId, cpJob.FilePath, cpJob.FileSize, chunkNumber, out chunkNumberOut, out data)
                                                                                                       : ImagesManager.GetImageChunkLocal(jobticketId, cpJob.FilePath, chunkNumber, out chunkNumberOut, out data)))
            {
                result.Data = data;
                result.ChunkNumber = chunkNumberOut;
            }
            else
            {
                ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.CzImageChunkNrInvalid, jobticketId), Severity.Warning));
                result.Result = CzResult.FileDownloadCorrupted;
            }

            return result;
        }

        /// <summary>
        /// Update job ticket status
        /// </summary>
        /// <param name="jobticketUpdates"></param>
        /// <param name="imageUpdates"></param>
        private static void SetJobticketAndImageStateUpdates(CpDataObject data, DbContextBL context)
        {
            try
            {
                CpProcessingDataObject aliveSenderData = (CpProcessingDataObject)data;

                ProcessImageStatus(aliveSenderData.ImageStatusUpdates, context);
                ProcessJobTicketStatus(aliveSenderData.JobticketStatusUpdates, context);
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log(String.Format(MessageConstants.CPSetJobTicketAndImageStatusFailed, data.Username, data.Password), ex);
            }
        }

        private static void ProcessJobTicketStatus(ICollection<CpJobticketStatusUpdate> jobticketStatusUpdates, DbContextBL context)
        {
            if (jobticketStatusUpdates.Count > 0)
            {
                List<JobStateUpdate> jobsToUpdate = new List<JobStateUpdate>();
                //Process Job ticket status updates
                foreach (CpJobticketStatusUpdate job in jobticketStatusUpdates)
                {
                    jobsToUpdate.Add(new JobStateUpdate()
                    {
                        JobGUID = job.CzJobticketId.ToString(),
                        JobStatus = (ColorProofJobStatus)job.NewStatus
                    });

                    ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.CPJobStatusUpdated, job.CzJobticketId.ToString(), ((ColorProofJobStatus)job.NewStatus).ToString(), job.Message ?? String.Empty), Severity.Info));

                }

                UpdateJobsStaus(jobsToUpdate, context);
            }
        }

        private static void ProcessImageStatus(ICollection<CpImageStatusUpdate> imageStatusUpdates, DbContextBL context)
        {
            // For debug tests
            //List<CpImageStatusUpdate> imageUpdates = TempData.CreateTestFiles();

            if (imageStatusUpdates.Count > 0)
            {
                var imagesToUpdate = GetImagesToUpdate(imageStatusUpdates, context);
                
                UpdateImageStatus(imagesToUpdate, context);
            }
        }

        private static List<ImageStateUpdate> GetImagesToUpdate(ICollection<CpImageStatusUpdate> imageStatusUpdates, DbContextBL context)
        {
            List<ImageStateUpdate> imagesToUpdate = new List<ImageStateUpdate>();

            foreach (CpImageStatusUpdate imageUpdate in imageStatusUpdates)
            {
                ImageStateUpdate imagetoUpdate = new ImageStateUpdate();
                if (imageUpdate.ProofVerificationResults.Count > 0)
                {
                    ColorProofComponentInstance.Log(new LoggingNotification(String.Format(MessageConstants.CPUpdateATPJobStatusHasVerificationResults, imageUpdate.UserFriendlyName, imageUpdate.ProofVerificationResults.Count), Severity.Info));

                    foreach (CpProofVerificationResult proofVerif in imageUpdate.ProofVerificationResults)
                    {
                        foreach (CpSpotColorVerificationResult spotColorVerif in proofVerif.SpotColorVerificationResults)
                        {
                            imagetoUpdate.proofVerificationResult.Add(new ImageProofVerificationResult()
                            {
                                MaxDelta = spotColorVerif.MaxDeltaEInGamut,
                                AvgDelta = spotColorVerif.AverageDeltaEInGamut,
                                StripName = proofVerif.StripName,
                                IsSpotColorVerification = true
                            });
                        }

                        if (proofVerif.VerificationResults.Count > 0)
                        {
                            imagetoUpdate.proofVerificationResult.Add(new ImageProofVerificationResult()
                            {
                                MaxDelta = proofVerif.VerificationResults.Where(t => t.VerificationType == ProofVerificationType.Maximum).SingleOrDefault().Value,
                                AvgDelta = proofVerif.VerificationResults.Where(t => t.VerificationType == ProofVerificationType.Average).SingleOrDefault().Value,
                                AllowedTolleranceForMax = proofVerif.VerificationResults.Where(t => t.VerificationType == ProofVerificationType.Maximum).SingleOrDefault().MaxValue,
                                AllowedTolleranceForAvg = proofVerif.VerificationResults.Where(t => t.VerificationType == ProofVerificationType.Average).SingleOrDefault().MaxValue,
                                ProofResult = proofVerif.VerificationResults.All(t => t.Result == true).ToString(),
                                StripName = proofVerif.StripName,
                                IsSpotColorVerification = false
                            });
                        }
                    }
                }
                imagetoUpdate.UserFriendlyName = imageUpdate.UserFriendlyName;
                imagetoUpdate.ImageState = (ImageState)imageUpdate.NewStatus;
                imagetoUpdate.JobGuid = imageUpdate.CzJobticketId.ToString();
                imagetoUpdate.ErrMessage = imageUpdate.Message;
                ColorProofComponentInstance.Log(new LoggingNotification(string.Format(MessageConstants.CPJobPageStatusUpdated, imagetoUpdate.UserFriendlyName, (imagetoUpdate.ImageState).ToString()), Severity.Info));

                imagesToUpdate.Add(imagetoUpdate);
            }

            return imagesToUpdate;
        }

        /// <summary>
        /// Returns the list of jobs to be sent to ColorProof
        /// </summary>
        /// <param name="CPusername"></param>
        /// <param name="CPpassword"></param>
        /// <returns></returns>
        private ICollection<CzJobTicket> GetListofJobsforTimeStamp0(string CPGuid)
        {
            ICollection<CzJobTicket> jobList = new List<CzJobTicket>();
            DeliverJob failedJob = new DeliverJob();
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    List<DeliverJob> listojJobs = (from dj in context.DeliverJobs
                                                   join status in context.DeliverJobStatus on dj.Status equals status.ID
                                                   join cpczwf in context.ColorProofCoZoneWorkflows on dj.CPWorkflow equals cpczwf.ID
                                                   join cpwf in context.ColorProofWorkflows on cpczwf.ColorProofWorkflow equals cpwf.ID
                                                   join cpins in context.ColorProofInstances on cpwf.ColorProofInstance equals cpins.ID
                                                   where cpins.Guid == CPGuid &&
                                                        status.Key != (int)DeliverJobStatus.ErrorDeleted &&
                                                        status.Key != (int)DeliverJobStatus.CompletedDeleted &&
                                                        status.Key != (int)DeliverJobStatus.Submitting && 
                                                        dj.IsDeleted == false
                                                   select dj).ToList();

                    bool needToSave = false;

                    foreach (DeliverJob job in listojJobs)
                    {
                        failedJob = job;
                        CzJobTicket jobTicket = new CzJobTicket();

                        if (job.IncludeProofMetaInformationOnLabel)
                        {
                            jobTicket.CoZoneLabelInfo = job.Job1.Title;
                        }
                        Guid guid = new Guid();
                        Guid.TryParse(job.Guid, out guid);
                        jobTicket.JobticketId = guid;
                        jobTicket.WorkflowName = job.ColorProofCoZoneWorkflow.ColorProofWorkflow1.Name;
                        jobTicket.BacklinkUrl = job.BackLinkUrl;

                        CzFile jobticketImage = new CzFile();
                        jobticketImage.ImageId = Guid.NewGuid();
                        string FilePath = ImagesManager.GetJobFilePath(job.Guid, job.FileName);

                        if (job.FileCheckSum == null)
                        {
                            jobticketImage.CheckSum = ImagesManager.GetJobFileCheckSum(job.Guid, FilePath, job.FileName);
                            job.FileCheckSum = jobticketImage.CheckSum;
                            needToSave = true;
                        }
                        else
                        {
                            jobticketImage.CheckSum = job.FileCheckSum;
                        }

                        jobticketImage.TotalSize = Convert.ToInt64(Convert.ToDouble(job.Size));
                        jobticketImage.FileName = job.FileName;

                        Collection<int> pages = new Collection<int>();
                        pages = GMG.CoZone.Common.Utils.GetPages(job.Pages);
                        jobticketImage.Pages = pages;

                        jobTicket.Files.Add(jobticketImage);

                        jobList.Add(jobTicket);
                    }

                    if (needToSave)
                    {
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log(String.Format(MessageConstants.CPGetDeliverJobsFailed, CPGuid, failedJob.Guid), ex);
            }
            return jobList;
        }

        /// <summary>
        /// Update jobs status of the specified jobs
        /// </summary>
        /// <param name="jobsToUpdate"></param>
        private static void UpdateJobsStaus(List<JobStateUpdate> jobsToUpdate, DbContextBL context)
        {
            try
            {
                foreach (JobStateUpdate update in jobsToUpdate)
                {
                    DeliverJob deliverJob = context.DeliverJobs.SingleOrDefault(t => t.Guid == update.JobGUID && t.IsDeleted == false && t.DeliverJobStatu.Key != (int)DeliverJobStatus.CompletedDeleted && t.DeliverJobStatu.Key != (int)DeliverJobStatus.ErrorDeleted);

                    if (deliverJob == null)
                    {
                        continue;
                    }

                    if (deliverJob.IsCancelRequested)
                    {
                        deliverJob.IsCancelRequested = false;
                    }
                    
                    DeliverJobStatus newStatus = Utils.Utilities.GetDeliverJobStatusFromColorProofJobStatus(update.JobStatus, deliverJob);

                    DeliverJobStatu statusInCoZone = context.DeliverJobStatus.SingleOrDefault(s => s.Key == (int)newStatus);
                    ColorProofComponentInstance.Log(String.Format("Update Job Status for guid {0} with ColorProof status {1}, CoZone status {2}", update.JobGUID, update.JobStatus.ToString(), statusInCoZone.Name), Severity.Info);

                    if (newStatus == DeliverJobStatus.Completed)
                    {
                        NotificationServiceBL.CreateNotification(new GMGColorNotificationService.DeliverJobCompleted()
                        {
                            EventCreator = deliverJob.Owner,
                            InternalRecipient = deliverJob.Owner,
                            CreatedDate = DateTime.UtcNow,
                            JobsGuids = new[] { deliverJob.Guid }
                        },
                                                                deliverJob.User,
                                                                context
                                                                );

                        var deliverExternalUsers = DeliverExternalCollaborator.GetDeliverCollaboratorsByJobGuids(new[] { deliverJob.Guid }, GMGColorDAL.EventType.GetDeliverEventTypeKey(GMGColorDAL.EventType.TypeEnum.Deliver_File_Completed), context);
                        //send emails for deliver external users 
                        foreach (var user in deliverExternalUsers)
                        {
                            NotificationServiceBL.CreateNotification(new GMGColorNotificationService.DeliverJobCompleted
                            {
                                EventCreator = deliverJob.Owner,
                                ExternalRecipient = user,
                                CreatedDate = DateTime.UtcNow,
                                JobsGuids = new[] { deliverJob.Guid }
                            },
                                                                   deliverJob.User,
                                                                   context
                                                                 );
                        }

                        // if job came from Upload Engine check if needed to be uploaded on Upload Engine profile Preset
                        if (!String.IsNullOrEmpty(deliverJob.FTPSourceFolder))
                        {
                            DeliverBL.UploadDeliverJobToFTPIfNeeded(deliverJob, context);
                        }
                    }
                    else if (newStatus == DeliverJobStatus.ProcessingError)
                    {
                        NotificationServiceBL.CreateNotification(new GMGColorNotificationService.DeliverJobError
                        {
                            EventCreator = deliverJob.Owner,
                            InternalRecipient = deliverJob.Owner,
                            CreatedDate = DateTime.UtcNow,
                            JobsGuids = new[] { deliverJob.Guid }
                        },
                                                                 deliverJob.User,
                                                                 context
                                                                 );

                        var deliverExternalUsers = DeliverExternalCollaborator.GetDeliverCollaboratorsByJobGuids(new[] { deliverJob.Guid }, GMGColorDAL.EventType.GetDeliverEventTypeKey(GMGColorDAL.EventType.TypeEnum.Deliver_File_Errored), context);

                        //send emails for deliver external users
                        foreach (var user in deliverExternalUsers)
                        {
                            NotificationServiceBL.CreateNotification(new GMGColorNotificationService.DeliverJobError
                            {
                                EventCreator = deliverJob.Owner,
                                ExternalRecipient = user,
                                CreatedDate = DateTime.UtcNow,
                                JobsGuids = new[] { deliverJob.Guid }
                            },
                                                                   deliverJob.User,
                                                                   context
                                                                 );
                        }
                    }


                    if (statusInCoZone != null)
                    {
                        deliverJob.DeliverJobStatu = statusInCoZone;
                    }

                    ColorProofJobStatu statusInColorProof = context.ColorProofJobStatus.FirstOrDefault(o => o.Key == (int)update.JobStatus);
                    if (statusInColorProof != null)
                    {
                        deliverJob.ColorProofJobStatu = statusInColorProof;
                    }
                }
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log("Update Job Status Failed", ex);
            }
        }

        /// <summary>
        /// Update Image Statuses in Database
        /// </summary>
        /// <param name="imagesToUpdate">List of New Updates</param>
        /// <param name="dbContext">Database Context</param>
        private static void UpdateImageStatus(List<ImageStateUpdate> imagesToUpdate, DbContextBL dbContext)
        {
            try
            {
                //Remove duplicate
                imagesToUpdate = (imagesToUpdate.GroupBy(x => new { x.UserFriendlyName, x.JobGuid }).Select(x => x.First())).ToList();

                var groupedImagesByJob = imagesToUpdate.GroupBy(x =>  x.JobGuid);
                foreach (var job in groupedImagesByJob)
                {
                    string jobGuid = job.Key;
                    DeliverJob deliverJob = dbContext.DeliverJobs.SingleOrDefault(t => t.Guid == jobGuid && t.IsDeleted == false);
                    
                    if (deliverJob != null)
                    {
                        var images = job.Select(img => img).ToList();
                        if (images.Any())
                        {
                           SetJobStatusBasedOnImagesStatus(deliverJob, images, dbContext);

                            foreach (ImageStateUpdate image in images)
                            {
                                string userFriendlyName = image.UserFriendlyName;
                                if (!String.IsNullOrEmpty(jobGuid) && !String.IsNullOrEmpty(userFriendlyName))
                                {
                                    var existingPage = dbContext.DeliverJobPages.SingleOrDefault(t => t.DeliverJob1.Guid == jobGuid && t.UserFriendlyName == userFriendlyName);
                                    if (existingPage != null)
                                    {
                                        List<DeliverJobPageVerificationResult> verifResults = dbContext.DeliverJobPageVerificationResults.Where(t => t.DeliverJobPage == existingPage.ID).ToList();

                                        for (int i = verifResults.Count - 1; i >= 0; i--)
                                        {
                                            dbContext.DeliverJobPageVerificationResults.Remove(verifResults[i]);
                                        }

                                        dbContext.DeliverJobPages.Remove(existingPage);
                                    }

                                    int newStatus = Convert.ToInt32(image.ImageState);

                                    DeliverJobPage pageToAdd = new DeliverJobPage();
                                    pageToAdd.UserFriendlyName = userFriendlyName;
                                    pageToAdd.DeliverJob = deliverJob.ID;
                                    pageToAdd.Status = dbContext.DeliverJobPageStatus.SingleOrDefault(t => t.Key == newStatus).ID;
                                    if (!String.IsNullOrEmpty(image.ErrMessage))
                                    {
                                        pageToAdd.ErrorMessage = image.ErrMessage;
                                    }
                                    if (deliverJob.LogProofControlResults)
                                    {
                                        foreach (var verifResult in image.proofVerificationResult)
                                        {
                                            DeliverJobPageVerificationResult newVerificationResult = new DeliverJobPageVerificationResult();
                                            newVerificationResult.StripName = verifResult.StripName;
                                            newVerificationResult.MaxDelta = verifResult.MaxDelta;
                                            newVerificationResult.AvgDelta = verifResult.AvgDelta;
                                            newVerificationResult.IsSpotColorVerification = Convert.ToBoolean(verifResult.IsSpotColorVerification);
                                            if (verifResult.AllowedTolleranceForMax != 0)
                                            {
                                                newVerificationResult.AllowedTolleranceMax = verifResult.AllowedTolleranceForMax;
                                            }
                                            if (verifResult.AllowedTolleranceForAvg != 0)
                                            {
                                                newVerificationResult.AllowedTolleranceAvg = verifResult.AllowedTolleranceForAvg;
                                            }
                                            if (!String.IsNullOrEmpty(verifResult.ProofResult))
                                            {
                                                newVerificationResult.ProofResult = Convert.ToBoolean(verifResult.ProofResult);
                                            }
                                            dbContext.DeliverJobPageVerificationResults.Add(newVerificationResult);
                                            pageToAdd.DeliverJobPageVerificationResults.Add(newVerificationResult);
                                        }
                                    }
                                    dbContext.DeliverJobPages.Add(pageToAdd);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log("Update Image Status Method Failed", ex);
            }
        }

        private static void SetJobStatusBasedOnImagesStatus(DeliverJob job, List<ImageStateUpdate> imageStatusUpdates, DbContextBL context)
        {
            if ((job.DeliverJobStatu.Key == (int)DeliverJobStatus.Completed && job.ColorProofJobStatu.Key == (int)ColorProofJobStatus.Completed) ||
                (job.DeliverJobStatu.Key == (int)DeliverJobStatus.ProcessingError && job.ColorProofJobStatu.Key == (int)ColorProofJobStatus.ProcessingError))
            {
                var allPages = job.DeliverJobPages.Select(djp => new DeliverPage
                {
                    UserFriendlyName = djp.UserFriendlyName,
                    Status = djp.DeliverJobPageStatu.Key
                }).ToList();

                foreach (var page in allPages)
                {
                    var cpImageStatusUpdate =
                        imageStatusUpdates.FirstOrDefault(p => p.UserFriendlyName == page.UserFriendlyName);
                    if (cpImageStatusUpdate != null)
                    {
                        page.Status = (int)((ImageState)cpImageStatusUpdate.ImageState);
                    }
                }

                var deliverJobStatusKey = job.DeliverJobStatu.Key;
                if (job.DeliverJobStatu.Key == (int)DeliverJobStatus.ProcessingError &&
                    allPages.All(t => t.Status == (int)DeliverPageStatus.Deleted))
                {
                    deliverJobStatusKey = (int)DeliverJobStatus.ErrorDeleted;
                }
                else if (job.DeliverJobStatu.Key == (int)DeliverJobStatus.Completed &&
                         allPages.All(t => t.Status == (int)DeliverPageStatus.Deleted))
                {
                    deliverJobStatusKey = (int)DeliverJobStatus.CompletedDeleted;
                }

                job.Status = context.DeliverJobStatus.Where(djs => djs.Key == deliverJobStatusKey).Select(djs => djs.ID).FirstOrDefault();
            }
        }

        #endregion

        #region Workflow Synchronization

        /// <summary>
        /// Synchronize Workflows in Database
        /// </summary>
        /// <param name="senderData">New Workflow Data</param>
        /// <param name="registration">ColorProof Instance</param>
        /// <param name="context">Database Context</param>
        private static void SynchronizePublishedWorkflows(CpWorkflowDataObject senderData, ColorProofInstance registration, GMGColorContext context)
        {
            try
            {
                if (senderData.PublishedWorkflows != null)
                {
                    LookForNewPublishedWorkflows(senderData.PublishedWorkflows, registration, context);

                    LookForChangedProofCapabilities(senderData.PublishedWorkflows, registration, context);

                    LookForDeactivatedWorkflows(senderData.PublishedWorkflows, registration, context);

                    LookForOnlyRenamedWorkflow(senderData.PublishedWorkflows, registration, context);
                }
            }
            catch (Exception ex)
            {
                ColorProofComponentInstance.Log(String.Format("SynchronizePublishedWorkflows method failed for Instance with username {0} and password {1}", senderData.Username, senderData.Password), ex);
            }
        }

        private static void LookForOnlyRenamedWorkflow(ICollection<CpWorkflow> senderData, ColorProofInstance registration, GMGColorContext context)
        {
            foreach (CpWorkflow cpWorkflow in senderData)
            {
                ColorProofWorkflow renamedWorkflow = null;

                bool isNameUsed = false;
                //First check is name already used - if yes this is update scenario, handled before
                foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
                {
                    if (czWorkflow.Name == cpWorkflow.Name)
                    {
                        isNameUsed = true;
                        break;
                    }
                }

                if (!isNameUsed &&
                    cpWorkflow.Name != cpWorkflow.OldName) //This is rename scenario
                {
                    foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
                    {
                        if (cpWorkflow.OldName == czWorkflow.Name) //Find by old name
                        {
                            //If different - it is also a Proofing Capabilities change => handled before
                            if (!Utils.Utilities.IsDifferentProofCapabilities(czWorkflow, cpWorkflow))
                            {
                                renamedWorkflow = czWorkflow;
                            }
                        }
                    }
                }

                if (renamedWorkflow != null)
                {
                    renamedWorkflow.Name = cpWorkflow.Name; //Actual rename
                    renamedWorkflow.OldName = cpWorkflow.OldName;//Set the old name becouse UI match by clone.OldName == uiWorkflow.Name
                    renamedWorkflow.IsActivated = true;
                }
            }
        }

        private static void LookForDeactivatedWorkflows(ICollection<CpWorkflow> senderData, ColorProofInstance registration, GMGColorContext context)
        {
            foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
            {
                //Only check for Activated workflow
                if (czWorkflow.IsActivated.GetValueOrDefault())
                {
                    bool isDeleted = true;
                    foreach (CpWorkflow cpWorkflow in senderData)
                    {
                        //If name is found - it is not deleted, if not - deleted
                        if (cpWorkflow.Name == czWorkflow.Name)
                        {
                            isDeleted = false;
                            break;
                        }
                    }

                    if (isDeleted)
                    {
                        //Do not Delete Jobtickets related to this Workflow. Jobtickets will not be sent to ColorProof becouse Workflow in not Activated, 
                        //but if in some point ColoProof again report Workflow with same name and no Proofing Capabilities changes - Jobtickets will be send to ColorProof on startup.
                        czWorkflow.IsActivated = false;
                        czWorkflow.OldName = czWorkflow.Name; //Set the old name becouse UI match by clone.OldName == uiWorkflow.Name

                        var jobs = context.DeliverJobs.Where(t => t.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ID == czWorkflow.ID && t.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.Cancelled
                                                                                           && t.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.Completed &&
                                                                                           t.DeliverJobStatu.Key != (int)GMG.CoZone.Common.DeliverJobStatus.CompletedDeleted).ToList();
                        var jobstatus = context.DeliverJobStatus.Where(t => t.Key == (int)GMG.CoZone.Common.DeliverJobStatus.Cancelled).SingleOrDefault();
                        foreach (DeliverJob job in jobs)
                        {
                            job.Status = jobstatus.ID;
                        }

                        // deactivate also the workflows from the ColorProofCoZoneWorkflow table that are linked with the current czWorkflow
                        czWorkflow.ColorProofCoZoneWorkflows.ToList().ForEach(o => o.IsAvailable = false);
                    }
                }
            }
        }

        private static void LookForChangedProofCapabilities(ICollection<CpWorkflow> senderData, ColorProofInstance registration, GMGColorContext context)
        {
            foreach (CpWorkflow cpWorkflow in senderData)
            {
                ColorProofWorkflow changedPublishedWorkflow = null;
                string workflowRenamed = string.Empty;
                //First check by Name for update + activate of existing use-case
                foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
                {
                    if (!czWorkflow.IsActivated.GetValueOrDefault() &&
                        cpWorkflow.Name == czWorkflow.Name)
                    {
                        workflowRenamed = cpWorkflow.OldName;
                        cpWorkflow.OldName = czWorkflow.Name; //Set the old name becouse UI match by clone.OldName == uiWorkflow.Name. This is switch case - so we need to switch cpWorkflow old name.
                        czWorkflow.IsActivated = true;

                        if (Utils.Utilities.IsDifferentProofCapabilities(czWorkflow, cpWorkflow))
                        {
                            changedPublishedWorkflow = czWorkflow;
                        }
                        break;
                    }
                }

                //If Workflow is renamed to existing name - deactivate workflow with old name
                if (!String.IsNullOrEmpty(workflowRenamed))
                {
                    foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
                    {
                        if (czWorkflow.Name == workflowRenamed &&
                            czWorkflow.IsActivated.GetValueOrDefault())
                        {
                            czWorkflow.OldName = czWorkflow.Name;//Set the old name becouse UI match by clone.OldName == uiWorkflow.Name
                            czWorkflow.IsActivated = false;
                        }
                    }
                }

                //If nothing is found - check by OldName if this is rename + update. It is also good for scenario when oldName == name
                if (changedPublishedWorkflow == null)
                {
                    foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
                    {
                        if (cpWorkflow.OldName == czWorkflow.Name)
                        {
                            if (Utils.Utilities.IsDifferentProofCapabilities(czWorkflow, cpWorkflow))
                            {
                                changedPublishedWorkflow = czWorkflow;
                            }
                            break;
                        }
                    }
                }

                if (changedPublishedWorkflow != null)
                {
                    changedPublishedWorkflow.Name = cpWorkflow.Name;
                    changedPublishedWorkflow.OldName = cpWorkflow.OldName;
                    changedPublishedWorkflow.ProofStandard = cpWorkflow.ProofStandard;
                    changedPublishedWorkflow.MaximumUsablePaperWidth = cpWorkflow.MaximumUsablePaperWidth;
                    changedPublishedWorkflow.IsActivated = changedPublishedWorkflow.IsActivated;
                    changedPublishedWorkflow.IsInlineProofVerificationSupported = cpWorkflow.IsInlineProofVerificationSupported;
                    changedPublishedWorkflow.SupportedSpotColors = Utils.Utilities.GetSupportedSpottedColorString(cpWorkflow.SupportedSpotColors);
                    //SetDeleteTicketsForWorkflowName(cpWorkflow.OldName, registration);
                }
            }
        }

        private static void LookForNewPublishedWorkflows(ICollection<CpWorkflow> senderData, ColorProofInstance registration, GMGColorContext context)
        {
            foreach (CpWorkflow cpWorkflow in senderData)
            {
                if (String.IsNullOrEmpty(cpWorkflow.OldName) ||
                    //This is for use-case of crating registration with same username again, so we can sync with DTOCoZoneWorkflows in ColorProof database 
                    !IsWorkflowWithSameNamePresent(cpWorkflow.OldName, registration.ColorProofWorkflows))
                {
                    //See if this is just new Workflow or we need to activate old with same Name
                    ColorProofWorkflow workflowWithSameName = null;
                    foreach (ColorProofWorkflow czWorkflow in registration.ColorProofWorkflows)
                    {
                        if (cpWorkflow.Name == czWorkflow.Name)
                        {
                            workflowWithSameName = czWorkflow;
                            //Workflow with same name found, but does it have same Proofing Capabilities?
                            if (Utils.Utilities.IsDifferentProofCapabilities(czWorkflow, cpWorkflow))
                            {
                                //SetDeleteTicketsForWorkflowName(cpWorkflow.Name, registration);
                            }
                            break;
                        }
                    }

                    if (workflowWithSameName != null)
                    {
                        workflowWithSameName.IsActivated = true;
                        var czWorkflows = context.ColorProofCoZoneWorkflows.Where(t => t.ColorProofWorkflow1.ID == workflowWithSameName.ID);
                        czWorkflows.ToList().ForEach(t => t.IsAvailable = true);
                    }
                    else
                    {
                        ColorProofWorkflow newCzWorkflow = new ColorProofWorkflow();
                        Utils.Utilities.CreateWorkflowBOObject(newCzWorkflow, cpWorkflow);
                        registration.ColorProofWorkflows.Add(newCzWorkflow);
                    }
                }
            }
        }

        private static bool IsWorkflowWithSameNamePresent(string workflowName, ICollection<ColorProofWorkflow> allworkflows)
        {
            bool retVal = false;

            foreach (ColorProofWorkflow wf in allworkflows)
            {
                if (wf.Name == workflowName)
                {
                    retVal = true;
                    break;
                }
            }

            return retVal;
        }

        #endregion
    }
}
