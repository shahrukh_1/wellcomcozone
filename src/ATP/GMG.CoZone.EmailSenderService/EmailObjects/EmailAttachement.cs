﻿using System.IO;

namespace GMG.CoZone.EmailSenderService.EmailObjects
{
    public class EmailAttachement
    {
        public Stream Stream { get; set; }
        public string FileName { get; set; }
    }
}
