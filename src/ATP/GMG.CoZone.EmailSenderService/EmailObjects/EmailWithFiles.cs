﻿using System.Net.Mail;

namespace GMG.CoZone.EmailSenderService.EmailObjects
{
    public class EmailWithFiles
    {
        public int DbEmailId { get; set; }
        public MailMessage Email { get; set; }
        public EmailAttachement[] AttachedFiles { get; set; }
    }
}
