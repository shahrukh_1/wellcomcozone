﻿using System;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMG.CoZone.Common.Interfaces;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using GMG.CoZone.EmailSenderService.EmailObjects;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;

namespace GMG.CoZone.EmailSenderService.Workers
{
    public class EmailSenderWorker : IEmailSenderWorker
    {
        private IGMGConfiguration _gmgConfiguration;
        private IEmailRetriever _emailRetriever;
        private IUnityContainer _unityContainer;
        private IServiceLogger _serviceLogger;

        private string _region = string.Empty;

        public EmailSenderWorker(IGMGConfiguration gmgConfiguration, IEmailRetriever emailRetriever, IServiceLogger servicelogger)
        {
            _gmgConfiguration = gmgConfiguration;
            _emailRetriever = emailRetriever;
            _region = string.IsNullOrEmpty(_region) ? (!_gmgConfiguration.IsEnabledS3Bucket) ? string.Empty : _gmgConfiguration.AWSRegion : _region;
            _unityContainer = UnityConfig.GetContainerInstance();
            _serviceLogger = servicelogger;
        }

        public void SendEmails()
        {
            var groupedEmailsToSend = _emailRetriever.GetEmailsToSend();

            if (_gmgConfiguration.IsSendEmails)
            {
                if (groupedEmailsToSend.Count > 0)
                {
                    foreach (var group in groupedEmailsToSend)
                    {
                        _serviceLogger.Log(new LoggingNotification("EmailSender: Total mails to send: " + group.Value.Count, Severity.Info));
                        if (group.Key == 0)
                        {
                            SendEmails(group.Value, _unityContainer.Resolve<ISmtpEmailSender>("defaultEmailSender"), _region, group.Key.Value);
                        }
                        else
                        {
                            SendEmails(group.Value, _unityContainer.Resolve<ISmtpEmailSender>("customEmailSender"), _region, group.Key.Value);
                        }
                    }
                }
            }
        }

        public void Stop()
        {

        }

        /// <summary>
        /// Send emails using different smpt implementations
        /// </summary>
        /// <param name="emailList">Emails list to send</param>
        /// <param name="emailService">The smtp sender implmentation</param>
        /// <param name="region">The region (local or aws)</param>
        private void SendEmails(List<EmailWithFiles> emailList, ISmtpEmailSender emailService, string region, int groupAccount = 0)
        {
            try
            {
                emailService.SendEmails(emailList, region, groupAccount);
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("SendEmails failed with the following error: {0}", ex.Message), ex);
            }
        }
    }
}
