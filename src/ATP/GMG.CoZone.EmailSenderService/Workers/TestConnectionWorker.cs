﻿using GMG.CoZone.EmailSenderService.Interfaces;
using GMG.CoZone.Common.Interfaces;
using Newtonsoft.Json;
using GMG.CoZone.Common.DomainModels;
using GMGColor.AWS;
using System;


namespace GMG.CoZone.EmailSenderService.Workers
{
    public class TestConnectionWorker : ITestConnectionWorker
    {
        private IQueueMessageManager _queueMessageManager;
        private IGMGConfiguration _gmgConfiguration;
        private ISmtpTestConnectionEmailSender _smtpTestConnectionEmailSender;
        private IServiceLogger _serviceLogger;
        private string _readQueueName;
        private string _writeQueueName;

        public TestConnectionWorker(IQueueMessageManager queueMessageManager, 
                                    IGMGConfiguration gmgConfiguration, 
                                    ISmtpTestConnectionEmailSender smtpTestConnectionEmailSender,
                                    IServiceLogger serviceLogger)
        {
            _queueMessageManager = queueMessageManager;
            _gmgConfiguration = gmgConfiguration;
            _smtpTestConnectionEmailSender = smtpTestConnectionEmailSender;
            _serviceLogger = serviceLogger;
            _readQueueName = _gmgConfiguration.CustomSmtpWriteQueueName.ToString();
            _writeQueueName = _gmgConfiguration.CustomSmtpReadQueueName.ToString(); 
        }

        public void TestConnections()
        {
            try
            {
                if (_gmgConfiguration.IsEnabledAmazonSQS)
                {
                    ProcessSmtpTestConnectionMessagesFromAWSQueue(_readQueueName, _writeQueueName);
                }
                else
                {
                    ProcessSmtpConnectionMessagesFromLocalQueue(_readQueueName, _writeQueueName);
                }
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("TestConnection failed with the following error {0}", ex.Message), ex);
            }
        }

        public void ProcessSmtpConnectionMessagesFromLocalQueue(string _readQueueName, string _writeQueueName)
        {
            var queueMessages = _queueMessageManager.ReadMessagesFromLocalQueue(_readQueueName);

            foreach (var message in queueMessages)
            {
                try
                {
                    var messageBody = JsonConvert.DeserializeObject<CustomSmtpTestConnectionModel>(message);
                    var connectionError = _smtpTestConnectionEmailSender.SendEmails(messageBody);

                    _queueMessageManager.WriteMessageToLocalQueue(_writeQueueName, JsonConvert.SerializeObject(new CustomSmtpEmailSenderReply
                    {
                        AccountId = messageBody.AccountId,
                        MessageGuid = messageBody.MessageGuid,
                        Error = connectionError
                    }));
                }
                catch (Exception ex)
                {

                    _serviceLogger.Log(string.Format("Send Test Email in Local config failed with the following error message: {0}", ex.Message), ex);
                }
            }
        }

        public void ProcessSmtpTestConnectionMessagesFromAWSQueue(string readQueueName, string writeQueueName)
        {
            var queueMessages = _queueMessageManager.ReadMessagesFromAWSQueue(readQueueName);

            foreach (var message in queueMessages)
            {
                try
                {
                    // Deserialize message object body
                    var messageBody = JsonConvert.DeserializeObject<CustomSmtpTestConnectionModel>(message.Body);

                    // Send test connection email
                    var connectionError = _smtpTestConnectionEmailSender.SendEmails(messageBody);

                    _queueMessageManager.WriteMessageToAWSQueue(writeQueueName, JsonConvert.SerializeObject(new CustomSmtpEmailSenderReply
                    {
                        AccountId = messageBody.AccountId,
                        MessageGuid = messageBody.MessageGuid,
                        Error = connectionError
                    }));

                    // After read, delete message
                    AWSSimpleQueueService.DeleteMessage(readQueueName, message, _gmgConfiguration.AWSAccessKey, _gmgConfiguration.AWSSecretKey, _gmgConfiguration.AWSRegion);
                }
                catch (Exception ex)
                {
                    _serviceLogger.Log(string.Format("Send Test Email in AWS config failed with the following error message: {0}", ex.Message), ex);
                }
            }
        }
    }
}
