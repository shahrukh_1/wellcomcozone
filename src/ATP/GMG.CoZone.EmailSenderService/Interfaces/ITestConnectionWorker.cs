﻿namespace GMG.CoZone.EmailSenderService.Interfaces
{
    public interface ITestConnectionWorker
    {
        /// <summary>
        /// Test custom smtp connection
        /// </summary>
        void TestConnections();

        /// <summary>
        /// Process AWS queue messages for test custom smtp connection and write reply with status to a queue
        /// </summary>
        /// <param name="readQueueName">Read queue name</param>
        /// <param name="writeQueueName">Write queue name</param>
        void ProcessSmtpTestConnectionMessagesFromAWSQueue(string readQueueName, string writeQueueName);

        /// <summary>
        /// Process Local queue messages for test custom smtp connection and write reply with status to a queue
        /// </summary>
        /// <param name="readQueueName">Read queue name</param>
        /// <param name="writeQueueName">Write queue name</param>
        void ProcessSmtpConnectionMessagesFromLocalQueue(string readQueueName, string writeQueueName);
    }
}
