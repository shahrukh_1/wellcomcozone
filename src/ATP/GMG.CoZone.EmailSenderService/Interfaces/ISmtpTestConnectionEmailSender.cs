﻿using GMG.CoZone.Common.DomainModels;

namespace GMG.CoZone.EmailSenderService.Interfaces
{
    public interface ISmtpTestConnectionEmailSender
    {
        string SendEmails(CustomSmtpTestConnectionModel emailData);
    }
}
