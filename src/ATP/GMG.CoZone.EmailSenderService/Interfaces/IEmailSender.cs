﻿namespace GMG.CoZone.EmailSenderService.Interfaces
{
    /// <summary>
    /// Email sender interface
    /// </summary>
    public interface IEmailSender
    {
        /// <summary>
        /// Start workers
        /// </summary>
        void Start();

        /// <summary>
        /// Stop workers
        /// </summary>
        void Stop();
    }
}
