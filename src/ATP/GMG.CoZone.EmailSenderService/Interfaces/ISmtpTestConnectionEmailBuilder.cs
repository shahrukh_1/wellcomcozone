﻿using GMG.CoZone.Common.DomainModels;
using System.Net.Mail;

namespace GMG.CoZone.EmailSenderService.Interfaces
{
    /// <summary>
    /// Interface used to build email messages for test custom smtp connection
    /// </summary>
    public interface ISmtpTestConnectionEmailBuilder
    {
        /// <summary>
        /// Build the email message to check Custom Smtp Connection
        /// </summary>
        /// <param name="customSmtpTestSettings">The configured Custom SMTP Settings</param>
        /// <returns>Mail message object to send</returns>
        MailMessage BuildMessage(CustomSmtpTestModel customSmtpTestSettings);

        /// <summary>
        /// Get the email template used for the Custom SMTP Server connection test
        /// </summary>
        /// <returns>Email body as a string</returns>
        string GetEmailTemplate();

        /// <summary>
        /// Replace email template placeholders
        /// </summary>
        /// <param name="mailMessageBody">Email template body</param>
        /// <param name="customSmtpTestSettings">The configured Custom SMTP Settingss</param>
        /// <returns>Email body as a string ready to be sent</returns>
        string ReplaceTemplatePlaceHolders(string mailMessageBody, CustomSmtpTestModel customSmtpTestSettings);
    }
}
