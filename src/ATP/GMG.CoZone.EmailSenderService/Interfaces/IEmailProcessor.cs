﻿using GMG.CoZone.EmailSenderService.EmailObjects;
using GMGColorDAL;
using System.Collections.Generic;

namespace GMG.CoZone.EmailSenderService.Interfaces
{
    /// <summary>
    /// Interface used to process database email before sending
    /// </summary>
    public interface IEmailProcessor
    {
        /// <summary>
        /// Get and process a default of 20 emails from the database for sending
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="numberOfEmailsToRetrieve">The number of emails to get</param>
        /// <returns>Dictionary of emails grouped by account id</returns>
        Dictionary<int?, List<EmailWithFiles>> GetProcessedEmails(GMGColorContext context, int numberOfEmailsToRetrieve = 20);

        /// <summary>
        /// Clear email processing key
        /// </summary>
        /// <param name="context">Database context</param>
        void ClearProcessingKey(GMGColorContext context);
    }
}
