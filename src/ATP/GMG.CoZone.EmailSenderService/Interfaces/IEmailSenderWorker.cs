﻿namespace GMG.CoZone.EmailSenderService.Interfaces
{
    public interface IEmailSenderWorker
    {
        void SendEmails();
    }
}
