﻿using GMG.CoZone.EmailSenderService.EmailObjects;
using System.Collections.Generic;

namespace GMG.CoZone.EmailSenderService.Interfaces
{
    public interface IEmailRetriever
    {
        Dictionary<int?, List<EmailWithFiles>> GetEmailsToSend();
    }
}
