﻿using GMG.CoZone.EmailSenderService.EmailObjects;
using System.Collections.Generic;

namespace GMG.CoZone.EmailSenderService.Interfaces
{
    /// <summary>
    /// Interface that sends the CoZone emails
    /// </summary>
    public interface ISmtpEmailSender
    {
        /// <summary>
        /// Sends list of mails using default CoZone configuration
        /// </summary>
        /// <param name="mailMessagesToSend">Mail list to send</param>
        /// <param name="region">Application Region AWS or Local</param>
        /// <param name="groupAccount">Mail list group account id</param>
        void SendEmails(List<EmailWithFiles> mailMessagesToSend, string region, int groupAccount);
    }
}
