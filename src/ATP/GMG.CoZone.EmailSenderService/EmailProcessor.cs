﻿using System;
using System.Collections.Generic;
using GMG.CoZone.EmailSenderService.EmailObjects;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMGColorDAL;
using System.Linq;
using System.Net.Mail;
using GMG.CoZone.Common.Interfaces;
using System.IO;

namespace GMG.CoZone.EmailSenderService
{
    public class EmailProcessor : IEmailProcessor
    {
        /// <summary>
        /// Email processing key
        /// </summary>
        private string processingInProgressKey = Guid.NewGuid().ToString();
        private string _region;

        private IFileManager _fileManager;
        private IGMGConfiguration _gmgConfiguration;

        public EmailProcessor(IFileManager fileManager, IGMGConfiguration gmgConfiguration)
        {
            _fileManager = fileManager;
            _gmgConfiguration = gmgConfiguration;
            _region = string.IsNullOrEmpty(_region) ? (!_gmgConfiguration.IsEnabledS3Bucket) ? string.Empty : _gmgConfiguration.AWSRegion : _region;
        }

        public Dictionary<int?, List<EmailWithFiles>> GetProcessedEmails(GMGColorContext context, int numberOfEmailsToRetrieve = 20)
        {
            //TODO Create a config setting for number of trycount
            var emails = context.Emails.Where(em => em.ProcessingInProgress == null && (em.TryCount < 2 || em.TryCount == null))
                                       .OrderBy(em => em.TryCount)
                                       .Select(em => em)
                                       .Take(numberOfEmailsToRetrieve)
                                       .ToList();
            emails.ForEach(em => { if (em.Account == null) { em.Account = 0; }; });

            return ProcessDbEmails(context, MarkEmailForProcessing(context, emails, numberOfEmailsToRetrieve).GroupBy(em => em.Account).ToDictionary(gr => gr.Key, gr => gr.ToList()));
        }

        /// <summary>
        /// Clear emails processing key
        /// </summary>
        /// <param name="context"></param>
        public void ClearProcessingKey(GMGColorContext context)
        {
            try
            {
                List<Email> emails = (from e in context.Emails where e.ProcessingInProgress == processingInProgressKey select e).ToList();
                foreach (Email email in emails)
                {
                    email.ProcessingInProgress = null;
                }
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Mark emails for processing
        /// </summary>
        /// <param name="context"></param>
        /// <param name="emailsToMark"></param>
        /// <param name="numberOfEmails"></param>
        /// <returns></returns>
        private List<Email> MarkEmailForProcessing(GMGColorContext context, List<Email> emailsToMark, int numberOfEmails = 20)
        {
            foreach (Email email in emailsToMark)
            {
                if (email.ProcessingInProgress == null)
                {
                    email.ProcessingInProgress = processingInProgressKey;
                }
            }
            context.SaveChanges();

            return (from e in context.Emails where e.ProcessingInProgress == processingInProgressKey select e).Take(numberOfEmails).ToList();
        }

        /// <summary>
        /// Go trough the list of emails and add attachements if there are any
        /// </summary>
        /// <param name="emailsToSend">Email dictionary with email grouped by Account ID</param>
        private Dictionary<int?, List<EmailWithFiles>> ProcessDbEmails(GMGColorContext context, Dictionary<int?, List<Email>> emailsToSend)
        {
            var groupedMailMessages = new Dictionary<int?, List<EmailWithFiles>>();

            foreach (var emailGroup in emailsToSend)
            {
                var emailsAccount = emailGroup.Key;
                var mailMessages = new List<EmailWithFiles>();

                foreach (var email in emailGroup.Value)
                {
                    try
                    {
                        var attachements = CreateEmailAttachements(email);
                        mailMessages.Add(BuildEmailMessageToSent(email, attachements));
                    }
                    catch (Exception ex)
                    {
                        email.LastErrorMessage = ex.Message;
                        email.TryCount = email.TryCount == null ? 1 : email.TryCount + 1;
                        email.ProcessingInProgress = null;
                        context.SaveChanges();
                    }
                }
                groupedMailMessages.Add(emailsAccount, mailMessages);
            }

            return groupedMailMessages;
        }

        /// <summary>
        /// Add email attachements if there are any
        /// </summary>
        /// <param name="email">Email object to update</param>
        private EmailAttachement[] CreateEmailAttachements(Email email)
        {
            string[] attachements = null;

            //Check if email has attachements
            if (!String.IsNullOrEmpty(email.Attachments))
            {
                attachements = email.Attachments.Split(';');
            }

            //Get email attachments streams using relative path
            var attachmentsFiles = attachements != null
                ? (from e in attachements
                   select
                       new EmailAttachement
                       {
                           Stream = _fileManager.FileExists(e, _region)
                               ? _fileManager.GetMemoryStreamOfAFile(e, _region)
                               : null,
                           FileName = Path.GetFileName(e)
                       }).ToArray()
                : null;

            // throw an exception if at least one file stream is null
            if (attachmentsFiles != null)
            {
                if (attachmentsFiles.Any(o => o.Stream == null))
                {
                    //cleanup memory usage
                    foreach (var attachment in attachmentsFiles.Where(o => o.Stream != null))
                    {
                        attachment.Stream.Close();
                    }
                    throw new Exception("One or more attachments not exists on bucket for email with ID " + email.ID);
                }
            }
            return attachmentsFiles;
        }

        /// <summary>
        /// Create the email message that will be sent
        /// </summary>
        /// <param name="email">Database email to convert</param>
        /// <returns></returns>
        private EmailWithFiles BuildEmailMessageToSent(Email email, EmailAttachement[] attachmentsFiles)
        {
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(email.fromEmail, (email.fromName ?? string.Empty).Replace(",", " "));
            mailMessage.To.Add(new MailAddress(email.toEmail, (email.toName ?? string.Empty).Replace(",", " ")));
            mailMessage.BodyEncoding =
            mailMessage.SubjectEncoding =
            mailMessage.HeadersEncoding = System.Text.Encoding.UTF8;

            if (!string.IsNullOrEmpty(email.toCC))
            {
                mailMessage.CC.Add(new MailAddress(email.toCC));
            }

            mailMessage.Subject = email.subject;
            mailMessage.Body = email.bodyText;

            if (!string.IsNullOrEmpty(email.bodyHtml))
            {
                mailMessage.Body = email.bodyHtml;
                mailMessage.IsBodyHtml = true;
            }

            if (attachmentsFiles != null)
            {
                foreach (var attachment in attachmentsFiles)
                {
                    if (!string.IsNullOrEmpty(attachment.FileName) && attachment.Stream != null)
                    {
                        mailMessage.Attachments.Add(new Attachment(attachment.Stream,
                            attachment.FileName));
                    }
                }
            }
            return new EmailWithFiles()
            {
                Email = mailMessage,
                AttachedFiles = attachmentsFiles,
                DbEmailId = email.ID,
            };
        }
    }
}
