﻿using System;
using GMG.CoZone.EmailSenderService.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using GMG.CoZone.Common.Interfaces;

namespace GMG.CoZone.EmailSenderService
{
    public class EmailSender : IEmailSender
    {
        private readonly IGMGConfiguration _gmgConfiguration;
        private readonly IEmailSenderWorker _emailSenderWorker;
        private readonly ITestConnectionWorker _testConectionWorker;
        private readonly IServiceLogger _serviceLogger;

        private CancellationTokenSource _testConnectionWorkerTaskCTS = new CancellationTokenSource();
        private CancellationTokenSource _emailSenderWorkerTasksCTS = new CancellationTokenSource();
        private CancellationTokenSource _workersTaskCTS = new CancellationTokenSource();

        public EmailSender(IGMGConfiguration gmgConfiguration, 
                           IEmailSenderWorker emailSenderWorker, 
                           ITestConnectionWorker testConnectionWorker,
                           IServiceLogger serviceLogger)
        {
            _gmgConfiguration = gmgConfiguration;
            _emailSenderWorker = emailSenderWorker;
            _testConectionWorker = testConnectionWorker;
            _serviceLogger = serviceLogger;
        }

        public void Start()
        {
            try
            {
                Task.Factory.StartNew(() => {
                    StartWorkers();
                }, _workersTaskCTS.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("Email Sender failed with the following error {0}", ex.Message), ex);
            }
        }

        private void StartWorkers()
        {
            try
            {                
                while (!_workersTaskCTS.Token.IsCancellationRequested)
                {
                    _emailSenderWorker.SendEmails();
                    _testConectionWorker.TestConnections();

                    Thread.Sleep(10 * 1000); // wait 2 seconds between syncs
                }
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("Start Workers failed with the following error {0}", ex.Message), ex);
            }
        }

        public void Stop()
        {
            _workersTaskCTS.Cancel();
        }
    }
}
