﻿using System;
using System.Collections.Generic;
using GMG.CoZone.EmailSenderService.EmailObjects;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMGColorDAL;
using GMG.CoZone.Common.Interfaces;

namespace GMG.CoZone.EmailSenderService
{
    public class EmailRetriever : IEmailRetriever
    {
        private IEmailProcessor _emailProcessor;
        private IGMGConfiguration _gmgConfiguration;
        private IServiceLogger _serviceLogger;

        public EmailRetriever(IEmailProcessor emailProcessor, IGMGConfiguration gmgConfiguration, IServiceLogger servicelogger)
        {
            _emailProcessor = emailProcessor;
            _gmgConfiguration = gmgConfiguration;
            _serviceLogger = servicelogger;
        }

        public Dictionary<int?, List<EmailWithFiles>> GetEmailsToSend()
        {
            var emailsToSend = new Dictionary<int?, List<EmailWithFiles>>();

            using (var context = new GMGColorContext())
            {
                try
                {
                    emailsToSend = _emailProcessor.GetProcessedEmails(context, _gmgConfiguration.NumberOfEmailsToSend);
                }
                catch (Exception ex)
                {
                    _serviceLogger.Log(string.Format("GetEmailsToSend failed with the following error: {0}", ex.Message), ex);
                }
                finally
                {
                    _emailProcessor.ClearProcessingKey(context);
                }
            }

            return emailsToSend;
        }
    }
}
