﻿using System;
using System.Net.Mail;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMGColor.Resources;
using System.IO;
using System.Reflection;

namespace GMG.CoZone.EmailSenderService
{
    public class SmtpTestConnectionEmailBuilder : ISmtpTestConnectionEmailBuilder
    {
        private const string TEMPLATE_FOLDER_LOCATION = "\\EmailTemplates\\ConfirmSmtpSettingsEmail.html";

        public MailMessage BuildMessage(CustomSmtpTestModel customSmtpTestSettings)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add(customSmtpTestSettings.RecipientEmail);
            mailMessage.Subject = Resources.lblCustomSmtpNotificationWasSent;
            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(customSmtpTestSettings.SenderEmail);

            mailMessage.Body = GetEmailTemplate();
            mailMessage.Body = ReplaceTemplatePlaceHolders(mailMessage.Body, customSmtpTestSettings);

            return mailMessage;
        }

        public string GetEmailTemplate()
        {
            var workingDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            if (workingDir.StartsWith("file:\\"))
            {
                workingDir = workingDir.Replace("file:\\", String.Empty);
                //workingDir = workingDir.Substring(0, workingDir.LastIndexOf("\\"));
                //workingDir = workingDir.Substring(0, workingDir.LastIndexOf("\\"));
            }
            var fullPath = workingDir + TEMPLATE_FOLDER_LOCATION;
            return File.ReadAllText(fullPath);
        }

        public string ReplaceTemplatePlaceHolders(string mailMessageBody, CustomSmtpTestModel customSmtpTestSettings)
        {
            var baseurl = customSmtpTestSettings.EmailAccountData.IsSecureConnection ? "http://" + customSmtpTestSettings.EmailAccountData.Domain + "/PersonalSettings/Notifications" :
                                                               "http://" + customSmtpTestSettings.EmailAccountData.Domain + "/PersonalSettings/Notifications";
            mailMessageBody = mailMessageBody.Replace("<$customsmtp-account_thumbnail>", customSmtpTestSettings.EmailAccountData.EmailLogoPath);
            mailMessageBody = mailMessageBody.Replace("<$customSmtpTitle>", Resources.lblCustomSmtpNotificationWasSent);
            mailMessageBody = mailMessageBody.Replace("<$default_avatar_thumbnail$>", customSmtpTestSettings.EmailAccountData.UserLogoPath);
            mailMessageBody = mailMessageBody.Replace("<$customsmtp-account-url>", customSmtpTestSettings.EmailAccountData.Domain);
            mailMessageBody = mailMessageBody.Replace("<$customsmtp-account-base-url>", "");
            mailMessageBody = mailMessageBody.Replace("<$pharagraphCommonFooter>", Resources.pharagraphCommonFooter1);
            mailMessageBody = mailMessageBody.Replace("<$pharagraphManageYourEmailNotification>", Resources.pharagraphManageYourEmailNotification);
            mailMessageBody = mailMessageBody.Replace("{0}", customSmtpTestSettings.EmailAccountData.AccountName);
            mailMessageBody = mailMessageBody.Replace("<$lblSendTo>", Resources.lblSendTo);
            mailMessageBody = mailMessageBody.Replace("<$manager-notification>", baseurl);
            mailMessageBody = mailMessageBody.Replace("<$smtpEmailMessage>", Resources.lblCustomSmtpTestMessage);

            mailMessageBody = mailMessageBody.Replace("{0}", customSmtpTestSettings.RecipientEmail);
            mailMessageBody = mailMessageBody.Replace("’", "&#39;");

            return mailMessageBody;
        }
    }
}
