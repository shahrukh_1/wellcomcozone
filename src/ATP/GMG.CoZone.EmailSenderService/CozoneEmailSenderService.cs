﻿using GMG.CoZone.EmailSenderService.Interfaces;
using Microsoft.Practices.Unity;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace GMG.CoZone.EmailSenderService
{
    public partial class CozoneEmailSenderService : ServiceBase, ICoZoneEmailSenderService
    {
        private static IUnityContainer _container;
        private IEmailSender _emailSender;
        private Task _emailSenderTask;

        public CozoneEmailSenderService(IUnityContainer container, IEmailSender emailSender)
        {
            _container = container;
            _emailSender = emailSender;

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _emailSenderTask = new Task(_emailSender.Start);
            _emailSenderTask.Start();
        }

        protected override void OnStop()
        {
            _emailSender.Stop();
        }
    }
}
