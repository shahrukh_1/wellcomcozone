﻿using GMG.CoZone.Common.AWS;
using GMG.CoZone.Common.AWS.Interfaces;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Module;
using GMG.CoZone.Common.Module.IO;
using GMG.CoZone.Common.Module.Loggers;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMG.CoZone.EmailSenderService.SmtpSenders;
using GMG.CoZone.EmailSenderService.Workers;
using Microsoft.Practices.Unity;

namespace GMG.CoZone.EmailSenderService
{
    public class UnityConfig
    {
        private const string DEFAULT_SMTP_MAPING_NAME = "defaultEmailSender";
        private const string CUSTOM_SMTP_MAPING_NAME = "customEmailSender";

        private static IUnityContainer _container;

        public static IUnityContainer GetContainerInstance()
        {
            _container = new UnityContainer();
            RegisterComponents();
            return _container;
        }

        private static void RegisterComponents()
        {
            _container.RegisterType<ICoZoneEmailSenderService, CozoneEmailSenderService>();
            _container.RegisterType<IEmailSender, EmailSender>();
            _container.RegisterType<IEmailSenderWorker, EmailSenderWorker>();
            _container.RegisterType<ITestConnectionWorker, TestConnectionWorker>();
            _container.RegisterType<IServiceLogger, ServiceLogger>();
            _container.RegisterType<IGMGConfiguration, GMGConfiguration>(new HierarchicalLifetimeManager());
            _container.RegisterType<ISmtpEmailSender, DefaultSmtpEmailSender>(DEFAULT_SMTP_MAPING_NAME);    
            _container.RegisterType<ISmtpEmailSender, CustomSmtpEmailSender>(CUSTOM_SMTP_MAPING_NAME);
            _container.RegisterType<IEmailRetriever, EmailRetriever>();
            _container.RegisterType<IFileManager, FileManager>();
            _container.RegisterType<IAWSSimpleStorageService, AWSSimpleStorageService>();
            _container.RegisterType<IEmailProcessor, EmailProcessor>();
            _container.RegisterType<ISmtpTestConnectionEmailSender, SmtpTestConnectionEmailSender>();
            _container.RegisterType<ISmtpTestConnectionEmailBuilder, SmtpTestConnectionEmailBuilder>();
            _container.RegisterType<IQueueMessageManager, QueueMessageManager>();

        }
    }
}
