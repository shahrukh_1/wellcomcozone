﻿using System;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.EmailSenderService.Interfaces;
using System.Net.Mail;
using System.Net;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;

namespace GMG.CoZone.EmailSenderService.SmtpSenders
{
    public class SmtpTestConnectionEmailSender : ISmtpTestConnectionEmailSender
    {
        private readonly ISmtpTestConnectionEmailBuilder _smtpEmailBuilder;
        private readonly IServiceLogger _serviceLogger;

        public SmtpTestConnectionEmailSender(ISmtpTestConnectionEmailBuilder smtpEmailBuilder, IServiceLogger serviceLogger)
        {
            _smtpEmailBuilder = smtpEmailBuilder;
            _serviceLogger = serviceLogger;
        }

        public string SendEmails(CustomSmtpTestConnectionModel emailData)
        {
            var connectionError = string.Empty;
            var testConnectionEmail =  _smtpEmailBuilder.BuildMessage(emailData.TestData);

            using (var smtpClient = new SmtpClient
            {
                Host = emailData.TestData.Host,
                Port = emailData.TestData.Port,
                EnableSsl = emailData.TestData.EnabledSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailData.TestData.SenderEmail, emailData.TestData.Password)
            })
            {
                try
                {
                    smtpClient.Send(testConnectionEmail);
                    string message = String.Format("Test Email sent to user {0} with subject {1} for account {2}", testConnectionEmail.To, testConnectionEmail.Subject, emailData.AccountId);
                    _serviceLogger.Log(new LoggingNotification(message, Severity.Info));
                }
                catch (Exception ex)
                {
                    connectionError = ex.Message;
                    _serviceLogger.Log(string.Format("Send Test Email failed with the following error: {0} for account {1}", ex.Message, emailData.AccountId), ex);
                }
            }
            return connectionError;
        }
    }
}
