﻿using System;
using System.Collections.Generic;
using GMG.CoZone.EmailSenderService.EmailObjects;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMG.CoZone.Common.Interfaces;
using System.Net.Mail;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;

namespace GMG.CoZone.EmailSenderService.SmtpSenders
{
    /// <summary>
    /// Class used to send email with CoZone default AWS configuration
    /// </summary>
    class DefaultSmtpEmailSender : BaseSmtpEmailSender, ISmtpEmailSender
    {
        private IGMGConfiguration _gmgConfiguration;
        private IServiceLogger _serviceLogger;

        public DefaultSmtpEmailSender(IFileManager fileManager, IGMGConfiguration gmgConfiguration, IServiceLogger serviceLogger)
            :base(fileManager, serviceLogger)
        {
            _gmgConfiguration = gmgConfiguration;
            _serviceLogger = serviceLogger;
        }

        /// <summary>
        /// Sends list of mails using default CoZone configuration
        /// </summary>
        /// <param name="mailMessagesToSend">Mail list to send</param>
        /// <param name="region">Application Region AWS or Local</param>
        /// <param name="groupAccount">Mail list group account id</param>
        public void SendEmails(List<EmailWithFiles> mailMessagesToSend, string region, int groupAccount)
        {
            using (var smtpClient = new SmtpClient((_gmgConfiguration.MailServerType.ToUpper() == "AWS")
                                                    ? _gmgConfiguration.AWSMailServer
                                                    : _gmgConfiguration.MailServer, Convert.ToInt32(_gmgConfiguration.MailServerPort)))
            {
                foreach (var mailMessage in mailMessagesToSend)
                {
                    var emailHasFailed = false;
                    var emailError = string.Empty;

                    try
                    {
                        SendEmail(smtpClient, mailMessage);
                    }
                    catch (Exception ex)
                    {
                        var emailRecipient = mailMessage.Email.To.ToString();
                        emailError = ex.Message;
                        LogError(out emailHasFailed, emailError, emailRecipient, ex);
                    }
                    finally
                    {
                        CleanUpData(emailHasFailed, mailMessage, emailError, region);
                    }
                }
            }
        }

        /// <summary>
        /// Send email using custom smtp
        /// </summary>
        /// <param name="smtpClient">Smtp client</param>
        /// <param name="mailMessage">Mail message</param>
        private void SendEmail(SmtpClient smtpClient, EmailWithFiles mailMessage)
        {
            if (_gmgConfiguration.MailServerType.ToUpper() == "AWS")
            {
                smtpClient.Credentials = new System.Net.NetworkCredential("AKIAR6KWPSILWN7BABFV",
                                                                          "BHiVfLvSl26oyqchR6fOdCkfq9MjjyHyvd93mYfQQCaj");
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            }
            smtpClient.Send(mailMessage.Email);
            LogEmailSentInfo(mailMessage.Email);
        }
    }
}
