﻿using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Module.Exceptions;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.EmailSenderService.EmailObjects;
using GMGColorDAL;
using System;
using System.Linq;
using System.Net.Mail;

namespace GMG.CoZone.EmailSenderService.SmtpSenders
{
    /// <summary>
    /// Abstract class that perfomes cleanup for successfull or failed emails
    /// </summary>
    public abstract class BaseSmtpEmailSender
    {
        private IFileManager _fileManager;
        private IServiceLogger _serviceLogger;

        public BaseSmtpEmailSender(IFileManager fileManager, IServiceLogger serviceLogger)
        {
            _fileManager = fileManager;
            _serviceLogger = serviceLogger;
        }

        /// <summary>
        /// Clean up email info
        /// </summary>
        /// <param name="emailHasFailed">Flag for email fail</param>
        /// <param name="mailMessage">Mail message</param>
        /// <param name="emailError">Mail message error</param>
        /// <param name="region">Region</param>
        protected void CleanUpData(bool emailHasFailed, EmailWithFiles mailMessage, string emailError, string region)
        {
            try
            {
                if (emailHasFailed)
                {
                    MarkEmailFailed(mailMessage.DbEmailId, emailError);
                }
                else
                {
                    RemoveSuccessEmail(mailMessage.DbEmailId);
                }
                CleanUpEmailData(mailMessage, region);
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("CleanUpData failed for email {0}, with the following error {1}", mailMessage.Email, ex.Message), ex);
            }
        }

        /// <summary>
        /// Log error to database and update email error fields
        /// </summary>
        /// <param name="emailHasFailed">Email failed flag</param>
        /// <param name="emailError">Email error message</param>
        /// <param name="ex">Faile exception</param>
        protected void LogError(out bool emailHasFailed, string emailError, string emailRecipient, Exception ex)
        {
            string message = String.Format("Error occurred while sending the email ... {0}", emailRecipient);
            emailHasFailed = true;
            emailError = message;
            _serviceLogger.Log(message, new LoggingException(message, ex, Severity.Error));
        }

        /// <summary>
        /// Log info to database
        /// </summary>
        /// <param name="emailError">Email info message</param>
        protected void LogEmailSentInfo(MailMessage email)
        {
            string message = String.Format("EmailSender: Sending the email to user {0} with subject {1}", email.To, email.Subject);
            _serviceLogger.Log(new LoggingNotification(message, Severity.Info));
        }

        /// <summary>
        /// Free attachements, clean-up memory and temp-email folder
        /// </summary>
        /// <param name="mailMessage">Email to cleanup</param>
        /// <param name="region">Region local or AWS</param>
        private void CleanUpEmailData(EmailWithFiles mailMessage, string region)
        {
            try
            {
                //free attachements
                foreach (var attachements in mailMessage.Email.Attachments)
                {
                    attachements.Dispose();
                }

                //cleanup memory usage
                if (mailMessage.AttachedFiles != null)
                {
                    foreach (var attachment in mailMessage.AttachedFiles.Where(o => o.Stream != null))
                    {
                        attachment.Stream.Close();
                    }
                }

                // cleanup temp-email folder
                if (mailMessage.AttachedFiles != null)
                {
                    foreach (var file in mailMessage.AttachedFiles)
                    {
                        _fileManager.DeleteFileWithBackup(file.FileName, region);
                    }
                }
                mailMessage.Email.Dispose();
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("CleanUpEmailData failed for email {0}, with the following message {1}", mailMessage.Email, ex.Message), ex);
            }
        }

        /// <summary>
        /// Update email db info if send has failed
        /// </summary>
        /// <param name="emailId">The email entity id</param>
        /// <param name="emailError">The error with which it has failed</param>
        private void MarkEmailFailed(int emailId, string emailError)
        {
            try
            {
                using (var context = new GMGColorContext())
                {
                    var failedEmail = context.Emails.Find(emailId);
                    if (failedEmail != null)
                    {
                        failedEmail.ProcessingInProgress = null;
                        failedEmail.TryCount = failedEmail.TryCount == null ? 1 : failedEmail.TryCount + 1;
                        failedEmail.LastErrorMessage = emailError;

                        context.Entry(failedEmail).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("MarkEmailFailed failed for email with Id {0}, with the following error {1}", emailId, ex.Message), ex);
            }
        }

        /// <summary>
        /// Remove email from database if send successfuly
        /// </summary>
        /// <param name="emailId">The email entity id</param>
        private void RemoveSuccessEmail(int emailId)
        {
            try
            {
                using (var context = new GMGColorContext())
                {
                    var emailItem = new Email()
                    {
                        ID = emailId
                    };
                    context.Emails.Attach(emailItem);
                    context.Emails.Remove(emailItem);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _serviceLogger.Log(string.Format("RemoveSuccessEmail failed for email with id {0}, with the following message {1}", emailId, ex.Message), ex);
            }
        }
    }
}
