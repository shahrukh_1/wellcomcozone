﻿using System;
using System.Collections.Generic;
using GMG.CoZone.EmailSenderService.EmailObjects;
using GMG.CoZone.EmailSenderService.Interfaces;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.DomainModels;
using GMGColorDAL;
using GMG.CoZone.Common.Module.Enums;
using System.Linq;
using GMG.CoZone.Common.Module.ExtensionMethods;
using GMG.CoZone.Common;
using System.Net.Mail;
using System.Net;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;

namespace GMG.CoZone.EmailSenderService.SmtpSenders
{
    /// <summary>
    /// Class that send emails through the Accounts custom smtp server
    /// </summary>
    class CustomSmtpEmailSender : BaseSmtpEmailSender, ISmtpEmailSender
    {
        public CustomSmtpEmailSender(IFileManager fileManager, IServiceLogger serviceLogger)
            :base(fileManager, serviceLogger)
        {
        }
        
        /// <summary>
        /// Send emails using account custom smtp
        /// </summary>
        /// <param name="mailMessagesToSend">Mail list to send</param>
        /// <param name="region">Application Region AWS or Local</param>
        /// <param name="groupAccount">Mail list group account id</param>
        public void SendEmails(List<EmailWithFiles> mailMessagesToSend, string region, int groupAccount)
        {
            var accountSmtpSettings = GetAccountSmtpSettings(groupAccount);

            using (var customSmptClient = new SmtpClient
            {
                Host = accountSmtpSettings.ServerName,
                Port = accountSmtpSettings.Port.Value,
                EnableSsl = accountSmtpSettings.EnableSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(accountSmtpSettings.EmailAddress, accountSmtpSettings.Password)
            })
            {
                foreach (var mailMessage in mailMessagesToSend)
                {
                    var emailHasFailed = false;
                    var emailError = string.Empty;

                    try
                    {
                        customSmptClient.Send(mailMessage.Email);
                        LogEmailSentInfo(mailMessage.Email);
                    }
                    catch (Exception ex)
                    {
                        var emailRecipient = mailMessage.Email.To.ToString();
                        emailError = ex.Message;
                        LogError(out emailHasFailed, emailError, emailRecipient, ex);
                    }
                    finally
                    {
                        CleanUpData(emailHasFailed, mailMessage, emailError, region);
                    }
                }
            }
        }

        /// <summary>
        /// Get Account Custom Smpt settings
        /// </summary>
        /// <param name="accoutId">The accounts id</param>
        /// <returns></returns>
        private CustomSMTPServer GetAccountSmtpSettings(int accoutId)
        {
            var accountSettingsString = string.Empty;
            var sharedSecret = string.Empty;

            using (var context = new GMGColorContext())
            {
                var accountSettingsKey = Enum.GetName(typeof(SettingsKeyEnum), SettingsKeyEnum.CustomSmtpServer);
                var accountSettings = context.AccountSettings.Where(acs => acs.Account == accoutId && acs.Name == accountSettingsKey).FirstOrDefault();

                if (accountSettings != null)
                {
                    accountSettingsString = accountSettings.Value;
                    sharedSecret = accountSettings.Account1.Guid;
                }
            }

            var settingsObj = accountSettingsString.ToObject<CustomSMTPServer>();
            settingsObj.Password = Utils.Decrypt(settingsObj.Password, sharedSecret);

            return settingsObj;
        }
    }
}
