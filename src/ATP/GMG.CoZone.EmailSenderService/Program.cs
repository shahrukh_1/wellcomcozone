﻿using GMG.CoZone.EmailSenderService.Interfaces;
using System;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Common.Interfaces;

namespace GMG.CoZone.EmailSenderService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            var unityContainer = UnityConfig.GetContainerInstance();
            var logger = unityContainer.Resolve<IServiceLogger>();

            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine("CoZone Email Sender Service Started !\nPress Any key to exit");

                var emailSender = unityContainer.Resolve<IEmailSender>();
                try
                {
                    logger.Log(new LoggingNotification("Emails Sender service started", Severity.Info));
                    var emailSenderTask = new Task(emailSender.Start);
                    emailSenderTask.Start();
                }
                catch (Exception ex)
                {
                    logger.Log("Email Sender Service fatal exception: ", new LoggingException("Start SendEmailWorker instance failed", ex, Severity.Fatal));
                }

                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }

                emailSender.Stop();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    unityContainer.Resolve<CozoneEmailSenderService>()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
