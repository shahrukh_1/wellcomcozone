﻿using System.Data;
using System.Data.SqlClient;
using System;
using System.IO;
using GMG.CoZone.FTPCustomProvider;

namespace GMG.CoZone.FTP
{
    public class DataAccess
    {
        #region Private Members
        #endregion

        #region Public Methods

        /// <summary>
        /// Authenticate user and authorize it for upload jobs to CoZone
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool AuthenticateUser(string username, string password)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Settings.CONNECTION_STRING_COZONE))
                {
                    using (SqlCommand command = new SqlCommand("SPC_ReturnFTPUserLogin", connection))
                    {
                        SqlDataAdapter adapt = new SqlDataAdapter(command);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@P_Username", SqlDbType.VarChar).Value = username;
                        command.Parameters.Add("@P_Password", SqlDbType.VarChar).Value = password;

                        DataTable dt = new DataTable();
                        adapt.Fill(dt);
                        
                        if (dt.Rows.Count > 0)
                        {
                            LogMessage(LogLevel.Info, string.Format("The user <{0}> successfully logged in", username));
                            return true;
                        }

                        LogMessage(LogLevel.Warning, string.Format("Login for <{0}> user failed", username));
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage(LogLevel.Error, ex.Message, ex.StackTrace);
            }
            return false;
        }

        public static string GetUserFolderName(string username)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Settings.CONNECTION_STRING_COZONE))
                {
                    string commandStr = @"SELECT    DISTINCT
                                                    u.GUID
                                        FROM        [dbo].[User] u
                                                    JOIN [dbo].[UserStatus] us ON u.[Status] = us.ID
                                                    JOIN [dbo].[Account] ac ON u.[Account] = ac.[ID]
                                                    JOIN [dbo].[AccountStatus] acs ON ac.[Status] = acs.[ID]
                                        WHERE       u.[Username] = @username AND
                                                    us.[Key] = 'A' AND
                                                    acs.[Key] = 'A'";

                    using (SqlCommand command = new SqlCommand(commandStr, connection))
                    {
                        command.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
                        connection.Open();
                        return (string)command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage(LogLevel.Error, ex.Message, ex.StackTrace);
            }
            return null;
        }

        #region Logging

        /// <summary>
        /// Log message to Database
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void LogMessage(LogLevel level, string message, string exception = "")
        {
            // check the log level first
            if(Settings.LOG_LEVEL > (int)level)
                return;

            try
            {
                using (SqlConnection connection = new SqlConnection(Settings.CONNECTION_STRING_ELMAH))
                {
                    string commandStr = @" INSERT INTO [ELMAH].[dbo].[FTPIISCustomProviderLog]
                                               ([Date]
                                               ,[Level]
                                               ,[Message]
                                               ,[Exception])
                                         VALUES (GetDate(), @Level, @Message, @Exception)";

                    using (SqlCommand command = new SqlCommand(commandStr, connection))
                    {
                        command.Parameters.AddWithValue("@Level", Enum.GetName(level.GetType(), level));
                        command.Parameters.AddWithValue("@Message", message);
                        command.Parameters.AddWithValue("@Exception", exception);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                string logFileDir = @"C:\CoZoneFTPLog";
                string logFileName = "GMGCoZoneFTP.log";

                if (!Directory.Exists(logFileDir))
                {
                    Directory.CreateDirectory(logFileDir);
                }

                using (StreamWriter w = File.AppendText(Path.Combine(logFileDir, logFileName)))
                {
                    w.WriteLine("{0}: Log failed with message: {1}; Stack Trace: {2}", DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString(),
                        ex.Message, ex.StackTrace);
                }
            }
        }

        #endregion

        #endregion

    }
}
