﻿using System;
using GMG.CoZone.FTPCustomProvider;
using Microsoft.Web.FtpServer;
using System.IO;

namespace GMG.CoZone.FTP
{
    public class CutomProvider : BaseProvider, IFtpAuthenticationProvider, IFtpRoleProvider, IFtpHomeDirectoryProvider, IFtpLogProvider
    {
        bool IFtpAuthenticationProvider.AuthenticateUser(string sessionId, string siteName, string userName, string userPassword, out string canonicalUserName)
        {
            canonicalUserName = userName;
            return DataAccess.AuthenticateUser(userName, userPassword);
        }

        bool IFtpRoleProvider.IsUserInRole(string sessionId, string siteName, string userName, string userRole)
        {
            return false;
        }

        string IFtpHomeDirectoryProvider.GetUserHomeDirectoryData(string sessionId, string siteName, string userName)
        {
            string userSubDir = DataAccess.GetUserFolderName(userName);
            string userFullDirPath = Path.Combine(Settings.FTP_ROOT_DIR, userSubDir);
            
            if (!Directory.Exists(userFullDirPath))
            {
                Directory.CreateDirectory(userFullDirPath);
            }
            return userFullDirPath;
        }

        void IFtpLogProvider.Log(FtpLogEntry loggingParameters)
        {
            string eventLogStr =
                String.Format(
                    "{0} command received from <{1}> user (Command parameters: {2}; Remote IP: {3}; Session ID: {4})",
                    loggingParameters.Command, loggingParameters.UserName, loggingParameters.CommandParameters,
                    loggingParameters.RemoteIPAddress, loggingParameters.SessionId);
            
            DataAccess.LogMessage(LogLevel.FTPCommandInfo, eventLogStr);
        }
    }
}
