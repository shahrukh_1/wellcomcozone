﻿namespace GMG.CoZone.FTPCustomProvider
{
    class Settings
    {
#if (DEBUG)
        public const string CONNECTION_STRING_COZONE = "Data Source=(local); Initial Catalog=GMGCoZone;Integrated Security=SSPI";
        public const string CONNECTION_STRING_ELMAH = "Data Source=(local); Initial Catalog=ELMAH;Integrated Security=SSPI";
        public const string FTP_ROOT_DIR = @"C:\FTPRoot";
        public const int LOG_LEVEL = 0;
#elif (STAGE_EU)
        public const string CONNECTION_STRING_COZONE = "server=stage-db.staging.wellcomapproval.com;user id=gmgcozonedb; password=CoZone2W3llc0m;initial catalog=GMGCoZone";
        public const string CONNECTION_STRING_ELMAH = "server=stage-db.staging.wellcomapproval.com;user id=gmgcozonedb; password=CoZone2W3llc0m;initial catalog=ELMAH";
        public const string FTP_ROOT_DIR = @"C:\FTPRoot\";
        public const int LOG_LEVEL = 2;
#elif (TEST_EU)
        public const string CONNECTION_STRING_COZONE = "server=production-db.staging.wellcomapproval.com;user id=gmgcozonedb; password=CoZone2W3llc0m;initial catalog=GMGCoZone";
        public const string CONNECTION_STRING_ELMAH = "server=production-db.staging.wellcomapproval.com;user id=gmgcozonedb; password=CoZone2W3llc0m;initial catalog=ELMAH";
        public const string FTP_ROOT_DIR = @"C:\FTPRoot\";
        public const int LOG_LEVEL = 0;
#elif (PRODUCTION)
        public const string CONNECTION_STRING_COZONE = "server=production-db.wellcomapproval.com;user id=gmgcozonedb; password=CoZone2W3llc0m;initial catalog=GMGCoZone";
        public const string CONNECTION_STRING_ELMAH = "server=production-db.wellcomapproval.com;user id=gmgcozonedb; password=CoZone2W3llc0m;initial catalog=ELMAH";
        public const string FTP_ROOT_DIR = @"D:\FTPRoot\";
        public const int LOG_LEVEL = 2;
#elif (LOCAL)
        public const string CONNECTION_STRING_COZONE = "server=TOFILLLOCALIP, 1433;user id=TOFILLLOCALUSER; password=TOFILLLOCALPASS;initial catalog=GMGCoZone";
        public const string CONNECTION_STRING_ELMAH = "server=TOFILLLOCALIP, 1433;user id=TOFILLLOCALUSER; password=TOFILLLOCALPASS;initial catalog=ELMAH";
        public const string FTP_ROOT_DIR = @"C:\FTPRoot\";
        public const int LOG_LEVEL = 0;
#endif
    }
}
