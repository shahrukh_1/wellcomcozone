﻿

namespace GMG.CoZone.FTP
{
    /// <summary>
    /// Log level Enum
    /// </summary>
    public enum LogLevel
    {
        Debug = 0,
        FTPCommandInfo,
        Info,
        Warning,
        Error,
        Fatal
    }
    
}
