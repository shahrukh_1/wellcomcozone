﻿namespace GMG.CoZone.Common
{
    public static class Permissions
    {
        private const string READ_ONLY = "RDO";
        private const string REVIEW = "RVW";
        private const string APPROVE_REVIEW = "ANR";

        public static bool CanApprove(string permission)
        {
            bool canApprove = false;
            if (!string.IsNullOrEmpty(permission))
            {
                switch (permission.ToUpper())
                {
                    case APPROVE_REVIEW:
                        canApprove = true;
                        break;
                    case REVIEW:
                        canApprove = false;
                        break;
                    case READ_ONLY:
                        canApprove = false;
                        break;
                }
            }
            return canApprove;
        }

        public static bool CanAnnotate(string permission)
        {
            bool canAnnotate = false;
            if (!string.IsNullOrEmpty(permission))
            {
                switch (permission.ToUpper())
                {
                    case APPROVE_REVIEW:
                        canAnnotate = true;
                        break;
                    case REVIEW:
                        canAnnotate = true;
                        break;
                    case READ_ONLY:
                        canAnnotate = false;
                        break;
                    
                }
            }
            return canAnnotate;
        }

    }
}
