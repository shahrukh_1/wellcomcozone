﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.Common
{
    /// <summary>
    /// Retrieves all output paths for the given folder
    /// </summary>
    public class ApprovalJobFolder
    {
        #region Private members
        private string _jobGUID;
        private string _fileName;
        private string _processingFolderPath;   // The local processing folder path (for local environemnt is the same as _dataStorageFolderPath)
        private string _dataStorageRemoteURI;  // The folder path where the ouput resides (local drive for local distribution or S3 path for AWS setup)
        #endregion

        #region Constants
        public const string EMBEDDED_PROFILE_NAME = "EmbeddedProfile.icc";
        public const string CUSTOM_ICC_PROFILE = "CustomICCProfile.icc";
        public const string PAPERTINT_ADJUSTED_NAME = "PaperTintAdjusted.icc";
        public const string TEXT_DESCRIPTOR_FILE_NAME = "TextDescriptor.xml";
        public const string JSON_TEXT_DESCRIPTOR_FILE_NAME = "TextDescriptor.json";
        public const string CONTAINER_FOLDER_NAME = "approvals";
        public const string FULL_SIZE_PREVIEW_FILE_NAME = "Converted.jpg";
        public const string BANDS_FOLDER = "Bands";
        public const string BAND_FORMAT = "Band{0}.bin";
        public const string CACHE_FOLDER = "Cache";
        public const string COMPLETED_MARKER = "CompletedMarker.txt";
        public const string PARTIAL_COMPLETED_MARKER = "PartialCompletedMarker.txt";
        #endregion

        #region Ctor
        public ApprovalJobFolder(string jobGUID, string fileName, string processingFolderPath, string dataStorageRemoteURI)
        {
            _jobGUID = jobGUID;
            _fileName = fileName;
            _processingFolderPath = processingFolderPath;
            _dataStorageRemoteURI = dataStorageRemoteURI;
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// Approval folder relative path to the container output folder
        /// </summary>
        public string ApprovalFolderRelativePath
        {
            get
            {
                return CONTAINER_FOLDER_NAME + @"\" + _jobGUID + @"\";
            }
        }

        public string ApprovalCacheProcessingFolderPath
        {
            get
            {
                return _processingFolderPath + CONTAINER_FOLDER_NAME + @"\" + CACHE_FOLDER + @"\" + _jobGUID + @"\";
            }
        }

        /// <summary>
        /// Approval file relative path to the container output folder
        /// </summary>
        public string ApprovalFileRelativePath
        {
            get
            {
                return ApprovalFolderRelativePath + _fileName;
            }
        }

        /// <summary>
        /// Approval file relative path to the container output folder
        /// </summary>
        public string ApprovalFileRemoteRelativePath
        {
            get
            {
                return ApprovalFileRelativePath.Replace('\\', '/');
            }
        }

        /// <summary>
        /// The folder path where the ouput resides (local drive for local distribution or S3 path for AWS setup)
        /// Absolute
        /// </summary>
        public string DataStorageFolderPath
        {
            get
            {
                return _dataStorageRemoteURI;
            }
        }

        /// <summary>
        /// The Approval local processing folder path
        /// Absolute
        /// </summary>
        public string ApprovalProcessingFolderPath
        {
            get
            {
                return _processingFolderPath + ApprovalFolderRelativePath;
            }
        }

        /// <summary>
        /// The approval file processing path
        /// Absolute
        /// </summary>
        public string ApprovalProcessingFilePath
        {
            get
            {
                return ApprovalProcessingFolderPath + _fileName;
            }
        }

        /// <summary>
        /// Embeded document profile processing path
        /// Absolute
        /// </summary>
        public string EmbededDocumentProfileProocessPath(string docPofileName)
        {
            return ApprovalProcessingFolderPath + docPofileName;
        }

        /// <summary>
        /// Embeded document profile relative path
        /// Absolute
        /// </summary>
        public string EmbededDocumentProfileRelativePath(string docProfileName)
        {
            return ApprovalFolderRelativePath + docProfileName;
        }

        /// <summary>
        /// Embeded document profile relative path
        /// Absolute
        /// </summary>
        public string EmbededDocumentProfileRemoteRelativePath(string docProfileName)
        {
            return EmbededDocumentProfileRelativePath(docProfileName).Replace(@"\", "/");
        }

        /// <summary>
        /// Tect descriptor file path
        /// </summary>
        public string TextDescriptorFilePath
        {
            get
            {
                return ApprovalProcessingFolderPath + TEXT_DESCRIPTOR_FILE_NAME;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieves the processing path of the given page
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageProcessingFolderPath(uint pageNo)
        {
            return ApprovalProcessingFolderPath + pageNo + @"\";
        }

        /// <summary>
        /// Retrieves page S3 bucket path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageBucketFolderPath(uint pageNo)
        {
            return DataStorageFolderPath + ApprovalFolderRelativePath + pageNo + @"\";
        }

        /// <summary>
        /// Retrieves the page soft proofing session bucket tiles path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="softProofingSessionGUID"></param>
        /// <returns></returns>
        public string GetPageSoftProofingSessionRemoteFolderPath(uint pageNo, string softProofingSessionGUID)
        {
            return _dataStorageRemoteURI + ApprovalFolderRelativePath.Replace(@"\","/") + softProofingSessionGUID + @"/" + pageNo + @"/";
        }

        /// <summary>
        /// Retrieves the page soft proofing session bucket tiles path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="softProofingSessionGUID"></param>
        /// <returns></returns>
        public string GetPageSoftProofingSessionRelativeFolderPath(uint pageNo, string softProofingSessionGUID)
        {
            return ApprovalFolderRelativePath.Replace(@"\", "/") + softProofingSessionGUID + @"/" + pageNo + @"/";
        }

        /// <summary>
        /// Retrieves the soft proofing session relative path
        /// </summary>
        /// <param name="softProofingSessionGUID"></param>
        /// <returns></returns>
        public string GetSoftProofingSessionRelativeFolderPath(string softProofingSessionGUID)
        {
            return ApprovalFolderRelativePath.Replace(@"\", "/") + softProofingSessionGUID + @"/";
        }

        /// <summary>
        /// Get Custom ICC Profile relative path
        /// </summary>
        /// <param name="softProofingSessionGUID"></param>
        /// <returns></returns>
        public string GetCustomICCRelativeFilePath(string softProofingSessionGUID)
        {
            return GetSoftProofingSessionRelativeFolderPath(softProofingSessionGUID) + CUSTOM_ICC_PROFILE;
        }

        /// <summary>
        /// Get Ppaer tint adjusted profile relative path
        /// </summary>
        /// <param name="softProofingSessionGUID"></param>
        /// <returns></returns>
        public string GetPaperTintICCRelativeFilePath(string softProofingSessionGUID)
        {
            return GetSoftProofingSessionRelativeFolderPath(softProofingSessionGUID) + PAPERTINT_ADJUSTED_NAME;
        }
		
		 /// <summary>
        /// Retrieves the approval buchet path
        /// </summary>
        /// <returns></returns>
        public string GetApprovalRemoteFolderPath()
        {
            return _dataStorageRemoteURI + ApprovalFolderRelativePath.Replace(@"\", "/");
        }

        /// <summary>
        /// Retrieves the page soft proofing session bucket bands path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageSoftProofingSessionRemoteBandsFolderPath()
        {
            return _dataStorageRemoteURI + ApprovalFolderRelativePath.Replace(@"\", "/") + BANDS_FOLDER + @"/";
        }

        /// <summary>
        /// Get 1:1 page preview file processing path (absolute)
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageFullSizePreviewProcessingPath(uint pageNo)
        {
            return GetPageProcessingFolderPath(pageNo) + FULL_SIZE_PREVIEW_FILE_NAME;
        }

        /// <summary>
        /// Get preview processing path for the given preview size (absolute)
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public string GetPageSmallPreviewProcessingPath(uint pageNo, int size)
        {
            return GetPageProcessingFolderPath(pageNo) + "Thumb-" + size + "px.jpg";
        }

        /// <summary>
        /// Retrieves the page path relative to the container folder path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageFolderReleativePath(uint pageNo)
        {
            return ApprovalFolderRelativePath + pageNo + @"\";
        }

        /// <summary>
        /// Pages band folder
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageBandsFolder(uint pageNo)
        {
            return GetPageFolderReleativePath(pageNo) + BANDS_FOLDER;
        }

        /// <summary>
        /// Pages band folder processing path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public string GetPageBandsFolderProcessingPath(uint pageNo)
        {
            return GetPageProcessingFolderPath(pageNo) + BANDS_FOLDER;
        }


        public string GetPageCacheFolder(uint pageNo)
        {
            return ApprovalCacheProcessingFolderPath + pageNo;
        }
        /// <summary>
        /// Get band folder cache path
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="bandNr"></param>
        /// <returns></returns>
        public string GetPageBandCacheFolder(uint pageNo)
        {
            return GetPageCacheFolder(pageNo) + @"\" + BANDS_FOLDER;
        }

        /// <summary>
        /// Get band file path from cache folder
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="bandNr"></param>
        /// <returns></returns>

        public string GetPageBandCachePath(uint pageNo, uint bandNr)
        {
            return GetPageBandCacheFolder(pageNo) + @"\" + String.Format(BAND_FORMAT, bandNr);
        }

        public string GetXmlFileProcessFolderPath(string pageNumber)
        {
            return ApprovalProcessingFolderPath + pageNumber + @"\" + Constants.XMLTextHighlightingFileName;
        }

        public string GetJsonFileProcessFolderPath(string pageNumber)
        {
            return ApprovalProcessingFolderPath + pageNumber + @"\" + Constants.JsonTextHighlightingFileName;
        }

        #endregion
    }
}
