﻿using System;
using System.Collections.Generic;

namespace GMG.CoZone.Common
{
	public class TempNotificationItem
	{		
		public static TempNotificationItem Create(List<int> approvalsIds, int loggedUserId, int newCollaborator, NotificationType notificationType, bool isExternal = false, string optionalMessage = "")
		{
			TempNotificationItem notificationItem = new TempNotificationItem(approvalsIds, loggedUserId, notificationType, newCollaborator, optionalMessage, isExternal);
			return notificationItem;
		}

		public static TempNotificationItem Create(List<int> approvalsIds, int eventCreator, NotificationType notificationType, int recipientId, string optionalMessage, bool isExternal = false)
		{
			TempNotificationItem notificationItem = new TempNotificationItem(approvalsIds, eventCreator, notificationType, recipientId, optionalMessage, isExternal);
			return notificationItem;
		}
		private TempNotificationItem(List<int> approvalsIds, int eventCreator, NotificationType notificationType, int recipientId, string optionalMessage, bool isExternal = false)
		{
			CreatedDate = DateTime.UtcNow;
			ApprovalsIds = approvalsIds;
			EventCreator = eventCreator;
			NotificationType = notificationType;
			if (isExternal)
			{
				ExternalRecipient = recipientId;
				InternalRecipient = null;
			}
			else
			{
				ExternalRecipient = null;
				InternalRecipient = recipientId;
			}

			OptionalMessage = optionalMessage;
		}

		public TempNotificationItem() {// empty constructor for serialization
			ApprovalsIds = new List<int>();
		}
		public List<int> ApprovalsIds { get; set; }
		public DateTime CreatedDate { get; set; }
		public int EventCreator { get; set; }
		public string OptionalMessage { get;set;}

		public int? InternalRecipient { get; set; }
		public int? ExternalRecipient { get; set; }
		public NotificationType NotificationType { get; set; }
	}
}
