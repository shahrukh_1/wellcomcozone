﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.Common
{
    public static class TrialAccountMessages
    {
        #region Constants

        public const string DefaultPassword = "password";
        public const string TrialAccountsName = "TrialAccountParent";
        public const string DefaultLanguage = "en-US";
        public const string DefaultDateFormat = "dd/MM/yyyy";
        public const string DefaultTimeFormat = "24HR";
        public const string DefaultContractPeriodKey = "ONEY";
        public const string DefaultAccountStatusKey = "A";
        public const string DefaultAccountSuspendedStatusKey = "S";
        public const string DefaultAccountTypeKeyForParent = "SBSY";
        public const string DefaultAccountTypeKey = "CLNT";
        public const string DefaultCurrencyKey = "EUR";
        public const string DefaultUserstateKeyForParent = "A";
        public const string DefaultUserstateKey = "N";
        public const string DefaultNotificationsFrequencyKey = "NVR";
        public const string DefaultUserPresetKey = "L";
        public const string DefaultThemeKey = "D";
        public const string DefaultAdminRoleKey = "ADM";
        public const string DefaultNoAccessRoleKey = "NON";
        public const string DefaultTimeZoneUtc = "GMT Standard Time";
        public const string CollaborateBillingPlanName = "Collaborate Prepress 50 Trial Demo";       
        public const string DeliverBillingPlanName = "Deliver 50 Trial Demo";
        public const string AccountDomainReplacePattern = @"[^a-zA-Z0-9_\-]+";
        public const string ScriptTagsPattern = @"<script[^>]*>";
        public const string AccountSuspendedReason = "Trial account suspended automatically after 15 days";       
        public const int CollaborateBillingPlanUnits = 50;
        public const int DeliverBillingPlanUnits = 50;
        public const int DefaultCollaborateModuleKey = 0;       
        public const int DefaultDeliverModuleKey = 2;
        public const int DefaultAdminModuleKey = 3;


        #endregion

        #region Error messages

        public const string CreateUserOfParentAccountIsNull = "Creator user of the system admin account is null";
        public const string CompanyOfParentAccountIsNull = "Company of the system admin account is null";
        public const string ParentAccountIsNull = "Parent account is null";
        public const string LanguageNotFound = "Language was not found in the system";
        public const string CountryNotFound = "Country was not found in the system";
        public const string TimezoneNotFound = "Timezone was not found in the system";
        public const string UnknowError = "Error creating the trial account";
        #endregion

        #region Validation Messages

        public const string NotSetSecurityToken = "Security token not set";
        public const string InvalidSecurityToken = "Security token is invalid";
        public const string RequiredFirstName = "First Name is required";
        public const string ToManyCharactersFirstName = "First Name - too many characters, max is 128";
        public const string RequiredLastName = "Last Name is required";
        public const string ToManyCharactersLastName = "Last Name - too many characters, max is 128";
        public const string RequiredPhone = "Phone is required";
        public const string ToManyCharactersPhone = "Phone - too many characters, max is 20";
        public const string RequiredEmail = "Email address is required";
        public const string ToManyCharactersEmail = "Email - too many characters, max is 64";
        public const string InvalidEmail = "Invalid email address";
        public const string RequiredCompanyName = "Company name is required";
        public const string InvalidCompanyName = "Invalid company name";
        public const string ToManyCharactersCompanyName = "Company name - too many characters, max is 128";
        public const string RequiredCity = "City is required";
        public const string RequiredAddress1 = "Address1 is required";
        public const string ToManyCharactersCity = "City - too many characters, max is 64";
        public const string RequiredCountry = "Country is required";
        public const string ToManyCharactersCountry = "Country - too many characters, max is 64";
        public const string RequiredTimeZone = "Timezone is required";
        public const string ToManyCharactersTimezone = "Timezone - too many characters, max is 64";
        public const string RequiredLanguage = "Language is required";
        public const string ToManyCharactersLanguage = "Language - too many characters, max is 64";
        public const string ToManyCharactersAddress1 = "Address1 - too many characters, max in 128";
        public const string ToManyCharactersAddress2 = "Address1 - too many characters, max in 128";
        public const string ScriptsTagsNotAllowed = "Script tags not allowed";

        #endregion
    }
}
