﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.Common
{
    public class CustomDictionary<TKey, TValue> : Dictionary<TKey, TValue> where TKey: IComparable, IEnumerable where TValue: IComparable
    {
        public new TValue this[TKey key]
        {
            get
            {
                return base.ContainsKey(key) ? base[key] : default(TValue);
            }
            set
            {
                base[key] = value;
            }
        }
    }
}
