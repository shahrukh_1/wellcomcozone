﻿using GMGColor.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;

namespace GMG.CoZone.Common
{
    /// <summary>
    /// Enum for ColorProof job status
    /// </summary>
    public enum ColorProofJobStatus
    {
        Created = 0,
        Downloading = 1,
        Waiting = 2,
        Processing = 3,
        Completed = 4,
        Cancelled = 5,
        Deleted = 6,
        DownloadFailed = 7,
        ValidationFailed = 8,
        ProcessingError = 9,
        CancelNotPossible = 10,
        Submitting = 11 // default value when adding in CoZone
    }

    /// <summary>
    /// Enum for deliver job status
    /// </summary>
    public enum DeliverJobStatus
    {
        Submitting = 0,
        Created = 1,
        Downloading = 2,
        Waiting = 3,
        Processing = 4,
        Completed = 5,
        Cancelled = 6,
        DownloadToCPFailed = 7,
        ValidationInCpFailed = 8,
        ProcessingError = 9,
        CompletedDeleted = 10,
        ErrorDeleted = 11
    }

    public enum DeliverPageStatus
    {
        Created = 0,
        Waiting,
        Printing,
        Completed,
        Deleted,
        Failed
    }

    public enum ImageState
    {
        Created = 0,
        Waiting = 1,
        Printing = 2,
        Completed = 3,
        Deleted = 4,
        Failed = 5
    }

    /// <summary>
    /// Enum for ActionTypes
    /// </summary>
    public enum ActionType
    {
        AddNewJob,
        AddNewCommand
    }

    public enum FTPProtocol
    {
        FTP = 0,
        FTPS = 1
    }

    /// <summary>
    /// Enum for Commands Type
    /// </summary>
    public enum CommandType
    {
        Cancel,
        Delete
    }

    /// <summary>
    /// Enum for Application Module
    /// </summary>
    public enum AppModule
    {
        Collaborate = 0,      
        Deliver =2,
        Admin = 3,
        SysAdmin = 4,
        FileTransfer = 6,
        Reports = 7,
        ProjectFolders = 8
    }

    /// <summary>
    /// Enum for JobType
    /// </summary>
    public enum JobType
    {
        DeliverJob
    }

    public enum PagesSelectionEnum
    {
        Single = 0,
        Range = 1,
        All = 2
    }

    /// <summary>
    /// Enum for Upload Engine duplicate file name options
    /// </summary>
    public enum FileUploadDuplicateName
    {
        Ignore = 0,
        ImportAsNewJob,
        MatchFileNameExactly,
        FileWithSuffix_vX
    }

    /// <summary>
    /// Enum for Upload Engine version name options
    /// </summary>
    public enum VersionUploadName
    {
        KeepOriginalName = 0,
        AppendSuffixvX,
        AppendSuffix_vX,
        AppendvXAtPosition
    }

    /// <summary>
    /// Enum for Upload Engine User Group Permissions
    /// </summary>
    public enum UploadEnginePermissionEnum
    {
        Uploader = 1,       
        DownloadFromCollaborate = 4,
        DownloadFromDeliver = 8       
    }

    public enum DownloadVersionTypes
    {
        [Display(ResourceType = typeof(Resources), Name = "lblCurrentVersion")]
        CurrentVersion = 0,
        [Display(ResourceType = typeof(Resources), Name = "lblAllVersions")]
        AllVersions
    }

    public enum DownloadFileTypes
    {
        [Display(ResourceType = typeof(Resources), Name = "enumFileTypeNative")]
        Native = 0,
        Zip
    }

    /// <summary>
    /// Enum for FTP Actions
    /// </summary>
    public enum FTPAction
    {
        UploadAction,
        UploadStatus
    }

    /// <summary>
    /// Enum for SoftProofing Actions
    /// </summary>
    public enum SoftProofingAction
    {
        ReadProfileWhitePoint = 0,
        CalculateDelta2000,
        ValidateCustomProfile
    }

    /// <summary>
    /// Enum for Default XML Fields
    /// </summary>
    public enum DefaultXMLTemplateField
    {
        JobName = 0,
        Modules,
        InternalCollaborator,
        ExternalCollaborator,
        PrimaryDecisionMaker,
        Owner,
        GenerateSeparations,
        LockWhenAllDecisionsHaveBeenMade,
        Deadline,
        AllowUsersToDownloadOriginalFile,
        DestinationFolder,
        ColorProofCozoneInstanceName,
        ColorProofCozoneWorkflow,
        Pages,
        LogProofControlResults,
        IncludeProofMetaInformationOnProofLabel,       
        DoNotApplyColorManagementIfPreflightFailed,
        LockWhenFirstDecisionHasBeenMade
    }

    /// <summary>
    /// Enum for Job Source
    /// </summary>
    public enum JobSource
    {
        WebUI = 0,
        WebService,
        UploadEngine
    }

    /// <summary>
    /// Enum for XML Job Type
    /// </summary>
    public enum XMLJobType
    {
        Collaborate = 1,
        Deliver = 2
    }

    // TODO - remove it and remove the admin/application switch from the entire system
    public enum SitePermissions
    {
        // The user can access the administrator site only.
        AdministratorSiteOnly,
        // The user can access the application site only.
        ApplicationSiteOnly,
        // The user can access both administrator and application sites.
        BothSites
    }

    /// <summary>
    /// Enum for Annotation Report Type
    /// </summary>
    public enum AnnotationReportType
    {
        Comments = 0,
        Pins,
        CommentsAndPins
    }

    /// <summary>
    /// Enum for Annotation Report Grouping
    /// </summary>
    public enum AnnotationReportGrouping
    {
        Date = 0,
        User,
        Group
    }

    /// <summary>
    /// Enum for Annotation Report Filtering
    /// </summary>
    public enum AnnotationReportFiltering
    {
        User = 0,
        Group
    }

    /// <summary>
    /// Enum for Annotation Report Filtering Report Format Type
    /// </summary>
    public enum AnnotationReportFormatType
    {
        PDFView = 1,
        DetailView = 2,
        ListView = 3
    }

    /// <summary>
    /// Enum for Collaborate Global Setting options
    /// </summary>
    public enum CollaborateGlobalSetting
    {
        AllCollaboratorsDecisionRequired,
        EnableFlexViewer,
        ProofStudioPenWidth,
        ShowAllFilesToAdmins,
        ShowAllFilesToManagers,
        ProofStudioShowAnnotationsForExternalUsers,
        DisplayRetouchers,
        EnableRetoucherWorkflow,
        EnableCustomProfile,
        InheritPermissionsFromParentFolder,
        DisableFileDueDateinUploader,
        HideCompletedApprovalsInDashboard,
        ShowBadges,
        DisableSOADIndicator,
        DisableVisualIndicator,
        DisableExternalUsersEmailIndicator,
        KeepExternalUsersWhenUploadingNewVersion,
        DisableTagwordsInDashbord,
        EnableProofStudioZoomlevelFitToPageOnload,
        ShowSubstrateFromICCProfileInProofStudio,
        EnableArchiveTimeLimit,
        ArchiveTimeLimit,
        ArchiveTimeLimitFormat
    }

    /// <summary>
    /// Approval file statuses
    /// </summary>
    public enum ApprovalFileStatus
    {
        NewVersion = 0,
        DuplicatedFile = 1,
        IgnoredFile = 2,
        NewJob = 3
    }

    /// <summary>
    /// Notification Preset Frequency types
    /// </summary>
    public enum NotificationPresetFrequency
    {
        AsEventOccur = 0,
        EveryHour,
        Every2Hours,
        Every4Hours,
        EveryDay,
        EveryWeek
    }

    /// <summary>
    /// Notification Preset Collaborate Units
    /// </summary>
    public enum NotificationPresetCollaborateUnit
    {
        Hours = 0,
        Days,
        Weeks
    }

    /// <summary>
    /// Notification Preset Collaborate trigger
    /// </summary>
    public enum NotificationPresetCollaborateTrigger
    {
        AfterStart = 0,
        BeforeEnd
    }

    /// <summary>
    /// Printer Company Type
    /// </summary>
    public enum PrinterCompanyTypeEnum
    {
        Supplier = 0,
        Client,
        Internal
    }

    ///<summary>
    /// PrePressOptions Enum
    /// </summary>
    public enum PlanFeatures
    {
        RotateTool = 1,
        Separations,
        Densitometer,
        Ruler,
        CustomProfile,
        ProfilePreview,
        ViewingConditions
    }

    /// <summary>
    /// Phase Deadline units
    /// </summary>
    public enum PhaseDeadlineUnit
    {
        Hours = 0,
        Days
    }

    /// <summary>
    /// Phase Deadline trigger
    /// </summary>
    public enum PhaseDeadlineTrigger
    {
        AfterUpload = 0,
        LastPhaseCompleted,
        ApprovalDeadline
    }

    public enum DeadlineType
    {
        NoDeadline = 0,
        FixDate,
        Automatic
    }

    public enum UserStatus
    {
        Active = 1,
        Invited,
        Inactive,
        Delete
    }

    public enum PlanViewType
    {
        Owned = 0,
        Attached
    }

	public enum NotificationType
	{
		ACommentIsMade = 0,
		RepliesToMyComments,
		NewAnnotationAdded,
		NewApprovalAdded,
		ApprovalWasDownloaded,
		ApprovalWasUpdated,
		ApprovalWasDeleted,
		ApprovalChangesCompleted,
		NewVersionWasCreated,
		ApprovalWasShared,
		UserWasAdded,
		GroupWasCreated,
		PlanWasChanged,
		AccountWasDeleted,
		AccountWasAdded,
		DeliverFileWasSubmitted,
		DeliverFileCompleted,
		DeliverFileErrored,
		DeliverFileTimeOut,
		ApprovalStatusChanged,		
		WorkstationRegistered,
		SharedWorkflowAccessRevoked,
		InvitedUserDeleted,
		InvitedUserFileSubmitted,
		ShareRequestAccepted,
		SharedWorkflowDeleted,
		HostAdminUserDeleted,
		HostAdminUserFileSubmitted,
		UserWelcome,
		ApprovalReminder,
		SharedWorkflowInvitation,
		BillingReport,
		LostPassword,
		ResetPassword,
		DeliverNewPairingCode,
		AccountSupport,
		DemoPlanExpired,		
		PlanLimitReached,
		PlanLimitWarning,
		ApprovalWasRejected,
		ApprovalOverdue,
		ApprovalStatusChangedByPdm,
		PhaseWasDeactivated,
		PhaseWasActivated,
		WorkflowPhaseRerun,
		PhaseOverdue,
        NewFileTransferAdded,
        FileTransferDownloaded,
        AtUserInComment,
        AtUserInReply,
        OutOfOffice,
        ChecklistItemEdited,
        ApprovalJobCompleted ,
        NewProjectFolder,
        ProjectFolderWasShared,
        ProjectFolderWasUpdated,
        NewAnnotationAtUserInComment

    }

    public enum CallBackType
    {
        ApprovalDueDate = 0,
        PhaseDueDate,
        LastPhaseDueDate,
        OnAnnotation
    }

    /// <summary>
    /// Role  ID 
    /// </summary>
    public enum RoleID
    {
        SysAdmin_Administrator = 1,
        SysAdmin_Manager,

        Collaborate_Administrator,
        Collaborate_Manager,
        Collaborate_Moderator,
        Collaborate_Contributor,
        Collaborate_Viewer,
        Collaborate_NoAccess,

        Deliver_Administrator = 14,
        Deliver_Manager,
        Deliver_Moderator,
        Deliver_Contributor,
        Deliver_NoAccess,

        Admin_Administrator,
        Admin_NoAccess,

        Collaborate_Retoucher = 28,

        FileTransfer_Administrator,
        FileTransfer_Manager,
        FileTransfer_NoAccess,

        Reports_Administrator,
        Reports_NoAccess,
        Reports_Moderator,

        ProjectReport_Administrator,
        ProjectReport_NoAccess,
        ProjectReport_Manager

    }

    public enum ProjectFolderStatusID
    {
        NotAssigned = 1,
        Ongoing ,
        Completed,
        Archived
    }
}