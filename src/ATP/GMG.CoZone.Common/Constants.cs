﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Remoting.Activation;
using System.Text;

namespace GMG.CoZone.Common
{
    public static class Constants
    {
        public const string HighResSessiobGuid = "SessionGuid";
        public const string IsJobHighRes = "IsJobHighRes";
        public const string JobMessageType = "JobMessageType";

        public const string LoggedUserId = "LoggedUserId";

        public const string ApprovalMsgApprovalID = "ApprovalID";
        public const string ApprovalMsgApprovalType = "ApprovalType";
        public const string ApprovalMsgImageFormat = "ImageFormat";
        public const string ApprovalMsgSeperations = "Seperations";
        public const string ApprovalMsgSimulateOverPrinting = "SimulateOverPrinting";
        public const string ApprovalMsgPagebox = "Pagebox";
        public const string ApprovalMsgColorSpace = "ColorSpace";
        public const string ApprovalMsgCompression = "Compression";
        public const string ApprovalMsgUrl = "Url";
        public const string ApprovalMsgApprovalPageID = "ApprovalPageID";
        public const string ApprovalMsgImageResolution = "ImageResolution";
        public const string ApprovalMsgWebCaptureDelay = "WebCaptureDelay";

        public const string CustomICCProfileID = "CustomICCProfileID";

        public const string DeliverMsgPagebox = "Pagebox";
        public const string DeliverMsgImageResolution = "ImageResolution";
        public const string DeliverMsgCompression = "Compression";
        public const string DeliverMsgColorSpace = "ColorSpace";
        public const string DeliverMsgImageFormat = "ImageFormat";
        public const string DeliverMsgSimulateOverPrinting = "SimulateOverPrinting";

        public const string DeliverQueueJobTitle = "Job-Title";
        public const string DeliverQueueCPServerName = "CPServer-Name";
        public const string DeliverQueueCPServerUsername = "CPServer-Username";
        public const string DeliverQueueCPServerPassword = "CPServer-Password";
        public const string DeliverQueueCPServerGuid = "CPServer-Guid";
        public const string DeliverQueueCPWorkflowName = "CPWorkflow-Name";
        public const string DeliverQueueJobGuid = "CPDeliverJob-Guid";
        public const string DeliverQueueJobBackLinkURL = "CPDeliverJob-BacklinkURL";
        public const string DeliverQueueJobFilename = "CPDeliverJob-Filename";
        public const string DeliverQueueJobPages = "CPDeliverJob-Pages";
        public const string DeliverQueueJobIncludeMetaInfo = "CPDeliverJob-IncludeProofMetaInformation";
        public const string DeliverQueueJobLogProofControlResults = "CPDeliverJob-LogProofControlResults";
        public const string DeliverQueueCoZoneUsername = "CoZone-Username";
        public const string DeliverQueueJobFileSize = "CPDeliverJob-Filesize";

        public const string DeliverQueueJobId = "DeliverID";
        public const string DeliverQueueActionType = "ActionType";
        public const string DeliverQueueCommandType = "CommandType";   

        public const string CollaborateRoleIdInSession = "gmg_role_collaborate_id";
        public const string FileTransferIdInSession = "gmg_role_filetransfer_id";
        public const string DeliverRoleIdInSession = "gmg_role_deliver_id";
        public const string AdminRoleIdInSession = "gmg_role_admin_id";
        public const string SysAdminRoleIdInSession = "gmg_role_sysadmin_id";
        public const string ReportIdInSession = "gmg_role_report_id";

        public const string ApprovalImageFile = "image";
        public const string ApprovalVideoFile = "video";
        public const string ApprovalSwfFile = "swf";

        public const string FTPPresetJobID = "FTPPresetJobID";
        public const string FTPAction = "FTPAction";
        public const string FTPGroupKey = "FTPGroupKey";
        public const string CollaborateFTPAccess = "HasAccessToFTPFromCollaborate";

        public const string CollaborateFolderName = "Collaborate";
        public const string DeliverFolderName = "Deliver";       

        public const string ApprovalReadOnlyRole = "readonly";
        public const string ApprovalReviewerRole = "reviewer";
        public const string ApprovalApproverAndReviewerRole = "approverandreviewer";

        public const string CoZoneZipName = "CoZone.zip";

        public const string ProfileId = "ProfileId";
        public const string Action = "Action";

        public const string WorkstationCalibrationDataGuid = "WorkstationCalibrationDataGuid";

        public readonly static string[] DangerousXSSTags = new string[]
                                                {
                                                    "applet", "body", "embeded", "frame", "script", "frameset",
                                                    "html", "iframe", "img", "style", "layer", "link", "ilayer",
                                                    "meta", "object", "html", "head", "body", "noscript", "comment"
                                                };

        public static readonly string[] DangerousXSSWords = new string[]
                                                             {
                                                                 "FSCommand", "onAbort", "onActivate", "onAfterPrint",
                                                                 "onAfterUpdate", "onBeforeActivate", "onBeforeCopy",
                                                                 "onBeforeCut", "onBeforeDeactivate","onBeforeEditFocus",
                                                                 "onBeforePaste", "onBeforePrint", "onBeforeUnload",
                                                                 "onBeforeUpdate","onBegin", "onBlur", "onBounce", 
                                                                 "onCellChange", "onChange", "onClick","onDataAvailable",
                                                                 "onContextMenu", "onControlSelect", "onCopy", "onCut",
                                                                 "onDataSetChanged", "onDataSetComplete", "onDblClick",
                                                                 "onDeactivate", "onEnd",
                                                                 "onDrag", "onDragEnd", "onDragLeave", "onDragEnter",
                                                                 "onDragOver", "onDragDrop", "onDragStart", "onDrop",
                                                                 "onError", "onErrorUpdate", "onFilterChange",
                                                                 "onFinish", "onFocus", "onFocusIn", "onFocusOut",
                                                                 "onHashChange", "onHelp", "onInput", "onKeyDown",
                                                                 "onKeyPress", "onKeyUp", "onLayoutComplete", "onLoad",
                                                                 "onLoseCapture", "onMediaComplete", "onMediaError",
                                                                 "onMessage", "onMouseDown", "onMouseEnter",
                                                                 "onMouseLeave", "onMouseMove", "onMouseOut",
                                                                 "onMouseOver", "onMouseUp", "onMouseWheel", "onMove",
                                                                 "onMoveEnd", "onMoveStart", "onOffline", "onOnline",
                                                                 "onOutOfSync", "onPaste", "onPause", "onPopState",
                                                                 "onProgress", "onPropertyChange", "onReadyStateChange",
                                                                 "onRedo", "onRepeat", "onReset", "onResize",
                                                                 "onResizeEnd", "onResizeStart", "onResume", "onReverse"
                                                                 , "onRowsEnter", "onRowExit", "onRowDelete",
                                                                 "onRowInserted", "onScroll", "onSeek", "onSelect",
                                                                 "onSelectionChange", "onSelectStart", "onStart",
                                                                 "onStop", "onStorage", "onSyncRestored", "onSubmit",
                                                                 "onTimeError", "onTrackChange", "onUndo", "onUnload",
                                                                 "onURLFlip", "seekSegmentTime", "javascript:",
                                                                 "@import", "xss:", "expression", "behavior", "CDATA",
                                                                 "#exec", "ActiveXObject", "onreadystatechange",
                                                                 "host/?"
                                                             };

        public const string SelectedApprovalId = "SelectedApprovalId";
        public const string SelectedTopLinkId = "SelectedTopLinkId";
        public const string EmailContentDataPattern = @"\(*<\$[a-zA-Z0-9_]+\$>\)*";
        public const string PasswordValidDataPattern = @"((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,20})";
        public const string ApprovalStatus = "Status";
        public const string ApprovalPopulateId = "PopulateID";
        public const string ApprovalsCurrentPage = "Approvals_Current_Page";
        public const string ApprovalsResetParameters = "Reset_Parameters";
        public const string ApprovalsCannotDelete = "Approvals_Cannt_Delete";
        public const string ApprovalsFolderId = "Approvals_Folder_ID";
        public const string ApprovalsTopLink = "Approvals_Top_Link";
        public const string ApprovalsExistingFolders = "Existing_Folders_";
        public const string ViewAllApprovals = "ViewAllApprovals";
        public const string AdminHasFullRights = "AdminHasFullRights_";
        public const string OutOfOfficeUser = "Out_Of_Office_User";
        public const string IsOpenedFromReportAppIndex = "isOpenedFromReportAppIndex";
        public const string SelectedProjectId = "SelectedProjectId";
        public const string IsFromProjectFolderIndex = "IsFrom_ProjectFolder_Index";

        public const string ProjectFilterField = "Project_Filter_Field";
        public const string ProjectFilterDir = "Project_Filter_Dir";
        public const string ProjectStatusFilter = "Project_Status_Filter";


        public const string EncryptDecryptPrivateKey = "0663FA63-CB11-4182-869D-8AA5411A15B3";
        public const string EncryptDecryptPrivateKeyXMLEnginePreset = "C8AAB7F8-AB19-47DE-A7AA-5E325F909A2D";

        public const string SubDomainNameRegex = @"^[a-zA-Z0-9\-]+$";
        public const string DomainNameRegex = @"^[a-zA-Z0-9_\-\.]*$";
        public const string CustomDomainRegex = @"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$";

        [ObsoleteAttribute("This const is obsolete. Use the const from the Common.Module")]
        public const string EmailRegex = @"^(""[^""]+""@[a-zA-Z0-9\-_]+[\.][a-zA-Z0-9\.\-_]+)$|^([a-zA-Z0-9\\\/$!%=_\.+\-""]+@[a-zA-Z0-9\-_]+[\.]+[a-zA-Z0-9\.\-_]*)$";
        public const string PlanPriceRegex = @"^\d{0,6}$|^\d{0,6}\.\d{0,2}$";
        /*
         * The previous email regex validate the following email addresses:
         * "Abc\@def"@example.com
         * "Fred Bloggs"@example.com
         * "Joe\\Blow"@example.com
         * "Abc@def"@example.com
         * customer/department=shipping@example.com
         * \$A12345@example.com
         * !def!xyz%abc@example.com
         * _somename@example.com
         * "sadsad@go.
         * user.fdfs@example.com
         * gmgcozone+PeterMartin@gmail.com
         * ana-maria@yahoo.co.uk
         */

        public const string NameRegex = @"^[a-zA-Z0-9 ]+$";
        public const string CollaborateNameRegex = @"^[a-zA-Z0-9 \(\)\&]+$";

        public const string EnableProofStudioZoomlevelFitToPageOnload = "EnableProofStudioZoomlevelFitToPageOnload";
        public const string ShowSubstrateFromICCProfileInProofStudio = "ShowSubstrateFromICCProfileInProofStudio";

        public const int TopLinkOffset = 10;

        public const string PrefixResizableColumnsCookieName = "resizable-columns-";

        public const string SimulationProfileMsgProfilelID = "ProfileId";
        public const string SimulationProfileAction = "SimulationProfileAction";
        public const string PaperTindId = "PpaerTindId";
        public const string MeasurementCond = "MeasurementCond";

        #region CoZone XML job Nodes

        public const string XMLNodeInternalCollaboratorUserName = "Username";
        public const string XMLNoteCollaboratorRole = "Role";
        public const string XMLNodeExternalCollaboratorEmail = "Email";
        public const string XMLNodePrimaryDecisionMakerUser = "User";
        public const string XMLNodePrimaryDecisionMakerIsExternal = "IsExternal";

        #endregion

        #region SoftProofing
        public const string PAPER_TINT = "PaperTint";
        public const string PAGEID = "PageId";
        public const string OUTPUT_PROFILENAME = "Profile";
        public const string IS_DEFAULT_PROFILE = "DefaultProfile";
        public const string SESSIONGUID = "SessionId";
        public const string CHANNELS = "Channels";
        public const string EMBEDDEDPROFILENAME = "EmbeddedProfile";
        public const string REDPLATE = "red";
        public const string GREENPLATE = "green";
        public const string BLUEPLATE = "blue";
        #endregion

        #region Instant Notifications

        public const string HubsControllerActionName = "hubs";
        public const string HubsControllerName = "signalr";

        #endregion       

        public const int NumberOfApprovalsPerPageInProofStudio = 50;

        #region Text Highlighting

        public static string JsonTextHighlightingFileName = "TextDescriptor.json";
        public static string XMLTextHighlightingFileName = "TextDescriptor.xml";

        #endregion
    }
}
