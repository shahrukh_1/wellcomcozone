﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.Net;
using System.IO;


namespace GMG.CoZone.Common
{
    public class Utils
    {
        private static readonly byte[] PrivateKey = Encoding.ASCII.GetBytes("o6806642kbM7c5@");
        internal static string PublicKey = "GMGColor";       

        #region Constants

        public const string jobStatusWaiting = "Waiting";
        public const string jobStatusProcessing = "Processing";
        public const string jobStatusFinished = "Finished";
        public const string jobStatusPreflightPass = "PreflightPass";
        public const string jobStatusPreflightFail = "PreflightFail";
        
        private const string randomcharactersAllowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        #endregion

        /// <summary>
        /// Add a Command message to delivery queue messages
        /// </summary>
        /// <param name="guid">Guid of the JobTicket</param>
        /// <param name="userName">ColorProof Instance username</param>
        /// <param name="password">ColorProof Instance password</param>
        /// <param name="type">Command Type</param>
        /// <returns></returns>
        [Obsolete("Use from FtpService")]
        public static Dictionary<string, string> CreateCommandQueueMessage(string guid, string userName, string password, string colorProofInstanceGuid, CommandType type)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add(Constants.DeliverQueueActionType, ActionType.AddNewCommand.ToString());
            dict.Add(Constants.DeliverQueueCommandType, type.ToString());
            dict.Add(Constants.DeliverQueueJobGuid, guid);
            dict.Add(Constants.DeliverQueueCPServerGuid, colorProofInstanceGuid);

            return dict;
        }

        /// <summary>
        /// Creates the message to be added to FTP queue messages
        /// </summary>
        /// <param name="jobId">FTPDownloadJobId</param>
        /// <param name="jobType">JobType</param>
        /// <param name="action">Ftp Action</param>
        /// <returns>The message to be added to the queue</returns>
        [Obsolete("Use from FtpService")]
        public static CustomDictionary<string, string> GetDictionaryForFTPQueue(int jobId, AppModule jobType, FTPAction action)
        {
            var dict = new CustomDictionary<string, string>
            {
                {Constants.FTPPresetJobID, jobId.ToString()},
                {Constants.JobMessageType, jobType.ToString()},
                {Constants.FTPAction, action.ToString()}
            };

            return dict;
        }

        /// <summary>
        /// Returns user Input from Pages string format
        /// </summary>
        /// <param name="dbValue"></param>
        /// <param name="pageRangeType"></param>
        /// <param name="pageRangeValue"></param>
        public static void StringToPage(string dbValue, out PagesSelectionEnum pageRangeType, out string pageRangeValue)
        {
            try
            {
                int charPosition = dbValue.IndexOf(":");
                if (charPosition > -1)
                {
                    pageRangeType =
                        (PagesSelectionEnum) Enum.Parse(typeof (PagesSelectionEnum), dbValue.Substring(0, charPosition));
                    pageRangeValue = dbValue.Substring(charPosition + 1);
                }
                else
                {
                    pageRangeValue = string.Empty;
                    pageRangeType = (PagesSelectionEnum) Enum.Parse(typeof (PagesSelectionEnum), dbValue);
                }
            }
            catch(Exception)
            {
                pageRangeValue = string.Empty;
                pageRangeType = PagesSelectionEnum.Single;
            }
        }

        /// <summary>
        /// Get Pages For CzJobTicket
        /// </summary>
        /// <returns>The Collection of Pages to be sent to ColorProof for this Job</returns>
        public static Collection<int> GetPages(string selectedPages)
        {
            Collection<int> pages = new Collection<int>();

            PagesSelectionEnum pageType = PagesSelectionEnum.Single;
            string rangeValue;
            Utils.StringToPage(selectedPages ?? string.Empty, out pageType, out rangeValue);

            switch (pageType)
            {
                case PagesSelectionEnum.Single:
                    {
                        pages.Add(0);
                        break;
                    }
                case PagesSelectionEnum.All:
                    {
                        return null;
                    }
                case PagesSelectionEnum.Range:
                    {
                        pages = ReturnRangePages(rangeValue);
                        break;
                    }
            }
            return pages;
        }

        /// <summary>
        /// Returns the range pages.
        /// </summary>
        /// <param name="rangeValue">The range value.</param>
        /// <returns></returns>
        public static Collection<int> ReturnRangePages(string rangeValue)
        {
            Collection<int> pages = new Collection<int>();

            string[] pagesStr = rangeValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string pageStr in pagesStr)
            {
                string[] finalPages = pageStr.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (finalPages.Length > 0)
                {
                    int firstPage = 0;
                    int lastPage = 0;
                    if (int.TryParse(finalPages.First(), out firstPage) && int.TryParse(finalPages.Last(), out lastPage))
                    {
                        if (firstPage > lastPage)
                        {
                            int aux = firstPage;
                            firstPage = lastPage;
                            lastPage = aux;
                        }
                        for (int i = firstPage - 1; i <= lastPage - 1; i++)
                        {
                            pages.Add(i);
                        }
                    }
                }
            }
            List<int> auxList = pages.Distinct().ToList();
            auxList.Sort();
            Collection<int> pagesToReturn = new Collection<int>(auxList);
            return pagesToReturn;
        }

        /// <summary>
        /// Gets the supported spot colors for web.
        /// </summary>
        /// <param name="supportedSpotColors">The supported spot colors.</param>
        /// <returns></returns>
        public static string GetSupportedSpotColorsForWeb(string supportedSpotColors)
        {
            var arr = (supportedSpotColors ?? string.Empty).Split(new string[] { Environment.NewLine },
                                                                     StringSplitOptions.None);
            return string.Join("", arr.Select(o => "<div class=\"spotcolor-line\">" + o + "</div>"));
        }

        /// <summary>
        /// Creates an async web request
        /// </summary>
        /// <param name="xmlData">data to be sent on request</param>
        /// <param name="url">url to which to send the request</param>
        public static string SendWebRequest(XDocument xmlData, string url)
        {
            WebRequest webRequest = WebRequest.Create(new Uri(url));
            webRequest.Method = "POST";
            webRequest.ContentType = "application/xml";

            byte[] bytes = Encoding.UTF8.GetBytes(xmlData.ToString());

            webRequest.ContentLength = bytes.Length;
            ((HttpWebRequest)webRequest).AllowWriteStreamBuffering = true;

            using (System.IO.Stream putStream = webRequest.GetRequestStream())
            {
                putStream.Write(bytes, 0, bytes.Length);
            }

            ((HttpWebRequest)webRequest).KeepAlive = false;

            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
            string responseStatus = response.StatusCode.ToString();
            response.Close();

            return responseStatus;
        }

        /// <summary>
        /// Tries to get profile folder name from ftp folder structure [profile\jobFolder]
        /// </summary>
        /// <param name="ftpFolderStructure"></param>
        public static void TryGetProfileFolder(string ftpFolderStructure, out string profileFolder)
        {
            profileFolder = null;
            if (ftpFolderStructure != null)
            {
                string[] ftpFolders = ftpFolderStructure.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                //Only 2 folders should be here : [profileFolder]\[jobSourceFolder]
                if (ftpFolders.Count() == 2)
                {
                    profileFolder = ftpFolders[0];
                }
            }
        }

        /// <summary>
        /// Encrypts the specified to encrypt.
        /// </summary>
        /// <param name="toEncrypt">To encrypt.</param>
        /// <param name="privateKey">The private key.</param>
        /// <param name="useHashing">if set to <c>true</c> [use hashing].</param>
        /// <returns></returns>
        public static string Encrypt(string toEncrypt, string privateKey = Constants.EncryptDecryptPrivateKey, bool useHashing = true)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);

            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(privateKey));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = Encoding.UTF8.GetBytes(privateKey);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return ByteArrayToString(resultArray);
        }

        /// <summary>
        /// Decrypts the specified cipher string.
        /// </summary>
        /// <param name="cipherString">The cipher string.</param>
        /// <param name="privateKey">The private key.</param>
        /// <param name="useHashing">if set to <c>true</c> [use hashing].</param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, string privateKey = Constants.EncryptDecryptPrivateKey, bool useHashing = true)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = StringToByteArray(cipherString);

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(privateKey));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = Encoding.UTF8.GetBytes(privateKey);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return Encoding.UTF8.GetString(resultArray);
        }

        /// <summary>
        /// Bytes the array to string.
        /// </summary>
        /// <param name="ba">The ba.</param>
        /// <returns></returns>
        private static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        /// <summary>
        /// Strings to byte array.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <returns></returns>
        private static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length / 2;
            byte[] bytes = new byte[NumberChars];
            using (var sr = new StringReader(hex))
            {
                for (int i = 0; i < NumberChars; i++)
                    bytes[i] =
                      Convert.ToByte(new string(new char[2] { (char)sr.Read(), (char)sr.Read() }), 16);
            }
            return bytes;
        }

        /// <summary>
        /// Converts string to date time
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="dateformat"></param>
        /// <returns></returns>
        public static DateTime? ConvertString2Datetime(string datetime, string dateformat)
        {
            if (string.IsNullOrEmpty(datetime))
                return null;

            DateTime date;
            return
                (DateTime.TryParseExact(datetime, dateformat, null, DateTimeStyles.None,
                                        out date))
                    ? (DateTime?)date
                    : null;
        }

        /// <summary>
        /// Generates a random alphanumeric string with the specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GenerateRandomAlphanumeric(int length)
        {
            var stringChars = new char[length];
            var random = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = randomcharactersAllowed[random.Next(randomcharactersAllowed.Length)];
            }
            return  new String(stringChars);
        }

        /// <summary>
        /// Returns a list of predefined colors
        /// </summary>
        /// <returns></returns>
        public static string[] GetPredefinedColorList()
        {
            return new[]
            {
                "#00FFFF",
                "#000000",
                "#0000FF",
                "#8A2BE2",
                "#A52A2A",
                "#7FFF00",
                "#D2691E",
                "#6495ED",
                "#006400",
                "#FF0000",
                "#1E90FF",
                "#FFD700",
                "#ADFF2F",
                "#FF00FF",
                "#9370DB",
                "#556B2F",
                "#FFFF00",
                "#008080",
                "#6A6C36",
                "#8DC852"
            };
        }
		
		public static string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
		}

        public static double BytestoGB(decimal bytes)
        {
            return (double)bytes / 1024 / 1024 / 1024;
        }

        /// <summary>
        /// Convert a hex string to it's RGB string representation
        /// </summary>
        /// <param name="hexColor"></param>
        /// <returns></returns>
        public static string ConvertHexToRGB(string hexColor)
        {
            var argColor = System.Drawing.ColorTranslator.FromHtml(hexColor);
            return "rgb(" + argColor.R + "," + argColor.G + "," + argColor.B + ")";
        }

        /// <summary>
        /// Convert rgb string to hex
        /// </summary>
        /// <param name="rgbColor"></param>
        /// <returns></returns>
        public static string ConvertRgbToHex(string rgbColor)
        {
            return System.Text.RegularExpressions.Regex.Replace
                (
                    rgbColor,
                    @"rgb\([ ]*(\d+)[ ]*,[ ]*(\d+)[ ]*,[ ]*(\d+)[ ]*\)",
                    m =>
                    {
                        return "#" + Int32.Parse(m.Groups[1].Value).ToString("X2") +
                        Int32.Parse(m.Groups[2].Value).ToString("X2") +
                        Int32.Parse(m.Groups[3].Value).ToString("X2");
                    }
                );
        }

        /// <summary>
        /// Encrypts the specified plain text.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <returns></returns>
        internal static string Encrypt(string plainText)
        {
            return Encrypt(plainText, PublicKey);
        }

        /// <summary>
        /// Encrypt the given string using AES.  The string can be decrypted using 
        /// DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        public static string Encrypt(string plainText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            string outStr = null;                       // Encrypted string to return
            RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, PrivateKey);

                // Create a RijndaelManaged object
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // prepend the IV
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                    }
                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return outStr;
        }

        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <returns></returns>
        internal static string Decrypt(string cipherText)
        {
            return Decrypt(cipherText, PublicKey);
        }

        /// <summary>
        /// Decrypt the given string.  Assumes the string was encrypted using 
        /// EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        public static string Decrypt(string cipherText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException("cipherText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, PrivateKey);

                // Create the streams used for decryption.                
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    // Create a RijndaelManaged object
                    // with the specified key and IV.
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    // Get the initialization vector from the encrypted stream
                    aesAlg.IV = ReadByteArray(msDecrypt);
                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;
        }

        /// <summary>
        /// Reads the byte array.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        private static byte[] ReadByteArray(Stream s)
        {
            byte[] rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }
}
