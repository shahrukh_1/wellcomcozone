﻿using System;
using System.Reflection;
using GMGColorDAL;
using log4net.Repository.Hierarchy;
using GMG.CoZone.Component.Logging;

namespace GMG.CoZone.EmailService
{
    /// <summary>
    /// Please do not use this method and instead go to GMG.CoZone.Common.Module
    /// </summary>
    public static class ServiceLog
    {
        private static readonly GMG.CoZone.Component.Logging.Logger _log =
            new Component.Logging.Logger(GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.ActiveConfiguration + " " + GMGColorConfiguration.AppConfiguration.AWSRegion
                                                                                : String.Empty
                                        , "DBErrorLogger", "EmailLogger");

        /// <summary>
        /// Log method
        /// </summary>
        /// <param name="message">proper message associated with the log</param>
        /// <param name="ex">The Exception that was thrown</param>
        public static void Log(string message, Exception ex)
        {
            _log.Log(new LoggingException(message, ex));
        }

        /// <summary>
        /// Log method
        /// </summary>
        /// <param name="notification"></param>
        public static void Log(LoggingNotification notification)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int) notification.Severity)
            {
                _log.Log(notification);
            }
        }
    }
}
