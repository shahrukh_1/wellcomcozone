﻿using GMG.CoZone.Common;
using System.Collections.Generic;
using System;
using System.Messaging;
using System.Linq;
using GMGColorDAL;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using System.Xml.Serialization;
using Newtonsoft.Json;
using GMGColor.AWS;
using System.Threading.Tasks;
using Amazon.SQS.Model;

namespace GMG.CoZone.EmailService
{
    /// <summary>
    /// This class reads temporary notification from a queue (AWS or Local) and returns a grouped list of notification by approval id
    /// </summary>
    class TempNotificationReader
    {                                                         
        private string _queueName = GMGColorConfiguration.AppConfiguration.TempNotificationQueueName;
        private bool _isEnabledAmazonSQS = GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS;

        private static Amazon.SQS.AmazonSQSClient amazoneSQSClient;
        /// <summary>
        /// Checks if Amazon SQS is enable and reads queue based on outcome
        /// </summary>
        /// <returns></returns>
        public List<TempNotificationItem> ReadApplyFolderPermissionTempNotifications()
        {
            if (_isEnabledAmazonSQS)
            {
                return ReadApplyFolderPermissionTempNotificationFromAWS();
            }
            else
            {
                return ReadApplyFolderPermissionTempNotificationFromLocal();
            }
        }

        /// <summary>
        /// Return a list of grouped temp notification items based on approval id
        /// </summary>
        /// <returns></returns>
        private List<TempNotificationItem> ReadApplyFolderPermissionTempNotificationFromLocal()
        {
            var TempNotificationItemList = new List<TempNotificationItem>();
            try
            {
                var messages = ReadLocalQueueMessages();

                messages.ForEach(msg => { TempNotificationItemList.Add((TempNotificationItem)msg.Body); });
            }
            catch (Exception ex)
            {
                ServiceLog.Log(new LoggingNotification("ReadApplyFolderPermissionTempNotificationFromLocal method failed Exception: " + ex.Message, Severity.Error));
            }
            return TempNotificationItemList;
        }

        public static ReceiveMessageResponse ReadMessages(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            var receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.QueueUrl = AWSSimpleQueueService.GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);

            return SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ReceiveMessage(receiveMessageRequest);
        }
        public static bool DeleteMessage(string queueName, Amazon.SQS.Model.Message message, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            bool deleteSuccess = false;

            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest();
            deleteMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            deleteMessageRequest.ReceiptHandle = message.ReceiptHandle;

            DeleteMessageResponse objDeleteMessageResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).DeleteMessage(deleteMessageRequest);

            deleteSuccess = true;

            return deleteSuccess;
        }
        public static string GetQueueUrl(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            string queueUrl = string.Empty;

            ListQueuesRequest listQueuesRequest = new ListQueuesRequest();
            listQueuesRequest.QueueNamePrefix = queueName;
            ListQueuesResponse listQueuesResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ListQueues(listQueuesRequest);

            if (listQueuesResponse.QueueUrls.Count > 0)
            {
                queueUrl = listQueuesResponse.QueueUrls[0].ToString();
            }
            else
            {
                throw (new Exception("The specified Queue was not found on AWS Queue server."));
            }

            return queueUrl;
        }
        public static Amazon.SQS.AmazonSQSClient SQSClient(string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            if (amazoneSQSClient == null)
            {
                amazoneSQSClient = new Amazon.SQS.AmazonSQSClient(AWSAccessKey, AWSSecretKey, Amazon.RegionEndpoint.GetBySystemName(AWSRegion));
            }
            return amazoneSQSClient;
        }

        /// <summary>
        /// Groups the read messages to by approval id
        /// </summary>
        /// <returns></returns>
        private List<TempNotificationItem> ReadApplyFolderPermissionTempNotificationFromAWS()
        {
            var tempNotificationList = new List<TempNotificationItem>();
            try
            {
                var receivedMessages = ReadMessages(_queueName,
                                                    GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                    GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                    GMGColorConfiguration.AppConfiguration.AWSRegion
                                                   );

                if (receivedMessages.Messages.Count > 0)
                {
                    foreach (var message in receivedMessages.Messages)
                    {
                        ServiceLog.Log(new LoggingNotification(String.Format("Message {0}", message.Body), Severity.Info));

                        var notification = JsonConvert.DeserializeObject<TempNotificationItem>(message.Body);

                        ServiceLog.Log(new LoggingNotification(String.Format("Notification {0} : {1} ", notification.NotificationType.ToString(), notification.EventCreator), Severity.Info));

                        tempNotificationList.Add(notification);

                        // After read, delete message
                        DeleteMessage(_queueName,
                                      message,
                                      GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                      GMGColorConfiguration.AppConfiguration.AWSRegion);
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceLog.Log(String.Format("ReadApplyFolderPermissionTempNotificationFromAWS method failed: {0}", ex.Message), new LoggingException( ex.Message, ex, Severity.Error));
            }
            return tempNotificationList;
        }

        /// <summary>
        /// Read local queue and return a list of notification items
        /// </summary>
        /// <returns></returns>
        private List<System.Messaging.Message> ReadLocalQueueMessages()
        {
            var tempMessageList = new List<System.Messaging.Message>();
            MessageQueue messageQueue = new MessageQueue(_queueName);
            messageQueue.Formatter = new XmlMessageFormatter(new Type[] { typeof(TempNotificationItem) });
            var msgEnumerator = messageQueue.GetMessageEnumerator2();

            while (msgEnumerator.MoveNext(new TimeSpan(0, 0, 1)))
            {
                var msg = messageQueue.ReceiveById(msgEnumerator.Current.Id, new TimeSpan(0, 0, 1));
                tempMessageList.Add(msg);
            }
            return tempMessageList;
        }
    }
}
