﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.EmailService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine("CoZone Email Service Started !\nPress Any key to exit");

                var emailService = new EmailService();

                var processNotificationQueueTask = new Task(emailService.StartEmailService);
                processNotificationQueueTask.Start();
               
                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }

                emailService.StopEmailService();
            }
            else
            {
                ServiceBase[] servicesToRun;
                servicesToRun = new ServiceBase[] 
			    { 
				    new CoZoneEmailService() 
			    };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
