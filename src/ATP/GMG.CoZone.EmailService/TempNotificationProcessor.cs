﻿using GMG.CoZone.Common;
using GMGColorBusinessLogic;
using GMGColorNotificationService;
using GMGColorNotificationService.CreateNotifications;
using System.Collections.Generic;
using System.Linq;

namespace GMG.CoZone.EmailService
{
    /// <summary>
    /// This class creates notification items for Apply Folder Permissions
    /// </summary>
    class TempNotificationProcessor
    {
        private DbContextBL _context;
        private List<TempNotificationItem> _processTempNotificationItems;
		private INotificationCreator _notificationCreator;

        public TempNotificationProcessor(DbContextBL context, List<TempNotificationItem> processTempNotificationItems)
        {
            _context = context;
            _processTempNotificationItems = processTempNotificationItems;
			_notificationCreator = NotificationsCreator.getInstance();

		}

        /// <summary>
        /// Create actual notification items based on temp items
        /// </summary>
        public void StartProcessingTempNotifications()
         {
            foreach (var tempNotificationItem in _processTempNotificationItems)
            {
                var loggedUser = _context.Users.SingleOrDefault(us => us.ID == tempNotificationItem.EventCreator);

                NotificationServiceBL.CreateNotification(_notificationCreator.Create(tempNotificationItem), loggedUser, _context, saveChanges: false);
            }
            _context.SaveChanges();
        }
    }
}
