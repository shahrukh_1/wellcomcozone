﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Extensions;
using GMGColorNotificationService;
using GMG.CoZone.Common;

namespace GMG.CoZone.EmailService.Utils
{
    class EmailServiceUtils
    {
        /// <summary>
        /// Creates daily/weekly email for the user
        /// </summary>
        /// <param name="userFrequency"></param>
        /// <param name="userId"></param>
        /// <param name="userNotifications"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static GMGColorDAL.Email CreateDigestEmailByFrequency(NotificationFrequency.Frequency userFrequency, int userId,
            List<UserNotification> userNotifications, List<GMGColorDAL.EventType> eventTypes, Dictionary<string, List<string>> emailTemplates ,DbContextBL context)
        {
            Email email = null;
            List<NotificationGroup> userNotificationsToSend = new List<NotificationGroup>();

            foreach (UserNotification notificationItem in userNotifications)
            {
                try
                {
                    // Get the notification type
                    NotificationType? notificationType = NotificationTypeParser.ParseString(
                        eventTypes.FirstOrDefault(
                            et => et.ID == notificationItem.NotificationItem.NotificationType).Key);

                    // Create notification item
                    Notification notification =
                        NotificationServiceBL.CreateNotification(notificationItem.NotificationItem,
                            notificationType.Value);

                    //check if email is ready to be sent
                    if (!IsEmailReadyToBeSent(notification, context))
                    {
                        continue;
                    }

                    userNotificationsToSend.Add(new NotificationGroup()
                    {
                        Notifications = notification,
                        EventGroupKey = notificationItem.EventGroupKey,
                        NotificationItem = notificationItem.NotificationItem
                    });
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("ProcessInternalUsersEmails: Send DailyWeekly Email Load Notification exception: ",
                        new LoggingException("ProcessEmailQueue failed", ex, Severity.Error));
                }
            }

            try
            {
                //Create base template based on notification frequency (Daily or Weekly)
                var template = EmailTemplate.CommonOuterTemplate.Replace("<$template_content$>",
                    userFrequency == NotificationFrequency.Frequency.Weekly
                                    ? EmailTemplate.DigestWeekly
                                    : EmailTemplate.DigestDaily);


                // Send daily or weekly emails
                email = EmailUtils.ProcessEmailsByFrequencyType(userNotificationsToSend,
                    userFrequency,
                    emailTemplates, userId, template, context);

                //mark items to be deleted from database if email is created with success
                context.NotificationItems.RemoveRange(
                    userNotificationsToSend.Select(t => t.NotificationItem));

                ServiceLog.Log(
                    new LoggingNotification(
                        String.Format("{0} emails send on: {1} for user {2}", userFrequency,
                            DateTime.UtcNow, userId),
                        Severity.Debug));
            }
            catch (Exception ex)
            {
                ServiceLog.Log(
                    String.Format("ProcessDailyWeeklyEmails: Send {0} email for user {1} failed", userFrequency,
                        userId),
                    new LoggingException("ProcessEmailsByFrequencyType failed", ex, Severity.Error));
            }

            return email;
        }
        /// <summary>
        /// Determines whether the email is ready to be sent now or should be skipped and sent later
        /// </summary>
        /// <param name="notification">Current Notification to be sent</param>
        /// <param name="context">Database Notification</param>
        /// <returns></returns>
        public static bool IsEmailReadyToBeSent(Notification notification, DbContextBL context)
        {
            switch (notification.NotificationType)
            {
                case NotificationType.NewApprovalAdded:
                case NotificationType.NewVersionWasCreated:
                case NotificationType.ApprovalWasShared:
                    ApprovalNotification approvalNotif = notification as ApprovalNotification;
                    return approvalNotif != null && approvalNotif.ApprovalsIds.All(t => ApprovalBL.IsApprovalProcessingCompleted(t, context));
                case NotificationType.DeliverFileWasSubmitted:
                case NotificationType.DeliverFileCompleted:
                case NotificationType.DeliverFileErrored:
                case NotificationType.DeliverFileTimeOut:
                case NotificationType.HostAdminUserFileSubmitted:
                case NotificationType.InvitedUserFileSubmitted:
                    DeliverNotification deliverNotif = notification as DeliverNotification;
                    return deliverNotif != null && DeliverBL.AreThumbnailsProcessed(deliverNotif.JobsGuids, context);
               
            }

            return true;
        }
        
        /// <summary>
        /// Saves all emails to be sent
        /// </summary>
        /// <param name="emailsToSent">A list of emails to be sent</param>
        /// <param name="context">Database context</param>
        public static void SaveEmails(List<Email> emailsToSent, DbContextBL context)
        {
            try
            {
                context.Emails.AddRange(emailsToSent);
            }
            catch (Exception ex)
            {
                ServiceLog.Log("SaveEmails method failed", new LoggingException("Exception in SaveEmails method", ex, Severity.Error));
            }
        }
    }
}
