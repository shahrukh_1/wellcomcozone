﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using GMG.CoZone.Common;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Extensions;
using Email = GMGColor.AWS.Email;
using System.Globalization;
using static GMG.CoZone.EmailService.EmailService;
using Newtonsoft.Json.Linq;
using Amazon.SimpleEmail.Model;
using Amazon.SimpleEmail;
using GMG.CoZone.Common.Interfaces;
using Amazon;

namespace GMG.CoZone.EmailService
{
    class ProofStudioSessionWorker
    {
        #region Private Members

        private CancellationTokenSource _sendEmailTaskCTS = new CancellationTokenSource();
        private List<GMGColorDAL.EventType> _eventTypes;
        private Dictionary<string, List<string>> _emailTemplates;
        #endregion

        #region Constructors
        public ProofStudioSessionWorker(List<GMGColorDAL.EventType> typesOfEvents, Dictionary<string, List<string>> emailTemplates)
        {
            _eventTypes = typesOfEvents;
            _emailTemplates = emailTemplates;
        }
        #endregion

        #region Methods

        public void StartSessionWorker()
        {
            while (!_sendEmailTaskCTS.Token.IsCancellationRequested)
            {
                ProcessPsNotifications();
                Thread.Sleep(TimeSpan.FromMinutes(10)); // wait 2 seconds between syncs
            }
        }

        public void StopSessionWorker()
        {
            _sendEmailTaskCTS.Cancel();
        }

        public void ProcessPsNotifications()
        {
            try
            {
                List<UserNotifications> userNotifications;
                var emails = new List<GMGColorDAL.Email>();
                List<Receiver> receivers = new List<Receiver>();
                string emailSenderAddress = string.Empty;
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    using (var context = new DbContextBL())
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        userNotifications = NotificationServiceBL.GetNotificationsPerProofStudioSession(context, GMGColorConfiguration.AppConfiguration.ProofStudioSessionTimeOut);

                        foreach (var userNotification in userNotifications)
                        {
                            //if user was deleted, remove all the emails related with him
                            if (userNotification.InternalUserRecipient.UserStatusKey == "D")
                            {
                                foreach (UserNotification notificationItem in userNotification.Notifications)
                                {
                                    context.NotificationItems.Remove(notificationItem.NotificationItem);
                                }

                                continue;
                            }
                            //set thread culture for current user
                            var userLocale = UserBL.GetUserLocale(userNotification.InternalUserRecipient.UserId, context);
                            Thread.CurrentThread.CurrentCulture =
                                Thread.CurrentThread.CurrentUICulture =
                                    CultureInfo.CreateSpecificCulture(userLocale);

                            //group notifications per session, each session should generate an email
                            var grouppedNotifPerSession = userNotification.Notifications.GroupBy(t => t.SessionId);

                            foreach (var session in grouppedNotifPerSession)
                            {
                                var sessionEmail = Utils.EmailServiceUtils.CreateDigestEmailByFrequency(NotificationFrequency.Frequency.PerProofStudioSession,
                                        userNotification.InternalUserRecipient.UserId, session.ToList(), _eventTypes,
                                        _emailTemplates, context);

                                if (sessionEmail != null)
                                {
                                    var Jsondata = new JObject();
                                    Jsondata.Add("subject", sessionEmail.subject);
                                    Jsondata.Add("htmlBody", sessionEmail.bodyHtml);
                                    Receiver receiverData = new Receiver();
                                    receiverData.JSONTemplateData = Jsondata.ToString();
                                    receiverData.ReceiverEmail = sessionEmail.toEmail;
                                    receivers.Add(receiverData);
                                }
                                if (emailSenderAddress == "")
                                {
                                    emailSenderAddress = sessionEmail.fromEmail;
                                }
                            }
                        }
                        if (receivers.Count > 0)
                        {
                            while (receivers.Count > 50)
                            {
                                var data = receivers.GetRange(0, 50);
                                SendBulkEmailUsingTemplate(data, emailSenderAddress);
                                receivers.RemoveRange(0, 50);
                            }
                            if (receivers.Count > 0 && receivers.Count < 50)
                            {
                                SendBulkEmailUsingTemplate(receivers, emailSenderAddress);
                            }
                        }

                        // Save emails in database to be send
                        Utils.EmailServiceUtils.SaveEmails(emails, context);

                        NotificationServiceBL.MarkAsDeletedCompletedPSSessions(userNotifications, context);

                        context.SaveChanges();
                    }
                }
            }
            catch (DbEntityValidationException valEx)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(valEx);
                ServiceLog.Log("StudioProof session email sender entity validation error: errormessage: " + errorMessage, valEx);
            }
            catch (Exception ex)
            {
                ServiceLog.Log("Daily/weekly email sender error: ", ex);
            }
        }

        public void SendBulkEmailUsingTemplate(List<Receiver> receiversAddress, string senderAddress)
        {
            string TemplateName = "SampleTemplate20";
            string TemplateDefaultData = "{ \"subject\":\"sample subject\", \"htmlBody\": \"sample html body\" }";
            List<BulkEmailDestination> destinations = new List<BulkEmailDestination>();
            foreach (var item in receiversAddress)
            {
                destinations.Add(new BulkEmailDestination
                {
                    Destination = new Destination
                    {
                        ToAddresses = new List<string> { item.ReceiverEmail }
                    },
                    ReplacementTemplateData = item.JSONTemplateData
                });
            }
            using (var client = new AmazonSimpleEmailServiceClient(new Amazon.Runtime.BasicAWSCredentials("AKIAR6KWPSIL75ELFDFO", "Y4H+zpWRnUfLWZCNGPT/AExRy43B22QMprjxFBMW"), RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendBulkTemplatedEmailRequest
                {
                    Source = senderAddress,
                    Template = TemplateName,
                    Destinations = destinations,
                    DefaultTemplateData = TemplateDefaultData
                };
                try
                {
                    var response = client.SendBulkTemplatedEmailAsync(sendRequest);
                    response.Wait();
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("Unable to send email error: ", ex);
                }
            }
            try
            {
                foreach (var item in receiversAddress)
                {
                    JObject jsonData = JObject.Parse(item.JSONTemplateData);
                    string subject = jsonData.GetValue("subject").ToString();
                    LogEmailSentInfo(item.ReceiverEmail, subject);
                }
            }
            catch (Exception e)
            {
                ServiceLog.Log("Unable to enter the sent emails info into the log table :", e);
            }
        }

        protected void LogEmailSentInfo(string emailTo, string emailSubject)
        {
            string message = String.Format("EmailSender: Sending the email to user {0} with subject {1}", emailTo, emailSubject);
            ServiceLog.Log(new LoggingNotification(message, Severity.Info));
        }
        #endregion 
    }
}
