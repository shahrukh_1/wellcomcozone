﻿using System;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.EmailService
{
    public partial class CoZoneEmailService : ServiceBase
    {       
        private EmailService _emailService;
        private Task _emailServiceTask;

        public CoZoneEmailService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _emailService = new EmailService();
            _emailServiceTask = new Task(_emailService.StartEmailService);
            _emailServiceTask.Start();          
        }

        protected override void OnStop()
        {
            _emailService.StopEmailService();
        }
    }
}
