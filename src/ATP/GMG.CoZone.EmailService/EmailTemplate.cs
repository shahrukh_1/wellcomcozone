﻿using GMG.CoZone.Common;
using GMGColorDAL.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace GMG.CoZone.EmailService
{
	public class EmailTemplate
    {
        #region Private Static Members
        
        private static string _annotationTemplate;
        private static string _planChangedTemplate;
        private static string _approvalTemplate;
        private static string _folderCreatedTemplate;
        private static string _genericTemplate;
        private static string _outOfOffice;
        private static string _welcomeTemplate;
        private static string _lostPasswordTemplate;
        private static string _resetPasswordTemplate;
        private static string _sharedWorkflowTemplate;
        private static string _newPairingCodeTemplate;
        private static string _approvalReminderTemplate;
        private static string _commonOuterTemplate;
        private static string _billingReportTemplate;
        private static string _digestAsEventOccur;
        private static string _digestDaily;
        private static string _digestWeekly;

        public const string _emailFolder = "EmailTemplates";
        #endregion

        #region Properties
        public static string AnnotationTemplate
        {
            get
            {
                if (String.IsNullOrEmpty(_annotationTemplate))
                {
                    _annotationTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\Common\\comment_is_made.html"));
                }
                return _annotationTemplate;
            }
        }

        public static string FolderCreatedTemplate
        {
            get
            {
                if (String.IsNullOrEmpty(_folderCreatedTemplate))
                {
                    _folderCreatedTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\Common\\new_folder_was_created.html"));
                }
                return _folderCreatedTemplate;
            }
        }

        public static string PlanChangedTemplate
        {
            get
            {
                if (String.IsNullOrEmpty(_planChangedTemplate))
                {
                    _planChangedTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\Common\\plan_was_changed.html"));
                }
                return _planChangedTemplate;
            }
        }

        public static string ApprovalTemplate
        {
            get
            {
                if (String.IsNullOrEmpty(_approvalTemplate))
                {
                    _approvalTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\Common\\common_approval_template.html"));
                }
                return _approvalTemplate;
            }
        }

        public static string OutOfOffice
        {
            get
            {
                if(string.IsNullOrEmpty(_outOfOffice))
                {
                    _outOfOffice = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\Common\\common_event_template.html"));
                }
                return _outOfOffice;
            }
        }

        public static string GenericTemplate
        {
            get
            {
                if (String.IsNullOrEmpty(_genericTemplate))
                {
                    _genericTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\Common\\common_event_template.html"));
                }
                return _genericTemplate;
            }
        }

        public static string CommonOuterTemplate
        {
            get
            {
                if (_commonOuterTemplate == null)
                {
                    _commonOuterTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("CommonOuterTemplate.html"));
                }

                return _commonOuterTemplate;
            }
        }

        public static string WelcomeTemplate
        {
            get
            {
                if (_welcomeTemplate == null)
                {
                    _welcomeTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("Welcome.html"));
                }
                return _welcomeTemplate;
            }
        }

        public static string LostPasswordTemplate
        {
            get
            {
                if (_lostPasswordTemplate == null)
                {
                    _lostPasswordTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("LostPassword.html"));
                }
                return _lostPasswordTemplate;
            }
        }

        public static string ResetPasswordTemplate
        {
            get
            {
                if (_resetPasswordTemplate == null)
                {
                    _resetPasswordTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("ResetPassword.html"));
                }
                return _resetPasswordTemplate;
            }
        }

        public static string BillingReportTemplate
        {
            get
            {
                if (_billingReportTemplate == null)
                {
                    _billingReportTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("ReportTemplate.html"));
                }
                return _billingReportTemplate;
            }
        }

        public static string SharedWorkflowTemplate
        {
            get
            {
                if (_sharedWorkflowTemplate == null)
                {
                    _sharedWorkflowTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("SharedCPWorkflow.html"));
                }
                return _sharedWorkflowTemplate;
            }
        }

        public static string NewPairingCodeTemplate
        {
            get
            {
                if (_newPairingCodeTemplate == null)
                {
                    _newPairingCodeTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("DeliverNewCPInstance.html"));
                }
                return _newPairingCodeTemplate;
            }
        }

        public static string ApprovalReminderTemplate
        {
            get
            {
                if (_approvalReminderTemplate == null)
                {
                    _approvalReminderTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("ApprovalReminder.html"));
                }
                return _approvalReminderTemplate;
            }
        }
        
        public static string DigestAsEventOccur
        {
            get
            {
                if (_digestAsEventOccur == null)
                {
                    _digestAsEventOccur = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\digest_AsEventOccur.html"));
                }
                return _digestAsEventOccur;
            }
        }

        public static string DigestDaily
        {
            get
            {
                if (_digestDaily == null)
                {
                    _digestDaily = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\digest_daily.html"));
                }
                return _digestDaily;
            }
        }

        public static string DigestWeekly
        {
            get
            {
                if (_digestWeekly == null)
                {
                    _digestWeekly = GMGColorCommon.ReadServerFile(GetTemplatePath("NotificationTemplates\\digest_weekly.html"));
                }
                return _digestWeekly;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get template based on the notification type
        /// </summary>
        /// <param name="notificationType"></param>
        /// <returns>Template content as string</returns>
        public static string GetNotificationTemplate(NotificationType notificationType)
        {
            switch (notificationType)
            {
                case NotificationType.ACommentIsMade:
                case NotificationType.RepliesToMyComments:
                case NotificationType.NewAnnotationAdded:
                case NotificationType.NewAnnotationAtUserInComment:
                case NotificationType.ApprovalStatusChanged:
                case NotificationType.ApprovalStatusChangedByPdm:
                case NotificationType.ApprovalChangesCompleted:
                case NotificationType.ApprovalJobCompleted:
                case NotificationType.ApprovalWasDeleted:
                case NotificationType.PhaseWasDeactivated:
                case NotificationType.PhaseWasActivated:
                    return AnnotationTemplate;
                case NotificationType.ApprovalWasDownloaded:
                case NotificationType.UserWasAdded:
                case NotificationType.GroupWasCreated:
                case NotificationType.AccountWasDeleted:
                case NotificationType.AccountWasAdded:
                case NotificationType.DeliverFileTimeOut:
                case NotificationType.WorkstationRegistered:
                case NotificationType.DeliverFileWasSubmitted:                
                case NotificationType.DeliverFileCompleted:
                case NotificationType.DeliverFileErrored:
                case NotificationType.SharedWorkflowAccessRevoked:
                case NotificationType.HostAdminUserDeleted:
                case NotificationType.InvitedUserDeleted:
                case NotificationType.HostAdminUserFileSubmitted:
                case NotificationType.InvitedUserFileSubmitted:
                case NotificationType.SharedWorkflowDeleted:
                case NotificationType.ShareRequestAccepted:
                case NotificationType.DemoPlanExpired:               
                case NotificationType.PlanLimitReached:
                case NotificationType.PlanLimitWarning:
                case NotificationType.ApprovalWasRejected:
                case NotificationType.NewProjectFolder:
                case NotificationType.ProjectFolderWasUpdated:
                case NotificationType.ProjectFolderWasShared:
                    return GenericTemplate;
                case NotificationType.ApprovalWasUpdated:
                case NotificationType.NewApprovalAdded:
                case NotificationType.NewVersionWasCreated:
                case NotificationType.ApprovalWasShared:
                case NotificationType.ApprovalOverdue:
                case NotificationType.WorkflowPhaseRerun:
                    return ApprovalTemplate;
                case NotificationType.PlanWasChanged:
                    return PlanChangedTemplate;
                case NotificationType.UserWelcome:
                    return WelcomeTemplate;
                case NotificationType.LostPassword:
                    return LostPasswordTemplate;
                case NotificationType.ResetPassword:
                    return ResetPasswordTemplate;
                case NotificationType.BillingReport:
                    return BillingReportTemplate;
                case NotificationType.SharedWorkflowInvitation:
                    return SharedWorkflowTemplate;
                case NotificationType.DeliverNewPairingCode:
                    return NewPairingCodeTemplate;
                case NotificationType.ApprovalReminder:
                    return ApprovalReminderTemplate;
                case NotificationType.NewFileTransferAdded:
                    return ApprovalTemplate;
                case NotificationType.FileTransferDownloaded:
                    return ApprovalTemplate;
                case NotificationType.OutOfOffice:
                    return OutOfOffice;
                case NotificationType.ChecklistItemEdited:
                    return AnnotationTemplate;
                default:
                    return String.Empty;
            }
        }

        /// <summary>
        /// Returns a dictionary of templates and notification types
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, List<string>> GetTemplateDictionary()
        {
            List<string> list;
            var templates = new Dictionary<string, List<string>>();
           
            foreach (var notificationType in Enum.GetNames(typeof(NotificationType)))
            {
                var template = GetNotificationTemplate((NotificationType)Enum.Parse(typeof(NotificationType), notificationType));
                if (!templates.TryGetValue(template, out list))
                {
                    list = new List<string>();
                    templates.Add(template, list);
                }
                list.Add(notificationType);
            }

            return templates;
        }

        #endregion

        #region Private Methods

        private static string GetTemplatePath(string templateRelativePath)
        {
            string workingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

            if (workingDirectory.StartsWith("file:\\"))
            {
                workingDirectory = workingDirectory.Replace("file:\\", String.Empty);
            }

            string profileAbsPath = Path.Combine(workingDirectory, _emailFolder, templateRelativePath);

            if (File.Exists(profileAbsPath))
            {
                return profileAbsPath;
            }

            return null;
        }

        #endregion
    }
}
