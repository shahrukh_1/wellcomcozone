﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace GMG.CoZone.EmailService
{
    [RunInstaller(true)]
    public partial class EmailServiceInstaller : System.Configuration.Install.Installer
    {
        public EmailServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
