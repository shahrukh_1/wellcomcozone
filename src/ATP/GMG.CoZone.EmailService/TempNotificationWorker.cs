﻿using GMGColorBusinessLogic;
using System.Threading;

namespace GMG.CoZone.EmailService
{
    class TempNotificationWorker
    {
        private CancellationTokenSource _cts;
        public TempNotificationWorker(CancellationTokenSource cts)
        {
            _cts = cts;
        }
        public void ProcessTempNotifications()
        {

            var notificationCreator = new TempNotificationReader();

            while (!_cts.Token.IsCancellationRequested)
            {
                try
                {
                    var tempNotificationItems = notificationCreator.ReadApplyFolderPermissionTempNotifications();
                    if (tempNotificationItems.Count > 0)
                    {
                        using (var context = new DbContextBL())
                        {
                            var notificationProcessor = new TempNotificationProcessor(context, tempNotificationItems);
                            notificationProcessor.StartProcessingTempNotifications();
                        }
                    }
                    Thread.Sleep(10 * 1000); // wait 2 seconds between syncs
                }
                catch (System.Exception ex)
                {
                    ServiceLog.Log("Process email queue failed: ", ex);
                }
            }

        }

        public void StopProcessTempNotifications()
        {
            _cts.Cancel();
        }
    }
}
