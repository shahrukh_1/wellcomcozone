﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Extensions;
using System.Linq;
using GMGColorNotificationService;
using System.Globalization;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon;
using Newtonsoft.Json.Linq;
using GMG.CoZone.Common.Interfaces;
using System.Net.Mail;

namespace GMG.CoZone.EmailService
{
	public class EmailService
    {
        #region Private Members

        private CancellationTokenSource _processNotificationQueueTaskCTS = new CancellationTokenSource();
        private CancellationTokenSource _sendEmailTaskCTS = new CancellationTokenSource();
        private CancellationTokenSource _startDailyWeeklyEmailWorkerCTS = new CancellationTokenSource();
        private CancellationTokenSource _startBulkScheduledEmailWorkerCTS = new CancellationTokenSource();
        private CancellationTokenSource _processTempNotificationItemsCTS = new CancellationTokenSource();
        private List<GMGColorDAL.EventType> _eventTypes;
        private System.Timers.Timer _dailyWeeklyEmailTimer;
        private System.Timers.Timer _bulkScheduledEmailTimer;
        private System.Timers.Timer _approvalRemindersEmailTimer;
        private Dictionary<string, List<string>> _emailTemplates;
        private ProofStudioSessionWorker _psSessionWorker;
        private Task _processNotificationQueueTask;
        private Task _sendEmailServiceTask;
        private TempNotificationWorker _notificationWorker;
        #endregion

        public class Receiver
        {
            public string ReceiverEmail { get; set; }
            public string JSONTemplateData { get; set; }
            public int? ApprovalId { get; set; }
            public int UserId { get; set; }
            public bool IsExternal { get; set; }
        }
        public EmailService()
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                GMGColor.AWS.AWSClient.Init(new GMGColor.AWS.AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }
        }

        /// <summary>
        /// Start Email Service 
        /// </summary>
        public void StartEmailService()
        {
            using (DbContextBL context = new DbContextBL())
            {
                _eventTypes = (from e in context.EventTypes select e).ToList();
            }

            _emailTemplates = EmailTemplate.GetTemplateDictionary();

            _notificationWorker = new TempNotificationWorker(_processTempNotificationItemsCTS);
            Task.Factory.StartNew(() =>
            {
                _notificationWorker.ProcessTempNotifications();
            }, _processTempNotificationItemsCTS.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);

            _processNotificationQueueTask = new Task(ProcessNotificationQueue);
            _processNotificationQueueTask.Start();

            _sendEmailServiceTask = new Task(StartEmailWorkers);
            _sendEmailServiceTask.Start();
        }


        /// <summary>
        /// Start daily/weekly email worker 
        /// </summary>
        /// <param name="numberOfEmails">Number of emails</param>
        public void StartEmailWorkers()
        {
            try
            {
                ServiceLog.Log(new LoggingNotification("Send Email Queue worker started", Severity.Info));

                Task startDailyWeeklyEmailTask = new Task(StartDailyWeeklyEmailWorkerTask);
                startDailyWeeklyEmailTask.Start();

                Task startBulkScheduledEmailTask = new Task(StartBulkScheduledEmailTask);
                startBulkScheduledEmailTask.Start();

                //Task handles emails by ProofStudio session
                _psSessionWorker = new ProofStudioSessionWorker(_eventTypes, _emailTemplates);
                Task startProofStudioSessionEmailTask = new Task(_psSessionWorker.StartSessionWorker);
                startProofStudioSessionEmailTask.Start();
            }
            catch (Exception e)
            {
                ServiceLog.Log("SendEmailWorker fatal exception: ", new LoggingException("Start SendEmailWorker instance failed", e, Severity.Fatal));
            }
        }

        /// <summary>
        /// Start email sender worker
        /// </summary>
        public void ProcessNotificationQueue()
        {
            ServiceLog.Log(new LoggingNotification("Process Notification Queue worker started", Severity.Info));
            while (!_processNotificationQueueTaskCTS.Token.IsCancellationRequested)
            {
                try
                {
                    ProcessEmailQueue();

                    Thread.Sleep(20 * 1000); // wait 20 seconds between syncs

                }
                catch (Exception e)
                {
                    ServiceLog.Log("ProcessNotificationQueue fatal exception: ", new LoggingException("Start ProcessNotificationQueue instance failed", e, Severity.Fatal));
                }
            }
        }

        public void StopEmailService()
        {
            StopProcessNotificationQueue();
            StopSendEmailWorker();

            _notificationWorker.StopProcessTempNotifications();
        }

        /// <summary>
        /// Stops processing notification queue worker
        /// </summary>
        public void StopProcessNotificationQueue()
        {
            _processNotificationQueueTaskCTS.Cancel();
        }

        /// <summary>
        /// Stops email sender worker
        /// </summary>
        public void StopSendEmailWorker()
        {
            _sendEmailTaskCTS.Cancel();
            _startDailyWeeklyEmailWorkerCTS.Cancel();
            _startBulkScheduledEmailWorkerCTS.Cancel();
            if (_psSessionWorker != null)
            {
                _psSessionWorker.StopSessionWorker();
            }
        }

        /// <summary>
        /// Process existing database notifications
        /// </summary>
        private void ProcessEmailQueue()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    context.Configuration.AutoDetectChangesEnabled = true;

                    Utils.EmailServiceUtils.SaveEmails(ProcessInstantEmails(context), context);

                    context.SaveChanges();
                }
            }
            catch (DbEntityValidationException dex)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(dex);
                ServiceLog.Log(new LoggingNotification("SaveEmails method failed Exception: " + errorMessage, Severity.Error));
            }
            catch (Exception e)
            {
                ServiceLog.Log("Process email queue failed: ", e);
            }
        }

        /// <summary>
        /// Processes the internal users emails
        /// </summary>
        /// <param name="context"></param>
        private List<Email> ProcessInstantEmails(DbContextBL context)
        {
                List<Receiver> receivers = new List<Receiver>();
                List<Email> emails = new List<Email>();
                string emailSenderAddress = string.Empty;
                List<int> ListOfOutOfOfficeUsers = (from o in context.OutOfOffices where o.StartDate < DateTime.UtcNow && o.EndDate > DateTime.UtcNow select o.Owner ).ToList();
                List<int> ListOfRestrictedEventTypes = new List<int>{ 5 , 10, 11, 34, 38,39,57}; 
                List<UserNotifications> usersNotifications = NotificationServiceBL.GetInstantNotificationsToPprocess(context);
                foreach (UserNotifications userNotification in usersNotifications)
                {
                    //if user was deleted, remove all the emails related with him
                    if (userNotification.InternalUserRecipient != null && userNotification.InternalUserRecipient.UserStatusKey == "D")
                    {
                        ServiceLog.Log(new LoggingNotification(String.Format("EmailService (ProcessInstantEmails). Remove deleted user notifications:  {0}", userNotification.InternalUserRecipient), Severity.Info));
                        foreach (UserNotification notificationItem in userNotification.Notifications)
                        {
                            context.NotificationItems.Remove(notificationItem.NotificationItem);
                        }

                        continue;
                    }

                    List<Notification> userNotificationsToSend = new List<Notification>();

                    foreach (UserNotification notificationItem in userNotification.Notifications)
                    {
                        if (userNotification.InternalUserRecipient != null &&
                            (userNotification.InternalUserRecipient.NotificationFrequencyKey == "NVR" && !notificationItem.IsFromGlobalSchedule) &&
                            notificationItem.EventGroupKey != "IE" && notificationItem.NotificationItem.NotificationType != (int)Common.NotificationType.ChecklistItemEdited + 7)
                        {
                            //Remove notifications for users with never frequency from database that are not contained in a global notificaton schedule 
                            context.NotificationItems.Remove(notificationItem.NotificationItem);

                            string message = String.Format("EmailService: Remove notification item for user {0} with message {1}", userNotification.InternalUserRecipient, notificationItem.NotificationItem);
                            ServiceLog.Log(new LoggingNotification(message, Severity.Info));
                        }
                        else
                        {
                            try
                            {
                                // Get the notification type
                                Common.NotificationType? notificationType = NotificationTypeParser.ParseString(
                                    _eventTypes.FirstOrDefault(
                                        e => e.ID == notificationItem.NotificationItem.NotificationType).Key);

                                // Create notification item
                                Notification notification =
                                    NotificationServiceBL.CreateNotification(notificationItem.NotificationItem,
                                                                                notificationType.Value);


                                //check if email is ready to be sent
                                if (!Utils.EmailServiceUtils.IsEmailReadyToBeSent(notification, context))
                                {
                                    continue;
                                }

                                userNotificationsToSend.Add(notification);
                                try
                                {
                                    ServiceLog.Log(new LoggingNotification(String.Format("EmailService: Notification Id {0} with InternalRecepient {1} and NotificationType '{2}' for Approval: {3}", notificationItem.NotificationItem.ID, notificationItem.NotificationItem.InternalRecipient, notification.NotificationType.ToString(), ((GMGColorNotificationService.ApprovalNotification)notification).ApprovalsIds[0]), Severity.Info));
                                }
                                catch { }
                                // Mark the notification item for deletion
                                context.NotificationItems.Remove(notificationItem.NotificationItem);
                            }
                            catch (Exception ex)
                            {
                                ServiceLog.Log("ProcessInternalUsersEmails: Load Notification exception for NotificationItem ID " + notificationItem.NotificationItem.ID,
                                                new LoggingException("ProcessEmailQueue failed", ex, Severity.Error));
                            }
                        }
                    }

                    // Create email(s) from notification(s) and send them to recipient
                    foreach (var notification in userNotificationsToSend)
                    {
                        try
                        {
                            int s = (int)notification.NotificationType;
                            //set current notification thread culture
                            Thread.CurrentThread.CurrentCulture =
                            Thread.CurrentThread.CurrentUICulture =
                            CultureInfo.CreateSpecificCulture(notification.GetLocale(context));

                            // as event occur
                            string template = EmailTemplate.GetNotificationTemplate(notification.NotificationType);
                            Email email = notification.NotificationType == Common.NotificationType.NewAnnotationAtUserInComment ?
                                 notification.GetNotificationEmail(template, EmailTemplate.DigestAsEventOccur, EmailTemplate.CommonOuterTemplate, context, true)
                                : notification.GetNotificationEmail(template, EmailTemplate.DigestAsEventOccur, EmailTemplate.CommonOuterTemplate, context, false);

                            if (email != null)
                            {
                                email.subject = TrimSubjectLengthAbove255Characters(email.subject);
                            // Local - stores in Email table
                            if (!GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                                {
                                emails.Add(email);

                                if (notification.InternalRecipient != null ? (ListOfOutOfOfficeUsers.Contains(notification.InternalRecipient.Value) && !ListOfRestrictedEventTypes.Contains(s)) : false) 
                                        {
                                            var SubstituteUser = (from u in context.Users
                                                                   join o in context.OutOfOffices on u.ID equals o.SubstituteUser
                                                                   where o.Owner == notification.InternalRecipient.Value &&
                                                                   o.StartDate < DateTime.UtcNow && o.EndDate > DateTime.UtcNow
                                                                   select new { u.EmailAddress, u.GivenName, u.FamilyName }).FirstOrDefault();

                                            if (SubstituteUser != null)
                                            {
                                            Email SubstituteUsersEmail = notification.NotificationType == Common.NotificationType.NewAnnotationAtUserInComment ?
                                             notification.GetNotificationEmail(template, EmailTemplate.DigestAsEventOccur, EmailTemplate.CommonOuterTemplate, context, true)
                                            : notification.GetNotificationEmail(template, EmailTemplate.DigestAsEventOccur, EmailTemplate.CommonOuterTemplate, context, false);

                                             SubstituteUsersEmail.subject = email.subject + "( Out Of Office - " + SubstituteUser.GivenName + " " + SubstituteUser.FamilyName + " )";
                                             SubstituteUsersEmail.toEmail = SubstituteUser.EmailAddress;
                                             SubstituteUsersEmail.toName = SubstituteUser.GivenName + " " + SubstituteUser.FamilyName;
                                             emails.Add(SubstituteUsersEmail);
                                            }
                                        }
                                }

                                var Jsondata = new JObject();
                                Jsondata.Add("subject", email.subject);
                                Jsondata.Add("htmlBody", email.bodyHtml);
                                Receiver receiverData = new Receiver();
                                receiverData.JSONTemplateData = Jsondata.ToString();
                                receiverData.ReceiverEmail = email.toEmail;
                                receiverData.ApprovalId = notification.ApprovalID;
                                receiverData.IsExternal = notification.InternalRecipient == null;
                                receiverData.UserId = notification.InternalRecipient != null ? notification.InternalRecipient.Value : notification.ExternalRecipient != null ? notification.ExternalRecipient.Value : 0 ;
                                receivers.Add(receiverData);

                            // for AWS Email service
                            if (notification.InternalRecipient != null ? (ListOfOutOfOfficeUsers.Contains(notification.InternalRecipient.Value) && !ListOfRestrictedEventTypes.Contains(s)) : false)
                            {
                                var SubstituteUser = (from u in context.Users
                                                       join o in context.OutOfOffices on u.ID equals o.SubstituteUser
                                                       where o.Owner == notification.InternalRecipient.Value &&
                                                       o.StartDate < DateTime.UtcNow && o.EndDate > DateTime.UtcNow
                                                       select new { u.ID , u.EmailAddress, u.GivenName, u.FamilyName }).FirstOrDefault();

                                    if (SubstituteUser != null)
                                    {
                                    Email SubstituteUsersEmail = notification.NotificationType == Common.NotificationType.NewAnnotationAtUserInComment ?
                                             notification.GetNotificationEmail(template, EmailTemplate.DigestAsEventOccur, EmailTemplate.CommonOuterTemplate, context, true)
                                            : notification.GetNotificationEmail(template, EmailTemplate.DigestAsEventOccur, EmailTemplate.CommonOuterTemplate, context, false);


                                    var Json_data = new JObject();
                                    Json_data.Add("subject", SubstituteUsersEmail.subject + " (Out of Office - " + SubstituteUser.GivenName +"  " + SubstituteUser.FamilyName + ")");
                                    Json_data.Add("htmlBody", SubstituteUsersEmail.bodyHtml);
                                    Receiver receiver_Data = new Receiver();
                                    receiver_Data.JSONTemplateData = Json_data.ToString();
                                    receiver_Data.ReceiverEmail = SubstituteUser.EmailAddress;
                                    receiver_Data.ApprovalId = notification.ApprovalID;
                                    receiver_Data.IsExternal = false;
                                    receiver_Data.UserId = notification.InternalRecipient != null ? SubstituteUser.ID : 0;
                                    receivers.Add(receiver_Data);
                                    }
                                }
                            }
                            if (emailSenderAddress == "")
                            {
                                emailSenderAddress = email.fromEmail;
                            }
                        }
                        catch (Exception ex)
                        {
                            ServiceLog.Log("ProcessInternalUsersEmails: Create Email exception: ", new LoggingException("ProcessEmailQueue failed", ex, Severity.Error));
                        }
                    }
                }
            // cannot send more than 50 emails on single thread
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                if (receivers.Count > 0)
                {
                    while (receivers.Count > 50)
                    {
                        var data = receivers.GetRange(0, 50);
                        SendBulkEmailUsingTemplate(data, emailSenderAddress, context);
                        receivers.RemoveRange(0, 50);
                    }
                    if (receivers.Count > 0 && receivers.Count < 50)
                    {
                        SendBulkEmailUsingTemplate(receivers, emailSenderAddress, context);
                    }
                }
                if (emails.Count > 0)
                {
                    ServiceLog.Log(new LoggingNotification(String.Format("Total emails to send: {0}", emails.Count), Severity.Info));
                }
            }
            return emails;
        }

        //creation of new template
        public static void CreateTemplate1()
        {
            string TemplateName = "SampleTemplate20";
            string Subject = "{{subject}}";
            string Html = "{{htmlBody}}";
            string Text = string.Empty;
            using (var client = new AmazonSimpleEmailServiceClient(new Amazon.Runtime.BasicAWSCredentials("AKIAR6KWPSIL75ELFDFO", "Y4H+zpWRnUfLWZCNGPT/AExRy43B22QMprjxFBMW"), RegionEndpoint.EUWest1))
            {
                var Request = new CreateTemplateRequest
                {
                    Template = new Template
                    {
                        TemplateName = TemplateName,
                        SubjectPart = Subject,
                        TextPart = Text,
                        HtmlPart = Html
                    }
                };
                try
                {
                    var response = client.CreateTemplateAsync(Request);
                    response.Wait();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        // sending bulk emails
        public void SendBulkEmailUsingTemplate(List<Receiver> receiversAddress, string senderAddress, DbContextBL context)
        {
            string TemplateName = "SampleTemplate20";
            string TemplateDefaultData = "{ \"subject\":\"sample subject\", \"htmlBody\": \"sample html body\" }";
            List<BulkEmailDestination> destinations = new List<BulkEmailDestination>();
            foreach (var item in receiversAddress)
            {
                destinations.Add(new BulkEmailDestination
                {
                    Destination = new Destination
                    {
                        ToAddresses = new List<string> { item.ReceiverEmail }
                    },
                    ReplacementTemplateData = item.JSONTemplateData
                });
            }
            using (var client = new AmazonSimpleEmailServiceClient(new Amazon.Runtime.BasicAWSCredentials("AKIAR6KWPSIL75ELFDFO", "Y4H+zpWRnUfLWZCNGPT/AExRy43B22QMprjxFBMW"), RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendBulkTemplatedEmailRequest
                {
                    Source = senderAddress,
                    Template = TemplateName,
                    Destinations = destinations,
                    DefaultTemplateData = TemplateDefaultData
                };
                try
                {
                    var response = client.SendBulkTemplatedEmailAsync(sendRequest);
                    response.Wait();
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("Unable to send email error: ", ex);
                }
            }
            try
            {
                foreach (var item in receiversAddress)
                {
                    JObject jsonData = JObject.Parse(item.JSONTemplateData);
                    string subject = jsonData.GetValue("subject").ToString();
                    LogEmailSentInfo(item.ReceiverEmail, subject, item.ApprovalId);
                    if((item.ApprovalId != null && !item.IsExternal) ? (item.ApprovalId > 0 && item.UserId > 0) : false)
                    {
                        var ObjapprovalCollaborate = (from ac in context.ApprovalCollaborators
                                                      join a in context.Approvals on ac.Approval equals a.ID
                                                      where ac.Collaborator == item.UserId
                                                      && ac.Approval == item.ApprovalId && a.CurrentPhase == ac.Phase
                                                      select ac).FirstOrDefault();
                        if(ObjapprovalCollaborate != null)
                        {
                            ObjapprovalCollaborate.IsReminderMailSent = true;
                            ObjapprovalCollaborate.MailSentDate = DateTime.UtcNow;
                        }
                    }
                    if ((item.ApprovalId != null && item.IsExternal) ? (item.ApprovalId > 0 && item.UserId > 0) : false)
                    {
                        var ObjsharedApproval = (from sa in context.SharedApprovals
                                                  join a in context.Approvals on sa.Approval equals a.ID
                                                  where sa.ExternalCollaborator == item.UserId
                                                      && sa.Approval == item.ApprovalId && a.CurrentPhase == sa.Phase
                                                  select sa).FirstOrDefault();
                        if (ObjsharedApproval != null)
                        {
                            ObjsharedApproval.IsReminderMailSent = true;
                            ObjsharedApproval.MailSentDate = DateTime.UtcNow;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLog.Log("Unable to enter the sent emails info into the log table :", e);
            }
        }

        protected void LogEmailSentInfo(string emailTo, string emailSubject, int? ApprovalId)
        {
            string message = String.Format("EmailSender: Sending the email to user {0} with subject {1} for Approval {2}", emailTo, emailSubject, ApprovalId);
            ServiceLog.Log(new LoggingNotification(message, Severity.Info));
        }

        //public static void SendEmailUsingTemplate(string JsonData)
        //{
        //    string TemplateName = "SampleTemplate20";
        //    using (var client = new AmazonSimpleEmailServiceClient((new Amazon.Runtime.BasicAWSCredentials(GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey)), RegionEndpoint.APSoutheast1))
        //    {
        //        var sendRequest = new SendTemplatedEmailRequest
        //        {
        //            Source = senderAddress,
        //            Template = TemplateName,
        //            Destination = new Destination
        //            {
        //                ToAddresses =
        //                new List<string> { receiverAddress }
        //            },
        //            TemplateData = JsonData
        //        };
        //        try
        //        {
        //            var response = client.SendTemplatedEmailAsync(sendRequest);
        //            response.Wait();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}


        /// <summary>
        /// Start the task that handles daily and weekly emails
        /// </summary>
        private void StartDailyWeeklyEmailWorkerTask()
        {
            DateTime currentDate = DateTime.UtcNow;
            DateTime starTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, GMGColorConfiguration.AppConfiguration.UTCHourForSendingDailyWeeklyEmails, 0, 0);
            starTime = starTime.ToUniversalTime();

            // Run the daily/weekly task every day/week after 7AM UTC time
            while ((Math.Abs(currentDate.Subtract(starTime).TotalMinutes) > 10) && !_startDailyWeeklyEmailWorkerCTS.IsCancellationRequested) // check if the difference between current and the start time is bigger then 10 minutes
            {
                Thread.Sleep(3 * 60000); // wait for 3 minutes

                // Update time to current
                currentDate = DateTime.UtcNow;
                starTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, GMGColorConfiguration.AppConfiguration.UTCHourForSendingDailyWeeklyEmails, 0, 0);
                starTime = starTime.ToUniversalTime();
            }
            TimeSpan interval = new TimeSpan(1, 0, 0, 0); // 24 hours timespan

            _dailyWeeklyEmailTimer = new System.Timers.Timer(interval.TotalMilliseconds);
            _dailyWeeklyEmailTimer.Elapsed += DailyWeeklyEmailSender;
            _dailyWeeklyEmailTimer.Enabled = true;

            ServiceLog.Log(new LoggingNotification("Send Daily/Weekly Email started", Severity.Info));
            DailyWeeklyEmailSender(null, null);
        }

        /// <summary>
        /// Start the task that handles account notifications emails
        /// </summary>
        private void StartBulkScheduledEmailTask()
        {
            // start the bulk email worker thread
            while (DateTime.UtcNow.Minute > 1 && !_startBulkScheduledEmailWorkerCTS.IsCancellationRequested)
            {
                Thread.Sleep(300);
            }

            //Scheduled Emails Timer
            TimeSpan bulkEmailsinterval = new TimeSpan(0, 1, 0, 0); // 1 hour timespan

            _bulkScheduledEmailTimer = new System.Timers.Timer(bulkEmailsinterval.TotalMilliseconds);
            _bulkScheduledEmailTimer.Elapsed += BulkEmailSender;
            _bulkScheduledEmailTimer.Enabled = true;

            ServiceLog.Log(new LoggingNotification("Send bulk scheduled Email started", Severity.Info));

            //Approval Reminders Timer
            TimeSpan approvalRemindersInterval = new TimeSpan(0, 0, 30, 0); // 30 minutes timespan

            _approvalRemindersEmailTimer = new System.Timers.Timer(approvalRemindersInterval.TotalMilliseconds);
            _approvalRemindersEmailTimer.Elapsed += ApprovalRemindersSender;
            _approvalRemindersEmailTimer.Enabled = true;

            ServiceLog.Log(new LoggingNotification("Send approval reminders Email started", Severity.Info));

            BulkEmailSender(null, null);
            ApprovalRemindersSender(null, null);
        }

        /// <summary>
        /// Handle daily and weekly emails
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void DailyWeeklyEmailSender(Object source, ElapsedEventArgs e)
        {
            try
            {
                List<UserNotifications> usersNotifications;
                List<Email> emails = new List<Email>();
                List<Receiver> receivers = new List<Receiver>();
                string emailSenderAddress = string.Empty;
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    using (var context = new DbContextBL())
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        usersNotifications = NotificationServiceBL.GetDailyWeeklyNotificationsToProcess(context);

                        foreach (UserNotifications userNotification in usersNotifications)
                        {
                            //if user was deleted, remove all the emails related with him
                            if (userNotification.InternalUserRecipient.UserStatusKey == "D" || userNotification.InternalUserRecipient.AccountStatusKey != "A")
                            {
                                foreach (UserNotification notificationItem in userNotification.Notifications)
                                {
                                    context.NotificationItems.Remove(notificationItem.NotificationItem);
                                }

                                continue;
                            }

                            //set thread culture for current user
                            string userLocale = UserBL.GetUserLocale(userNotification.InternalUserRecipient.UserId, context);

                            Thread.CurrentThread.CurrentCulture =
                            Thread.CurrentThread.CurrentUICulture =
                            CultureInfo.CreateSpecificCulture(userLocale);

                            #region Daily Notifications

                            //get daily notifications for current user
                            List<UserNotification> userDailyNotifications =
                                userNotification.Notifications.Where(
                                    t =>
                                        t.NotificationPresetFrecvencyKey ==
                                        (int)CoZone.Common.NotificationPresetFrequency.EveryDay).ToList();

                            if (userDailyNotifications.Any())
                            {
                                ServiceLog.Log(new LoggingNotification(String.Format(
                                    "EmailService. Found daily notifications: {0} for user {1}", userDailyNotifications.Count, userNotification.InternalUserRecipient),
                                    Severity.Info));

                                Email dailyEmail = Utils.EmailServiceUtils.CreateDigestEmailByFrequency(NotificationFrequency.Frequency.Daily,
                                    userNotification.InternalUserRecipient.UserId, userDailyNotifications, _eventTypes, _emailTemplates, context);

                                if (dailyEmail != null)
                                {
                                    var Jsondata = new JObject();
                                    Jsondata.Add("subject", dailyEmail.subject);
                                    Jsondata.Add("htmlBody", dailyEmail.bodyHtml);
                                    Receiver receiverData = new Receiver();
                                    receiverData.JSONTemplateData = Jsondata.ToString();
                                    receiverData.ReceiverEmail = dailyEmail.toEmail;
                                    receivers.Add(receiverData);
                                }
                                if (emailSenderAddress == "")
                                {
                                    emailSenderAddress = dailyEmail.fromEmail;
                                }
                            }

                            #endregion

                            #region Weekly Notifications

                            DayOfWeek dayOfWeek =
                                (DayOfWeek)
                                    Enum.Parse(typeof(DayOfWeek),
                                        GMGColorConfiguration.AppConfiguration.DayOfWeekForrWeeklyEmails);

                            if (DateTime.UtcNow.DayOfWeek != dayOfWeek)
                            {
                                //skip user if it is not the day for weekly emails
                                continue;
                            }

                            //get weekly notifications for the current user
                            List<UserNotification> userWeeklyNotifications =
                                userNotification.Notifications.Where(
                                    t =>
                                        t.NotificationPresetFrecvencyKey ==
                                        (int)CoZone.Common.NotificationPresetFrequency.EveryWeek).ToList();

                            // if any weekly notification for current user
                            if (userWeeklyNotifications.Any())
                            {
                                ServiceLog.Log(new LoggingNotification(
                                    "EmailService. Found weekly notifications:  " + userWeeklyNotifications.Count,
                                    Severity.Info));

                                Email weeklyEmail = Utils.EmailServiceUtils.CreateDigestEmailByFrequency(NotificationFrequency.Frequency.Weekly,
                                     userNotification.InternalUserRecipient.UserId, userWeeklyNotifications, _eventTypes, _emailTemplates, context);

                                if (weeklyEmail != null)
                                {
                                    weeklyEmail.subject = TrimSubjectLengthAbove255Characters(weeklyEmail.subject);
                                    var Jsondata = new JObject();
                                    Jsondata.Add("subject", weeklyEmail.subject);
                                    Jsondata.Add("htmlBody", weeklyEmail.bodyHtml);
                                    Receiver receiverData = new Receiver();
                                    receiverData.JSONTemplateData = Jsondata.ToString();
                                    receiverData.ReceiverEmail = weeklyEmail.toEmail;
                                    receivers.Add(receiverData);
                                }
                                if (emailSenderAddress == "")
                                {
                                    emailSenderAddress = weeklyEmail.fromEmail;
                                }
                            }

                            #endregion
                        }
                        if (receivers.Count > 0)
                        {
                            while (receivers.Count > 50)
                            {
                                var data = receivers.GetRange(0, 50);
                                SendBulkEmailUsingTemplate(data, emailSenderAddress, context);
                                receivers.RemoveRange(0, 50);
                            }
                            if (receivers.Count > 0 && receivers.Count < 50)
                            {
                                SendBulkEmailUsingTemplate(receivers, emailSenderAddress, context);
                            }
                        }

                        //Save emails in database to be send
                        Utils.EmailServiceUtils.SaveEmails(emails, context);

                        context.SaveChanges();
                    }
                }
            }
            catch (DbEntityValidationException valEx)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(valEx);
                ServiceLog.Log("Daily/Weekly email sender entity validation error: errormessage: " + errorMessage, valEx);
            }
            catch (Exception ex)
            {
                ServiceLog.Log("Daily/weekly email sender error: ", ex);
            }
        }

        /// <summary>
        /// Handle bulk scheduled emails
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void BulkEmailSender(Object source, ElapsedEventArgs e)
        {
            try
            {
                List<UserNotifications> usersNotifications;
                List<Email> emails = new List<Email>();
                List<Receiver> receivers = new List<Receiver>();
                string emailSenderAddress = string.Empty;
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    using (var context = new DbContextBL())
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        usersNotifications = NotificationServiceBL.GetScheduledNotificationsToProcess(context);

                        if (usersNotifications.Any())
                        {
                            ServiceLog.Log(new LoggingNotification(
                                   "EmailService. BulkEmailSender. Found users notifications:  " + usersNotifications.Count,
                                   Severity.Info));
                        }

                        foreach (UserNotifications userNotification in usersNotifications)
                        {
                            //if user was deleted, remove all the emails related with him
                            if (userNotification.InternalUserRecipient.UserStatusKey == "D" || userNotification.InternalUserRecipient.AccountStatusKey != "A")
                            {
                                foreach (UserNotification notificationItem in userNotification.Notifications)
                                {
                                    context.NotificationItems.Remove(notificationItem.NotificationItem);
                                }

                                continue;
                            }

                            List<NotificationGroup> userNotificationsToSend = new List<NotificationGroup>();

                            //group user notifications by schedule frequency
                            var notificationsGroupedBySchedule = userNotification.Notifications.GroupBy(g => g.NotificationPresetFrecvencyKey).Select(n => n.ToList()).ToList();

                            foreach (var groupNotifications in notificationsGroupedBySchedule)
                            {
                                var notifPreset = groupNotifications.Select(f => f.NotificationPresetFrecvencyKey).FirstOrDefault();

                                //check if is the right time to send notification, else skip it
                                if (!NotificationServiceBL.IsScheduledNotificationTimeToSend(
                                    userNotification.InternalUserRecipient.UserId,
                                    notifPreset,
                                    context))
                                {
                                    continue;
                                }

                                foreach (UserNotification notificationItem in groupNotifications)
                                {
                                    try
                                    {
                                        // Get the notification type
                                        Common.NotificationType? notificationType = NotificationTypeParser.ParseString(_eventTypes.FirstOrDefault(
                                                et => et.ID == notificationItem.NotificationItem.NotificationType).Key);

                                        // Create notification item
                                        Notification notification =
                                            NotificationServiceBL.CreateNotification(notificationItem.NotificationItem,
                                                                                     notificationType.Value);

                                        //check if email is ready to be sent
                                        if (!Utils.EmailServiceUtils.IsEmailReadyToBeSent(notification, context))
                                        {
                                            continue;
                                        }

                                        userNotificationsToSend.Add(new NotificationGroup()
                                        {
                                            Notifications = notification,
                                            EventGroupKey = notificationItem.EventGroupKey,
                                            NotificationItem = notificationItem.NotificationItem
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                        ServiceLog.Log("BulkEmailSender: Load Notification exception for notificationItem ID: " + notificationItem.NotificationItem.ID,
                                                       new LoggingException("BulkEmailSender failed", ex, Severity.Error));
                                    }
                                }
                            }

                            if (userNotificationsToSend.Count > 0)
                            {
                                //set thread culture for current user
                                string userLocale = UserBL.GetUserLocale(userNotification.InternalUserRecipient.UserId, context);
                                Thread.CurrentThread.CurrentCulture =
                                Thread.CurrentThread.CurrentUICulture =
                                CultureInfo.CreateSpecificCulture(userLocale);

                                try
                                {
                                    //Get template (use weekly digest template for scheduled email items)
                                    var template = EmailTemplate.CommonOuterTemplate.Replace("<$template_content$>",
                                        EmailTemplate.DigestWeekly);

                                    var email = EmailUtils.CreateUserScheduleEmail(userNotificationsToSend, _emailTemplates, userNotification.InternalUserRecipient.UserId, template, context);
                                    if (email != null)
                                    {
                                        email.subject = TrimSubjectLengthAbove255Characters(email.subject);
                                        //Send scheduled email for user
                                        var Jsondata = new JObject();
                                        Jsondata.Add("subject", email.subject);
                                        Jsondata.Add("htmlBody", email.bodyHtml);
                                        Receiver receiverData = new Receiver();
                                        receiverData.JSONTemplateData = Jsondata.ToString();
                                        receiverData.ReceiverEmail = email.toEmail;
                                        receivers.Add(receiverData);

                                        //group user notifications by event type
                                        var uniqueUserEvents = userNotificationsToSend.GroupBy(et => et.NotificationItem.EventType)
                                                              .Select(i => i.OrderByDescending(d => d.NotificationItem.CreatedDate).FirstOrDefault())
                                                              .ToList();

                                        //update log table emails last sent date
                                        uniqueUserEvents.ForEach(t => NotificationServiceBL.MarkUserScheduleEventAsSent(userNotification.InternalUserRecipient.UserId, t.NotificationItem.EventType.ID, context));
                                    }
                                    if (emailSenderAddress == "")
                                    {
                                        emailSenderAddress = email.fromEmail;
                                    }

                                    //mark items to be deleted from database if email is created with success
                                    context.NotificationItems.RemoveRange(
                                        userNotificationsToSend.Select(t => t.NotificationItem));

                                    ServiceLog.Log(
                                        new LoggingNotification(
                                            String.Format("Bulk emails sent on: {0} for user {1}",
                                                DateTime.UtcNow, userNotification.InternalUserRecipient.UserId),
                                            Severity.Info));
                                }
                                catch (Exception ex)
                                {
                                    ServiceLog.Log(
                                        String.Format("BulkEmailSender: SendBulk email for user {0} failed",
                                            userNotification.InternalUserRecipient.UserId),
                                        new LoggingException("BulkEmailSender failed", ex, Severity.Error));
                                }
                            }
                        }
                        if (receivers.Count > 0)
                        {
                            while (receivers.Count > 50)
                            {
                                var data = receivers.GetRange(0, 50);
                                SendBulkEmailUsingTemplate(data, emailSenderAddress, context);
                                receivers.RemoveRange(0, 50);
                            }
                            if (receivers.Count > 0 && receivers.Count < 50)
                            {
                                SendBulkEmailUsingTemplate(receivers, emailSenderAddress, context);
                            }
                        }

                        //Save emails in database to be send
                        Utils.EmailServiceUtils.SaveEmails(emails, context);

                        context.SaveChanges();
                    }
                }
            }
            catch (DbEntityValidationException valEx)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(valEx);
                ServiceLog.Log("Bulk email sender entity validation error: errormessage: " + errorMessage, valEx);
            }
            catch (Exception ex)
            {
                ServiceLog.Log("Bulk email sender error: ", ex);
            }
        }

        /// <summary>
        /// Handles approval reminders
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void ApprovalRemindersSender(Object source, ElapsedEventArgs e)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                List<Email> emails = new List<Email>();
                List<Receiver> receivers = new List<Receiver>();
                string emailSenderAddress = string.Empty;
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    using (var context = new DbContextBL())
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        var collaborateRemindersNotifications = NotificationServiceBL.GetCollaborateRemindersNotifications(context);

                        foreach (var remindersNotification in collaborateRemindersNotifications)
                        {
                            //set thread culture for current user
                            string userLocale = UserBL.GetUserLocale(remindersNotification.InternalUserRecipient, context);
                            Thread.CurrentThread.CurrentCulture =
                            Thread.CurrentThread.CurrentUICulture =
                            CultureInfo.CreateSpecificCulture(userLocale);

                            var userRemindersToSend = new List<ApprovalReminder>();
                            foreach (var notificationsSchedule in remindersNotification.NotificationsSchedules)
                            {
                                for (int i = remindersNotification.Approvals.Count - 1; i >= 0; i--)
                                {
                                    //check if is the right time to send notification, else skip it
                                    if (!NotificationServiceBL.IsApprovalReminderTimeToSend(
                                            remindersNotification.InternalUserRecipient,
                                            remindersNotification.Approvals[i],
                                            notificationsSchedule, context))
                                    {
                                        continue;
                                    }

                                    //update log table emails last sent date
                                    var notification = new ApprovalReminder()
                                    {
                                        CreatedDate = DateTime.UtcNow,
                                        InternalRecipient = remindersNotification.InternalUserRecipient,
                                        ApprovalId = remindersNotification.Approvals[i].ID
                                    };

                                    userRemindersToSend.Add(notification);
                                    remindersNotification.Approvals.RemoveAt(i);
                                }
                            }

                            if (!userRemindersToSend.Any())
                            {
                                continue;
                            }
                            //Get template
                            Email email = EmailUtils.CreateApprovalReminderEmail(userRemindersToSend, EmailTemplate.CommonOuterTemplate, remindersNotification.InternalUserRecipient,
                                                                              EmailTemplate.ApprovalReminderTemplate, EmailTemplate.DigestAsEventOccur, context);
                            if (email != null)
                            {
                                var Jsondata = new JObject();
                                email.subject = TrimSubjectLengthAbove255Characters(email.subject);
                                Jsondata.Add("subject", email.subject);
                                Jsondata.Add("htmlBody", email.bodyHtml);
                                Receiver receiverData = new Receiver();
                                receiverData.JSONTemplateData = Jsondata.ToString();
                                receiverData.ReceiverEmail = email.toEmail;
                                receivers.Add(receiverData);

                                //update log table emails last sent date
                                userRemindersToSend.ForEach(t => NotificationServiceBL.MarkUserApprovalRemindersAsSent(remindersNotification.InternalUserRecipient, t.ApprovalId, startTime, context));

                                ServiceLog.Log(
                                            new LoggingNotification(
                                                String.Format("Approval reminders emails sent on: {0} for user {1}",
                                                    DateTime.UtcNow, remindersNotification.InternalUserRecipient),
                                                Severity.Info));
                            }
                            if (emailSenderAddress == "")
                            {
                                emailSenderAddress = email.fromEmail;
                            }
                        }
                        if (receivers.Count > 0)
                        {
                            while (receivers.Count > 50)
                            {
                                var data = receivers.GetRange(0, 50);
                                SendBulkEmailUsingTemplate(data, emailSenderAddress, context);
                                receivers.RemoveRange(0, 50);
                            }
                            if (receivers.Count > 0 && receivers.Count < 50)
                            {
                                SendBulkEmailUsingTemplate(receivers, emailSenderAddress, context);
                            }
                        }
                        //Save emails in database to be sent
                        Utils.EmailServiceUtils.SaveEmails(emails, context);

                        context.SaveChanges();
                    }
                }
            }
            catch (DbEntityValidationException valEx)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(valEx);
                ServiceLog.Log("Approval reminders sender entity validation error: errormessage: " + errorMessage, valEx);
            }
            catch (Exception ex)
            {
                ServiceLog.Log("Approval reminders sender error: ", ex);
            }
        }

        private string TrimSubjectLengthAbove255Characters(string emailSubject)
        {
            if (emailSubject?.Length > 255)
            {
                return emailSubject?.Substring(0, 250) + "...";
            }
            return emailSubject;
        }
        
    }
}
