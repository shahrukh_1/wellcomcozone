﻿using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.ArchiveCompletedJobsService.Workers
{
    public class WeeklyWorkers
    {
        private CancellationTokenSource _cts;

        public WeeklyWorkers(CancellationTokenSource cts)
        {
            _cts = cts;
        }

        public void RunWeeklyWorkers()
        {
            while (!_cts.IsCancellationRequested)
            {
                TimeToWaitTillNextIterration();

                Task.Factory.StartNew(() =>
                {
                    ArchiveCompletedJobsServiceWorker.DoCleaningWork();
                    CancelTask(_cts.Token);
                }, _cts.Token);
            }
        }

        /// <summary>
        /// Based on configuration put thread to sleep
        /// </summary>
        private void TimeToWaitTillNextIterration()
        {
            DateTime currentTime = DateTime.UtcNow;
            var dayOfWeekForWeeklyTasks =
                (DayOfWeek)
                Enum.Parse(typeof(DayOfWeek),
                            GMGColorConfiguration.AppConfiguration.DayOfWeekForWeeklyTasks);
            int daysTillStart = currentTime.DaysTillWeekDay(dayOfWeekForWeeklyTasks);
            DateTime nextDate = currentTime.AddDays(daysTillStart);
            DateTime desiredTime = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day,
                                                GMGColorConfiguration.AppConfiguration
                                                                        .UTCHourForRunningMaintenaceTasks, 0, 0);

            var timeSpan = desiredTime - currentTime;            
            Thread.Sleep(timeSpan);
        }


        /// <summary>
        /// Cancel task by throwing Operation Cancellation Exception
        /// </summary>
        /// <param name="token">The token used for the running task</param>
        private void CancelTask(CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
        }
    }
}
