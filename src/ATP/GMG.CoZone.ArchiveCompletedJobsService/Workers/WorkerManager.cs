﻿using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.ArchiveCompletedJobsService.Workers
{
    public class WorkerManager
    {
        private CancellationTokenSource _cts;

        public WorkerManager(CancellationTokenSource cts)
        {
            _cts = cts;
        }
        
        /// <summary>
        /// Run data cleanup service operations one after another waiting for each to finish
        /// </summary>
        public void RunMaintenaceTasks()
        {
            try
            {
                WeeklyWorkers weeklyWorkers = new WeeklyWorkers(_cts);
                weeklyWorkers.RunWeeklyWorkers();
            }
            catch (OperationCanceledException)
            {
                //Ignore task cancellation
            }
            catch (AggregateException e)
            {
                foreach (var v in e.InnerExceptions)
                {
                    ServiceLog.Log("ArchiveCompletedJobsService failed with the following error: ", v);
                }
            }   
        }
    }
}
