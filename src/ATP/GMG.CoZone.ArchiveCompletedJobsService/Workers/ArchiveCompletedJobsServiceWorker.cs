﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorDAL;

namespace GMG.CoZone.ArchiveCompletedJobsService.Workers
{
    public class ArchiveCompletedJobsServiceWorker
    {
        public static void DoCleaningWork()
        {
            try
            {
                ServiceLog.Log(new LoggingNotification("ArchiveCompletedJobs() started", Severity.Info));
                ArchiveCompletedJobs();

                ServiceLog.Log(new LoggingNotification("ArchiveCompletedJobs() finished", Severity.Info));
            }
            catch (DbEntityValidationException valEx)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(valEx);
                ServiceLog.Log("ArchiveCompletedJobs entity validation error: errormessage: " + errorMessage, valEx);
            }
            catch (Exception ex)
            {
                ServiceLog.Log("ArchiveCompletedJobs method failed", ex);
            }
        }

        public static void ArchiveCompletedJobs()
        {
            using (GMGColorContext context = new GMGColorContext())
            {
                List<Job> lstJobs = Job.GetCompletedJobs(context);
                int archivedStatus = JobStatu.GetJobStatus(JobStatu.Status.Archived, context).ID;
                lstJobs.ForEach(o => Job.ArchiveJob(o, archivedStatus));
                context.SaveChanges();
            }
        }
    }
}
