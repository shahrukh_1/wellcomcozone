﻿using System;
using System.ServiceProcess;
using System.Threading;
using GMG.CoZone.ArchiveCompletedJobsService.Workers;
using GMGColor.AWS;
using GMGColorDAL;

namespace GMG.CoZone.ArchiveCompletedJobsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure(); // configure log4net

            Console.WriteLine("CoZone Data Cleanup Service Started !\nPress Any key to exit");

            //Create cancellaction token                
            CancellationTokenSource cts = new CancellationTokenSource();
            var token = cts.Token;
            var workerManager = new WorkerManager(cts);

            while (!token.IsCancellationRequested)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
                    }

                    if (!Console.KeyAvailable)
                    {
                        workerManager.RunMaintenaceTasks();
                    }
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[]
                    {
                        new CoZoneArchiveCompletedJobsService(cts, workerManager)
                    };
                    ServiceBase.Run(ServicesToRun);
                }
            }

            cts.Cancel();
        }
    }
}
