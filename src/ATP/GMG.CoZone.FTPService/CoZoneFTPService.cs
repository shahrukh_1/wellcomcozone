﻿using System.ServiceProcess;
using System.Threading.Tasks;
using GMGColorDAL;
using GMGColor.AWS;

namespace GMG.CoZone.FTPService
{
    public partial class CoZoneFTPService : ServiceBase
    {
        #region Private Data
        
        private Task _ftpClientTask;
        private FTPClient ftpClient;
        
        private FTPServer ftpServer;
        private Task _ftpServerTask;
        
        #endregion

        #region Ctors
        public CoZoneFTPService()
        {
            InitializeComponent();
        }
        #endregion

        #region Protected Methods
        
        protected override void OnStart(string[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }

            // Start FTP client
            ftpClient = new FTPClient();
            _ftpClientTask = new Task(() => ftpClient.Start());
            _ftpClientTask.Start();

            // Stop FTP client
            ftpServer = new FTPServer();
            _ftpServerTask = new Task( () => ftpServer.Start());
            _ftpServerTask.Start();
        }

        protected override void OnStop()
        {
            ftpClient.Stop();
            ftpServer.Stop();
        }

        #endregion
    }
}
