﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GMGColorDAL;
using GMGColor.AWS;

namespace GMG.CoZone.FTPService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine("CoZone FTP Service Started !\nPress Any key to exit");

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
                }

                FTPClient ftpClient = new FTPClient();
                Task ftpClientTask = new Task(ftpClient.Start);
                ftpClientTask.Start();

                FTPServer ftpServer = new FTPServer();
                Task ftpServerTask = new Task(ftpServer.Start);
                ftpServerTask.Start();

                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }

                ftpClient.Stop();
                ftpServer.Stop();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			    { 
				    new CoZoneFTPService() 
			    };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
