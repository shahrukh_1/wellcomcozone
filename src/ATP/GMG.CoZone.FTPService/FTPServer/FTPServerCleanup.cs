﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;

namespace GMG.CoZone.FTPService
{
    /// <summary>
    /// FTP cleanup worker
    /// </summary>
    public class FTPServerCleanup
    {
        #region Inner Classes
        
        /// <summary>
        /// Cleanup item object
        /// </summary>
        private class CleanupItem
        {
            public string Path { get; set; }
            public bool IsFile { get; set; }
            public bool IsHandled { get; set; }
        }
        #endregion

        #region Private Data
        
        private static FTPServerCleanup _instance;
        private BlockingCollection<CleanupItem> _cleanupItemQueue;
        private Task _workerTask = null;
        private CancellationTokenSource _workerTaskCTS;
        private bool _isRunning = false;
        #endregion

        #region Properties

        public static FTPServerCleanup Instance
        {
            get { return _instance ?? (_instance = new FTPServerCleanup()); }
        }
        #endregion

        #region Ctors

        private FTPServerCleanup()
        {
            _cleanupItemQueue = new BlockingCollection<CleanupItem>();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Starts FTP cleanup worker
        /// </summary>
        public void Start()
        {
            if (_workerTask == null)
            {
                _workerTask = new Task(DoWork);
            }

            // Stop worker in case is already running
            if (_isRunning && _workerTaskCTS != null)
            {
                _workerTaskCTS.Cancel();
                while (_isRunning)
                {
                    Thread.Sleep(100);
                }
            }

            _workerTaskCTS = new CancellationTokenSource();
            _workerTask.Start();
        }

        /// <summary>
        /// Stops the cleanup worker
        /// </summary>
        public void Stop()
        {
            _workerTaskCTS.Cancel();
            _cleanupItemQueue.Dispose();
        }

        /// <summary>
        /// Enqueue an item for being deleted
        /// </summary>
        /// <param name="filePath"></param>
        public void DeleteFile(string filePath)
        {
            _cleanupItemQueue.Add(new CleanupItem() { Path = filePath, IsFile = true, IsHandled = false });
            FTPLogger.Instance.Log(new LoggingNotification(String.Format("FTPServerCleanup: Cleanup file {0} was queued for deletion", filePath), Severity.Info));
        }

        /// <summary>
        /// Enqueue an item for being deleted
        /// </summary>
        /// <param name="folderPath"></param>
        public void DeleteFolder(string folderPath)
        {
            _cleanupItemQueue.Add(new CleanupItem() { Path = folderPath, IsFile = false, IsHandled = false });
            FTPLogger.Instance.Log(new LoggingNotification(String.Format("FTPServerCleanup: Cleanup folder {0} was queued for deletion", folderPath), Severity.Info));
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Cleanup worker
        /// </summary>
        private void DoWork()
        {
            FTPLogger.Instance.Log(new LoggingNotification("FTPServerCleanup: FTP Cleanup Worker started", Severity.Info));
            _isRunning = true;
            while (!_workerTaskCTS.IsCancellationRequested)
            {
                CleanupItem item;
                while (_cleanupItemQueue.Count > 0)
                {
                    item = _cleanupItemQueue.Take();
                    try
                    {
                        if (item.IsFile)
                        {
                            if (!TryDeleteFile(item) && !item.IsHandled)
                            {
                                _cleanupItemQueue.Add(item);
                            }
                        }
                        else
                        {
                            if (!TryDeleteFolder(item) && !item.IsHandled)
                            {
                                _cleanupItemQueue.Add(item);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        FTPLogger.Instance.Log(new LoggingException("FTPServerCleanup: remove file routine thrown an exception", Severity.Info));            
                    }
                }
                // wait 0.5 secons between iterationsB
                Thread.Sleep(500);
            }

            _workerTaskCTS.Dispose();
            _isRunning = false;

            FTPLogger.Instance.Log(new LoggingNotification("FTPServerCleanup: FTP Cleanup Worker stopped", Severity.Info));
        }

        /// <summary>
        /// Try to remove the specified file
        /// </summary>
        /// <param name="item"></param>
        /// <returns>True in case it succeeded otherwise false</returns>
        private bool TryDeleteFile(CleanupItem item)
        {
            try
            {
                File.Delete(item.Path);
                item.IsHandled = true;
                FTPLogger.Instance.Log(new LoggingNotification(String.Format("FTPServerCleanup: File {0} was removed from server", item.Path), Severity.Info));
                return true;
            }
            catch
            {
                // In case the file doesn't exit, mark it as handled to be removed from queue
                if (!File.Exists(item.Path))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("FTPServerCleanup: File {0} was not found on server and was removed from queue", item.Path), Severity.Info));
                    item.IsHandled = true;
                }
            }
            return false;
        }

        /// <summary>
        /// Try to remove the specified folder
        /// </summary>
        /// <param name="item"></param>
        /// <returns>True in case it succeeded otherwise false</returns>
        private bool TryDeleteFolder(CleanupItem item)
        {
            try
            {
                Directory.Delete(item.Path, true);
                item.IsHandled = true;
                FTPLogger.Instance.Log(new LoggingNotification(String.Format("REMOVE: Folder {0} was removed from server", item.Path), Severity.Info));
                return true;
            }
            catch
            {
                // In case the file doesn't exit, mark it as handled to be removed from queue
                if (!Directory.Exists(item.Path))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("REMOVE: Folder {0} was not found on server and was removed from queue", item.Path), Severity.Info));
                    item.IsHandled = true;
                }
            }
            return false;
        }
        #endregion
    }
}
