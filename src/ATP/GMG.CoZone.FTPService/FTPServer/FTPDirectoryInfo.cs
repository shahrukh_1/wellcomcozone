﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GMG.CoZone.FTPService
{
    public class FTPDirectoryInfo
    {
        #region Properties

        public string FtpRootFolder { get; private set; }
        public string UserFolderName { get; private set; }
        public string ProfileFolderName { get; private set; }
        public string JobFolderName { get; private set; }
        public string JobFolderSubdirName { get; private set; }
        public FTPDirectoryType DirectoryType { get; private set; }
        #endregion

        #region Ctors

        private FTPDirectoryInfo()
        {
            FtpRootFolder =
            UserFolderName =
            ProfileFolderName =
            JobFolderSubdirName = String.Empty;

            DirectoryType = FTPDirectoryType.Invalid;
        }

        #endregion 
        
        #region Public Methds

        /// <summary>
        /// Parse the given path and validate it against the expected structure
        /// The expected structure is: FTPRoot/UserFolder(GUID)/ProfileFolder/JobFolder
        /// </summary>
        /// <param name="path"></param>
        /// <param name="ftpRootFolder"></param>
        /// <returns></returns>
        public static FTPDirectoryInfo Parse(string path, string ftpRootFolder)
        {
            FTPDirectoryInfo ftpDirectory = new FTPDirectoryInfo();

            //Check if path is a valid directory
            if (!Directory.Exists(path))
                return null;

            //Check if the root folder is not part of the path
            int position = path.IndexOf(ftpRootFolder);
            if (position != 0)
                return null;

            ftpDirectory.FtpRootFolder = ftpRootFolder;

            // extract root folder from path and process subfolder path
            string rootSubfolder = path.Remove(position, ftpRootFolder.Length);

            string[] dirHierarchy = rootSubfolder.Split(new[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

            // Set the subdirs to the job directory subfolder level
            // What comes under the Job folder is considered invalit input and is ignored
            int index = 0;
            foreach (string part in dirHierarchy)
            {
                switch (index)
                {
                    case 0:
                        ftpDirectory.UserFolderName = part;
                        break;
                    case 1:
                        ftpDirectory.ProfileFolderName = part;
                        break;
                    case 2:
                        ftpDirectory.JobFolderName = part;
                        break;
                    case 3:
                        ftpDirectory.JobFolderSubdirName = part;
                        break;
                }
                index++;
            }

            ftpDirectory.DetermineDirectoryType();

            return ftpDirectory;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Set the directory type based on the hierarchy level
        /// </summary>
        private void DetermineDirectoryType()
        {
            if (!String.IsNullOrEmpty(JobFolderSubdirName))
            {
                DirectoryType = FTPDirectoryType.Invalid;
            }
            else if (!String.IsNullOrEmpty(JobFolderName))
            {
                DirectoryType = FTPDirectoryType.Job;
            }
            else if (!String.IsNullOrEmpty(ProfileFolderName))
            {
                DirectoryType = FTPDirectoryType.Profile;
            }
            else if (!String.IsNullOrEmpty(UserFolderName))
            {
                DirectoryType = FTPDirectoryType.User;
            }
            else
            {
                DirectoryType = FTPDirectoryType.Invalid;
            }
        }


        #endregion
    }
}
