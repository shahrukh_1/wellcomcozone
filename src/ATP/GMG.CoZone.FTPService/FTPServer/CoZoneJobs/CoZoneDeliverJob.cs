﻿using System;
using System.Collections.Generic;
using GMGColorDAL.CustomModels.XMLEngine;
using GMGColorBusinessLogic;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;
using GMGColorDAL;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using iTextSharp.text.pdf;

namespace GMG.CoZone.FTPService
{
    public class CoZoneDeliverJob
    {
        /// <summary>
        /// Validates XML Deliver Job
        /// </summary>
        /// <param name="collaborateJob"></param>
        /// <param name="userGuid"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool ValidateDeliverJob(XMLJob job, int userID, int accID, DbContextBL context)
        {
            try
            {
                var deliverJob = job.Deliver;
                if (!DeliverBL.IsValidDeliverType(job.FileType, context))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job for userID {0} originated from folder {1} with XMLFile {2} could not be processed, file type is not valid for deliver!", userID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.FailedToProcessInvalidJobTypeMsg, string.Format("{0}//{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }

                GMGColorDAL.Role.RoleName deliverUserRole = GMGColorDAL.Role.GetRole(GMGColorDAL.User.GetRoleId(GMG.CoZone.Common.AppModule.Deliver, userID, context).GetValueOrDefault(), context);
                if (deliverUserRole == GMGColorDAL.Role.RoleName.AccountContributor)
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job for userID {0} originated from folder {1} with XMLFile {2} could not be processed, user doesn't have right to add jobs!", userID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.FailedToProcessDueToUserRightsMsg, string.Format("{0}//{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }

                if (!DeliverBL.RunningColorProofInstanceExists(deliverJob.ColorProofCozoneInstanceName, context) && !DeliverBL.RunningSharedColorProofInstanceExist(deliverJob.ColorProofCozoneInstanceName, accID, context))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job for userID {0} originated from folder {1} with XMLFile {2} could not be processed, Specified Running ColorProofInstance was not found in database!", userID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.FailedToProcessCpInstanceNotFoundMsg, string.Format("{0}//{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }
                if (!DeliverBL.CoZoneColorProofWorkflowExists(deliverJob.ColorProofCozoneWorkflow, deliverJob.ColorProofCozoneInstanceName, context) && !DeliverBL.SharedColorProofWorkflowExists(deliverJob.ColorProofCozoneWorkflow, deliverJob.ColorProofCozoneInstanceName, accID, context))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job for userID {0} originated from folder {1} with XMLFile {2} could not be processed, Specified ColorProofCozoneWorkflow was not found in!", userID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.FailedToProcessCpWrokflowNotFoundMsg, string.Format("{0}//{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }
                if (!DeliverBL.IsCoZoneWorkFlowInSecurityGroup(deliverJob.ColorProofCozoneWorkflow, deliverJob.ColorProofCozoneInstanceName, userID, context))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job for userID {0} originated from folder {1} with XMLFile {2} could not be processed, Specified ColorProofCozoneWorkflow doesn t belong to a security group", userID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.FailedToProcessDueToUserRightsMsg, string.Format("{0}//{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Validate Deliver Job for userID {0} originated from folder {1} with XMLFile {2} failed!", userID, job.OriginalFolderPath, job.XMLFilePath), ex), string.Format("{0}//{1}", job.OriginalFolderPath, job.JobName));
                return false;
            }
        }

         /// <summary>
        /// Creates ApprovalJob DAL Object
        /// </summary>
        /// <param name="job"></param>
        /// <param name="context"></param>
        /// <param name="duplicateOption"></param>
        /// <returns></returns>
        public static DeliverJob BuildDeliverJob(XMLJob job, int uploaderUserID, int uploaderUserAccID, DbContextBL context)
        {
            try
            {
                int jobStatusNew = GMGColorDAL.JobStatu.GetJobStatus(GMGColorDAL.JobStatu.Status.New, context).ID;

                int colorProofStatusSubmitting = DeliverBL.GetDeliverJobStatusInColorProofID(ColorProofJobStatus.Submitting, context);

                int deliverJobStatusSubmitting = DeliverBL.GetDeliverJobStatusID(DeliverJobStatus.Submitting, context);

                Job objJob = new Job();
                objJob.Title = job.JobName;
                objJob.Account = uploaderUserAccID;
                objJob.Status = jobStatusNew;
                objJob.Guid = Guid.NewGuid().ToString();
                objJob.ModifiedDate = DateTime.UtcNow;

                DeliverJob objDeliverJob = new DeliverJob();
                objDeliverJob.FTPSourceFolder = job.JobSourceFolderRelativeToUserFolder;
                objDeliverJob.Owner = uploaderUserID;
                objDeliverJob.JobSource = JobBL.GetJobSourceByKey(Common.JobSource.UploadEngine, context);
                objDeliverJob.Guid = Guid.NewGuid().ToString();
                objDeliverJob.Status = deliverJobStatusSubmitting;
                objDeliverJob.StatusInColorProof = colorProofStatusSubmitting;
                objDeliverJob.CreatedDate = DateTime.UtcNow;
                objDeliverJob.LogProofControlResults = job.Deliver.LogProofControlResults;
                objDeliverJob.IncludeProofMetaInformationOnLabel = job.Deliver.IncludeProofMetaInformationOnProofLabel;
                objDeliverJob.CPWorkflow = DeliverBL.GetCoZoneColorProofWorkflowByNameAndInstanceName(job.Deliver.ColorProofCozoneWorkflow, job.Deliver.ColorProofCozoneInstanceName, context);
                
                // Set page range for PDF documents
                if (job.AssetFilePath.ToLower().EndsWith(".pdf"))
                {
                    if (String.IsNullOrEmpty(job.Deliver.Pages) || String.Compare(job.Deliver.Pages.Trim().ToLower(), "all", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        objDeliverJob.Pages = "All";
                    }
                    else
                    {
                        objDeliverJob.Pages = job.Deliver.Pages;
                    }
                    // Get page count in case of a PDF file type
                    PdfReader pdfReader = new PdfReader(job.AssetFilePath);
                    objDeliverJob.TotalNrOfPages = pdfReader.NumberOfPages;
                }
                else
                {
                    objDeliverJob.TotalNrOfPages = 1;
                    objDeliverJob.Pages = "All";
                }

                objDeliverJob.Size = job.FileSize;
                objDeliverJob.FileName = job.FileName;

                objJob.DeliverJobs.Add(objDeliverJob);
                context.Jobs.Add(objJob);
                context.DeliverJobs.Add(objDeliverJob);

                return objDeliverJob;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Build Deliver Job for user {0} originated from folder {1} with XMLFile {2} failed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), ex));
            }
            return null;
        }

        /// <summary>
        /// Uploades deliver file
        /// </summary>
        /// <param name="loggedUserTempFolder"></param>
        /// <param name="fileName"></param>
        /// <param name="guid"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        public static bool MoveDeliverFile(string loggedUserTempFolder, string fileName, string guid, string region)
        {
            try
            {
                string sourceAttachedFilePath = loggedUserTempFolder + guid + "/" + fileName;
                string relativeFolderPath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + guid + "/";
                string destinationFilePath = relativeFolderPath + fileName;
                if (GMGColorIO.FolderExists(relativeFolderPath, region, true) &&
                    GMGColorIO.FileExists(sourceAttachedFilePath, region))
                {
                    GMGColorIO.MoveFile(sourceAttachedFilePath, destinationFilePath, region);
                }
                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Move Deliver Job file {0} originated from folder {1} with guid {2}", fileName, loggedUserTempFolder, guid), ex));
            }
            return false;
        }

        /// <summary>
        /// Creates define and deliver queue messages
        /// </summary>
        /// <param name="objDeliverJob"></param>
        /// <param name="accId"></param>
        /// <param name="backlinkUrl"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool CreateQueueMessages(DeliverJob objDeliverJob, int accId, string instanceName, DbContextBL context)
        {
            try
            {
                string backlinkUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + AccountBL.GetAccountDomain(accId, context) + "/Deliver/JobDetailLink?SelectedInstance=" + objDeliverJob.ID;

                Dictionary<string, string> dict = DeliverBL.GetDictionaryForDeliverQueue(objDeliverJob, backlinkUrl, AccountBL.GetAccountName(accId, context), instanceName, context);
                GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.DeliverJob);

                // job for generating preview
                dict = DeliverBL.GetDictionaryForDefineQueue(objDeliverJob);
                GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.ApprovalJob);

                objDeliverJob.BackLinkUrl = backlinkUrl;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("CreateQueueMessages for deliver with ID {0}", objDeliverJob.ID), ex));
            }

            return true;
        }
    }
}
