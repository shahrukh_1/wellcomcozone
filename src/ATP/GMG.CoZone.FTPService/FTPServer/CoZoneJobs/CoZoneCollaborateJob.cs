﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.XMLEngine;
using GMG.CoZone.Component.Logging;
using GMGColorDAL;
using GMGColorBusinessLogic;
using GMG.CoZone.Common;
using GMG.CoZone.Component;
using GMGColorDAL.Common;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using GMGColorNotificationService;

namespace GMG.CoZone.FTPService
{
    public class CoZoneCollaborateJob
    {
        #region Properties 

        private static object s_newApprovalsLocker = new object();
        //List containing approval id and creator id to send emails
        private static List<int> newApprovalsIDS = new  List<int>();
        private static Task _sendNewApprovalEmailsTask = null;
        private static CancellationTokenSource _sendNewApprovalEmailsCTS;

        #endregion

        /// <summary>
        /// Validate XML Collaborate Job
        /// </summary>
        /// <param name="collaborateJob"></param>
        /// <param name="userGuid"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool ValidateCollaborateJob(XMLJob job, int userID, Account account, DbContextBL context)
        {
            try
            {
                var collaborateJob = job.Collaborate;
                GMGColorDAL.Role.RoleName collaborateUserRole = GMGColorDAL.Role.GetRole(User.GetRoleId(GMG.CoZone.Common.AppModule.Collaborate, userID, context).GetValueOrDefault(), context);
                if (collaborateUserRole == GMGColorDAL.Role.RoleName.AccountViewer || collaborateUserRole == GMGColorDAL.Role.RoleName.AccountContributor)
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} could not be added, User doesn't have rights!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.AddJobFailedDueToUserRightsMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }

                if (GMGColorCommon.IsVideoFile(Path.GetExtension(job.AssetFilePath)) && !AccountBL.HasMultimediaOptionsEnabled(account.ID, GMG.CoZone.Common.AppModule.Collaborate, context))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} could not be added, video files not allowed billing plane type doesn' t have enabled Multimedia Options ", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.AddJobFailedDueToMissingMmOMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }

                //Validate Owner Field
                if (collaborateJob.Owner != null)
                {
                    bool ownerExists = UserBL.UserNameExists(collaborateJob.Owner, account.ID, context);

                    if (!ownerExists)
                    {
                        collaborateJob.Owner = UserBL.GetUserNameByID(userID, context);
                        FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} owner value {3} is invalid, it was replaced with uploader user username!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, collaborateJob.Owner), Severity.Warning), FTPXmlFeedBackConstants.InvalidJobNameMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                    }
                }

                //Validate Destination Folder Structure
                if (collaborateJob.DestinationFolder != null)
                {
                    string[] folders = collaborateJob.DestinationFolder.Split(new string[] {"/"}, StringSplitOptions.RemoveEmptyEntries);
                    if (folders.Count() > 1)
                    {
                        for (int i = 1; i < folders.Count(); i++)
                        {
                            if (!FolderBL.FolderExists(folders[i], folders[i - 1], userID, context))
                            {
                                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} destinationFolder path {3} is invalid, it was removed!", job.UserGUID, job.XMLFilePath, job.XMLFilePath, collaborateJob.DestinationFolder), Severity.Warning), FTPXmlFeedBackConstants.InvalidJobMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                                collaborateJob.DestinationFolder = null;
                            }
                        }
                    }
                    else if (folders.Count() == 1)
                    {
                        if (!FolderBL.FolderExists(folders[0], userID, context))
                        {
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} destinationFolder value {3} is invalid, it was removed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, collaborateJob.DestinationFolder), Severity.Warning), FTPXmlFeedBackConstants.InvalidJobMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                            collaborateJob.DestinationFolder = null;
                        }
                    }
                }

                //Validate PrimaryDecisionMaker
                if (!collaborateJob.PrimaryDecisionMaker.Equals(default(KeyValuePair<string, bool>)))
                {
                    if (collaborateJob.PrimaryDecisionMaker.Value)
                    {
                        if (!ExternalUserBL.ValidateExternalUserEmail(collaborateJob.PrimaryDecisionMaker.Key, userID, context))
                        {
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} external PDM email value {3} is invalid, it was removed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, collaborateJob.PrimaryDecisionMaker.Key), Severity.Warning), FTPXmlFeedBackConstants.InvalidExternalPDMMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                            collaborateJob.PrimaryDecisionMaker = default(KeyValuePair<string, bool>);
                        }
                    }
                    else
                    {
                        if (!UserBL.UserNameExists(collaborateJob.PrimaryDecisionMaker.Key, account.ID, context))
                        {
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} internal PDM username value {3} is invalid, it was removed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, collaborateJob.PrimaryDecisionMaker.Key), Severity.Warning), FTPXmlFeedBackConstants.InvalidPDMMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                            collaborateJob.PrimaryDecisionMaker = default(KeyValuePair<string, bool>);
                        }
                    }
                }

                //Validate internal collaborators
                if (collaborateJob.InternalCollaborator.Any())
                {
                    List<string> removedInternalCollaborators = new List<string>();
                    foreach (KeyValuePair<string, string> user in collaborateJob.InternalCollaborator)
                    {
                        if (!UserBL.UserNameExists(user.Key, account.ID, context))
                        {
                            removedInternalCollaborators.Add(user.Key);
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} internal collaborator username {3} is invalid, it was removed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, user.Key), Severity.Warning), FTPXmlFeedBackConstants.InvalidCollaboratorsMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                        }
                    }
                    //Remove invalid users from dictionary
                    removedInternalCollaborators.ForEach(t => collaborateJob.InternalCollaborator.Remove(t));
                }

                //Validate external collaborators
                if (collaborateJob.ExternalCollaborator.Any())
                {
                    List<string> removedExternalCollaborators = new List<string>();
                    foreach (KeyValuePair<string, string> exUser in collaborateJob.ExternalCollaborator)
                    {
                        if (!ExternalUserBL.ValidateExternalUserEmail(exUser.Key, userID, context))
                        {
                            removedExternalCollaborators.Add(exUser.Key);
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} external collaborator email {3} is invalid, it was removed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, exUser.Key), Severity.Warning), FTPXmlFeedBackConstants.InvalidExternalCollaboratorsMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                        }
                    }
                    //Remove invalid users from dictionary
                    removedExternalCollaborators.ForEach(t => collaborateJob.ExternalCollaborator.Remove(t));
                }

                //check billing plan limits
                string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(account, context.Users.FirstOrDefault(t => t.ID == userID), GMG.CoZone.Common.AppModule.Collaborate, 1, context, null, job.FileSize);
                if (!string.IsNullOrEmpty(warningMessage))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} could not be added billing plan limit has been reached ", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), warningMessage, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Validate Collaborate Job for user {0} originated from folder {1} with XMLFile {2} failed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), ex), job.OriginalFolderPath);
                return false;
            }
        }

        /// <summary>
        /// Creates ApprovalJob DAL Object
        /// </summary>
        /// <param name="job"></param>
        /// <param name="context"></param>
        /// <param name="duplicateOption"></param>
        /// <returns></returns>
        public static Approval BuildCollaborateJob(XMLJob job, int uploaderUserID, Account uploaderUserAcc, DbContextBL context, bool enableVersionMirroring, FileUploadDuplicateName? duplicateOption = null)
        {
            try
            {
                Approval objApproval = new Approval();
                objApproval.FTPSourceFolder = job.JobSourceFolderRelativeToUserFolder;

                Job objJob = null;
                var isVersionMirroringEnabled= false;
                var fileName = Path.GetFileName(job.AssetFilePath);
                if (duplicateOption != null)
                {
                    isVersionMirroringEnabled = enableVersionMirroring;
                    switch (duplicateOption.Value)
                    {
                        case  FileUploadDuplicateName.FileWithSuffix_vX:
                            {
                                var fName = Path.GetFileNameWithoutExtension(fileName).ToLower();
                                var vIndex = fName.LastIndexOf("_v", StringComparison.Ordinal);

                                //Get filename without _vX suffix
                                if (vIndex > 0)
                                {
                                    fName = fileName.Substring(0, vIndex).ToLower();
                                }

                                //Checkes whether a duplicate file with (_vX) suffix exists in same source folder
                                var objJobs = XMLEngineBL.GetDuplicateFileWithVersionSuffixJob(fName, job.JobSourceFolderRelativeToUserFolder, context);

                                //Checks whether exists in the current folder at least one file with same name and (_vX) suffix
                                var duplicateHasSuffix = (from j in objJobs
                                                            let vIdx = j.FileName.ToLower().LastIndexOf("_v", StringComparison.Ordinal)
                                                            where vIdx > 0 && j.FileName.ToLower().Substring(0, vIdx) == fName
                                                            select j).Any();

                                //Remove _vX suffix from all filenames and compare their content
                                foreach (Approval.DuplicatedFileNamesJobs j in objJobs)
                                {
                                    string jobName = j.FileName.ToLower();
                                    var idx = jobName.LastIndexOf("_v", StringComparison.Ordinal);
                                    if (idx > 0 && jobName.Substring(0, idx) == fName || Path.GetFileNameWithoutExtension(jobName) == fName)
                                    {
                                        objJob = JobBL.GetJobByID(j.JobID, context);
                                        break;
                                    }
                                }

                                //ignore file that has no (_vX) suffix
                                if (vIndex <= 0 && (objJob != null && objJob.ID > 0) && duplicateHasSuffix)
                                {
                                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job for user {0} originated from folder {1} with XMLFile {2} failed Version invalid, file ignored!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning));
                                    return null;
                                }
                            }
                            break;
                        case FileUploadDuplicateName.MatchFileNameExactly:
                            {
                                //Checkes whether a duplicate file with exact filename exists in same source folder and add it as a version
                                objJob = XMLEngineBL.GetDuplicateFileJob(fileName, job.JobSourceFolderRelativeToUserFolder, context);
                                break;
                            }
                    }
                }

                if (objJob == null)
                {
                    objJob = new Job
                        {
                            Title = job.JobName,
                            Account = uploaderUserAcc.ID,
                            ModifiedDate = DateTime.UtcNow,
                            Guid = Guid.NewGuid().ToString()
                        };

                    context.Jobs.Add(objJob);
                }
                else
                {
                    //mark previous versions as locked
                    foreach (Approval version in objJob.Approvals)
                    {
                        version.IsLocked = true;
                    }
                }

                objApproval.Guid = Guid.NewGuid().ToString();
                objApproval.Creator = uploaderUserID;
                objApproval.CreatedDate = DateTime.UtcNow;
                objApproval.Modifier = uploaderUserID;
                objApproval.ModifiedDate = DateTime.UtcNow;
                objApproval.IsDeleted = false;
                int[] versions = ApprovalBL.GetVersions(objJob.ID, context);
                objApproval.Version = isVersionMirroringEnabled ? ApprovalBL.GetFileVersion(fileName, versions) : versions.Any() ? (versions.Max() + 1) : 1;
                if (!enableVersionMirroring)
                {
                    objApproval.VersionSufix = string.Empty;
                }
                objApproval.VersionSufix = ApprovalBL.GetVersionSufix(Path.GetFileNameWithoutExtension(fileName));
                objApproval.AllowDownloadOriginal = job.Collaborate.AllowUsersToDownloadOriginalFile;
                objApproval.LockProofWhenAllDecisionsMade = job.Collaborate.LockWhenAllDecisionsHaveBeenMade;
                objApproval.LockProofWhenFirstDecisionsMade = job.Collaborate.LockWhenFirstDecisionHasBeenMade;

                objApproval.JobSource = JobBL.GetJobSourceByKey(GMG.CoZone.Common.JobSource.UploadEngine, context);
                objApproval.FolderPath = String.Empty;

                objApproval.Size = job.FileSize;

                //log file usage
                
                PlansBL.LogApprovalSizeUsage(null, uploaderUserAcc, job.FileSize, context);

                objApproval.FileName = job.FileName;
                objApproval.FileType1 = (GMGColorDAL.FileType.GetFileType(job.FileName, context));
                objApproval.FileType = objApproval.FileType1.ID;
                objApproval.Type = (objApproval.FileType1 != null && objApproval.FileType1.Type == "video") ||
                    (objApproval.FileType1 != null && objApproval.FileType1.Type == "swf") ? ApprovalBL.GetApprovalTypeID(Approval.ApprovalTypeEnum.Movie, context) : ApprovalBL.GetApprovalTypeID(Approval.ApprovalTypeEnum.Image, context);

                objApproval.Owner = UserBL.GetUserIdByUserName(job.Collaborate.Owner, uploaderUserAcc.ID, context);

                //Check if pdm is set
                if (!job.Collaborate.PrimaryDecisionMaker.Equals(default(KeyValuePair<string, bool>)))
                {
                    objApproval.OnlyOneDecisionRequired = true;
                    if (job.Collaborate.PrimaryDecisionMaker.Value)
                    {
                        //External
                        objApproval.ExternalPrimaryDecisionMaker = ExternalUserBL.GetExternalUserIDByEmail(job.Collaborate.PrimaryDecisionMaker.Key, uploaderUserID, context);
                    }
                    else
                    {
                        //Internal
                        objApproval.PrimaryDecisionMaker = UserBL.GetUserIdByUserName(job.Collaborate.PrimaryDecisionMaker.Key, uploaderUserAcc.ID,  context);
                    }
                }

                if (!String.IsNullOrEmpty(job.Collaborate.DestinationFolder))
                {
                    //Reconstruct folder hierarchy from the specified string
                    string[] folders = job.Collaborate.DestinationFolder.Split(new string[] { "/"}, StringSplitOptions.RemoveEmptyEntries);
                    if (folders.Count() > 1)
                    {
                        int folderId = FolderBL.GetFolderIDByName(folders[0], uploaderUserID, context);
                        for (int i = 1; i < folders.Count(); i++)
                        {
                            folderId = FolderBL.GetFolderIDByNameAndParentID(folders[i], folderId, uploaderUserID, context);
                        }
                        objApproval.Folders.Add(FolderBL.GetFolderByID(folderId, uploaderUserID, context));
                    }
                    else if (folders.Count() == 1)
                    {
                        objApproval.Folders.Add(FolderBL.GetFolderByName(folders[0], uploaderUserID, context));
                    }
                }

                // Parse deadline date
                string deadLineString = !String.IsNullOrEmpty(job.Collaborate.Deadline) ? job.Collaborate.Deadline : DateTime.UtcNow.ToString();
                
                DateTime deadline;
                DateTime.TryParseExact(deadLineString, AccountBL.GetAccountDateFormatPattern(uploaderUserID, context) + " " + AccountBL.GetUSerAccountAccountTimeFormat(uploaderUserID, context),
                    System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out deadline);
                if (deadline == default(DateTime))
                {
                    deadline = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Unspecified);
                }
                objApproval.Deadline = String.IsNullOrEmpty(job.Collaborate.Deadline) ? deadline: GMGColorFormatData.SetUserTimeToUTC(deadline, AccountBL.GetAccountTimeZone(uploaderUserID, context));

                objJob.Approvals.Add(objApproval);
                objJob.Status = GMGColorDAL.JobStatu.GetJobStatusId(GMGColorDAL.JobStatu.Status.New, context);
                context.Approvals.Add(objApproval);

                return objApproval;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Build Collaborate Job for user {0} originated from folder {1} with XMLFile {2} failed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), ex));
            }
            return null;
        }

        /// <summary>
        /// Sets Approval Collaborators
        /// </summary>
        /// <param name="objApproval">New created approval object</param>
        /// <param name="accId">uploader user Account</param>
        /// <param name="internalCollaborators">Dictionary containing internalcollaborators usernames and roles</param>
        /// <param name="externalCollaborators">Dictionary containing externalCollaborators usernames and roles</param>
        /// <param name="context">DB Context</param>
        /// <returns></returns>
        public static bool SetApprovalCollaborators(Approval objApproval, int accId, Dictionary<string, string> internalCollaborators, Dictionary<string, string> externalCollaborators, DbContextBL context)
        {
            try
            {
                //Internal Collaborators
                GMGColorDAL.CustomModels.PermissionsModel permissionsModel = new GMGColorDAL.CustomModels.PermissionsModel();
                permissionsModel.IsFolder = false;
                permissionsModel.Creator = objApproval.Creator;
                permissionsModel.ID = objApproval.ID;
                permissionsModel.SendEmails = false;

                List<GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup> usersWhereThisIsApproval = new List<GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup>();

                int approverAndReviewerRoleID = ApprovalBL.GetApprovalRoleID(ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer, context);
                int reviewerRoleID = ApprovalBL.GetApprovalRoleID(ApprovalCollaboratorRole.ApprovalRoleName.Reviewer, context);
                int readonlyRoleID = ApprovalBL.GetApprovalRoleID(ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly, context);

                if (internalCollaborators.Count > 0)
                {
                    foreach (KeyValuePair<string, string> user in internalCollaborators)
                    {
                        //if no role is set default should be readonly
                        int approvalRoleID = readonlyRoleID;
                        if (user.Value != String.Empty)
                        {
                            switch (user.Value.ToLower())
                            {
                                case Constants.ApprovalReviewerRole:
                                    {
                                        approvalRoleID = reviewerRoleID;
                                        break;
                                    }
                                case Constants.ApprovalApproverAndReviewerRole:
                                    {
                                        approvalRoleID = approverAndReviewerRoleID;
                                        break;
                                    }
                            }
                        }

                        int userID = UserBL.GetUserIdByUserName(user.Key, accId, context);

                        if (!usersWhereThisIsApproval.Any(t => t.ID == userID))
                        {
                             usersWhereThisIsApproval.Add(new GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup()
                                                        {
                                                            ID = userID,
                                                            IsChecked = true,
                                                            ApprovalRole = approvalRoleID
                                                        }
                                                    );
                        }
                    }
                }

                //Uploader must be approval collaborator
                if (!usersWhereThisIsApproval.Any(t => t.ID == objApproval.Creator))
                {
                    usersWhereThisIsApproval.Add(new GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup()
                                                    {
                                                        ID = objApproval.Creator,
                                                        IsChecked = true,
                                                        ApprovalRole = approverAndReviewerRoleID
                                                    }
                                                );
                }

                //Owner must be approval collaborator
                if (!usersWhereThisIsApproval.Any(t => t.ID == objApproval.Owner))
                {
                    usersWhereThisIsApproval.Add(
                                                    new GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup()
                                                    {
                                                        ID = objApproval.Owner,
                                                        IsChecked = true,
                                                        ApprovalRole = approverAndReviewerRoleID
                                                    });
                }

                if (objApproval.PrimaryDecisionMaker != null)
                {
                    //PDM Must be collaborator
                    if (!usersWhereThisIsApproval.Any(t => t.ID == objApproval.PrimaryDecisionMaker))
                    {
                        usersWhereThisIsApproval.Add(
                                                        new GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup()
                                                        {
                                                            ID = objApproval.PrimaryDecisionMaker.Value,
                                                            IsChecked = true,
                                                            ApprovalRole = approverAndReviewerRoleID
                                                        });
                    }
                    else
                    {
                        //PDM must have approver and reviewer role
                        GMGColorDAL.CustomModels.PermissionsModel.UserOrUserGroup pdm = usersWhereThisIsApproval.SingleOrDefault(t => t.ID == objApproval.PrimaryDecisionMaker);
                        if (pdm.ApprovalRole != approverAndReviewerRoleID)
                        {
                            pdm.ApprovalRole = approverAndReviewerRoleID;
                        }
                    }
                }

                permissionsModel.SelectedUsersAndGroups = usersWhereThisIsApproval;
                PermissionsBL.SetApprovalPermissionFromFTP(permissionsModel, objApproval.ID, context);

                //External Collaborators
                if (externalCollaborators.Count > 0)
                {
                    //Create expected string by SendAndShare Method (need to be compatible with UI Version)
                    string externalusers = "[";
                    int approvalRoleID = readonlyRoleID;
                    foreach (KeyValuePair<string, string> exUser in externalCollaborators)
                    {
                        if (exUser.Value != String.Empty)
                        {
                            switch (exUser.Value.ToLower())
                            {
                                case Constants.ApprovalReviewerRole:
                                    {
                                        approvalRoleID = reviewerRoleID;
                                        break;
                                    }
                                case Constants.ApprovalApproverAndReviewerRole:
                                    {
                                        approvalRoleID = approverAndReviewerRoleID;
                                        break;
                                    }
                            }
                        }

                        int exUserID = ExternalUserBL.GetExternalUserIDByEmail(exUser.Key, objApproval.Creator, context);

                        string exUserString = String.Format("\"{0}|{1}\",", exUserID, approvalRoleID);

                        externalusers += exUserString;
                    }

                    if (objApproval.ExternalPrimaryDecisionMaker.HasValue)
                    {
                        if (!externalCollaborators.ContainsKey(ExternalUserBL.GetExternalUserEmailByID(objApproval.ExternalPrimaryDecisionMaker.Value, objApproval.Creator, context)))
                        {
                            externalusers += String.Format("\"{0}|{1}\",", objApproval.ExternalPrimaryDecisionMaker.Value, approverAndReviewerRoleID);
                        }
                    }

                    //Remove last occurring comma
                    externalusers = externalusers.Remove(externalusers.Length - 1);
                    externalusers += "]";

                    var sendAndShareModel = new PermissionsBL.SendAndShareModel();
                    sendAndShareModel.approvalId = objApproval.ID;
                    sendAndShareModel.externalUsers = externalusers;
                    sendAndShareModel.context = context;
                    sendAndShareModel.loggedUser = objApproval.CreatorUser;
                    sendAndShareModel.loggedAccountId = accId;
                    sendAndShareModel.sendEmails = false;
                    PermissionsBL.SendAndShare(sendAndShareModel);
                }
                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Set collaborators for approval with id {0} failed", objApproval.ID), ex));
                return false;
            }
        }

        /// <summary>
        /// Gets Duplicate File Name Settings
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="userId"></param>
        /// <param name="jobSourceFolder"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static FileUploadDuplicateName? GetFileDuplicateName(string fileName, int userId, string jobSourceFolder, DbContextBL context)
        {
            FileUploadDuplicateName? duplicateOption = null;

            var fName = Path.GetFileNameWithoutExtension(fileName);

            if (fName != null)
            {
                fName = fName.ToLower();
                var vIndex = fName.LastIndexOf("_v", StringComparison.Ordinal);

                //Get filename without _vX suffix
                if (vIndex > 0)
                {
                    fName = fileName.Substring(0, vIndex);
                }

                if (XMLEngineBL.DuplicateFileNameExists(fName, userId, jobSourceFolder, context))
                {
                    int? duplicateFileNameKey = XMLEngineBL.GetDuplicateFileNameSetting(userId, context);
                    duplicateOption = duplicateFileNameKey.HasValue
                        ? (FileUploadDuplicateName) duplicateFileNameKey
                        : FileUploadDuplicateName.MatchFileNameExactly;
                }
            }

            return duplicateOption;
        }

        /// <summary>
        /// Adds approval to the internal list of approvals for which an email must be sent
        /// </summary>
        /// <param name="appID"></param>
        public static void ScheduleApprovalForEmail(int appID)
        {
            lock (s_newApprovalsLocker)
            {
                newApprovalsIDS.Add(appID);
            }
        }

        /// <summary>
        /// Thread needed for sending new approval emails after have been processed by define
        /// </summary>
        public static void StartEmailsThread()
        {
            try
            {
                if (_sendNewApprovalEmailsTask == null)
                {
                    _sendNewApprovalEmailsTask = new Task(SendEmails);
                }

                _sendNewApprovalEmailsCTS = new CancellationTokenSource();
                _sendNewApprovalEmailsTask.Start();
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException("CoZoneCollaborateJob StartEmailsThread() method failed", ex));
            }
        }

        public static void StopEmailsThread()
        {
            if (_sendNewApprovalEmailsCTS != null)
            {
                _sendNewApprovalEmailsCTS.Cancel();
            }
        }

        public static void SendEmails()
        {
            FTPLogger.Instance.Log(new LoggingNotification("CoZoneCollaborateJob SendEmails thread started", Severity.Info));

            while (!_sendNewApprovalEmailsCTS.Token.IsCancellationRequested)
            {
                try
                {
                    lock (s_newApprovalsLocker)
                    {
                        using (DbContextBL context = new DbContextBL())
                        {
                            for (int i = newApprovalsIDS.Count - 1; i >= 0; i--)
                            {
                                if (!ApprovalBL.IsApprovalProcessingCompleted(newApprovalsIDS[i], context))
                                {
                                    continue;
                                }
                                else
                                {
                                    SendApprovalEmail(newApprovalsIDS[i], context);
                                    newApprovalsIDS.Remove(newApprovalsIDS[i]);
                                }
                            }
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    FTPLogger.Instance.Log(new LoggingException("CoZoneCollaborateJob SendEmails exception", ex));
                }
                finally
                {
                    //Sleep 5 sec
                    Thread.Sleep(1000 * 5);
                }
            }
            FTPLogger.Instance.Log(new LoggingNotification("CoZoneCollaborateJob SendEmails thread stopped", Severity.Info));
        }

        /// <summary>
        /// Sends email for the specified approval
        /// </summary>
        /// <param name="approvalID"></param>
        /// <param name="context"></param>
        public static void SendApprovalEmail(int approvalID, DbContextBL context)
        {
            var approval = ApprovalBL.GetApproval(approvalID, context);
            List<int> externalCollaborators = ApprovalBL.GetExternalCollaborators(approvalID, context);
            NotificationType eventType = ApprovalBL.IsFirstVersion(approvalID, context) ? NotificationType.NewApprovalAdded : NotificationType.NewVersionWasCreated;

            //Use List to remove duplicate emails from notification process
            List<int> approvalIdList = new List<int>();
            approvalIdList.Add(approvalID);

            foreach (int externalCollaboratorId in externalCollaborators)
            {
                if (eventType == NotificationType.NewApprovalAdded)
                {
                    NotificationServiceBL.CreateNotification(new NewApprovalAdded
                                                            {
                                                                EventCreator = approval.CreatorUser.ID,
                                                                ExternalRecipient = externalCollaboratorId,
                                                                ApprovalsIds = approvalIdList.ToArray()
                                                            },
                                                                approval.CreatorUser,
                                                                context,
                                                                false
                                                             );
                }
                else
                {
                    NotificationServiceBL.CreateNotification(new NewVersionWasCreated
                                                            {
                                                                EventCreator = approval.CreatorUser.ID,
                                                                ExternalRecipient = externalCollaboratorId,
                                                                ApprovalsIds = approvalIdList.ToArray()
                                                            },
                                                                approval.CreatorUser,
                                                                context,
                                                                false
                                                              );
                }
            }
            IEnumerable<int> approvalCollaborators = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(eventType), approvalID, context);

            foreach (int userId in approvalCollaborators)
            {
                if (eventType == NotificationType.NewApprovalAdded)
                {
                    NotificationServiceBL.CreateNotification(new NewApprovalAdded()
                                                            {
                                                                EventCreator = approval.CreatorUser.ID,
                                                                InternalRecipient = userId,
                                                                ApprovalsIds = approvalIdList.ToArray()
                                                            },
                                                                approval.CreatorUser,
                                                                context,
                                                                false
                                                             );
                }
                else
                {
                    NotificationServiceBL.CreateNotification(new NewVersionWasCreated()
                                                                {
                                                                    EventCreator = approval.CreatorUser.ID,
                                                                    InternalRecipient = userId,
                                                                    ApprovalsIds = approvalIdList.ToArray()
                                                                },
                                                                approval.CreatorUser,
                                                                context,
                                                                false
                                                              );
                }
            }
        }
    }
}
