﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.FTPService
{
    public class FTPError
    {
        public string Message { get; set; }
        public string FileName { get; set; }
        public string LoggedDate { get; set; }
    }
}
