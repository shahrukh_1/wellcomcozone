﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Linq;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorDAL;

namespace GMG.CoZone.FTPService
{
    public static class FTPXmlFeedBack
    {

        #region Private Methods
        private static FTPError GenerateErrorXmlData(DateTime loggedErrorDate, string message ,string filePath)
        {
            var fileName = filePath.Substring(filePath.LastIndexOf("\\", StringComparison.InvariantCulture) + 1);
            return new FTPError()
            {
                LoggedDate = loggedErrorDate.ToString("g", CultureInfo.InvariantCulture),
                FileName = fileName,
                Message = message
            };
        }

        private static XDocument GenerateErrorXmlDocument(FTPError xmlErrorData)
        {
            return new XDocument(
                new XElement(FTPXmlFeedBackConstants.FtpUploadError,
                    new XElement(FTPXmlFeedBackConstants.LoggedDate, xmlErrorData.LoggedDate),
                    new XElement(FTPXmlFeedBackConstants.FileName, xmlErrorData.FileName),
                    new XElement(FTPXmlFeedBackConstants.ErrorMessage, xmlErrorData.Message)
                ));
        }

        private static string GenerateErrorFolderPath(string message, bool isOutsideJobFolder = false)
        {
            var newPath = "";
            if (isOutsideJobFolder != true)
            {
                newPath = Path.GetFullPath(Path.Combine(message, @"..\..\"));
            }
            else {
                newPath = Path.GetFullPath(Path.Combine(message, @"..\"));
            }
                
            var pathWithoutFileName = newPath.Substring(0, newPath.LastIndexOf("\\", StringComparison.InvariantCulture)) + "\\" + FTPXmlFeedBackConstants.Errors;
            return
                pathWithoutFileName.Substring(
                    pathWithoutFileName.IndexOf(GMGColorConfiguration.AppConfiguration.FTPRootFolder, StringComparison.InvariantCulture));
        }

        private static string GetFileName(string filePath)
        {
            var fileGuid = Guid.NewGuid().ToString();
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            return string.Format("{0}.{1}.xml", fileName, fileGuid.Substring(0, 5));
        }

        private static void UploadErrorXmlDocument(XDocument document, string filePath, bool isOutsideJobFolder = false)
        {
            var errorFolderPath = GenerateErrorFolderPath(filePath, isOutsideJobFolder);
            if (!Directory.Exists(errorFolderPath))
            {
                Directory.CreateDirectory(errorFolderPath);
            }
            document.Save(Path.Combine(errorFolderPath, GetFileName(filePath)));
        }

        #endregion
        
        #region Public Methods
        public static void GenerateFeedBack(string notification, string filePath, bool isOutsideJobFolder = false)
        {
            try
            {
                var logErrorDateTime = DateTime.UtcNow;
                var xmlErrorData = GenerateErrorXmlData(logErrorDateTime, notification, filePath);
                var xmlErrorDoc = GenerateErrorXmlDocument(xmlErrorData);
                UploadErrorXmlDocument(xmlErrorDoc, filePath, isOutsideJobFolder);
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(ex.Message));
            }
        }
        #endregion
    }
}
