﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.FTPService
{
    public class FTPXmlFeedBackConstants
    {
        public static string FtpUploadError { get { return "FTPUploadError"; } }
        public static string LoggedDate { get { return "LoggedDate"; } }
        public static string ErrorMessage { get { return "ErrorMessage"; } }
        public static string Errors { get { return "Errors"; } }
        public static string UploadError { get { return "UploadErrror"; } }
        public static string FileName { get { return "FileName"; } }
        
        public static string FailedToProcessInvalidJobTypeMsg { get { return "Failed to process job, because file type is not valid!"; } }
        public static string FailedToProcessDueToUserRightsMsg { get { return "Failed to process job, because the user does not have the right to add jobs!"; } }
        public static string FailedToProcessCpInstanceNotFoundMsg { get { return "Failed to process job, because Specified Running ColorProofInstance was not found in database!"; } }
        public static string FailedToProcessCpWrokflowNotFoundMsg { get { return "Failed to process job, because Specified ColorProofCozoneWorkflow was not found in!"; } }
        public static string FailedToProcessCpSecurityGroupMsg { get { return "Failed to process job, because Specified ColorProofCozoneWorkflow doesn't belong to a security group!"; } }
        public static string IncorrectFileLocationMsg { get { return "File was deleted, because files can only be placed inside job folders!"; } }
        public static string AddJobFailedDueToUserRightsMsg { get { return "Job could not be added, User doesn't have rights!"; } }
        public static string AddJobFailedDueToMissingMmOMsg { get { return "Job could not be added, video files not allowed billing plane type doesn' t have enabled Multimedia Options!"; } }
        public static string AddJobFailedDueToBillingPlanMsg { get { return "Job could not be added, account billing plan doesn't have the option!"; } }
        public static string InvalidJobNameMsg { get { return "Job is invalid, it was replaced with uploader user username!"; } }
        public static string InvalidJobMsg { get { return "Job folder structure is invalid, it was removed!"; } }
        public static string InvalidExternalPDMMsg { get { return "Job External PDM  is invalid, it was removed!"; } }
        public static string InvalidPDMMsg { get { return "Job PDM  is invalid, it was removed!"; } }
        public static string InvalidCollaboratorsMsg { get { return "Job internal collaborators is invalid, it was removed!"; } }
        public static string InvalidExternalCollaboratorsMsg { get { return "Job external collaborators is invalid, it was removed!"; } }
        public static string InvalidXmlMsg { get { return "The uploaded XML file has some errors!"; } }
        public static string DefaultErrorMsg { get { return "An uknow error has occured. Please try again!"; } }
        public static string InvalidFileTypeMsg { get { return "Job could not be processed, file type is not valid for manage!"; } }
        public static string InvalidPreFlightProfileMsg { get { return "Job could not be processed, Specified preflight profile name was not found in database!"; }}
        public static string InvalidInputColorSpaceMsg { get { return "Job could not be processed, Specified input color space was not found in database!"; }}
        public static string InvalidOutputColorSpaceMsg { get { return "Job could not be processed, Specified input color space was not found in database!"; }}
        public static string MissingColorProofMsg { get { return "Job could not be processed, ColorProof Workflow or Instance missing!"; } }
        public static string MaximumProofsReachedMsg { get { return "Job could not be processed, you have reached the maximum allowed number of proofs!"; } }
        public static string InvalidFolderStructureMsg { get { return "Folder structure was deleted, either the user or profile folders are invalid!"; } }
    }
}
