﻿using System.IO;
using System.Reflection;
using System.Threading;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;
using GMGColorDAL;
using System.Linq;
using System;
using GMG.CoZone.Common;
using GMGColorBusinessLogic;

namespace GMG.CoZone.FTPService
{
    public class FTPServer
    {
        #region Private data

    
        private CancellationTokenSource _ftpServerTaskCTS = new CancellationTokenSource();
        private FileSystemWatcher _rootFolderWatcher;
        private readonly string _ftpRootDir;
        #endregion

        #region Ctors

        public FTPServer()
        {
            _ftpRootDir = GMGColorConfiguration.AppConfiguration.FTPRootFolder;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// FTP Server worker thread
        /// </summary>
        public void Start()
        {
            try
            {
                FTPLogger.Instance.Log(new LoggingNotification("FTPServer: Thread started", Severity.Info));

                // Start cleanup service
                FTPServerCleanup.Instance.Start();

                while (true)
                {
                    try
                    {
                        // Init file system watcher
                        InitFileSystemWatcher(_ftpRootDir);

                        //Start ProcessingJob Service
                        ProcessJobService.Instance.Start();
                        break;
                    }
                    catch (Exception ex)
                    {
                        //Fix for the case when restarting AWS Instance and Z drive isn't mounted yet
                        FTPLogger.Instance.Log(new LoggingException("FTPServer: Start() Init SystemWatcher error ", ex));
                        //sleep 10 sec
                        Thread.Sleep(10000);
                    }
                }

                while (!_ftpServerTaskCTS.Token.IsCancellationRequested)
                {
                    Thread.Sleep(100 * 10); // wait 1 seconds between syncs
                }

                FTPLogger.Instance.Log(new LoggingNotification("FTPServer: Thread stopped", Severity.Info));
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException("FTPServer: Start() method exception ", ex));
            }
        }

        /// <summary>
        /// Stops the FTP server worker thread
        /// </summary>
        public void Stop()
        {
            if (_rootFolderWatcher != null)
            {
                _rootFolderWatcher.Dispose();
            }

            if (_ftpServerTaskCTS != null)
            {
                _ftpServerTaskCTS.Cancel();
            }

            FTPServerCleanup.Instance.Stop();
            ProcessJobService.Instance.Stop();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Init file watcher notifications
        /// </summary>
        /// <param name="pathToWatch"></param>
        private void InitFileSystemWatcher(string folderToWatch)
        {
            try
            {
                if (Directory.Exists(folderToWatch))
                {
                    // Create a new FileSystemWatcher and set its properties.
                    _rootFolderWatcher = new FileSystemWatcher();
                    _rootFolderWatcher.Path = folderToWatch;
                    _rootFolderWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;

                    //_rootFolderWatcher.Changed += FileEventRaised;
                    _rootFolderWatcher.Created += FileEventRaised;

                    _rootFolderWatcher.EnableRaisingEvents = true;
                    _rootFolderWatcher.IncludeSubdirectories = true;
                    //increase the buffer size to overcome the 80 files watched in the same time limitation
                    _rootFolderWatcher.InternalBufferSize = 32768;
                }
                else
                {
                    FTPLogger.Instance.Log(
                        new LoggingNotification(
                            String.Format("FTPServer: FTP watcher cannot be initialized. The {0} path doesn't exist", folderToWatch),
                            Severity.Fatal));
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(MessageConstants.FTPFileWatcherInitializationFailed, ex));
            }
        }

        /// <summary>
        /// File event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileEventRaised(object sender, FileSystemEventArgs e)
        {
            try
            {
                switch (e.ChangeType)
                {
                    case WatcherChangeTypes.Created:
                        {
                            FileAttributes attr = File.GetAttributes(e.FullPath);

                            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                            {
                                HandleFolderCreatedEvent(e.FullPath);
                            }
                            else
                            {
                                HandleNewFileCreatedEvent(e.FullPath);
                            }
                            break;
                        }
                    default:
                        {
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("FTPServer: FTP watcher raised a {0} event for path {1}", e.ChangeType, e.FullPath), Severity.Info));
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("FTPServer: FTP watcher FileEventRaised()  exception for {0} event for path {1}", e.ChangeType, e.FullPath), ex));
            }
        }

        /// <summary>
        /// Handler for directory created event
        /// </summary>
        /// <param name="folderPath"></param>
        private void HandleFolderCreatedEvent(string folderPath)
        {
            try
            {
                if (folderPath.IndexOf(string.Format("\\{0}", FTPJobStatusConstants.JobStatuses), StringComparison.InvariantCulture) != -1 || 
                    folderPath.IndexOf(string.Format("\\{0}", FTPJobStatusConstants.ErrorsFolder), StringComparison.InvariantCulture) != -1) return;
                //Parse new created folder path
                FTPDirectoryInfo dirInfo = FTPDirectoryInfo.Parse(folderPath, _ftpRootDir);

                FTPLogger.Instance.Log(
                    new LoggingNotification(
                        String.Format("FTPServer: New directory created: Type: {0}, Path: {1}",
                            dirInfo.DirectoryType, folderPath), Severity.Info));

                switch (dirInfo.DirectoryType)
                {
                    case FTPDirectoryType.Invalid:
                        FTPServerCleanup.Instance.DeleteFolder(folderPath);
                        break;
                    case FTPDirectoryType.User:
                        if (!XMLEngineBL.ValidateUserFolder(dirInfo.UserFolderName))
                        {
                            FTPLogger.Instance.Log(
                                new LoggingNotification(
                                    String.Format(
                                        "FTPServer: UserGuid folder is not valid and has been deleted !: Type: {0}, Path: {1}",
                                        dirInfo.DirectoryType, folderPath), Severity.Warning));
                            FTPServerCleanup.Instance.DeleteFolder(folderPath);
                        }
                        break;
                    case FTPDirectoryType.Profile:
                        if (!XMLEngineBL.ValidateProfileFolder(dirInfo.ProfileFolderName, dirInfo.UserFolderName))
                        {
                            FTPLogger.Instance.Log(
                                new LoggingNotification(
                                    String.Format(
                                        "FTPServer: Profile folder is not valid and has been deleted !: Type: {0}, Path: {1}",
                                        dirInfo.DirectoryType, folderPath), Severity.Warning));
                            FTPServerCleanup.Instance.DeleteFolder(folderPath);
                        } 
                        break;
                    case FTPDirectoryType.Job:
                        // Do nothing
                        break;
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("FTPServer: FTP watcher HandleFolderCreatedEvent() exception for folderPath {0}", folderPath), ex));
            }
        }

        /// <summary>
        /// Handler for new file created event
        /// </summary>
        /// <param name="filePath"></param>
        private void HandleNewFileCreatedEvent(string filePath)
        {
            try
            {
                if (filePath.IndexOf(string.Format("\\{0}\\", FTPJobStatusConstants.JobStatuses), StringComparison.InvariantCulture) != -1 ||
                    filePath.IndexOf(string.Format("\\{0}\\", FTPJobStatusConstants.ErrorsFolder), StringComparison.InvariantCulture) != -1) return;
                FileInfo fileInfo = new FileInfo(filePath);
                FTPDirectoryInfo dirInfo = FTPDirectoryInfo.Parse(fileInfo.DirectoryName, _ftpRootDir);

                FTPLogger.Instance.Log(
                    new LoggingNotification(
                        String.Format("FTPServer: New file created: Directory Type: {0}, Path: {1}",
                            dirInfo.DirectoryType, filePath), Severity.Info));

                switch (dirInfo.DirectoryType)
                {
                    case FTPDirectoryType.Invalid:
                    case FTPDirectoryType.User:
                    case FTPDirectoryType.Profile:
                    {
                        FTPLogger.Instance.Log(
                            new LoggingNotification(
                                String.Format(
                                    "FTPServer: File {0} has been deleted , files are allowed to be created only inside job source folder!",
                                    filePath), Severity.Warning), FTPXmlFeedBackConstants.IncorrectFileLocationMsg, filePath, true);

                        FTPServerCleanup.Instance.DeleteFile(filePath);
                        break;
                    }
                    case FTPDirectoryType.Job:
                    {
                        using (DbContextBL context = new DbContextBL())
                        {
                            if (
                                !XMLEngineBL.IsProfileStillValid(dirInfo.ProfileFolderName, dirInfo.UserFolderName,
                                    context))
                            {
                                FTPServerCleanup.Instance.DeleteFolder(Path.Combine(_ftpRootDir,
                                    dirInfo.UserFolderName, dirInfo.ProfileFolderName));
                                FTPLogger.Instance.Log(
                                    new LoggingNotification(
                                        String.Format(
                                            "FTPServer: New file created: Directory Type: {0}, Path: {1}, profile folder has been deleted since is not valid anymore",
                                            dirInfo.DirectoryType, filePath), Severity.Warning));
                            }
                            else
                            {
                                if (XMLEngineBL.IsProfileActive(dirInfo.ProfileFolderName, dirInfo.UserFolderName,
                                    context))
                                {
                                    ProcessJobService.Instance.ProcessFile(fileInfo.Name, dirInfo, context);
                                }
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("FTPServer: FTP watcher HandleNewFileCreatedEvent() exception for filePath {0}", filePath), ex));
            }
        }
    #endregion
    }
}
