﻿using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using GMG.CoZone.Common;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;
using System.Linq;
using System.IO;
using System;
using System.Collections.Generic;
using GMGColorDAL;
using GMGColorBusinessLogic;
using GMGColorDAL.CustomModels.XMLEngine;
using GMGColorDAL.Common;
using ICSharpCode.SharpZipLib.Zip;

namespace GMG.CoZone.FTPService
{
    /// <summary>
    /// Process job worker
    /// </summary>
    public class ProcessJobService
    {
        #region Inner Classes
        
        /// <summary>
        /// Enum for the file type
        /// </summary>
        private enum JobFileType
        {
            Asset = 0,
            XML
        }

        /// <summary>
        /// CoZone job item
        /// </summary>
        private class CoZoneJob
        {
            #region Properties
            /// <summary>
            /// FTP job directory source info
            /// </summary>
            public FTPDirectoryInfo JobDirectoryInfo { get; set; }
            public CoZoneFile AssetFile { get; set; }
            public CoZoneFile XMLFile { get; set; }
            public bool ReadyToMove { get { return XMLFile != null && AssetFile != null; } }
            public bool ReadyToProcess { get; set; }
            /// <summary>
            /// Curent job folder path (FTP directory or Temp processing diretory)
            /// </summary>
            public string AbsoluteJobFolderPath
            {
                get
                {
                    if (ReadyToProcess)
                    {
                        return Path.Combine(GMGColorConfiguration.AppConfiguration.XMLJobsTempFolder, JobTempFolder);
                    }
                    else
                    {
                        return Path.Combine(JobDirectoryInfo.FtpRootFolder, JobDirectoryInfo.UserFolderName, JobDirectoryInfo.ProfileFolderName, JobDirectoryInfo.JobFolderName);
                    }
                }
            }
            /// <summary>
            /// Job temporary folder path (process folder) 
            /// </summary>
            public string JobTempFolder { get; set; }
            /// <summary>
            /// XML file current path
            /// </summary>
            public string AbsoluteXMLFilePath
            {
                get
                {
                    return XMLFile != null ? Path.Combine(AbsoluteJobFolderPath, XMLFile.Name) : String.Empty;
                }
            }
            /// <summary>
            /// Asset file current path
            /// </summary>
            public string AbsoluteAssetFilePath
            {
                get
                {
                    return AssetFile != null ? Path.Combine(AbsoluteJobFolderPath, AssetFile.Name) : String.Empty;
                }
            }
            /// <summary>
            /// FTP source file path
            /// </summary>
            public string OriginalJobPath
            {
                get
                {
                    return Path.Combine(JobDirectoryInfo.FtpRootFolder, JobDirectoryInfo.UserFolderName, JobDirectoryInfo.ProfileFolderName, JobDirectoryInfo.JobFolderName);
                }
            }

            #endregion

            #region Methods 
 
            /// <summary>
            /// Determines if a job with specified folders exists when adding a new file
            /// </summary>
            /// <param name="userGuid"></param>
            /// <param name="profile"></param>
            /// <param name="sourceFolder"></param>
            /// <returns></returns>
            public bool JobAlreadyExists(string userGuid, string profile, string sourceFolder)
            {
                return JobDirectoryInfo.JobFolderName == sourceFolder && JobDirectoryInfo.ProfileFolderName == profile && JobDirectoryInfo.UserFolderName == userGuid;
            }

            #endregion
        }

        /// <summary>
        /// CoZone File item
        /// </summary>
        private class CoZoneFile
        {
            public string Name { get; set; }
            public JobFileType Type { get; set; }
            public DateTime AddedTime { get; set; }
        }

        #endregion

        #region Private Data

        private static ProcessJobService _instance;
        private static object s_locker = new object();
        private List<CoZoneJob> _coZoneJobItemQueue;
        private Task _workerPrepareJobsTask = null;
        private Task _workerProcessJobsTask = null;
        private CancellationTokenSource _workerPrepareJobsTaskCTS;
        private CancellationTokenSource _workerProcessJobsTaskCTS;
        private bool _isPrepareJobsRunning = false;
        private bool _isProcessJobsRunning = false;

        #endregion

        #region Properties

        public static ProcessJobService Instance
        {
            get { return _instance ?? (_instance = new ProcessJobService()); }
        }

        #endregion

        #region Ctors

        private ProcessJobService()
        {
            _coZoneJobItemQueue = new List<CoZoneJob>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Starts FTP ProcessJobService worker
        /// </summary>
        public void Start()
        {
            try
            {
                if (_workerPrepareJobsTask == null)
                {
                    _workerPrepareJobsTask = new Task(PrepareJobs);
                }

                if (_workerProcessJobsTask == null)
                {
                    _workerProcessJobsTask = new Task(ProcessJobs);
                }

                // Stop worker in case is already running
                if (_isPrepareJobsRunning && _workerPrepareJobsTaskCTS != null)
                {
                    _workerPrepareJobsTaskCTS.Cancel();
                    while (_isPrepareJobsRunning)
                    {
                        Thread.Sleep(100);
                    }
                }

                // Stop worker in case is already running
                if (_isProcessJobsRunning && _workerProcessJobsTaskCTS != null)
                {
                    _workerProcessJobsTaskCTS.Cancel();
                    while (_isProcessJobsRunning)
                    {
                        Thread.Sleep(100);
                    }
                }

                _workerPrepareJobsTaskCTS = new CancellationTokenSource();
                _workerPrepareJobsTask.Start();

                _workerProcessJobsTaskCTS = new CancellationTokenSource();
                _workerProcessJobsTask.Start();

                CoZoneCollaborateJob.StartEmailsThread();

                Directory.CreateDirectory(GMGColorConfiguration.AppConfiguration.XMLJobsTempFolder);
                Directory.CreateDirectory(GMGColorConfiguration.AppConfiguration.XMLJobsFailedFolder);
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException("ProcessJobService: Start() method failed", ex));
            }
        }

        /// <summary>
        /// Stops the ProcessJobService worker
        /// </summary>
        public void Stop()
        {
            if (_workerPrepareJobsTaskCTS != null)
            {
                _workerPrepareJobsTaskCTS.Cancel();
            }

            if (_workerProcessJobsTaskCTS != null)
            {
                _workerProcessJobsTaskCTS.Cancel();
            }

            CoZoneCollaborateJob.StopEmailsThread();
        }

        /// <summary>
        /// Process the new file that has been detected (adding it to the waiting list)
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dirInfo"></param>
        public void ProcessFile(string fileName, FTPDirectoryInfo dirInfo, DbContextBL context)
        {
            try
            {
                // Check if the file is an archive first
                if (Path.GetExtension(fileName).ToLower() == ".zip")
                {
                    ProcessArchive(fileName, dirInfo, context);
                    return;
                }

                lock (s_locker)
                {
                    CoZoneFile newFile = new CoZoneFile() { AddedTime = DateTime.UtcNow, Name = fileName, Type = DetermineFileType(fileName) };

                    //check if the created file is a valid type for XML Engine
                    if (newFile.Type == JobFileType.Asset)
                    {
                        if (!XMLEngineBL.IsFileValid(Path.GetExtension(fileName), dirInfo.UserFolderName, context))
                        {
                            string filePath = Path.Combine(dirInfo.FtpRootFolder, dirInfo.UserFolderName,
                                                        dirInfo.ProfileFolderName, dirInfo.JobFolderName, fileName);
                            //File is Not valid
                            FTPServerCleanup.Instance.DeleteFile(filePath);
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("File {0} doesn't have a valid type specified in account settings and has been deleted", filePath), Severity.Warning), FTPXmlFeedBackConstants.FailedToProcessInvalidJobTypeMsg, filePath);
                            return;
                        }
                    }

                    //Search for the other job file type that make a job complete
                    JobFileType searchedType = newFile.Type == JobFileType.XML ? JobFileType.Asset : JobFileType.XML;

                    //Try to find an existing job that would make the job ready for processing
                    CoZoneJob existingJob = TryTakeExistingJob(dirInfo.UserFolderName, dirInfo.ProfileFolderName, dirInfo.JobFolderName, searchedType);

                    if (existingJob == null)
                    {
                        //Create a new job entry and put it in the waiting list
                        CoZoneJob newJob = new CoZoneJob() { JobDirectoryInfo = dirInfo };
                        if (newFile.Type == JobFileType.XML)
                        {
                            newJob.XMLFile = newFile;
                        }
                        else
                        {
                            newJob.AssetFile = newFile;
                        }

                        _coZoneJobItemQueue.Add(newJob);
                    }
                    else
                    {
                        //This job should be ready for processing now
                        if (newFile.Type == JobFileType.XML)
                        {
                            existingJob.XMLFile = newFile;
                        }
                        else
                        {
                            existingJob.AssetFile = newFile;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("ProcessJobService: AddNewFile method failed for file {0}. The file will be deleted", fileName), ex));
                FTPServerCleanup.Instance.DeleteFile(Path.Combine(dirInfo.FtpRootFolder, dirInfo.UserFolderName,
                                                        dirInfo.ProfileFolderName, dirInfo.JobFolderName,fileName));
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Process the archive input
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dirInfo"></param>
        private void ProcessArchive(string fileName, FTPDirectoryInfo dirInfo, DbContextBL context)
        {
            try
            {
                //File is Not valid and it will be removed
                string fileDirectoryPath = Path.Combine(dirInfo.FtpRootFolder, dirInfo.UserFolderName,
                    dirInfo.ProfileFolderName, dirInfo.JobFolderName);

                string zipFileName = Path.Combine(fileDirectoryPath, fileName);
                //Check if the user is allowed to upload ZIP files and remove it in case it doesn't
                if (!XMLEngineBL.IsFileValid(Path.GetExtension(fileName), dirInfo.UserFolderName, context))
                {
                    FTPServerCleanup.Instance.DeleteFile(zipFileName);
                    FTPLogger.Instance.Log(
                        new LoggingNotification(
                            String.Format(
                                "The account settings does not allow zip files so the input folder {0} of the {1} archive was deleted",
                                fileDirectoryPath, fileName), Severity.Info));
                    return;
                }

                //Wait until the zip is completely uploaded on the FTP before trying to unzip files, wait maximum 20 minutes
                bool isZipReady = false;
                Stopwatch timeoutWatch = new Stopwatch();
                timeoutWatch.Start();
                while (!isZipReady && timeoutWatch.ElapsedMilliseconds < 20 * 60 * 1000)
                {
                    FileStream zipStream = null;
                    try
                    {
                        FileInfo zipFileInfo = new FileInfo(zipFileName);
                        zipStream = zipFileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                        isZipReady = true; //uploading complete
                    }
                    catch (IOException)
                    {
                        //still in progress of uploading
                        Thread.Sleep(100);
                    }
                    finally
                    {
                        if (zipStream != null)
                        {
                            zipStream.Close();
                        }
                    }
                }

                //TODO Check if the file exists when unzipping to avoid overwriting
                //Unzip the archive in the same location (the same events like for the user upload will be fired)
                FileStream fileStreamIn = new FileStream(zipFileName, FileMode.Open, FileAccess.Read);
                ZipInputStream zipInStream = new ZipInputStream(fileStreamIn);
                ZipEntry entry;
                while ((entry = zipInStream.GetNextEntry()) != null)
                {
                    FileStream fileStreamOut = new FileStream(Path.Combine(fileDirectoryPath, entry.Name), FileMode.Create,
                        FileAccess.Write);
                    int size;
                    byte[] buffer = new byte[2048];
                    do
                    {
                        size = zipInStream.Read(buffer, 0, buffer.Length);
                        fileStreamOut.Write(buffer, 0, size);
                    } while (size > 0);
                    fileStreamOut.Close();
                }
                zipInStream.Close();
                fileStreamIn.Close();

                // Delete archive as it is not needed anymore
                FTPLogger.Instance.Log(new LoggingNotification(String.Format("TheArchive was extracted successfully {0}", zipFileName), Severity.Info));
                FTPServerCleanup.Instance.DeleteFile(zipFileName);
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("ProcessJobService: ProcessArchive Exception for file {0}", fileName), ex));
                //Move archive to error folder
                string fileDirectoryPath = Path.Combine(dirInfo.FtpRootFolder, dirInfo.UserFolderName,
                    dirInfo.ProfileFolderName, dirInfo.JobFolderName);
                string zipFileName = Path.Combine(fileDirectoryPath, fileName);
                File.Move(zipFileName, Path.Combine(GMGColorConfiguration.AppConfiguration.XMLJobsFailedFolder, fileName));
            }
        }

        /// <summary>
        /// Prepare jobs for processing step worker
        /// </summary>
        private void PrepareJobs()
        {
            FTPLogger.Instance.Log(new LoggingNotification("ProcessJobService: PrepareJobs Worker started", Severity.Info));
            _isPrepareJobsRunning = true;
            while (!_workerPrepareJobsTaskCTS.IsCancellationRequested)
            {
                try
                {
                    CoZoneJob jobToMove = TryTakeReadyToMoveJob();

                    if (jobToMove != null)
                    {
                        jobToMove.ReadyToProcess = MoveJobFolder(jobToMove);
                        // Add to the end ofthe queue to let the subsequent items to be moved
                        AddNewJob(jobToMove);
                    }

                    CheckForExpiredJobs();
                }
                catch (Exception ex)
                {
                    FTPLogger.Instance.Log(new LoggingException("ProcessJobService: PrepareJobs Worker Exception", ex));
                }
                finally
                {
                    // wait 1/4 seconds between iterationsB
                    Thread.Sleep(250);
                }
            }

            _workerPrepareJobsTaskCTS.Dispose();
            _isPrepareJobsRunning = false;

            FTPLogger.Instance.Log(new LoggingNotification("ProcessJobService: PrepareJobs Worker stopped", Severity.Info));
        }

        /// <summary>
        /// Process job worker
        /// </summary>
        private void ProcessJobs()
        {
            FTPLogger.Instance.Log(new LoggingNotification("ProcessJobService: ProcessJobs Worker started", Severity.Info));
             _isProcessJobsRunning = true;

             while (!_workerProcessJobsTaskCTS.IsCancellationRequested)
            {
                try
                {
                    CoZoneJob jobToProcess = TryTakeReadyToProcessJob();

                    if (jobToProcess != null)
                    {
                        if (!ProcessJob(jobToProcess))
                        {
                            AddNewJob(jobToProcess);
                        }
                    }

                }
                catch (Exception ex)
                {
                    FTPLogger.Instance.Log(new LoggingException("ProcessJobService: ProcessJobs Worker Exception", ex));
                }
                finally
                {
                    // wait 1/4 seconds between iterations
                    Thread.Sleep(250);
                }
            }

            _workerProcessJobsTaskCTS.Dispose();
            _isProcessJobsRunning = false;

            FTPLogger.Instance.Log(new LoggingNotification("ProcessJobService: ProcessJobs Worker stopped", Severity.Info));
        }

        /// <summary>
        /// Determines FileType
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private JobFileType DetermineFileType(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            if (extension == ".xml")
            {
                return JobFileType.XML;
            }

            return JobFileType.Asset;
        }

        /// <summary>
        /// Moves Ready job to be processed in a Temp folder
        /// </summary>
        /// <param name="readyJob"></param>
        /// <returns></returns>
        private bool MoveJobFolder(CoZoneJob readyJob)
        {
            string destFolder = Path.Combine(GMGColorConfiguration.AppConfiguration.XMLJobsTempFolder, Guid.NewGuid().ToString());
            try
            {
                string xmlAbsoluteFilePath = Path.Combine(readyJob.AbsoluteJobFolderPath, readyJob.XMLFile.Name);
                string assetAbsoluteFilePath = Path.Combine(readyJob.AbsoluteJobFolderPath, readyJob.AssetFile.Name);
                
                string destXmlFilePath = Path.Combine(destFolder, readyJob.XMLFile.Name);
                string destAssetFilePath = Path.Combine(destFolder, readyJob.AssetFile.Name);
				
				#region Check if the files are still in use (the upload to FTP is not completed)
                FileStream xmlStream = null;
                FileStream assetStream = null;
                try
                {
                    FileInfo xmlFileInfo = new FileInfo(xmlAbsoluteFilePath);
                    xmlStream = xmlFileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                    FileInfo assetFileInfo = new FileInfo(assetAbsoluteFilePath);
                    assetStream = assetFileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (IOException)
                {
                    return false;
                }
                finally
                {
                    if (xmlStream != null)
                        xmlStream.Close();
                    if (assetStream != null)
                        assetStream.Close();
                }
				#endregion

                Directory.CreateDirectory(destFolder);
                File.Move(xmlAbsoluteFilePath, destXmlFilePath);
                File.Move(assetAbsoluteFilePath, destAssetFilePath);
                
                readyJob.JobTempFolder = destFolder;
            }
            catch (Exception ex)
            {
                // Cleanup temporary folder in case one of the move statements failed
                FTPServerCleanup.Instance.DeleteFolder(destFolder);
                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Could not move File {0} and {1} to temp folder Exception: {2}", readyJob.AbsoluteAssetFilePath, readyJob.AbsoluteXMLFilePath, ex.Message), Severity.Warning));
                return false;
            }

            return true;
        }

        #region Thread Safe Queue Methods

        /// <summary>
        /// Returns a ready to move Job
        /// </summary>
        /// <returns></returns>
        private CoZoneJob TryTakeReadyToMoveJob()
        {
            lock (s_locker)
            {
                CoZoneJob item = _coZoneJobItemQueue.FirstOrDefault(t => t.ReadyToMove && !t.ReadyToProcess);
                if (item != null)
                {
                    _coZoneJobItemQueue.Remove(item);
                }
                return item;
            }
        }

        /// <summary>
        /// Returns a ready to process Job
        /// </summary>
        /// <returns></returns>
        private CoZoneJob TryTakeReadyToProcessJob()
        {
            lock (s_locker)
            {
                CoZoneJob item = _coZoneJobItemQueue.FirstOrDefault(t => t.ReadyToProcess);
                if (item != null)
                {
                    _coZoneJobItemQueue.Remove(item);
                }
                return item;
            }
        }

        /// <summary>
        /// Try to take the specified job from the list if exists
        /// </summary>
        /// <param name="userGuid"></param>
        /// <param name="profile"></param>
        /// <param name="sourceFolder"></param>
        /// <returns></returns>
        private CoZoneJob TryTakeExistingJob(string userGuid, string profile, string sourceFolder, JobFileType searchedType)
        {
            CoZoneJob existingJob = _coZoneJobItemQueue.FirstOrDefault(t => t.JobAlreadyExists(userGuid, profile, sourceFolder) && !t.ReadyToMove);
            if (existingJob != null && ((searchedType == JobFileType.XML && existingJob.XMLFile != null) || (searchedType == JobFileType.Asset && existingJob.AssetFile != null)))
            {
                return existingJob;
            }
            return null;
        }

        /// <summary>
        /// Add a NewJob to the waiting list
        /// </summary>
        /// <param name="job"></param>
        private void AddNewJob(CoZoneJob job)
        {
            lock (s_locker)
            {
                _coZoneJobItemQueue.Add(job);
            }
        }

        /// <summary>
        /// Deletes expired files that have not been processed after the timeout time and also removes them from waiting list
        /// </summary>
        private void CheckForExpiredJobs()
        {
            lock (s_locker)
            {
                for (int i = _coZoneJobItemQueue.Count - 1; i >= 0; i--)
                {
                    if (!_coZoneJobItemQueue[i].ReadyToMove && (DateTime.UtcNow - (_coZoneJobItemQueue[i].AssetFile ?? _coZoneJobItemQueue[i].XMLFile).AddedTime).TotalMinutes > GMGColorConfiguration.AppConfiguration.PendingXMLJobLifeTimeInMinutes)
                    {
                        _coZoneJobItemQueue.Remove(_coZoneJobItemQueue[i]);
                        FTPLogger.Instance.Log(new LoggingNotification(String.Format("The file {0} originated from {1} was removed as the waiting file for the counterpart file expired", _coZoneJobItemQueue[i].AssetFile != null ? _coZoneJobItemQueue[i].AssetFile.Name : _coZoneJobItemQueue[i].XMLFile.Name, _coZoneJobItemQueue[i].OriginalJobPath), Severity.Warning));
                        FTPServerCleanup.Instance.DeleteFile(Path.Combine(_coZoneJobItemQueue[i].AbsoluteJobFolderPath, (_coZoneJobItemQueue[i].AssetFile ?? _coZoneJobItemQueue[i].XMLFile).Name));
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Process the specified job
        /// </summary>
        /// <param name="job"></param>
        /// <returns>Returns false only one the jobs needs to re-added to the processing list for later processing</returns>
        private bool ProcessJob(CoZoneJob job)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    XMLJob xmlJob = ProcessXML(job, context);
                    if (xmlJob != null)
                    {
                        CreateJob(xmlJob, context);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("ProcessJobService: ProcessJob Method Exception processing job {0} originated from {1}", job.AbsoluteJobFolderPath, job.OriginalJobPath), ex));
                return false;
            }
        }

        /// <summary>
        /// Parses Input xml file and try to map fields to a XMLJob object
        /// </summary>
        /// <param name="job"></param>
        /// <param name="context"></param>
        /// <returns>The CoZone job based on the XML fields found</returns>
        private XMLJob ProcessXML(CoZoneJob job, DbContextBL context)
        {
            try
            {
                List<XMLField> xmlFields = null;

                int? customTemplateID = XMLEngineBL.GetCustomTemplateIDByProfileName(job.JobDirectoryInfo.ProfileFolderName, job.JobDirectoryInfo.UserFolderName, context);

                xmlFields = XMLEngineBL.GetXMLTemplateFields(context, customTemplateID);

                XDocument jobXML;

                using (var oReader = new StreamReader(job.AbsoluteXMLFilePath, Encoding.UTF8))
                {
                    jobXML = XDocument.Load(oReader);
                }

                XMLJob coZoneXmlJob = new XMLJob(job.AbsoluteAssetFilePath);
                coZoneXmlJob.UserGUID = job.JobDirectoryInfo.UserFolderName;
                coZoneXmlJob.JobSourceFolderRelativeToUserFolder = Path.Combine(job.JobDirectoryInfo.ProfileFolderName, job.JobDirectoryInfo.JobFolderName);
                coZoneXmlJob.OriginalFolderPath = job.OriginalJobPath;
                coZoneXmlJob.XMLFilePath = job.AbsoluteXMLFilePath;

                foreach (DefaultXMLField xmlNode in xmlFields)
                {
                    #region Process Node

                    IEnumerable<XElement> elements = jobXML.XPathSelectElements(xmlNode.XPath);
                    if (elements.Any())
                    {
                        switch ((DefaultXMLTemplateField)xmlNode.Key)
                        {
                            case DefaultXMLTemplateField.JobName:
                            {
                                coZoneXmlJob.JobName = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.Modules:
                            {
                                coZoneXmlJob.SetModules(elements.First().Value.TrimEnd().TrimStart());
                                break;
                            }
                            case DefaultXMLTemplateField.InternalCollaborator:
                            {
                                foreach (XElement collaborator in elements)
                                {
                                    XElement user = collaborator.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals(Constants.XMLNodeInternalCollaboratorUserName, StringComparison.OrdinalIgnoreCase));
                                    XElement role = collaborator.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals(Constants.XMLNoteCollaboratorRole, StringComparison.OrdinalIgnoreCase));
                                    if (user != null)
                                    {
                                        coZoneXmlJob.Collaborate.InternalCollaborator.Add(user.Value, role!= null ? role.Value : String.Empty);
                                    }
                                }
                                break;
                            }
                            case DefaultXMLTemplateField.ExternalCollaborator:
                            {
                                foreach (XElement collaborator in elements)
                                {
                                    XElement email = collaborator.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals(Constants.XMLNodeExternalCollaboratorEmail, StringComparison.OrdinalIgnoreCase));
                                    XElement role = collaborator.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals(Constants.XMLNoteCollaboratorRole, StringComparison.OrdinalIgnoreCase));
                                    if (email != null)
                                    {
                                        coZoneXmlJob.Collaborate.ExternalCollaborator.Add(email.Value, role!= null ? role.Value : String.Empty);
                                    }
                                }
                                break;
                            }
                            case DefaultXMLTemplateField.PrimaryDecisionMaker:
                            {
                                XElement user = elements.First().Descendants().FirstOrDefault(x => x.Name.LocalName.Equals(Constants.XMLNodePrimaryDecisionMakerUser, StringComparison.OrdinalIgnoreCase));
                                XElement isExternal = elements.First().Descendants().FirstOrDefault(x => x.Name.LocalName.Equals(Constants.XMLNodePrimaryDecisionMakerIsExternal, StringComparison.OrdinalIgnoreCase));
                                if (user != null)
                                {
                                    coZoneXmlJob.Collaborate.PrimaryDecisionMaker = new KeyValuePair<string, bool>(user.Value, isExternal != null && isExternal.Value.ToLower() == "true");
                                }
                                break;
                            }
                            case DefaultXMLTemplateField.Owner:
                            {
                                coZoneXmlJob.Collaborate.Owner = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.GenerateSeparations:
                            {
                                coZoneXmlJob.Collaborate.GenerateSeparations = (elements.First().Value.TrimEnd().TrimStart().ToLower() == "true");
                                break;
                            }
                            case DefaultXMLTemplateField.LockWhenAllDecisionsHaveBeenMade:
                            {
                                coZoneXmlJob.Collaborate.LockWhenAllDecisionsHaveBeenMade = (elements.First().Value.TrimEnd().TrimStart().ToLower() == "true");
                                break;
                            }
                            case DefaultXMLTemplateField.LockWhenFirstDecisionHasBeenMade:
                            {
                                coZoneXmlJob.Collaborate.LockWhenFirstDecisionHasBeenMade = (elements.First().Value.TrimEnd().TrimStart().ToLower() == "true");
                                break;
                            }
                            case DefaultXMLTemplateField.Deadline:
                            {
                                coZoneXmlJob.Collaborate.Deadline = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.AllowUsersToDownloadOriginalFile:
                            {
                                coZoneXmlJob.Collaborate.AllowUsersToDownloadOriginalFile = (elements.First().Value.TrimEnd().TrimStart().ToLower() == "true");
                                break;
                            }
                            case DefaultXMLTemplateField.DestinationFolder:
                            {
                                coZoneXmlJob.Collaborate.DestinationFolder = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.ColorProofCozoneInstanceName:
                            {
                                coZoneXmlJob.Deliver.ColorProofCozoneInstanceName = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.ColorProofCozoneWorkflow:
                            {
                                coZoneXmlJob.Deliver.ColorProofCozoneWorkflow = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.Pages:
                            {
                                coZoneXmlJob.Deliver.Pages = elements.First().Value.TrimEnd().TrimStart();
                                break;
                            }
                            case DefaultXMLTemplateField.LogProofControlResults:
                            {
                                coZoneXmlJob.Deliver.LogProofControlResults = (elements.First().Value.TrimEnd().TrimStart().ToLower() == "true");
                                break;
                            }
                            case DefaultXMLTemplateField.IncludeProofMetaInformationOnProofLabel:
                            {
                                coZoneXmlJob.Deliver.IncludeProofMetaInformationOnProofLabel = (elements.First().Value.TrimEnd().TrimStart().ToLower() == "true");
                                break;
                            }                          
                        }
                    }
                    #endregion
                }
                return coZoneXmlJob;
            }
            catch (Exception ex)
            {
                TryMoveFolder(job.AbsoluteJobFolderPath, Path.Combine(GMGColorConfiguration.AppConfiguration.XMLJobsFailedFolder, Path.GetFileName(job.JobTempFolder)));
                var jobPath = job.OriginalJobPath + "\\" + job.AssetFile.Name;
                FTPLogger.Instance.Log(new LoggingException(String.Format("Could not process XML file {0} originated from {1}", job.AbsoluteXMLFilePath, job.OriginalJobPath), ex), jobPath);
            }
            return null;
        }

        /// <summary>
        /// Tries to move a folder
        /// </summary>
        /// <param name="sourceDir"></param>
        /// <param name="destDir"></param>
        /// <returns></returns>
        public bool TryMoveFolder(string sourceDir, string destDir)
        {
            try
            {
                Directory.Move(sourceDir, destDir);
                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Could not move folder {0} to {1}", sourceDir, destDir), ex));
                return false;
            }
        }

        /// <summary>
        /// Creates CoZone Job from XMLJob
        /// </summary>
        /// <param name="job"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool CreateJob(XMLJob job, DbContextBL context)
        {
            try
            {
                #region Validations

                if (job.Modules == 0 || String.IsNullOrEmpty(job.JobName))
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Job for user {0} originated from folder {1} with XMLFile {2} could not be processed, Modules or JobName missing!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning));
                    return false;
                }

                #endregion

                #region Job CoZone Properties

                int? uploaderUserId = UserBL.GetUserId(job.UserGUID, context);

                if (uploaderUserId == 0)
                {
                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("Failed to retrieve user ID from database for user guid {0}", job.UserGUID), Severity.Error));
                }               
              
                Account account = AccountBL.GetAccountByUserGuid(job.UserGUID, context);
                string accountRegion = account.Region;

                string loggedUserTempFolderPath = User.GetUserTempFolderPath(uploaderUserId, account.ID, accountRegion);

                #endregion

                if ((job.Modules & (int)GMG.CoZone.Common.XMLJobType.Deliver) != 0)
                {
                    if (String.IsNullOrEmpty(job.Deliver.ColorProofCozoneInstanceName) || String.IsNullOrEmpty(job.Deliver.ColorProofCozoneWorkflow))
                    {
                        FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job for user {0} originated from folder {1} with XMLFile {2} could not be processed, ColorProofCozoneWorkflow or ColorProofInstance missing!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning), FTPXmlFeedBackConstants.MissingColorProofMsg, string.Format("{0}\\{1}", job.OriginalFolderPath, job.JobName));
                    }
                    else
                    {
                        if (CoZoneDeliverJob.ValidateDeliverJob(job, uploaderUserId.Value, account.ID, context))
                        {
                            // Build CoZone approval job
                            DeliverJob objDeliver = CoZoneDeliverJob.BuildDeliverJob(job, uploaderUserId.Value, account.ID, context);
                            if (objDeliver != null)
                            {
                                //if user is allowed to add jobs then the warningMessage will be empty
                                string warningMessage = PlansBL.CheckIfJobSubmissionAllowed(account, objDeliver.User, Common.AppModule.Deliver, 1, context);
                                if (!string.IsNullOrEmpty(warningMessage))
                                {
                                    FTPLogger.Instance.Log(new LoggingNotification(warningMessage, Severity.Warning));
                                    return false;
                                }

                                // Upload to AWS user temp folder
                                CopyAssetFileInUserTempFolder(job.AssetFilePath, objDeliver.Guid, loggedUserTempFolderPath, accountRegion);
                                context.SaveChanges();
                                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Deliver Job with ID {0} created !", objDeliver.ID), Severity.Info));
                                //if build job with success save changes and add collaborators to approval
                                CoZoneDeliverJob.MoveDeliverFile(loggedUserTempFolderPath, objDeliver.FileName, objDeliver.Guid, accountRegion);
                                CoZoneDeliverJob.CreateQueueMessages(objDeliver, account.ID, job.Deliver.ColorProofCozoneInstanceName, context);
                                NotificationServiceBL.CreateNotification(new GMGColorNotificationService.NewDeliverJob
                                                                {
                                                                    EventCreator = uploaderUserId,
                                                                    InternalRecipient = uploaderUserId,
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    JobsGuids = new [] {objDeliver.Guid}
                                                                },
                                                                objDeliver.User,
                                                                context
                                                                );
                            }
                        }
                    }
                }
               
                //Check if Collaborate module is set in XML
                if ((job.Modules & (int)XMLJobType.Collaborate) != 0)
                {
                    //Check if an existing file with same name from same source ftp folder exists
                    FileUploadDuplicateName? duplicateOption = CoZoneCollaborateJob.GetFileDuplicateName(job.FileName, uploaderUserId.Value, job.JobSourceFolderRelativeToUserFolder, context);
                    if (duplicateOption.HasValue && duplicateOption == FileUploadDuplicateName.Ignore)
                    {
                        FTPLogger.Instance.Log(new LoggingNotification(String.Format("Job for user {0} originated from folder {1} with XMLFile {2} was not processed, DulicateFile found and duplicateFileName option was set to ignore, do not upload file", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), Severity.Warning));
                    }
                    else
                    {
                        if (CoZoneCollaborateJob.ValidateCollaborateJob(job, uploaderUserId.Value, account, context))
                        {
                            // Build CoZone approval job
                            Approval objApproval = CoZoneCollaborateJob.BuildCollaborateJob(job, uploaderUserId.Value, account, context, account.UploadEngineAccountSetting.EnableVersionMirroring, duplicateOption);
                            if (objApproval != null)
                            {
                                // Upload to AWS user temp folder
                                CopyAssetFileInUserTempFolder(job.AssetFilePath, objApproval.Guid, loggedUserTempFolderPath, accountRegion);
                                //if build job with success save changes and add collaborators to approval
                                context.SaveChanges();
                                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Collaborate Job with ID {0} created !", objApproval.ID), Severity.Info));
                                CoZoneCollaborateJob.SetApprovalCollaborators(objApproval, account.ID, job.Collaborate.InternalCollaborator, job.Collaborate.ExternalCollaborator, context);
                                CoZoneCollaborateJob.ScheduleApprovalForEmail(objApproval.ID);
                            }
                        }
                    }
                }

               
                //Remove temp folder
               FTPServerCleanup.Instance.DeleteFolder(Path.GetDirectoryName(job.AssetFilePath));
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                string validationException = DALUtils.GetDbEntityValidationException(ex);
                FTPLogger.Instance.Log(new LoggingException(String.Format("Create Job for user {0} originated from folder {1} with XMLFile {2} failed! with validation message {3}", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath, validationException), ex));
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Create Job for user {0} originated from folder {1} with XMLFile {2} failed!", job.UserGUID, job.OriginalFolderPath, job.XMLFilePath), ex));
            }

            return true;
        }

        /// <summary>
        /// Copy Asset File From Processing Temp Folder
        /// </summary>
        /// <param name="assetFilePath"></param>
        /// <param name="loggedUserTempFolderPath"></param>
        /// <param name="accountRegion"></param>
        /// <returns></returns>
        public bool CopyAssetFileInUserTempFolder(string assetFilePath, string guidFolder, string loggedUserTempFolderPath, string accountRegion)
        {
            try
            {
                string relativeFolderPath = loggedUserTempFolderPath + guidFolder + "/";
                string relativeFilePath = relativeFolderPath + Path.GetFileName(assetFilePath);
                if (GMGColorIO.FolderExists(relativeFolderPath, accountRegion, true))
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        GMGColor.AWS.AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), assetFilePath, relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    }
                    else
                    {
                        string destinationFolderPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFolderPath + "/";
                        string destinationFilePath = destinationFolderPath + Path.GetFileName(assetFilePath);
                        File.Copy(assetFilePath, destinationFilePath);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Could not copy assef tile {0} in user temp folder {1}!", assetFilePath, loggedUserTempFolderPath), ex));
                return false;
            }
        }

        #endregion
    }
}
