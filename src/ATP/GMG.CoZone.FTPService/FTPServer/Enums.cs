﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.FTPService
{
    /// <summary>
    /// Ftp directory type
    /// </summary>
    public enum FTPDirectoryType
    {
        Invalid = 0,
        User,
        Profile,
        Job
    }
}
