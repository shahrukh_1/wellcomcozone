﻿using System;
using System.Reflection;
using Amazon.CloudFront.Model;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorDAL;

namespace GMG.CoZone.FTPService
{
    /// <summary>
    /// FTP logger singletone wrapper
    /// </summary>
    public class FTPLogger
    {
        #region Private Data
        private static FTPLogger _instance;
        private readonly Logger _log;
        #endregion

        #region Properties
        public static FTPLogger Instance
        {
            get { return _instance ?? (_instance = new FTPLogger()); }
        }
        #endregion

        #region Ctors

        private FTPLogger()
        {
            _log = new Logger(GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.ActiveConfiguration + " " + GMGColorConfiguration.AppConfiguration.AWSRegion
                                                                                       : String.Empty
                              , "DBErrorLogger", "FTPServiceLogger");
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Log notification
        /// </summary>
        /// <param name="notification"></param>
        public void Log(LoggingNotification notification)
        {
            _log.Log(notification);
        }

        /// <summary>
        /// Log notification used to create an XML file with user feedback
        /// </summary>
        /// <param name="notification"></param>
        public void Log(LoggingNotification notification, string friendlyMessage, string filePath, bool isOutsideJobFolder = false)
        {
            _log.Log(notification);
            if (notification.Severity != Severity.Info && notification.Severity != Severity.Debug)
            {
                FTPXmlFeedBack.GenerateFeedBack(friendlyMessage, filePath, isOutsideJobFolder);
            }
        }
        
        /// <summary>
        /// Log event
        /// </summary>
        /// <param name="ftpEvent"></param>
        public void Log(LoggingEvent ftpEvent)
        {
            _log.Log(ftpEvent);
        }

        /// <summary>
        /// Log exception
        /// </summary>
        /// <param name="exception"></param>
        public void Log(LoggingException exception)
        {
            _log.Log(exception);
        }


        /// <summary>
        /// Log exception
        /// </summary>
        /// <param name="exception"></param>
        public void Log(LoggingException exception, string filePath)
        {
            _log.Log(exception);
            FTPXmlFeedBack.GenerateFeedBack(FTPXmlFeedBackConstants.DefaultErrorMsg, filePath);
        }
        #endregion
    }
}
