﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using System.Xml.Linq;

namespace GMG.CoZone.FTPService
{
    public class FTPJob
    {
        #region Properties 

        public int FtpPresetDownloadId { get; set; }

        public AppModule JobType { get; set; }

        public FTPAction ActionType { get; set; }

        public string GroupKey { get; set; }

        public int ApprovalId { get; set; }

        #endregion

        public FTPJob(string queueMessage)
        {
            try
            {
                XDocument message = XDocument.Parse(queueMessage);

                if (message.Descendants(GMG.CoZone.Common.Constants.FTPPresetJobID).FirstOrDefault() != null)
                {
                    FtpPresetDownloadId = Convert.ToInt32(message.Descendants(GMG.CoZone.Common.Constants.FTPPresetJobID).FirstOrDefault().Value);
                }
                if (message.Descendants(GMG.CoZone.Common.Constants.FTPAction).FirstOrDefault() != null)
                {
                    ActionType = (FTPAction)Enum.Parse(typeof(FTPAction), message.Descendants(GMG.CoZone.Common.Constants.FTPAction).FirstOrDefault().Value);
                }
                if (message.Descendants(GMG.CoZone.Common.Constants.JobMessageType).FirstOrDefault() != null)
                {
                    JobType = (AppModule)Enum.Parse(typeof(AppModule), message.Descendants(GMG.CoZone.Common.Constants.JobMessageType).FirstOrDefault().Value);
                }
                if (message.Descendants(GMG.CoZone.Common.Constants.FTPGroupKey).FirstOrDefault() != null)
                {
                    GroupKey = message.Descendants(GMG.CoZone.Common.Constants.FTPGroupKey).FirstOrDefault().Value;
                }
                if (message.Descendants(GMG.CoZone.Common.Constants.ApprovalMsgApprovalID).FirstOrDefault() != null)
                {
                    ApprovalId = Convert.ToInt32(message.Descendants(GMG.CoZone.Common.Constants.ApprovalMsgApprovalID).FirstOrDefault().Value);
                }
            }
            catch (Exception ex)
            {
                FTPClient.Log("Exception when creating a FTPJob object from message queue message", ex);
            }
            
        }
    }
}
