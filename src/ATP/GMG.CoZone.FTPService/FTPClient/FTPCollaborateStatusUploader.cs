﻿using System;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using GMG.CoZone.Component;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.IO;
using System.Threading;
using GMG.CoZone.Component.Logging;
using GMGColorDAL.Common;


namespace GMG.CoZone.FTPService
{
    public class FTPCollaborateStatusUploader
    {
        /// <summary>
        /// Get the required information for creating the XML file for the approval status change
        /// </summary>
        /// <param name="approvalId">The ID of the approval for which the status was updated</param>
        /// <returns>Custom object that has all the required data</returns>
        private FTPStatus CreateFtpStatusMessage(int approvalId, DbContextBL context)
        {
            var statusInfo = new FTPStatus();

            var accDetails = (from app in context.Approvals
                                join us in context.Users on app.Creator equals us.ID
                                join acc in context.Accounts on us.Account equals acc.ID
                                join l in context.Locales on us.Locale equals l.ID
                                where app.ID == approvalId
                                select new
                                {
                                    us.ID,
                                    us.Locale,
                                    l.Name
                            }).FirstOrDefault();
            Thread.CurrentThread.CurrentCulture =
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(accDetails.Name);

            var customDecisions = ApprovalCustomDecision.GetCustomDecisionsByAccountId(accDetails.ID, accDetails.Locale, context);
            var xmlData = (from a in context.Approvals
                            join jb in context.Jobs on a.Job equals jb.ID
                            join us in context.Users on a.Creator equals us.ID
                            join acc in context.Accounts on us.Account equals acc.ID
                            join df in context.DateFormats on acc.DateFormat equals df.ID
                            join tf in context.TimeFormats on acc.TimeFormat equals tf.ID
                            let internalCollaborators = (from ac in context.ApprovalCollaborators
                                                        join u in context.Users on ac.Collaborator equals u.ID
                                                        where ac.Approval == a.ID
                                                        select new
                                                        {
                                                            UserName = u.GivenName + " " + u.FamilyName,
                                                            Email = u.EmailAddress,
                                                            Sent =  ac.AssignedDate,
                                                            Opened = (from auv in context.ApprovalUserViewInfoes
                                                                        where auv.Approval == approvalId && auv.User == ac.Collaborator
                                                                        orderby auv.ViewedDate descending
                                                                        select auv.ViewedDate).FirstOrDefault(),
                                                            DecisionMade = (from acd in context.ApprovalCollaboratorDecisions
                                                                            join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                                            where acd.Collaborator == u.ID && acd.Approval == approvalId
                                                                            select new
                                                                            {
                                                                                ad.Key,
                                                                                ad.Name
                                                                            }).FirstOrDefault(),
                                                            IsExternal = false
                                                        }).ToList()

                            let externalCollaborators = (from sh in context.SharedApprovals
                                                        join exc in context.ExternalCollaborators on sh.ExternalCollaborator equals exc.ID
                                                        join acde in context.ApprovalCollaboratorDecisions on exc.ID equals acde.ExternalCollaborator
                                                        where sh.Approval == a.ID && sh.IsExpired == false && acde.Approval == a.ID
                                                        select new
                                                        {
                                                            UserName = exc.GivenName + " " + exc.FamilyName,
                                                            Email = exc.EmailAddress,
                                                            Sent = sh.SharedDate,
                                                            Opened = (from o in context.ApprovalCollaboratorDecisions
                                                                        where sh.Approval == approvalId && o.ExternalCollaborator == sh.ExternalCollaborator
                                                                        orderby o.AssignedDate descending
                                                                        select o.OpenedDate).FirstOrDefault() ?? DateTime.MinValue,
                                                            DecisionMade = (from acd in context.ApprovalCollaboratorDecisions
                                                                            join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                                            where acd.Approval == approvalId && acd.ExternalCollaborator == exc.ID
                                                                            select new
                                                                            {
                                                                                ad.Key,
                                                                                ad.Name
                                                                            }).FirstOrDefault(),
                                                            IsExternal = true
                                                        }).ToList()
                            let jobStatusKey = (from j in context.Jobs
                                            join js in context.JobStatus on j.Status equals js.ID
                                            where a.Job == j.ID
                                            select js.Key).FirstOrDefault()
                            where a.ID == approvalId
                            select new 
                            {
                                DeadLine = a.Deadline,
                                UserActitivy = internalCollaborators.Union(externalCollaborators).ToList(),
                                JobStatusKey = jobStatusKey,
                                ApprovalVersion = a.Version,
                                jb.Account,
                                JobName = jb.Title,
                                GUID = us.Guid,
                                FtpJobSourceFolder = a.FTPSourceFolder,
                                acc.TimeZone,
                                DateFormat = df.Pattern,
                                TimeFormat = tf.ID
                            }).FirstOrDefault();

            if (xmlData != null)
            {
                //Check if Retoucher role is enabled
                var isRetoucherEnabled = CheckIsRetoucherEnabled(xmlData.Account, context);

                //Populate status info object
                statusInfo.JobName = xmlData.JobName;
                statusInfo.DeadLine = (xmlData.DeadLine.Value == DateTime.MinValue) ? "N/A" : FormatDateTimeBasedOnUserSettings(xmlData.DeadLine.Value, xmlData.TimeZone, xmlData.DateFormat, xmlData.TimeFormat);
                statusInfo.UserActitivy = xmlData.UserActitivy.Select(u => new FTPCollaboratorStatusUserActivity()
                {
                    UserName = u.UserName,
                    Email = u.Email,
                    Sent = FormatDateTimeBasedOnUserSettings(u.Sent, xmlData.TimeZone, xmlData.DateFormat, xmlData.TimeFormat),
                    Opened = (u.Opened == DateTime.MinValue) ? "N/A" : FormatDateTimeBasedOnUserSettings(u.Opened, xmlData.TimeZone, xmlData.DateFormat, xmlData.TimeFormat),
                    DecisionMade = (u.DecisionMade == null) ? "N/A" : customDecisions.Any(d => d.Key == u.DecisionMade.Key && !string.IsNullOrEmpty(d.Name)) ? customDecisions.FirstOrDefault(d => d.Key == u.DecisionMade.Key).Name : ApprovalDecision.GetLocalizedApprovalDecision(u.DecisionMade.Key),
                    isExternal = u.IsExternal
                }).ToList();
                statusInfo.Account = xmlData.Account;
                statusInfo.GUID = xmlData.GUID;
                statusInfo.FtpJobSourceFolder = xmlData.FtpJobSourceFolder;
                statusInfo.ApprovalVersion = xmlData.ApprovalVersion;
                var appStatus = ApprovalBL.GetApprovapStatus(approvalId, xmlData.Account, context);
                if (!string.IsNullOrEmpty(appStatus))
                {
                    statusInfo.ApprovalStatus = GetApprovalStatusLabel(appStatus, xmlData.GUID, context) ??
                                                FTPJobStatusConstants.DefaultDecision;
                }
                else
                {
                    statusInfo.ApprovalStatus = "N/A";
                }
                statusInfo.JobStatus = JobStatu.GetLocalizedJobStatus(JobStatu.GetJobStatus(xmlData.JobStatusKey), isRetoucherEnabled);
            }
            
            return statusInfo;
        }
        
        /// <summary>
        /// Convert date time based on user time zone and selected format
        /// </summary>
        /// <param name="date"></param>
        /// <param name="timeZone"></param>
        /// <param name="datePattern"></param>
        /// <param name="timeFormat"></param>
        /// <returns></returns>
        private string FormatDateTimeBasedOnUserSettings(DateTime date, string timeZone, string datePattern, int timeFormat)
        {
            var appDate = GMGColorFormatData.GetUserTimeFromUTC(date, timeZone);
            var appTime = GMGColorFormatData.GetFormattedTime(appDate, timeFormat);
            return GMGColorFormatData.GetFormattedDate(appDate, datePattern) + ", " + appTime;
        }

        /// <summary>
        /// Based on the user locale get the correct translation for the approval status. Use default if no translation is available
        /// </summary>
        /// <param name="appStatusKey">The approval status Key</param>
        /// <param name="userGuid">The users guid used to calculate the language</param>
        /// <param name="context">GMGColorContext</param>
        /// <returns></returns>
        private string GetApprovalStatusLabel(string appStatusKey, string userGuid, GMGColorContext context)
        {
            var account = (from a in context.Accounts
                           join u in context.Users on a.ID equals u.Account
                           where u.Guid == userGuid
                           select new
                           {
                               AccountId = a.ID,
                               Local = u.Locale
                           }).FirstOrDefault();
            var customDecision = (from ad in context.ApprovalDecisions
                                  join acd in context.ApprovalCustomDecisions on ad.ID equals acd.Decision
                                  where acd.Account == account.AccountId && acd.Locale == account.Local && ad.Key == appStatusKey
                                  select acd.Name
                    ).FirstOrDefault();
            if (customDecision != null)
            {
                return customDecision;
            }

            return ApprovalDecision.GetLocalizedApprovalDecision(appStatusKey);           
        }

        /// <summary>
        /// Check if the option Retoucher Workflow is enabled on the account in order to set the JobStatus
        /// </summary>
        /// <param name="approvalId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool CheckIsRetoucherEnabled(int accId, GMGColorContext context)
        {
            var isRetoucherEnabled = (from ac in context.Accounts
                join acs in context.AccountSettings on ac.ID equals acs.Account
                where ac.ID == accId && acs.Name == FTPJobStatusConstants.EnableRetoucher
                select new
                {
                    acs.Name,
                    acs.Value
                }).FirstOrDefault();

            if (isRetoucherEnabled == null)
            {
                return false;
            }

            return Convert.ToBoolean(isRetoucherEnabled.Value);
        }

        /// <summary>
        /// Create the Status XML with the provided information
        /// </summary>
        /// <param name="statusInfo">Database information</param>
        /// <returns></returns>
        private XDocument CreateStatusXML(FTPStatus statusInfo)
        {
            return new XDocument(
                new XElement(FTPJobStatusConstants.CoZoneJob,
                    new XElement(FTPJobStatusConstants.Header,new XElement(FTPJobStatusConstants.JobName, statusInfo.JobName)),
                    new XElement(FTPJobStatusConstants.CollaborateStatus,
                        new XElement(FTPJobStatusConstants.CollaborateDeadline, statusInfo.DeadLine),
                        new XElement(FTPJobStatusConstants.UserActivity, statusInfo.UserActitivy.Select(ua =>
                            new XElement(FTPJobStatusConstants.User,
                                new XElement(FTPJobStatusConstants.UserName, ua.UserName),
                                new XElement(FTPJobStatusConstants.Email, ua.Email),
                                new XElement(FTPJobStatusConstants.Sent, ua.Sent),
                                new XElement(FTPJobStatusConstants.Opened, ua.Opened),
                                new XElement(FTPJobStatusConstants.DecisionMade, ua.DecisionMade),
                                new XElement(FTPJobStatusConstants.IsExternal, ua.isExternal)
                                ))),
                        new XElement(FTPJobStatusConstants.JobStatus, statusInfo.JobStatus),
                        new XElement(FTPJobStatusConstants.ApprovalVersion, statusInfo.ApprovalVersion),
                        new XElement(FTPJobStatusConstants.ApprovalStatus, statusInfo.ApprovalStatus)
                        )));
        }

        /// <summary>
        /// Generated the XML file name based on the required parameters
        /// </summary>
        /// <param name="jobName">JobName</param>
        /// <param name="approvalVersion">Max approval version</param>
        /// <param name="approvalStatus">Calculated approval status</param>
        /// <param name="guid">Approval GUID</param>
        /// <returns></returns>
        private string GenerateXmlFileName(string jobName, int approvalVersion, string approvalStatus, string guid)
        {
            if (approvalStatus == "N/A")
            {
                return string.Format("{0}.v{1}.{2}.xml", jobName, approvalVersion, guid.Substring(0, 7));
            }
            else 
            {
                return string.Format("{0}.v{1}.{2}.{3}.xml", jobName, approvalVersion, approvalStatus, guid.Substring(0, 7));
            }
        }

        /// <summary>
        /// Build file path for the Status XML
        /// </summary>
        /// <param name="root">FTP Root</param>
        /// <param name="GUID">User GUID</param>
        /// <param name="ftpSourceFolder">Approval source folder</param>
        /// <param name="jobName">Job Name</param>
        /// <returns></returns>
        private string GenerateJobFolderPath(string root, string GUID, string ftpSourceFolder)
        {
            return string.Format("{0}\\{1}\\{2}\\{3}\\{4}", root, GUID, ftpSourceFolder, FTPJobStatusConstants.JobStatuses, FTPJobStatusConstants.CollaborateModule);
        }

        /// <summary>
        /// Create required folders and upload the XML file
        /// </summary>
        /// <param name="statusJob"></param>
        /// <param name="statusJobXmlDocument"></param>
        private void UploadFTPJobStatus(FTPStatus statusJob, XDocument statusJobXmlDocument)
        {
            var statusJobXmlFileName = GenerateXmlFileName(statusJob.JobName, statusJob.ApprovalVersion, statusJob.ApprovalStatus, statusJob.GUID);
            var statusJobPath = GenerateJobFolderPath(GMGColorConfiguration.AppConfiguration.FTPRootFolder, statusJob.GUID, statusJob.FtpJobSourceFolder);
            try
            {
                if (!Directory.Exists(statusJobPath))
                {
                    Directory.CreateDirectory(statusJobPath);
                }
                statusJobXmlDocument.Save(Path.Combine(statusJobPath, statusJobXmlFileName));
                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Saved xml file in folder {0} with name {1}", statusJobPath, statusJobXmlFileName), Severity.Info));
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Failed to save job {0}", statusJob.JobName), ex));
            }
        }

        private bool ValidateUserFolderStructure(int approvalId, DbContextBL context)
        {
            var userFolders =(from a in context.Approvals
                                join us in context.Users on a.Creator equals us.ID
                                where a.ID == approvalId
                                select new
                                {
                                    a.FTPSourceFolder,
                                    us.Guid
                                }).FirstOrDefault();

            return XMLEngineBL.ValidateUserFolder(userFolders.Guid, context) && 
                XMLEngineBL.ValidateProfileFolder(userFolders.FTPSourceFolder.Substring(0, userFolders.FTPSourceFolder.IndexOf("\\")), userFolders.Guid, context);            
        }

        /// <summary>
        /// Public method that is called by the FTP client
        /// </summary>
        /// <param name="approvalId"></param>
        public void ProcessUploadStatus(int approvalId)
        {
            using (var context = new DbContextBL())
            {
                try
                {
                    if (ValidateUserFolderStructure(approvalId, context))
                    {
                        var statusInfo = CreateFtpStatusMessage(approvalId, context);
                        var statusXml = CreateStatusXML(statusInfo);
                        UploadFTPJobStatus(statusInfo, statusXml);
                    }
                    else
                    {
                        FTPLogger.Instance.Log(
                        new LoggingNotification(
                            String.Format("Status Xml was not uploaded for approval {0} because folder structure is not valid", approvalId), Severity.Warning));
                    }
                }
                catch (Exception ex)
                {
                    FTPLogger.Instance.Log(
                        new LoggingException(
                            String.Format("Failed to Upload FTP Job Status for approval {0}", approvalId), ex));
                }              
            }
        }
    }
}
