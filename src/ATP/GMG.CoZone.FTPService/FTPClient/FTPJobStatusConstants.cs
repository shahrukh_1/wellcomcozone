﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.FTPService
{
    public static class FTPJobStatusConstants
    {
        public static string CoZoneJob { get { return "CoZoneJob"; }}
        public static string Header { get { return "Header"; } }
        public static string JobName { get { return "JobName"; } }
        public static string CollaborateStatus { get { return "CollaborateStatus"; } }
        public static string CollaborateDeadline { get { return "CollaborateDeadline"; } }
        public static string UserActivity { get { return "UserActivity"; } }
        public static string User { get { return "User"; } }
        public static string UserName { get { return "UserName"; } }
        public static string Email { get { return "Email"; } }
        public static string Sent { get { return "Sent"; } }
        public static string Opened { get { return "Opened"; } }
        public static string DecisionMade { get { return "DecisionMade"; } }
        public static string IsExternal { get { return "IsExternal"; } }
        public static string JobStatus { get { return "JobStatus"; } }
        public static string ApprovalVersion { get { return "ApprovalVersion"; } }
        public static string ApprovalStatus { get { return "ApprovalStatus"; } }
        public static string JobStatuses { get { return "JobStatuses"; } }
        public static string CollaborateModule { get { return "Collaborate"; } }
        public static string EnableRetoucher { get { return "EnableRetoucherWorkflow"; } }
        public static string DefaultDecision { get { return "Pending"; } }
        public static string ErrorsFolder { get { return "Errors"; } }
    }
}
