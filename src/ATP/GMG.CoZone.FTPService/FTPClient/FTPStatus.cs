﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Profile;
using GMGColorDAL;

namespace GMG.CoZone.FTPService
{
    public class FTPStatus
    {
        public string JobName { get; set; }
        public string DeadLine { get; set; }
        public List<FTPCollaboratorStatusUserActivity> UserActitivy { get; set; }
        public string JobStatus { get; set; }
        public int ApprovalVersion { get; set; }
        public string ApprovalStatus { get; set; }
        public int Account { get; set; }
        public string GUID { get; set; }
        public string FtpJobSourceFolder { get; set; }
    }
}
