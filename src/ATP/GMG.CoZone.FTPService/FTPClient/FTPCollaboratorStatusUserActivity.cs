﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Profile;

namespace GMG.CoZone.FTPService
{
    public class FTPCollaboratorStatusUserActivity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Sent { get; set; }
        public string Opened { get; set; }
        public string DecisionMade { get; set; }
        public bool isExternal { get; set; }
    }
}
