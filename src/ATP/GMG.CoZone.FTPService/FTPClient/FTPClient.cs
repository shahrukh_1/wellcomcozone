﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CoZone.Component.Logging;
using System.Reflection;
using System.Threading;
using GMG.CoZone.Component;
using GMGColorDAL;
using GMGColor.AWS;
using System.Messaging;
using Amazon.SQS.Model;
using GMG.CoZone.Common;
using System.IO;
using GMGColorDAL.Common;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Web;

namespace GMG.CoZone.FTPService
{
    public class FTPClient
    {
        #region Private data

        private CancellationTokenSource _ftpClientTaskCTS = new CancellationTokenSource();

        private MessageQueue _mq;

        private int _timePeriodFTPJobQueue = 0;

        #endregion

        #region Properties 

        private int FTPJobQueueVisibilityTimePeriod
        {
            get
            {
                if (_timePeriodFTPJobQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorConfiguration.AppConfiguration.FTPJobQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSRegion);

                        _timePeriodFTPJobQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        FTPLogger.Instance.Log(new LoggingException("FTP Service unable to get the SQS FTPJobQueueName attributes. Exception", ex));
                    }

                    if (_timePeriodFTPJobQueue < 60)
                    {
                        _timePeriodFTPJobQueue = 60; // 60 Seconds
                    }
                }

                return _timePeriodFTPJobQueue;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// FTP Client worker thread
        /// </summary>
        public void Start()
        {
            FTPLogger.Instance.Log(new LoggingNotification("FTP Client started", Severity.Info));

            if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
            {
                FTPLogger.Instance.Log(new LoggingNotification("Is SQS setup", Severity.Info));
            }
            else
            {
                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Is local setup"), Severity.Info));
            }

            while (!_ftpClientTaskCTS.Token.IsCancellationRequested)
            {
                try
                {
                    bool queueIsEmpty = false;
                    string ftpQueueName = GMGColorConfiguration.AppConfiguration.FTPJobQueueName;

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                    {
                        ReceiveMessageResponse oResponse = AWSSimpleQueueService.CheckQueueForMessages(ftpQueueName, 10, FTPJobQueueVisibilityTimePeriod,
                                                                                                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                                                        GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                                                        GMGColorConfiguration.AppConfiguration.AWSRegion);
                        if (oResponse.Messages.Count > 0)
                        {
                            foreach (Amazon.SQS.Model.Message processMsg in oResponse.Messages)
                            {
                                try
                                {
                                    FTPJob job = new FTPJob(processMsg.Body);

                                    if (job.ActionType == FTPAction.UploadAction)
                                    {
                                        UploadFTPJob(job);
                                    }
                                    else if (job.ActionType == FTPAction.UploadStatus)
                                    {
                                        var uploader = new FTPCollaborateStatusUploader();
                                        uploader.ProcessUploadStatus(job.ApprovalId);
                                    }

                                    AWSSimpleQueueService.DeleteMessage(GMGColorConfiguration.AppConfiguration.FTPJobQueueName, processMsg, GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                            GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
                                }
                                catch (Exception ex)
                                {
                                    FTPLogger.Instance.Log(new LoggingException(MessageConstants.ProcessingAmazonSQSMessageFiled, ex));
                                }
                            }
                        }
                        else
                        {
                            queueIsEmpty = true;
                        }
                    }
                    else
                    {
                        string message;
                        try
                        {
                            _mq = new MessageQueue(ftpQueueName);
                            System.Messaging.Message msg = _mq.Receive(new TimeSpan(0, 0, 3));
                            msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                            message = msg.Body.ToString();

                            FTPJob job = new FTPJob(message);

                            if (job.ActionType == FTPAction.UploadAction)
                            {
                                UploadFTPJob(job);
                            }
                            else if (job.ActionType == FTPAction.UploadStatus)
                            {
                                var uploader = new FTPCollaborateStatusUploader();
                                uploader.ProcessUploadStatus(job.ApprovalId);
                            }
                        }
                        catch (MessageQueueException msqsEx)
                        {
                            if (msqsEx.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                queueIsEmpty = true;
                            }
                            else
                            {
                                FTPLogger.Instance.Log(new LoggingException(MessageConstants.ReadFromFTPQueueFail, msqsEx));
                            }
                        }
                    }

                    // Sleep thread for 1 seconds
                    if (queueIsEmpty)
                    {
                        Thread.Sleep(100 * 10); // wait 1 second between syncs
                    }
                }
                catch (Exception ex)
                {
                    FTPLogger.Instance.Log(new LoggingException(MessageConstants.FTPClientStartFailed, ex));
                }
            }

            FTPLogger.Instance.Log(new LoggingNotification("FTP Client stopped", Severity.Info));
        }

        /// <summary>
        /// Stops FTP Client worker Task
        /// </summary>
        public void Stop()
        {
            _ftpClientTaskCTS.Cancel();
        }

        /// <summary>
        /// Log method
        /// </summary>
        /// <param name="message">proper message associated with the log</param>
        /// <param name="ex">The Exception that was thrown</param>
        public static void Log(string message, Exception ex)
        {
            FTPLogger.Instance.Log(new LoggingException(message, ex));
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Uploads the specified job to the specified FTP Server
        /// </summary>
        /// <param name="ftpJob"></param>
        private void UploadFTPJob(FTPJob ftpJob)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    List<FTPPresetJobDownload> jobsToUpload = new List<FTPPresetJobDownload>();

                    if (ftpJob.GroupKey != null)
                    {
                        jobsToUpload = (from fpj in context.FTPPresetJobDownloads
                                        where fpj.GroupKey == ftpJob.GroupKey
                                        select fpj).ToList();

                        if (jobsToUpload.Count == 0)
                        {
                            throw new Exception(String.Format("FTPPresetDownload Jobs with GroupKey {0} not found in Database!", ftpJob.GroupKey));
                        }
                    }
                    else
                    {
                        FTPPresetJobDownload job = (from fpj in context.FTPPresetJobDownloads
                                                    where fpj.ID == ftpJob.FtpPresetDownloadId
                                                    select fpj).SingleOrDefault();

                        if (job == null)
                        {
                            throw new Exception(String.Format("FTPPresetDownload Job with ID {0} not found in Database!", ftpJob.FtpPresetDownloadId));
                        }

                        jobsToUpload.Add(job);
                    }

                    UploadEnginePreset jobPreset = jobsToUpload.FirstOrDefault().UploadEnginePreset;

                    if (jobPreset == null)
                    {
                        throw new Exception(String.Format("JobPreset for Job with ID {0} not found in Database!", ftpJob.FtpPresetDownloadId));
                    }

                    bool archiveFiles = (from ueset in context.UploadEngineAccountSettings
                                         join ac in context.Accounts on ueset.ID equals ac.UploadEngineSetings
                                         join pr in context.UploadEnginePresets on ac.ID equals pr.Account
                                         where pr.ID == jobPreset.ID
                                         select ueset.ZipFilesWhenDownloading).FirstOrDefault();

                    ZipOutputStream zipStream = null;
                    MemoryStream memoryZipStream = null;
                    zipStream.UseZip64 = UseZip64.Off;

                    if (archiveFiles)
                    {
                        memoryZipStream = new MemoryStream();
                        zipStream = new ZipOutputStream(memoryZipStream);
                    }

                    string moduleFolderName = String.Empty;

                    switch(ftpJob.JobType)
                    {
                        case GMG.CoZone.Common.AppModule.Collaborate:
                            moduleFolderName = Constants.CollaborateFolderName;
                            break;
                        case GMG.CoZone.Common.AppModule.Deliver:
                            moduleFolderName = Constants.DeliverFolderName;
                            break;                       
                    }

                    CreateFTPFolderIfNotExist(jobPreset, moduleFolderName);

                    string region = GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.AWSRegion : String.Empty;

                    foreach (FTPPresetJobDownload job in jobsToUpload)
                    {
                        FTPLogger.Instance.Log(new LoggingNotification(String.Format("Started processing FTPDownload Job with ID {0}", job.ID), Severity.Debug));
                        try
                        {
                            #region GetFile

                            string relativeFilePath = String.Empty;
                            string fileName = String.Empty;

                            switch (ftpJob.JobType)
                            {
                                case GMG.CoZone.Common.AppModule.Collaborate:
                                    {
                                        Approval approvalJob = (from aj in context.Approvals
                                                                where aj.ID == job.ApprovalJob
                                                                select aj).SingleOrDefault();

                                        string folderName = moduleFolderName + "/";

                                        if (approvalJob == null)
                                        {
                                            throw new Exception(String.Format("Approval Job with ID {0} not found in Database!", job.ApprovalJob));
                                        }

                                        //Replicate CoZone Folders, create folders for file downloading, create path in case of archive
                                        if (jobPreset.ReplicateCoZoneFolders && approvalJob.Folders.Any())
                                        {
                                            Folder objFolder = approvalJob.Folders.FirstOrDefault();

                                            CreateApprovalFolder(jobPreset, objFolder, ref folderName, !archiveFiles, context);
                                        }

                                        relativeFilePath = Path.Combine(GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath, approvalJob.Guid, approvalJob.FileName);

                                        //Gets VersionName Settings for the current account
                                        var versionNameData = (from ueset in context.UploadEngineAccountSettings
                                                              join ac in context.Accounts on ueset.ID equals ac.UploadEngineSetings
                                                              join pr in context.UploadEnginePresets on ac.ID equals pr.Account
                                                              join vn in context.UploadEngineVersionNames on ueset.VersionName equals vn.ID
                                                              where pr.ID == jobPreset.ID
                                                              select new
                                                              {
                                                                  Key = vn.Key,
                                                                  PositionFromStart = ueset.VersionNumberPositionFromStart,
                                                                  PositionInName = ueset.VersionNumberPositionInName
                                                              }).FirstOrDefault();

                                        string approvalName = approvalJob.FileName;

                                        //If settings have been found handle version name accordingly
                                        if (versionNameData != null)
                                        {
                                            string extension = approvalJob.FileName.Substring(approvalJob.FileName.LastIndexOf("."));
                                            string originalFileNameWithoutExtension = approvalJob.FileName.Substring(0, approvalJob.FileName.LastIndexOf("."));
                                            approvalName = originalFileNameWithoutExtension;

                                            switch (versionNameData.Key)
                                            {
                                                case (int)GMG.CoZone.Common.VersionUploadName.KeepOriginalName:
                                                    break;
                                                case (int)GMG.CoZone.Common.VersionUploadName.AppendSuffix_vX:
                                                    approvalName = approvalName + "_v" + approvalJob.Version;
                                                    break;
                                                case (int)GMG.CoZone.Common.VersionUploadName.AppendSuffixvX:
                                                    approvalName = approvalName + "v" + approvalJob.Version;
                                                    break;
                                                case (int)GMG.CoZone.Common.VersionUploadName.AppendvXAtPosition:
                                                    approvalName = GetVersionInName(approvalName, versionNameData.PositionFromStart.Value, versionNameData.PositionInName.Value, approvalJob.Version);
                                                    break;
                                            }

                                            approvalName += extension;
                                        }

                                        //Remove ModuleFolderName but keep the folders hierarchy in case files are to be added to archive
                                        fileName = archiveFiles ? folderName.Remove(folderName.IndexOf(moduleFolderName), moduleFolderName.Length + 1) + HttpUtility.UrlPathEncode(approvalName) :
                                                                folderName + HttpUtility.UrlPathEncode(approvalName);
                                    }
                                    break;
                                case GMG.CoZone.Common.AppModule.Deliver:
                                    {
                                        DeliverJob deliverJob = (from dj in context.DeliverJobs
                                                                 where dj.ID == job.DeliverJob
                                                                 select dj).SingleOrDefault();

                                        if (deliverJob == null)
                                        {
                                            throw new Exception(String.Format("DeliverJob Job with ID {0} not found in Database!", job.DeliverJob));
                                        }

                                        relativeFilePath = Path.Combine(GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath, deliverJob.Guid, deliverJob.FileName);

                                        //Remove ModuleFolderName in case files are to be added to archive
                                        fileName = archiveFiles ? HttpUtility.UrlPathEncode(deliverJob.FileName) :
                                                                 moduleFolderName + "/" + HttpUtility.UrlPathEncode(deliverJob.FileName);
                                    }
                                    break;                               
                            }

                            Stream fileStream = GMGColorIO.GetMemoryStreamOfAFile(relativeFilePath, region);

                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Retrieved fileStream from S3 for Job with ID {0}", job.ID), Severity.Debug));

                            #endregion

                            #region Upload to FTP or Add to Archive

                            if (archiveFiles)
                            {
                                ZipEntry zipFile = new ZipEntry(fileName);
                                zipStream.PutNextEntry(zipFile);

                                StreamUtils.Copy(fileStream, zipStream, new byte[4096]);
                                zipStream.CloseEntry();
                                fileStream.Close();
                                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Job with ID {0} added to archive", job.ID), Severity.Debug));
                            }
                            else
                            {
                                string response = String.Empty;
                                fileName = GetUniqueFileName(jobPreset, fileName);
                                FTPLogger.Instance.Log(new LoggingNotification(String.Format("Started sending uploadrequest for Job with ID {0} and name {1}", job.ID, fileName), Severity.Debug));
                                if (SendFTPUploadRequest(jobPreset, fileStream, fileName, out response))
                                {
                                    job.Status = true;
                                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("File {0} with FTPPresetDownloadJobID {1} uploaded to FTP with success!", fileName, job.ID), Severity.Info));
                                    Console.WriteLine(String.Format("File {0} with FTPPresetDownloadJobID {1} uploaded to FTP with success!\n", fileName, job.ID));
                                }
                                else
                                {
                                    job.Status = false;
                                    FTPLogger.Instance.Log(new LoggingNotification(String.Format("File {0} with FTPPresetDownloadJobID {1} upload to FTP failed!", fileName, job.ID), Severity.Error));
                                    Console.WriteLine(String.Format("File {0} with FTPPresetDownloadJobID {1} upload to FTP failed!\n", fileName, job.ID));
                                }
                                job.StatusDescription = response;
                            }

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            FTPLogger.Instance.Log(new LoggingException(String.Format("Processing FTPDownloadJob with ID {0} failed", job.ID), ex));
                        }

                        FTPLogger.Instance.Log(new LoggingNotification(String.Format("Finished processing FTPDownload Job with ID {0}", job.ID), Severity.Debug));
                    }

                    #region Upload Archive

                    if (archiveFiles)
                    {
                        zipStream.IsStreamOwner = false;
                        zipStream.Close();
                        memoryZipStream.Position = 0;

                        string response = String.Empty;
                        string archiveName = GetUniqueFileName(jobPreset, moduleFolderName + "/" + Constants.CoZoneZipName);
                        if (SendFTPUploadRequest(jobPreset, memoryZipStream, archiveName, out response))
                        {
                            jobsToUpload.ForEach(t => t.Status = true);
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Archive {0} uploaded to FTP with success!", archiveName), Severity.Info));
                            Console.WriteLine(String.Format("Archive {0} uploaded to FTP with success!", archiveName));
                        }
                        else
                        {
                            jobsToUpload.ForEach(t => t.Status = false);
                            FTPLogger.Instance.Log(new LoggingNotification(String.Format("Archive {0} upload to FTP failed!", archiveName), Severity.Error));
                            Console.WriteLine(String.Format("Archive {0} upload to FTP failed!", archiveName));
                        }
                        jobsToUpload.ForEach(t => t.StatusDescription = response);
                    }

                    #endregion

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Upload FTPDownloadJob with ID {0} or GroupKey {1} failed", ftpJob.FtpPresetDownloadId, ftpJob.GroupKey ?? String.Empty), ex));
            }
        }

        /// <summary>
        /// Upload the Specified Stream to the FTP
        /// </summary>
        /// <param name="jobPreset">Preset containing FTP Server connection details</param>
        /// <param name="uploadStream">Stream to be uploaded</param>
        /// <param name="fileName">Name of the file to be uploaded</param>
        /// <param name="responseDescription">Response from FTP Server</param>
        /// <returns>Whether Upload succeeds or not</returns>
        private bool SendFTPUploadRequest(UploadEnginePreset jobPreset, Stream uploadStream, string fileName, out string responseDescription)
        {
            try
            {
                FtpWebRequest request = CreateFTPRequest(jobPreset, fileName);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                request.UseBinary = true;
                request.ContentLength = uploadStream.Length;

                Stream requestStream = request.GetRequestStream();
                uploadStream.CopyTo(requestStream);

                requestStream.Close();
                uploadStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                responseDescription = response.StatusDescription;

                response.Close();

                return true;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("SendFTPUploadRequest with filename {0} failed", fileName), ex));
                responseDescription = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Checkes whether the file exists or not on the FTP Server
        /// </summary>
        /// <param name="jobPreset">Preset containing FTP Server connection details</param>
        /// <param name="fileName">The file to be checked</param>
        /// <returns></returns>
        private bool FileExistsOnFTP(UploadEnginePreset jobPreset, string fileName)
        {
            try
            {
                FtpWebRequest request = CreateFTPRequest(jobPreset, fileName);

                request.Method = WebRequestMethods.Ftp.GetFileSize;

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse());

            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    return false;
                }
                throw ex;
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("FileExistsOnFTP with filename {0} failed", fileName), ex));
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// Returns the Valid FileName that is unique on the FTPServer
        /// </summary>
        /// <param name="jobPreset">Preset containing FTP Server connection details</param>
        /// <param name="fileName">The file to be checked</param>
        /// <returns></returns>
        private string GetUniqueFileName(UploadEnginePreset jobPreset, string fileName)
        {
            try
            {
                string extension = fileName.Substring(fileName.LastIndexOf("."));
                string originalFileNameWithoutExtension = fileName.Substring(0, fileName.LastIndexOf("."));
                string originalFileName = fileName;

                while (FileExistsOnFTP(jobPreset, fileName))
                {
                    if (fileName == originalFileName)
                    {
                        fileName = String.Format("{0}({1}){2}", originalFileNameWithoutExtension, 1, extension);
                    }
                    else
                    {
                        string fileNameWithoutExtension = fileName.Substring(0, fileName.LastIndexOf("."));
                        string index = fileNameWithoutExtension.Substring(originalFileNameWithoutExtension.Length); //get currentIndex in "(number)" format
                        index = index.Remove(index.Length - 1, 1).Remove(0, 1); // Remove () and keep only the number
                        int currentIndex = Convert.ToInt32(index);
                        currentIndex++;
                        fileName = String.Format("{0}({1}){2}", originalFileNameWithoutExtension, currentIndex, extension);
                    }
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("GetUniqueFileName with filename {0} failed", fileName), ex));
            }

            return fileName;
        }

        /// <summary>
        /// Creates a Ftp Web request using the specified settings in the preset
        /// </summary>
        /// <param name="jobPreset">Upload Engine Preset</param>
        /// <param name="path">Path to be added to the request url</param>
        /// <param name="moduleFolder">Folder for CoZone Module</param>
        /// <returns></returns>
        private FtpWebRequest CreateFTPRequest(UploadEnginePreset jobPreset, string path)
        {
            string url = String.Empty;
            if (jobPreset != null)
            {
                url = "ftp://" + jobPreset.Host + ":" + jobPreset.Port;
                if (!String.IsNullOrEmpty(jobPreset.DefaultDirectory))
                {
                    url = url + "/" + HttpUtility.UrlPathEncode(jobPreset.DefaultDirectory);
                }
            }
            url = url + "/" + path;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);

            if (!String.IsNullOrEmpty(jobPreset.Username))
            {
                request.Credentials = new NetworkCredential(jobPreset.Username, GMG.CoZone.Common.Utils.Decrypt(jobPreset.Password, GMG.CoZone.Common.Constants.EncryptDecryptPrivateKeyXMLEnginePreset, true));
            }

            if (jobPreset.FTPProtocol.Key == (int)GMG.CoZone.Common.FTPProtocol.FTPS)
            {
                request.EnableSsl = true;
            }

            if (!jobPreset.IsActiveTransferMode)
            {
                request.UsePassive = true;
            }

            return request;
        }

        /// <summary>
        /// Creates the specified folder if not present
        /// </summary>
        /// <param name="jobPreset">Preset containing FTP Server connection details</param>
        /// <param name="folderName">Name of the new folder to be created</param>
        /// <returns>Whether the folder is created or not</returns>
        private bool CreateFTPFolderIfNotExist(UploadEnginePreset jobPreset, string folderName)
        {
            try
            {
                FtpWebRequest request = CreateFTPRequest(jobPreset, folderName);

                request.Method = WebRequestMethods.Ftp.MakeDirectory;

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse()) ;

                return true;
            }
            catch(WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    return false; //folder already exists
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException("Create Folder on FTP Failed!", ex));
                return false;
            }
        }

        /// <summary>
        /// Create approval folder structure hierarchy
        /// </summary>
        /// <param name="jobPreset">Upload Engine Preset</param>
        /// <param name="objFolder">Folder object</param>
        /// <param name="folderName">folderName</param>
        /// <param name="createFolders">In case of archives don't create folder, just create path for entry in archive</param>
        /// <param name="context"></param>
        private void CreateApprovalFolder(UploadEnginePreset jobPreset, Folder objFolder, ref string folderName, bool createFolders, GMGColorContext context)
        {
             Folder objParentFolder = (from fl in context.Folders
                                        where fl.ID == objFolder.Parent
                                        select fl).SingleOrDefault();

            if(objParentFolder != null)
            {
                CreateApprovalFolder(jobPreset, objParentFolder, ref folderName, createFolders, context);
            }

            folderName += HttpUtility.UrlPathEncode(objFolder.Name);
            if (createFolders)
            {
                CreateFTPFolderIfNotExist(jobPreset, folderName);
            }
            folderName += "/";
        }

        /// <summary>
        /// Gets Version File Name inside File Name
        /// </summary>
        /// <param name="approvalName">Original File Name</param>
        /// <param name="isFromStart">Is Position From Start</param>
        /// <param name="position">Postition in string</param>
        /// <param name="version">Version number</param>
        /// <returns>formatted name</returns>
        private string GetVersionInName(string approvalName, bool isFromStart, int position, int version)
        {
            try
            {
                string versionSubstring = "_v" + version;
                if (position > approvalName.Length)
                {
                    approvalName = approvalName + versionSubstring;
                }
                else
                {
                    if (isFromStart)
                    {
                        approvalName = approvalName.Substring(0, position) + versionSubstring + approvalName.Substring(position);
                    }
                    else
                    {
                        approvalName = approvalName.Substring(0, approvalName.Length - position) + versionSubstring + approvalName.Substring(approvalName.Length - position);
                    }
                }
            }
            catch (Exception ex)
            {
                FTPLogger.Instance.Log(new LoggingException(String.Format("Failed to GetVersionInName for File {0}", approvalName) , ex));
            }

            return approvalName;
        }

        #endregion
    }
}
