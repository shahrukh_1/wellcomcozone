﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GMG.CoZone.SoftProofingDataService.Workers;
using GMGColorDAL;
using GMGColor.AWS;

namespace GMG.CoZone.SoftProofingDataService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine("CoZone Email Service Started !\nPress Any key to exit");

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    AWSClient.Init(new AWSSettings() {Region = GMGColorConfiguration.AppConfiguration.AWSRegion});
                }

                var checkNewSoftProofingJobsTask = new Task(ProcessCalibrationData.ProcessNewSoftProofingCalibrationData);
                checkNewSoftProofingJobsTask.Start();

                var checkDeletedSoftProofingJobsTask = new Task(ProcessCalibrationData.ProcessDeletedSoftProofingCalibrationData);
                checkDeletedSoftProofingJobsTask.Start();

                var processNewSimulationProfiles = new Task(ProcessSoftProofingRequests.CheckSoftProofingQueue);
                processNewSimulationProfiles.Start();

               

                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }

                ProcessCalibrationData.StopCheckSoftProofingCalibrationData();
                ProcessSoftProofingRequests.StopCheckSPQueue();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			{ 
				new SoftProofingDataService() 
			};
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
