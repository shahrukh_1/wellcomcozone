﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GMG.CoZone.Common;

namespace GMG.CoZone.SoftProofingDataService.Models
{
    public class SoftProofingMetadata
    {
        #region Properties

        public int ProfileId { get; set; }

        public int PaperTintId { get; set; }

        public SoftProofingAction Action { get; set; }

        #endregion

        #region Constructors

        public SoftProofingMetadata(string queueMessage)
        {
            try
            {
                XDocument message = XDocument.Parse(queueMessage);

                if (message.Descendants(Constants.ProfileId).FirstOrDefault() != null)
                {
                    ProfileId = Convert.ToInt32(message.Descendants(GMG.CoZone.Common.Constants.ProfileId).FirstOrDefault().Value);
                }
                if (message.Descendants(Constants.Action).FirstOrDefault() != null)
                {
                    Action = (SoftProofingAction)Enum.Parse(typeof(SoftProofingAction), message.Descendants(GMG.CoZone.Common.Constants.Action).FirstOrDefault().Value);
                }
                if (message.Descendants(Constants.PaperTindId).FirstOrDefault() != null)
                {
                    PaperTintId = Convert.ToInt32(message.Descendants(GMG.CoZone.Common.Constants.PaperTindId).FirstOrDefault().Value);
                }
            }
            catch (Exception ex)
            {
                ServiceLog.Log("Exception when creating a FTPJob object from message queue message", ex);
            }
        }

        #endregion
    }
}
