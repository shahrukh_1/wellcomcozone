﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColor.AWS;
using Amazon.SQS.Model;
using System.Messaging;
using System.Xml.Linq;
using GMG.CoZone.Common;
using GMGColorDAL.Common;

namespace GMG.CoZone.SoftProofingDataService.Workers
{
    public class ProcessCalibrationData
    {
        #region Properties

        private static readonly CancellationTokenSource _checkSoftProofingJobsTaskCTS = new CancellationTokenSource();

        private static int _timePeriodWriteQueue = 0;

        private static int _timePeriodReadQueue = 0;

        private static int WriteQueueVisibilityTimePeriod
        {
            get
            {
                if (_timePeriodWriteQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationWriteQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSRegion);

                        _timePeriodWriteQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        ServiceLog.Log("FTP Service unable to get the SQS SoftProofingQueue attributes. Exception", ex);
                    }

                    if (_timePeriodWriteQueue < 60)
                    {
                        _timePeriodWriteQueue = 60; // 60 Seconds
                    }
                }

                return _timePeriodWriteQueue;
            }
        }

        private static int ReadQueueVisibilityTimePeriod
        {
            get
            {
                if (_timePeriodReadQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationReadQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSRegion);

                        _timePeriodReadQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        ServiceLog.Log("FTP Service unable to get the SQS SoftProofingQueue attributes. Exception", ex);
                    }

                    if (_timePeriodReadQueue < 60)
                    {
                        _timePeriodReadQueue = 60; // 60 Seconds
                    }
                }

                return _timePeriodReadQueue;
            }
        }

        #endregion

        #region Public Methods

        public static void ProcessNewSoftProofingCalibrationData()
        {
            while (!_checkSoftProofingJobsTaskCTS.IsCancellationRequested)
            {
                try
                {
                    string softProofingCalibrationWriteQueueName = GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationWriteQueueName;

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                    {
                        ProcessSoftProofingCalibrationDataFromAWSQueue(softProofingCalibrationWriteQueueName);
                    }
                    else
                    {
                        ProcessSoftProofingCalibrationDataFromLocalQueue(softProofingCalibrationWriteQueueName);
                    }
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("ProcessNewSoftProofingCalibrationData method failed", ex);
                }
                finally
                {
                    Thread.Sleep(500);
                }
            }
        }

        private static void ProcessSoftProofingCalibrationDataFromAWSQueue(string readQueueName)
        {
            ReceiveMessageResponse oResponse =
                            AWSSimpleQueueService.CheckQueueForMessages(readQueueName, 10,
                                WriteQueueVisibilityTimePeriod,
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                GMGColorConfiguration.AppConfiguration.AWSRegion);
            if (oResponse.Messages.Count > 0)
            {
                foreach (Amazon.SQS.Model.Message processMsg in oResponse.Messages)
                {
                    try
                    {
                        XDocument sqsMessage = XDocument.Parse(processMsg.Body);

                        if (sqsMessage.Descendants(Constants.WorkstationCalibrationDataGuid).FirstOrDefault() != null)
                        {
                            var workstationCalibrationDataGuid = sqsMessage.Descendants(GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid).FirstOrDefault().Value;
                            SoftProofingBL.ProcessNewSoftProofingJobs(workstationCalibrationDataGuid);

                            //create define queue message for validation of icc profile
                            Dictionary<string, string> dictJobParameters = new Dictionary<string, string>();
                            dictJobParameters.Add(GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid, workstationCalibrationDataGuid);

                            GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.SoftProofingCalibrationRead);
                        }

                        AWSSimpleQueueService.DeleteMessage(
                            readQueueName,
                            processMsg,
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                            GMGColorConfiguration.AppConfiguration.AWSRegion);

                    }

                    catch (Exception ex)
                    {
                        ServiceLog.Log(MessageConstants.ProcessingAmazonSQSMessageFiled, ex);
                    }
                }
            }
        }

        private static void ProcessSoftProofingCalibrationDataFromLocalQueue(string readQueueName)
        {
            string message;
            try
            {
                var mq = new MessageQueue(readQueueName);
                System.Messaging.Message msg = mq.Receive(new TimeSpan(0, 0, 3));
                msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

                var mqMessage = XDocument.Parse(msg.Body.ToString());

                if (mqMessage.Descendants(Constants.WorkstationCalibrationDataGuid).FirstOrDefault() != null)
                {
                    var workstationCalibrationDataGuid = mqMessage.Descendants(GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid).FirstOrDefault().Value;
                    SoftProofingBL.ProcessNewSoftProofingJobs(workstationCalibrationDataGuid);
                    //create define queue message for validation of icc profile
                    Dictionary<string, string> dictJobParameters = new Dictionary<string, string>();
                    dictJobParameters.Add(GMG.CoZone.Common.Constants.WorkstationCalibrationDataGuid, workstationCalibrationDataGuid);

                    GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.SoftProofingCalibrationRead);
                }
            }
            catch (MessageQueueException msqsEx)
            {
                if (msqsEx.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                {
                    ServiceLog.Log(MessageConstants.ReadFromFTPQueueFail, msqsEx);
                }
            }
        }

        public static void ProcessDeletedSoftProofingCalibrationData()
        {
            while (!_checkSoftProofingJobsTaskCTS.IsCancellationRequested)
            {
                try
                {
                    SoftProofingBL.ProcessDeletedSoftProofingJobs();
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("ProcessDeletedSoftProofingCalibrationData method failed", ex);
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                }
            }
        }

        public static void StopCheckSoftProofingCalibrationData()
        {
            _checkSoftProofingJobsTaskCTS.Cancel();
        }

        #endregion
    }
}
