﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GMG.CoZone.Common;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.SoftProofingDataService.Models;
using GMGColor.AWS;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.Messaging;
using Amazon.SQS.Model;
using System.Threading.Tasks;

namespace GMG.CoZone.SoftProofingDataService.Workers
{
    public class ProcessSoftProofingRequests
    {
        #region Properties

        private static readonly CancellationTokenSource _checkNewSimulationProfilesCTS = new CancellationTokenSource();

        private static int _timePeriodQueue = 0;

        private static int SoftProofingQueueVisibilityTimePeriod
        {
            get
            {
                if (_timePeriodQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorConfiguration.AppConfiguration.SoftProofingQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSRegion);

                        _timePeriodQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        ServiceLog.Log("FTP Service unable to get the SQS SoftProofingQueue attributes. Exception", ex);
                    }

                    if (_timePeriodQueue < 60)
                    {
                        _timePeriodQueue = 60; // 60 Seconds
                    }
                }

                return _timePeriodQueue;
            }
        }

        #endregion

        #region Public Methods

        public static void CheckSoftProofingQueue()
        {
            MessageQueue mq;
            while (!_checkNewSimulationProfilesCTS.IsCancellationRequested)
            {
                try
                {
                    ServiceLog.Log(new LoggingNotification("CheckSoftProofingQueue() started",
                        Severity.Debug));
                    bool queueIsEmpty = false;
                    string softProofingQueueName = GMGColorConfiguration.AppConfiguration.SoftProofingQueueName;

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                    {
                        ReceiveMessageResponse oResponse =
                            AWSSimpleQueueService.CheckQueueForMessages(softProofingQueueName, 10,
                                SoftProofingQueueVisibilityTimePeriod,
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                GMGColorConfiguration.AppConfiguration.AWSRegion);
                        if (oResponse.Messages.Count > 0)
                        {
                            foreach (Amazon.SQS.Model.Message processMsg in oResponse.Messages)
                            {
                                try
                                {
                                    var spmsg = new SoftProofingMetadata(processMsg.Body);
                                    if (spmsg.Action == SoftProofingAction.ReadProfileWhitePoint)
                                    {
                                        var readProfileWhitePointTask = new Task(() => SetProfileWhitePointLab(spmsg.ProfileId));
                                        readProfileWhitePointTask.Start();
                                    }
                                    else if (spmsg.Action == SoftProofingAction.ValidateCustomProfile)
                                    {
                                        var validateCustomProfile = new Task(() => ValidateCustomICCProfile(spmsg.ProfileId));
                                        validateCustomProfile.Start();
                                    }

                                    AWSSimpleQueueService.DeleteMessage(
                                        GMGColorConfiguration.AppConfiguration.SoftProofingQueueName,
                                        processMsg,
                                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                        GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                        GMGColorConfiguration.AppConfiguration.AWSRegion);
                                }
                                catch (Exception ex)
                                {
                                    ServiceLog.Log(MessageConstants.ProcessingAmazonSQSMessageFiled, ex);
                                }
                            }
                        }
                        else
                        {
                            queueIsEmpty = true;
                        }
                    }
                    else
                    {
                        string message;
                        try
                        {
                            mq = new MessageQueue(softProofingQueueName);
                            System.Messaging.Message msg = mq.Receive(new TimeSpan(0, 0, 3));
                            msg.Formatter = new XmlMessageFormatter(new String[] {"System.String,mscorlib"});
                            message = msg.Body.ToString();

                            var spmsg = new SoftProofingMetadata(message);
                            if (spmsg.Action == SoftProofingAction.ReadProfileWhitePoint)
                            {
                                var readProfileWhitePointTask = new Task(() => SetProfileWhitePointLab(spmsg.ProfileId));
                                readProfileWhitePointTask.Start();
                            }
                            else if (spmsg.Action == SoftProofingAction.ValidateCustomProfile)
                            {
                                var validateCustomProfile = new Task(() => ValidateCustomICCProfile(spmsg.ProfileId));
                                validateCustomProfile.Start();
                            }
                        }
                        catch (MessageQueueException msqsEx)
                        {
                            if (msqsEx.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                queueIsEmpty = true;
                            }
                            else
                            {
                                ServiceLog.Log(MessageConstants.ReadFromFTPQueueFail, msqsEx);
                            }
                        }
                    }

                    // Sleep thread for 1 seconds
                    if (queueIsEmpty)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(2)); // wait 1 second between syncs
                    }
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("ProcessSoftProofingCalibrationData method failed", ex);
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                }
            }
        }

        public static void StopCheckSPQueue()
        {
            _checkNewSimulationProfilesCTS.Cancel();
        }

        #endregion

        #region Private

        static void SetProfileWhitePointLab(int profileId)
        {
            try
            {
                SoftProofingBL.SetProfileWhitePointLab(profileId);
            }
            catch (Exception ex)
            {
                ServiceLog.Log(String.Format("SetProfileWhitePointLab method for profile {0} failed", profileId), ex);
            }

        }

        static void ValidateCustomICCProfile(int profileId)
        {
            try
            {
                SoftProofingBL.ValidateCustomICCProfile(profileId);
            }
            catch (Exception ex)
            {
                ServiceLog.Log(String.Format("ValidateCustomICCProfile method for profile {0} failed", profileId), ex);
            }

        }
        #endregion
    }
}
