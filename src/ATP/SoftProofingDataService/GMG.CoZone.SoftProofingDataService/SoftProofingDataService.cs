﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using GMG.CoZone.SoftProofingDataService.Workers;
using GMGColor.AWS;
using GMGColorDAL;

namespace GMG.CoZone.SoftProofingDataService
{
    public partial class SoftProofingDataService : ServiceBase
    {
        private Task _processNewCalibrationData;
        private Task _processDeletedCalibrationData;
        private Task _checkNewSimulationProfiles;

        public SoftProofingDataService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                AWSClient.Init(new AWSSettings { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }

            _processNewCalibrationData = new Task(ProcessCalibrationData.ProcessNewSoftProofingCalibrationData);
            _processNewCalibrationData.Start();

            _processDeletedCalibrationData = new Task(ProcessCalibrationData.ProcessDeletedSoftProofingCalibrationData);
            _processDeletedCalibrationData.Start();

            _checkNewSimulationProfiles = new Task(ProcessSoftProofingRequests.CheckSoftProofingQueue);
            _checkNewSimulationProfiles.Start();
        }

        protected override void OnStop()
        {
            ProcessCalibrationData.StopCheckSoftProofingCalibrationData();
            ProcessSoftProofingRequests.StopCheckSPQueue();
        }
    }
}
