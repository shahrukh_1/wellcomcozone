﻿using GMG.CoZone.NotificationGenerationService.Workers;
using GMGColor.AWS;
using GMGColorDAL;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.NotificationGenerationService
{
    partial class CoZoneNotificationGenerationService : ServiceBase
    {
        private CancellationTokenSource _cts;
        private WorkerManager _workerManager;

        public CoZoneNotificationGenerationService(CancellationTokenSource cts, WorkerManager workerManager)
        {
            InitializeComponent();
            _cts = cts;
            _workerManager = workerManager;
        }

        protected override void OnStart(string[] args)
        {

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }

            Task.Factory.StartNew(_workerManager.RunMaintenaceTasks);
        }

        protected override void OnStop()
        {
            _cts.Cancel();
        }
    }
}
