﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace GMG.CoZone.NotificationGenerationService
{
    [RunInstaller(true)]
    public partial class NotificationGenerationServiceInstaller : System.Configuration.Install.Installer
    {
        public NotificationGenerationServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
