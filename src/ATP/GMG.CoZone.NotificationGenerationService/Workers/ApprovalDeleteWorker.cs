﻿using System;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.Threading;
using GMG.CoZone.Component;

namespace GMG.CoZone.NotificationGenerationService.Workers
{
    public class ApprovalDeleteWorker
    {
        private static readonly CancellationTokenSource _checkApprovalsToDelete = new CancellationTokenSource();

        public static void CheckApprovalsToDelete()
        {
            while (!_checkApprovalsToDelete.IsCancellationRequested)
            {
                try
                {
                    ServiceLog.Log(new LoggingNotification("DeleteArchivedApprovalsAfterSixMonths() started", Severity.Info));
                    string ApprovaslsDeletedFromArchive = ApprovalBL.DeleteArchivedApprovalsAfterSixMonths();
                    ServiceLog.Log(new LoggingNotification("Approvals Deleted from Archive - "+ ApprovaslsDeletedFromArchive + "  DeleteArchivedApprovalsAfterSixMonths() finished", Severity.Info));

                    ServiceLog.Log(new LoggingNotification("DeleteRecycleBinApprovalsAfterOneMonth() started", Severity.Info));
                    string ApprovalsDeletedFromRecycleBin = ApprovalBL.DeleteRecycleBinApprovalsAfterOneMonth();
                    ServiceLog.Log(new LoggingNotification("Approvals Deleted from RecycleBin - "+ ApprovalsDeletedFromRecycleBin +"  DeleteRecycleBinApprovalsAfterOneMonth() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("CheckApprovalsToDelete method failed", ex);
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromDays(1));
                }
            }
        }

    }
}
