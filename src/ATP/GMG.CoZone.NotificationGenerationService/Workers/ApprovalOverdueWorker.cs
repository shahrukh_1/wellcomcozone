﻿using System;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.Threading;
using GMG.CoZone.Component;

namespace GMG.CoZone.NotificationGenerationService.Workers
{
    public class ApprovalOverdueWorker
    {
        private static readonly CancellationTokenSource _checkApprovalOverdueTaskCTS = new CancellationTokenSource();

        public static void CheckApprovalOverdue()
        {
            int inLastNoOfDays = GMGColorConfiguration.AppConfiguration.ApprovalsOverdueByNoOfDays;
            while (!_checkApprovalOverdueTaskCTS.IsCancellationRequested)
            {
                try
                {
                    ServiceLog.Log(new LoggingNotification("CheckApprovalOverdue() started", Severity.Info));

                    ApprovalBL.CheckApprovalsOverdue(inLastNoOfDays);
                    ApprovalBL.CheckPhaseOverdue(inLastNoOfDays);

                    inLastNoOfDays = 1;

                    ServiceLog.Log(new LoggingNotification("CheckApprovalOverdue() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("CheckApprovalsOverdue method failed", ex);
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromMinutes(30));
                }
            }
        }
    }
}
