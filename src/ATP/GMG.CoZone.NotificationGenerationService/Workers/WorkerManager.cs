﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.NotificationGenerationService.Workers
{
    public class WorkerManager
    {
        private CancellationTokenSource _cts;

        public WorkerManager(CancellationTokenSource cts)
        {
            _cts = cts;
        }
        
        /// <summary>
        /// Run Notification Generation Service operations one after another waiting for each to finish
        /// </summary>
        public void RunMaintenaceTasks()
        {
            try
            { 
               Parallel.Invoke(() =>
                               {
                                   DeliverJobsTimeOutWorker.CheckTimedOutDeliverJobs();
                                   CancelMaintenanceTask(_cts.Token);
                               }, 
                               () =>
                               {
                                    ExpiredDemoPlanstWorker.CheckExpiredDemoPlans();
                                    CancelMaintenanceTask(_cts.Token);
                               },
                               () =>
                               {
                                    ApprovalOverdueWorker.CheckApprovalOverdue();
                                    CancelMaintenanceTask(_cts.Token);
                               },
                               () =>
                               {
                                   NotificationItemMaintanceWorker.CheckNotificationItemsToDelete();
                                   CancelMaintenanceTask(_cts.Token);
                               },
                               () =>
                               {
                                   ApprovalDeleteWorker.CheckApprovalsToDelete();
                                   CancelMaintenanceTask(_cts.Token);
                               }
                    );           
            }
            catch (OperationCanceledException)
            {
                //Ignore task cancellation
            }
            catch (AggregateException e)
            {
                foreach (var v in e.InnerExceptions)
                {
                    ServiceLog.Log("Notification Generation Service service failed with the following error: ", v);
                }
            }   
        }

        /// <summary>
        /// Cancel task by throwing Operation Cancellation Exception
        /// </summary>
        /// <param name="token">The token used for the running task</param>
        private void CancelMaintenanceTask(CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
        }        
    }
}
