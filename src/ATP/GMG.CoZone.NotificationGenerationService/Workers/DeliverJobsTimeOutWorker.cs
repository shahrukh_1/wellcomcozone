﻿using System;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using System.Threading;

namespace GMG.CoZone.NotificationGenerationService.Workers
{
    public class DeliverJobsTimeOutWorker
    {
        private static readonly CancellationTokenSource _checkTimedOutDeliverJobsTaskCTS = new CancellationTokenSource();        

        public static void CheckTimedOutDeliverJobs()
        {
            while (!_checkTimedOutDeliverJobsTaskCTS.IsCancellationRequested)
            {
                try
                {
                    ServiceLog.Log(new LoggingNotification("CheckTimedOutDeliverJobs() started", Severity.Info));

                    DeliverBL.CheckDeliverJobs();

                    ServiceLog.Log(new LoggingNotification("CheckTimedOutDeliverJobs() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("CheckTimedOutDeliverJobs method failed", ex);
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromMinutes(10));
                }                   
            }
        }
    }
}
