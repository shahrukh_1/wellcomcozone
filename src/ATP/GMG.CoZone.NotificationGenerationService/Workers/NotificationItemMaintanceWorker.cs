﻿using System;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.Threading;
using GMG.CoZone.Component;

namespace GMG.CoZone.NotificationGenerationService.Workers
{
    public class NotificationItemMaintanceWorker
    {
        
        private static readonly CancellationTokenSource _checkNotificationItemsToDeleteTaskCTS = new CancellationTokenSource();

        public static void CheckNotificationItemsToDelete()
        {
            while (!_checkNotificationItemsToDeleteTaskCTS.IsCancellationRequested)
            {
                try
                {
                    
                    ServiceLog.Log(new LoggingNotification("DeleteNotificationItemsAfterOneWeek() started", Severity.Info));
                    GlobalNotificationBL.DeleteNotificationItemsAfterOneWeek();
                    ServiceLog.Log(new LoggingNotification("DeleteNotificationItemsAfterOneWeek() finished", Severity.Info));

                    ServiceLog.Log(new LoggingNotification("RemoveExpiredCollaborateAPISessions() started", Severity.Info));
                    ApprovalBL.RemoveExpiredCollaborateAPISessions();
                    ServiceLog.Log(new LoggingNotification("RemoveExpiredCollaborateAPISessions() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("CheckNotificationItemsToDelete method failed", ex);
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromDays(1));
                }
            }
        }
    }
}
