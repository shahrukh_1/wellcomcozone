﻿using System;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using System.Threading;
using GMG.CoZone.Component;

namespace GMG.CoZone.NotificationGenerationService.Workers
{
    public class ExpiredDemoPlanstWorker
    {
        private static readonly CancellationTokenSource _checkExpiredDemoPlansTaskCTS = new CancellationTokenSource();

        public static void CheckExpiredDemoPlans()
        {
            while (!_checkExpiredDemoPlansTaskCTS.IsCancellationRequested)
            {
                try
                {
                    ServiceLog.Log(new LoggingNotification("CheckExpiredDemoPlans() started", Severity.Info));
                    PlansBL.CheckDemoPlans();
                    ServiceLog.Log(new LoggingNotification("CheckExpiredDemoPlans() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("CheckExpiredDemoPlans method failed", ex);
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromDays(1));
                }
            }                
        }
    }
}
