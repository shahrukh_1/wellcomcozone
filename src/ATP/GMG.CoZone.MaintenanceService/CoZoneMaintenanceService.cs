﻿using System.ServiceProcess;
using System.Threading.Tasks;
using GMG.CoZone.Maintenance.Workers;
using GMGColorDAL;
using GMGColor.AWS;
using System.Threading;
using System;

namespace GMG.CoZone.Maintenance
{
    public partial class CoZoneMaintenanceService : ServiceBase
    {
        private CancellationTokenSource _cts;
        private WorkerManager _workerManager;

        public CoZoneMaintenanceService(CancellationTokenSource cts, WorkerManager workerManager)
        {
            InitializeComponent();
            _cts = cts;
            _workerManager = workerManager;
        }

        protected override void OnStart(string[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }

            Task.Factory.StartNew(_workerManager.RunMaintenaceTasks);

            /* HIGHLY DANGEROUS METHOD: takes all AWS S3 folders and loops through it by checking the db context approvals list, if S3 folder GUID is not found in approval guid list, then the folder is deleted from S3.
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                var deleteGarbageFolders = new Task(DeleteGarbageFoldersWorker.DeleteGarbageFolders);
                deleteGarbageFolders.Start();
            }*/
        }

        protected override void OnStop()
        {
            _cts.Cancel();
        }
    }
}
