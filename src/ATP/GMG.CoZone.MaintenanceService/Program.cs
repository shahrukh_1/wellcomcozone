﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GMG.CoZone.Maintenance.Workers;
using GMGColor.AWS;
using GMGColorDAL;
using GMGColorBusinessLogic.Common;
using GMG.CoZone.Component.Logging;
using GMG.CoZone.Component;

namespace GMG.CoZone.Maintenance
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure(); // configure log4net

            Console.WriteLine("CoZone Email Service Started !\nPress Any key to exit");

            //Create cancellaction token                
            CancellationTokenSource cts = new CancellationTokenSource();
            var token = cts.Token;
            var workerManager = new WorkerManager(cts);

            while (!token.IsCancellationRequested)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
                    }

                    if (!Console.KeyAvailable)
                    {
                        workerManager.RunMaintenaceTasks();
                    }
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[]
                    {
                        new CoZoneMaintenanceService(cts, workerManager)
                    };
                    ServiceBase.Run(ServicesToRun);
                }
            }

            cts.Cancel();
        }
    }
}
