﻿using System;
using System.Threading;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace GMG.CoZone.Maintenance.Workers
{
    public class DataBaseMantainanceWorker
    {
        private  CancellationTokenSource _doDataBaseMantainanceTaskCTS;

        public DataBaseMantainanceWorker(CancellationTokenSource cts)
        {
            _doDataBaseMantainanceTaskCTS = cts;
        }

        public void DoDataBaseMantainance()
        {
            while (!_doDataBaseMantainanceTaskCTS.IsCancellationRequested)
            {
                WaitTillNextIterration();

                try
                {
                    ServiceLog.Log(new LoggingNotification("DoDataBaseMantainance() started", Severity.Info));

                    using (DbContextBL context = new DbContextBL())
                    {
                        var databaseMaintenanceBL = new DatabaseMaintenanceBL();
                        databaseMaintenanceBL.ClearStoredProceduresCache(context);
                        databaseMaintenanceBL.UpdateStatistics(context);
                    }

                    ServiceLog.Log(new LoggingNotification("DoDataBaseMantainance() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("DoDataBaseMantainance method failed", ex);
                }

                CancelMaintenanceTask(_doDataBaseMantainanceTaskCTS.Token);
            }
        }

        /// <summary>
        /// Execute daily
        /// </summary>
        private void WaitTillNextIterration()
        {
            DateTime currentTime = DateTime.UtcNow;

            // Get time span to next iteration    
            DateTime nextDate = currentTime;
            if (nextDate.Hour >= GMGColorConfiguration.AppConfiguration.UTCHourForRunningRecreateDBIndexesTasks)
            {
                nextDate = currentTime.AddDays(1);
            }
            
            DateTime desiredTime = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, GMGColorConfiguration.AppConfiguration.UTCHourForRunningMaintenaceTasks, 0, 0);

            TimeSpan span = desiredTime - currentTime;
            Thread.Sleep(span);
        }


        /// <summary>
        /// Cancel task by throwing Operation Cancellation Exception
        /// </summary>
        /// <param name="token">The token used for the running task</param>
        private void CancelMaintenanceTask(CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
        }
    }
}
