﻿using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using System;
using System.Threading;

namespace GMG.CoZone.Maintenance.Workers
{
    public class RecreateDBIndexesWorker
    {
        private CancellationTokenSource _recreateDBIndexesWorkerTaskCTS;

        public RecreateDBIndexesWorker(CancellationTokenSource cts)
        {
            _recreateDBIndexesWorkerTaskCTS = cts;
        }

        public void RecreateDBIndexes()
        {
            while (!_recreateDBIndexesWorkerTaskCTS.IsCancellationRequested)
            {
                WaitTillNextIterration();

                try
                {
                    ServiceLog.Log(new LoggingNotification("RecreateDBIndexesWorker() started", Severity.Info));

                    using (DbContextBL context = new DbContextBL())
                    {
                        var databaseMaintenanceBL = new DatabaseMaintenanceBL();
                        databaseMaintenanceBL.RecreateDBIndexes(context);                      
                    }

                    ServiceLog.Log(new LoggingNotification("RecreateDBIndexesWorker() finished", Severity.Info));
                }
                catch (Exception ex)
                {
                    ServiceLog.Log("RecreateDBIndexes method failed", ex);
                }

                CancelMaintenanceTask(_recreateDBIndexesWorkerTaskCTS.Token);
            }
        }

        /// <summary>
        /// Based on configuration put thread to sleep
        /// </summary>
        private void WaitTillNextIterration()
        {
            DateTime currentTime = DateTime.UtcNow;

            // Get time span to next iteration
            DateTime nextDate = currentTime;
            if (nextDate.Hour >= GMGColorConfiguration.AppConfiguration.UTCHourForRunningRecreateDBIndexesTasks)
            {
                nextDate = currentTime.AddDays(1);
            }
            DateTime desiredTime = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, GMGColorConfiguration.AppConfiguration.UTCHourForRunningRecreateDBIndexesTasks, 0, 0);

            var timeSpan = desiredTime - currentTime;
            Thread.Sleep(timeSpan);
        }


        /// <summary>
        /// Cancel task by throwing Operation Cancellation Exception
        /// </summary>
        /// <param name="token">The token used for the running task</param>
        private void CancelMaintenanceTask(CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
        }
    }
}