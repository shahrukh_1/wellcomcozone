﻿using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.Maintenance.Workers
{
    public class WorkerManager
    {
        private CancellationTokenSource _cts;

        public WorkerManager(CancellationTokenSource cts)
        {
            _cts = cts;
        }
        
        /// <summary>
        /// Run maintenance operations one after another waiting for each to finish
        /// </summary>
        public void RunMaintenaceTasks()
        {
            try
            {
                Parallel.Invoke(() =>
                                {
                                    RecreateDBIndexesWorker recreateDBIndexesWorker = new RecreateDBIndexesWorker(_cts);
                                    recreateDBIndexesWorker.RecreateDBIndexes();
                                },
                                () =>
                                {
                                    DataBaseMantainanceWorker databaseMaintenanceWorker = new DataBaseMantainanceWorker(_cts);
                                    databaseMaintenanceWorker.DoDataBaseMantainance();
                                });
            }
            catch (OperationCanceledException)
            {
                //Ignore task cancellation
            }
            catch (AggregateException e)
            {
                foreach (var v in e.InnerExceptions)
                {
                    ServiceLog.Log("Maintenance service failed with the following error: ", v);
                }
            }   
        }
    }
}
