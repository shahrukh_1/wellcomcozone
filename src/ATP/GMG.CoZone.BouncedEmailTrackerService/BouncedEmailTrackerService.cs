﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using GMGColorDAL;
using GMGColor.AWS;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColorBusinessLogic;
using Newtonsoft.Json;

namespace GMG.CoZone.BouncedEmailTrackerService
{
    public class BouncedEmailTrackerService
    {
        #region Private Members
        private const string _SnsAttributeName = "DisplayName";
        private const string _SnsAttributeValue = "Notifications for BouncedEmails";
        private CancellationTokenSource _processBouncedEmailsTaskCTS = new CancellationTokenSource();
        private Task _processBouncedEmailsTask;
        #endregion

        #region Public Methods
        public BouncedEmailTrackerService()
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                GMGColor.AWS.AWSClient.Init(new GMGColor.AWS.AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
            }      
        }

        /// <summary>
        /// Start the Bounced Email Tracker Services
        /// </summary>
        public void StartBouncedEmailTrackerService()
        {
            //Set up AWS components to handle bounce notifications
            //ConfigureAwsComponents();

            //Start to process the bounced email queue
            _processBouncedEmailsTask = new Task(ProcessBouncedEmailQueue);
            _processBouncedEmailsTask.Start();
        }

        /// <summary>
        /// Stop the Bounced Email Tracker Services
        /// </summary>
        public void StopBouncedEmailTrackerService()
        {
            _processBouncedEmailsTaskCTS.Cancel();
        }

        /// <summary>
        /// Pool the queue and get bouncing email notifications and process them
        /// </summary>
        public void ProcessBouncedEmailQueue()
        {
            try
            {
                while (!_processBouncedEmailsTaskCTS.Token.IsCancellationRequested)
                {
                    var receiveMessageResponse = GMGColor.AWS.AWSSimpleQueueService.ReadMessages(GMGColorConfiguration.AppConfiguration.AWSSQSBounceQueue,
                                                                                                    GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                                                    GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                                                    GMGColorConfiguration.AppConfiguration.AWSRegion);

                    if (receiveMessageResponse.Messages.Count > 0)
                    {
                        foreach (var message in receiveMessageResponse.Messages)
                        {
                            // Convert Amazon SNS message into a JSON object
                            var notification = Newtonsoft.Json.JsonConvert.DeserializeObject<SESBounceNotification.AmazonSqsNotification>(message.Body);
                            
                            // Access the Amazon SES bounce notification.
                            var bounce = Newtonsoft.Json.JsonConvert.DeserializeObject<SESBounceNotification.AmazonSesBounceNotification>(notification.Message);

                            //Save message to database
                            foreach (var recipient in bounce.Bounce.BouncedRecipients)
                            {
                                //Only save bounce to database if the bounce is Permanent. Transient and Undetermined are not considered
                                if (bounce.Bounce != null && (bounce.Bounce.BounceType == Enum.GetName(typeof(AWSBounceTypes.BounceType),1)))
                                {
                                    SaveBouncedMessageToDB(recipient.EmailAddress, bounce.Bounce);
                                }
                            }

                            // After read, delete message
                            GMGColor.AWS.AWSSimpleQueueService.DeleteMessage(GMGColorConfiguration.AppConfiguration.AWSSQSBounceQueue, message, 
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                GMGColorConfiguration.AppConfiguration.AWSRegion);
                        }
                    }

                    Thread.Sleep(60 * 1000); // sleep for one minute
                }

            }
            catch (Exception ex)
            {
                ServiceLog.Log("ProcessBouncedEmailQueue exception: ", new LoggingException("ProcessBouncedEmailQueue failed", ex, Severity.Error));
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Configure required AWS components
        /// </summary>
        private void ConfigureAwsComponents()
        {
            try
            {
                //Check if Amazon SQS exists and create it if it does not
                var queueExists = AWSSimpleQueueService.CheckIfQueueExists(GMGColorConfiguration.AppConfiguration.AWSSQSBounceQueue,
                                                                            string.Empty,
                                                                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                            GMGColorConfiguration.AppConfiguration.AWSRegion);
                if (!queueExists)
                {
                    AWSSimpleQueueService.CreateQueue(GMGColorConfiguration.AppConfiguration.AWSSQSBounceQueue,
                                                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                        GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                        GMGColorConfiguration.AppConfiguration.AWSRegion);
                }

                //Create amazon SNS topic and configure it to publish to SQS queue
                var sqsARN = AWSSimpleQueueService.GetSQSArn(GMGColorConfiguration.AppConfiguration.AWSSQSBounceQueue,
                                                                AWSSimpleQueueService.SQSAttribute.QueueArn,
                                                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                GMGColorConfiguration.AppConfiguration.AWSRegion);

                AWSSimpleNotificationService.CreateSubscriber(GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                GMGColorConfiguration.AppConfiguration.AWSRegion,
                                                                GMGColorConfiguration.AppConfiguration.AWSSNSTopicName,
                                                                sqsARN,
                                                                "sqs",
                                                                _SnsAttributeName,
                                                                _SnsAttributeValue);
                
            }
            catch (Exception ex)
            {
                ServiceLog.Log("ConfigureAwsComponents exception: ", new LoggingException("ConfigureAwsComponents failed", ex, Severity.Error));
            }
        }

        /// <summary>
        /// Save the bounce email data to the database
        /// </summary>
        /// <param name="email">The user email of the bounce recipient</param>
        /// <param name="message">The body of the message</param>
        private void SaveBouncedMessageToDB(string email, SESBounceNotification.AmazonSesBounce bounce)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    var bouncedEmail = new GMGColorDAL.BouncedEmail();
                    if (!context.BouncedEmails.Any(t => t.Email.Equals(email)))
                    {
                        //Populate bounce object and add it to context
                        bouncedEmail.Email = email;
                        bouncedEmail.Message = JsonConvert.SerializeObject(bounce);
                        context.BouncedEmails.Add(bouncedEmail);   
                    }
                    else
                    {
                        bouncedEmail = context.BouncedEmails.Where(t => t.Email == email).FirstOrDefault();
                    }

                    MarkUsersWithBouncedEmail(email, context, bouncedEmail);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ServiceLog.Log("SaveBouncedMessage exception: ", new LoggingException("SaveBouncedMessage failed", ex, Severity.Error));
            }
        }

        /// <summary>
        /// Mark both interal and external users with bounced email
        /// </summary>
        /// <param name="email">Users email</param>
        /// <param name="context"></param>
        /// <param name="bouncedEmail">The bounced email content</param>
        private static void MarkUsersWithBouncedEmail(string email, DbContextBL context, BouncedEmail bouncedEmail)
        {
            //Get the bounced email user and save the message ID
            var internalUsers = context.Users.Where(u => u.EmailAddress == email).ToList();
            var externalUsers = context.ExternalCollaborators.Where(u => u.EmailAddress == email).ToList();

            if (internalUsers != null)
            {
                internalUsers.ForEach(u => u.BouncedEmail = bouncedEmail);
            }

            if (externalUsers != null)
            {
                externalUsers.ForEach(u => u.BouncedEmail = bouncedEmail);
            }
        }
        #endregion
    }
}
