﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GMG.CoZone.BouncedEmailTrackerService
{
    partial class CoZoneBouncedEmailTrackerService : ServiceBase
    {
        #region Private Members
        private BouncedEmailTrackerService _bouncedEmailTrackerService;
        private Task _bouncedEmailTrackerServiceTask;
        #endregion

        #region Public Methods
        public CoZoneBouncedEmailTrackerService()
        {
            InitializeComponent();
        }
        #endregion

        #region Protected Methods
        protected override void OnStart(string[] args)
        {
            _bouncedEmailTrackerService = new BouncedEmailTrackerService();
            _bouncedEmailTrackerServiceTask = new Task(_bouncedEmailTrackerService.StartBouncedEmailTrackerService);
            _bouncedEmailTrackerServiceTask.Start();

        }

        protected override void OnStop()
        {
            _bouncedEmailTrackerService.StopBouncedEmailTrackerService();
        }
        #endregion
    }
}
