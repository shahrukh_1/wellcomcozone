﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace GMG.CoZone.BouncedEmailTrackerService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine("CoZone Bounced Email Tracker Service Started !\nPress Any key to exit");

                var bouncedEmailTrackerService = new BouncedEmailTrackerService();

                var processBouncedEmailsTask = new Task(bouncedEmailTrackerService.StartBouncedEmailTrackerService);
                processBouncedEmailsTask.Start();

                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }

                bouncedEmailTrackerService.StopBouncedEmailTrackerService();
            }
            else 
            {
                ServiceBase[] servicesToRun = new ServiceBase[] 
			    { 
				    new CoZoneBouncedEmailTrackerService() 
			    };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
