﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.BouncedEmailTrackerService
{
    public class AWSBounceTypes
    {
        public enum BounceType
        {
            Undetermined = 0,
            Permanent,
            Transient
        }
    }
}
