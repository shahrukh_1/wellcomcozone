﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.Component
{
    public class MessageConstants
    {
        #region Notification Messages

        public const string CPUpdateInfoSuccess = "UpdateColorProof Instance Succeeded";
        public const string CPUpdateInfoFail = "UpdateColorProof Instance Failed user: {0} pass: {1}";
        public const string CPExchangeDataFailed = "Exchange Data Failed for CPInstance user {0}  pass: {1}";
        public const string CPKeepAliveFail = "ColorProofKeepAlive Failed for instance user: {0} pass: {1}";
        public const string CPGetInstancesFromAtp = "Get ColorProof instances from ATP failed";
        public const string CPDeleteJobStatesFailed = "Delete Deliver JobStates from ATP failed";      
        public const string ColorConvFailed = "ApplyColorConversion method failed";
        public const string PreflightFailed = "ApplyPreflight method failed";       
        public const string CPDeleteImagesStatesFailed = "Delete ImagesState from ATP failed";
        public const string CPGetJobsStateFailed = "Get deliver jobs status from ATP failed";       
        public const string CPGetImagesStateFailed = "Get image status from ATP failed";
        public const string CPKeepAliveSuccess = "ColorProofKeepAlive Succeeded";
        public const string CPUpdateWorkFlowsFail = "Update ColorProof Workflows failed, instance user: {0} pass: {1}";
        public const string CPSetJobTicketAndImageStatusFailed = "SetJobTicketAndImageStatus Failed for instance user: {0} pass: {1}";
        public const string CPUpdateWorkFlowsSuccess = "Update ColorProof Workflows succeeded";
        public const string CPAddCPInstancesToATPFailed = "Add ColorProof Instances to ATP failed";
        public const string CPSendDeliverJobFailed = "Send Deliver Job to ColorProofATPComponent Failed";       
        public const string CPSendDeliverJobCommandFailed = "Send Deliver Job Command to ColorProofATPComponent Failed";
        public const string CPSendDeliverJobSucceeded = "Send Deliver Job to ColorProofATPComponent Succeeded";
        public const string CPSendDeliverJobCommandSucceeded = "Send Deliver Job command to ColorProofATPComponent Succeeded";
        public const string CPDeleteCPInstanceFromATP = "Delete ColorProof Instances from ATP failed";
        public const string CPCheckForTicketsFailed = "CPServer failed in CheckForTickets method, instance user: {0} pass: {1}";
        public const string CPCheckForTicketsTm0 = "CPServer failed when sending jobs for timestamp 0 in checkfortickets method";
        public const string CPGetDeliverJobsFailed = "Get Deliver job list for time stamp 0 method failed instance with guid: {0}, Failed job: {1} ";
        public const string CPUpdateATPJobsStatusFailed = "Update color proof status failed";
        public const string CPUpdateATPJobsStatusSucceeded = "Update color proof status Succeeded";
        public const string CPUpdateATPJobStatusHasVerificationResults = "Page with name {0} has {1} verification results";
        public const string CPUpdateATPImageStateSucceeded = "Update color proof image status Succeeded";
        public const string CPUpdateATPImageStateFailed = "Update color proof image status Failed";
        public const string CPJobStatusUpdated = "Color proof status for job {0} set to {1} with Message {2}";
        public const string CPJobPageStatusUpdated = "Color proof image {0} set to {1}";
        public const string CPJobStatusUpdatedNOK = "Color proof status for job {0} cannot be updated. Job is not found in cache";
        public const string CPAddNewJobOrCommandFailed = "Add new job or command to memcached failed!";
        public const string JobStringNodeNotFoud = "Job string node is missing from response message";
        public const string JobStateNodeNotFoud = "Job state node is missing from response message";
        public const string CPJobNodeNotFound = "Color Proof job node is missing from response message";
        public const string InvalidSyncResponse = "Invalid response for sync command";
        public const string UpdateCoZoneJobStateFailes = "Update cozone job state failed";
        public const string UpdateCoZonePageStateFailed = "Update cozone page state failed";
        public const string FailedRetrievingCurrentTime = "Cannot retrieve server current time";
        public const string CreateAuJobFailed = "Component failed in CreateAUJob";
        public const string czImagesCleanUpThreadFiled = "Component Failed in Images CleanUpThread";
        public const string FailedGetCommands = "Cannot get commands from ATP for instance user: {0} pass: {1}";
        public const string FailedDeleteCommands = "Cannot delete commands from ATP for instance user: {0} pass: {1}";
        public const string SuccessDeleteCommands = "Delete commands from ATP succeeded";
        public const string AWSImageDownloaded = "AWS job image downloaded to get checksum";
        public const string AWSImageDownloadedFailed = "AWS job image download failed filepath: {0}, exception message: {1}, stacktrace: {2}";
        public const string AWSImageDownloadedFileNotFound = "AWS job image download failed filepath: {0}";
        public const string CreateNewColorProofJobFailed = "Create new ColorProof job failed";
        public const string ColorProofJobCreated = "ColorProof {0} job created in local cache";
        public const string ColorProofJobDownloading = "ColorProof {0} job download started";
        public const string ColorProofJobFinished = "ColorProof {0} job download finished and removed from local cache";
        public const string ColorProofJobSentToColorProof = "ColorProof {0} job sent to ColorProof";
        public const string CheckForNewDeliverJobsError = "ColorProofConnector Service: Exception thrown by CheckForNewDeliverJobs";       
        public const string ReadFromQueueFail = "Read from local deliver queue failed";       
        public const string ReadFromFTPQueueFail = "Read from local FTP queue failed";
        public const string ProcessingAmazonSQSMessageFiled = "Processing SQS message failed";
        public const string CPInstanceAcitvityTimeoutFailed = "Failed to retrieve the color proof instance activity timeout from settings";
        public const string CPInstanceActivated = "Color proof instance has bee activated in CoZone. CP Name : {0}";
        public const string CPWorkflowAdded = "New workflow added to CoZone. CP Name: {0}; Workflow name: {1}";
        public const string WorkflowRenamedToExistingName = "The {0} workflow ({1}) was renamed to existing name ({2})";
        public const string WorkflowRenamedAndUpdates = "The {0} workflow ({1}) was renamed to ({2})";
        public const string WorkflowRenamed = "The {0} workflow ({1}) was renamed to ({2})";
        public const string CPWorkflowWithDifferentCapUpdate = "The {0} workflow of {1} ColorProof has different capabilities and has been updated";
        public const string CPWorkflowHasBeenDeactivated = "The {0} workflow of {1} ColorProof has been deleted from ColorProof";
        public const string CPInstanceAddedToATP = "The {0} ColorProof has been added to ATP";
        public const string CPInstanceRemoveFromATP = "The {0} ColorProof was not found in CoZone and has been deleted from ATP";
        public const string CPDeleteJob = "Delete Job with GUID {0} from ATP Failed";
        public const string CPGetPages = "Get Pages Method failed";
        public const string CPCheckInstancesConnectivityFailed = "CheckInstancesConnectivity method failed";
        public const string FTPClientStartFailed = "Exception thrown by Client start";
        public const string FTPFileWatcherInitializationFailed = "Exception thrown by InitFileSystemWatcher";
        public const string DeliverJobStatusFromColorProofStatusInfo= "Get Deliver status from ColorProof job status : ColorProof job status - {0}, Deliver job status - {1}, Job Guid - {2}";
        public const string JobCompletedAndDeletedStatusInfo = "Utilities -> IsJobCompletedAndDeleted() : Page type - {0}, Pages Count - {1}, TotalNrOfPages - {2}, Job Guid - {3}";
		
        #endregion

        #region Configuration Messages

        public const string CzImageChunkNrInvalid = "ChunkNumer not valid for job: {0}";
        public const string ColorProofComp = "ATPColorProofComponentName";       
        public const string ImageChunkSize = "ImageChunkSize";
        public static string CPWFSupportedColorsSeparator = Environment.NewLine;
        public const string TranslateError = "Error: {0} for colorproof with username: {1}";

        #endregion
    }
}
