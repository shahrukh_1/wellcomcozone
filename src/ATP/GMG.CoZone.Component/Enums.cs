﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.Component
{
    /// <summary>
    /// Logging event types
    /// </summary>
    public enum EventType
    {
        Notification,
        Exception
    }

    /// <summary>
    /// Notification severity level
    /// </summary>
    public enum Severity
    {
        Debug = 0,
        Info,
        Warning,
        Error,
        Fatal
    }
}
