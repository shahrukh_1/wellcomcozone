﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.Reflection;

namespace GMG.CoZone.Component.Logging
{
    /// <summary>
    /// Log helper class
    /// </summary>
    public class Logger
    {
        #region Private data

        private static ILog _log;
        private static ILog _notify;
        private static string _environment;
        #endregion

        #region Constructors

        public Logger(string environment, string errorLoggrer, string eventNotifier = null)
        {
            _environment = environment;
            _log = LogManager.GetLogger(errorLoggrer);
            _notify = !String.IsNullOrEmpty(eventNotifier) && !String.IsNullOrEmpty(environment) ? LogManager.GetLogger(eventNotifier) : null;
            XmlConfigurator.Configure();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Log custom component event
        /// </summary>
        /// <param name="AUEvent"></param>
        public void Log(LoggingEvent componentEvent)
        {

            switch (componentEvent.EventType)
            {
                case EventType.Exception:
                {
                    Log(componentEvent.Exception);                        
                    break;
                }
                case EventType.Notification:
                {
                    Log(componentEvent.Notification);
                    break;
                }
            }
        }

        /// <summary>
        /// Log custom exception
        /// </summary>
        /// <param name="exception"></param>
        public void Log(LoggingException exception)
        {
            if (exception != null)
            {
                switch (exception.Severity)
                {
                    case Severity.Debug:
                    {
                        _log.Debug(exception.Message, exception.InnerException);
                        break;
                    }
                    case Severity.Info:
                    {
                        _log.Info(exception.Message, exception.InnerException);
                        break;
                    }
                    case Severity.Warning:
                    {
                        _log.Warn(exception.Message, exception.InnerException);
                        break;
                    }
                    case Severity.Error:
                    {
                        _log.Error(exception.Message, exception.InnerException);                       
                        break;
                    }
                    case Severity.Fatal:
                    {
                        _log.Fatal(exception.Message, exception.InnerException);                        
                        break;
                    }
                }

                SendNotification(exception.Severity, String.Format("{0}Message: {1}{2}InnerException: {3}", Environment.NewLine, exception.Message, Environment.NewLine ,exception.InnerException) );
            }
            else
            {
                _log.Error("Component exception not initialized");
            }
        }

        /// <summary>
        /// Log custom notification
        /// </summary>
        /// <param name="notification"></param>
        public void Log(LoggingNotification notification)
        {
            if (notification != null)
            {
                string message = /*"Notification <" + notification.Severity.ToString() + ">: " +*/ notification.Description;

                switch (notification.Severity)
                {
                    case Severity.Debug:
                    {
                        _log.Debug(message);
                        break;
                    }
                    case Severity.Info:
                    {
                        _log.Info(message);
                        break;
                    }
                    case Severity.Warning:
                    {
                        _log.Warn(message);
                        break;
                    }
                    case Severity.Error:
                    {
                        _log.Error(message);
                        break;
                    }
                    case Severity.Fatal:
                    {
                        _log.Fatal(message);
                        break;
                    }
                }

                SendNotification(notification.Severity, message);
            }
            else
            {
                _log.Error("Component notification not initialized");
            }
        }

        public void SendNotification(Severity severity, string message)
        {
            if (_notify == null)
            {
                return;
            }

            string notificationMessage = String.Format("Environment: {0}{1}{2}", _environment, Environment.NewLine, message);
            switch (severity)
            {
                case Severity.Debug:
                {
                    _notify.Debug(notificationMessage);
                    break;
                }
                case Severity.Info:
                {
                    _notify.Info(notificationMessage);
                    break;
                }
                case Severity.Warning:
                {
                    _notify.Warn(notificationMessage);
                    break;
                }
                case Severity.Error:
                {
                    _notify.Error(notificationMessage);
                    break;
                }
                case Severity.Fatal:
                {
                    _notify.Fatal(notificationMessage);
                    break;
                }
            }
        }

        #endregion

    }
}
