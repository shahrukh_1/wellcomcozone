﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.Component.Logging
{
    /// <summary>
    /// Internal exception class
    /// </summary>
    public class LoggingException : Exception
    {
        #region Properties
        
        /// <summary>
        /// Exception severity level
        /// </summary>
        public Severity Severity { get; private set; }

        #endregion

        #region Contructors

        public LoggingException(String message)
            : this(message, null, Severity.Fatal) { }

        public LoggingException(String message, Severity severity)
            : this(message, null, severity) { }

        public LoggingException(String message, Exception innerException)
            : this(message, innerException, Severity.Fatal) { }

        public LoggingException(String message, Exception innerException, Severity severity)
            : base(message, innerException)
        {
            Severity = severity;
        }

        #endregion
    }
}
