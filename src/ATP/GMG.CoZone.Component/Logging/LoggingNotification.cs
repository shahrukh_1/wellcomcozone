﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CoZone.Component.Logging
{
    /// <summary>
    /// Notification class 
    /// </summary>
    public class LoggingNotification
    {
        #region Properties

        /// <summary>
        /// Notification description
        /// </summary>
        public string Description { get; private set; }
        
        /// <summary>
        /// Notification severity level
        /// </summary>
        public Severity Severity { get; private set; }

        #endregion

        #region Constructors

        public LoggingNotification(string description, Severity severity)
        {
            Description = description;
            Severity = severity;
        }
        #endregion
    }
}
