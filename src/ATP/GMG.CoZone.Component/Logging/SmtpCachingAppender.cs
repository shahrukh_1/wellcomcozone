﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Appender;
using System.Timers;
using log4net.Core;

namespace GMG.CoZone.Component.Logging
{
    public class SmtpCachingAppender : SmtpAppender
    {
        Timer _timer;
        private readonly List<log4net.Core.LoggingEvent> _eventsBuffer;
        private int _flushCount { get; set; }
        private Object _eventsBufferListLock = new Object();

        public SmtpCachingAppender()
        {
            _flushCount = 20;
            _eventsBuffer = new List<log4net.Core.LoggingEvent>();
        }

        /// <summary>
        /// Create a timer that fires to force flushing cached log events
        /// via SMTP at a specified interval.
        /// </summary>
        public override void ActivateOptions()
        {
            if (_timer == null)
            {
                _timer = new Timer(TimeSpan.FromHours(12).TotalMilliseconds); // send time span between emails to 12 hours
                _timer.Elapsed += OnTimedEvent;
                _timer.Enabled = true;
            }

            base.ActivateOptions();
        }

        protected override void SendBuffer(log4net.Core.LoggingEvent[] events)
        {
            var evaluator = Evaluator as LevelEvaluator;
            if (evaluator != null)
            {
                Level thresholdLevel = evaluator.Threshold;

                lock (_eventsBufferListLock)
                {
                    _eventsBuffer.AddRange(events.Where(logEvent => Level.Compare(logEvent.Level, thresholdLevel) >= 0).Take(50));
                }
            }
        }

        private void SendBuffer()
        {
            lock (_eventsBufferListLock)
            {
                if (_eventsBuffer.Count > 0)
                {
                    Subject = "Application Event " + AppDomain.CurrentDomain.FriendlyName;
                    base.SendBuffer(_eventsBuffer.ToArray());
                    _eventsBuffer.Clear();
                }
            }

        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            SendBuffer();
            Flush(true);
        }

    }
}