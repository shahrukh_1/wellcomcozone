﻿using System;

namespace GMG.CoZone.Component.Logging
{
    /// <summary>
    /// Event class 
    /// can be either a notification for the internal events or an exception occurred
    /// </summary>
    public class LoggingEvent
    {
        #region Properties

        /// <summary>
        /// Type of the event
        /// </summary>
        public EventType EventType { get; private set; }

        /// <summary>
        /// Event exception
        /// </summary>
        public LoggingException Exception { get; private set; }

        /// <summary>
        /// Notification details
        /// </summary>
        public LoggingNotification Notification { get; private set; }

        /// <summary>
        /// Specifies if the error refers to a communication error or not
        /// </summary>
        public bool IsCommunicationError { get; private set; }

        #endregion

        #region Constructors

        public LoggingEvent(LoggingException exception, bool isCommunicationError = false) :
            this(EventType.Exception, exception, null, isCommunicationError) { }

        public LoggingEvent(LoggingNotification notification, bool isCommunicationError = false) :
            this(EventType.Notification, null, notification, isCommunicationError) { }

        private LoggingEvent(EventType type, LoggingException exception, LoggingNotification notification, bool isCommunicationError = false)
        {
            EventType = type;
            Exception = exception;
            Notification = notification;
            IsCommunicationError = isCommunicationError;
        }

        #endregion
    }
}
