// OpenAPI TestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AOITestApp.h"
#include "TestDlg.h"
#include "../SoftProofingRenderEngine/Sources/RenderInstance.h"
#include "../SoftProofingRenderEngine/Sources/ICCProfileManager.h"
#include <vector>
#include "gdiplus.h"
using namespace Gdiplus;

#define MULTIRENDER 0

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

float SX = 1;
float SY = 1;
// CAboutDlg dialog used for App About

CStringW UTF8toUTF16(const CStringA& utf8)
{
	CStringW utf16;
	int len = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, 0);
	if (len>1)
	{ 
		wchar_t *ptr = utf16.GetBuffer(len-1);
		if (ptr) MultiByteToWideChar(CP_UTF8, 0, utf8, -1, ptr, len);
		utf16.ReleaseBuffer();
	}
	return utf16;
}

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:
	DECLARE_MESSAGE_MAP()
};

void CTestDlg::EnableActions(bool enabled)
{
	GetDlgItem(IDC_OPEN)->EnableWindow(enabled);
	GetDlgItem(IDC_PREV)->EnableWindow(enabled);
	GetDlgItem(IDC_NEXT)->EnableWindow(enabled);
	GetDlgItem(IDCANCEL)->EnableWindow(enabled);
	GetDlgItem(IDOK)->EnableWindow(enabled);
	GetDlgItem(IDC_BUTTON_GENERATE_RAW)->EnableWindow(enabled);
	GetDlgItem(IDC_GENERATE_TILES)->EnableWindow(enabled);
}

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

CTestDlg::CTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestDlg::IDD, pParent),
	 mTileSettings( 256, 256, 20, 3500, 300, 100, 650, UINT_MAX )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	mRenderer = NULL;
	msRGBProfilePath[0] = 0;
	mISOCoatedProfilePath[0] = 0;
	mAdobeRGBProfilePath[0] = 0;
	mLabProfilePath[0] = 0;

	mPreviewImage = NULL;
	mInstnace = NULL;
	LoadPaths();
}

CTestDlg::~CTestDlg()
{	
	delete mRenderer;	
}

void CTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_OPEN, OpenDoc)
	ON_BN_CLICKED(IDC_PREV, OnPrev)
	ON_BN_CLICKED(IDC_NEXT, OnNext)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_BUTTON_GENERATE_RAW, &CTestDlg::OnBnClickedButtonGenerateRaw)
	ON_BN_CLICKED(IDC_GENERATE_TILES, &CTestDlg::OnBnClickedButtonCreateTiles)
	
END_MESSAGE_MAP()

BOOL CTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestDlg::LoadPaths()
{	
	CString strFolder = GetCurrentPath();
	
	CString profilePath = strFolder + "\\Profiles\\sRGB.icc";
	strcpy(msRGBProfilePath, (LPCTSTR)profilePath);

	profilePath = strFolder + "\\Profiles\\AdobeRGB1998.icc";
	strcpy(mAdobeRGBProfilePath, (LPCTSTR)profilePath);

	profilePath = strFolder + "\\Profiles\\ISOcoated_v2_eci.icc";
	strcpy(mISOCoatedProfilePath, (LPCTSTR)profilePath);

	profilePath = strFolder + "\\Profiles\\Lab-to-Lab_Desaturation5.icc";
	strcpy(mLabProfilePath, (LPCTSTR)profilePath);
}

CString CTestDlg::GetCurrentPath()
{
	char path[MAX_PATH];
    GetModuleFileName( NULL, path, MAX_PATH );
    PathRemoveFileSpec(path);
	return path;
}

BOOL CTestDlg::CreateJobDir()
{	
	CString outputPath = GetCurrentPath();
	outputPath = outputPath + "\\Output";

	if (GetFileAttributes(outputPath) == INVALID_FILE_ATTRIBUTES) {
		CreateDirectory(outputPath,NULL);
	}

	GUID guid;
	HRESULT hr = CoCreateGuid(&guid);
	CString jobFolderName;
	CComBSTR guidBstr(guid);
	CString guidStr(guidBstr);
	guidStr.Delete(0);
	guidStr.Delete(guidStr.GetLength() - 1);

	outputPath = outputPath + "\\" + guidStr;
	
	if( GetFileAttributes(outputPath) != INVALID_FILE_ATTRIBUTES)
	{
		return TRUE;
	}
	else if( CreateDirectory(outputPath, NULL) )
	{
		strcpy(mJobDir, (LPCTSTR)outputPath);
		return TRUE;
	}

	mJobDir[0] = 0;	
	return FALSE;
}

CString CTestDlg::GetPageDir()
{
	CString path;

	CString page;
	page.Format("%d", mCurrentPage + 1);
	
	CString pageDir = mJobDir;
	pageDir += "\\";
	pageDir += page;

	if( GetFileAttributes(pageDir) == INVALID_FILE_ATTRIBUTES )
	{
		if( !CreateDirectory(pageDir, NULL) )
		{		
			pageDir.Empty(); //Empty string to signal failure 
		}
	}

	return pageDir;
}

void CTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void CTestDlg::OnPaint()
{	
	DrawPreview();
}

void CTestDlg::DrawPreview()
{
	if(mPreviewImage != NULL)
	{
		CDC*	dc = GetDlgItem( IDC_PREVIEW )->GetDC();
		HDC hdc = *dc;

		CRect rect;
		GetDlgItem( IDC_PREVIEW )->GetWindowRect(&rect);
		ScreenToClient(&rect);

		mPreviewImage->Draw(hdc, (rect.Width() - mPreviewWidth)/2, (rect.Height() - mPreviewHeight)/2, mPreviewWidth, mPreviewHeight);
		ReleaseDC( dc );
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTestDlg::DoPage()
{	
	float pageWidth, pageHeight;
	mRenderer->GetPageDimenssions(mCurrentPage, pageWidth, pageHeight);
	CRect rect;

	GetDlgItem( IDC_PREVIEW )->GetWindowRect(&rect);
	ScreenToClient(&rect);

	mPreviewWidth = rect.Width();
	mPreviewHeight = pageHeight*mPreviewWidth/pageWidth;
	if(mPreviewHeight > rect.Height())
	{
		mPreviewHeight = rect.Height();
		mPreviewWidth = pageWidth*mPreviewHeight/pageHeight;
	}

	CString previewPath = GetPageDir();
	previewPath  = previewPath + "\\Preview.jpg" ;
	mRenderer->RenderPage(UTF8toUTF16(previewPath), "jpg", mPreviewHeight > mPreviewWidth ? mPreviewHeight : mPreviewWidth, mCurrentPage);

	if(mPreviewImage != NULL)
	{
		delete mPreviewImage;
		mPreviewImage = NULL;
	}
	mPreviewImage = new CImage();
	
	mPreviewImage->Load(previewPath);
	GetDlgItem( IDC_PREVIEW )->GetDC()->FillSolidRect(0, 0, rect.Width(), rect.Height(), RGB(240, 240, 240));
	DrawPreview();	
}

bool	CTestDlg::ProgressionCallback( float percentage, void* ref )
{
//	OS_BEEP;
	return false;
}

enum SuiteableSpace
{
	eSimpleNoSpace = 0,
	eSimpleRGBSpace,
	eSimpleCMYKSpace,
	eSimpleCMYKOGSpace,
	eSimpleHLSSpace,
	eSimpleLABSpace,
	eSimpleXYZSpace,
	eSimpleGRAYSpace,
	eSimpleCMYKRGBSpace
};

void CTestDlg::OpenDoc()
{
	EnableActions(FALSE);
	CString strFilter = "Image file|*.*;|";

	CFileDialog	dlog( true, NULL, NULL, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, strFilter );
	CString title;
	VERIFY( title.LoadString( AFX_IDS_OPENFILE ) );
	dlog.m_ofn.lpstrTitle = title;
	dlog.m_ofn.lpstrFile = new char[ 100000 ];
	dlog.m_ofn.lpstrFile[0] = 0;
	dlog.m_ofn.nMaxFile = 100000;
	if( dlog.DoModal() == IDCANCEL )
	{
		EnableActions(TRUE);	
		delete[] dlog.m_ofn.lpstrFile;
		return;
	}
	if( mRenderer )
	{
		delete mRenderer;
		mRenderer = NULL;
	}
	POSITION filePos = dlog.GetStartPosition();
	bool addedAPage = false;
	CString p = dlog.GetNextPathName(filePos);

	mInstnace = new RenderInstance(TRUE, 0);
	mRenderer = new SoftProofRenderer(mInstnace, UTF8toUTF16(p), true);

	delete[] dlog.m_ofn.lpstrFile;
	
	if( mRenderer && CreateJobDir())
	{	
		mCurrentPage = 0;
		mNoOfPages = mRenderer->GetPagesCount();
		UpdateNavigation();
		mRenderer->Setup( UTF8toUTF16(mISOCoatedProfilePath), UTF8toUTF16(msRGBProfilePath), 12, false );
		UTF16Char profileName[200];
		mRenderer->SaveDocumentEmbeddedProfile(UTF8toUTF16(mJobDir), profileName);

		DoPage();
	}
	EnableActions(TRUE);
}

void CTestDlg::UpdateNavigation()
{
	char pageNr[256];
	sprintf( pageNr, "%d / %d", mCurrentPage + 1, mNoOfPages );	
	CWnd* ww = GetDlgItem( IDC_PAGENR );
	ww->SetWindowText( pageNr );
}

void CTestDlg::OnPrev()
{
	EnableActions(FALSE);
	mCurrentPage--;	
	if(mCurrentPage < 0)
		mCurrentPage = 0;
	
	UpdateNavigation();
	DoPage();
	EnableActions(TRUE);
}

void CTestDlg::OnNext()
{
	EnableActions(FALSE);
	mCurrentPage++;	
	if(mCurrentPage >= mNoOfPages)
		mCurrentPage = mNoOfPages - 1;
	
	UpdateNavigation();
	DoPage();
	EnableActions(TRUE);
}

void CTestDlg::OnCancel()
{	
	delete mRenderer;
	mRenderer = NULL;
	delete mInstnace;
	mInstnace = NULL;
	CDialog::OnCancel();
}

void CTestDlg::OnOk()
{
	delete mRenderer;
	mRenderer = NULL;
	delete mInstnace;
	mInstnace = NULL;
	CDialog::OnOK();
}

void CTestDlg::OnBnClickedButtonGenerateRaw()
{
	EnableActions(FALSE);
	mRenderer->RenderRawData( mTileSettings, UTF8toUTF16(GetPageDir()), mCurrentPage, mCurrentPageOutputDpi );

	// Get extra plates
	mExtraPlates.clear();

	uint32_t spotColorCount = mRenderer->GetDocumentSpotColorsCount();
	if(spotColorCount > 0)
	{
		for ( int i =0; i < spotColorCount; i++ )
		{
			SpotColorCieLab spotColor;
			mRenderer->GetSpotColorCIELab(i, spotColor);
			mExtraPlates.push_back(spotColor);
		}
		mRenderer->GetProfilePaperWhiteCIELab(mWhitePaperWhite);
	}
	

	EnableActions(TRUE);
}

void CTestDlg::OnBnClickedButtonCreateTiles()
{
	EnableActions(FALSE);
	CString inputMergeProfile = mISOCoatedProfilePath;

	// Create custom profile in case the document contains extra plates
	if(	mExtraPlates.size() > 0	)
	{
		inputMergeProfile = mISOCoatedProfilePath;
		CString outoutProfilePath = GetPageDir() + "\\merged.icc";

		for (int i = 0; i < mExtraPlates.size(); i++)
		{
			// merge the channel custom generated profiele witht the input profile
			double lab[2*4];
			lab[0] = 0;
			lab[1] = mWhitePaperWhite.mLab[0];
			lab[2] = mWhitePaperWhite.mLab[1];
			lab[3] = mWhitePaperWhite.mLab[2];
			
			lab[4] = 100;
			lab[5] = mExtraPlates[i].mLab[0];
			lab[6] = mExtraPlates[i].mLab[1];
			lab[7] = mExtraPlates[i].mLab[2];
			
			CString tempProfilePath = GetPageDir() + "\\" + mExtraPlates[i].mName + "_1ch.icc";

			mRenderer->CreateMergedProfile(mExtraPlates[i].mName, &lab[0], 2, inputMergeProfile, outoutProfilePath, tempProfilePath );		
			inputMergeProfile = outoutProfilePath;
		}		
	}

	bool isRgbDocument = mRenderer->IsRGBDocumentColorSpace();

	void* hTransform = mRenderer->CreateTransform(inputMergeProfile, mLabProfilePath, msRGBProfilePath, true, mRenderer->GetDocumentChannelCount());

	if(!isRgbDocument && hTransform == NULL)
	{
		EnableActions(TRUE);
		return;
	}

	//-- Generate tiles
	//----------------------------
	CString bandsFolderPath = GetPageDir() + "\\Bands";

	std::vector< CString > filepaths ;
	CFileFind finder;
	CString wildcard( bandsFolderPath );
	wildcard += _T("\\*.*");

	BOOL working = finder.FindFile( wildcard );
	while ( working )
	{			
		working = finder.FindNextFile();
		if ( finder.IsDots() )
			continue;
		if ( finder.IsDirectory() )
			continue;
		filepaths.push_back( finder.GetFilePath() );
	}
	finder.Close();
	
	CString tilesPath = GetPageDir() + "\\Tiles"; 
	CreateDirectory(tilesPath, NULL);
	CStringW tilesPathw = UTF8toUTF16(tilesPath);

	size_t size;
	FILE* f = _wfopen(UTF8toUTF16(msRGBProfilePath), L"rb");
	if (f == NULL) return;

	// obtain file size:
	fseek (f , 0 , SEEK_END);
	size = ftell (f);
	rewind (f);

	uint8_t* outputProfileBuffer = new unsigned char[size + 1];
	uint32_t outputProfileBufferLength = fread(outputProfileBuffer, 1, size, f);
	fclose(f);
	outputProfileBuffer[outputProfileBufferLength] = 0;
    float pageWidth, pageHeight;
	mRenderer->GetPageDimenssions(mCurrentPage, pageWidth, pageHeight);
	mRenderer->InitTileSettings(mTileSettings, mCurrentPage, tilesPathw, pageWidth / 18 * mCurrentPageOutputDpi, pageHeight / 18 * mCurrentPageOutputDpi);

	for (int i = 0; i < filepaths.size(); i++)
	{		
		CString currentPath(filepaths.at(i));
			
		FILE * pFile;
		long lSize;
		char * buffer;
		size_t result;

		pFile = fopen ( currentPath , "rb" );
		if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

		// obtain file size:
		fseek (pFile , 0 , SEEK_END);
		lSize = ftell (pFile);
		rewind (pFile);

		// allocate memory to contain the whole file:
		buffer = (char*) malloc (sizeof(char)*lSize);
		if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

		// copy the file into the buffer:
		result = fread (buffer,1,lSize,pFile);
		if (result != lSize) {fputs ("Reading error",stderr); exit (3);}

		if(!isRgbDocument)
		{
			uint64_t channelsMask = ULONG_MAX;
			/* the whole file is now loaded in the memory buffer. */		
			long outputSize = (lSize/4)*3;
			char * outBuffer = (char*) malloc (sizeof(char)*outputSize);
			// apply lcms transformation
			mRenderer->DoTransform(hTransform, buffer, outBuffer, lSize/mRenderer->GetDocumentChannelCount(), channelsMask , mRenderer->GetDocumentChannelCount());
			mRenderer->CreateFirstLevelTiles((uint8_t*)outBuffer, outputSize, outputProfileBuffer, outputProfileBufferLength);
			free(outBuffer);
		}
		else
		{
			mRenderer->CreateFirstLevelTiles((uint8_t*)buffer, lSize, outputProfileBuffer, outputProfileBufferLength);
		}

		// terminate
		fclose (pFile);
		free (buffer);
	}

	if(!isRgbDocument)
	{
		mRenderer->DeleteTransform( hTransform );
	}

	mRenderer->DownSampleTiles( outputProfileBuffer, outputProfileBufferLength );

	delete outputProfileBuffer;
	
	EnableActions(TRUE);
}