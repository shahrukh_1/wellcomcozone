// OpenAPI Test.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "AOITestApp.h"
#include "TestDlg.h"
#include "../SoftProofingRenderEngine/Sources/RenderInstance.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestApp

BEGIN_MESSAGE_MAP(CTestApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTestApp construction

CTestApp::CTestApp()
{
#ifdef _DEBUG
	for( int i = 0; i != __argc; ++i )
		if( strcmp( __targv[i], "-memdebug" ) == 0 )
			afxMemDF = allocMemDF | checkAlwaysMemDF;	//	Debug checking on
#endif
}

CTestApp theApp;

// CTestApp initialization

BOOL CTestApp::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("AOI Test"));

	//
	//	Aurelon Open API startup
	//
	RenderInstance::Startup( "AOITest", "17C8E36C" );

	CTestDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	RenderInstance::Terminate();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
