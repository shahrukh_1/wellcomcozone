// OpenAPI TestDlg.h : header file
//
#include "../SoftProofingRenderEngine/Sources/RenderInstance.h"
#include "../SoftProofingRenderEngine/Sources/TileSettings.h"
#include "../SoftProofingRenderEngine/Sources/SoftProofRenderer.h"
#include "atlimage.h"
#include <vector>

#pragma once

class AOI_Document;

// CTestDlg dialog
class CTestDlg : public CDialog
{
public:
			CTestDlg(CWnd* pParent = NULL);	// standard constructor
	virtual	~CTestDlg();

	enum { IDD = IDD_OpenAPITEST_DIALOG };

	afx_msg void OpenDoc();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()
private:
	static	bool	ProgressionCallback( float, void* );
			void	DoPage();
			void	GenerateTiles();	
			void	DrawPreview();
			void	LoadPaths();
			CString	GetCurrentPath();
			BOOL	CreateJobDir();
			CString GetPageDir();
			void	UpdateNavigation();
			void	EnableActions(bool enabled);

	SoftProofRenderer*	mRenderer;
	RenderInstance*		mInstnace;			

	char			msRGBProfilePath[_MAX_PATH];
	char			mISOCoatedProfilePath[_MAX_PATH];
	char			mAdobeRGBProfilePath[_MAX_PATH];
	char			mLabProfilePath[_MAX_PATH];
	
	char			mJobDir[_MAX_PATH];
	
	CImage*			mPreviewImage;
	int				mPreviewHeight;
	int				mPreviewWidth;
	int				mCurrentPage;
	uint32_t		mCurrentPageOutputDpi;
	int				mNoOfPages;
	TileSettings	mTileSettings;
	
	std::vector<SpotColorCieLab>	mExtraPlates;
	SpotColorCieLab					mWhitePaperWhite;	

public:
	afx_msg void OnPrev();
	afx_msg void OnNext();
	afx_msg void OnCancel();
	afx_msg void OnOk();	
	afx_msg void OnBnClickedButtonGenerateRaw();
	afx_msg void OnBnClickedButtonCreateTiles();
};
