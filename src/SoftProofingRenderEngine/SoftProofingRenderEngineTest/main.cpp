#include <iostream>
#include "../ACPL/CString.h"
#include "../OpenAPI/AOI_Instance.h"
#include "../OpenAPI/AOI_Document.h"

int main (int argc, char * const argv[])
{
    std::cout << "OpenAPI Test\n";
	
	AOI_Instance* instance = AOI_Instance::Startup( "AOITest", "17C8E36C"/* LICENSE CODE */ );

	AOI_Document* doc = new AOI_Document( instance );
	doc->Open( "/Test.pdf" );
	delete doc;

	AOI_Instance::Terminate();
	
    return 0;
}
