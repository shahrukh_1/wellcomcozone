//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AOI Test.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_OpenAPITEST_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_OPEN                        1000
#define IDC_PAGENR                      1001
#define IDC_PREV                        1002
#define IDC_NEXT                        1003
#define IDC_PREVIEW                     1004
#define IDC_CHECK_SIXTEEN               1005
#define IDC_OPEN2                       1006
#define IDC_LOOP                        1006
#define IDC_ANTIALIAS                   1006
#define IDC_ZOOM                        1007
#define IDC_ALPHA                       1008
#define IDC_ALPHA2                      1009
#define IDC_CMYK                        1009
#define IDC_LUS                         1010
#define IDC_EXHAUSTIVE                  1011
#define IDC_PATH                        1012
#define IDC_RENDERSCALE                 1013
#define IDC_HIT                         1014
#define IDC_SAVETILES                   1014
#define IDC_BUTTON1                     1015
#define IDC_BUTTON_SAVE_THUMBNAIL       1015
#define IDC_BUTTON_GENERATE_RAW         1015
#define IDC_BUTTON2                     1016
#define IDC_GENERATE_TILES              1016
#define IDC_BUTTON3                     1017
#define IDC_BUTTON4                     1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
