#include "./AOI_SoftProofRenderer.h"
#include "../GDocument.h"
#include "../GRenderer.h"
#include "../GLayer.h"
#include "../CImport.h"
#include "../GProfile.h"
#include <PNG Library/png.h>
#include <JPEG Library/jpeglib.h>
#include "../f_MUL.h"
#include "../GInstance.h"
#include <ACPL/Utilities.h>
#include <ACPL/FileStream.h>
#include <ACPL/XML.h>
#include "../GVector.h"
#include "../ColorSpace.h"
#include "../GPlaceHolder.h"
#include "../GRectangle.h"
#include "../FindSpotcolorInLibraries.h"

using namespace aur;
using namespace aur::PDF;
using namespace aur::ACPL;

class ThumbnailRenderer : public aur::PDF::Renderer
{
public:
							ThumbnailRenderer();
	virtual					~ThumbnailRenderer();
	void					SetProfile( aur::PDF::Document* doc, aur::PDF::ProfilePtr refProf, aur::PDF::Intent intent, aur::PDF::CMM* deviceCMMs );
	const PDF::ProfilePtr	GetProfile() const;
	void					CopyBits( uint8_t* dest, const aur::PDF::LRectangle& updateBounds, int32_t rowBytes, bool bImageAlphaBlending = false );
	bool					Update( int32_t, int32_t );
	const PDF::CMM*			GetDeviceCMM() const;
	void					SetVisiblePlates( uint64_t mask );
	const uint8_t*			GetChannelRGB( uint32_t channelIndex ) const;
	const std::vector<aur::ACPL::String>	GetExtraPlates() const;				
private:
	typedef	struct RGBValues
	{
		uint8_t	v[256][3];
	}	RGBValues;
	aur::PDF::ProfilePtr		mProofProfile;
	aur::PDF::CMM*				mDeviceCMM;
	aur::PDF::Intent			mProofIntent;
	bool						mProofProfileOpen;
	std::vector<RGBValues>		mChannelRGB;
	float						mChannelSolidity[MAX_CHANNELS];
	std::vector<aur::ACPL::String>	mExtraPlates;
	uint64_t					mVisiblePlates;
	bool						mTransparencyGrid;
	enum CopyVariant
	{
		eNoMatch,
		eDeviceN,
		eICC
	}							mWhichCopy;

	void			CopyBitsDeviceN( uint8_t*, const aur::PDF::LRectangle&, int32_t, bool bImageAlphaBlending );
	void			CopyBitsICC( uint8_t*, const aur::PDF::LRectangle&, int32_t, bool bImageAlphaBlending );
	void			CopyBitsNoMatch( uint8_t*, const aur::PDF::LRectangle&, int32_t, bool bImageAlphaBlending );
	void			CopyBitsSingleChannel( uint8_t*, const aur::PDF::LRectangle&, int32_t, bool bImageAlphaBlending );

	void			AddDocumentColorants( aur::PDF::Document* doc );
	void			DeterminePlates( PDF::Document* doc );
	void			FindSpotColorsInLibraries( PDF::Document* doc );
};

const std::vector<aur::ACPL::String> ThumbnailRenderer::GetExtraPlates() const
{
	return mExtraPlates;
}

const uint8_t* ThumbnailRenderer::GetChannelRGB( uint32_t channelIndex ) const
{
	return mChannelRGB[channelIndex].v[255];
}

const aur::PDF::CMM* ThumbnailRenderer::GetDeviceCMM() const
{
	return mDeviceCMM;
}

const aur::PDF::ProfilePtr ThumbnailRenderer::GetProfile() const
{
	return mProofProfile;
}

static void png_write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
	fwrite(data, 1, length, (FILE *)(png_ptr->io_ptr));
}

/*!
	Saves the specified page from the document as PNG to a file.

	\param	filePath		Output File where the tiff will be saved.
	\param	buf				The buffer containing ong data that will be saved.
	\param	width			Width of the resulting png.
	\param	height			Height of the resulting png.
*/
static bool SaveAsPNG( const UTF16Char* filePath, const uint8_t* buf, int32_t width, int32_t height )
{
	uint8_t * * buffer = new uint8_t*[height];
	int32_t lineLen = 4 * width;
	for ( int32_t nCount = 0; nCount < height; nCount++ )
	{
		buffer[nCount] = new unsigned char[lineLen];
		memcpy( buffer[nCount], buf + (nCount * lineLen) , lineLen );
	}

	FILE            *fp;
	png_structp     png_ptr;
	png_infop       info_ptr;

#if ACPL_WIN
	fp = _wfopen( filePath, L"wb" );
#else
	fp = fopen( ACPL::UString(filePath).ToUTF8(), "wb" );
#endif
	if( fp == NULL)
		return false;

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	info_ptr = png_create_info_struct(png_ptr);
	png_ptr->io_ptr = (png_voidp)fp;

	png_set_compression_level(png_ptr, 6);

	png_set_IHDR(png_ptr, info_ptr, width, height,
		8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	double filegamma_ = 0.5;

	png_set_gAMA(png_ptr, info_ptr, filegamma_);
	png_set_gAMA(png_ptr, info_ptr, 0.5 );

	time_t          gmt;
	png_time        mod_time;
	png_text        text_ptr[5];
	time(&gmt);
	png_convert_from_time_t(&mod_time, gmt);
	png_set_tIME(png_ptr, info_ptr, &mod_time);
	text_ptr[0].key = "Title";
	text_ptr[0].text = "Preview";
	text_ptr[0].compression = PNG_TEXT_COMPRESSION_NONE;
	text_ptr[1].key = "Author";
	text_ptr[1].text = "Aurelon";
	text_ptr[1].compression = PNG_TEXT_COMPRESSION_NONE;
	text_ptr[2].key = "Description";
	text_ptr[2].text = "";
	text_ptr[2].compression = PNG_TEXT_COMPRESSION_NONE;
	text_ptr[3].key = "Creation Time";
#if defined(PNG_TIME_RFC1123_SUPPORTED)
	text_ptr[3].text = png_convert_to_rfc1123(png_ptr, &mod_time);
#else
	static const char short_months[12][4] =
	{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

	text_ptr[3].text = new char[128];
	sprintf(text_ptr[3].text, "%d %s %d %02d:%02d:%02d +0000",
		mod_time.day % 32, short_months[(mod_time.month - 1) % 12],
		mod_time.year, mod_time.hour % 24, mod_time.minute % 60,
		mod_time.second % 61);
#endif
	text_ptr[3].compression = PNG_TEXT_COMPRESSION_NONE;
	text_ptr[4].key = "Software";
	text_ptr[4].text = "";
	text_ptr[4].compression = PNG_TEXT_COMPRESSION_NONE;
	png_set_text(png_ptr, info_ptr, text_ptr, 5);
	png_ptr->write_data_fn = png_write_data;
	png_write_info(png_ptr, info_ptr);

	png_write_image(png_ptr, buffer);
	png_write_end(png_ptr, info_ptr);
	png_destroy_write_struct(&png_ptr, &info_ptr);
	fclose(fp);

	for (int i = 0; i < height; i++)
		delete[] buffer[i];
	delete[] buffer;

	return true;
}

 /*!
	Saves the specified page from the document as JPEG to a file.

	\param	filePath		Output File where the tiff will be saved.
	\param	buf				The buffer containing ong data that will be saved.
	\param	width			Width of the resulting png.
	\param	height			Height of the resulting png.
	\param	quality			Quality factor of the resulting jpg.
*/
static bool SaveAsJPG(const UTF16Char* filePath, const uint8_t* buf, int32_t width, int32_t height, int32_t quality )
{
	/* This struct contains the JPEG compression parameters and pointers to
	* working space (which is allocated as needed by the JPEG library).
	* It is possible to have several such structures, representing multiple
	* compression/decompression processes, in existence at once.  We refer
	* to any one struct (and its associated working data) as a "JPEG object".
	*/
	struct jpeg_compress_struct cinfo;
	/* This struct represents a JPEG error handler.  It is declared separately
	* because applications often want to supply a specialized error handler
	* (see the second half of this file for an example).  But here we just
	* take the easy way out and use the standard error handler, which will
	* print a message on stderr and call exit() if compression fails.
	* Note that this struct must live as long as the main JPEG parameter
	* struct, to avoid dangling-pointer problems.
	*/
	struct jpeg_error_mgr jerr;
	/* More stuff */
	FILE * fp;       /* target file */
	JSAMPROW row_pointer[1];  /* pointer to JSAMPLE row[s] */
	int row_stride;       /* physical row width in image buffer */

	/* Step 1: allocate and initialize JPEG compression object */

	/* We have to set up the error handler first, in case the initialization
	* step fails.  (Unlikely, but it could happen if you are out of memory.)
	* This routine fills in the contents of struct jerr, and returns jerr's
	* address which we place into the link field in cinfo.
	*/
	cinfo.err = jpeg_std_error(&jerr);
	/* Now we can initialize the JPEG compression object. */
	jpeg_create_compress(&cinfo);

	/* Step 2: specify data destination (eg, a file) */
	/* Note: steps 2 and 3 can be done in either order. */

	/* Here we use the library-supplied code to send compressed data to a
	* stdio stream.  You can also write your own code to do something else.
	* VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
	* requires it in order to write binary files.
	*/
#if ACPL_WIN
	fp = _wfopen( filePath, L"wb" );
#else
	fp = fopen( filePath.ToUTF8(), "wb" );
#endif
	if( fp == NULL)
		return false;

	jpeg_stdio_dest(&cinfo, fp);

	/* Step 3: set parameters for compression */

	/* First we supply a description of the input image.
	* Four fields of the cinfo struct must be filled in:
	*/
	cinfo.image_width = width;  /* image width and height, in pixels */
	cinfo.image_height = height;
	cinfo.input_components = 3;       /* # of color components per pixel */
	cinfo.in_color_space = JCS_RGB;     /* colorspace of input image */
	/* Now use the library's routine to set default compression parameters.
	* (You must set at least cinfo.in_color_space before calling this,
	* since the defaults depend on the source color space.)
	*/
	jpeg_set_defaults(&cinfo);
	/* Now you can set any non-default parameters you wish to.
	* Here we just illustrate the use of quality (quantization table) scaling:
	*/
	jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

	/* Step 4: Start compressor */

	/* TRUE ensures that we will write a complete interchange-JPEG file.
	* Pass TRUE unless you are very sure of what you're doing.
	*/
	jpeg_start_compress(&cinfo, TRUE);

	/* Step 5: while (scan lines remain to be written) */
	/*           jpeg_write_scanlines(...); */

	/* Here we use the library's state variable cinfo.next_scanline as the
	* loop counter, so that we don't have to keep track ourselves.
	* To keep things simple, we pass one scanline per call; you can pass
	* more if you wish, though.
	*/
	row_stride = width * 3; /* JSAMPLEs per row in image_buffer */

	while (cinfo.next_scanline < cinfo.image_height) {
		/* jpeg_write_scanlines expects an array of pointers to scanlines.
		* Here the array is only one element long, but you could pass
		* more than one scanline at a time if that's more convenient.
		*/
		row_pointer[0] =  (JSAMPROW)&buf[cinfo.next_scanline * row_stride];
		(void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	/* Step 6: Finish compression */

	jpeg_finish_compress(&cinfo);
	/* After finish_compress, we can close the output file. */
	fclose(fp);

	/* Step 7: release JPEG compression object */

	/* This is an important step since it will release a good deal of memory. */
	jpeg_destroy_compress(&cinfo);

	return true;
}

/*!
	\class		AOI_SoftProofRenderer
	\ingroup	AOI_Output
	\brief		Renderer / RIP
*/

/*!
	Constructs a new AOI_SoftProofRenderer. This renderer can be used to create
	thumbnails or tiles in jpeg or png format.

	\param	document	AOI_Document object used by this thread.
*/
AOI_SoftProofRenderer::AOI_SoftProofRenderer( AOI_Document* document ) :
	mOutputSpace ( NULL ),
	mIsRGBDocument( false )
{
	if( document )
	{
		mDocument = document->mDocument;
	}
}

/*!
	Setup the document using the specified profile file and CMM file 
	For the documents with embedded profiles the routine will use the embedded profile for setting up the renderer.

	\param	proofSpace					The path to the profile file used to setup the document renderer.
	\param	outputSpace					The path to the PMM file used to setup the document renderer.
	\param	maxNrOfChannels				Maximum Number of channels used for rendering, if the document has a greater number of channels than the remaining ones will be ignored.

	\return								TRUE if the method is successfully executed otherwise false.
*/
bool AOI_SoftProofRenderer::Setup( const UTF16Char* proofSpace, const UTF16Char* outputSpace, uint32_t maxNrOfChannels, bool useCustomProfile )
{
	FileSpec docProfile;
	if( proofSpace != NULL )
	{
		docProfile.Make( proofSpace );
		if( !docProfile.Exists() )
			return false;
	}

	FileSpec cmmspec;
	if( outputSpace != NULL )
	{
		cmmspec.Make( outputSpace );
		if( !cmmspec.Exists() )
			return false;
	}

	mDocument->GetInstance()->SetMaxChannels( maxNrOfChannels );
	PDF::Profile* proofSpaceProfile = new PDF::Profile( docProfile );
	PDF::Profile* embeddedProfile = NULL;
	mOutputSpace =  new PDF::Profile( cmmspec );
	mOutputSpace->Open( PDF::CMM::eOutput, PDF::eRelativeColorimetric );

	ThumbnailRenderer* renderer = new ThumbnailRenderer();

	if( useCustomProfile )
	{
		renderer->SetProfile( mDocument, proofSpaceProfile, PDF::eRelativeColorimetric, mOutputSpace->GetCMM( PDF::CMM::eOutput, PDF::eRelativeColorimetric ) );
	}
	else
	{
		ColorSpaceObject* cs = mDocument->GetDocumentColorSpace();
	
		if( cs && cs->ResourceType() == PDF::ICCColorSpace::eResourceType )
		{
			embeddedProfile = new PDF::Profile( static_cast<PDF::ICCColorSpace*>( cs ) );
			if( cs->Space() == RGBSpace || cs->Space() == IndexedRGBSpace)
			{
				mIsRGBDocument = true;
			}
		}
		else
		{
			PDF::ICCColorSpace* rasterICCColorSpace = GetRasterEmbededICCColorSpace( mIsRGBDocument );
			if( rasterICCColorSpace )
			{
				embeddedProfile = new PDF::Profile( rasterICCColorSpace );
			}
		}

		if( embeddedProfile != NULL )
		{
			renderer->SetProfile( mDocument, embeddedProfile, PDF::eRelativeColorimetric, mOutputSpace->GetCMM( PDF::CMM::eOutput, PDF::eRelativeColorimetric ) );
		}
		else
		{
			if( mIsRGBDocument )
			{
				renderer->SetProfile( mDocument, mOutputSpace, PDF::eRelativeColorimetric, mOutputSpace->GetCMM( PDF::CMM::eOutput, PDF::eRelativeColorimetric ) );
			}
			else
			{
				renderer->SetProfile( mDocument, proofSpaceProfile, PDF::eRelativeColorimetric, mOutputSpace->GetCMM( PDF::CMM::eOutput, PDF::eRelativeColorimetric ) );
			}
		}
	}

	mDocument->SetRenderer( renderer );

	return true;
}

/*!
	In case the document has an embedded profile it saves it to the specified path

	\param	embeddedProfileSavePath		The path to save the document embedded profile
	\param	embeddedProfileSavePath		Output parameter that stores the embedded profile name

	\return								TRUE if the document contains an embedded profile that it is successfully saved otherwise false.

*/
bool AOI_SoftProofRenderer::SaveDocumentEmbeddedProfile( const UTF16Char* outputFolderPath, UTF16Char* profileName )
{
	if( outputFolderPath != NULL )
	{
		PDF::ICCColorSpace* iccCs = NULL;
		PDF::ColorSpaceObject* cs = mDocument->GetDocumentColorSpace();

		//save embedded profile
		if( cs && cs->ResourceType() == PDF::ICCColorSpace::eResourceType )
		{
			iccCs = static_cast<PDF::ICCColorSpace*>( cs );
		}
		else
		{
			bool rgbDocument;
			iccCs = GetRasterEmbededICCColorSpace( rgbDocument );
		}

		if( iccCs )
		{
			ACPL::UString path = GetEmbededProfilePath( iccCs, outputFolderPath, profileName );

			uint8_t* data = new uint8_t[ iccCs->GetHeader().size ];
			iccCs->GetICCData( data );

			ACPL::FileSpec f2;
			f2.Make( path );
			if( !f2.Exists() )
				f2.Create();

			ACPL::FileStream file( f2, ACPL::eWritePermission );
			file.WriteBlock( data, iccCs->GetHeader().size );

			delete[] data;

			return true;
		}
	}

	return false;
}

PDF::ICCColorSpace* AOI_SoftProofRenderer::GetRasterEmbededICCColorSpace( bool& isRGB )
{
	isRGB = false;
	ICCColorSpace* iccColorSpace = NULL;
	bool checkIsRGBFailed = false, checkEmbededProfileFailed = false;

	for( uint32_t i = 0; i < mDocument->GetPageCount(); ++i )
	{
		mDocument->SetCurrentPage( i );
		
		PDF::Object *obj = NULL;
		ColorSpaceObject* colorSpace = NULL;

		// the document is considered RGB if all objects are raster type with RGB colorspace
		while( mDocument->TraverseNext( obj ) )
		{
			if( obj->GetType() == PDF::gRasterType )
			{
				colorSpace = ((PDF::Raster*)obj)->GetColorSpace();
				ICCColorSpace* iccCS = GetICCColorSpace(colorSpace);
			
				// Check embeded profile
				if( !checkEmbededProfileFailed && iccCS)
				{
					if(iccColorSpace == NULL)
					{
						iccColorSpace = iccCS;
					}
					else
					{
						if( iccCS->Space() != iccColorSpace->Space() || iccColorSpace->GetDescriptor() != iccCS->GetDescriptor() )
						{
							iccColorSpace = NULL;
							checkEmbededProfileFailed = true;
						}
					}
				}

				// Check is RGB document
				if( !checkIsRGBFailed )
				{
					if( colorSpace->Space() == RGBSpace || colorSpace->Space() == IndexedRGBSpace )
					{
						isRGB = true;		
					}
					else
					{
						isRGB = false;
						checkIsRGBFailed = true;
					}
				}
			}
			else if ( obj->GetType() != PDF::gLayerType )
			{
				isRGB = false;
				return NULL;
			}

			if( checkIsRGBFailed && checkEmbededProfileFailed )
			{
				isRGB = false;
				return NULL;
			}
		}
	}

	return iccColorSpace;
}


ACPL::UString AOI_SoftProofRenderer::GetEmbededProfilePath( PDF::ICCColorSpace* iccCs, const UTF16Char* outputFolderPath, UTF16Char* profileName )
{
	ACPL::UString profile = iccCs->GetDescriptor();
	if(profile.IsEmpty())
		profile = ACPL::UString::FromAscii( "EmbededProfile" );
	else
		profile = ACPL::FileSpec::ValidName( profile );
	profile += ".icc";

	ACPL::UString path = outputFolderPath;
	path += "\\";
	path += profile;

	wcscpy(profileName, profile.operator const aur::UniChar *());

	return path;
}


PDF::ICCColorSpace* AOI_SoftProofRenderer::GetICCColorSpace( ColorSpaceObject* cs )
{
	if( cs && cs->ResourceType() == PDF::ICCColorSpace::eResourceType )
	{
		return static_cast<PDF::ICCColorSpace*>( cs );
	}
	else if( cs && ( cs->ResourceType() == PDF::IndexColorSpace::eResourceType ) && 
		((PDF::IndexColorSpace*)cs)->GetBase() != NULL && ((PDF::IndexColorSpace*)cs)->GetBase()->ResourceType() == PDF::ICCColorSpace::eResourceType )
	{
		return static_cast<PDF::ICCColorSpace*>(((PDF::IndexColorSpace*)cs)->GetBase());
	}

	return NULL;
}

/*!
	Determines if the current document is rendered as an RGB Document.

	\return						TRUE if the document has Embedded RGB Profile, FALSE otherwise.
*/
bool AOI_SoftProofRenderer::IsRGBDocumentColorSpace() const
{
	return mIsRGBDocument;
}

/*!
	Renders the document using the specified rectangle, and the profiles used in setup method.
	It also saves the output in the specified format to a file.

	\param	outputPath			The path where the rendering result will be saved.
	\param	thumbnailType		jpg or png.
	\param	thumbnailMaxSize	The maximum size (width or height) of the output preview
	\param	quality				Quality factor when saving as JPEG.	
	\param	pageNumber			The document page tp be rendered.
	\return						TRUE if the method is successfully executed otherwise false.
*/
bool AOI_SoftProofRenderer::RenderPage( const UTF16Char* outputPath, const char* thumbnailType, uint32_t thumbnailMaxSize, uint32_t pageNumber, uint32_t quality )
{
	//Calculate DPU based on original page size
	float srcPageWidth, srcPageHeight;
	mDocument->GetPage( pageNumber )->GetDimensions( srcPageWidth, srcPageHeight );

	float dpu;
	uint32_t width, height;

	if( srcPageWidth > srcPageHeight ) 
	{ 
		width = thumbnailMaxSize;
		height = (uint32_t)ceil((float)width * srcPageHeight / srcPageWidth);
		dpu = width / srcPageWidth;
	}
	else 
	{ 
		height = thumbnailMaxSize;
        width = (uint32_t)ceil((float)height * srcPageWidth / srcPageHeight);
		dpu = height / srcPageHeight;
	}

	UpdateRenderer( width, height );

	// Apply DPU scaling factor to renderer and render the document
	PDF::Mapping map;
	map.Reset();
	map.map[0][0] = dpu;
	map.map[1][1] = dpu;
	map.map[2][0] = 0;
	map.map[2][1] = 0;

	( (ThumbnailRenderer*)mDocument->GetRenderer() )->SetMapping( map );

	( (ThumbnailRenderer*)mDocument->GetRenderer() )->RenderDocument( mDocument, true, true, NULL );

	// Calculate size of thumbnail in bytes and copy the data in buffer
	int32_t destLineBytes;
	if(::strcmp( thumbnailType, "png" ) == 0)
	{
		destLineBytes = width * (( (ThumbnailRenderer*)mDocument->GetRenderer() )->GetDeviceCMM()->GetSpaceComponents() + 1 );
	}
	else if(::strcmp( thumbnailType, "jpg" ) == 0)
	{
		destLineBytes = width * ( (ThumbnailRenderer*)mDocument->GetRenderer() )->GetDeviceCMM()->GetSpaceComponents();
	}
	else
	{
		return false;
	}

	uint32_t dataLen = destLineBytes * height;
	uint8_t* data = new uint8_t[dataLen];
	::memset( data, 0, dataLen );
	PDF::LRectangle ubounds;
	ubounds.left = 0;
	ubounds.top = 0;
	ubounds.right = width;
	ubounds.bottom = height;

	if( ::strcmp( thumbnailType, "png" ) == 0 )
	{
		( (ThumbnailRenderer*)mDocument->GetRenderer() )->CopyBits( data, ubounds, destLineBytes );
		SaveAsPNG( outputPath, data, width, height );
	}
	else if( ::strcmp( thumbnailType, "jpg" ) == 0 )
	{
		( (ThumbnailRenderer*)mDocument->GetRenderer() )->CopyBits( data, ubounds, destLineBytes, true );
		SaveAsJPG( outputPath, data, width, height, quality );
	}

	delete[] data;
	return true;
}


/*!
	Updates the renderer for rendering at specified dimensions

	\param	width	The rendering width

	\param	height	The rendering height
*/
void AOI_SoftProofRenderer::UpdateRenderer( uint32_t width, uint32_t height )
{
	if( ( (ThumbnailRenderer*)mDocument->GetRenderer() )->Update( width, height ) )
	{
		mDocument->Invalidate();
	}
}

/*!
	Returns the document renderer channel name with the specified index

	\param	index	The channel name index to return

	\return			The name of the channel with the specified index
*/
const char* AOI_SoftProofRenderer::GetDocumentChannelName( uint32_t index )
{
	return mDocument->GetRenderer()->GetChannelName( index );
}

/*!
	Returns the document renderer channels count

	\return			The name of the channel with the specified index
*/
uint32_t AOI_SoftProofRenderer::GetDocumentChannelCount()
{
	return mDocument->GetRenderer()->GetChannelCount();
}

/*!
	Specifies whether the document has alpha channel or not

	\return			True if alpha channel exists otherwise false.
*/
bool AOI_SoftProofRenderer::HasAlpha() const
{
	return mDocument->GetRenderer()->mHasAlpha;
}

/*!
	Specifies whether the document has transparency group or not

	\return			True if transparency groupexists otherwise false.
*/
bool AOI_SoftProofRenderer::HasTransparencyGroup() const
{
	return mDocument->GetRenderer()->mIsTransparencyGroup;
}

/*!
	Specifies the color system type

	\return			True if the colors are substractive.
*/
bool AOI_SoftProofRenderer::IsSubstractive() const
{
	return mDocument->GetRenderer()->mIsSubstractive;
}

/*!
	Reads the size of the image document

	\param		width	The image width to return
	\param		height	The image height to return

	\return		True if the size is successfully read otherwise false.
*/
bool AOI_SoftProofRenderer::GetImageSize( int32_t& width, int32_t& height ) const
{	
	PDF::Object *obj = NULL;
	while( mDocument->TraverseNext(obj) )
	{
		if( obj->GetType() == PDF::gRasterType )
		{
			PDF::Raster *ras = (PDF::Raster*)obj;
			height = ras->GetBitmap()->GetBitmap()->height;
			width = ras->GetBitmap()->GetBitmap()->width;
			return true;
		}
	}
	return false;
}


/*!
	Transforms the renderer result into the output format and retrieves the resulted row data.

	\param	dest					The output destination buffer.
	\param	width					The width of the area to be converted.
	\param	height					The height of the area to be converted.
	\param	rowBytes				The size of the output in bytes.
	\param	bImageAlphaBlending		The image alpha bleeding state.
*/
void AOI_SoftProofRenderer::ConvertToOutputColorSpace( uint8_t* dest, uint32_t width, uint32_t height, int32_t rowBytes, bool bImageAlphaBlending )
{
	PDF::LRectangle ubounds;
	ubounds.left = 0;
	ubounds.top = 0;
	ubounds.right = width;
	ubounds.bottom = height;
	
	( (ThumbnailRenderer*)mDocument->GetRenderer() )->CopyBits( dest, ubounds, rowBytes, bImageAlphaBlending );
}

/*!
	Renders the specified band in the document.

	\param	y					y coordinate in the document where the rendering starts.
	\param	renderingWidth		Width that is going to be rendered
	\param	renderingHeight		Height that is going to be rendered
	\param	dpu					DPU scaling factor

	\return						Pointer to the buffer containing the rendered raw data.
*/
uint8_t* AOI_SoftProofRenderer::RenderRawBand( uint32_t y, uint32_t renderingWidth, uint32_t renderingHeight, float dpu )
{
	UpdateRenderer( renderingWidth, renderingHeight );
	// Apply DPU scaling factor to renderer and render the document
	PDF::Mapping map;
	map.Reset();
	map.map[0][0] = dpu;
	map.map[1][1] = dpu;
	map.map[2][1] = - float( y );

	( (ThumbnailRenderer*)mDocument->GetRenderer() )->SetMapping( map );
	mDocument->Invalidate();
	( (ThumbnailRenderer*)mDocument->GetRenderer() )->RenderDocument( mDocument, true, true, NULL );

	return mDocument->GetRenderer()->mDestImage;
}

/*!
	Releases the resources and memory used by the renderer
*/
AOI_SoftProofRenderer::~AOI_SoftProofRenderer()
{
	mOutputSpace->Close( PDF::CMM::eOutput, PDF::eRelativeColorimetric );
	PDF::Renderer* renderer = mDocument->GetRenderer();
	delete renderer;
}

ThumbnailRenderer::ThumbnailRenderer() :
PDF::Renderer(),
	mProofProfile( NULL ),
	mDeviceCMM( NULL ),
	mProofIntent( PDF::eRelativeColorimetric ),
	mProofProfileOpen( false ),
	mWhichCopy( eNoMatch )
{
	SetAntiAliasGlyph( true );
	mDeviceWidth = 1;
	mDeviceHeight = 1;
	for( uint32_t i = 0; i != MAX_CHANNELS; ++i )
		mChannelSolidity[i] = 0;
}

/*!
	Gets Paper White Lab measurements for the current document

	\return		Structure containing information about the document Paper White Lab measurements.
*/
SpotColorCieLab AOI_SoftProofRenderer::GetProfilePaperWhiteCIELab()
{
	SpotColorCieLab whitePaperLab;
	double	lab[3];
	double	src[MAX_CHANNELS];
	::memset( src, 0, MAX_CHANNELS * sizeof( double ) );

	((ThumbnailRenderer*)mDocument->GetRenderer() )->GetProfile()->Open( PDF::CMM::eInput, PDF::eAbsoluteColorimetric );
	((ThumbnailRenderer*)mDocument->GetRenderer() )->GetProfile()->GetCMM( PDF::CMM::eInput, PDF::Intent::eAbsoluteColorimetric )->Color2Lab( src, lab );
	((ThumbnailRenderer*)mDocument->GetRenderer() )->GetProfile()->Close( PDF::CMM::eInput, PDF::eAbsoluteColorimetric );
	whitePaperLab.mName[0] = '\0';
	whitePaperLab.mLab[0] = (float)lab[0];
	whitePaperLab.mLab[1] = (float)lab[1];
	whitePaperLab.mLab[2] = (float)lab[2];

	return whitePaperLab;
}

/*!
	Gets the number of spot colors present in the current document

	\return		The number of spot colors in document.
*/
uint32_t AOI_SoftProofRenderer::GetDocumentSpotColorsCount()
{
	return ((ThumbnailRenderer*)mDocument->GetRenderer())->GetExtraPlates().size();
}


/*!
	Retrieve name and lab colour information about the spot color found at the specified channel index.

	\param	index	Spot Color channel index in the document

	\return		Structure containing information about the spot color found at the specified index.
*/
SpotColorCieLab AOI_SoftProofRenderer::GetSpotColorCIELab( uint32_t index )
{
	SpotColorCieLab spotColor;
	::strcpy( spotColor.mName, ((ThumbnailRenderer*)mDocument->GetRenderer())->GetExtraPlates()[index]);
	
	for( int j = 0; j != mDocument->GetColorantCount(); ++j )
	{
		if( ::stricmp( spotColor.mName, mDocument->GetColorant(j).name ) == 0 )
		{
			spotColor.mLab[0] = mDocument->GetColorant(j).lab[0];
			spotColor.mLab[1] = mDocument->GetColorant(j).lab[1];
			spotColor.mLab[2] = mDocument->GetColorant(j).lab[2];
			
			const uint8_t* rgbVal = (( (ThumbnailRenderer*)mDocument->GetRenderer() )->GetChannelRGB( index ));
			spotColor.mRgb[0] = rgbVal[0];
			spotColor.mRgb[1] = rgbVal[1];
			spotColor.mRgb[2] = rgbVal[2];
			break;
		}
	}
	return spotColor;
}

ThumbnailRenderer::~ThumbnailRenderer()
{
	if( mProofProfile )
	{
		if( mProofProfileOpen )
			mProofProfile->Close( PDF::CMM::eInput, mProofIntent );
		mProofProfile->Dispose();
	}
	mDeviceCMM->Dispose();
}

void ThumbnailRenderer::SetProfile( PDF::Document* doc, PDF::ProfilePtr refProf, PDF::Intent intent, PDF::CMM* deviceCMM )
{
	refProf->Clone();
	if( mProofProfile )
	{
		if( mProofProfileOpen )
			mProofProfile->Close( PDF::CMM::eInput, mProofIntent );
		mProofProfile->Dispose();
	}
	mProofProfile = refProf;
	mProofIntent = intent;
	deviceCMM->Clone();
	mDeviceCMM->Dispose();
	mDeviceCMM = deviceCMM;
	mProofProfileOpen = false;
	mVisiblePlates = UINT64_MAX;

	//
	//	Clean up current setup
	//
	for( uint32_t i = 0; i != MAX_CHANNELS; ++i )
		mChannelSolidity[i] = 0;
	mChannelRGB.clear();
	mExtraPlates.clear();
	//
	//	Add colorants from document
	//
	if( doc )
	{
		DeterminePlates( doc );
		AddDocumentColorants( doc );
	}

	if( doc->GetFileType() == "pdf" )
		FindSpotColorsInLibraries( doc );

	ACPL::Array<char*>	extraPlates;
	for( size_t i = 0; i != mExtraPlates.size(); ++i )
		extraPlates.Append( const_cast<char*>( mExtraPlates[i].operator const char*() ) );
	Setup( mProofProfile->GetFileSpec(), &extraPlates, mDeviceWidth, mDeviceHeight, true, false, true );

	switch( mProofProfile->GetSpace() )
	{
	case PDF::RGBSpace :
		if( mComponents > 3 )
			mWhichCopy = eDeviceN;
		else
			mWhichCopy = EqualBytes( &mProofProfile->GetHeader(), &mDeviceCMM->GetHeader(), 16 * sizeof(uint32_t) ) &&
			EqualBytes( &mProofProfile->GetHeader().illuminant, &mDeviceCMM->GetHeader().illuminant, 6 + 4 ) ? eNoMatch : eICC;
		break;
	case PDF::CMYKSpace :
		if( mComponents > 4 )
			mWhichCopy = eDeviceN;
		else
			mWhichCopy = eICC;
		break;
	default :
		mWhichCopy = eDeviceN;
		break;
	}

	if( mWhichCopy == eICC || mWhichCopy == eDeviceN )
	{
		mProofProfile->Open( PDF::CMM::eInput, mProofIntent );
		/*if( mProofIntent == eRelativeColorimetric )
		{
			mProofProfile->GetCMM( PDF::CMM::eInput, mProofIntent )->BlackPointCompensation( 0.0F );
		}*/
		mProofProfileOpen = true;
	}
}

void ThumbnailRenderer::FindSpotColorsInLibraries( PDF::Document* doc )
{
	PDF::LookupSpotColor fe;
	
	for( uint32_t cIdx = 0; cIdx != doc->GetColorantCount(); ++cIdx )
	{
		if( fe.FindInLibraries( doc->GetColorant( cIdx ).name ) && fe.mSpace == PDF::LABSpace )
		{ // color in Lab
			PDF::Document::Colorant colorant = doc->GetColorant( cIdx );
			colorant.lab[0] = fe.mLab[0];
			colorant.lab[1] = fe.mLab[1];
			colorant.lab[2] = fe.mLab[2];
			doc->SetColorant( cIdx, colorant );
		}
	}	
}

bool	ThumbnailRenderer::Update( int32_t inWidth, int32_t inHeight )
{
	if( mDeviceWidth != inWidth || mDeviceHeight != inHeight )
	{
		ACPL::Array<char*>	extraPlates;
		for( size_t i = 0; i != mExtraPlates.size(); ++i )
			extraPlates.Append( const_cast<char*>( mExtraPlates[i].operator const char*() ) );
		Setup( mProofProfile->GetFileSpec(), &extraPlates, inWidth, inHeight, true, false, true );
		return true;
	}
	return false;
}

static void	DetermineCIELab( const double* in, PDF::ColorSpaceObject* inSpace, PDF::CMM* destCMM, float outLab[3] )
{
	//	Simply copy the values if it is already CIELab
	if( inSpace->Space() == PDF::LABSpace )
	{
		outLab[0] = float( in[0] );
		outLab[1] = float( in[1] );
		outLab[2] = float( in[2] );
		return;
	}

	//	Determine CIELab from other colorspaces
	uint32_t	inComps = inSpace->NrOfComponents();
	uint32_t	outComps = destCMM->GetSpaceComponents();
	if( inComps <= outComps && dynamic_cast<PDF::DeviceColorSpace*>( inSpace ) != NULL )
	{
		//	Link channels
		uint32_t	linkCnt = 0;
		double	val[MAX_CHANNELS];
		::memset( val, 0, MAX_CHANNELS*sizeof(double) );
		for( uint32_t i = 0; i != inComps && linkCnt == i; ++i )
		{
			const char* inChannelName = inSpace->ChannelName( i );
			for( uint32_t j = 0; j != outComps; ++j )
			{
				const char* outChannelName = destCMM->GetChannelName( j );
				if( ::stricmp( inChannelName, outChannelName ) == 0 )
				{
					val[ j ] = in[ i ];
					++linkCnt;
					break;
				}
			}
		}
		//	Convert to CIELab if there is a link
		if( linkCnt == inComps )
		{
			double	lab[3];
			destCMM->Color2Lab( val, lab );
			outLab[0] = (float)lab[0];
			outLab[1] = (float)lab[1];
			outLab[2] = (float)lab[2];
			return;
		}
	}
	//	If there is no link or not a device space
	uint16_t	in16[MAX_CHANNELS];
	for( uint32_t i = 0; i != inComps; ++i )
		in16[i] = uint16_t( in[i] * PDF::Component1 );
	inSpace->GetCIELabFloat( in16, outLab );
}

void	ThumbnailRenderer::DeterminePlates( PDF::Document* doc )
{
	//
	//	Determine the plates based on the DeviceN colorspaces in the documents
	//
	uint32_t		i,j;
	int32_t		profileChannelCount = ColorSpaceComponents( mProofProfile->GetSpace() );
	Array<PDF::Document::Colorant>	extraPlates;
	Array<PDF::Function*>			colorantFunc;
	Array<PDF::ColorSpaceObject*>	colorantSpace;
	Array<PDF::DeviceN*>			devNs;
	PDF::Document::Colorant			colorant;

	PDF::DeviceN*	devn = NULL;
	int32_t devNCnt = 0;
	while( ( devn = (PDF::DeviceN*)doc->TraverseCommitedResources( PDF::DeviceN::eResourceType, devn ) ) != NULL )
	{
		++devNCnt;
		for( j = 0; j != devn->NrOfComponents(); ++j )
		{
			const char* n = devn->ChannelName( j );
			bool	found = ::strcmp( n, "None" ) == 0 || ::strcmp( n, "All" ) == 0;
			//	Find channel in process colors (profile)
			if( mProofProfile->GetSpace() == PDF::GraySpace || mProofProfile->GetSpace() == PDF::RGBSpace )
				found = true;
			else
				for( int32_t ch = 0; !found && ch != profileChannelCount; ++ch )
				{
					String	profChannelName;
					mProofProfile->GetChannel( ch, profChannelName );
					found = profChannelName == n;
				}
				//	Find channel in document colorants
				for( i = 0; !found && i != doc->GetColorantCount(); ++i )
					found = ::stricmp( n, doc->GetColorant(i).name ) == 0;
				if( !found )
				{
					for( i = 0; i != extraPlates.GetCount() && ::stricmp( n, extraPlates[i].name ); ++i )
						;
					if( i < uint32_t( doc->GetInstance()->GetMaxChannels()-profileChannelCount-doc->GetColorantCount() ) )
					{
						if( i == extraPlates.GetCount() )
						{
							// check if this a stylerepository item
							bool inRepo = false;
							if( devn->NrOfComponents() == 1 && devn->OwnerCount() == 1 )
								for( int32_t r = 0 ; r != doc->GetRepositoryStyleCount(); ++r )
								{
									PDF::Style*	s = doc->GetRepositoryStyle( r );
									if( s->ResourceType() == PDF::Solid::eResourceType )
									{
										if( devn->GetResourceID() == PDF::SolidPtr(s)->GetSpace()->GetResourceID() )
											inRepo = true;
									}
								}

								if( inRepo == false )
								{
									colorant = PDF::Document::Colorant();
									::strcpy( colorant.name, n );
									extraPlates.Append( colorant );
									colorantSpace.Append( devn->ChannelSpace( j ) );
									colorantFunc.Append( devn->ChannelFunction( j ) );
									devNs.Append( devn );
								}
						}
						else if( colorantSpace[i] == NULL )
						{
							colorantSpace[i] = devn->ChannelSpace( j );
							colorantFunc[i] = devn->ChannelFunction( j );
						}
					}
				}
		}
	}

	if( extraPlates.GetCount() == 0 )
		return;

	mProofProfile->Open( PDF::CMM::eInput, PDF::eAbsoluteColorimetric );
	PDF::CMM* proofCMM = mProofProfile->GetCMM( PDF::CMM::eInput, PDF::eAbsoluteColorimetric );

	PDF::LookupSpotColor	fe;
	for( uint32_t col = 0; col != extraPlates.GetCount(); ++col )
	{
		colorant = extraPlates[col];
		double	tr[MAX_CHANNELS];
		if( colorantSpace[col] )
		{
			double	in = 1;
			colorantFunc[col]->Transform( &in, tr );
			DetermineCIELab( tr, colorantSpace[col], proofCMM, colorant.lab );
		}
		else
		{
			if( fe.FindInLibraries( colorant.name ) && fe.mSpace == PDF::LABSpace )
			{ // color in Lab
				colorant.lab[0] = fe.mLab[ 0 ];
				colorant.lab[1] = fe.mLab[ 1 ];
				colorant.lab[2] = fe.mLab[ 2 ];
			}
			else
			{
				if( !::stricmp( colorant.name, "Red" ) ||
					!::stricmp( colorant.name, "Green" ) ||
					!::stricmp( colorant.name, "Blue" ) )
				{
					uint16_t	rgb16[3] = { 0, 0, 0 };
					if( !::stricmp( colorant.name, "Red" ) )
						rgb16[1] = rgb16[2] = PDF::Component1;
					else if( !::stricmp( colorant.name, "Green" ) )
						rgb16[0] = rgb16[2] = PDF::Component1;
					else
						rgb16[0] = rgb16[1] = PDF::Component1;
					doc->GetDeviceCMYKColorSpace()->GetCIELabFloat( rgb16, colorant.lab );
				}
				else
				{
					PDF::DeviceN*	devN = devNs[ col ];

					double	in[MAX_CHANNELS];
					::memset( in, 0, MAX_CHANNELS * sizeof( double ) );

					uint32_t	index = 0;
					while( ::stricmp( devN->ChannelName( index ), colorant.name ) )
						++index;
					in[ index ] = 1;

					devN->GetFunction()->Transform( in, tr );
					DetermineCIELab( tr, devN->GetAlternate(), proofCMM, colorant.lab );
				}
			}
		}
		if( devNCnt == 1 )
		{
			if( doc->GetPageCount() == 1 &&
				doc->GetPage()->GetFirstLayer()->GetNext() == NULL &&
				doc->GetPage()->GetFirstLayer()->GetFirstChild() &&
				doc->GetPage()->GetFirstLayer()->GetFirstChild()->GetNext() == NULL &&
				doc->GetPage()->GetFirstLayer()->GetFirstChild()->GetType() == PDF::gRasterType )
			{
				PDF::RasterPtr ras = (PDF::RasterPtr)doc->GetPage()->GetFirstLayer()->GetFirstChild();
				XML* xml = ras->MetaData( false );
				if( xml )
				{
					XML::Node* ci = xml->root->GetNodeByName( "ChannelInfo" );
					if( ci )
					{
						for( i = 0 ; i != ci->GetNrOfChildren(); ++i )
						{
							XML::Node* chan = ci->GetNodeByIndex( i );
							if( chan->GetAttributeByName( "Name" )->ToString() == colorant.name )
							{
								XML::Node* opa = chan->GetNodeByName( "Opacity" );
								colorant.solidity = opa->ToFloat();
							}
						}
					}
				}
			}
		}

		uint32_t	cIdx = 0;
		while( cIdx != doc->GetColorantCount() )
		{
			if( ::stricmp( doc->GetColorant( cIdx ).name, colorant.name ) == 0 )
				break;
			++cIdx;
		}
		doc->SetColorant( cIdx, colorant );
	}

	mProofProfile->Close( PDF::CMM::eInput, PDF::eAbsoluteColorimetric );
}

void	ThumbnailRenderer::AddDocumentColorants( PDF::Document* doc )
{
	//
	//	Determine the extra channels based on the proof profile and colorant in the document
	//
	uint32_t	i;
	size_t		j;
	int32_t	profileChannelCount = ColorSpaceComponents( mProofProfile->GetSpace() );
	for( i = 0; i != doc->GetColorantCount() && i < uint32_t( doc->GetInstance()->GetMaxChannels() - profileChannelCount ); ++i )
	{
		bool	found = false;
		const PDF::Document::Colorant& colorant = doc->GetColorant(i);
		if( mProofProfile->GetSpace() == PDF::GraySpace || mProofProfile->GetSpace() == PDF::RGBSpace )
			found = true;
		else
			for( int32_t ch = 0; !found && ch != profileChannelCount; ++ch )
			{
				ACPL::String	profChannelName;
				mProofProfile->GetChannel( ch, profChannelName );
				found = profChannelName == colorant.name;
			}
			if( !found )
			{
				j = mExtraPlates.size();
				mExtraPlates.push_back( colorant.name );

				uint8_t		rgb8[3];
				uint16_t	labV[3];
				RGBValues	channelRGB;

				double	dL = ( ( colorant.lab[0] / 100 ) - 1 ) * 0xFF;
				double	da = colorant.lab[1];
				double	db = colorant.lab[2];
				uint32_t	v;
				if( colorant.tvi == 0 )
				{
					for( v = 0; v != 256; ++v )
					{
						labV[0] = uint16_t( 0xFF00 + v * dL );
						labV[1] = uint16_t( 0x8000 + v * da );
						labV[2] = uint16_t( 0x8000 + v * db );
						mDeviceCMM->Lab2Image( labV, rgb8, 1 );
						channelRGB.v[v][0] = rgb8[ 0 ];
						channelRGB.v[v][1] = rgb8[ 1 ];
						channelRGB.v[v][2] = rgb8[ 2 ];
					}
				}
				else
				{
					for( v = 0; v != 256; ++v )
					{
						double	percentage = v / 255.0;
						percentage += sin( percentage * pi ) * colorant.tvi;
						if( percentage < 0 )
							percentage = 0;
						else if( percentage > 1 )
							percentage = 255;
						else
							percentage *= 255;
						labV[0] = uint16_t( 0xFF00 + percentage * dL );
						labV[1] = uint16_t( 0x8000 + percentage * da );
						labV[2] = uint16_t( 0x8000 + percentage * db );
						mDeviceCMM->Lab2Image( labV, rgb8, 1 );
						channelRGB.v[v][0] = rgb8[ 0 ];
						channelRGB.v[v][1] = rgb8[ 1 ];
						channelRGB.v[v][2] = rgb8[ 2 ];
					}
				}
				mChannelRGB.push_back( channelRGB );
				mChannelSolidity[j] = colorant.solidity;
			}
	}
}

void	ThumbnailRenderer::CopyBitsDeviceN( uint8_t* dest, const PDF::LRectangle& updateBounds, int32_t rowBytes, bool bImageAlphaBlending )
{
	int32_t	w = updateBounds.right - updateBounds.left;
	uint8_t*	src;
	uint8_t*	srcX;
	uint8_t*	destX;
	uint8_t*	srcAlpha;
	uint16_t	alpha;
	uint16_t	backDrop;
	int32_t	top = updateBounds.top - int32_t( mMapping.map[2][1] );
	int32_t	bottom = top + updateBounds.bottom - updateBounds.top;
	int32_t	left = updateBounds.left - int32_t( mMapping.map[2][0] );
	int32_t	right = left + w;
	int32_t	c;
	int32_t	x, y;
	int32_t	components = mComponents;
	int32_t	profComponents = ColorSpaceComponents( mProofProfile->GetSpace() );
	bool	showBack = false;
	bool	mask[MAX_CHANNELS];

	for( c = 0, x = 0; c != components; ++c )
		if( ( mask[c] = ( int64_t( 1 << c ) & mVisiblePlates ) == 0 ) == false )
			++x;
	if( x == 1 )
	{
		CopyBitsSingleChannel( dest, updateBounds, rowBytes, bImageAlphaBlending );
		return;
	}
	uint16_t*	lab = new uint16_t[ w * 3 ];
	uint8_t*	blended = new uint8_t[ w * ( mComponents > 3 ? mComponents : 3 ) ];

	src = mDestImage + updateBounds.top * mDestLineBytes + updateBounds.left * mDestPixelBytes;
	//dest += updateBounds.top * rowBytes + updateBounds.left * 4;
	for( y = top; y != bottom; ++y )
	{
		srcX = src;
		destX = blended;
		if( mIsSubstractive )
		{
			for( x = left; x != right; ++x )
			{
				if( ( alpha = srcX[components] ) == 255 )
				{
					for( c = 0; c != profComponents; ++c )
					{
						*destX = mask[c] ? 0 : *srcX;
						++destX;
						++srcX;
					}
					srcX += components - profComponents;
				}
				else if( alpha == 0 )
				{
					for( c = 0; c != profComponents; ++c )
						*destX++ = 0;
					srcX += components;
				}
				else
				{
					for( c = 0; c != profComponents; ++c )
					{
						*destX = mask[c] ? 0 : uint8_t( ( *srcX * alpha ) >> 8 );
						++destX;
						++srcX;
					}
					srcX += components - profComponents;
				}
				++srcX;
			}
		}
		else
		{
			for( x = left; x != right; ++x )
			{
				if( ( alpha = srcX[components] ) == 255 )
				{
					for( c = 0; c != profComponents; ++c )
					{
						*destX = mask[c] ? 0 : *srcX;
						++destX;
						++srcX;
					}
					srcX += components - profComponents;
				}
				else if( alpha == 0 )
				{
					for( c = 0; c != profComponents; ++c )
						*destX++ = 255;
					srcX += components;
				}
				else
				{
					backDrop = ( 255 - alpha ) << 8;
					for( c = 0; c != profComponents; ++c )
					{
						*destX = mask[c] ? 0 : uint8_t( ( backDrop + *srcX * alpha ) >> 8 );
						++destX;
						++srcX;
					}
					srcX += components - profComponents;
				}
				++srcX;
			}
		}
		//
		//	Convert from Proof ICC space to Screen RGB
		//
		mProofProfile->Image2Lab( mProofIntent, blended, lab, w );
		mDeviceCMM->Lab2Image( lab, blended, w );
		//
		//	Mix in spot colors
		//
		for( c = profComponents; c != components; ++c )
			if( mask[c] == false )
			{
				const uint8_t (*mul)[256] = GetMUL8();
				const RGBValues&	component = mChannelRGB[c-profComponents];
				srcX = src + c;
				srcAlpha = src + components;
				destX = blended;
				if( mChannelSolidity[c-profComponents] == 0 )
				{
					for( x = left; x != right; ++x )
					{
						if( ( alpha = *srcAlpha ) == 255 )
							alpha = *srcX;
						else if( alpha != 0 )
							alpha = ( *srcX * alpha ) >> 8;
						uint8_t	srcR = component.v[alpha][0];
						uint8_t	srcG = component.v[alpha][1];
						uint8_t	srcB = component.v[alpha][2];
						srcX += components + 1;
						srcAlpha += components + 1;
						*destX = mul[ srcR ][ *destX ];
						++destX;
						*destX = mul[ srcG ][ *destX ];
						++destX;
						*destX = mul[ srcB ][ *destX ];
						++destX;
					}
				}
				else
				{
					float	solidity = mChannelSolidity[c-profComponents];
					float	invSolidity;
					for( x = left; x != right; ++x )
					{
						if( ( alpha = *srcAlpha ) == 255 )
							alpha = *srcX;
						else if( alpha != 0 )
							alpha = ( *srcX * alpha ) >> 8;
						uint8_t	srcR = component.v[alpha][0];
						uint8_t	srcG = component.v[alpha][1];
						uint8_t	srcB = component.v[alpha][2];
						srcX += components + 1;
						srcAlpha += components + 1;

						//	Solidity formula
						//		Solidity = 0 -> Full mixing with background
						//		Solidity = 1 -> Full inkcoverage, background invisible
						//
						//	Effective solidity is related to coverage of ink (ink amount)
						//		S2 = Solidity * coverage (= *srcX)
						//	Background contribution is inverse of the solidity
						//		Background contribution = 1 - S2
						//
						if( ( invSolidity = 1 - ( solidity * alpha / 255 ) ) == 1 )
						{
							*destX = mul[ srcR ][ *destX ];
							++destX;
							*destX = mul[ srcG ][ *destX ];
							++destX;
							*destX = mul[ srcB ][ *destX ];
							++destX;
						}
						else
						{
							*destX = mul[ srcR ][ 255 - uint8_t( ( 255 - *destX ) * invSolidity ) ];
							++destX;
							*destX = mul[ srcG ][ 255 - uint8_t( ( 255 - *destX ) * invSolidity ) ];
							++destX;
							*destX = mul[ srcB ][ 255 - uint8_t( ( 255 - *destX ) * invSolidity ) ];
							++destX;
						}
					}
				}
			}
			//
			//	Copy and blend with alpha
			//
			srcX = blended;
			destX = dest;
			srcAlpha = src + components;
			for( x = left; x != right; ++x )
			{
				*destX++ = *srcX++;
				*destX++ = *srcX++;
				*destX++ = *srcX++;
				if( !bImageAlphaBlending )
					*destX++ = *srcAlpha;
				srcAlpha += components + 1;
			}
			src += mDestLineBytes;
			dest += rowBytes;
	}
	delete[] blended;
	delete[] lab;
}

void	ThumbnailRenderer::CopyBitsNoMatch( uint8_t* dest, const PDF::LRectangle& updateBounds, int32_t rowBytes, bool bImageAlphaBlending )
{
	if( ( mVisiblePlates&7 ) == ( 1 << 0 ) || ( mVisiblePlates&7 ) == ( 1 << 1 ) || ( mVisiblePlates&7 ) == ( 1 << 2 ) )
	{
		CopyBitsSingleChannel( dest, updateBounds, rowBytes, bImageAlphaBlending );
		return;
	}

	uint8_t*	src;
	uint8_t*	srcX;
	uint8_t*	destX;
	uint16_t	alpha;
	uint16_t	backDrop;
	bool	showBack = false;
	bool	mask[3];
	int32_t	top = updateBounds.top - int32_t( mMapping.map[2][1] );
	int32_t	bottom = top + updateBounds.bottom - updateBounds.top;
	int32_t	left = updateBounds.left - int32_t( mMapping.map[2][0] );
	int32_t	right = left + updateBounds.right - updateBounds.left;

	mask[0] = ( ( 1 << 0 ) & mVisiblePlates ) == 0;
	mask[1] = ( ( 1 << 1 ) & mVisiblePlates ) == 0;
	mask[2] = ( ( 1 << 2 ) & mVisiblePlates ) == 0;

	src = mDestImage + updateBounds.top * mDestLineBytes + updateBounds.left * mDestPixelBytes;
	//dest += updateBounds.top * rowBytes + updateBounds.left * 4;
	for( int32_t y = top; y != bottom; ++y )
	{
		srcX = src;
		destX = dest;
		src += mDestLineBytes;
		dest += rowBytes;
		for( int32_t x = left; x != right; ++x )
		{
			if( !bImageAlphaBlending )
			{			
				*destX++ = *srcX++;
				*destX++ = *srcX++;
				*destX++ = *srcX++;
				*destX++ = *srcX++;
			}
			else
			{
				if( ( alpha = srcX[3] ) == 255 )
				{
					*destX++ = mask[0] ? 0 : srcX[0];
					*destX++ = mask[1] ? 0 : srcX[1];
					*destX++ = mask[2] ? 0 : srcX[2];
				}
				else if( alpha == 0 )
				{
					register uint8_t backDrop8;
					if( showBack && ( ( ( x >> 3 ) + ( y >> 3 ) ) & 1 ) == 0 )
						backDrop8 = 191;
					else
						backDrop8 = 255;
					*destX++ = backDrop8;
					*destX++ = backDrop8;
					*destX++ = backDrop8;
				}
				else
				{
					if( showBack && ( ( ( x >> 3 ) + ( y >> 3 ) ) & 1 ) == 0 )
						backDrop = ( 255 - alpha ) * 192;
					else
						backDrop = ( 255 - alpha ) << 8;
					*destX++ = uint8_t( ( backDrop + ( mask[0] ? 0 : srcX[0] ) * alpha ) >> 8 );
					*destX++ = uint8_t( ( backDrop + ( mask[1] ? 0 : srcX[1] ) * alpha ) >> 8 );
					*destX++ = uint8_t( ( backDrop + ( mask[2] ? 0 : srcX[2] ) * alpha ) >> 8 );
				}
				srcX += 4;
			}
		}
	}
}

void	ThumbnailRenderer::CopyBitsICC( uint8_t* dest, const PDF::LRectangle& updateBounds, int32_t rowBytes, bool bImageAlphaBlending )
{
	int32_t	w = updateBounds.right - updateBounds.left;
	uint8_t*	src;
	uint8_t*	srcX;
	uint8_t*	destX;
	uint8_t*	srcAlpha;
	uint16_t	alpha;
	uint16_t	backDrop;
	int32_t	top = updateBounds.top - int32_t( mMapping.map[2][1] );
	int32_t	bottom = top + updateBounds.bottom - updateBounds.top;
	int32_t	left = updateBounds.left - int32_t( mMapping.map[2][0] );
	int32_t	right = left + w;
	int32_t	c;
	int32_t	x, y;
	int32_t	components = mComponents;
	bool	mask[MAX_CHANNELS];

	for( c = 0, x = 0; c != components; ++c )
		if( ( mask[c] = ( int64_t( 1 << c ) & mVisiblePlates ) == 0 ) == false )
			++x;
	if( x == 1 )
	{
		CopyBitsSingleChannel( dest, updateBounds, rowBytes, bImageAlphaBlending );
		return;
	}
	uint16_t*	lab = new uint16_t[ w * 3 ];
	uint8_t*	blended = new uint8_t[ w * ( mComponents > 3 ? mComponents : 3 ) ];

	src = mDestImage + updateBounds.top * mDestLineBytes + updateBounds.left * mDestPixelBytes;
	//dest += updateBounds.top * rowBytes + updateBounds.left * 4;
	for( y = top; y != bottom; ++y )
	{
		srcX = src;
		destX = blended;
		if( mIsSubstractive )
		{
			for( x = left; x != right; ++x )
			{
				if( ( alpha = srcX[components] ) == 255 )
				{
					for( c = 0; c != components; ++c )
					{
						*destX = mask[c] ? 0 : *srcX;
						++destX;
						++srcX;
					}
				}
				else if( alpha == 0 )
				{
					for( c = 0; c != components; ++c )
						*destX++ = 0;
					srcX += components;
				}
				else
				{
					for( c = 0; c != components; ++c )
					{
						*destX = mask[c] ? 0 : uint8_t( ( *srcX * alpha ) >> 8 );
						++destX;
						++srcX;
					}
				}
				++srcX;
			}
		}
		else
		{
			for( x = left; x != right; ++x )
			{
				if( ( alpha = srcX[components] ) == 255 )
				{
					for( c = 0; c != components; ++c )
					{
						*destX = mask[c] ? 0 : *srcX;
						++destX;
						++srcX;
					}
				}
				else if( alpha == 0 )
				{
					for( c = 0; c != components; ++c )
						*destX++ = 255;
					srcX += components;
				}
				else
				{
					backDrop = ( 255 - alpha ) << 8;
					for( c = 0; c != components; ++c )
					{
						*destX = mask[c] ? 0 : uint8_t( ( backDrop + *srcX * alpha ) >> 8 );
						++destX;
						++srcX;
					}
				}
				++srcX;
			}
		}
		mProofProfile->Image2Lab( mProofIntent, blended, lab, w );
		mDeviceCMM->Lab2Image( lab, blended, w );
		srcX = blended;
		destX = dest;
		srcAlpha = src + components;
		for( x = left; x != right; ++x )
		{
			*destX++ = *srcX++;
			*destX++ = *srcX++;
			*destX++ = *srcX++;
			if( !bImageAlphaBlending )
				*destX++ = *srcAlpha;
			srcAlpha += components + 1;
		}
		src += mDestLineBytes;
		dest += rowBytes;
	}
	delete[] blended;
	delete[] lab;
}

void	ThumbnailRenderer::CopyBitsSingleChannel( uint8_t* dest, const PDF::LRectangle& updateBounds, int32_t rowBytes, bool bImageAlphaBlending )
{
	uint8_t*	src;
	uint8_t*	srcX;
	uint8_t*	destX;
	uint16_t	alpha;
	bool	showBack = false;
	int32_t	top = updateBounds.top - int32_t( mMapping.map[2][1] );
	int32_t	bottom = top + updateBounds.bottom - updateBounds.top;
	int32_t	left = updateBounds.left - int32_t( mMapping.map[2][0] );
	int32_t	right = left + updateBounds.right - updateBounds.left;
	int32_t	components = mComponents;
	int32_t	c;
	register uint8_t backDrop8;

	for( c = 0; c != components; ++c )
		if( ( int64_t( 1 << c ) & mVisiblePlates ) != 0 )
			break;

	src = mDestImage + updateBounds.top * mDestLineBytes + updateBounds.left * mDestPixelBytes;
	//dest += updateBounds.top * rowBytes + updateBounds.left * 4;
	for( int32_t y = top; y != bottom; ++y )
	{
		srcX = src;
		destX = dest;
		src += mDestLineBytes;
		dest += rowBytes;
		if( mIsSubstractive )
		{
			for( int32_t x = left; x != right; ++x )
			{
				if( ( alpha = srcX[components] ) == 255 )
					backDrop8 = 255 - srcX[c];
				else if( alpha == 0 )
				{
					if( showBack && ( ( ( x >> 3 ) + ( y >> 3 ) ) & 1 ) == 0 )
						backDrop8 = 191;
					else
						backDrop8 = 255;
				}
				else
				{
					register uint16_t	backDrop;
					if( showBack && ( ( ( x >> 3 ) + ( y >> 3 ) ) & 1 ) == 0 )
						backDrop = ( 255 - alpha ) * 192;
					else
						backDrop = ( 255 - alpha ) << 8;
					backDrop8 = uint8_t( ( backDrop + ( 255 - srcX[c] ) * alpha ) >> 8 );
				}
				*destX++ = backDrop8;
				*destX++ = backDrop8;
				*destX++ = backDrop8;

				if( !bImageAlphaBlending )
				{
					*destX++ = 255;
				}

				srcX += mDestPixelBytes;
			}
		}
		else
		{
			for( int32_t x = left; x != right; ++x )
			{
				if( ( alpha = srcX[components] ) == 255 )
					backDrop8 = srcX[c];
				else if( alpha == 0 )
				{
					if( showBack && ( ( ( x >> 3 ) + ( y >> 3 ) ) & 1 ) == 0 )
						backDrop8 = 191;
					else
						backDrop8 = 255;
				}
				else
				{
					register uint16_t	backDrop;
					if( showBack && ( ( ( x >> 3 ) + ( y >> 3 ) ) & 1 ) == 0 )
						backDrop = ( 255 - alpha ) * 192;
					else
						backDrop = ( 255 - alpha ) << 8;
					backDrop8 = uint8_t( ( backDrop + srcX[c] * alpha ) >> 8 );
				}
				*destX++ = backDrop8;
				*destX++ = backDrop8;
				*destX++ = backDrop8;

				if( !bImageAlphaBlending )
				{
					*destX++ = 255;
				}

				srcX += mDestPixelBytes;
			}
		}
	}
}

void	ThumbnailRenderer::CopyBits( uint8_t* dest, const PDF::LRectangle& updateBounds, int32_t rowBytes, bool bImageAlphaBlending )
{
	switch( mWhichCopy )
	{
	case eNoMatch :
		CopyBitsNoMatch( dest, updateBounds, rowBytes, bImageAlphaBlending );
		break;
	case eDeviceN :
		CopyBitsDeviceN( dest, updateBounds, rowBytes, bImageAlphaBlending );
		break;
	case eICC :
		CopyBitsICC( dest, updateBounds, rowBytes, bImageAlphaBlending );
		break;
	}
}

void ThumbnailRenderer::SetVisiblePlates( uint64_t mask )
{
	mVisiblePlates = mask;
}