#pragma once

#include <ACPL/Types.h>

namespace aur {
	namespace ACPL {
		class	XML;
		class	String;
		class	UString;
		class	Stream;
	}

	namespace ADA {

	typedef uint32_t MarkType;

#define	SDK_VERSION		304

	static const uint32_t	kKEY_Class1 = 0x44455354;	//	'DEST'
	static const uint32_t	kKEY_Class2 = 0x50524F46;	//	'PROF'
	static const uint32_t	kKEY_Class3 = 0x4C46504C;	//	'LFPL'
	static const uint32_t	kKEY_Class4 = 0x4C465048;	//	'LFPH'
	static const uint32_t	kKEY_Class5 = 0x57465020;	//	'WFP '
	static const uint32_t	kKEY_Router = 0x524F5554;	//	'ROUT'
	static const uint32_t	kKEY_Cutter = 0x43555420;	//	'CUT '

#ifndef inch
#	define inch		* 18.0f
#endif
#ifndef	mm
#	define mm		* ( 18.0f / 25.4f )
#endif
#ifndef	unit2cm
#	define unit2cm	* ( 18.0f / 2.54f )
#endif

#define kMaxSheetLenght 20000 mm

	/*! \mainpage Aurelon Driver SDK

		\section intro_sec Introduction

		The Aurelon Driver SDK the possibility to create drivers for Aurelon RIP
		based product and/or use driver compatible with Aurelon RIP based products.

		There are 3 main modules in the API:
		- DLL exports : Main access routines to query the driver and/or to create a driver object (instance).
		- Driver information : Explanation of the data retrieved from a driver query.
		- Driver methods : Explanation of methods available in the driver object (to driver the device).
	*/

	/*!
		\defgroup	DLL_API		Driver DLL exported
	*/
	/*!
		\defgroup	DRV_Info	Driver information
	*/
	/*!
		\defgroup	DRV_Methods	Driver methods

		The methods described in the DriverMethods are to be called using one of the following calling orders.
		Driver rely on this.

		The calling order (Typical print only):

		- Initialize
		- JobStart
		- for( every page )
			- PageStart
			- RasterStart
			- for( every plane in every line )
				- ProcessRawBuffer
			- RasterEnd
			- PageEnd
		- JobEnd
		- Terminate

		The calling order (Typicall cutting only):

		- Initialize
		- JobStart
		- for( every page )
			- RegistrationMarks (if present)
			- PageStart
			- VectorStart
			- SetCut
			- Mix of calls below to build contours
				- Move
				- Line
				- Vertex
				- PenUp
				- PenDown
				- CurveStart
				- CurveEnd
			- VectorEnd
			- PageEnd
			- Transport
		- JobEnd
		- Terminate

		The calling order (Print and cut):

		- Initialize
		- JobStart
		- for( every page )
			- RegistrationMarks (if present)
			- PageStart
			- RasterStart
			- for( every plane in every line )
				- ProcessRawBuffer
			- RasterEnd
			- VectorStart
			- Mix of calls below to build contours
				- SetTool
				- Move
				- Line
				- Vertex
				- PenUp
				- PenDown
				- CurveStart
				- CurveEnd
			- VectorEnd
			- PageEnd
			- Transport
		- JobEnd
		- Terminate

	*/
	/*!
		Halftone spot shape
		\ingroup	DRV_Info

		The types of spots that are used by standard drivers. Additional
		spot types can be defined based on PostScript function.
	*/
	typedef enum SpotType
	{
		eAdobeRound = 0,	//!<	Euclidean round
		eAdobeEllipse = 1,	//!<	Euclidean ellipse
		eDiamond = 2,
		eSquare = 3,
		eRound = 4,
		eEllipseA = 5,
		eEllipseB = 6,
		eEllipseC = 7,
		eCosine = 8,
		eLine = 9,
		eCross = 10,
		eRhomboid = 11,
		eEllipse2 = 12,
		eLineX = 13,
		eLineY = 14,
		eDoubleDot = 15,
		eInvertedDoubleDot = 16,
		eSimpleDot = 17,
		eInvertedEllipseA = 18,
		eInvertedEllipseC = 19
	}	SpotType;

	/*!
		Halftone channel definition
		\ingroup	DRV_Info

		Definition of the parameters of a halftone channel, used when Screening is eAMScreen.
	 */
	class ChannelDefinition
	{
	public:
		char		name[64];	//!<	Name of the channel
		uint16_t	L,a,b;		//!<	16-bit encoded CIELab color that describes the color of the channel for preview purposes
		SpotType	shape;		//!<	Halftone spot shape
		float		angle;		//!<	Halftone angle
		float		frequency;	//!<	Halftone frequency in lines per inch
		bool		on;			//!<	Flag to indicate if this channel will be output or not
	};

	/*!
		Screening type requested from the RIP
		\ingroup	DRV_Info
	*/
	typedef enum ScreenType
	{
		eNotDefined = 0,		//!<	No screening type prefered
		eErrorDiffusion = 1,	//!<	Any stochastic (multi level) screening (Aurelon CED)
		eFMScreen = 2,			//!<	FM mask based screening
		eAMScreen = 3,			//!<	Halftones (Shape optimized)
		eTrueColor = 4,			//!<	No screening, 8-bit contone data
		eFloydSteinberg = 5,	//!<	Floyd-Steinberg error diffusion
		eChromaticED = 6		//!<	Chromatic correct error diffusion, only avalable for CMYK
	}	ScreenType;

	typedef enum USBFilterType
	{
		eByIEEEE = 0,
		eByModelName = 1
	}	USBFilterType;

	typedef enum EmbeddedProfileUse
	{
		eUseEmbeddedNone = 0,
		eUseEmbeddedProfileIntent = 1,
		eUseEmbeddedProfile = 2
	}	EmbeddedProfileUse;

	/*!
		Channel type of the ink
		\ingroup	DRV_Info
	*/
	typedef enum ChannelType
	{
		eProcess,				//!<	Process ink, use for profiling
		eSpot,					//!<	Spot color, do not profile
		eSpecial				//!<	White ink or varnish
	}	ChannelType;

	/*!
		Media size description
		\ingroup	DRV_Info

		Describes the media size and orientation
	*/
	class	TMediaSize
	{
	public:
		enum EMediaType{
			eLogical,		// Used in nesting and other logical operations.
			ePhysical		// Printed size.
		};

		char	name[32];		//!<	Name of the media size
		bool	transverse;		//!<	True when the media is fed rotated in the machine. The RIP must provide rotated data.
		bool	roll;			//!<	True when the media size is a roll, False when it is a sheet.
		float	width;			//!<	Physical width in AOI units
		float	length;			//!<	Physical length/height in AOI units
		float	left;			//!<	Left non-imageable margin
		float	right;			//!<	Right non-imageable margin
		float	top;			//!<	Top non-imageable margin
		float	bottom;			//!<	Bottom non-imageable margin

		inline float		GetLength				( EMediaType which = eLogical ) const;
		inline float		GetWidth				( EMediaType which = eLogical ) const;
		inline float		GetImageableLength		( EMediaType which = eLogical ) const;
		inline float		GetImageableWidth		( EMediaType which = eLogical ) const;

		inline float		GetLeftMargin			( EMediaType which = eLogical ) const;
		inline float		GetRightMargin			( EMediaType which = eLogical ) const;
		inline float		GetTopMargin			( EMediaType which = eLogical ) const;
		inline float		GetBottomMargin			( EMediaType which = eLogical ) const;
	};

	inline float TMediaSize::GetLength( EMediaType which ) const
	{
		return which == ePhysical && transverse ? width : length;
	}

	inline float TMediaSize::GetWidth( EMediaType which ) const
	{
		return which == ePhysical && transverse ? length : width;
	}

	inline float TMediaSize::GetImageableLength( EMediaType which ) const
	{
		return which == ePhysical && transverse ? width - left - right : length - top - bottom;
	}

	inline float TMediaSize::GetImageableWidth( EMediaType which ) const
	{
		return which == ePhysical && transverse ? length - top - bottom : width - left - right;
	}

	inline float TMediaSize::GetLeftMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? top : left;
	}

	inline float TMediaSize::GetRightMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? bottom : right;
	}

	inline float TMediaSize::GetTopMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? right : top;
	}

	inline float TMediaSize::GetBottomMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? left : bottom;
	}

	/*!
		Tray size description
		\ingroup	DRV_Info

		Describes the trays and the maximum size it can accept.
	*/
	class	TTray
	{
	public:
		char	name[32];		//!<	Name of the tray
		bool	roll;			//!<	True when the tray is a roll, False when it is sheet fed or a cassette.
		float	width;			//!<	Maximum physical width in AOI units of media it accepts.
		float	length;			//!<	Maximum physical length/height in AOI units of media it accepts.
		float	left;			//!<	Left non-imageable margin
		float	right;			//!<	Right non-imageable margin
		float	top;			//!<	Top non-imageable margin
		float	bottom;			//!<	Bottom non-imageable margin
	};

	/*!
		Ink definition
		\ingroup	DRV_Info

		Describes the ink of the channel and it properties.
	*/
	class	TInk
	{
	public:
		int8_t		color[3];			//!<	CIELab color to be used when generating previews without access to an ICC profile
		char		name[32];			//!<	Name of the ink. Prepended with the keyword "Light" or "Medium" to indicate the tone of the ink color.
		ScreenType	screening;			//!<	Prefered screening type for this ink.
		uint8_t		blueNoiseFactor;	//!<	Multiplication factor to apply on the blue noise of error diffusion or FM screening. Default 0. 0 is equal to a multiplication factor of 1.
		float		halftoneAngle;		//!<	Default halftone angles to be used when the screening is set to halftone.
		float		halftoneFrequency;	//!<	Default halftone frequency to be used when the screening is set to halftone.
		SpotType	halftoneShape;		//!<	Default halftone shape to be used when the screening is set to halftone.
		float		maxDotAmount;		//!<	Maximum amount of dots for all but large dot to be used for the screening. Range [0-1].
		ChannelType	type;				//!<	Channel type of this ink. The ink can either Process, Spot or Special.
		uint32_t	dotCount;			//!<	Number of levels for this channel (ink) including 0. Thus 1 drop size means 2 levels and 2 drop sizes means 3 levels.
		float		dotSizes[8];		//!<	Relative dot weights normalize to 1. A dotweight of 1 means full coverage. A dotweight of 0.5 means 50% coverage.
										//!<	Dotsizes larger of 1 are allowed and impose a screening based ink limit on that dot.
										//!<
										//!<	The number of dot sizes is dotCount-1
		float		picoliter[8];		//!<	Number of picoliter of the dot. The number of dots is dotCount-1. Only valid when DriverInfo::Caps::estimateInfo is set.
	};

	//
	//		DriverInfo
	//
	/*!
		Driver information
		\ingroup	DRV_Info

		The Info function supplies the RIP with all needed information about the device like:
		-	Descriptive information like DPI, sizes and inks.
		-	Media sizes and trays
		-	Communication methods available for this device
		-	XML that generates the user interface for the device specific settings

		\sa	Info
	*/
	class DriverInfo
	{
	public:
		uint32_t		version;
		//
		//	Identification
		//
		char			name[140];
		char			profileName[32];
		uint32_t		group;
		uint32_t		uniqueDriverID;
		//
		//	Capabilities
		//
		struct Caps
		{
			uint16_t	print : 1;
			uint16_t	cut : 1;
			uint16_t	flatbed : 1;
			uint16_t	highlevel : 1;
			uint16_t	additiveColors : 1;
			uint16_t	estimateInfo : 1;
			uint16_t	barcodeScanner : 1;
			uint16_t	mediaQuery : 1;
			uint16_t	deviceStatus : 1;
			uint16_t	jobStatus : 1;
			uint16_t	reversePrint : 1;
			uint16_t	hasConsole : 1;
			uint16_t	doubleSided : 1;
			uint16_t	buildinSpectrometer : 1;
			uint16_t	needPreviewAtJobStart : 1;
			uint16_t	needUnimagedArea : 1;
		}				caps;
		//
		//	Connection
		//
		struct Conns
		{
			uint16_t	parallel : 1;
			uint16_t	tcpip : 1;
			uint16_t	serial : 1;
			uint16_t	usb : 1;
			uint16_t	custom : 1;
			uint16_t	folder : 1;
			uint16_t	hplx : 1;
		}				conns;
		int16_t			serBaudRate;
		int16_t			serFlow;
		uint16_t		tcpPort;
#if ACPL_MAC
		char			usbFilter[32];
		USBFilterType	usbFilterType;
		uint8_t			usbClass;
#else
		char			parallelFilter[32];
		char			usbGUID[128];
#endif
		//
		//	Device capabilities
		//
		float			dpuOutputX;
		float			dpuOutputY;
		float			dpuRender;
		float			steps;
		float			maxWidth;
		float			maxHeight;
		MarkType		registrationType;
		float			sqUnitsPerHour;
		//
		//	Inks
		//
		int32_t			inkCount;
		TInk*			inks;
		char			inkType[64];
		//
		//	Media and trays
		//
		int32_t			mediaCount;
		int32_t			trayCount;
		TMediaSize*		media;
		TTray*			tray;
		//
		//	Custom settings script
		//
		char*			script;

						DriverInfo();
						~DriverInfo();

		DriverInfo&		operator>>(ACPL::Stream&);
		DriverInfo&		operator<<(ACPL::Stream&);
	private:
						DriverInfo( const DriverInfo& );
		DriverInfo&		operator=( const DriverInfo& );
	};

	/*!
		\var	char	DriverInfo::name[140]
				Describing the device name. E.g. “HP DesignJet 2000CP”. This name corresponds with
				the name returned by ListModels(). The generic printer driver adds a driver prefix:
				"Generic " + 128 chr for the OS printer name
	*/
	/*!
		\var	char	DriverInfo::profileName[32]
				Defines the group name of models that can share the same printer profiles. This because
				the printers are only size variations or OEM models with a different name.
	*/
	/*!
		\var	uint32_t	DriverInfo::group
				Defines the license class to which this driver belongs. The following license classes
				are defined:
				- 'DEST' : Category 1 drivers
				- 'PROF' : Category 2 drivers
				- 'LFPL' : Category 3 drivers
				- 'LFPH' : Category 4 drivers
				- 'WFP ' : Category 5 drivers
				- 'ROUT' : Routers / Engravers
				- 'CUT ' : Cutting plotters / Pen plotters
	*/
	/*!
		\var	uint32_t	DriverInfo::uniqueDriverID
				Unique driver ID used to facilitate licensing. This value correspond with the record
				number in the iOrder driver database and it used in the license product definition.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::print
				Flag indicate that this driver has printing capabilities.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::cut
				Flag indicate that this driver has (contour) cutting capabilities.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::flatbed
				Flag indicate that this device is a flatbed printer or plotter.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::highlevel
				Flag indicate that this driver supports high level vector graphics
				expressed in real coordinates and supporting bezier curves.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::additiveColors
				Flag indicate that this driver (using the current settings) has 
				additive colors instead of substractive inks.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::estimateInfo
				Flag indicate that this driver supplies ink estimation and time
				estimation information. See TInk::picoliter and DriverInfo::sqUnitsPerHour
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::barcodeScanner
				Flag indicate that this device has a build-in barcode scanner for
				job recognition.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::mediaQuery
				Flag indicate that this device supports measuring of loaded media
				and optionally provides information on media type.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::deviceStatus
				Flag indicate that this device supports feedback on device status.
				This might include loaded media size and type and ink levels.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::jobStatus
				Flag indicate that this device supports status information based on
				the job its GUID.
	*/
	/*!
		\var	uint16_t	DriverInfo::Caps::reversePrint
				Flag indicate that this this print mode requires reverse
				printing of calibration targets and output.
	 */
	/*!
		\var	uint16_t	DriverInfo::Caps::hasConsole
				Flag indicate that this device has a DFE and jobs will first appear there
				and require a printer operator to print the job.
	 */
	/*!
		\var	uint16_t	DriverInfo::Caps::doubleSided
				Flag indicate that this device has double sided printing capabilities with
				automatic loading the B-side after the A-side has been printed.
	 */
	/*!
		\var	uint16_t	DriverInfo::Caps::buildinSpectrometer
				Flag indicate that this device has a build-in spectrophotometer.
	 */
	/*!
		\var	uint16_t	DriverInfo::Caps::needPreviewAtJobStart
				Flag indicate that this driver requires a preview in the job XML before
				JobStart is called.
	 */
	/*!
		\var	uint16_t	DriverInfo::Caps::needUnimagedArea
				Flag indicate that this device no positioning capabilities and thus the x/y
				position in PageStart and RasterStart are ignored. This means that the RIP
				has to image also the non-imaged area in order to get correct positioning.
	 */
	/*!
		\var	uint16_t	DriverInfo::Conns::parallel
				Flag indicate that this driver supports Centronics connections.

				The deviceURL needs to conform to the format "par://[port name]".
	*/
	/*!
		\var	uint16_t	DriverInfo::Conns::tcpip
				Flag indicate that this driver supports TCP/IP connection. The default
				TCP port is defined by tcpPort.

				The deviceURL needs to conform to the format "tcp://[host]:[port]" or
				"raw://[host]:[port]" for RTelnet / Raw protocol. When no port is defined
				9100 is assumed.
				The format "lpr://[host]:[port]" invokes the LPR/LPD protocol and port 515
				is assumed when ommited.
				The format "http://[host]:[port]" invoked a Raw protocol on port 80 when ommitted.
				The [host] might be an IP addres in the form A:B:C:D or a DNS name.
		\sa		tcpPort
	*/
	/*!
		\var	uint16_t	DriverInfo::Conns::serial
				Flag indicate that this driver supports serial connection. The default
				serial settings are defined by serBaudRate and serFlow.
				device that matches the pattern.

			The deviceURL needs to conform to the format "ser://[port name]:[baud],[bits],[parity],[handshake]".
				For valid values of handshake see serFlow.
		\sa		serBaudRate, serFlow
	*/
	/*!
		\var	uint16_t	DriverInfo::Conns::usb
				Flag indicate that this driver supports USB connection. The default
				USB IEEE filter and class or GUID. On Windows the usbGUID is used to
				located the driver. On MacOS X the usbFilter is used to find an USB
				device that matches the pattern.

				The deviceURL needs to conform on Windows to the format "usb://USB[port index]".
				On MacOS X the format needs to be "usb://[port name]:[model],[class]:[filter]".
		\sa		usbFilter, usbFilterType, usbClass, usbGUID
	*/
	/*!
		\var	uint16_t	DriverInfo::Conns::custom
				Flag indicate that this driver has a non standard comunnication method.
				The driver exports its custom port-list method List() that provides a packed
				string with all available ports.

				The deviceURL can be of any format and has only meaning to the driver itself.
		\sa		List
	*/
	/*!
		\var	uint16_t	DriverInfo::Conns::folder
				Flag indicate that this driver delivers a file or folder as output.

				The deviceURL needs to conform to the format "file://[full path]".
	*/
	/*!
		\var	int16_t	DriverInfo::serBaudRate
				The default baudrate used by the device. Valid when the serial connection flag is set.
		\sa		Conns::serial, serFlow
	*/
	/*!
		\var	int16_t	DriverInfo::serFlow
				The default handshake used by the device. Valid when the serial connection flag is set.
				Valid values are:
				- 1 : No handshake
				- 2 : XOn / XOff
				- 3 : DTR / CTS
				- 4 : Both
		\sa		Conns::serial, serFlow
	*/
	/*!
		\var	int16_t	DriverInfo::tcpPort
				The default TCP port used by the device. Valid when the tcp connection flag is set.
				The port type also implictely defines the default protocol used:
				- 9100 : RTelnet / Raw
				- 515 : LPR/LPD
				- 80 : HTTP
		\sa		Conns::tcpip
	*/
	/*!
		\var	char	DriverInfo::usbFilter[32]
				MacOS X only : The USB IEEE or MfID filter for finding devices. Valid when the USB
				connection flag is set. The filter can be the exact MFG+MDL tag if the IEEE ID or
				it can be a partial name appended with an * wildcard. This filter will be used by
				the driver host to list suitable ports for this device.
		\sa		Conns::usb, usbFilterType, usbClass
	*/
	/*!
		\var	USBFilterType	DriverInfo::usbFilterType
				MacOS X only : The type of USB filter define by usbFilter. Valid when the USB
				connection flag is set. The filter can filter on the IEEE 1283 string or on the
				product field from the USBDescriptor.
		\sa		Conns::usb, usbFilter, usbClass
	*/
	/*!
		\var	uint8_t	DriverInfo::usbClass
				MacOS X only : The USB class of the device. Valid when the USB connection flag is set.
				If this value is set to 0 no filtering will be done on the USB device class.
		\sa		Conns::usb, usbFilter, usbFilterType
	*/
	/*!
		\var	char	DriverInfo::usbGUID[128]
				Windows only : The GUID of the driver that driver that drives this device in the OS.
				Valid when the USB connection flag is set.
		\sa		Conns::usb
	*/
	/*!
		\var	float	DriverInfo::dpuOutputX
				Horizontal output resolution of the image data the RIP must provide to the driver.
				The resolution is expressed in AOI Units which are 1/18th of an inch. This means
				that it is the DPI divided by 4.
		\sa		dpuOutputY, dpuRender
	*/
	/*!
		\var	float	DriverInfo::dpuOutputY
				Vertical output resolution of the image data the RIP must provide to the driver.
				The resolution is expressed in AOI Units which are 1/18th of an inch. This means
				that it is the DPI divided by 4.
		\sa		dpuOutputX, dpuRender
	*/
	/*!
		\var	float	DriverInfo::dpuRender
				Proposed render resolution of the contone data before screening. During the screening
				the data needs to be upsampled to dpuOutputX x dpuOutputY.
				The resolution is expressed in AOI Units which are 1/18th of an inch. This means
				that it is the DPI divided by 4.
		\sa		dpuOutputX, dpuOutputY
	*/
	/*!
		\var	float	DriverInfo::steps
				Number of steps per AOI Unit (1/18th of an inch) used by the plotter. All vector
				calls are to be provided in this resolution except for plotter that have the highlevel
				flag set. Valid only when the cut flag is set.
		\sa		Caps::highlevel
	*/
	/*!
		\var	float	DriverInfo::maxWidth
				Maximum imageable width of the device expressed in AOI units.
		\sa		maxHeight
	*/
	/*!
		\var	float	DriverInfo::maxHeight
				Maximum imageable height/length of the device expressed in AOI units.
		\sa		maxWidth
	*/
	/*!
		\var	float	DriverInfo::sqUnitsPerHour
				Amount of square units this devices produces using the given settings. Used for
				calculating production estimated. Valid only when the estimateInfo flag is set.
		\sa		Caps::estimateInfo
	*/
	/*!
		\var	MarkType	DriverInfo::registrationType
				Flags indicating which contour cutting registration marks are supported by this
				device.
	*/
	/*!
		\var	int32_t	DriverInfo::inkCount
				Number of ink channels that are available using the given settings. The
				ink definition array DriverInfo::inks is filled with this number of entries.
		\sa		inks
	*/
	/*!
		\var	TInk*	DriverInfo::inks
				Array of ink definitions. Every entry describes the ink of the corresponding
				head.
		\sa		inkCount
	*/
	/*!
		\var	char	DriverInfo::inks[64]
				Type of the ink used, like Solvent, UV-Curing.
	*/
	/*!
		\var	int32_t	DriverInfo::mediaCount
				Number of media sizes that are available using the given settings. The
				media definition array DriverInfo::media is filled with this number of entries.
		\sa		media
	*/
	/*!
		\var	TMediaSize*	DriverInfo::media
				Array of media definitions.
		\sa		mediaCount
	*/
	/*!
		\var	int32_t	DriverInfo::trayCount
				Number of trays that are available using the given settings. The
				tray definition array DriverInfo::tray is filled with this number of entries.
		\sa		tray
	*/
	/*!
		\var	TTray*	DriverInfo::tray
				Array of tray definitions.
		\sa		trayCount
	*/
	/*!
		\var	char*	DriverInfo::script
				XML based UI definition that describes the driver settings. An UI can be generated
				using this XML and results in the settings XML passed to the Info() and Initialize()
				calls. For more information about the XML based UI see http://wiki.aurelon.com/twiki/bin/view/Main/DynamicUI
	*/

	/*!
		\class		ModelID
		\ingroup	DRV_Info
	*/
	typedef struct ModelID
	{
		const char*	name;
		const char*	profileName;
		uint32_t	group;
		uint32_t	uniqueDriverID;
		float		maxWidth;
		float		maxHeight;
	}	ModelID;

	extern const ModelID kDriverID[];
	/*!
		\var	const char*	ModelID::name
				Name of the device model
	*/
	/*!
		\var	const char*	ModelID::profileName
				Defines the group name of models that can share the same printer profiles. This because
				the printers are only size variations or OEM models with a different name.
	*/
	/*!
		\var	uint32_t	ModelID::group
				Defines the license class to which this driver belongs. The following license classes
				are defined:
				- 'DEST' : Category 1 drivers
				- 'PROF' : Category 2 drivers
				- 'LFPL' : Category 3 drivers
				- 'LFPH' : Category 4 drivers
				- 'WFP ' : Category 5 drivers
				- 'ROUT' : Routers / Engravers
				- 'CUT ' : Cutting plotters / Pen plotters
	*/
	/*!
		\var	uint32_t	ModelID::uniqueDriverID
				Unique driver ID used to facilitate licensing a RIP with only 1 driver (model or
				group). When not used this value can be 0.
	*/
	/*!
		\var	float	ModelID::maxWidth
				Maximum imageable width of the device expressed in AOI units.
		\sa		maxHeight
	*/
	/*!
		\var	float	ModelID::maxHeight
				Maximum imageable height/length of the device expressed in AOI units.
		\sa		maxWidth
	*/

	/*!
		Constructor of the DriverInfo class creates an empty info structure
		that can be used for the Info call.
	*/
	inline DriverInfo::DriverInfo() :
		version( SDK_VERSION ),
		group( ADA::kKEY_Class1 ),
		uniqueDriverID( 0 ),
		inkCount( 0 ),
		inks( NULL ), 
		mediaCount( 0 ),
		trayCount( 0 ),
		media( NULL ),
		tray( NULL ),
		script( NULL )
	{
	}

	inline DriverInfo::~DriverInfo()
	{
		delete[] inks;
		delete[] media;
		delete[] tray;
		delete[] script;
	}

	/*!
		State returned from a Direct Access Method
		\ingroup	DRV_Methods
	*/
	typedef enum DirectState
	{
		ePlotterOK = 0,
		ePlotterBusy,
		ePlotterFunctionNotUsed,
		ePlotterError
	}	DirectState;

	class PageRect
	{
	public:
		float	left;
		float	top;
		float	right;
		float	bottom;

		inline	PageRect()
		{
			left = top = 3.402823466e+38F;
			right = bottom = -3.402823466e+38F;
		}

		inline	PageRect( float x1, float y1, float x2, float y2 )
		{
			left = x1;
			top = y1;
			right = x2;
			bottom = y2;
		}

		inline void	Union( const PageRect& inBounds )
		{
			if( inBounds.left < left )
				left = inBounds.left;
			if( inBounds.top < top )
				top = inBounds.top;
			if( inBounds.right > right )
				right = inBounds.right;
			if( inBounds.bottom > bottom )
				bottom = inBounds.bottom;
		}

		inline float Width() const
		{
			return right - left;
		}

		inline float Height() const
		{
			return bottom - top;
		}
	};

	class MarkerPosition
	{
	public:
		float	x;
		float	y;

		inline	MarkerPosition() : x(0), y(0) {}
		inline	MarkerPosition( float inX, float inY ) : x(inX), y(inY) {}
	};

	/*!
		Driver methods
		\ingroup	DRV_Methods

		The driver method exported by the driver. The entries might be NULL and are thus
		not supported. This means the call can ommitted and the next call in the calling order
		should be executed (see below).
	*/
	typedef struct DriverMethods
	{
		/*!
			Starts a session. Initialized the driver with the settings to be used. Many drivers will open
			the communication channel during this call.
			This the only method that will return an error. Other methods will throw a C++ exception of
			the type ExceptionCode.

			\param[in]	obj			Opaque driver reference
			\param[in]	jobDict		XML containing the job (ticket). This maybe be NULL. The driver can
									use information from the job and may write additional private or public data
									in the job.
			\param[in]	dpuX		Horizontal resolution of the halftone data that will be provided in AOI units.
			\param[in]	dpuY		Vertical resolution of the halftone data that will be provided in AOI units.
			\param[in]	settings	Settings XML containing the settings extracted from the XML based UI.
			\return					NoError when successfull. The error when the call failed. When the method
									ErrorToName is available the error must be translated using that method to
									get a user readeable error message.
		*/
		ExceptionCode	(*Initialize)( void* obj, ACPL::XML* jobDict, float dpuX, float dpuY, const char* settings );
		/*!
			Terminates/closes the session of the driver.

			\param[in]	obj			Opaque driver reference
			\param[in]	abort		True when the current connection and output needs to be aborted. False when
									a gracefull close and exit is requested.
		*/
		void			(*Terminate)( void* obj, bool abort );
		/*!
			Converts an error code to a readeable description. 

			\param[in]	err			The error to translate
			\param[out]	errorName	String of 256 characters long that will receive the description.
			\return					True when the error is known and a description is generated.
		*/
		bool			(*ErrorToName)( ExceptionCode err, char* errorName );
		/*!
			Adds a preview to the page that has just been closed. The BMP is expected to be in 24 bits RGB.

			\param[in]	obj			Opaque driver reference
			\param[in]	pathToBMP	Full path to the preview BMP that will be used as source of the preview.
		*/
		void			(*AddPreview)( void* obj, const ACPL::UString& pathToBMP );
		/*!
			Performs a direct home of the head without opening a job and page. The commands are directly
			flushed and thus performed. These commands are used to implement remote control of cutting
			heads for example.

			\param[in]	obj			Opaque driver reference
		*/
		DirectState		(*DirectHome)( void* obj );
		/*!
			Performs a direct move of the head without opening a job and page. The commands are directly
			flushed and thus performed. These commands are used to implement remote control of cutting
			heads for example.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in AOI units 
			\param[in]	y			Y coordinate in AOI units
		*/
		DirectState		(*DirectMove)( void* obj, float x, float y );
		/*!
			Sets the new home position of the head to the current position. The commands are directly
			flushed and thus performed. These commands are used to implement remote control of cutting
			heads for example.

			\param[in]	obj			Opaque driver reference
		*/
		DirectState		(*DirectSetHome)( void* obj );
		/*!
			Queries the size of the currently loaded media. The commands are directly flushed and
			thus performed. There is no need to open a job or a page. If the device uses a different 
			protocol or returns more than only the size then use the exported DLL method PrinterStatus.

			\param[in]	obj			Opaque driver reference
			\param[out]	outSize		Rectangle that receives the media size.
		*/
		DirectState		(*DirectSizeQuery)( void* obj, PageRect& outSize );
		/*!
			Queries the current position of the head. The commands are directly flushed and
			thus performed. There is no need to open a job or a page.

			\param[in]	obj			Opaque driver reference
			\param[out]	outCoord	Coordinate that receives the head position.
		*/
		DirectState		(*DirectPosQuery)( void* obj, float& outCoordX, float& outCoordY );
		void			(*ObsoleteMethod)();
		DirectState		(*DirectBarcodeQuery)( void*, ACPL::String& );
		/*!
			Start a job on the device. A job can contain more than 1 pages. This call must be paired with
			JobEnd after all pages are send.

			\param[in]	obj				Opaque driver reference
			\param[in]	jobName			Name of the job
			\param[in]	copies			Number of copies. When the device has internal copy facilities the copies should
										be set to 0 before returning.
			\param[in]	mediaSize		Media size to be used. The media size must be respecting the margins and sizes set by the tray.
			\param[in]	tray			Tray to be used. This needs to be a copy of a TTray entry returned bij DriverInfo.
			\param[in]	hasImageData	Flag that indicates if the job contains image data (a print)
			\param[in]	hasVectorData	Flag that indicates if the job contains vector data (contour cut)
			\sa			JobEnd
		*/
		void			(*JobStart)( void* obj, const ACPL::UString& jobName, int32_t& copies, const TMediaSize& mediaSize, const TTray& tray, bool hasImageData, bool hasVectorData );
		/*!
			Ends the job on the device. Can only be called after all pages are send and a job is open.

			\param[in]	obj			Opaque driver reference
			\sa			JobStart
		*/
		void			(*JobEnd)( void* obj );
		/*!
			Passed the list of registration mark positions if present.

			\param[in]	obj			Opaque driver reference
			\param[in]	nrOfPoints	Number of registration points. The array points is expected to contain nrOfPoints values
			\param[in]	points		Array of MarkerPositions in AOI units, containing nrOfPoints elements.
		*/
		void			(*PageRegistrationMarks)( void* obj, uint32_t nrOfPoints, const MarkerPosition * const points );
		/*!
			Start a page on the device. This call must be paired with PageEnd. A call to JobStart must
			preceed this call.

			\param[in]	obj			Opaque driver reference
			\param[in]	barcode		Barcode identifying this page. When no barcode is provided then this is empty. The barcode is
									identical for the printed page and the cut page that is associated.
			\param[in]	mediaSize	Media size to be used. The media size must be respecting the margins and sizes set by the tray.
			\param[in]	tray		Tray to be used. This needs to be a copy of a TTray entry returned bij DriverInfo.
			\param[in]	imagedRect	Area of the page filled by the data to come expressed on AOI units where the
									origin is the top left corner of the imageable area.
			\sa			PageEnd
		*/
		void			(*PageStart)( void* obj, const ACPL::String& barcode, const TMediaSize& mediaSize, const TTray& tray, const PageRect& imagedRect );
		/*!
			Ends the page, a page must be open.

			\param[in]	obj			Opaque driver reference
			\param[in]	endOffset	Location where the (cutting) head must go when the page is finished:
									- 1 : Move the head to the origin of the impression
									- 2 : Move to the bottom left of the impression
									- 3 : Move to the right top of the impression
									- 4 : Transport media (flatbed) by the length of the impression
									- 5 : Same as 4 and cut the media
									- 6 : Manually position the head (device goes offline)
			\sa			PageStart
		*/
		bool			(*PageEnd)( void* obj, int16_t endOffset );
		/*!
			Invokes the roll cutter of the printer.

			\param[in]	obj			Opaque driver reference
		*/
		void			(*PanelCut)( void* obj );
		//
		//	Rasterizing
		//
		/*!
			Starts a print on the device. Only one call to RasterStart is allowed per page.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			Left offset in pixels relative to the imageble area of the printer.
			\param[in]	y			Top offset in pixels relative to the imageble area of the printer.
			\param[in]	width		Width of the print in pixels.
			\param[in]	height		Height of the print in pixels.
			\sa			RasterEnd, ProcessRawBuffer
		*/
		void			(*RasterStart)( void* obj, int32_t x, int32_t y, int32_t width, int32_t height );
		/*!
			Ends a print on the device.

			\param[in]	obj			Opaque driver reference
			\sa			RasterStart, ProcessRawBuffer
		*/
		void			(*RasterEnd)( void* obj );
		/*!
			Processes 1 plane of a line of the image. The driver expect all planes of a line even if they
			are empty, so for a CMYK print 4 calls to this method are required to process 1 line.
			The provided halftone data must be truncated so that no white exists on the right side of the
			plane. When this is not done the printing efficiency might go down. It is allow to pass a NULL
			pointer when the data length is 0.

			\param[in]	obj			Opaque driver reference
			\param[in]	planeData	Pointer to the halftoned data of 1 plane.
			\param[in]	dataBytes	Number of bytes of halftone data.
			\param[in]	planeIdx	Channel number to which this plane data refers
			\sa			RasterStart, RasterEnd
		*/
		void			(*ProcessRawBuffer)( void* obj, const uint8_t* planeData, int32_t dataBytes, uint8_t planeIdx );
		//
		//	Cutting
		//
		/*!
			Starts a plot on the device. A plot can be drawing, cutting, routing or engraving. Only one
			call to VectorStart is allowed per page.

			\param[in]	obj			Opaque driver reference
			\sa			VectorEnd
		*/
		void			(*VectorStart)( void* obj );
		/*!
			Ends a plot on the device.

			\param[in]	obj			Opaque driver reference
			\sa			VectorStart
		*/
		void			(*VectorEnd)( void* obj );
		/*!
			Moves the current point of the plot to the given coordinate.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in DriverInfo::steps 
			\param[in]	y			Y coordinate in DriverInfo::steps
		*/
		void			(*Move)( void* obj, int32_t x, int32_t y );
		/*!
			Draws a line from the current point of the plot to the given coordinate and makes it the
			current point.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in DriverInfo::steps 
			\param[in]	y			Y coordinate in DriverInfo::steps
		*/
		void			(*Line)( void* obj, int32_t x, int32_t y );
		/*!
			Draws a line from the current point of the plot to the given coordinate and makes it the
			current point. This line segment is originated from a curve (Bezier, B-spline or other)

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in DriverInfo::steps 
			\param[in]	y			Y coordinate in DriverInfo::steps
			\param[in]	last		True when this is the last line segment of the curve.
		*/
		void			(*Vertex)( void* obj, int32_t x, int32_t y, bool last );
		/*!
			Instructs the plotter to raise the tool/pen.

			\param[in]	obj			Opaque driver reference
			\sa			CurveStart, CurveEnd
		*/
		void			(*PenUp)( void* obj );
		/*!
			Instructs the plotter to lower the tool/pen.

			\param[in]	obj			Opaque driver reference
		*/
		void			(*PenDown)( void* obj );
		/*!
			Marks the start of a series of line segments forming a curve.

			\param[in]	obj			Opaque driver reference
			\sa			Vertex, CurveEnd
		*/
		void			(*CurveStart)( void* obj );
		/*!
			Marks the end of a series of line segments forming a curve.

			\param[in]	obj			Opaque driver reference
			\sa			Vertex, CurveStart
		*/
		void			(*CurveEnd)( void* obj );
		/*!
			Instructs the plotter to select the specified tool as defined by
			the passed driver setting.

			\param[in]	obj			Opaque driver reference
			\param[in]	settings	Settings XML containing the settings extracted from the XML based UI
									for the tool to be selected.
		*/
		void			(*SetTool)( void* obj, const char* settings );
		//
		//	High level
		//
		/*!
			Moves the current point of the plot to the given coordinate.

			Only valid when DriverInfo::Caps::highlevel flag is set

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in AOI units 
			\param[in]	y			Y coordinate in AOI units
			\param[in]	closed		True when the path that is about to start is closed, false if it is open
		*/
		void			(*HLMove)( void* obj, float x, float y, bool closed );
		/*!
			Draws a line from the current point of the plot to the given coordinate and makes it the
			current point.

			Only valid when DriverInfo::Caps::highlevel flag is set

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in AOI units 
			\param[in]	y			Y coordinate in AOI units
		*/
		void			(*HLLine)( void* obj, float x, float y );
		/*!
			Draws a bezier from the current point of the plot and makes x3,y3 the
			current point.

			Only valid when DriverInfo::Caps::highlevel flag is set

			\param	obj			Opaque driver reference
			\param	x1			X coordinate in AOI units of the first Bezier handle
			\param	y1			Y coordinate in AOI units of the first Bezier handle
			\param	x2			X coordinate in AOI units of the second Bezier handle
			\param	y2			Y coordinate in AOI units of the second Bezier handle
			\param	x3			X coordinate in AOI units of the Bezier end point
			\param	y3			Y coordinate in AOI units of the Bezier end point
		*/
		void			(*HLBezier)( void* obj, float x1, float y1, float x2, float y2, float x3, float y3 );
		void			(*CustomAction)( void* obj, int a, const char* b );
	}	DriverMethods;

	static const ExceptionCode	kErrorLookup = 0x4C4B4552;	//	'LKER'
	static const ExceptionCode	kErrorRefused = 0x52454655;	//	'REFU'

	class	Instance;
		
	/*!
		Instance methods
		\ingroup	INST_Methods

		The instance method exported by the driver. The entries might be NULL and are thus
		not supported. This means the call can ommitted and the next call in the calling order
		should be executed (see below).
	*/
	typedef struct InstanceMethods
	{
		/*!
			The Info function supplies the RIP with all needed information about the device like:
			-	Descriptive information like DPI, sizes and inks.
			-	Media sizes and trays
			-	Communication methods available for this device
			-	An XML that generates the user interface for the device specific settings

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	info		Initialized info class that will receive the driver
			\param	settings	XML containing the driver settings. When an empty string is supplied the
								default settings are used to fill the DriverInfo
		*/
		ExceptionCode	(*Info)( Instance* driverObj, DriverInfo* info, const char* settings );
		/*!
			\ingroup	DLL_API

			The DisposeInfo function deletes the content of the DriverInfo object allocated by the Info-call after which the
			calling host can dispose the DriverInfo object.

			\param	info		DriverInfo object to dispose
			\sa	Info, DriverInfo
		*/
		void			(*DisposeInfo)( DriverInfo* info );
		/*!
			This function is called to create an instance of the driver for the given model. This function is
			not called when information about the driver is queried (like media sizes) only when a job has to
			be processed.

			The CreateSession function supplies the RIP with a reference to an object. This reference is opaque to the RIP.
			This means that the RIP does not know the definition of the object. The RIP uses this reference only as
			a reference of the driver and has only a meaning to the driver it self (e.g. it might be the address of
			a C++ object or a structure in memory to hold information needed during processing)
			The Create function also supplies a pointer to a table of routine pointers. Those routines are called
			during the processing of the job. Every routine has as first parameter the aforementioned reference.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	methods		Pointer that receives the pointer to the DriverMethods table of the driver.
			\param	deviceURL	URL that describes the connection to be used. When NULL is specified then the default URI
								will be used. Only used for folder based devices.
			\return				Opaque driver reference to be used with all driver methods and Dispose.
			\sa	DisposeSession, DriverMethods
		*/
		void*			(*CreateSession)( Instance* driverObj, const DriverMethods** methods, const char* deviceURL );
		/*!
			This function is called when the RIP is done using the instance of the driver after processing a job.

			\param	obj			Opaque driver reference returned by Create().
			\sa	CreateSession
		*/
		void			(*DisposeSession)( Instance* driverObj, void* obj );
		/*!
			PrinterStatus returns information about the printer ink levels and loaded media if the routine is available
			and the printer supports this information.
			The data is returned in XML form containing all the information available, if the printer is not supporting a
			certain type of feedback the XML will not contain the keywords.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	obj			Opaque driver reference returned by Create().
			\param statusDict	XML DOM containing the status information on return, documentation of the keyword can be found in the xds
		 */
		void			(*PrinterStatus)( Instance* driverObj, ACPL::XML* statusDict );
		/*!
			JobStatus returns information about a job if the routine is available and the printer supports this information.
			The data is returned in XML form containing all the information available, if the printer is not supporting a
			certain type of feedback the XML will not contain the keywords.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	jobGUID     GUID of the job send to the printer.
			\param	statusDict	XML DOM containing the job information on return, documentation of the keyword can be found in the xds
		 */
		void			(*JobStatus)( Instance* driverObj, const char* jobGUID, ACPL::XML* statusDict );
		/*!
			GetPrivateCacheData retrieves information stored by the driver in its private cache to allow it to be retained. This
			private data can be cached information from the actual printer to speed-up the queries or to build the proper content
			of the DriverInfo structure (like medias installed at the printer).

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	outStream   Empty stream that receives that private cache data that is used to initialize this printer instance.
			\sa		SetPrivateCacheData
		 */
		void			(*GetPrivateCacheData)( Instance* driverObj, ACPL::Stream& outStream );
		/*!
			SetPrivateCacheData stored retained private information used by the driver to overwrite its private cache. This
			private data can be cached information from the actual printer to speed-up the queries or to build the proper content
			of the DriverInfo structure (like medias installed at the printer).

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	inStream	Stream with its marker set to the start of the stream and contain the private cache data that is
								used to initialize this printer instance. The data is obtained earlier using GetPrivateCacheData.
			\sa		GetPrivateCacheData
		 */
		void			(*SetPrivateCacheData)( Instance* driverObj, ACPL::Stream& inStream );
		/*!
			SetHostInterface provides a callback mechanism for the driver to call specific functions in the hosting application.
			The call is generalized in an XML query and XML response, the provided functionality through this API is host specific
			and can't be trusted to be available in every host.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	hostName	Name to identify the host, this can be used by the driver to determine which API is available.
			\param	hostObj		Reference to the host object, this needs to be provided by every call to the host-callback routine.
			\param	hostCB		Routine pointer providing query-XML as input and response-XML as output XML. Return value true means
								success and false that the API is not available or the call failed.
		 */
		void			(*SetHostInterface)( Instance* driverObj, const char* hostName, void* hostObj, bool (*hostCB)( void* hostObj, ACPL::XML& query, ACPL::XML& response ) );
		/*!
			Query used to by pass the workflow component. Used by the Calibrator/Profiler to access build-in
			photospectrometers. The implementation is device dependend, the API is generalization of
			the process.

			\param		driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param[in]	query		Query to the device.
			\param[out]	response	Response from the device.
		*/
		bool			(*PrinterQuery)( Instance* driverObj, ACPL::XML& query, ACPL::XML& response );
	}	InstanceMethods;

	/*!
		\ingroup	DLL_API

		The ListModels function supplies the Aurelon RIP with al list of all models supported by this driver.

		\return	Static list of models. The last entry in the model list is a NULL pointer to mark the end of the list.
	*/
	DLLAPI	const ModelID*	CALLBACK	ListModels();
	/*!
		\ingroup	DLL_API

		PrinterInitialize is to be called once at the start-up of the host(RIP) when the routine is available.
		The routine initializes the printer once, like updating cache files, downloading media definitions, etc.

		\param	modelName	Name of the printer/plotter model as supplied by ListModels().
		\param	deviceURL	URL that describes the connection to be used. Meaning full only to the driver.
		\return				Reference to a printer object, this is passed to Create and PrinterTerminate objects.
	*/
	DLLAPI	Instance*		CALLBACK	PrinterInitialize( const char* modelName, const char* deviceURL, const InstanceMethods** methods );
	/*!
		\ingroup	DLL_API

		PrinterInitialize is to be called when the host(RIP) closes if the routine is available.

		\param	driverObject	Reference to the printer object obtained from PrinterIntitialize.
	*/
	DLLAPI	void			CALLBACK	PrinterTerminate( Instance* driverObj );

	DLLAPI	void	CALLBACK	ListChannels( const char* modelName, char* list );
	DLLAPI	void	CALLBACK	DriverIcon( const char* modelName, uint8_t* iconData, int32_t& iconDataSize);
}}

