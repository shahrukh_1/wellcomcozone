/*
 * AOI_Ellipse
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Vector.h"

class AOIAPI AOI_Object_Ellipse : public AOI_Object_Vector
{
public:
	const static uint32_t	Type = 'GELL';

	void		GetEllipse( AOI_Rectangle& outRect ) const;
	void		GetEllipse( AOI_Rectangle& outRect, float& startAngle, float& sweep, bool& wedge ) const;
	float		GetStartAngle() const;
	float		GetSweepAngle() const;
	void		SetSweepAngle( float ) const;
	float		GetXRadius() const;
	float		GetYRadius() const;
	bool		GetWedge() const;
	AOI_Point	GetStartPoint() const;
	AOI_Point	GetEndPoint() const;
	AOI_Mapping	GetMapping() const;
};
typedef AOI_Object_Ellipse*	AOI_Object_EllipsePtr;
