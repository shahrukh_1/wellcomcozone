/*
 * AOI_TIFFRenderer
 * Copyright (c) 2011 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Renderer.h"
#include <ACPL/MultiThreading.h>
#include "../GTypes.h"
#include "../GBitmap.h"

namespace aur { namespace PDF {
	class	CMM;
	class	ColorSpaceObject;
}}
class	CProgressionBar;

class AOI_TIFFRenderer
{
	class Unit
	{
	public:
		uint8_t*	mInputData;
		uint8_t*	mOutputData;
		bool		mIsLast;
		Unit*		mNext;
	};
	class UnitQueue
	{
		aur::ACPL::CriticalSection	mSemaphore;
		Unit*		mUnits;
	public:
					UnitQueue();
					~UnitQueue();
		void		AddBuffer( Unit* );
		Unit*		AquireBuffer();
		void		DestroyBuffers();
	};
	typedef struct	ImageDefinition
	{
		uint32_t	channelCount;
		uint32_t	rowBytes;
		uint32_t	width;
		uint32_t	height;
	}	ImageDefinition;

	aur::PDF::Document*		mDocument;
	aur::ACPL::FileSpec*	mImageSpec;
	aur::PDF::Renderer*		mColorConvertor;
	aur::PDF::Mapping		mRenderMapping;
	aur::PDF::Mapping		mImageMapping;
	ImageDefinition			mInputOriginal;
	ImageDefinition			mOutputOriginal;
	ImageDefinition			mInputClipped;
	ImageDefinition			mOutputClipped;
	uint16_t*				mLABLine;
	uint8_t*				mOutputBand;
	uint8_t					mWhite;
	float					mPageWidth;
	float					mPageHeight;
	volatile bool			mContinue;
	bool					mImageExhausted;

	uint32_t				mY_Input;
	uint32_t				mY_Input_Start;
	uint32_t				mInput_X_OffsetBytes;
	double					mYScale;
	float					mPreviousMapY;
	double					mY_Input_Threshold;
	bool					mResetInput;
	CProgressionBar*		mProgression;
	UnitQueue				mReaderQueue;		//	Units available to image readering process
	ACPL_THREAD				mReaderThreadH;
	volatile bool			mReaderEnded;
	static	ACPL_RES		ReaderCreate( void* );
	void					Reader();
	static ExceptionCode	ReaderPlugSetup( void* ref, const aur::PDF::BitmapT& inBitmap );
	static ExceptionCode	ReaderPlugSaveLine( void* ref, const uint8_t* inImageLine );

	UnitQueue				mCMMQueue;			//	Units available to CMM & Up/down sample process
	ACPL_THREAD				mCMMThreadH;
	volatile bool			mCMMEnded;
	ColorConvertCB			mColorConvertCallBack;
	void*					mColorConvertRef;
	aur::PDF::CMM*			mInputCMM;
	aur::PDF::CMM*			mOutputCMM;
	aur::PDF::ColorSpaceObject*	mInputCS;
	aur::PDF::ColorSpaceObject*	mOutputCS;
	static	ACPL_RES		CMMCreate( void* );
	void					ScaleCMM();
	void					SimpleConvert( uint32_t width, const uint8_t* src, uint8_t* dest );
	void					DownscaleFree( uint32_t channels, const uint8_t* inImage, int32_t inWidth, uint8_t* outImage, int32_t outWidth );
	void					UpscaleFree( uint32_t channels, const uint8_t* inImage, int32_t inWidth, uint8_t* outImage, int32_t outWidth );

	uint32_t				mY_Output_Start;
	uint32_t				mOutput_X_OffsetBytes;
	double					mY_Output_Threshold;
	UnitQueue				mWriterQueue;		//	Units available for writing in band
public:
							AOI_TIFFRenderer( AOI_Document& doc );
							~AOI_TIFFRenderer();
	aur::ExceptionCode		Setup( const aur::ACPL::FileSpec&, aur::ACPL::Array<char*>*, void* banddata, int32_t w, int32_t h, bool alpha );
	void					SetColorConvertCallBack( ColorConvertCB cb, void* ref );

	void					SetMapping( const AOI_Mapping& docToBandMap );
	void					Render();
private:
	void					Cleanup();
};
