/*
 * AOI_Dimension
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Object.h"

class AOIAPI AOI_Object_Dimension : public AOI_Object
{
public:
	const static uint32_t	Type = 'DIMS';
};
typedef AOI_Object_Dimension*	AOI_Object_DimensionPtr;
