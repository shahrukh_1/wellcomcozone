/*
 * AOI_PlaceHolder
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Symbol.h"

class AOIAPI AOI_Object_PlaceHolder : public AOI_Object_Symbol
{
public:
	const static uint32_t	Type = 'GPLH';
	typedef enum ResizeType
	{
		eOriginal = 0,
		eSmallest,
		eAvarage,
		eBiggest,
		eFit,
		eInvalidSize,
		eMaxEnumResizeType = 0xFFFFFFFFL
	}	ResizeType;
	typedef enum PHMarkerType
	{
		ePHNone = 0,
		ePHStandard,
		ePHCrop,
		ePHBorder,
		ePHSubscript,
		ePHInvalidMark,
		eMaxEnumMarkerType = 0xFFFFFFFFL
	}	PHMarkerType;
	typedef enum AlignType
	{
		eLeftTop = 0,
		eLeftCenter,
		eLeftBottom,
		eCenterTop,
		eCenterCenter,
		eCenterBottom,
		eRightTop,
		eRightCenter,
		eRightBottom,
		eInvalidAlign,
		eMaxEnumAlignType = 0xFFFFFFFFL
	}	AlignType;
};
typedef AOI_Object_PlaceHolder*	AOI_Object_PlaceHolderPtr;
