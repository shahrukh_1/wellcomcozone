/*
 * AOI_Bitmap
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_ColorSpace.h"

class	AOI_Bitmap
{
public:
	int32_t			width;
	int32_t			height;
	int32_t			rowBytes;
	int32_t			pixelSize;
	AOI_ColorSpace*	space;
};
typedef AOI_Bitmap*	AOI_BitmapPtr;

class AOIAPI AOI_Object_Bitmap
{
public:
	typedef enum MaskType
	{
		eNone = 0,
		eRanges,
		eImage,
		eMaxEnumMaskType = 0xFFFFFFFFL
	}	MaskType;

	void 					GetBitmapData( AOI_Bitmap& ) const;
	void					GetMaskRange( uint16_t[ MAX_CHANNELS ], uint16_t[ MAX_CHANNELS ] ) const;
	AOI_Object_Bitmap*		GetMaskImage() const;
	MaskType				GetMaskType() const;
	uint16_t				GetMaskMin( uint16_t ) const;
	uint16_t				GetMaskMax( uint16_t ) const;
	void					RemoveMask();
	AOI_ColorSpacePtr		GetColorSpace() const;
	void					OpenImageData() const;
	void					CloseImageData() const;
	bool					GetImageData( uint32_t startLine, uint32_t count, void* outData ) const;
	float					GetPixel( int32_t, int32_t, AOI_Color& ) const;
};
typedef AOI_Object_Bitmap*	AOI_Object_BitmapPtr;
