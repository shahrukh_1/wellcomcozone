/*
 * AOI_Path
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Vector.h"

class AOIAPI AOI_Object_Path : public AOI_Object_Vector
{
public:
	const static uint32_t	Type = 'GPAD';
	const	AOI_ContourPtr	GetGeometry() const;
};
typedef AOI_Object_Path*	AOI_Object_PathPtr;
