/*
 * AOI_Symbol
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Object.h"

class AOIAPI AOI_Object_Symbol : public AOI_Object
{
public:
	const static uint32_t	Type = 'SYMB';

	AOI_ObjectPtr		GetObj() const;
	AOI_Mapping			GetMapping() const;
	AOI_Style_ColorPtr	GetColor() const;
};
typedef AOI_Object_Symbol*	AOI_Object_SymbolPtr;

class AOIAPI AOI_Object_MultiSymbol : public AOI_Object_Symbol
{
public:
	const static uint32_t	Type = 'MSYM';

	uint32_t			GetPositions( AOI_Point* );
};
typedef AOI_Object_MultiSymbol*	AOI_Object_MultiSymbolPtr;
