/*
 * AOI_Contour
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Types.h"

class AOIAPI AOI_Contour
{
public:
	static AOI_Contour*		New();

	int32_t 				CountElements( int32_t inContour = 0 ) const;
	AOI_ContourElementPtr	GetData( int32_t ) const;
/*!
	\cond
*/
	void	operator delete( void* );
	void	operator delete( void*, const char*, int );
/*!
	\endcond
*/
private:
							AOI_Contour();
							AOI_Contour( const AOI_Contour& );
	void					operator=( const AOI_Contour& );
};
typedef AOI_Contour*	AOI_ContourPtr;
