/*
 * AOI_SoftProofRenderer
 * Copyright (c) 2014 Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Renderer.h"
#include "./AOI_Document.h"
#include "./AOI_Types.h"
#include <vector>

namespace aur { namespace PDF {
	class Document;
	class Renderer;
	class CMM;
	class Profile;
	class ICCColorSpace;
}
namespace ACPL {
	class	FileSpec;
	class	UString;
	class	FileStream;
}}

class AOIAPI SpotColorCieLab
{
public:
	char		mName[64];
	float		mLab[3];
	uint8_t		mRgb[3];
};

class AOIAPI AOI_SoftProofRenderer
{
public:
	AOI_SoftProofRenderer( AOI_Document* document );
	~AOI_SoftProofRenderer();
	bool		Setup( const UTF16Char* proofSpace, const UTF16Char* outputSpace, uint32_t maxNrOfChannels, bool useCustomProfile );
	bool		SaveDocumentEmbeddedProfile( const UTF16Char* outputFolderPath, UTF16Char* profileName );
	uint32_t	GetDocumentChannelCount();
	const char*	GetDocumentChannelName( uint32_t index );
	bool		GetImageSize( int32_t& width, int32_t& height ) const;
	bool		RenderPage( const UTF16Char* outputPath, const char* thumbnailType, uint32_t thumbnailMaxSize, uint32_t pageNumber, uint32_t quality = 100 );
	uint8_t*	RenderRawBand( uint32_t y, uint32_t renderingWidth, uint32_t renderingHeight, float dpu );
	void		ConvertToOutputColorSpace( uint8_t* dest, uint32_t width, uint32_t height, int32_t rowBytes, bool bImageAlphaBlending = false );
	bool		HasAlpha() const;
	bool		HasTransparencyGroup() const;
	bool		IsSubstractive() const;
	bool		IsRGBDocumentColorSpace() const;

	SpotColorCieLab	GetSpotColorCIELab( uint32_t index );
	uint32_t		GetDocumentSpotColorsCount();
	SpotColorCieLab	GetProfilePaperWhiteCIELab();
private:
	class		Internal;
	Internal*	mInternal;
	
};