/*
 * AOI_Types
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 * Types for several utility classes
 */
#pragma once

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#if defined(__GNUC__)

	#define	ACPL_MAC	1
	#define	ACPL_WIN	0

	#define	ARCH_PPC	__BIG_ENDIAN__
	#define	ARCH_INTEL	__LITTLE_ENDIAN__

	#define	AOIAPI		__attribute__((visibility("default")))
	#define	CALLBACK

	#define	OS_BEEP		::AudioServicesPlayAlertSound( kUserPreferredAlert )

	const float	AOI_NegInf	= -3.402823466e+38F;
	const float	AOI_PosInf	= 3.402823466e+38F;
	#define		FABS		fabs
#else

	#define	ACPL_MAC	0
	#define	ACPL_WIN	1

	#define	ARCH_PPC	0
	#define	ARCH_INTEL	1

	typedef		char**			PolyHandle;

#	define fixed1 Fixed16( 0x00010000L )// thus now exsist in Universal header...
#	define FixedToFloat(a) ( float(a) / fixed1 )
#	define FloatToFixed(a) Fixed16( float(a) * fixed1 )

	#ifdef AOI_EXPORTS
		#define	AOIAPI	__declspec(dllexport)
	#else
		#define	AOIAPI	__declspec(dllimport)
	#endif

	#define	OS_BEEP		::MessageBeep(-1)

	#include <float.h>

	const float	AOI_NegInf	= -FLT_MAX;
	const float	AOI_PosInf	= FLT_MAX;
	#define		FABS		fabs
#endif

typedef		char *			Ptr;
typedef		unsigned char	Uchar;
typedef		unsigned char *	StringPtr;
typedef		const unsigned char *ConstStringPtr;
typedef		unsigned char	Str31[32];
typedef		unsigned char	Str63[64];
typedef		unsigned char	Str255[256];

typedef		int32_t			Fixed16;
typedef		long			ExceptionCode;
typedef		int32_t			Message;
typedef		unsigned short	UTF16Char;

#define		ENUM32( v )		*(int32_t*)&v

#define MAX(x,y)	((x)>(y) ? (x) : (y))
#define MIN(x,y)	((x)<(y) ? (x) : (y))

typedef struct AOI_ContourElement_Flags
{
	uint16_t	handle		: 1;
	uint16_t	selected	: 1;
	uint16_t	start		: 1;
	uint16_t	stop		: 1;
	uint16_t	closed		: 1;
	uint16_t	smooth		: 1;
	uint16_t	automatic	: 1;
	uint16_t	spline		: 1;
	uint16_t	multipurpose: 1;
	uint16_t	reserved1	: 1;
	uint16_t	reserved2	: 1;
	uint16_t	reserved3	: 1;
	uint16_t	reserved4	: 1;
	uint16_t	reserved5	: 1;
	uint16_t	reserved6	: 1;
	uint16_t	reserved7	: 1;
}	AOI_ContourElement_Flags;

typedef struct AOI_ContourElement
{
	float		x;
	float		y;
	AOI_ContourElement_Flags	flag;
}	AOI_ContourElement;
typedef AOI_ContourElement* AOI_ContourElementPtr;

class AOIAPI AOI_Mapping
{
public:
	float	map[3][3];

			AOI_Mapping();
			AOI_Mapping( const AOI_Mapping& );
			AOI_Mapping( const float* );
			AOI_Mapping( const double* );

	void	Reset();
	void	Invert();
	void	Move( float, float );
	void	Rotate( float, float, float );
	void	Scale( float, float, float, float );
	void	Skew( float, float, float, float );
	void	Map( const AOI_Mapping& );
	float	Determinant() const;
	bool	IsUnity() const;
	bool	IsMove() const;
	bool	IsScale() const;
	void	Check();
};

class AOIAPI AOI_Point
{
public:
	float	x;
	float	y;

			AOI_Point();
			AOI_Point( float, float );
			AOI_Point( const AOI_Point& );
};

class AOIAPI AOI_Rectangle
{
public:
	float	left;
	float	top;
	float	right;
	float	bottom;

			AOI_Rectangle();
			AOI_Rectangle( float x1, float y1, float x2, float y2 );
			AOI_Rectangle( const AOI_Rectangle& );
	void	Union( const AOI_Rectangle& );
	bool	Intersect( const AOI_Rectangle& );
	bool	CoordInBounds( const AOI_Point& ) const;
	bool	Overlaps( const AOI_Rectangle& ) const;
	void	TLtoBR();
};

typedef enum AOI_ColorSpaceEnum
{
	AOI_NoSpace = 0,
	AOI_RGBSpace = 1,
	AOI_CMYKSpace = 2,
	AOI_HLSSpace = 4,
	AOI_XYZSpace = 6,
	AOI_LABSpace = 8,
	AOI_GraySpace = 10,

	AOI_DeviceNSpace = 32,
	AOI_DeviceNMask = 31,

	AOI_IndexedSpaces = 64,
	AOI_IndexedMask = 63,
	AOI_IndexedRGBSpace = 65,
	AOI_IndexedCMYKSpace = 66,
	AOI_IndexedLABSpace = 72,
	AOI_IndexedGraySpace = 74,

	AOI_MaxSpace = 0xFFFFFFFF
}	AOI_ColorSpaceEnum;

int32_t	AOIAPI	AOI_ColorSpaceComponents( AOI_ColorSpaceEnum );
void	AOIAPI	AOI_ColorSpaceComponentLAB( AOI_ColorSpaceEnum space, int ch, uint16_t* lab );

inline int32_t	AOI_ColorSpaceBits( AOI_ColorSpaceEnum space )
{
	return AOI_ColorSpaceComponents( space ) << 3;
}

static const uint16_t	AOI_ColorValue1 = 0xFFFF;

#define AOI_INDEXED( s )	AOI_ColorSpaceEnum( (s) | AOI_IndexedSpaces )
#define AOI_NONINDEXED( s )	AOI_ColorSpaceEnum( (s) & ~AOI_IndexedSpaces )
#define	AOI_DEVICEN( n )	AOI_ColorSpaceEnum( AOI_DeviceNSpace | (n) )
#define	AOI_NCOMPS( s )		int32_t( (s) & AOI_DeviceNMask )
#define	MAX_CHANNELS		64

class	AOI_ColorSpace;

class	AOI_Color
{
public:
	AOI_ColorSpace*	space;
	uint16_t			channel[ MAX_CHANNELS ];
};
typedef	AOI_Color*	AOI_ColorPtr;

#define aurFontSansSerif  (0)
#define aurFontFixedWidth (1 << 0)
#define aurFontSerif      (1 << 1)
#define aurFontSymbolic   (1 << 2)
#define aurFontItalic     (1 << 6)
#define aurFontBold       (1 << 18)
#define aurFontJapanese   (1 << 7)
#define aurFontKorean     (1 << 8)
#define aurFontChineseT	  (1 << 9)
#define aurFontChineseS	  (1 << 10)
#define aurFontCJKV		  ( aurFontJapanese | aurFontKorean | aurFontChineseT | aurFontChineseS )

typedef enum	AOI_EncodingType
{
	AOI_ETUnicode = 0,
	AOI_ETMacRoman,
	AOI_ETWinAnsi,
	AOI_ETAdobeStandard,
	AOI_ETAdobeExpert,
	AOI_ETAdobeCustom,
	AOI_ETSymbol,
	AOI_ETZapfDingbats,
	AOI_ETCustom_Obsolete,
	AOI_ETCID,
	AOI_ETNoEncoding,
	AOI_ETMaxEnumEncodingType = 0xFFFFFFFFL
}	AOI_EncodingType;
