#pragma once

#include <ACPL/Types.h>

namespace aur {
	namespace ACPL {
		class	FileSpec;
		class	String;
	}
	namespace PDF {
		class	Instance;
	}
}


void PS2PDF( const aur::ACPL::FileSpec&, const aur::ACPL::FileSpec&, aur::ACPL::String&, aur::ACPL::String&, aur::PDF::Instance* );
