/*
 * AOI_Page
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Group.h"

class AOIAPI AOI_Page
{
public:
	void				GetDimensions( float&, float& ) const;
	bool				ApplyBox( const char* );
	void				SetDimensions( float, float );
	AOI_Object_LayerPtr	GetFirstLayer() const;
	AOI_ColorSpacePtr	GetPageColorSpace() const;
};
typedef AOI_Page*	AOI_PagePtr;
