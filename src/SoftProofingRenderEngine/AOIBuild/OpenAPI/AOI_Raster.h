/*
 * AOI_Raster
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Object.h"
#include "./AOI_Contour.h"
#include "./AOI_Bitmap.h"

class AOIAPI AOI_Object_Raster : public AOI_Object
{
public:
	const static uint32_t	Type = 'GRAS';

	static AOI_Object_Raster*	Create( AOI_Document* document, AOI_Object_Bitmap* bitmap );

	bool					IsMask() const;
	AOI_ColorSpacePtr		GetColorSpace() const;
	void					GetBitmapBounds( int32_t& width, int32_t& height ) const;
	AOI_Object_BitmapPtr	GetBitmap() const;
	void					GetMapping( AOI_Mapping& ) const;
	void					GetContour( AOI_ContourPtr ) const;
};
typedef AOI_Object_Raster*	AOI_Object_RasterPtr;
