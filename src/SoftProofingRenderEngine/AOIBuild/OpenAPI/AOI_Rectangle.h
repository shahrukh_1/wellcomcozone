/*
 * AOI_Vector
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Vector.h"

class AOIAPI AOI_Object_Rectangle : public AOI_Object_Vector
{
public:
	const static uint32_t	Type = 'GRCT';

	void		GetRectangle( AOI_Rectangle& ) const;
	AOI_Mapping	GetMapping() const;
};
typedef AOI_Object_Rectangle*	AOI_Object_RectanglePtr;
