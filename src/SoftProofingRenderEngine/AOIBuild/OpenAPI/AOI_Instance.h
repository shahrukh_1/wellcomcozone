/*
 * AOI_Instance
 * Copyright (c) 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Types.h"

namespace aur { namespace PDF {
	class Instance;
}}

typedef struct ColorConvert_Data	ColorConvert_Data;

class AOIAPI AOI_Instance
{
	friend class AOI_Document;
			bool			mOwnsInstance;
	aur::PDF::Instance*		mInstance;
							AOI_Instance( aur::PDF::Instance* );
public:
	typedef void (*ErrorCB)( uint16_t, uint16_t, const char*, void* );
	typedef bool (*ProgressionCB)( float, void* );

							AOI_Instance();
							~AOI_Instance();

	static	int				Version();
	static	AOI_Instance*	Startup( const char* uniqueID, const char* licenseKey, const char* appSignature = NULL );
	static	AOI_Instance*	Startup( bool componentStructure, const char* uniqueID, const char* licenseKey, const char* appSignature = NULL );
	static	void			Terminate();
			void			SetProgressCallBack( ProgressionCB, void* );
			void			SetErrorCallBack( ErrorCB, void* );
			bool			UseIGOR() const;
			void			SetIGOR( bool );
			void			SetOptions( uint32_t );
			void			ResourceDirectory( const char* );
			void			ResourceDirectory( const UTF16Char* );
	const	UTF16Char*		ResourceDirectory();
			ExceptionCode	GetDataDirectory( UTF16Char* outDirectory );
			ExceptionCode	GetApplicationDirectory( UTF16Char* outDirectory );
			float			GreekSize() const;
			void			SetGreekSize( float greekSize );
			void			SetFontAntiAliasing( bool useFontAntiAliasing );
			bool			UseFontAntiAliasing() const;
};
