/*
 * AOI_Group
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Object.h"
#include "./AOI_Contour.h"

class AOIAPI AOI_Object_Group : public AOI_Object
{
public:
	class	SoftMask
	{
	public:
		AOI_ObjectPtr		mContent;		//!<	Pointer to the object (tree) that defines the content of the softmask
		AOI_FunctionPtr		mTransfer;		//!<	Pointer to the function that defines the transfer from the actual renderered
											//!<	mContent and the values that make-up the softmask
		uint16_t			mBackDrop[4];	//!<	The colorspace that must be used to set-up the renderer to render the
											//!<	mContent.
		AOI_ColorSpaceEnum	mColorSpace;	//!<	The initial contents of the renderer before rendering mContent. mColorSpace
											//!<	defines how to interprete the number and content of these channels.
		bool				mUsesAlpha;		//!<	If the soft mask uses the alpha channel to define the transparency
											//!<	then this member is set to true. If luminosity is used this member
											//!<	is set to false.

		SoftMask();
		SoftMask( const SoftMask& );
		SoftMask& operator=( const SoftMask & );
	};

	const static uint32_t	Type = 'GGRP';

	bool			HasMask() const;
	bool			HasHardMask() const;
	bool			HasSoftMask() const;
	void			GetHardMask( AOI_ContourPtr ) const;
	const AOI_ContourPtr	GetHardMask() const;
	SoftMask		GetSoftMask() const;
	bool			GetEOFill() const;

	//	Compositing methods
	bool			GetTransparencyGroup() const;
	bool			GetIsolated() const;
	bool			GetKnockOut() const;

	AOI_ObjectPtr	GetFirstChild() const;
	int32_t			InsertChild( AOI_ObjectPtr child, int32_t index );
	void			AppendChild( AOI_ObjectPtr child );
};
typedef AOI_Object_Group*	AOI_Object_GroupPtr;

class AOIAPI AOI_Object_Layer : public AOI_Object_Group
{
public:
	const static uint32_t	Type = 'GLAY';
	const	UTF16Char*		GetName() const;
};
typedef AOI_Object_Layer*	AOI_Object_LayerPtr;
