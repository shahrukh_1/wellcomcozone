/*
 * AOI_TIFFWriter
 * Copyright � 2008 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Types.h"

#define	TIFF_Comp_None	0
#define	TIFF_Comp_CCITT	1
#define	TIFF_Comp_LZW	2

#define	TIFF_Platform_i386	uint16_t( 0x4949 )
#define	TIFF_Platform_PPC	uint16_t( 0x4D4D )

typedef	struct tiff TIFF;

#if ARCH_INTEL
#	define	TIFF_This_Platform	TIFF_Platform_i386
#else
#	define	TIFF_This_Platform	TIFF_Platform_PPC
#endif

ExceptionCode	AOIAPI AOI_WriteTIFF( const char* path, uint8_t compression, uint16_t platform,
						 AOI_ColorSpaceEnum colorSpace, uint16_t bitsPerPixel, uint32_t width, uint32_t height,
						 float dpiX, float dpiY, const void* imageData );

ExceptionCode	AOIAPI AOI_WriteTIFF( const UTF16Char* path, uint8_t compression, uint16_t platform,
						 AOI_ColorSpaceEnum colorSpace, uint16_t bitsPerPixel, uint32_t width, uint32_t height,
						 float dpiX, float dpiY, const void* imageData );

class AOIAPI AOI_TIFFWriter
{
	TIFF*			mTif;
	ExceptionCode	mErr;
	uint32_t		mWidth;
	uint32_t		mHeight;
	uint32_t		mNrOfPlanes;
	uint16_t		mBitsPerPixel;
	uint32_t		mRow;
public:
					AOI_TIFFWriter();
					~AOI_TIFFWriter();
	ExceptionCode	Init(	const UTF16Char* path, uint8_t compression, uint16_t platform,
							AOI_ColorSpaceEnum colorSpace, uint16_t bitsPerPixel, uint32_t width, uint32_t height,
							float dpiX, float dpiY );
	ExceptionCode	WriteData( uint32_t lines, const void* imageData );
};
