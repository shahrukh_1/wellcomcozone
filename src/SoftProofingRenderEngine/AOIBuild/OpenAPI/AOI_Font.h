/*
 * AOI_Font
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Types.h"

class AOIAPI AOI_Font_File
{
public:
/*	typedef	enum Type
	{
		eFile = 0,
		eMemory,
		eBuildIn,
		eMaxEnumFileType = 0xFFFFFFFFL
	}	Type;
	typedef enum FontType
	{
		eType1 = 0,
		eTrueType,
		eCFF,
		eUnknownFontType,
		eMaxEnumFontType = 0xFFFFFFFFL
	}	FontType;
*/
//	FT_Face				GetFace();

//const	char*		GetName() const;
//const	char*		GetSubstituteName() const;
//Type		GetType() const;
//uint32_t	GetStyle() const;
//float		CorpsToCapFactor();
//bool		IsEmbedded() const;
//bool		IsSubsituted() const;
//bool		GetData( unsigned char**, int* );
//FontType	GetFontType();
//char*		GetFileSubstitute();
//bool		SelectCharMap( EncodingType );
};

class AOIAPI AOI_Font_Descriptor
{
public:
//FontFile*			GetFont() const;
//FT_UInt			GlyphIndex( uint16_t ) const;
//uint16_t			GetSpaceCode() const;
//const char*		GetCharName( uint16_t ) const;
//uint16_t			GetCharCode( const char* ) const;

//EncodingType		GetBaseEncoding() const;
//FontEncoding*		GetCustomEncoding() const;
//uint16_t			GetWidth( uint16_t ) const;
//float				GetMissingGlyphWidth() const;
//bool				IsStdEncoded() const;
//bool				IsEditable() const;
//bool				HasWidthTable() const;
//FT_UInt			GlyphIndexType1( uint16_t ) const;
//FT_UInt			GlyphIndexTrueType( uint16_t ) const;
//FT_UInt			GlyphIndexCustom( uint16_t ) const;
//FT_UInt			GlyphIndexCID( uint16_t ) const;
};
