#pragma once

#include "AOI_String.h"
#include <string.h>

typedef	enum XMLResult
{
	eXMLNoError,
	eXMLNotValid,
	eXMLNotFound,
	eXMLInvalidTag
} XMLResult;

namespace aur { namespace ACPL {
	class	Stream;
	class	String;
}}
class	ElementsList;

class AOIAPI AOI_XML
{
public:
	class	Node;
	class AOIAPI Element
	{
		friend class Node;
	public:
						Element( const char* name, const char* value ); 
						Element( const char* name, int32_t value );
						Element( const char* name, uint32_t value );
						Element( const char* name, float value );
						Element( const char* name, bool value );
		virtual			~Element();

		const char*		operator+=( const char* inStr );
		void			operator=( const Element& );
		operator const char*() const;
		virtual Element* Duplicate() const;

		Node*			GetParentNode();

		float			ToFloat( float defaultV = 0 ) const;
		int32_t			ToInt( int32_t defaultV = 0 ) const;
		uint32_t		ToUInt( uint32_t defaultV = 0 ) const;
		bool			ToBool( bool defaultV = false ) const; 

		void			SetValue( const char* v );
		void			SetValue( int32_t v );
		void			SetValue( uint32_t v );
		void			SetValue( float v );
		void			SetValue( bool v );
		void			Format( const char* format, ... );

	const AOI_String&	Name() const;
		void			SetName( const char* name );
		bool			IsEmpty() const;

	protected:
		AOI_String		mValue;
		AOI_String		mName;
		Node*			mParentNode;

		virtual void	WriteToFile( aur::ACPL::Stream& );
	};

	class AOIAPI Node : public Element
	{
		friend class AOI_XML;
	public:
						Node( const char* name = NULL );
		virtual			~Node();
		virtual Element* Duplicate() const;

		bool			HasName() const;
		bool			HasAttributes() const;
		bool			HasChildren() const;

		uint32_t		GetNrOfChildren() const;
		uint32_t		AttributesCount() const;

		Element*		GetAttributeByIndex( uint32_t index ) const;
		Element*		GetAttributeByName( const char* name ) const;

		Element*		GetElementByIndex( uint32_t index ) const;
		Element*		GetElementByName( const char* name ) const;
		Element*		GetElementByXPath( const char* name );

		Node*			GetNodeByIndex( uint32_t index ) const;
		Node*			GetNodeByName( const char* ) const;
		Node*			GetNodeByXPath( const char* );

		void			AddAttribute( Element* element );
		void			AddAttribute( const char* name, const char* value );
		void			AddAttribute( const char* name, int32_t value );
		void			AddAttribute( const char* name, uint32_t value );
		void			AddAttribute( const char* name, float value );
		void			AddAttribute( const char* name, bool value );

		void			AddChildNode( Element* );
		bool			DeleteChildNodeByName( const char* );
		bool			DeleteChildNodeByIndex( uint32_t );
		bool			DetachChildNode( AOI_XML::Node* );

		void			SetElementValue( const char* label, int32_t value );
		void			SetElementValue( const char* label, uint32_t value );
		void			SetElementValue( const char* label, bool value );
		void			SetElementValue( const char* label, float value );
		void			SetElementValue( const char* label, const char* str );
		bool			GetElementValue( const char* label, AOI_String& str ) const;
		void			SetElementValue( const char* label, uint32_t dataLen, const uint8_t* data );

		bool			GetElementValue( const char* label, int32_t& value ) const;
		bool			GetElementValue( const char* label, uint32_t& value ) const;
		bool			GetElementValue( const char* label, bool& value ) const;
		bool			GetElementValue( const char* label, float& value ) const;
		bool			GetElementValue( const char* label, char* str ) const;
		bool			GetElementValue( const char* label, uint32_t& dataLen, uint8_t* data ) const;
	protected:
		ElementsList*	mAttributes;
		ElementsList*	mChildNodes;

		void			ReadNodeData( aur::ACPL::Stream& );
		XMLResult		ParseContent( aur::ACPL::Stream& );
		XMLResult		ParseTag( const aur::ACPL::String& );
		virtual void	WriteToFile( aur::ACPL::Stream& );
	};

				AOI_XML();
				~AOI_XML();
	XMLResult 	Load( const UTF16Char* xmlFilePath );
	XMLResult	Load( aur::ACPL::Stream& );
	XMLResult	Load( const char* );
	void 		Save( const UTF16Char* xmlFilePath );
	void 		Save( aur::ACPL::Stream& );
	char*		Save();
	static void	DisposeMemory( char* mem );
	void		Empty();

	Node*		root;
	Node*		GetNodeByXPath( const char* );
	const char*	GetValueByXPath( const char* );

private:
	XMLResult 	ValidateXML( aur::ACPL::Stream& );
	void 		SkipDTD( aur::ACPL::Stream& );
};
