#pragma once

#include "./AOI_Function.h"
#include "./profile.h"

class AOIAPI AOI_ColorSpace
{
public:
	uint32_t			GetType() const;
	AOI_ColorSpaceEnum	Space() const;
	uint32_t			NrOfComponents() const;
	const char*			ChannelName( uint32_t ) const;
	void				GetCIELab( const uint16_t* src, float* out ) const;
};
typedef AOI_ColorSpace*	AOI_ColorSpacePtr;

class AOIAPI  AOI_ColorSpace_Lab : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'LAB!';
};
typedef AOI_ColorSpace_Lab*	AOI_ColorSpace_LabPtr;

class AOIAPI AOI_ColorSpace_CalibratedGray : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'CL1!';

	void		GetWhitePoint( double[3] ) const;
	void		GetBlackPoint( double[3] ) const;
	double		GetGamma() const;
};
typedef AOI_ColorSpace_CalibratedGray*	AOI_ColorSpace_CalibratedGrayPtr;

class AOIAPI AOI_ColorSpace_CalibratedRGB : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'CL3!';

	void		GetWhitePoint( double[3] ) const;
	void		GetBlackPoint( double[3] ) const;
	void		GetGamma( double[3] ) const;
	void		GetRedColorant( double[3] ) const;
	void		GetGreenColorant( double[3] ) const;
	void		GetBlueColorant( double[3] ) const;
};
typedef AOI_ColorSpace_CalibratedRGB*	AOI_ColorSpace_CalibratedRGBPtr;

class AOIAPI AOI_ColorSpace_ICC : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'ICC!';

	const	icHeader&	GetHeader() const;
	void				GetDescriptor( char* ) const;
	void				GetICCData( uint8_t* ) const;
};
typedef AOI_ColorSpace_ICC*	AOI_ColorSpace_ICCPtr;

class AOIAPI AOI_ColorSpace_DeviceGray : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'GRY!';
};
typedef AOI_ColorSpace_DeviceGray*	AOI_ColorSpace_DeviceGrayPtr;

class AOIAPI AOI_ColorSpace_DeviceRGB : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'RGB!';
};
typedef AOI_ColorSpace_DeviceRGB*	AOI_ColorSpace_DeviceRGBPtr;

class AOIAPI AOI_ColorSpace_DeviceCMYK : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'CMY!';
};
typedef AOI_ColorSpace_DeviceCMYK*	AOI_ColorSpace_DeviceCMYKPtr;

class AOIAPI AOI_ColorSpace_IllustratorCMYK : public AOI_ColorSpace_DeviceCMYK
{
public:
	static const uint32_t	Type = 'cmy!';
};
typedef AOI_ColorSpace_IllustratorCMYK*	AOI_ColorSpace_IllustratorCMYKPtr;

class AOIAPI AOI_ColorSpace_DeviceN : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'DVN!';

	AOI_ColorSpacePtr	ChannelSpace( uint32_t ) const;
	AOI_Function*		ChannelFunction( uint32_t ) const;
	AOI_ColorSpacePtr	GetAlternate() const;
	AOI_Function*		GetFunction() const;
};
typedef AOI_ColorSpace_DeviceN*	AOI_ColorSpace_DeviceNPtr;

class AOIAPI	AOI_ColorSpace_Indexed : public AOI_ColorSpace
{
public:
	static const uint32_t	Type = 'IDX!';

	uint32_t			GetColorCount() const;
	AOI_ColorSpacePtr	GetBase() const;
	void				GetColor( uint32_t, uint16_t* ) const;
};
typedef AOI_ColorSpace_Indexed*	AOI_ColorSpace_IndexedPtr;
