/*
 * AOI_Style
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_ColorSpace.h"

class AOI_Object;
class AOI_Renderer;
class AOI_Document;

typedef enum AOI_BlendMode
{
	eAOI_BMNormal,
	eAOI_BMMultiply,
	eAOI_BMScreen,
	eAOI_BMOverlay,
	eAOI_BMSoftLight,
	eAOI_BMHardLight,
	eAOI_BMColorDodge,
	eAOI_BMColorBurn,
	eAOI_BMDarken,
	eAOI_BMLighten,
	eAOI_BMDifference,
	eAOI_BMExclusion,
	eAOI_BMHue,
	eAOI_BMSaturation,
	eAOI_BMColor,
	eAOI_BMLuminosity,
	eAOI_MaxEnumBlendMode = 0xFFFFFFFFL
} AOI_BlendMode;

typedef enum AOI_Intent
{
	eAOI_Perceptual = 0,
	eAOI_RelativeColorimetric = 1,
	eAOI_Saturation = 2,
	eAOI_AbsoluteColorimetric = 3,
	eAOI_MaxEnumIntent = 0xFFFFFFFFL
}	AOI_Intent;

class AOIAPI AOI_Style
{
public:
	uint32_t	GetType() const;
	void		Dispose();
};
typedef AOI_Style*	AOI_StylePtr;

class AOIAPI AOI_Style_Color : public AOI_Style
{
public:
	const static uint32_t	Type = 'STY0';

	static AOI_Style_Color*	Create( AOI_Document* document, const AOI_Color& );

	void			GetColor( AOI_Color& ) const;
	AOI_ColorSpace*	GetSpace() const;
	void			GetDescriptor( char* name ) const;
};
typedef AOI_Style_Color*	AOI_Style_ColorPtr;

class	AOIAPI	AOI_Style_Shading : public AOI_Style
{
public:
	const static uint32_t	Type = 'STY1';

			void			GetBounds( AOI_Rectangle& ) const;
			AOI_ColorSpace*	GetColorSpace() const;
	const	AOI_Mapping&	GetMapping() const;

			void			Open( AOI_Renderer* ) const;
			void			Close() const;
			bool			GetColor( int32_t, int32_t, AOI_Color& ) const;
};
typedef AOI_Style_Shading*	AOI_Style_ShadingPtr;

class	AOIAPI	AOI_Style_FunctionShading : public AOI_Style_Shading
{
public:
	const static uint32_t	Type = 'STY2';

	AOI_Function**	GetFunctions() const;
};
typedef AOI_Style_FunctionShading*	AOI_Style_FunctionShadingPtr;

class	AOIAPI	AOI_Style_Axial : public AOI_Style_Shading
{
public:
	const static uint32_t	Type = 'STY3';

	void			GetPoints( AOI_Point&, AOI_Point& ) const;
	void			GetExtends( bool&, bool& ) const;
	uint32_t		GetFunctionCount() const;
	AOI_Function**	GetFunctions( uint32_t& ) const;
};
typedef AOI_Style_Axial*	AOI_Style_AxialPtr;

class	AOIAPI	AOI_Style_Radial : public AOI_Style_Axial
{
public:
	const static uint32_t	Type = 'STY4';

	void			GetCircleStart( AOI_Point& outCenter, float& outRadius ) const;
	void			GetCircleEnd( AOI_Point& outCenter, float& outRadius ) const;
	uint32_t		GetFunctionCount() const;
	AOI_Function**	GetFunctions( uint32_t& ) const;
};
typedef AOI_Style_Radial*	AOI_Style_RadialPtr;

class	AOIAPI	AOI_Style_CoonePatch : public AOI_Style_Shading
{
public:
	const static uint32_t	Type = 'STY5';
/*!
	\cond
*/
	class AOI_SE
	{
	public:
		AOI_ContourElement	p;
		uint16_t			channel[ MAX_CHANNELS ];
	};
	class AOI_Beziers
	{
	public:
		uint32_t			nrElements;
		uint32_t			startPointIndex;
		uint32_t			endPointIndex;
		AOI_ContourElement* elements;
	};
	class AOI_Side
	{
	public:
		uint32_t	nr;
		uint32_t*	mBeziersIndexen;
	};
	class AOI_Patch
	{
	public:
		int32_t	sides[4];
	};
/*!
	\endcond
*/

public:
			uint32_t		GetPatchCount() const;
	const	AOI_Patch&		GetPatch( uint32_t index ) const;
	const	AOI_Side&		GetSide( uint32_t index ) const;
	const	AOI_Beziers&	GetBezier( uint32_t index ) const;
	const	AOI_SE&			GetPoint( uint32_t index ) const;
};
typedef AOI_Style_CoonePatch*	AOI_Style_CoonePatchPtr;

class	AOIAPI	AOI_Style_Pattern : public AOI_Style
{
public:
	const static uint32_t	Type = 'STY6';

			AOI_Object*		GetTile() const;
			AOI_Point		GetDeltas() const;
	const	AOI_Mapping&	GetMapping() const;
};
typedef AOI_Style_Pattern*	AOI_Style_PatternPtr;

struct	ColorConvert_Data
{
	const AOI_Object*	inObject;
	AOI_ColorSpace*		inSourceSpace;
	AOI_ColorSpace*		inDestinationSpace;
	bool				inIsTransparencyGroup;
	const uint16_t*		inChannels16;
	const uint8_t*		inChannels8;
	uint32_t			inPixelCount;
	bool				inIsVector;
	bool				inIsShading;
	//	Are initially filled with pointers to empty channels and set
	uint16_t*			outChannels16;
	uint8_t*			outChannels8;
	bool*				outChannelsUsed;
};
