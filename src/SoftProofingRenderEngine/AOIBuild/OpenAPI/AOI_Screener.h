/*
 * AOI_Screener
 * Copyright � 2008 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Types.h"

class AOIAPI AOI_Screener
{
/*!
	\cond
*/
	class		Internal;
	Internal*	mInternal;
/*!
	\endcond
*/
public:
					AOI_Screener( const char* screenerType );
					~AOI_Screener();
			void	PageStart( uint32_t channelCnt, uint32_t width, uint32_t height, uint32_t levels, const float* dotWeights, const uint16_t* linCurve );
			void	PageEnd();
			void	ProcessLine( const void* inLine, void* outLine );
};
