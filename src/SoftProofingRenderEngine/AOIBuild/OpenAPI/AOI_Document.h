/*
 * AOI_Document
 * Copyright (c) 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Page.h"
#include "./AOI_Rectangle.h"
#include "./AOI_Ellipse.h"
#include "./AOI_Path.h"
#include "./AOI_Text.h"
#include "./AOI_Raster.h"
#include "./AOI_Multigon.h"
#include "./AOI_Dimension.h"
#include "./AOI_PlaceHolder.h"

#include <ACPL/CString.h>

#include <vector>

class	AOI_Instance;

namespace aur {
	namespace PDF {
		class	Document;
		class	Instance;
	}
	namespace ACPL {
		class	FileSpec;
	}
}

typedef	const char*	SpotColorName;

class AOIAPI AOI_Document
{
	friend class AOI_Renderer;
	friend class AOI_TIFFRenderer;
	friend class AOI_Object_Text;
	friend class AOI_Object_Raster;
	friend class AOI_Style_Color;
	friend class AOI_SoftProofRenderer;
	aur::PDF::Instance*		mInstance;
	aur::PDF::Document*		mDocument;
	aur::ACPL::FileSpec*	mPDFSpec;
	SpotColorName*			mSpotColorList;
	aur::ACPL::FileSpec*	mImageSpec;
	bool					mUseSimpleRenderer;
public:
						AOI_Document( AOI_Instance* );
						~AOI_Document();

	void				Open( const char* path, const char* ownerPassword = NULL, const char* userPassword = NULL );
	void				Open( const UTF16Char* path, const char* ownerPassword = NULL, const char* userPassword = NULL );
	void				CloseFile( bool keepVM, bool createDefaults );
	ExceptionCode		ConvertPStoPDF( const UTF16Char* pathToPS, const UTF16Char* pathToPDF );
	ExceptionCode		SaveAsPDF( const UTF16Char* path );
	uint32_t			GetPageCount() const;
	AOI_Page*			GetPage( uint32_t ) const;
	bool				TraverseNext( AOI_Object*& ) const;
	AOI_ColorSpacePtr	GetDocumentColorSpace() const;
	AOI_ColorSpacePtr	GetLabColorSpace() const;
	AOI_ColorSpacePtr	GetDeviceGrayColorSpace() const;
	AOI_ColorSpacePtr	GetDeviceRGBColorSpace() const;
	AOI_ColorSpacePtr	GetDeviceCMYKColorSpace() const;
	bool				BlackAndWhiteDoc() const;
	bool				FontExists( bool onlyUsed ) const;
	SpotColorName*		GetSpotColorList();
	AOI_Object*			HitTest( AOI_Point p, int16_t snapDistance, bool& stroke, AOI_Color* outColor = NULL );
	void				SimpleImage();
};

class AOIAPI AOI_DocumentInfo
{
public:
	int32_t				fileSize;
	uint32_t			pageCount;
	float				width;
	float				height;
	AOI_ColorSpaceEnum	space;
	bool				encrypted;
	bool				denyPrint;
	bool				denyChange;
	bool				denyCopy;
	bool				denyNotes;
	std::vector<aur::ACPL::String>	fonts;
	aur::ACPL::String	fileType;
	aur::ACPL::String	description;
	aur::ACPL::String	artist;		//	name of the camera owner
	aur::ACPL::String	software;	//	Exif software version
	aur::ACPL::String	make;
	aur::ACPL::String	model;
	uint16_t			orientation;
	float				xResolution;
	float				yResolution;
	uint32_t			pixelWidth;
	uint32_t			pixelHeight;
	uint16_t			resolutionUnit;
	aur::ACPL::String	dateTime;
	//	EXIF IFD Attribute information
	float				exposureTime;
	float				Fnumber;
	uint16_t			ISOSpeedRating;
	float				exposureBiasValue;

			AOI_DocumentInfo();
	bool	GetInfo( const aur::ACPL::UString& path );
};
