/*
 * AOI_Text
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Vector.h"

class	AOI_Document;

typedef struct AOI_TextProperties
{
	struct AOI_TextStyle
	{
		char		fontName[64];
		float		size;
		float		width;
		float		slant;
		float		charSpacing;
		float		wordSpacing;
		float		transport;
	}				style;
	struct AOI_TextValid
	{
		bool		font;
		bool		size;
		bool		width;
		bool		slant;
		bool		charSpacing;
		bool		wordSpacing;
		bool		transport;
	}				valid;
}	AOI_TextProperties;

class AOIAPI AOI_Object_Text : public AOI_Object_Vector
{
public:
	const static uint32_t	Type = 'GTXT';

	static AOI_Object_Text*	Create( AOI_Document* document );

	void			SetText( const char* asciiText );
	void			SetText( const UTF16Char* uniText );
	int32_t			GetText( int selStart, int selEnd, UTF16Char* uniText ) const;
	void			SetProperties( int selStart, int selEnd, const AOI_TextProperties& properties );
	void			GetProperties( int selStart, int selEnd, AOI_TextProperties& properties ) const;
	AOI_Mapping		GetMapping() const;
};
typedef AOI_Object_Text*	AOI_Object_TextPtr;


class AOIAPI AOI_Object_PathText : public AOI_Object_Text
{
public:
	const static uint32_t	Type = 'PTXT';
};
typedef AOI_Object_PathText*	AOI_Object_PathTextPtr;
