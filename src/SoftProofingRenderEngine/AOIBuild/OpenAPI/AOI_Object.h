/*
 * AOI_Object
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Style.h"

class	AOI_Group;

class AOIAPI AOI_Object
{
public:
	static void		operator delete( void* );

	uint32_t		GetType() const;
	void			GetBounds( AOI_Rectangle& ) const;
	AOI_Object*		GetNext() const;
	AOI_Group*		GetParent() const;
	void			GetMaskedBounds( AOI_Rectangle& ) const;
	void			GetQuickBounds( AOI_Rectangle& ) const;
	void			GetPrecisionBounds( AOI_Rectangle& ) const;

	AOI_BlendMode	GetBlendMode() const;
	float			GetBlendValue() const;
	AOI_Intent		GetIntent() const;
	bool			GetOverprint() const;
	bool			Visible() const;

	void			SetBlendMode( AOI_BlendMode );
	void			SetBlendValue( float );
	void			SetIntent( AOI_Intent );
	void			SetOverprint( bool );

	void			Map( const AOI_Mapping& mapping );
	void			Scale( const AOI_Mapping& );
};
typedef AOI_Object*	AOI_ObjectPtr;
