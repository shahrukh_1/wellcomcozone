/*
 * AOI_Vector
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Object.h"
#include "./AOI_Contour.h"

class AOIAPI AOI_Object_Vector : public AOI_Object
{
public:
	void			GetContour( AOI_ContourPtr ) const;

	AOI_StylePtr	GetFillStyle() const;
	AOI_StylePtr	GetStrokeStyle() const;
	void			SetFillStyle( AOI_StylePtr );
	void			SetStrokeStyle( AOI_StylePtr );
	bool			GetEOFill() const;
	void			SetEOFill( bool );
	float			GetStrokeWidth() const;
	void			SetStrokeWidth( float );
	float			GetJoin() const;
	void			SetJoin( float );
	uint8_t			GetCaps() const;
	void			SetCaps( uint8_t );
	void			GetDash( int32_t*, float**, float* ) const;
	void			SetDash( int32_t, float*, float );
};
typedef AOI_Object_Vector*	AOI_Object_VectorPtr;
