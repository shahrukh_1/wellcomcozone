/*
 * AOI_Renderer
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Document.h"

namespace aur { namespace PDF {
	class	Renderer;
}}
class	AOI_TIFFRenderer;

typedef struct ColorConvert_Data	ColorConvert_Data;
typedef bool (*ColorConvertCB)( ColorConvert_Data&, void* );

class AOIAPI AOI_Renderer
{
	AOI_Document*		mDocument;
	aur::PDF::Renderer*	mRenderer;
	AOI_TIFFRenderer*	mSimpleRenderer;
public:
					AOI_Renderer( AOI_Document& doc );
					~AOI_Renderer();
	ExceptionCode	Setup( const UTF16Char* profilePath, uint32_t extraChannelCount, const char** extraChannelNames,
						int32_t bytesPerComponent, void* banddata, int32_t w, int32_t h, bool alpha, bool shape = false );
	ExceptionCode	Setup( const char* profilePath, uint32_t extraChannelCount, const char** extraChannelNames,
						int32_t bytesPerComponent, void* banddata, int32_t w, int32_t h, bool alpha, bool shape = false );
	void			SetColorConvertCallBack( ColorConvertCB cb, void* ref );
	void			SetAntiAlias( bool );

	void			SetMapping( const AOI_Mapping& docToBandMap );
	void			Render( const AOI_Rectangle& docRect, AOI_ObjectPtr toRenderObj = NULL );
};
