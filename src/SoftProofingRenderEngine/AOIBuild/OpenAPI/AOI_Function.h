/*
 * AOI_Function
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Types.h"

class AOIAPI AOI_Function
{
public:
			int				GetInputSize() const;
			int				GetOutputSize() const;
			void			Transform( double *in, double *out ) const;
//			void			GetDomain( int, double&, double& );
//			void			GetRange( int, double&, double& );
			bool			GetInfo( uint32_t& nrBounds, double* bounds );
};
typedef AOI_Function*	AOI_FunctionPtr;

class AOIAPI AOI_Function_Sampled : public AOI_Function
{
public:
};

class AOIAPI AOI_Function_Exponential : public AOI_Function
{
public:
};

class AOIAPI AOI_Function_Stitching: public AOI_Function
{
public:
	AOI_FunctionPtr		GetFunction( uint32_t ) const;
	double*				GetBounds() const;
};

class AOIAPI AOI_Function_Postscript : public AOI_Function
{
public:
};

class AOIAPI AOI_Function_Identity : public AOI_Function
{
public:
};
