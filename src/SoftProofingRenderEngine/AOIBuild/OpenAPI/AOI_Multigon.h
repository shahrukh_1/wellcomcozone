/*
 * AOI_Multigon
 * Copyright � 2007 Graphic Mission BV / Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./AOI_Vector.h"

class AOIAPI AOI_Object_Multigon : public AOI_Object_Vector
{
public:
	const static uint32_t	Type = 'MGON';

	AOI_Point	GetCentre() const;
	int32_t		GetNumberCorners() const;
	AOI_Point	GetRadi() const;
	float		GetAngle() const;
	bool		IsStar() const;
	AOI_Point	GetStart() const;
	void		GetMapping( AOI_Mapping& ) const;
};
typedef AOI_Object_Multigon*	AOI_Object_MultigonPtr;
