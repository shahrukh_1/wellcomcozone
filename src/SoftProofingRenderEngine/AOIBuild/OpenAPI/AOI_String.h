#pragma once

#include "AOI_Types.h"

class AOIAPI AOI_String
{
	char*			mString;
public:
					AOI_String();
					AOI_String( const char* );
					~AOI_String();
	AOI_String&		operator=( const AOI_String& );
	AOI_String&		operator=( const char* );

	int				GetLength() const;
	bool			IsEmpty() const;
	char*			GetBuffer( uint32_t );
	void			ReleaseBuffer();
					operator const char*() const;
	AOI_String&		operator+=( const char* );
	AOI_String&		operator+=( const AOI_String& );
	bool			operator==( const AOI_String& ) const;
	bool 			operator==( const char* ) const;
	bool			operator!=( const AOI_String& ) const;
	bool			operator!=( const char* ) const;
};

inline	AOI_String::operator const char*() const
{
	return mString;
}

inline	bool	AOI_String::IsEmpty() const
{
	return *mString == 0;
}

inline bool	AOI_String::operator!=( const AOI_String& s ) const
{
	return !(*this==s);
}

inline bool	AOI_String::operator!=( const char* s ) const
{
	return !(*this==s);
}
