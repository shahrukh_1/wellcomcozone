#pragma once

namespace aur { namespace PDF {
	class Object;
	class ColorSpaceObject;
}}

struct	ColorConvert_Data
{
	const aur::PDF::Object*		inObject;
	aur::PDF::ColorSpaceObject*	inSourceSpace;
	aur::PDF::ColorSpaceObject*	inDestinationSpace;
	bool						inIsTransparencyGroup;
	const uint16_t*				inChannels16;
	const uint8_t*				inChannels8;
	uint32_t					inPixelCount;
	bool						inIsVector;
	bool						inIsShading;
	//	Are initially filled with pointers to empty channels and set
	uint16_t*					outChannels16;
	uint8_t*					outChannels8;
	bool*						outChannelsUsed;
};
