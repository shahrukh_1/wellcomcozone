#pragma once
#include "./CString.h"

//UUID  = time-low "-" time-mid "-" time-high-and-version "-" clock-seq-and-reserved clock-seq-low "-" node

namespace aur { namespace ACPL {

	class ACPLAPI GUID
	{
	public:
		static String Generate();
	};

} }
