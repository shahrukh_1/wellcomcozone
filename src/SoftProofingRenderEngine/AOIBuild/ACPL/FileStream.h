/*
 * FileStream
 * Copyright (c) 1998-2012 Aurelon. All rights reserved.
 *
 * Class for reading/writing an ordered sequence of bytes to a file
 *
 *	14-11-2001	ES	Extended to 64 bit file offsets
 */

#pragma once

#include "./Stream.h"
#include "./FileSpec.h"

namespace aur { namespace ACPL {

#if ACPL_MAC || ACPL_LINUX
	enum Permission
	{
		eReadPermission = 1,
		eWritePermission,
		eRdWrPermission,
		eReadOnlyPermission
	};
#else
	enum Permission
	{
		eReadPermission = GENERIC_READ,
		eWritePermission = GENERIC_WRITE,
		eRdWrPermission = GENERIC_READ | GENERIC_WRITE,
		eReadOnlyPermission = 999
	};
#endif

	class ACPLAPI FileStream : public Stream
	{
	protected:
				FileSpec		mSpec;

				uint8_t*		mBuffer;
				uint32_t		mMaxBufSize;
				uint32_t		mBufSize;
				uint32_t		mBufFill;
				uint32_t		mBufPos;
				int64_t			mStaticLength;
				Permission		mPermission;
				bool			mDirty;
				bool			mWriteOnly;
#if ACPL_MAC
				FSIORefNum		mRef;
#elif ACPL_WIN
				HANDLE			mRef;
#elif ACPL_LINUX
				FILE*			mRef;
#endif
	public:
								FileStream( Permission = eRdWrPermission, int32_t = 5120 );
								FileStream( const FileSpec&, Permission = eRdWrPermission, int32_t = 5120 );
		virtual					~FileStream();
		inline	FileSpec&		GetSpec() { return mSpec; }
				void			Flush();
				void			ChangeFile( const FileSpec&, Permission = eRdWrPermission );

		virtual void			SetMarker( int64_t, StreamFrom );
		virtual int64_t			GetMarker() const;

		virtual void			SetLength( int64_t );
		virtual int64_t			GetLength() const;

		virtual ExceptionCode	GetBytes( void *outBuffer, int32_t &ioByteCount );
		virtual ExceptionCode	PutBytes( const void *inBuffer, int32_t &ioByteCount );
	private:
				void			Open( Permission );
				void			Close();
								FileStream( const FileStream& );
				void			operator=( const FileStream& );
				void			ReadWindow( int32_t );
				void			SetActualFilePos( int64_t );
	};

}}
