/*
 * ACPL Types
 * Copyright 1998-2012 Erik Strik. All rights reserved.
 *
 * Types for several utility classes
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#ifdef _DEBUG
#	define debugging 1
#endif

#if defined( __linux__ )

#	define	ACPL_MAC	0
#	define	ACPL_WIN	0
#	define	ACPL_LINUX	1

#	define	ARCH_PPC	0
#	define	ARCH_INTEL	1

#	ifdef ACPL_INTERNAL
#		define	ACPLAPI
#	elif defined( ACPL_EXPORTS )
#		define	ACPLAPI		__attribute__((visibility("default")))
#	else
#		define	ACPLAPI
#	endif
#	ifdef DLL_EXPORTS
#		define	DLLAPI		__attribute__((visibility("default")))
#	else
#		define	DLLAPI
#	endif
#	define	CALLBACK

#	define	OS_BEEP			::AudioServicesPlayAlertSound( kUserPreferredAlert )

#elif defined( __GNUC__ )

#	include <AudioToolbox/AudioToolbox.h>
#	include <Carbon/Carbon.h>

#	define	ACPL_MAC	1
#	define	ACPL_WIN	0
#	define	ACPL_LINUX	0

#	define	ARCH_PPC	__BIG_ENDIAN__
#	define	ARCH_INTEL	__LITTLE_ENDIAN__

#	ifdef ACPL_INTERNAL
#		define	ACPLAPI
#	elif defined( ACPL_EXPORTS )
#		define	ACPLAPI		__attribute__((visibility("default")))
#	else
#		define	ACPLAPI
#	endif
#	ifdef DLL_EXPORTS
#		define	DLLAPI		__attribute__((visibility("default")))
#	else
#		define	DLLAPI
#	endif
#	define	CALLBACK

#	define	OS_BEEP			::AudioServicesPlayAlertSound( kUserPreferredAlert )

	int ACPLAPI stricmp( const char * s1, const char * s2 );
	int ACPLAPI strnicmp( const char * s1, const char * s2, size_t len );

#else

#ifndef WIN32_LEAN_AND_MEAN
#	define WIN32_LEAN_AND_MEAN
#endif
#ifndef WINVER
#	define WINVER 0x0501
#endif
#ifndef _WIN32_WINNT
#	define _WIN32_WINNT 0x0501
#endif
#ifndef _WIN32_WINDOWS
#	define _WIN32_WINDOWS 0x0501
#endif
#ifndef UNICODE
#	define	UNICODE
#endif
#	include <windows.h>

#	define	ACPL_MAC	0
#	define	ACPL_WIN	1
#	define	ACPL_LINUX	0

#	define	ARCH_PPC	0
#	define	ARCH_INTEL	1

	const double pi = 3.14159265358979323846;

	typedef	char**			PolyHandle;

#	define fixed1 Fixed16( 0x00010000L )// thus now exsist in Universal header...
#	define FixedToFloat(a) ( float(a) / fixed1 )
#	define FloatToFixed(a) Fixed16( float(a) * fixed1 )

#	ifdef ACPL_INTERNAL
#		define	ACPLAPI
#	elif defined( ACPL_EXPORTS )
#		define	ACPLAPI		__declspec(dllexport)
#	else
#		define	ACPLAPI		__declspec(dllimport)
#	endif
#	ifdef DLL_EXPORTS
#		define	DLLAPI		__declspec(dllexport)
#	else
#		define	DLLAPI		__declspec(dllimport)
#	endif

#	define	OS_BEEP			::MessageBeep(-1)
#endif

#ifndef EPS
#	define EPS 0.0001f
#endif
#ifndef MIN
#	define MIN(x,y)	((x)<(y) ? (x) : (y))
#endif
#ifndef MAX
#	define MAX(x,y)	((x)>(y) ? (x) : (y))
#endif
#ifndef FEQ
#	define FEQ(x,y) (fabs((x) - (y)) < EPS)
#endif

namespace aur {

	typedef		int32_t			Fixed16;

	typedef		char *			Ptr;
	typedef		unsigned char *	StringPtr;
	typedef		const unsigned char *ConstStringPtr;
	typedef		unsigned char	Str31[32];
	typedef		unsigned char	Str63[64];
	typedef		unsigned char	Str255[256];

	typedef		uint16_t		UniChar;

	typedef		long			ExceptionCode;
	typedef		int32_t			Message;

	#define		ENUM32( v )		*(int32_t*)&v

	typedef struct Flags
	{
		uint16_t	handle		: 1;
		uint16_t	selected	: 1;
		uint16_t	start		: 1;
		uint16_t	stop		: 1;
		uint16_t	closed		: 1;
		uint16_t	smooth		: 1;
		uint16_t	automatic	: 1;
		uint16_t	spline		: 1;
		uint16_t	multipurpose: 1;
		uint16_t	reserved1	: 1;
		uint16_t	reserved2	: 1;
		uint16_t	reserved3	: 1;
		uint16_t	reserved4	: 1;
		uint16_t	reserved5	: 1;
		uint16_t	reserved6	: 1;
		uint16_t	reserved7	: 1;
	}	Flags;

	typedef struct ContourElement
	{
		float	x;
		float	y;
		Flags	flag;
	}	ContourElement, *ContourElementPtr;

	const int	LogLevelCritical	= 1;
	const int	LogLevelError		= 2;
	const int	LogLevelWarning		= 3;
	const int	LogLevelInfo		= 4;
	const int	LogLevelDebug		= 5;

	typedef void (*ExtraLogging)(const char*);
	
	void ACPLAPI	LogInit( const char* appName, int maxLevel = LogLevelWarning, ExtraLogging method = NULL );
	void ACPLAPI	LogEvent( int level, const char* str, ... );
	int	 ACPLAPI	GetLogLevel();
	void ACPLAPI	AlertThrowAt( ExceptionCode	inError, const char* inFile, int inLine );

#	define	ThrowErr(err)											\
		do {															\
			ExceptionCode	___theErr = err;							\
			AlertThrowAt( ___theErr, __FILE__, __LINE__);				\
			throw ExceptionCode(___theErr);								\
		} while (false)

#	define ThrowIfError(err)											\
		do {															\
			ExceptionCode	__theErr = err;								\
			if (__theErr != 0) {										\
				ThrowErr(__theErr);										\
			}															\
		} while (false)

#	define	ThrowIf(test)												\
		do {															\
			if (test) ThrowErr(ErrAssertFailed);						\
		} while (false)

#	define	ThrowIfNot(test)											\
		do {															\
			if (!(test)) ThrowErr(ErrAssertFailed);						\
		} while (false)

#if ACPL_MAC

#	define	ThrowIfResFail(h)											\
		do {															\
			if ((h) == nil) {											\
				OSErr	__theErr = ::ResError();						\
				if (__theErr == noErr) {								\
					__theErr = resNotFound;								\
				}														\
				ThrowErr(__theErr);										\
			}															\
		} while (false)


// ---------------------------------------------------------------------------

	const ExceptionCode	ErrFileNotFound	= fnfErr;
	const ExceptionCode	ErrEndOfFile	= eofErr;
	const ExceptionCode	ErrDiskFull		= dskFulErr;
	const ExceptionCode	ErrFilePostion	= posErr;
	const ExceptionCode	ErrUserCanceled	= userCanceledErr;
	const ExceptionCode	ErrMemFull		= memFullErr;
#elif ACPL_LINUX
	const ExceptionCode	ErrFileNotFound	= 'fnfE';
	const ExceptionCode	ErrFileCreation = 'fcrE';
	const ExceptionCode	ErrDirCreation	= 'dirE';
	const ExceptionCode	ErrEndOfFile	= 'eofE';
	const ExceptionCode	ErrDiskFull		= 'dfuE';
	const ExceptionCode	ErrFilePostion	= 'posE';
	const ExceptionCode	ErrUserCanceled	= 'uscE';
	const ExceptionCode	ErrMemFull		= 'mfuE';
#elif ACPL_WIN
	const ExceptionCode	ErrFileNotFound	= ERROR_FILE_NOT_FOUND;
	const ExceptionCode	ErrEndOfFile	= ERROR_HANDLE_EOF;
	const ExceptionCode	ErrDiskFull		= ERROR_HANDLE_DISK_FULL;
	const ExceptionCode	ErrFilePostion	= ERROR_NEGATIVE_SEEK;
	const ExceptionCode	ErrUserCanceled	= ERROR_CANCELLED;
	const ExceptionCode	ErrMemFull		= ERROR_OUTOFMEMORY;
#endif

	const ExceptionCode	ErrAssertFailed	= 0x41535254;	//	'ASRT'
	const ExceptionCode	ErrFatal		= 0x43524153;	//	'CRAS'
	const ExceptionCode	NoError			= 0;

}
