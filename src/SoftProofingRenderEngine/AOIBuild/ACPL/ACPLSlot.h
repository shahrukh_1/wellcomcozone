#pragma once

#include "./ACPLSignalSlotBase.h"

namespace aur { namespace ACPL {

/*!	\brief	Class representing a slot

Template class representing a definition of a Slot with an instance type (in case the Slot points to an instance method)
and maximum 10 method arguments.
In case the instance type is not specified or marked as Undefined, the Slot will be assumed to point to a static method.

\sa Signal, SignalSlotBase
*/
template <
			class InstType = Undefined,
			class Arg01Type = Undefined,
			class Arg02Type = Undefined,
			class Arg03Type = Undefined,
			class Arg04Type = Undefined,
			class Arg05Type = Undefined,
			class Arg06Type = Undefined,
			class Arg07Type = Undefined,
			class Arg08Type = Undefined,
			class Arg09Type = Undefined,
			class Arg10Type = Undefined
		>
class Slot : public SignalSlotBaseTemplate<Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type>
{
	// Instance method types
	typedef void (InstType::*FunctionI00)();
	typedef void (InstType::*FunctionI01)(Arg01Type);
	typedef void (InstType::*FunctionI02)(Arg01Type, Arg02Type);
	typedef void (InstType::*FunctionI03)(Arg01Type, Arg02Type, Arg03Type);
	typedef void (InstType::*FunctionI04)(Arg01Type, Arg02Type, Arg03Type, Arg04Type);
	typedef void (InstType::*FunctionI05)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type);
	typedef void (InstType::*FunctionI06)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type);
	typedef void (InstType::*FunctionI07)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type);
	typedef void (InstType::*FunctionI08)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type);
	typedef void (InstType::*FunctionI09)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type);
	typedef void (InstType::*FunctionI10)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type);
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg10Type), FunctionI09, FunctionI10)		FunctionIX10;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg09Type), FunctionI08, FunctionIX10)	FunctionIX09;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg08Type), FunctionI07, FunctionIX09)	FunctionIX08;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg07Type), FunctionI06, FunctionIX08)	FunctionIX07;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg06Type), FunctionI05, FunctionIX07)	FunctionIX06;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg05Type), FunctionI04, FunctionIX06)	FunctionIX05;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg04Type), FunctionI03, FunctionIX05)	FunctionIX04;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg03Type), FunctionI02, FunctionIX04)	FunctionIX03;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg02Type), FunctionI01, FunctionIX03)	FunctionIX02;

	// Static method types
	typedef void (*FunctionS00)();
	typedef void (*FunctionS01)(Arg01Type);
	typedef void (*FunctionS02)(Arg01Type, Arg02Type);
	typedef void (*FunctionS03)(Arg01Type, Arg02Type, Arg03Type);
	typedef void (*FunctionS04)(Arg01Type, Arg02Type, Arg03Type, Arg04Type);
	typedef void (*FunctionS05)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type);
	typedef void (*FunctionS06)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type);
	typedef void (*FunctionS07)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type);
	typedef void (*FunctionS08)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type);
	typedef void (*FunctionS09)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type);
	typedef void (*FunctionS10)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type);
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg10Type), FunctionS09, FunctionS10)		FunctionSX10;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg09Type), FunctionS08, FunctionSX10)	FunctionSX09;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg08Type), FunctionS07, FunctionSX09)	FunctionSX08;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg07Type), FunctionS06, FunctionSX08)	FunctionSX07;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg06Type), FunctionS05, FunctionSX07)	FunctionSX06;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg05Type), FunctionS04, FunctionSX06)	FunctionSX05;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg04Type), FunctionS03, FunctionSX05)	FunctionSX04;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg03Type), FunctionS02, FunctionSX04)	FunctionSX03;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg02Type), FunctionS01, FunctionSX03)	FunctionSX02;

	typedef InstanceCallback
			<
				InstType,
				Arg01Type,
				Arg02Type,
				Arg03Type,
				Arg04Type,
				Arg05Type,
				Arg06Type,
				Arg07Type,
				Arg08Type,
				Arg09Type,
				Arg10Type
			>
		InstanceCallbackType;

	typedef StaticCallback
			<
				Arg01Type,
				Arg02Type,
				Arg03Type,
				Arg04Type,
				Arg05Type,
				Arg06Type,
				Arg07Type,
				Arg08Type,
				Arg09Type,
				Arg10Type
			>
		StaticCallbackType;

	typedef SignalSlotBaseTemplate
			<
				Arg01Type,
				Arg02Type,
				Arg03Type,
				Arg04Type,
				Arg05Type,
				Arg06Type,
				Arg07Type,
				Arg08Type,
				Arg09Type,
				Arg10Type
			>
		SignalSlotBaseTemplateType;
	typedef Array<SignalSlotBaseStandard*> SignalSlotList;

	/*!	\brief	A type of any instance method with a variable number of parameters until 10
	*/
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg01Type), FunctionI00, FunctionIX02)	InstanceMethod;

	/*!	\brief	A type of any static method with a variable number of parameters until 10
	*/
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg01Type), FunctionS00, FunctionSX02)	StaticMethod;


	// Members
	InstanceCallbackType	mInstanceCallback;	//!<	The contained instance callback (InstType must not be Undefined).
	StaticCallbackType		mStaticCallback;	//!<	The contained static callback (InsType must be Undefined).
			bool			mInstanceBased;		//!<	Internal determination of the type of Slot (instance or static based).

public:
	// Constructor
							/*!	\brief	Default constructor
							*/
							Slot()
								: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSlot)
							{
								Init();
							}

							/*!	\brief	Constructor from signal, class instance and instance method

							Assumes this will be an instance slot.

							\param[in]	signal				The signal to connect to
							\param[in]	classInstance		The class instance that holds the instance method
							\param[in]	instanceMethod		The instance method that will be executed
							\param[in]	connectionType		Specifies whether synchronous, asynchronous or blocking asynchronous connection should be made
							\return		void
							*/
							Slot(SignalSlotBaseStandard* signal, InstType* classInstance, InstanceMethod instanceMethod, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
								: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSlot)
							{
								Init();
								Connect(signal, classInstance, instanceMethod, connectionType);
							}

							Slot(SignalSlotBaseStandard& signal, InstType* classInstance, InstanceMethod instanceMethod, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
								: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSlot)
							{
								Init();
								Connect(&signal, classInstance, instanceMethod, connectionType);
							}

							/*!	\brief	Constructor from signal and static method

							Assumes this will be a static slot.

							\param[in]	signal			The signal to connect to
							\param[in]	staticMethod	The static method that will be executed
							\return		void
							*/
							Slot(SignalSlotBaseStandard* signal, StaticMethod staticMethod)
								: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSlot)
							{
								Init();
								Connect(signal, staticMethod);
							}

							Slot(SignalSlotBaseStandard& signal, StaticMethod staticMethod)
								: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSlot)
							{
								Init();
								Connect(&signal, staticMethod);
							}

			virtual			~Slot();

	// Methods
			/*!	\brief	Connect to a signal

			This method connects to a signal.

			\param[in]	signal				The signal to connect to
			\param[in]	connectionType		Specifies whether synchronous, asynchronous or blocking asynchronous connection should be made
			\return		void
			*/
			void			Connect(SignalSlotBaseStandard* signal, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
			{
				SignalSlotBaseTemplateType::ConnectEntity(signal, connectionType);
			}

			void			Connect(SignalSlotBaseStandard& signal, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
			{
				SignalSlotBaseTemplateType::ConnectEntity(&signal, connectionType);
			}

			/*!	\brief	Connect to a signal as QT/Boost style

			This method connects to a signal as QT/Boost style connection (specifying the signal/class instance/instance method).

			\param[in]	signal				The signal to connect to
			\param[in]	classInstance		The class instance that holds the instance method
			\param[in]	instanceMethod		The instance method that will be executed
			\param[in]	connectionType		Specifies whether synchronous, asynchronous or blocking asynchronous connection should be made
			\return		void
			*/
			void			Connect(SignalSlotBaseStandard* signal, InstType* classInstance, InstanceMethod instanceMethod, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
			{
				if(!mInstanceBased)
					return;

				if( !SignalSlotBaseTemplateType::ConnectEntity(signal, connectionType) )
					return;

				SetInstanceMethod(classInstance, instanceMethod);
			}

			void			Connect(SignalSlotBaseStandard& signal, InstType* classInstance, InstanceMethod instanceMethod, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
			{
				if(!mInstanceBased)
					return;

				if( !SignalSlotBaseTemplateType::ConnectEntity(&signal, connectionType) )
					return;

				SetInstanceMethod(classInstance, instanceMethod);
			}

			/*!	\brief	Connect to a signal as QT/Boost style

			This method connects to a signal as QT/Boost style connection (specifying the signal/static method).

			\param[in]	signal			The signal to connect to
			\param[in]	staticMethod	The static method that will be executed
			\return		void
			*/
			void			Connect(SignalSlotBaseStandard* signal, StaticMethod staticMethod)
			{
				if(mInstanceBased)
					return;

				if( !SignalSlotBaseTemplateType::ConnectEntity(signal) )
					return;

				SetStaticMethod(staticMethod);
			}

			void			Connect(SignalSlotBaseStandard& signal, StaticMethod staticMethod)
			{
				if(mInstanceBased)
					return;

				if( !SignalSlotBaseTemplateType::ConnectEntity(&signal) )
					return;

				SetStaticMethod(staticMethod);
			}

			/*!	\brief	Sets the class instance and instance method

			Assumes to be an instance based Slot.

			\param[in]	classInstance	The class instance that holds the instance method
			\param[in]	instanceMethod	The instance method that will be called
			\return		void
			*/
			void			SetInstanceMethod(InstType* classInstance, InstanceMethod instanceMethod)
			{
				if(!mInstanceBased)
					return;

				this->mEntitiesSynchronize.Lock();
				mInstanceCallback.SetMethod(classInstance, instanceMethod);
				this->mEntitiesSynchronize.Unlock();
			}

			/*!	\brief	Sets the static method

			Assumes to be a static based Slot.

			\param[in]	staticMethod	The static method that will be called
			\return		void
			*/
			void			SetStaticMethod(StaticMethod staticMethod)
			{
				if(mInstanceBased)
					return;

				this->mEntitiesSynchronize.Lock();
				mStaticCallback.SetMethod(staticMethod);
				this->mEntitiesSynchronize.Unlock();
			}

protected:
			void			Init();

	virtual	void			DisconnectEntity(SignalSlotBaseStandard* entity)
	{
		SignalSlotBaseTemplateType::DisconnectEntity(entity);
		if(this->mCounterAsyncEntities.GetCount() == 0 && this->mCounterBlockingAsyncEntities.GetCount() == 0)
			mInstanceCallback.CancelPendingExecution();
	}

			void			Emit()
			{
				if(mInstanceBased)
					mInstanceCallback.Execute();
				else
					mStaticCallback.Execute();
			}

			void			Emit(Arg01Type arg01)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01);
				else
					mStaticCallback.Execute(arg01);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02);
				else
					mStaticCallback.Execute(arg01, arg02);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03);
				else
					mStaticCallback.Execute(arg01, arg02, arg03);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04, arg05);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
			}

			void			Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
			{
				if(mInstanceBased)
					mInstanceCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
			}

			int32_t			FinalizeEmitAsync(void* classInstance, int32_t countPendingsToProcess)
			{
				if(mInstanceBased && (mInstanceCallback.AsyncExecutionPending() || mInstanceCallback.BlockingAsyncExecutionPending()))
				{
					if(classInstance == NULL)
						return mInstanceCallback.FinalizeAsyncCalls(countPendingsToProcess);
					if(mInstanceCallback.GetClassInstance() != NULL && mInstanceCallback.GetClassInstance() == classInstance)
						return mInstanceCallback.FinalizeAsyncCalls(countPendingsToProcess);
				}
				return 0;
			}

			void			EmitAsync()
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync();
				else
					mStaticCallback.Execute();
			}

			void			EmitAsync(Arg01Type arg01)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01);
				else
					mStaticCallback.Execute(arg01);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02);
				else
					mStaticCallback.Execute(arg01, arg02);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03);
				else
					mStaticCallback.Execute(arg01, arg02, arg03);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04, arg05);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04, arg05, arg06);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
			}

			void			EmitAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
			}

			void			EmitBlockingAsync()
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync();
				else
					mStaticCallback.Execute();
			}

			void			EmitBlockingAsync(Arg01Type arg01)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01);
				else
					mStaticCallback.Execute(arg01);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02);
				else
					mStaticCallback.Execute(arg01, arg02);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03);
				else
					mStaticCallback.Execute(arg01, arg02, arg03);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04, arg05);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
			}

			void			EmitBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
			{
				if(mInstanceBased)
					mInstanceCallback.ExecuteBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
				else
					mStaticCallback.Execute(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
			}
};



/*!	\brief	Repository for all existing slots in the application

This class, which is implemented using the singleton model, is used to perform thread safe execution of slots,
as it is a repository for all existing slots in the application.

\sa Slot
*/
class GlobalSlotList
{
	template <
				class InstType,
				class Arg01Type,
				class Arg02Type,
				class Arg03Type,
				class Arg04Type,
				class Arg05Type,
				class Arg06Type,
				class Arg07Type,
				class Arg08Type,
				class Arg09Type,
				class Arg10Type
			>
			friend class Slot;

protected:
	typedef Array<SignalSlotBaseStandard*> SlotList;
	static ACPLAPI GlobalSlotList*	sGlobalSlotList;
			SlotList		mSlots;
			Semaphore		mSlotsSynchronize;

			inline			GlobalSlotList() {}

			inline			~GlobalSlotList()
							{
								mSlotsSynchronize.Lock();
								mSlots.Empty();
								mSlotsSynchronize.Unlock();
							}

			inline void	AddSlot(SignalSlotBaseStandard* slot)
			{
				if(!slot)
					return;
				if(slot->mEntityType != SignalSlotBaseStandard::eSlot)
					return;

				mSlotsSynchronize.Lock();
				for( uint32_t i = 0; i != mSlots.GetCount(); ++i )
					if( mSlots[i] == slot )
					{
						mSlotsSynchronize.Unlock();
						return;
					}
				mSlots.Append(slot);
				mSlotsSynchronize.Unlock();
			}

			inline void	RemoveSlot(SignalSlotBaseStandard* slot)
			{
				if(!slot)
					return;
				if(slot->mEntityType != SignalSlotBaseStandard::eSlot)
					return;

				mSlotsSynchronize.Lock();
				uint32_t i = 0;
				while( i != mSlots.GetCount() )
				{
					if( mSlots[i] == slot )
						mSlots.Remove( i );
					else
						++i;
				}
				mSlotsSynchronize.Unlock();
			}

public:
			/*!	\brief	Getter for the singleton instance of this class
			*/
			inline static GlobalSlotList*	Get()
			{
				if( sGlobalSlotList == NULL)
					sGlobalSlotList = new GlobalSlotList();

				return sGlobalSlotList;
			}
			/*!	\brief	dtor for the singleton instance of this class
			*/
			inline static void Dispose()
			{
				delete sGlobalSlotList;
				sGlobalSlotList = NULL;
			}

			/*!	\brief	Processes execution of slots

			Useful for finalizing the execution of thread safe slots, as it should be called inside an
			event loop based mechanism in the desired thread.

			\param[in]	countEventsToProcess	The number of events to process in this call.
												-1 = process of all pending events
												>= 0 = explicit count of events to be processed
			\return		void
			*/
			inline	void	ProcessEvents(int32_t countEventsToProcess = -1)
			{
				ProcessEvents(NULL, countEventsToProcess);
			}

			/*!	\brief	Processes execution of slots that have a particular class instance reference

			Useful for finalizing the execution of thread safe slots, as it should be called inside an
			event loop based mechanism in the desired thread.

			\param[in]	classInstance			The desired class instance
			\param[in]	countEventsToProcess	The number of events to process in this call.
												-1 = process of all pending events
												>= 0 = explicit count of events to be processed
			\return		void
			*/
			inline	void	ProcessEvents(void* classInstance, int32_t countEventsToProcess = -1)
			{
				if(countEventsToProcess < -1)
					return;

				mSlotsSynchronize.Lock();
				if(countEventsToProcess == -1)		// process all events of the specified class instance
				{
					for(uint32_t i = 0; i < mSlots.GetCount(); i ++)
						mSlots[i]->FinalizeEmitAsync(classInstance, -1);
				}
				else if(countEventsToProcess > 0)	// process the requested number of events of the specified class instance
				{
					uint64_t processedEventsCount = 0;
					for(uint32_t i = 0; i < mSlots.GetCount(); i ++)
					{
						if(processedEventsCount >= uint64_t( countEventsToProcess ) )
							break;
						processedEventsCount += (uint64_t)mSlots[i]->FinalizeEmitAsync(classInstance, countEventsToProcess - (int32_t)processedEventsCount);
					}
				}
				mSlotsSynchronize.Unlock();
			}
};

template
	<
		class InstType,
		class Arg01Type,
		class Arg02Type,
		class Arg03Type,
		class Arg04Type,
		class Arg05Type,
		class Arg06Type,
		class Arg07Type,
		class Arg08Type,
		class Arg09Type,
		class Arg10Type
	>
Slot<
		InstType,
		Arg01Type,
		Arg02Type,
		Arg03Type,
		Arg04Type,
		Arg05Type,
		Arg06Type,
		Arg07Type,
		Arg08Type,
		Arg09Type,
		Arg10Type
	>::~Slot()
{
	GlobalSlotList::Get()->RemoveSlot(this);
	mInstanceCallback.CancelPendingExecution();
}

template
	<
		class InstType,
		class Arg01Type,
		class Arg02Type,
		class Arg03Type,
		class Arg04Type,
		class Arg05Type,
		class Arg06Type,
		class Arg07Type,
		class Arg08Type,
		class Arg09Type,
		class Arg10Type
	>
void Slot
	<
		InstType,
		Arg01Type,
		Arg02Type,
		Arg03Type,
		Arg04Type,
		Arg05Type,
		Arg06Type,
		Arg07Type,
		Arg08Type,
		Arg09Type,
		Arg10Type
	>::Init()
{
	GlobalSlotList::Get()->AddSlot(this);
	mInstanceBased = !IS_UNDEFINED(InstType);
}

}}
