/*
 * FlateStream
 * Copyright (c) 2002-2012 Aurelon. All rights reserved.
 *
 * Class for reading/writing an flate compressed ordered sequence of bytes to a file
 *
 */

#pragma once

#include "./FileStream.h"

typedef struct z_stream_s z_stream;

namespace aur { namespace ACPL {

	class ACPLAPI FlateStream : public Stream
	{
		Stream*		mOutStream;
		Permission	mPermission;
		bool		mOwnsFileStream;

		uint8_t*	mCompr;
		uint8_t*	mUncompr;
		uint32_t	mBufLen;
		::z_stream* mStream;
	public:
								FlateStream( Permission, int = 1 /* Z_BEST_SPEED */ );
								FlateStream( Stream*, Permission, int = 1 /* Z_BEST_SPEED */ );
								FlateStream( const FileSpec&, Permission, int = 1 /* Z_BEST_SPEED */ );
		virtual					~FlateStream();
		virtual void			SetMarker( int64_t, StreamFrom );
		virtual int64_t			GetMarker() const;
		virtual void			SetLength( int64_t );
		virtual int64_t			GetLength() const;
		virtual ExceptionCode	GetBytes( void *outBuffer, int32_t &ioByteCount );
		virtual ExceptionCode	PutBytes( const void *inBuffer, int32_t &ioByteCount );
	private:
				void			Init( int );
	};

}}
