// ===========================================================================
//	HandleStream.h	(c) 1998-2012 Aurelon. All rights reserved.
// ===========================================================================
//
//	A Stream whose bytes are in a Handle block in memory

#pragma once

#include "./Stream.h"

#if ACPL_MAC

namespace aur { namespace ACPL {

	class ACPLAPI HandleStream : public Stream
	{
		Handle					mDataH;
	public:
								HandleStream();
								HandleStream( Handle );
		virtual					~HandleStream();

		virtual void			SetLength( int64_t );

		virtual ExceptionCode	PutBytes( const void*, int32_t& );
		virtual ExceptionCode	GetBytes( void*, int32_t& );

		void					SetDataHandle( Handle );
		inline	Handle			GetDataHandle() const { return mDataH; }
		Handle					DetachDataHandle();
	};

}}

#endif
