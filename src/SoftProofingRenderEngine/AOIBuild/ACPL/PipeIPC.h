#pragma once

#include "IPC.h"

namespace aur { namespace ACPL {
	class ACPLAPI PipeIPC : public IPC, public Thread
	{
	public:
								PipeIPC();
		virtual					~PipeIPC();

		/*!
		 * do not call Start(), the thread will be started inside Connect(), if a connection is made.
		 */
		virtual bool			Connect( const IPC::ConnectData* connection );
		virtual bool			Send( IPC::Message* message );

		/*!
		 * Disconnect() calls Stop() and waits for the thread to terminate.
		 * Avoid calling Disconnect() from slots connected ReceiveSignal.
		 * If the signal is synchronous, Disconnect() will be called from the PipeIPC thread and will block the pipe.
		 *
		 * \sa LayoutHandler::OnReceiveMessage()
		 */
		virtual bool			Disconnect();
	
	protected:
		virtual void			ThreadMain();

	private:
#if ACPL_MAC
		bool					CreateBidirectionalPipes();
#endif
		bool					CreatePipe();
		bool					CreateFile();
		MemoryStream*			ReceiveMessage();
		bool					SendMessage( char* message, int64_t len );
		bool					ConnectNamedPipe();
		void					Receive();

	private:
		IPC::ConnectData		mConnectionData;
		volatile bool			mConnected;
		ACPL::String			mServerName;
		ACPL::String			mClientName;
#if ACPL_WIN
		HANDLE					mPipeServer;
		HANDLE					mPipeClient;
		PSECURITY_DESCRIPTOR	mPipeSD;
		OVERLAPPED				mPipeServerOverlap;
		OVERLAPPED				mPipeClientOverlap;
#else
		int						mPipeServer;
		int						mPipeClient;
#endif
	};
}}
