/*
 * Random
 * Copyright (c) 2013 Aurelon. All rights reserved.
 *
 * Pseudo random to ensure consistent behavior across platforms
 */

#pragma once

#include "./Types.h"

namespace aur { namespace ACPL {

	class ACPLAPI Random
	{
	public:
		static const int	sMax = 0x7FFF;

				Random( int seed = 42 );
		void	srand( int seed );
		int		rand();
	private:
		int		mHistory[55];
		int		mCurrent;
	};

}}
