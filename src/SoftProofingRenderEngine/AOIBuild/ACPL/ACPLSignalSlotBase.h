#pragma once

#include "./Types.h"
#include "./Multithreading.h"
#include "./Array.h"

namespace aur { namespace ACPL {

/*!	\brief	Determines whether type T is undefined (Undefined) or not

Macro used to determine whether type T is an undefined type. In a list of default parameters (either to methods or class
template), if a parameter is not mentioned it can be assumed to be undefined.

\param[in]	T	Any type/class
\return			Boolean representing whether type T is an undefined type
\sa				SignalSlotBase, InstanceCallback, StaticCallback, Slot
*/
#define IS_UNDEFINED(T)	IsUndefined<T>::Value

/*!	\brief	Determines whether type T is defined or not (Undefined)

Macro used to determine whether type T is an defined type or not, by returning an int (1 or 0). This macro
is useful when needing to count in a variable list of arguments, how many are defined.

\param[in]	T	Any type/class
\return			Integer representing whether type T is a defined type
\sa				SignalSlotBase, InstanceCallback, StaticCallback, Slot
*/
#define COUNT_DEFINED(T)	CountDefined<T>::Value

/*!	\brief	Determines whether type T is a pointer

Macro used to determine whether type T is a pointer type.

\param[in]	T	Any type/class
\return			Boolean representing whether type T is a pointer type
\sa				InstanceCallback, StaticCallback
*/
#define IS_POINTER(T)	IsPointer<T>::Value

/*!	\brief	Determines whether type T is a reference type

Macro used to determine whether type T is a reference type.

\param[in]	T	Any type/class
\return			Boolean representing whether type T is a reference type
\sa				InstanceCallback, StaticCallback
*/
#define IS_REFERENCE(T)	IsReference<T>::Value

/*!	\brief	Conditional declaration

Macro used to make a conditional declaration depending on the evaluation of a condition. Determines which type 
to use to declare a future variable.

\param[in]		chooseTypeA	Condition which will be evaluated to a boolean value
\param[in]		TypeA		The first type definition that may be chosen
\param[in]		TypeB		The second type definition that may be chosen
\return			Either typedef TypeA (if the (chooseTypeA) condition evaluates to true) or typedef TypeB (if the
				(chooseTypeA) condition evaluates to false)
\sa				SignalSlotBase, InstanceCallback, StaticCallback, Slot.
*/
#define CHOOSE_TYPE(chooseTypeA, TypeA, TypeB)	ChooseType<chooseTypeA, TypeA, TypeB>::DesiredType

/*!	\brief	Base type declaration

Macro used to make a base type declaration depending on a real type which might be a pointer or reference type.
In case T is "int" or "int*" or "int&" (or any other combination), the macro will declare as "int"

\param[in]		T		The original type definition that must be based
\return		The base type of T, no pointer, nor reference.
\sa				SignalSlotBase, InstanceCallback, StaticCallback, Slot.
*/
#define CHOOSE_BASE_TYPE(T)	ChooseBaseType<T>::DesiredType

/*!	\brief	Storable type declaration

Macro used to make a storable type declaration depending on a real type which might be a pointer or reference type.
In case T is an "int" based type, the macro will declare it as follows:
 - int										=> int
 - any other combination of pure int		=> int
 - int&										=> int*
 - int*										=> int*
 - any other combination of int& or int*	=> int*


\param[in]		T		The original type definition that must be stored
\return		The storable form of T.
\sa				SignalSlotBase, InstanceCallback, StaticCallback, Slot.
*/
#define CHOOSE_STORABLE_TYPE(T)	ChooseStorableType<T>::DesiredType

/*!	\brief	Converts variable between 2 types

Macro used to convert from BaseType to FinalType, where both types are actually the same (or have implemented conversion
operators), but differ in access modifiers. Some examples of the (BaseType, FinalType) pair can be: (int, int*),
(int&, int*), (const int&, int*), etc.

\param[in]		BaseType		The base type to convert the variable from
\param[in]		FinalType		The final type to convert the variable to
\param[in]		var				The variable that needs to be converted
\return		The converted variable. For example if called BASE_TO_FINAL_TYPE(int, int*, var), it will return &var.
\sa				InstanceCallback.
*/
#define BASE_TO_FINAL_TYPE(BaseType, FinalType, var) BaseToFinalType<BaseType, FinalType>::Convert(var)


/*!	\brief	The definition of an undefined (not specified) type

In a list of default parameters (either to methods or class template), if a parameter is not mentioned
it can be assumed to be undefined. Moreover the default values of those parameters should be set to type Undefined

\sa				SignalSlotBase, InstanceCallback, StaticCallback, Slot.
*/
struct Undefined
{

};

template <typename T>
struct IsUndefined
{
	enum
	{
		Value = false
	};
};

template <>
struct IsUndefined<Undefined>
{
	enum
	{
		Value = true
	};
};


template <typename T>
struct CountDefined
{
	enum
	{
		Value = 1
	};
};

template <>
struct CountDefined<Undefined>
{
	enum
	{
		Value = 0
	};
};


template <typename T>
struct IsPointer
{
	enum
	{
		Value = false
	};
};

template <typename T>
struct IsPointer<T*>
{
	enum
	{
		Value = true
	};
};


template <typename T>
struct IsReference
{
	enum
	{
		Value = false
	};
};

template <typename T>
struct IsReference<T&>
{
	enum
	{
		Value = true
	};
};


template <typename T>
struct ChooseBaseType
{
	typedef T DesiredType;
};

template <typename T>
struct ChooseBaseType<T*>
{
	typedef T DesiredType;
};

template <typename T>
struct ChooseBaseType<T&>
{
	typedef T DesiredType;
};


template <bool chooseTypeA, class TypeA, class TypeB>
struct ChooseType
{
	typedef void DesiredType;
};

template <class TypeA, class TypeB>
struct ChooseType<true, TypeA, TypeB>
{
	typedef TypeA DesiredType;
};

template <class TypeA, class TypeB>
struct ChooseType<false, TypeA, TypeB>
{
	typedef TypeB DesiredType;
};


template <typename T>
struct ChooseStorableType
{
	typedef T DesiredType;
};

template <>
struct ChooseStorableType<Undefined>
{
	typedef ChooseBaseType<Undefined>::DesiredType* DesiredType;
};

template <typename T>
struct ChooseStorableType<T*>
{
	typedef typename ChooseBaseType<T>::DesiredType* DesiredType;
};

template <typename T>
struct ChooseStorableType<T&>
{
	typedef typename ChooseBaseType<T>::DesiredType* DesiredType;
};


template <class BaseType, class FinalType>
struct BaseToFinalType
{
	static FinalType& Convert(BaseType& param)
	{
		return param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<BaseType*, FinalType>
{
	static FinalType& Convert(BaseType*& param)
	{
		return *param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<BaseType, FinalType*>
{
	static FinalType* Convert(BaseType& param)
	{
		return &param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<BaseType*, FinalType*>
{
	static FinalType* Convert(BaseType*& param)
	{
		return param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<BaseType&, FinalType*>
{
	static FinalType* Convert(BaseType& param)
	{
		return &param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<BaseType*, FinalType&>
{
	static FinalType& Convert(BaseType*& param)
	{
		return *param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<const BaseType, FinalType>
{
	static FinalType& Convert(const BaseType& param)
	{
		return *((FinalType*)&param);
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<const BaseType, FinalType*>
{
	static FinalType* Convert(const BaseType& param)
	{
		return (FinalType*)&param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<BaseType*, const FinalType>
{
	static const FinalType& Convert(BaseType*& param)
	{
		return *param;
	}
};

template <class BaseType, class FinalType>
struct BaseToFinalType<const BaseType, FinalType&>
{
	static FinalType& Convert(const BaseType& param)
	{
		return *((FinalType*)&param);
	}
};


template <class T>
struct InitializeFinalVariable
{
	static void Initialize( T& /* var */ )
	{

	}
};

template <class T>
struct InitializeFinalVariable<T*>
{
	static void Initialize(T*& var)
	{
		var = NULL;
	}
};

class ThreadSynchronizer
{
		Semaphore*		mSemaphore;
	volatile	int8_t	mLockedCount;

public:
						ThreadSynchronizer()
						{
							mSemaphore = NULL;
							mLockedCount = 0;
						}

		virtual			~ThreadSynchronizer()
		{
			if(mSemaphore)
				while(mLockedCount > 0)
				{
					mLockedCount --;
					mSemaphore->Unlock();
				}
			delete mSemaphore;
		}

		void			Block()
		{
			if(mSemaphore && IsBlocked())
				return;

			if(!mSemaphore)
				mSemaphore = new Semaphore();

			if(mLockedCount == 0)
			{
				mLockedCount ++;
				if(!mSemaphore->Lock())
					mLockedCount --;
			}
			if(mLockedCount == 1)
			{
				mLockedCount ++;
				if(!mSemaphore->Lock())
					mLockedCount --;
			}
		}

		void			Release()
		{
			if(!mSemaphore)
				return;
			if(mSemaphore && !IsBlocked())
				return;

			mLockedCount --;
			mSemaphore->Unlock();
		}

	bool	IsBlocked() const
	{
		return mLockedCount > 1;
	}
};

template
	<
		class Arg01Type,
		class Arg02Type,
		class Arg03Type,
		class Arg04Type,
		class Arg05Type,
		class Arg06Type,
		class Arg07Type,
		class Arg08Type,
		class Arg09Type,
		class Arg10Type
	>
class Signal;

template 
	<
		class InstType,
		class Arg01Type,
		class Arg02Type,
		class Arg03Type,
		class Arg04Type,
		class Arg05Type,
		class Arg06Type,
		class Arg07Type,
		class Arg08Type,
		class Arg09Type,
		class Arg10Type
	>
class Slot;

/*!	\brief	A callback that is pointing to a member method of a class instance

Template definition of a callback that is pointing to a member method of a class instance, specified by the first template
parameter type (InstType).
The maximum number of method parameters is 10.
The following template parameters represent the types of each variable that the class instance method has. The template
parameters are default parameters, assuming that if they are not specified, type Undefined will be considered.

\sa	StaticCallback
*/
template <
			class InstType,
			class Arg01Type = Undefined,
			class Arg02Type = Undefined,
			class Arg03Type = Undefined,
			class Arg04Type = Undefined,
			class Arg05Type = Undefined,
			class Arg06Type = Undefined,
			class Arg07Type = Undefined,
			class Arg08Type = Undefined,
			class Arg09Type = Undefined,
			class Arg10Type = Undefined
		>
class InstanceCallback
{
	friend class Slot
		<
			InstType,
			Arg01Type,
			Arg02Type,
			Arg03Type,
			Arg04Type,
			Arg05Type,
			Arg06Type,
			Arg07Type,
			Arg08Type,
			Arg09Type,
			Arg10Type
		>;

	// Types
private:
	typedef void (InstType::*FunctionI00)();
	typedef void (InstType::*FunctionI01)(Arg01Type);
	typedef void (InstType::*FunctionI02)(Arg01Type, Arg02Type);
	typedef void (InstType::*FunctionI03)(Arg01Type, Arg02Type, Arg03Type);
	typedef void (InstType::*FunctionI04)(Arg01Type, Arg02Type, Arg03Type, Arg04Type);
	typedef void (InstType::*FunctionI05)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type);
	typedef void (InstType::*FunctionI06)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type);
	typedef void (InstType::*FunctionI07)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type);
	typedef void (InstType::*FunctionI08)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type);
	typedef void (InstType::*FunctionI09)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type);
	typedef void (InstType::*FunctionI10)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type);
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg10Type), FunctionI09, FunctionI10)		FunctionIX10;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg09Type), FunctionI08, FunctionIX10)	FunctionIX09;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg08Type), FunctionI07, FunctionIX09)	FunctionIX08;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg07Type), FunctionI06, FunctionIX08)	FunctionIX07;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg06Type), FunctionI05, FunctionIX07)	FunctionIX06;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg05Type), FunctionI04, FunctionIX06)	FunctionIX05;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg04Type), FunctionI03, FunctionIX05)	FunctionIX04;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg03Type), FunctionI02, FunctionIX04)	FunctionIX03;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg02Type), FunctionI01, FunctionIX03)	FunctionIX02;

	typedef typename CHOOSE_STORABLE_TYPE(Arg01Type)	Arg01StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg02Type)	Arg02StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg03Type)	Arg03StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg04Type)	Arg04StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg05Type)	Arg05StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg06Type)	Arg06StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg07Type)	Arg07StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg08Type)	Arg08StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg09Type)	Arg09StorableType;
	typedef typename CHOOSE_STORABLE_TYPE(Arg10Type)	Arg10StorableType;

	typedef typename CHOOSE_BASE_TYPE(InstType)			InstBaseType;

	typedef InstanceCallback
			<
				InstType,
				Arg01Type,
				Arg02Type,
				Arg03Type,
				Arg04Type,
				Arg05Type,
				Arg06Type,
				Arg07Type,
				Arg08Type,
				Arg09Type,
				Arg10Type
			>
		InstanceCallbackType;
	typedef Array<InstanceCallbackType*>	PendingCallList;

public:
	/*!	\brief	A type of any instance method with a variable number of parameters until 10
	*/
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg01Type), FunctionI00, FunctionIX02)	Method;

private:
	union FunctionI
	{
		FunctionI00 f00;
		FunctionI01 f01;
		FunctionI02 f02;
		FunctionI03 f03;
		FunctionI04 f04;
		FunctionI05 f05;
		FunctionI06 f06;
		FunctionI07 f07;
		FunctionI08 f08;
		FunctionI09 f09;
		FunctionI10 f10;
	};

	union FunctionA
	{
		Method f;
	};

	// Members
		InstType*		mClassInstance;				//!<	The contained class instance.
		Method			mInstanceMethod;			//!<	The contained instance method.
		FunctionI		mPendingInstanceMethod;		//!<	The pending method that needs to be called.
	Arg01StorableType	mPendingArg01;				//!<	The pending instance method's first argument.
	Arg02StorableType	mPendingArg02;				//!<	The pending instance method's second argument.
	Arg03StorableType	mPendingArg03;				//!<	The pending instance method's third argument.
	Arg04StorableType	mPendingArg04;				//!<	The pending instance method's fourth argument.
	Arg05StorableType	mPendingArg05;				//!<	The pending instance method's fifth argument.
	Arg06StorableType	mPendingArg06;				//!<	The pending instance method's sixth argument.
	Arg07StorableType	mPendingArg07;				//!<	The pending instance method's seventh argument.
	Arg08StorableType	mPendingArg08;				//!<	The pending instance method's eight argument.
	Arg09StorableType	mPendingArg09;				//!<	The pending instance method's ninth argument.
	Arg10StorableType	mPendingArg10;				//!<	The pending instance method's tenth argument.
	PendingCallList		mPendingAsyncCalls;			//!<	List of pending asynchronous calls of this InstanceCallback.
	PendingCallList		mPendingBlockingAsyncCalls;	//!<	List of pending blocking asynchronous calls of this InstanceCallback.
	ThreadSynchronizer	mBlockingAsyncSynchronize;	//!<	Call synchronization between different threads for blocking asynchronous calls.
	Semaphore			mPendingsSynchronize;		//!<	Pending calls synchronization.

	// Constructors
				/*!	\brief	Copy constructor
				*/
				InstanceCallback(InstanceCallbackType* otherInstanceCallback)
				{
					Init();
					if(otherInstanceCallback != NULL)
					{
						mClassInstance = otherInstanceCallback->mClassInstance;
						mInstanceMethod = otherInstanceCallback->mInstanceMethod;
					}
				}
public:
				/*!	\brief	Default constructor
				*/
				InstanceCallback()
				{
					SetMethod(NULL, NULL);
				}

				/*!	\brief	Constructor from class instance and instance method
				*/
				InstanceCallback(InstType* classInstance, Method instanceMethod)
				{
					SetMethod(classInstance, instanceMethod);
				}

	virtual		~InstanceCallback()
				{
					CancelPendingExecution();
				}

	// Methods
	/*!	\brief	Sets the class instance and instance method

	\param[in]	classInstance	The class instance which will be used to call the instanceMethod
	\param[in]	instanceMethod	The class member method that will be called on behalf of classInstance
	\return		void
	*/
	void		SetMethod(InstType* classInstance, Method instanceMethod)
	{
		mClassInstance = classInstance;
		mInstanceMethod = instanceMethod;
		Init();
	}

	/*!	\brief	Returns the class instance

	\return		const InstBaseType*
	*/
	const InstBaseType* GetClassInstance()
	{
		return mClassInstance;
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 0 parameters.
	This method does not guarantee thread safety.

	\return		void
	*/
	void		Execute()
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 1 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 2 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 3 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 4 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 5 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 6 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback.mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 7 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback.mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback.mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 8 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback.mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback.mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback.mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 9 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback.mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback.mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback.mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback.mPendingArg09 = BASE_TO_FINAL_TYPE(Arg09Type, Arg09StorableType, arg09);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 10 parameters.
	This method does not guarantee thread safety.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\param[in]	arg10	Tenth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType instCallback(this);
		instCallback.SavePendingMethod();
		instCallback.mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback.mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback.mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback.mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback.mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback.mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback.mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback.mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback.mPendingArg09 = BASE_TO_FINAL_TYPE(Arg09Type, Arg09StorableType, arg09);
		instCallback.mPendingArg10 = BASE_TO_FINAL_TYPE(Arg10Type, Arg10StorableType, arg10);
		instCallback.ExecutePendingMethod();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 0 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\return		void
	*/
	void		ExecuteAsync()
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 1 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 2 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 3 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 4 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 5 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 6 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 7 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 8 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback->mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 9 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback->mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback->mPendingArg09 = BASE_TO_FINAL_TYPE(Arg09Type, Arg09StorableType, arg09);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously

	Method that executes the contained method, which has 10 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\param[in]	arg10	Tenth parameter
	\return		void
	*/
	void		ExecuteAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback->mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback->mPendingArg09 = BASE_TO_FINAL_TYPE(Arg09Type, Arg09StorableType, arg09);
		instCallback->mPendingArg10 = BASE_TO_FINAL_TYPE(Arg10Type, Arg10StorableType, arg10);
		mPendingsSynchronize.Lock();
		mPendingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 0 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\return		void
	*/
	void		ExecuteBlockingAsync()
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 1 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 2 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 3 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 4 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 5 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 6 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 7 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 8 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback->mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 9 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback->mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback->mPendingArg09 = BASE_TO_FINAL_TYPE(Arg09Type, Arg09StorableType, arg09);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

	/*!	\brief	Executes the contained method asynchronously, blocking the calling thread

	Method that executes the contained method, which has 10 parameters.
	This method guarantees thread safety, if GlobalSlotList::ProcessEvents will be called from another thread, inside its
	event loop based mechanism.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\param[in]	arg10	Tenth parameter
	\return		void
	*/
	void		ExecuteBlockingAsync(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
	{
		if(!mInstanceMethod || !mClassInstance)
			return;

		InstanceCallbackType* instCallback = new InstanceCallbackType(this);
		instCallback->SavePendingMethod();
		instCallback->mPendingArg01 = BASE_TO_FINAL_TYPE(Arg01Type, Arg01StorableType, arg01);
		instCallback->mPendingArg02 = BASE_TO_FINAL_TYPE(Arg02Type, Arg02StorableType, arg02);
		instCallback->mPendingArg03 = BASE_TO_FINAL_TYPE(Arg03Type, Arg03StorableType, arg03);
		instCallback->mPendingArg04 = BASE_TO_FINAL_TYPE(Arg04Type, Arg04StorableType, arg04);
		instCallback->mPendingArg05 = BASE_TO_FINAL_TYPE(Arg05Type, Arg05StorableType, arg05);
		instCallback->mPendingArg06 = BASE_TO_FINAL_TYPE(Arg06Type, Arg06StorableType, arg06);
		instCallback->mPendingArg07 = BASE_TO_FINAL_TYPE(Arg07Type, Arg07StorableType, arg07);
		instCallback->mPendingArg08 = BASE_TO_FINAL_TYPE(Arg08Type, Arg08StorableType, arg08);
		instCallback->mPendingArg09 = BASE_TO_FINAL_TYPE(Arg09Type, Arg09StorableType, arg09);
		instCallback->mPendingArg10 = BASE_TO_FINAL_TYPE(Arg10Type, Arg10StorableType, arg10);
		mPendingsSynchronize.Lock();
		mPendingBlockingAsyncCalls.Append(instCallback);
		mPendingsSynchronize.Unlock();
		instCallback->mBlockingAsyncSynchronize.Block();
		delete instCallback;
	}

private:
	void		InitPendings()
	{
		InitializeFinalVariable<Arg01StorableType>::Initialize(mPendingArg01);
		InitializeFinalVariable<Arg02StorableType>::Initialize(mPendingArg02);
		InitializeFinalVariable<Arg03StorableType>::Initialize(mPendingArg03);
		InitializeFinalVariable<Arg04StorableType>::Initialize(mPendingArg04);
		InitializeFinalVariable<Arg05StorableType>::Initialize(mPendingArg05);
		InitializeFinalVariable<Arg06StorableType>::Initialize(mPendingArg06);
		InitializeFinalVariable<Arg07StorableType>::Initialize(mPendingArg07);
		InitializeFinalVariable<Arg08StorableType>::Initialize(mPendingArg08);
		InitializeFinalVariable<Arg09StorableType>::Initialize(mPendingArg09);
		InitializeFinalVariable<Arg10StorableType>::Initialize(mPendingArg10);
		mPendingInstanceMethod.f00 = NULL;
		mPendingInstanceMethod.f01 = NULL;
		mPendingInstanceMethod.f02 = NULL;
		mPendingInstanceMethod.f03 = NULL;
		mPendingInstanceMethod.f04 = NULL;
		mPendingInstanceMethod.f05 = NULL;
		mPendingInstanceMethod.f06 = NULL;
		mPendingInstanceMethod.f07 = NULL;
		mPendingInstanceMethod.f08 = NULL;
		mPendingInstanceMethod.f09 = NULL;
		mPendingInstanceMethod.f10 = NULL;
	}

	void		Init()
	{
		InitPendings();
	}

	void		SavePendingMethod()
	{
		FunctionA abstractMethod;
		abstractMethod.f = mInstanceMethod;
		::memcpy(&mPendingInstanceMethod, &abstractMethod, sizeof(mPendingInstanceMethod));
	}

	void		ExecutePendingMethod()
	{
		if(mClassInstance == NULL)
			return;
		if(mPendingInstanceMethod.f00 == NULL)
			return;

		uint8_t argsCount = COUNT_DEFINED(Arg01Type);
		argsCount += COUNT_DEFINED(Arg02Type);
		argsCount += COUNT_DEFINED(Arg03Type);
		argsCount += COUNT_DEFINED(Arg04Type);
		argsCount += COUNT_DEFINED(Arg05Type);
		argsCount += COUNT_DEFINED(Arg06Type);
		argsCount += COUNT_DEFINED(Arg07Type);
		argsCount += COUNT_DEFINED(Arg08Type);
		argsCount += COUNT_DEFINED(Arg09Type);
		argsCount += COUNT_DEFINED(Arg10Type);

		if(argsCount == 0)
			(mClassInstance->*mPendingInstanceMethod.f00)();
		else if(argsCount == 1)
			(mClassInstance->*mPendingInstanceMethod.f01)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01)
			);
		else if(argsCount == 2)
			(mClassInstance->*mPendingInstanceMethod.f02)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02)
			);
		else if(argsCount == 3)
			(mClassInstance->*mPendingInstanceMethod.f03)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03)
			);
		else if(argsCount == 4)
			(mClassInstance->*mPendingInstanceMethod.f04)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04)
			);
		else if(argsCount == 5)
			(mClassInstance->*mPendingInstanceMethod.f05)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04),
				BASE_TO_FINAL_TYPE(Arg05StorableType, Arg05Type, mPendingArg05)
			);
		else if(argsCount == 6)
			(mClassInstance->*mPendingInstanceMethod.f06)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04),
				BASE_TO_FINAL_TYPE(Arg05StorableType, Arg05Type, mPendingArg05),
				BASE_TO_FINAL_TYPE(Arg06StorableType, Arg06Type, mPendingArg06)
			);
		else if(argsCount == 7)
			(mClassInstance->*mPendingInstanceMethod.f07)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04),
				BASE_TO_FINAL_TYPE(Arg05StorableType, Arg05Type, mPendingArg05),
				BASE_TO_FINAL_TYPE(Arg06StorableType, Arg06Type, mPendingArg06),
				BASE_TO_FINAL_TYPE(Arg07StorableType, Arg07Type, mPendingArg07)
			);
		else if(argsCount == 8)
			(mClassInstance->*mPendingInstanceMethod.f08)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04),
				BASE_TO_FINAL_TYPE(Arg05StorableType, Arg05Type, mPendingArg05),
				BASE_TO_FINAL_TYPE(Arg06StorableType, Arg06Type, mPendingArg06),
				BASE_TO_FINAL_TYPE(Arg07StorableType, Arg07Type, mPendingArg07),
				BASE_TO_FINAL_TYPE(Arg08StorableType, Arg08Type, mPendingArg08)
			);
		else if(argsCount == 9)
			(mClassInstance->*mPendingInstanceMethod.f09)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04),
				BASE_TO_FINAL_TYPE(Arg05StorableType, Arg05Type, mPendingArg05),
				BASE_TO_FINAL_TYPE(Arg06StorableType, Arg06Type, mPendingArg06),
				BASE_TO_FINAL_TYPE(Arg07StorableType, Arg07Type, mPendingArg07),
				BASE_TO_FINAL_TYPE(Arg08StorableType, Arg08Type, mPendingArg08),
				BASE_TO_FINAL_TYPE(Arg09StorableType, Arg09Type, mPendingArg09)
			);
		else if(argsCount == 10)
			(mClassInstance->*mPendingInstanceMethod.f10)
			(
				BASE_TO_FINAL_TYPE(Arg01StorableType, Arg01Type, mPendingArg01),
				BASE_TO_FINAL_TYPE(Arg02StorableType, Arg02Type, mPendingArg02),
				BASE_TO_FINAL_TYPE(Arg03StorableType, Arg03Type, mPendingArg03),
				BASE_TO_FINAL_TYPE(Arg04StorableType, Arg04Type, mPendingArg04),
				BASE_TO_FINAL_TYPE(Arg05StorableType, Arg05Type, mPendingArg05),
				BASE_TO_FINAL_TYPE(Arg06StorableType, Arg06Type, mPendingArg06),
				BASE_TO_FINAL_TYPE(Arg07StorableType, Arg07Type, mPendingArg07),
				BASE_TO_FINAL_TYPE(Arg08StorableType, Arg08Type, mPendingArg08),
				BASE_TO_FINAL_TYPE(Arg09StorableType, Arg09Type, mPendingArg09),
				BASE_TO_FINAL_TYPE(Arg10StorableType, Arg10Type, mPendingArg10)
			);
	}

	/*!	\brief	Returns whether this instance callback has an asynchronous (or many) call(s) pending

	\return		bool
	*/
	bool		AsyncExecutionPending()
	{
		return mPendingAsyncCalls.GetCount() > 0;
	}

	/*!	\brief	Returns whether this instance callback has a blocking asynchronous (or many) call(s) pending

	\return		bool
	*/
	bool		BlockingAsyncExecutionPending()
	{
		if(mBlockingAsyncSynchronize.IsBlocked())
			return true;

		return mPendingBlockingAsyncCalls.GetCount() > 0;
	}

	/*!	\brief	Cancels any existing pending executions (both asynchronous and blocking asynchronous)

	\return		void
	*/
	void		CancelPendingExecution()
	{
		mBlockingAsyncSynchronize.Release();
		if(mPendingAsyncCalls.GetCount() == 0 && mPendingBlockingAsyncCalls.GetCount() == 0)
			return;

		mPendingsSynchronize.Lock();
		while(mPendingAsyncCalls.GetCount() > 0)
		{
			InstanceCallbackType* instCallback = mPendingAsyncCalls[0];
			mPendingAsyncCalls.Remove(0);
			instCallback->CancelPendingExecution();
			delete instCallback;
		}

		while(mPendingBlockingAsyncCalls.GetCount() > 0)
		{
			InstanceCallbackType* instCallback = mPendingBlockingAsyncCalls[0];
			mPendingBlockingAsyncCalls.Remove(0);
			instCallback->CancelPendingExecution();
		}
		mPendingsSynchronize.Unlock();
	}

	/*!	\brief	Finishes execution started by ExecuteAsync or ExecuteBlockingAsync

	Method that finishes execution started by ExecuteAsync or ExecuteBlockingAsync
	This method guarantees thread safety if called in the target's thread on an event loop based mechanism.

	\return		int32_t	Number of executed calls
	*/
	int32_t		FinalizeAsyncCalls(int32_t countCallsToExecute)
	{
		if(!AsyncExecutionPending() && !BlockingAsyncExecutionPending())
			return 0;
		if(mPendingAsyncCalls.GetCount() == 0 && mPendingBlockingAsyncCalls.GetCount() == 0)
			return 0;
		if(countCallsToExecute == 0)
			return 0;

		int32_t executedCallsCount = 0;
		int32_t asyncCountCallsToExecute = 0;
		int32_t blockingAsyncCountCallsToExecute = 0;

		mPendingsSynchronize.Lock();

		if(countCallsToExecute > 0)
		{
			if(mPendingAsyncCalls.GetCount() > 0 && mPendingBlockingAsyncCalls.GetCount() > 0)
			{
				asyncCountCallsToExecute = countCallsToExecute / 2;
				blockingAsyncCountCallsToExecute = countCallsToExecute - asyncCountCallsToExecute;
			}
			else if(mPendingAsyncCalls.GetCount() > 0)
				asyncCountCallsToExecute = countCallsToExecute;
			else if(mPendingBlockingAsyncCalls.GetCount() > 0)
				blockingAsyncCountCallsToExecute = countCallsToExecute;
		}
		else if(countCallsToExecute < 0)
			asyncCountCallsToExecute = blockingAsyncCountCallsToExecute = -1;

		executedCallsCount = 0;
		while(mPendingAsyncCalls.GetCount() > 0 && asyncCountCallsToExecute != 0)
		{
			InstanceCallbackType* instCallback = mPendingAsyncCalls[0];
			executedCallsCount ++;
			mPendingAsyncCalls.Remove(0);
			instCallback->ExecutePendingMethod();
			delete instCallback;

			if(asyncCountCallsToExecute > 0 && executedCallsCount >= asyncCountCallsToExecute)
				break;
		}

		executedCallsCount = 0;
		for(uint32_t i = 0; i < mPendingBlockingAsyncCalls.GetCount() && blockingAsyncCountCallsToExecute != 0; i ++)
		{
			InstanceCallbackType* instCallback = mPendingBlockingAsyncCalls[i];
			if(instCallback->BlockingAsyncExecutionPending())
			{
				executedCallsCount ++;
				mPendingBlockingAsyncCalls.Remove(i);
				instCallback->ExecutePendingMethod();
				instCallback->mBlockingAsyncSynchronize.Release();
				i = uint32_t(-1);
			}

			if(blockingAsyncCountCallsToExecute > 0 && executedCallsCount >= blockingAsyncCountCallsToExecute)
				break;
		}

		mPendingsSynchronize.Unlock();

		return executedCallsCount;
	}
};


/*!	\brief	A callback that is pointing to a static method

Template definition of a callback that is pointing to a static method. The maximum number of method parameters is 10.
The following template parameters represent the types of each variable that the class instance method has. The template
parameters are default parameters, assuming that if they are not specified, type Undefined will be considered.

\sa	InstanceCallback
*/
template <
			class Arg01Type = Undefined,
			class Arg02Type = Undefined,
			class Arg03Type = Undefined,
			class Arg04Type = Undefined,
			class Arg05Type = Undefined,
			class Arg06Type = Undefined,
			class Arg07Type = Undefined,
			class Arg08Type = Undefined,
			class Arg09Type = Undefined,
			class Arg10Type = Undefined
		>
class StaticCallback
{
	// Types
private:
	typedef void (*FunctionS00)();
	typedef void (*FunctionS01)(Arg01Type);
	typedef void (*FunctionS02)(Arg01Type, Arg02Type);
	typedef void (*FunctionS03)(Arg01Type, Arg02Type, Arg03Type);
	typedef void (*FunctionS04)(Arg01Type, Arg02Type, Arg03Type, Arg04Type);
	typedef void (*FunctionS05)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type);
	typedef void (*FunctionS06)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type);
	typedef void (*FunctionS07)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type);
	typedef void (*FunctionS08)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type);
	typedef void (*FunctionS09)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type);
	typedef void (*FunctionS10)(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type);
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg10Type), FunctionS09, FunctionS10)		FunctionSX10;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg09Type), FunctionS08, FunctionSX10)	FunctionSX09;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg08Type), FunctionS07, FunctionSX09)	FunctionSX08;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg07Type), FunctionS06, FunctionSX08)	FunctionSX07;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg06Type), FunctionS05, FunctionSX07)	FunctionSX06;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg05Type), FunctionS04, FunctionSX06)	FunctionSX05;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg04Type), FunctionS03, FunctionSX05)	FunctionSX04;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg03Type), FunctionS02, FunctionSX04)	FunctionSX03;
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg02Type), FunctionS01, FunctionSX03)	FunctionSX02;

public:
	/*!	\brief	A type of any instance method with a variable number of parameters until 10
	*/
	typedef typename CHOOSE_TYPE(IS_UNDEFINED(Arg01Type), FunctionS00, FunctionSX02)	Method;

private:
	union FunctionS
	{
		FunctionS00 f00;
		FunctionS01 f01;
		FunctionS02 f02;
		FunctionS03 f03;
		FunctionS04 f04;
		FunctionS05 f05;
		FunctionS06 f06;
		FunctionS07 f07;
		FunctionS08 f08;
		FunctionS09 f09;
		FunctionS10 f10;
	};

	union FunctionA
	{
		Method f;
	};

	// Members
	Method		mMethod;	//!<	The contained static method.

public:
	// Constructors
				/*!	\brief	Default constructor
				*/
				StaticCallback()
				{
					mMethod = NULL;
				}

				/*!	\brief	Constructor from static method
				*/
				StaticCallback(Method method)
				{
					mMethod = method;
				}

	// Methods
	/*!	\brief	Sets the class static method

	\param[in]	staticMethod	The static method that will be called
	\return		void
	*/
	void		SetMethod(Method staticMethod)
	{
		mMethod = staticMethod;
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 0 parameters.

	\return		void
	*/
	void		Execute()
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f00();
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 1 parameters.

	\param[in]	arg01	First parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f01(arg01);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 2 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f02(arg01, arg02);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 3 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f03(arg01, arg02, arg03);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 4 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f04(arg01, arg02, arg03, arg04);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 5 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f05(arg01, arg02, arg03, arg04, arg05);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 6 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f06(arg01, arg02, arg03, arg04, arg05, arg06);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 7 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f07(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 8 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f08(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 9 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f09(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
	}

	/*!	\brief	Executes the contained method, synchronously

	Method that executes the contained method, which has 10 parameters.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\param[in]	arg10	Tenth parameter
	\return		void
	*/
	void		Execute(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
	{
		if(!mMethod)
			return;

		FunctionS staticMethod;
		FunctionA abstractMethod;
		abstractMethod.f = mMethod;
		::memcpy(&staticMethod, &abstractMethod, sizeof(staticMethod));
		staticMethod.f10(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
	}
};

/*!	\brief	Abstract class representing the base standard class for any Signal or Slot

\sa	SignalSlotBaseTemplate, Signal, Slot
*/
class SignalSlotBaseStandard
{
	friend class GlobalSlotList;

public:
    enum ConnectionKind
	{
		eSynchronous = 0,
		eAsynchronous,
		eBlockingAsynchronous
	};

protected:
    enum EntityKind
	{
		eSignal = 0,
		eSlot
	};

	typedef Array<SignalSlotBaseStandard*> SignalSlotList;

		EntityKind	mEntityType;					//!<	The type of this entity.
	SignalSlotList	mCounterSyncEntities;			//!<	The counter entities list that are connected synchrnously.
	SignalSlotList	mCounterAsyncEntities;			//!<	The counter entities list that are connected asynchronously.
	SignalSlotList	mCounterBlockingAsyncEntities;	//!<	The counter entities list that are connected asynchronously, blocking the calling thread.
		Semaphore	mEntitiesSynchronize;			//!<	Entities synchronization.

	// Constructors & Destructor
					/*!	\brief	Private constructor from the type of an entity
					*/
					SignalSlotBaseStandard(EntityKind kind)
					{
						mEntityType = kind;
					}

public:
		/*!	\brief	Virtual destructor

		Performs the automatic disconnection of all linked counter entities.
		*/
		virtual		~SignalSlotBaseStandard()
					{
						DisconnectAll();
					}

	// Methods
	/*!	\brief	Method to connect to a counter entity

	Abstract method that is used to connect to a counter entity's list of entities in either synchronous, asynchronous or blocking asynchronous mode,
	depending on the second parameter.

	\param[in]	entity			The counter entity to connect to
	\param[in]	connectionType	Specifies whether synchronous, asynchronous or blocking asynchronous connection should be made
	\return		void
	*/
	virtual	void	Connect(SignalSlotBaseStandard*, ConnectionKind = eSynchronous) = 0;
	virtual	void	Connect(SignalSlotBaseStandard&, ConnectionKind = eSynchronous) = 0;

			/*!	\brief	Method to disconnect from a counter entity

			Method that is used to disconnect itself from a counter entity's list of entities.

			\param[in]	entity	The counter entity to disconnect from
			\return		void
			*/
			void	Disconnect(SignalSlotBaseStandard* entity)
			{
				DisconnectEntity(entity);
			}

			void	Disconnect(SignalSlotBaseStandard& entity)
			{
				DisconnectEntity(&entity);
			}

			/*!	\brief	Disconnects all the linked counter entities

			Method that is used to disconnect all linked counter entities.

			\return		void
			*/
			void	DisconnectAll()
			{
				while(mCounterSyncEntities.GetCount() > 0)
					Disconnect(mCounterSyncEntities[0]);

				while(mCounterAsyncEntities.GetCount() > 0)
					Disconnect(mCounterAsyncEntities[0]);

				while(mCounterBlockingAsyncEntities.GetCount() > 0)
					Disconnect(mCounterBlockingAsyncEntities[0]);
			}

protected:
	virtual	int32_t	FinalizeEmitAsync(void*, int32_t) = 0;

		int32_t		GetIndexOfSynchronousEntity(SignalSlotBaseStandard* entity)
		{
			if(!entity)
				return -1;

			int32_t index = -1;
			mEntitiesSynchronize.Lock();
			for(uint32_t i = 0; i < mCounterSyncEntities.GetCount(); i++)
				if(mCounterSyncEntities[i] == entity)
				{
					index = (int32_t)i;
					break;
				}
			mEntitiesSynchronize.Unlock();

			return index;
		}

		int32_t		GetIndexOfAsynchronousEntity(SignalSlotBaseStandard* entity)
		{
			if(!entity)
				return -1;

			int32_t index = -1;
			mEntitiesSynchronize.Lock();
			for(uint32_t i = 0; i < mCounterAsyncEntities.GetCount(); i++)
				if(mCounterAsyncEntities[i] == entity)
				{
					index = (int32_t)i;
					break;
				}
			mEntitiesSynchronize.Unlock();

			return index;
		}

		int32_t		GetIndexOfBlockingAsynchronousEntity(SignalSlotBaseStandard* entity)
		{
			if(!entity)
				return -1;

			int32_t index = -1;
			mEntitiesSynchronize.Lock();
			for(uint32_t i = 0; i < mCounterBlockingAsyncEntities.GetCount(); i++)
				if(mCounterBlockingAsyncEntities[i] == entity)
				{
					index = (int32_t)i;
					break;
				}
			mEntitiesSynchronize.Unlock();

			return index;
		}

	virtual	bool	ConnectEntity(SignalSlotBaseStandard* entity, ConnectionKind connectionType = eSynchronous)
	{
		if( !entity )
			return false;
		if(mEntityType == entity->mEntityType)
			return false;
		if(connectionType == eSynchronous && GetIndexOfSynchronousEntity(entity) != -1)
			return false;
		else if(connectionType == eAsynchronous && GetIndexOfAsynchronousEntity(entity) != -1)
			return false;
		else if(connectionType == eBlockingAsynchronous && GetIndexOfBlockingAsynchronousEntity(entity) != -1)
			return false;

		mEntitiesSynchronize.Lock();
		if(connectionType == eSynchronous)
			mCounterSyncEntities.Append(entity);
		else if(connectionType == eAsynchronous)
			mCounterAsyncEntities.Append(entity);
		else if(connectionType == eBlockingAsynchronous)
			mCounterBlockingAsyncEntities.Append(entity);
		mEntitiesSynchronize.Unlock();
		entity->Connect(this, connectionType);
		return true;
	}

	virtual	void	DisconnectEntity(SignalSlotBaseStandard* entity)
	{
		if(!entity)
			return;
		if(mEntityType == entity->mEntityType)
			return;
		int32_t synchronousIdx = GetIndexOfSynchronousEntity(entity);
		int32_t asynchronousIdx = GetIndexOfAsynchronousEntity(entity);
		int32_t blockingAsynchronousIdx = GetIndexOfBlockingAsynchronousEntity(entity);
		if(synchronousIdx == -1 && asynchronousIdx == -1 && blockingAsynchronousIdx == -1)
			return;

		mEntitiesSynchronize.Lock();
		if(synchronousIdx >= 0)
			mCounterSyncEntities.Remove((uint32_t)synchronousIdx);
		else if(asynchronousIdx >= 0)
			mCounterAsyncEntities.Remove((uint32_t)asynchronousIdx);
		else if(blockingAsynchronousIdx >= 0)
			mCounterBlockingAsyncEntities.Remove((uint32_t)blockingAsynchronousIdx);
		mEntitiesSynchronize.Unlock();
		entity->Disconnect(this);
	}
};

/*!	\brief	Abstract class representing the base template class for any Signal or Slot

Template definition of a base class for any Signal/Slot. The maximum number of method parameters is 10.
The following template parameters represent the types of each variable that the method has. The template
parameters are default parameters, assuming that if they are not specified, type Undefined will be considered.

\sa	Signal, Slot
*/
template <  
			class Arg01Type = Undefined,
			class Arg02Type = Undefined,
			class Arg03Type = Undefined,
			class Arg04Type = Undefined,
			class Arg05Type = Undefined,
			class Arg06Type = Undefined,
			class Arg07Type = Undefined,
			class Arg08Type = Undefined,
			class Arg09Type = Undefined,
			class Arg10Type = Undefined
		>
class SignalSlotBaseTemplate : public SignalSlotBaseStandard
{
protected:
	// Constructors

					/*!	\brief	Private constructor from the type of an entity
					*/
					SignalSlotBaseTemplate(SignalSlotBaseStandard::EntityKind kind)
						: SignalSlotBaseStandard(kind)
					{
					}

public:

	// Methods

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 0 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\return		void
	*/
	virtual void	Emit() = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 1 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 2 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 3 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 4 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the synchronous execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 5 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 6 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 7 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 8 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 9 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type) = 0;

	/*!	\brief	Emits the execution of the event

	Abstract method that emits the execution of the event.
	This method should be called in case a pair of signal/slot objects was created and linked to a method with 10 parameters.
	In case this object is a Signal, this method will trigger the execution of each linked slot's method.
	In case this object is a Slot, this method will execute the contained Instance or Static Callback.

	\param[in]	arg01	First parameter
	\param[in]	arg02	Second parameter
	\param[in]	arg03	Third parameter
	\param[in]	arg04	Fourth parameter
	\param[in]	arg05	Fifth parameter
	\param[in]	arg06	Sixth parameter
	\param[in]	arg07	Seventh parameter
	\param[in]	arg08	Eighth parameter
	\param[in]	arg09	Ninth parameter
	\param[in]	arg10	Tenth parameter
	\return		void
	*/
	virtual void	Emit(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type) = 0;

	virtual void	EmitAsync() = 0;
	virtual void	EmitAsync(Arg01Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type) = 0;
	virtual void	EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type) = 0;

	virtual void	EmitBlockingAsync() = 0;
	virtual void	EmitBlockingAsync(Arg01Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type) = 0;
	virtual void	EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type) = 0;
};

}}
