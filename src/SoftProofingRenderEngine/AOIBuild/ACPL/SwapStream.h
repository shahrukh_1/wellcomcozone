/*
 * SwapStream
 * Copyright (c) 1998-2012 Aurelon. All rights reserved.
 *
 * Class for reading/writing an ordered sequence of bytes to a file
 */

#pragma once

#include "./FileStream.h"
#include "./MemoryStream.h"

namespace aur { namespace ACPL {

	class ACPLAPI SwapStream : public FileStream
	{
	public:
							SwapStream( const FileSpec&, Permission = eRdWrPermission, int32_t = 5120 );

		virtual	Stream&		operator << (int16_t inNum);
		virtual	Stream&		operator << (uint16_t inNum);
		virtual	Stream&		operator << (int32_t inNum);
		virtual	Stream&		operator << (uint32_t inNum);
		virtual	Stream&		operator << (long inNum);
		virtual	Stream&		operator << (unsigned long inNum);
		virtual	Stream&		operator << (int64_t inNum);
		virtual	Stream&		operator << (float inNum);
		virtual	Stream&		operator << (double inNum);
		virtual	Stream&		operator << (const ContourElement&);

		virtual	Stream&		operator >> (int16_t &outNum);
		virtual	Stream&		operator >> (uint16_t &outNum);
		virtual	Stream&		operator >> (int32_t &outNum);
		virtual	Stream&		operator >> (uint32_t &outNum);
		virtual	Stream&		operator >> (long &outNum);
		virtual	Stream&		operator >> (unsigned long &outNum);
		virtual	Stream&		operator >> (int64_t &outNum);
		virtual	Stream&		operator >> (float &outNum);
		virtual	Stream&		operator >> (double &outNum);
		virtual	Stream&		operator >> (ContourElement&);
		virtual	Stream&		operator >> ( String& outStr );
		virtual	void		WriteUTF16String( const UString& inString );
		virtual	UString		ReadUTF16String();
	};

	class ACPLAPI SwapMemStream : public MemoryStream
	{
	public:
							SwapMemStream();
							SwapMemStream( char*, int32_t );

		virtual	Stream&		operator << (int16_t inNum);
		virtual	Stream&		operator << (uint16_t inNum);
		virtual	Stream&		operator << (int32_t inNum);
		virtual	Stream&		operator << (uint32_t inNum);
		virtual	Stream&		operator << (long inNum);
		virtual	Stream&		operator << (unsigned long inNum);
		virtual	Stream&		operator << (int64_t inNum);
		virtual	Stream&		operator << (float inNum);
		virtual	Stream&		operator << (double inNum);
		virtual	Stream&		operator << (const ContourElement&);

		virtual	Stream&		operator >> (int16_t &outNum);
		virtual	Stream&		operator >> (uint16_t &outNum);
		virtual	Stream&		operator >> (int32_t &outNum);
		virtual	Stream&		operator >> (uint32_t &outNum);
		virtual	Stream&		operator >> (long &outNum);
		virtual	Stream&		operator >> (unsigned long &outNum);
		virtual	Stream&		operator >> (int64_t &outNum);
		virtual	Stream&		operator >> (float &outNum);
		virtual	Stream&		operator >> (double &outNum);
		virtual	Stream&		operator >> (ContourElement&);
		virtual	Stream&		operator >> ( String& outStr );
		virtual	Stream&		operator >> ( UString& string );
	};

}}
