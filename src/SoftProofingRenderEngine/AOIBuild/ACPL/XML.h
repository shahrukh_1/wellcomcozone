#pragma once

#include "Array.h"
#include "CString.h"

namespace aur { namespace ACPL {

	class	FileSpec;
	class	Stream;
	class	MemoryStream;

	typedef	enum XMLResult
	{
		eXMLNoError,
		eXMLNotValid,
		eXMLNotFound,
		eXMLInvalidTag
	} XMLResult;

	class ACPLAPI XML
	{
	public:
		class	Node;
		class ACPLAPI Element
		{
			friend class Node;
		public:
							Element( const char* name, const char* value ); 
							Element( const char* name, int32_t value );
							Element( const char* name, int64_t value );
							Element( const char* name, uint32_t value );
							Element( const char* name, float value );
							Element( const char* name, double value );
							Element( const char* name, bool value );
							Element( const char* name, MemoryStream& value );
							Element( const char* name, const UString& value );
			virtual			~Element();

			String&			operator+=( const char* inStr );
			Element&		operator=( const Element& );
			bool			operator==( const Element& );
			operator const char*() const;
			Element*		Duplicate() const;

			Node*			GetParentNode();

			float			ToFloat( float defaultV = 0 ) const;
			double			ToDouble( double defaultV = 0 ) const;
			int32_t			ToInt( int32_t defaultV = 0 ) const;
			int64_t			ToInt64( int64_t defaultV = 0 ) const;
			uint32_t		ToUInt( uint32_t defaultV = 0 ) const;
			bool			ToBool( bool defaultV = false ) const; 
			const String&	ToString() const;
			UString			ToUString() const;
			uint32_t		ToBinary( uint8_t* data ) const;
			void			ToBinary( MemoryStream& ) const;
			bool			ToBinaryFromBase64( MemoryStream& ) const;

			void			SetValue( const char* v );
			void			SetValue( int32_t v );
			void			SetValue( int64_t v );
			void			SetValue( uint32_t v );
			void			SetValue( float v );
			void			SetValue( double v );
			void			SetValue( bool v );
			void			SetValue( uint32_t dataLen, const uint8_t* data );
			void			SetValueBase64( uint32_t dataLen, const uint8_t* data );
			void			SetValue( const UString& v );
			void			Format( const char* format, ... );

			const String&	Name() const;
			void			SetName( const char* name );
			bool			IsEmpty() const;

		protected:
			String			mValue;
			String			mName;
			Node*			mParentNode;

			virtual void	WriteToFile( Stream&, const char* );
			virtual Element* DuplicateImp() const;
		};

		typedef Array<Element*> ElementsList;

		class ACPLAPI Node : public Element
		{
			friend class XML;
		public:
							Node( const char* name = NULL );
							Node( const char* name, const char* value ); 
							Node( const char* name, int32_t value );
							Node( const char* name, int64_t value );
							Node( const char* name, uint32_t value );
							Node( const char* name, float value );
							Node( const char* name, double value );
							Node( const char* name, bool value );
							Node( const char* name, const UString& value );
			virtual			~Node();

			bool			HasName() const;
			bool			HasAttributes() const;
			bool			HasChildren() const;

			uint32_t		GetNrOfChildren() const;
			uint32_t		AttributesCount() const;

			Element*		GetAttributeByIndex( uint32_t index ) const;
			Element*		GetAttributeByName( const char* name ) const;

			Element*		GetElementByIndex( uint32_t index ) const;
			Element*		GetElementByName( const char* name ) const;
			Element*		GetElementByXPath( const char* name );

			Node*			GetNodeByIndex( uint32_t index ) const;
			Node*			GetNodeByName( const char* ) const;
			Node*			GetNodeByXPath( const char* );

			void			AddAttribute( Element* element );
			void			AddAttribute( const char* name, const char* value );
			void			AddAttribute( const char* name, int32_t value );
			void			AddAttribute( const char* name, uint32_t value );
			void			AddAttribute( const char* name, float value );
			void			AddAttribute( const char* name, double value );
			void			AddAttribute( const char* name, bool value );
			void			AddAttribute( const char* name, const UString& value );
			bool			DeleteAttribute( const char* name );

			void			AddChildNode( Element*, uint32_t atPosition = 0xFFFFFFFFU );
			XML::Node*		AddChildNode( const char* name, uint32_t atPosition = 0xFFFFFFFFU );
			bool			ReplaceChildNode( Element* );
			bool			DeleteChildNodeByName( const char* );
			bool			DeleteChildNodeByIndex( uint32_t );
			bool			DetachChildNode( Element* );

			void			SetElementValue( const char* label, int32_t value );
			void			SetElementValue( const char* label, int64_t value );
			void			SetElementValue( const char* label, uint32_t value );
			void			SetElementValue( const char* label, bool value );
			void			SetElementValue( const char* label, float value );
			void			SetElementValue( const char* label, double value );
			void			SetElementValue( const char* label, const char* str );
			void			SetElementValue( const char* label, const UString& str );
			void			SetElementValue( const char* label, uint32_t dataLen, const uint8_t* data );

			bool			GetElementValue( const char* label, int32_t& value ) const;
			bool			GetElementValue( const char* label, int64_t& value ) const;
			bool			GetElementValue( const char* label, uint32_t& value ) const;
			bool			GetElementValue( const char* label, bool& value ) const;
			bool			GetElementValue( const char* label, float& value ) const;
			bool			GetElementValue( const char* label, double& value ) const;
			bool			GetElementValue( const char* label, char* str ) const;
			bool			GetElementValue( const char* label, String& str ) const;
			bool			GetElementValue( const char* label, UString& str ) const;
			bool			GetElementValue( const char* label, uint32_t& dataLen, uint8_t* data ) const;
#if debugging
			/*!
			 DbgString
			 \ingroup	XML

			 Returns the string representation of the node and all its attributes and children under the  
			 form of a const char*. The method is deliberately leaking to avoid memory allocation problems with the Windows debugger.
			 Usage
			 Mac:
			 -	Set a breakpoint and type in the console: printf “%s”, node->DbgString(). The contents will be displayed in the console.
			 Win:
			 -	Limitation: DbgString cannot be called from the application context since it is defined in a dll. 
			 -	Add a global function called PrintNode( ACPL::XML::Node* ); in stdafx which calls the node method for us and dumps everything in the output console.
			 The method should contain the following code: OutputDebugString( node->DbgString() );
			 -	Usage: set a breakpoint, add a watch expression: node->PrintNode(). To get updates, use the refresh button. The XML contents are dumped in the output window.
			 */
			const char *	DbgString();
#if ACPL_MAC
			/*!
			 DumpNode
			 \ingroup	XML

			 Mac convenience XML debugging method.
			 The debugger console command is simple: call node->DumpNode()
			 The entire node will be displayed formatted in the console output.
			 */
			void			DumpNode();
#endif
#endif
		protected:
			ElementsList*	mAttributes;
			ElementsList*	mChildNodes;

			void			ReadNodeData( Stream& );
			XMLResult		ParseContent( Stream& );
			XMLResult		ParseTag( const String& );
			virtual void	WriteToFile( Stream&, const char* );
			virtual Element* DuplicateImp() const;
		};

					XML( const char* name = NULL );
					~XML();
		XMLResult 	Load( const FileSpec*, bool validate = true );
		XMLResult	Load( Stream&, bool validate = true );
		XMLResult	Load( const char*, bool validate = true );
		void 		Save( const FileSpec* );
		void 		Save( Stream& );
		String		Save();
		void		Empty();
		void		Initialize( const char* name );
		bool		IsXMP() const;
		void		SetXMP( bool xmp );

		Node*		root;
		Node*		GetNodeByXPath( const char* );
		Element*	GetElementByXPath( const char* );
		const char*	GetValueByXPath( const char* );

	private:
		bool		mIsXMP;
		XMLResult 	ValidateXML( Stream& );
		void 		SkipDTD( Stream& );
	};

	inline	bool	XML::IsXMP() const
	{
		return mIsXMP;
	}

	inline	void	XML::SetXMP( bool xmp )
	{
		mIsXMP = xmp;
	}

#if ACPL_MAC
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wtautological-compare"
#endif

	inline	bool	XML::Element::IsEmpty() const
	{
		if( this == NULL )
			return true;
		return mValue.IsEmpty();
	}

#if ACPL_MAC
#	pragma clang diagnostic pop
#endif

	inline	void	XML::Element::SetValue( const char* v )
	{
		mValue = v;
	}

	inline	void	XML::Element::SetValue( int32_t v )
	{
		mValue.Format( "%d", int( v ) );
	}

	inline	void	XML::Element::SetValue( int64_t v )
	{
		mValue.Format( "%lld", v );
	}

	inline	void	XML::Element::SetValue( uint32_t v )
	{
		mValue.Format( "%u", (unsigned int)v );
	}

	inline void		XML::Element::SetValue( bool v )
	{
		mValue = v ? "true" : "false";
	}

	inline XML::Element&	XML::Element::operator=( const Element& inVal )
	{
		mValue = inVal.mValue;
		mName = inVal.mName;
		return *this;
	}

	inline bool	XML::Element::operator==( const Element& other )
	{
		return mName == other.mName && mValue == other.mValue;
	}

	inline const String& XML::Element::Name() const
	{
		return mName;
	}

	inline void	XML::Element::SetName( const char* name)
	{
		mName = name;
	}

	inline	XML::Node* XML::Element::GetParentNode()
	{
		return mParentNode;
	}

	inline	bool	XML::Node::HasName() const
	{
		return mName.GetLength() > 0;
	}

#if ACPL_MAC
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wtautological-compare"
#endif

	inline	bool	XML::Node::HasAttributes() const
	{
		return this != NULL && mAttributes && mAttributes->GetCount() > 0;
	}

	inline	bool	XML::Node::HasChildren() const
	{
		return this != NULL && mChildNodes && mChildNodes->GetCount() != 0;
	}

#if ACPL_MAC
#	pragma clang diagnostic pop
#endif

	inline	void	XML::Node::AddAttribute( const char* name, const char* value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, int32_t value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, uint32_t value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, float value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, double value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, bool value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, const UString& value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline XML::Node*	XML::Node::GetNodeByXPath( const char* path )
	{
		return dynamic_cast<Node*>( GetElementByXPath(path) );
	}

}}
