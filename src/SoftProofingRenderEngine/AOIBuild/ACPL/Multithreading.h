/*
 *  Multithreading.h
 *  ACPL
 *
 *  Created by Erik Strik on 12-02-12.
 *  Copyright 2012 Aurelon. All rights reserved.
 *
 */
#pragma once

#include "./Types.h"
#include "CString.h"

#if ACPL_WIN
#	include <process.h>
#	define	ACPL_RES		void
#	define	ACPL_RETURN	
#	define	ACPL_THREAD		uintptr_t
#	define	ACPL_SLEEP(i)	::Sleep(i)
#	define	Duration		DWORD
#	define	ACPL_MEMORYBARRIER _ReadWriteBarrier
#elif ACPL_MAC
#	include <semaphore.h>
#	include <pthread.h>
#	include <libkern/OSAtomic.h>
#	define	ACPL_RES		void*
#	define	ACPL_RETURN		NULL
#	define	ACPL_THREAD		pthread_t
#	define	ACPL_SLEEP(i)	::usleep(i*1000)
#	define	ACPL_MEMORYBARRIER OSMemoryBarrier
#elif ACPL_LINUX
#	include <semaphore.h>
#	include <pthread.h>
#	include <time.h>
#	define	Duration		time_t
#	define	ACPL_RES		void*
#	define	ACPL_RETURN		NULL
#	define	ACPL_THREAD		pthread_t
#	define	ACPL_SLEEP(i)	::usleep(i*1000)
#	error	ACPL_MEMORYBARRIER Not implemented
#endif

namespace aur { namespace ACPL {

	class ACPLAPI	ConditionVariable
	{
#if ACPL_WIN
		CONDITION_VARIABLE	mConditionVariable;
		CRITICAL_SECTION	mMutex;
#else
		pthread_cond_t		mConditionVariable;
		pthread_mutex_t		mMutex;
#endif
	public:
				ConditionVariable();

		bool	Wait();
		bool	WakeAll();

		void	LockMutex();
		void	UnlockMutex();
	private:
				ConditionVariable( const ConditionVariable& );
				ConditionVariable& operator=( const ConditionVariable& );
	};

	class ACPLAPI	Semaphore
	{
#if ACPL_MAC
		MPSemaphoreID	mSemaphore;
#elif ACPL_LINUX
		sem_t			mSemaphore;
#else
		HANDLE			mSemaphore;
#endif
	public:
				Semaphore( uint32_t = 1, uint32_t = 1 );
				~Semaphore();
		bool	Lock();
		bool	Lock( Duration );
		void	Unlock();
		void	Unlock( uint32_t );
	private:
		Semaphore( const Semaphore& );
		Semaphore& operator=( const Semaphore& );
	};

	class ACPLAPI	InterProcessSemaphore
	{
		char	mSemName[256];
#if ACPL_WIN
		HANDLE	mSemaphore;
#else
		sem_t*	mSemaphore;
#endif
	public:
		InterProcessSemaphore( const char* name, uint32_t = 1, uint32_t = 1 );
		~InterProcessSemaphore();
		bool	Lock();
		bool	Lock( Duration );
		void	Unlock();
		void	Unlock( uint32_t );
	private:
		InterProcessSemaphore( const InterProcessSemaphore& );
		InterProcessSemaphore& operator=( const InterProcessSemaphore& );
	};

	class ACPLAPI	CriticalSection
	{
#if ACPL_WIN
		CRITICAL_SECTION	mMutex;
#else
		pthread_mutex_t		mMutex;
#endif
	public:
				CriticalSection();
				~CriticalSection();
		void	Enter();
		void	Leave();
	private:
		CriticalSection( const CriticalSection& );
		CriticalSection& operator=( const CriticalSection& );
	};

	inline	CriticalSection::CriticalSection()
	{
#if ACPL_WIN
		::InitializeCriticalSection( &mMutex );
#else
		pthread_mutexattr_t	attribute;
		::pthread_mutexattr_init( &attribute );
		::pthread_mutexattr_settype( &attribute, PTHREAD_MUTEX_RECURSIVE );
		::pthread_mutex_init( &mMutex, &attribute );
		::pthread_mutexattr_destroy( &attribute );
#endif
	}

	inline	CriticalSection::~CriticalSection()
	{
#if ACPL_WIN
		::DeleteCriticalSection( &mMutex );
#else
		::pthread_mutex_destroy( &mMutex );
#endif
	}

	inline	void	CriticalSection::Enter()
	{
#if ACPL_WIN
		::EnterCriticalSection( &mMutex );
#else
		::pthread_mutex_lock( &mMutex );
#endif
	}

	inline	void	CriticalSection::Leave()
	{
#if ACPL_WIN
		::LeaveCriticalSection( &mMutex );
#else
		::pthread_mutex_unlock( &mMutex );
#endif
	}

	class StCriticalSection
	{
		CriticalSection&	mCriticalSection;
	public:
		StCriticalSection( CriticalSection& );
		~StCriticalSection();
	private:
		StCriticalSection( const StCriticalSection& );
		StCriticalSection& operator=( const StCriticalSection& );
	};

	inline	StCriticalSection::StCriticalSection( CriticalSection& sc ) :
	mCriticalSection( sc )
	{
		mCriticalSection.Enter();
	}

	inline	StCriticalSection::~StCriticalSection()
	{
		mCriticalSection.Leave();
	}

	class ACPLAPI Thread
	{
#if ACPL_WIN
		typedef HANDLE		ThreadHandle;
#else
		typedef pthread_t	ThreadHandle;
#endif
	public:
		enum ThreadPriority
		{
			eBackground,
			eBelowNormal,
			eNormal,
			eAboveNormal,
			eTimeCritical
			//This value should be used only for real time critical computation! 
			//If the ThreadMain does not manages in a good mannered its computations it may block the system!!!
		};

	public:
		static void  SetName( const char* threadName );

		Thread( const char* name, uint32_t waitInterval );
		virtual ~Thread();

		void				Start();

		//Signals the worker thread to stop and returns the running state of the thread on exit
		//The timeout is the amount of time (in milliseconds) the function awaits for the thread to stop before exiting
		//timeout = 0 means the function just signals the thread to stop and exit
		//timeout =  WaitInfinite means the function waits until the thread stops
		bool				Stop(uint32_t timeout = 0);

		//terminate the worker thread - not guaranteed on MAC, use Stop() whenever possible
		void				Kill();

		bool				IsRunning() const;

		const String&		GetName() const;

		void				SetPriority( ThreadPriority priority );

		static const uint32_t	WaitInfinite;

	protected:
		virtual void		ThreadMain() = 0;
		virtual void		Cleanup();
		bool				ShouldContinue() const;
		void				Finished();

	private:
		Thread( const Thread& );
		Thread& operator=( const Thread& );
#if ACPL_WIN
		static DWORD WINAPI EntryPoint(void* params);
#else
		static void*		EntryPoint(void* params);
#endif
		void				Run();

		String				mName;

		ThreadHandle		mOSThread;
		volatile bool		mContinue;
		volatile bool		mRunning;
		ThreadPriority		mPriority;
		uint32_t			mWaitInterval;
	};

	inline bool Thread::ShouldContinue() const
	{
		return mContinue;
	}

	inline bool Thread::IsRunning() const
	{
		return mRunning;
	}

	inline const String& Thread::GetName() const
	{
		return mName;
	}

	inline void	Thread::Finished()
	{
		mContinue = false;
	}

	template<class T>
	class InstantThread : public Thread
	{
	public:
		InstantThread(const char* name, uint32_t waitInterval, T* callbackObject, void (T::*callback)(), void (T::*cleanupCallback)() = NULL);

	protected:
		void			ThreadMain();
		void			Cleanup();

	private:
		T*				mCallbackObject;
		void			(T::*mThreadCallback)();
		void			(T::*mCleanupCallback)();
	};

	template<class T>
	InstantThread<T>::InstantThread(const char* name, uint32_t waitInterval, T* callbackObject, void (T::*callback)(), void (T::*cleanupCallback)()) :
		Thread(name, waitInterval),
		mCallbackObject(callbackObject),
		mThreadCallback(callback),
		mCleanupCallback(cleanupCallback)
	{
	}

	template<class T>
	void InstantThread<T>::ThreadMain()
	{
		(mCallbackObject->*mThreadCallback)();
	}

	template<class T>
	void InstantThread<T>::Cleanup()
	{
		if(mCleanupCallback)
		{
			(mCallbackObject->*mCleanupCallback)();
		}
	}


	class ReadWriteSyncLocker
	{
		CriticalSection		mInternalCSection;
		Semaphore			mWriteSemaphore;
		int					mCountReaders;

	public:
						ReadWriteSyncLocker();
        virtual         ~ReadWriteSyncLocker();

		virtual	void	LockReading();
		virtual	void	UnlockReading();
		virtual	void	LockWriting();
		virtual	void	UnlockWriting();

	private:
		ReadWriteSyncLocker( const ReadWriteSyncLocker& );
		ReadWriteSyncLocker& operator=( const ReadWriteSyncLocker& );
	};

	inline ReadWriteSyncLocker::ReadWriteSyncLocker() :
		mCountReaders(0)
	{
	}
    
    inline ReadWriteSyncLocker::~ReadWriteSyncLocker()
    {
    }

	inline void ReadWriteSyncLocker::LockReading()
	{
		mInternalCSection.Enter();
		if( ++mCountReaders == 1 )
			mWriteSemaphore.Lock();
		mInternalCSection.Leave();
	}

	inline void ReadWriteSyncLocker::UnlockReading()
	{	
		mInternalCSection.Enter();
		if( mCountReaders > 0 )
			if( --mCountReaders == 0 )
				mWriteSemaphore.Unlock();
		mInternalCSection.Leave();
	}

	inline void ReadWriteSyncLocker::LockWriting()
	{
		mWriteSemaphore.Lock();
	}

	inline void ReadWriteSyncLocker::UnlockWriting()
	{
		mWriteSemaphore.Unlock();
	}

	class PriorityReadWriteLocker : public ReadWriteSyncLocker
	{
		ConditionVariable	mConditionVariable;
		bool				mWritePending;
	public:
						PriorityReadWriteLocker();
        virtual         ~PriorityReadWriteLocker();

		virtual	void	LockReading();
		virtual	void	UnlockReading();
		virtual	void	LockWriting();
		virtual	void	UnlockWriting();

	private:
		PriorityReadWriteLocker( const PriorityReadWriteLocker& );
		PriorityReadWriteLocker& operator=( const PriorityReadWriteLocker& );
	};

	inline PriorityReadWriteLocker::PriorityReadWriteLocker() :
			ReadWriteSyncLocker(),
			mWritePending( false )
	{
	}
    
    inline PriorityReadWriteLocker::~PriorityReadWriteLocker()
    {
    }

	inline void PriorityReadWriteLocker::LockReading()
	{
		mConditionVariable.LockMutex();
		while( mWritePending )
			mConditionVariable.Wait();
		mConditionVariable.UnlockMutex();

		ReadWriteSyncLocker::LockReading();
	}

	inline void PriorityReadWriteLocker::UnlockReading()
	{	
		ReadWriteSyncLocker::UnlockReading();
	}

	inline void PriorityReadWriteLocker::LockWriting()
	{
		mConditionVariable.LockMutex();
		mWritePending = true;
		mConditionVariable.UnlockMutex();

		ReadWriteSyncLocker::LockWriting();
	}

	inline void PriorityReadWriteLocker::UnlockWriting()
	{
		mConditionVariable.LockMutex();
		mWritePending = false;
		mConditionVariable.WakeAll();
		mConditionVariable.UnlockMutex();

		ReadWriteSyncLocker::UnlockWriting();
	}

	// Read locker
	class StReadLocker
	{
		ReadWriteSyncLocker& mReader;

	public:
		StReadLocker( ReadWriteSyncLocker& reader );
		~StReadLocker();

	private:
		StReadLocker( const StReadLocker& );
		StReadLocker& operator=( const StReadLocker& );
	};

	inline StReadLocker::StReadLocker( ReadWriteSyncLocker& reader ) : mReader( reader )
	{
		mReader.LockReading();
	}

	inline StReadLocker::~StReadLocker()
	{
		mReader.UnlockReading();
	}

	// Write locker
	class StWriteLocker
	{
		ReadWriteSyncLocker& mWriter;

	public:
		StWriteLocker( ReadWriteSyncLocker& writer );
		~StWriteLocker();

	private:
		StWriteLocker( const StWriteLocker& );
		StWriteLocker& operator=( const StWriteLocker& );
	};

	inline StWriteLocker::StWriteLocker( ReadWriteSyncLocker& writer ) : mWriter( writer )
	{
		mWriter.LockWriting();
	}

	inline StWriteLocker::~StWriteLocker()
	{
		mWriter.UnlockWriting();
	}

}}
