#pragma once

#include "./Types.h"
#include "./CString.h"
#include "./Array.h"

namespace aur { namespace ACPL {

	class	FileSpec;
	class	Stream;

	class ACPLAPI Dictionary
	{
	public:
		typedef	enum Result
		{
			eDictNoError,
			eDictNotFound,
			eDictTypeMismatch
		}	Result;

				Dictionary();
				~Dictionary();

		void	Empty();

		Dictionary& operator=(const Dictionary&);

		void	Save( const FileSpec* );
		void	Save( Stream& );
		void	Load( const FileSpec* );
		void	Load( Stream& );
		void	Merge( Dictionary* inDict, bool traverse, bool = true );

		void	Set( const char* label,	int32_t value );
		void	Set( const char* label,	uint32_t value );
		void	Set( const char* label,	bool value );
		void	Set( const char* label,	float value );
		void	Set( const char* label,	const char* str );
		void	Set( const char* label,	int32_t dataLen,  const uint8_t* data );
		void	Set( const char* label,	Dictionary*, bool transferOwnership = false );
		void	Set( const char* label,	const UString& str );

		void	Set( const char* label,	Array<int32_t>* value );
		void	Set( const char* label,	Array<uint32_t>* value );
		void	Set( const char* label,	Array<bool>* value );
		void	Set( const char* label,	Array<float>* value );
		void	Set( const char* label,	Array<const char*>* value);
		void	Set( const char* label,	Array<Dictionary*>* value, bool transferOwnership = false );

		Result	Get( const char* label,	int32_t& value ) const;
		Result	Get( const char* label,	uint32_t& value ) const;
		Result	Get( const char* label,	bool& value ) const;
		Result	Get( const char* label,	float& value ) const;
		Result	Get( const char* label,	char* str ) const;
		Result	Get( const char* label,	String& str ) const;
		Result	Get( const char* label,	UString& str ) const;
		// if data== NULL then only return dataLen
		Result	Get( const char* label,	int32_t& dataLen,	uint8_t* data ) const;
		Result	Get( const char* label, Dictionary*& ) const;
		Result	GetRef( const char* label, Dictionary*& ) const;
		int32_t	GetInt32( const char* label ) const;
		uint32_t	GetUInt32( const char* label ) const;
		bool	GetBool( const char* label ) const;
		float	GetFloat( const char* label ) const;
		const char*	GetString( const char* label ) const;

		Result	Get( const char* label, Array<int32_t>*& array) const;
		Result	Get( const char* label, Array<uint32_t>*& array) const;
		Result	Get( const char* label, Array<bool>*& array) const;
		Result	Get( const char* label, Array<float>*& array) const;
		Result	Get( const char* label, Array<char*>*& array) const;
		Result	Get( const char* label, Array<Dictionary*>*& array) const;

		void	SetEncoded( const char* label, const char* str );
		Result	GetDecoded( const char* label,	String& str ) const;

		int32_t	RemoveTag( const char* label);

	private:
		class Tag
		{
		public:
				String	tagName;
				const char*	tagType;
				int32_t	valLength;
				void*	tagValue;

				Tag();
				Tag(int32_t valS,int ntype);
				Tag(const Tag& inTag);
				~Tag();
			};

		Array<Tag*>* mTags;

		void	SetEmptyArray( const char* label, const char* type, int32_t count );
		void 	WriteToFile( Stream& f, uint32_t index, int32_t nTabs );
		bool	GetNextTag( Stream& stream, const String& );
		void	ParseTag( const char*, String&, String&, int32_t& );
		Tag* 	FindTag( const char* tagName, uint32_t& ) const;
	};

}}
