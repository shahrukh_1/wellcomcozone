/*
 * Receiver
 * Copyright (c) 1998-2012 Aurelon. All rights reserved.
 *
 * Receives messages which are broadcasted by a Transmitter
 */

#pragma once

#include "./Array.h"

namespace aur { namespace ACPL {

	class	Transmitter;

#if ACPL_WIN
#	pragma warning( disable: 4251 )
#endif

	class ACPLAPI Receiver
	{
		friend class Transmitter;
	public:
						Receiver();
						Receiver( const Receiver& inOriginal );
		virtual			~Receiver();

		inline	void	StartReceiving() { mIsReceiving = true; }
		inline	void	StopReceiving() { mIsReceiving = false; }
		inline	bool	IsReceiving() const { return mIsReceiving; }

						// Pure Virtual. Concrete subclasses must override
		virtual void	ReceiveMessage( Message inMessage, void* ioParam ) = 0;

	protected:
		Array<Transmitter*> mTransmitters;
				bool	mIsReceiving;

				void	AddTransmitter( Transmitter* inTransmitter );
				void	RemoveTransmitter( Transmitter* inTransmitter );
	};

#if ACPL_WIN
#	pragma warning( default: 4251 )
#endif

}}
