/*
 * Transmitter
 * Copyright (c) 1998-2012 Aurelon. All rights reserved.
 *
 * Transmits a message which can be received by a Receiver
 */

#pragma once

#include "./Array.h"

namespace aur { namespace ACPL {

	class	Receiver;

	const	Message		msg_TransmitterDied	= 2567;

#if ACPL_WIN
#	pragma warning( disable: 4251 )
#endif

	class ACPLAPI Transmitter
	{
	public:
						Transmitter();
						Transmitter( const Transmitter& inOriginal );
		virtual			~Transmitter();

				void	AddReceiver( Receiver* inReceiver );
				void	RemoveReceiver( Receiver* inReceiver );

		inline	void	StartTransmitting() { mIsTransmitting = true; }
		inline	void	StopTransmitting() { mIsTransmitting = false; }
		inline	bool	IsTransmitting() const { return mIsTransmitting; }

				void	TransmitMessage( Message inMessage, void* ioParam = NULL );

	protected:
		Array<Receiver*>	mReceivers;
		bool				mIsTransmitting;
	};

#if ACPL_WIN
#	pragma warning( default: 4251 )
#endif

}}
