/* MergeProfileDLL.h
 *
 * C-Interface
 * v1.0.0 Hanno Hoffstadt 11-FEB-15 
 * v1.2.0 Hanno Hoffstadt 15-APR-16, adding Monitor profile modification 
 *
 * Copyright (C) 2015-2016 GMG GmbH & Co. KG
 */

#ifndef LIBMERGEPROFILE_H
#define LIBMERGEPROFILE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MERGEPROFILE_HANDLE
#define MERGEPROFILE_HANDLE
typedef void *mergeprofile_handle;
#endif

#ifdef _MSC_VER
/* Microsoft Compiler */
#ifdef MAKEDLL_LIBMERGEPROFILE
#define DECL __declspec(dllexport)
#else
#define DECL __declspec(dllimport)
#endif
#else
/* not Microsoft Compiler */
/* for now assume GCC */
#define DECL extern
#endif

/* mergeprofile_version()
 * 
 * Get version number as string or int.
 */
DECL const char *mergeprofile_version();
DECL int mergeprofile_version_major();
DECL int mergeprofile_version_minor();
DECL int mergeprofile_version_bugfix();

/* ERROR CODES
 */

typedef enum {
    MergeNoError = 0,
    MergeErrorOpenFileForReading = -1,
    MergeErrorReadingFile = -2,
    MergeErrorOpenFileForWriting = -3,
    MergeErrorWritingFile = -4,
    MergeErrorSeekingFile = -5,
    // related to file structure
    MergeErrorCorruptProfile = -10,
    MergeErrorUnexpectedEOF = -11,
    MergeErrorCorruptTableEntry = -12,
    MergeErrorBadIndex = -13,
    MergeErrorTagNotFound = -14,
    MergeErrorBadTableSize = -15,
    MergeErrorMissingTag = -16,
    MergeErrorCorruptTag = -17,
    // related to features
    MergeErrorUnsupportedTable = -20,
    MergeErrorUnsupportedNumInChannels = -21,
    MergeErrorUnsupportedNumOutChannels = -22,
    MergeErrorUnsupportedTableFeature = -23,
    MergeErrorUnsupportedColorSpace = -24,
    MergeErrorUnsupportedTableSize = -25,
    // related to linking
    MergeErrorColorSpaceMismatch = -30,
    MergeErrorChannelMismatch = -31,
    MergeErrorNonlinearTable = -32,
    MergeErrorProfileMismatch = -33,
    // general logic
    MergeErrorCanceled = -40,
    MergeErrorPointerIsNull = -50,
    MergeErrorIncompleteSetup = -51,
    MergeErrorBadArgument = -52,
    MergeErrorSetupNoiseFailed = -53,
    MergeErrorSetupMissingDotsFailed = -54,
    MergeErrorSetupInksFailed = -55,
} MergeError;

/* MergeProfiles(...)
 *
 * Takes two profiles, creates an output profile (as file on disk) by "combination of inks".
 * If colorant sequences exist in the input profiles, or an input profile has only a single ink,
 * the colorant order of the output profile is "inks of profile 1 before those of profile 2".
 *
 * allowRelCol: if zero, creates an absolute colorimetric A2B table and a D50 media white point.
 * if non-zero, puts the media white into wtpt and makes the table relative to it.
 *
 * returns error code or 0 on success.
 */

DECL MergeError MergeProfiles(const char *utf8_path1,
                              const char *utf8_path2,
                              int useWhichWhite, /* 0 == given XYZ white, or 1, or 2. */
                              double whiteX, double whiteY, double whiteZ, /* given XYZ white */
                              const char *utf8_outpath,
                              int allowRelCol);

/* DevLab: Data structure to store wedge data.
 *
 */

struct DevLab {
    double dev; /* tone value, percentage 0..100 */
    double L, a, b;
};

/* CreateSpotProfile(...)
 *
 * Takes array of DevLab entries (ascending list of device step values with corresponding Lab values),
 * creates a single-color output profile (as file on disk).
 *
 * Note that the number of steps is currently used as the number of profile CLUT steps.
 * This can result in large profiles when used together with MergeProfiles().
 * A future version will reduce the number of steps.
 *
 * allowRelCol: if zero, creates an absolute colorimetric A2B table and a D50 media white point.
 * if non-zero, puts the media white into wtpt and makes the table relative to it.
 *
 * returns error code or 0 on success.
 */

DECL MergeError CreateSpotProfile(int numsteps,
                                  const struct DevLab *steps,
                                  const char *utf8_inkName,
                                  const char *utf8_outpath,
                                  int allowRelCol);

/* AdjustWhitePoint(...)
 *
 * Takes a profile and a new paper white as LAB.
 * Creates a modified output profile (as file on disk).
 *
 * returns error code or 0 on success.
 */

DECL MergeError AdjustProfileWhitePoint(const char *utf8_inpath,
                                        double newL, double newA, double newB,
                                        const char *utf8_outpath);

#ifdef __cplusplus
};
#endif

#endif
