#include "TileGenerator.h"
#include <ACPL/Utilities.h>
#include <ACPL/FileStream.h>
#include <ACPL/XML.h>
#include <JPEG Library/jpeglib.h>
#include "../../lcms2_DLL/include/lcms2.h"

using namespace aur;
using namespace aur::ACPL;


#define ICC_MARKER  (JPEG_APP0 + 2)	/* JPEG marker code for ICC */
#define ICC_OVERHEAD_LEN  14		/* size of non-profile data in APP2 */
#define MAX_BYTES_IN_MARKER  65533	/* maximum data len of a JPEG marker */
#define MAX_DATA_BYTES_IN_MARKER  (MAX_BYTES_IN_MARKER - ICC_OVERHEAD_LEN)
#define DPI_FACTOR 18


/*
 * This routine writes the given ICC profile data into a JPEG file.
 * It *must* be called AFTER calling jpeg_start_compress() and BEFORE
 * the first call to jpeg_write_scanlines().
 * (This ordering ensures that the APP2 marker(s) will appear after the
 * SOI and JFIF or Adobe markers, but before all else.)
 */
static void write_icc_profile (j_compress_ptr cinfo, const JOCTET *icc_data_ptr, unsigned int icc_data_len)
{
  unsigned int num_markers;	/* total number of markers we'll write */
  int cur_marker = 1;		/* per spec, counting starts at 1 */
  unsigned int length;		/* number of bytes to write in this marker */

  /* Calculate the number of markers we'll need, rounding up of course */
  num_markers = icc_data_len / MAX_DATA_BYTES_IN_MARKER;
  if (num_markers * MAX_DATA_BYTES_IN_MARKER != icc_data_len)
    num_markers++;

  while (icc_data_len > 0) {
    /* length of profile to put in this marker */
    length = icc_data_len;
    if (length > MAX_DATA_BYTES_IN_MARKER)
      length = MAX_DATA_BYTES_IN_MARKER;
    icc_data_len -= length;

    /* Write the JPEG marker header (APP2 code and marker length) */
    jpeg_write_m_header(cinfo, ICC_MARKER,
			(unsigned int) (length + ICC_OVERHEAD_LEN));

    /* Write the marker identifying string "ICC_PROFILE" (null-terminated).
     * We code it in this less-than-transparent way so that the code works
     * even if the local character set is not ASCII.
     */
    jpeg_write_m_byte(cinfo, 0x49);
    jpeg_write_m_byte(cinfo, 0x43);
    jpeg_write_m_byte(cinfo, 0x43);
    jpeg_write_m_byte(cinfo, 0x5F);
    jpeg_write_m_byte(cinfo, 0x50);
    jpeg_write_m_byte(cinfo, 0x52);
    jpeg_write_m_byte(cinfo, 0x4F);
    jpeg_write_m_byte(cinfo, 0x46);
    jpeg_write_m_byte(cinfo, 0x49);
    jpeg_write_m_byte(cinfo, 0x4C);
    jpeg_write_m_byte(cinfo, 0x45);
    jpeg_write_m_byte(cinfo, 0x0);

    /* Add the sequencing info */
    jpeg_write_m_byte(cinfo, cur_marker);
    jpeg_write_m_byte(cinfo, (int) num_markers);

    /* Add the profile data */
    while (length--) {
      jpeg_write_m_byte(cinfo, *icc_data_ptr);
      icc_data_ptr++;
    }
    cur_marker++;
  }
}

TileGenerator::TileGenerator( const TileSettings& settings ) :
mSettings( settings )
{
}

uint32_t TileGenerator::InitPage( float pageWidth, float pageHeight, bool isDPIApplied )
{
	uint32_t outputDpi = 0;

	if(!isDPIApplied) // page size is in units, calculate output pixel size using the DPI settings
	{
		outputDpi = mSettings.mMaxDpi;
		mPageWidth = (uint32_t)( pageWidth / DPI_FACTOR * mSettings.mMaxDpi );
		mPageHeight = (uint32_t)( pageHeight / DPI_FACTOR * mSettings.mMaxDpi );
		
		// Adjust DPI to fit the maximum size set
		if(mPageHeight > mSettings.mMaxOutputSize || mPageWidth > mSettings.mMaxOutputSize)
		{
			outputDpi = mPageHeight > mPageWidth ?
						(uint32_t)(mSettings.mMaxOutputSize * DPI_FACTOR / pageHeight) :
						(uint32_t)(mSettings.mMaxOutputSize * DPI_FACTOR / pageWidth);
			
			mPageWidth = (uint32_t)( pageWidth / DPI_FACTOR * outputDpi );
			mPageHeight = (uint32_t)( pageHeight / DPI_FACTOR * outputDpi );
		}		
	}
	else
	{
		mPageWidth = (uint32_t)pageWidth;
		mPageHeight = (uint32_t)pageHeight;
	}

	uint32_t maxSize = mPageHeight;

	if (mPageWidth > mPageHeight)
		maxSize = mPageWidth;

	mNrOfzoomLevels = 1;
	//Calculate number of zoom levels
	while (maxSize > mSettings.mTileSize)
	{
		mNrOfzoomLevels++;
		maxSize /= 2;
	}

	int32_t currentZoomlevel = mNrOfzoomLevels - 1;
	//Determine total number of tiles needed to be generated for this page for all zoom levels together
	mTotalNrOfTiles = 1;
	uint32_t tilesperColumn, tilesPerRows;
	while( currentZoomlevel )
	{
		tilesPerRows = ( ( mPageWidth >> ( mNrOfzoomLevels - currentZoomlevel - 1 ) ) + mSettings.mTileSize - 1 ) / mSettings.mTileSize;
		tilesperColumn = ( ( mPageHeight >> ( mNrOfzoomLevels - currentZoomlevel - 1 ) ) + mSettings.mTileSize - 1 ) / mSettings.mTileSize;
		mTotalNrOfTiles += tilesPerRows * tilesperColumn;
		currentZoomlevel--;
	}
	mRemainingNrOfTilesToProduce = mTotalNrOfTiles;

	return outputDpi;
}

void TileGenerator::SetZoomLevel( uint32_t level )
{
	mCurrentZoomLevel = level;
	mTilePositionInCurrentZoomLevel = 1;
	mNrOfVerticalTilesInCurrentZoomLevel = 0;
	// Size of page at this zoom level
	mScaledHeight = mPageHeight >> ( mNrOfzoomLevels - level - 1 );
	mScaledWidth = mPageWidth >> ( mNrOfzoomLevels - level - 1 );
	mTilesHorizontal = ( mScaledWidth + mSettings.mTileSize - 1 ) / mSettings.mTileSize;
	mTilesVertical = ( mScaledHeight + mSettings.mTileSize - 1 ) / mSettings.mTileSize;
	uint32_t maxMem = mSettings.mMaxMemoryUsage * 1024 * 1024;
	uint32_t maxLines = maxMem / ( RGB_CHANNEL_COUNT * mScaledWidth );
	//Determine how many bands will be used to process the page at this zoom level
	if( maxLines >= mScaledHeight )
	{
		mNrBands = 1;
		mBandHeight = mScaledHeight;
	}
	else
	{
		mBandHeight = maxLines / mSettings.mTileSize;
		mBandHeight = mBandHeight * mSettings.mTileSize;
		if( mBandHeight < mSettings.mTileSize )
			mBandHeight = mSettings.mTileSize;
		mNrBands = ( mScaledHeight + mBandHeight - 1 ) / mBandHeight;
	}
}

bool TileGenerator::WriteTile( uint32_t x, uint32_t yInBand, uint8_t* bandData, uint8_t* outputProfileBuffer, uint32_t outputProfileBufferLength )
{
	// tile position
	uint32_t tileX = mSettings.mTileSize * x;
	uint32_t tileY = mSettings.mTileSize * yInBand;
	uint32_t y = mNrOfVerticalTilesInCurrentZoomLevel;
	uint32_t rowbytes = RGB_CHANNEL_COUNT * mScaledWidth;
	
	if( tileX > mScaledWidth|| tileY > mScaledHeight )
		return false;

	// tile width
	uint32_t tileWidth = mSettings.mTileSize;
	if ( tileWidth > mScaledWidth ) // if we are at the smallest zoom, we could have a tile smaller than the standard size
	{
		tileWidth = mScaledWidth;
	}
	else if( ( x + 1 ) * mSettings.mTileSize > mScaledWidth ) //if it is the last tile in the row, it might be smaller than the standard width
	{				
		tileWidth = mScaledWidth - tileX;
	}

	if(tileWidth <= 0)
		return false;

	// tile height
	uint32_t tileHeight = mSettings.mTileSize;
	if( tileHeight > mScaledHeight ) // same for height at the smallest zoom level
	{
		tileHeight = mScaledHeight;
	}
	else if( ( y + 1 ) * mSettings.mTileSize > mScaledHeight ) // last row might be smaller in height as the page dimension doesn't divide exactly to tileHeight
	{
		tileHeight = mScaledHeight - mSettings.mTileSize * y;
	}

	if( tileHeight <= 0 )
		return false;
	//Zoomify tile group number
	uint32_t folderIndex = ( mRemainingNrOfTilesToProduce - ( mTilesVertical * mTilesHorizontal -  mTilePositionInCurrentZoomLevel ) - 1 ) / mSettings.mTileSize;

	FileSpec tileSpec;

	ACPL::UString folderName;
	folderName.Format( L"TileGroup%d", folderIndex );
	ACPL::FileSpec outputPathSpec, outputFolder;
	//FileSpec can represent a folder or a file. It is mandatory that paths are constructed using the FileSpec ability to receive a parent folder in its constructor. 
	//Avoid path construction in strings at all costs.
	outputPathSpec.Make( mPageOutputDataFolder.c_str() );
	outputFolder.Make( outputPathSpec, folderName );
	
	if( !outputFolder.DirectoryExists() )
	{
		outputFolder.CreateDirectory();
	}

	ACPL::UString tileName;
	tileName.Format( L"%d-%d-%d.jpg", mCurrentZoomLevel, x, y );
	tileSpec.Make( outputFolder, tileName );
	
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  FILE * fp;       /* target file */
  JSAMPROW row_pointer[1];  /* pointer to JSAMPLE row[s] */
  int row_stride;       /* physical row width in image buffer */
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);

#if ACPL_WIN
  fp = _wfopen( tileSpec.GetPOSIXPath().operator const aur::UniChar *(), L"wb" );
#else
  fp = fopen( tileSpec.GetPOSIXPath().ToUTF8(), "wb" );
#endif
  if( fp == NULL)
	  return false;

  jpeg_stdio_dest(&cinfo, fp);

  /* Step 3: set parameters for compression */

  /* First we supply a description of the input image.
   * Four fields of the cinfo struct must be filled in:
   */
  cinfo.image_width = tileWidth;  /* image width and height, in pixels */
  cinfo.image_height = tileHeight;
  cinfo.input_components = RGB_CHANNEL_COUNT;       /* # of color components per pixel */
  cinfo.in_color_space = JCS_RGB;     /* colorspace of input image */
  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, mSettings.mJpgQuality, TRUE /* limit to baseline-JPEG values */);
  jpeg_start_compress(&cinfo, TRUE);

  write_icc_profile(&cinfo, outputProfileBuffer, outputProfileBufferLength);
  row_stride = tileWidth * RGB_CHANNEL_COUNT; /* JSAMPLEs per row in image_buffer */

  uint8_t* buffer = bandData + tileY * rowbytes + tileX * RGB_CHANNEL_COUNT;

  while (cinfo.next_scanline < cinfo.image_height) {
    /* jpeg_write_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could pass
     * more than one scanline at a time if that's more convenient.
     */
    row_pointer[0] =  (JSAMPROW)buffer;
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
	buffer += rowbytes;
  }
  jpeg_finish_compress(&cinfo);
  fclose(fp);
  jpeg_destroy_compress(&cinfo);

	return true;
}

void TileGenerator::CreateTilesXML( const UTF16Char* outputPath )
{
	ACPL::FileSpec imageProperties;
	{
		ACPL::FileSpec f1;
		f1.Make( outputPath );
		imageProperties.Make( f1, L"ImageProperties.xml" );
	}

	ACPL::XML imagePropertiesXML( "IMAGE_PROPERTIES" );
	ACPL::XML::Node* xmlRoot = imagePropertiesXML.root;
	xmlRoot->AddAttribute( "WIDTH", mPageWidth );
	xmlRoot->AddAttribute( "HEIGHT", mPageHeight );
	xmlRoot->AddAttribute( "NUMTILES", mTotalNrOfTiles );
	xmlRoot->AddAttribute( "NUMIMAGES", 1 );
	xmlRoot->AddAttribute( "VERSION", 1.8 );
	xmlRoot->AddAttribute( "TILESIZE", mSettings.mTileSize );
	xmlRoot->AddAttribute( "THUMBNAIL_WIDTH", 128 );
	xmlRoot->AddAttribute( "THUMBNAIL_HEIGHT", 128 );
	xmlRoot->AddAttribute( "RESOLUTION", mSettings.mMaxDpi );
	imagePropertiesXML.Save( &imageProperties );
}

uint32_t TileGenerator::GetPageWidth() const
{
	return mPageWidth;
}

uint32_t TileGenerator::GetPageHeight() const
{
	return mPageHeight;
}

uint32_t TileGenerator::GetNrBands() const
{
	return mNrBands;
}

uint32_t TileGenerator::GetBandHeight() const
{
	return mBandHeight;
}

uint32_t TileGenerator::GetScaledHeight() const
{
	return mScaledHeight;
}

uint32_t TileGenerator::GetScaledWidth() const
{
	return mScaledWidth;
}

uint32_t TileGenerator::GetTilesHorizontal() const
{
	return mTilesHorizontal;
}

uint32_t TileGenerator::GetTilesVertical() const
{
	return mTilesVertical;
}

uint32_t TileGenerator::GetTilesVerticalInBand() const
{
	return ( mBandHeight + mSettings.mTileSize - 1 ) / mSettings.mTileSize;
}

uint32_t TileGenerator::GetNrZoomLevels() const
{
	return mNrOfzoomLevels;
}

uint32_t TileGenerator::GetCurrentZoomLevel() const
{
	return mCurrentZoomLevel;
}

uint32_t TileGenerator::GetTotalNrOfTiles() const
{
	return mTotalNrOfTiles;
}

uint32_t TileGenerator::GetPreviewDownsampleLevel() const
{	
	uint32_t maxSize = mPageHeight > mPageWidth ? mPageHeight : mPageWidth;

	uint32_t level = 1;
	while (maxSize / 2 > mSettings.mPreviewMinSize)
	{
		level++;
		maxSize /= 2;
	}

	return level;
}

std::wstring TileGenerator::GetPageOutputDataFolder() const
{
	return mPageOutputDataFolder;
}

uint32_t& TileGenerator::RemainingNfOfTiles()
{
	return mRemainingNrOfTilesToProduce;
}

uint32_t& TileGenerator::TilePositionInZoomLevel()
{
	return mTilePositionInCurrentZoomLevel;
}

uint32_t& TileGenerator::VerticalTilesInCurrentZoomLevel()
{
	return mNrOfVerticalTilesInCurrentZoomLevel;
}

void TileGenerator::SetPageOutputDataFolder(const UTF16Char* tempSpec )
{
	mPageOutputDataFolder = std::wstring(tempSpec);
}

std::wstring TileGenerator::GetTemporaryDataFilePath() const
{	
	std::wstring dataFilePath = mPageOutputDataFolder + L"\\cache.bin";
	return dataFilePath;
}
