#pragma once

#include <OpenAPI/AOI_Types.h>


class __declspec(dllexport) TileSettings
{
public:
	uint32_t				mTileSize;  //The output size used when rendering tiles.
	uint32_t				mPreviewMinSize; //Size of the desired preview in pixels.
	uint32_t				mMinZoomSize; //Not used
	uint32_t				mMaxMemoryUsage; //The maximum memory MB that the SoftProofRenderer is allowed to use when rendering a document.
	uint32_t				mMaxOutputSize; //Largest allowed diemnsion of the resulting rendering in pixels.
	float					mMaxDpi; //The resolution used for rendering in DPI
	int16_t					mJpgQuality; //Jpeg quality of the output.
	uint64_t				mVisiblePlates; //The mask containing the channels that should be rendered.
						
							TileSettings( uint32_t tileSize, uint32_t minZoomSize, uint32_t maxMemoryUsage, uint32_t maxOutputSize, float maxDpi, int16_t jpgQuality, uint32_t previewMinSize, uint64_t visiblePlates );
	void					SetVisiblePlates( uint64_t visiblePlates );
};