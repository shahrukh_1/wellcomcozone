#include "ICCProfileManager.h"

ICCProfileManager::ICCProfileManager( void* profileData, uint32_t profileSize )
{
	mProfile = cmsOpenProfileFromMem( profileData, profileSize );
}

ICCProfileManager::ICCProfileManager( const char* profilePath )
{
	mProfile = cmsOpenProfileFromFile( profilePath, "r" );
}

ICCProfileManager::~ICCProfileManager()
{
	cmsCloseProfile( mProfile );
}

bool ICCProfileManager::IsValid() const
{
	return mProfile != NULL;
}

bool ICCProfileManager::IsValidAsSimulationProfile() const
{
	cmsProfileClassSignature profileSignature = cmsGetDeviceClass( mProfile );

	//accept only Output Device Class profiles
	if( profileSignature != cmsSigOutputClass )
	{
		return false;
	}

	cmsColorSpaceSignature colorSpace = cmsGetColorSpace( mProfile );

	//accept only cmyk or xclr color spaces
	if( colorSpace != cmsSigCmykData )
	{
		return false;
	}

	return true;
}

time_t ICCProfileManager::GetCalibrationDateTag()
{
	if( cmsIsTag( mProfile, cmsSigCalibrationDateTimeTag ) )
	{
		return mktime( ( struct tm* )cmsReadTag( mProfile, cmsSigCalibrationDateTimeTag ) );
	}
	else
	{
		struct tm creationTime;
		if( cmsGetHeaderCreationDateTime( mProfile, &creationTime ) )
		{
			return mktime( &creationTime );
		}
	}

	return 0;
}

bool ICCProfileManager::GetProfileCreator( unsigned short* creatorSignature )
{
	uint32_t creator = cmsGetHeaderCreator( mProfile );

	creatorSignature[0] = (creator >> 24) & 0xFF;
	creatorSignature[1] = (creator >> 16) & 0xFF;
	creatorSignature[2] = (creator >> 8) & 0xFF;
	creatorSignature[3] = creator & 0xFF;
	creatorSignature[4] = '\0';

	return true;
}

void ICCProfileManager::GetWhitePointLab( cmsCIELab* whitePointLab )
{
	cmsCIEXYZ* whitePointXYZ = NULL;
	if( cmsIsTag( mProfile, cmsSigMediaWhitePointTag ) )
	{
		whitePointXYZ =  ( cmsCIEXYZ* )cmsReadTag( mProfile, cmsSigMediaWhitePointTag );
	}
	
	//convert xyz to Lab
	cmsXYZ2Lab( NULL, whitePointLab, whitePointXYZ != NULL ? whitePointXYZ : cmsD50_XYZ() );
}

cmsFloat64Number ICCProfileManager::CalculateDelta2000( const cmsCIELab* firstColor, const cmsCIELab* secondColor, cmsFloat64Number Kl, cmsFloat64Number Kc, cmsFloat64Number Kh )
{
	return cmsCIE2000DeltaE( firstColor, secondColor, Kl, Kc, Kh );
}

bool ICCProfileManager::IsCMYKorRGB( bool &isRGB, cmsCIELab* whitePointLab )
{
	//get colorspace
	cmsColorSpaceSignature colorSpace = cmsGetColorSpace( mProfile );

	//make sure colorspace is RGB or CMYK
	bool validColorSpace = true;
	if( colorSpace == cmsSigRgbData )
	{
		isRGB = true;
	}
	else if ( colorSpace == cmsSigCmykData )
	{
		isRGB = false;
		cmsProfileClassSignature profileSignature = cmsGetDeviceClass( mProfile );

		//accept only Output Device Class profiles
		if( profileSignature != cmsSigOutputClass )
		{
			return false;
		}

		GetWhitePointLab( whitePointLab );
	}
	else
	{
		validColorSpace = false;
	}

	return validColorSpace;
}