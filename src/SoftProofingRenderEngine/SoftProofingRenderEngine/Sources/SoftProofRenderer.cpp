#include "SoftProofRenderer.h"
#include "TileGenerator.h"
#include <ACPL/Utilities.h>
#include <ACPL/FileStream.h>
#include <ACPL/XML.h>
#include <JPEG Library/jpeglib.h>
#include "../../lcms2_DLL/include/lcms2.h"
#include "../../ProfileMerge/MergeProfile.h"
#include "../../lcms2intplugin/lcms2intplugin.h"

using namespace aur;
using namespace aur::ACPL;


SoftProofRenderer::SoftProofRenderer( RenderInstance* instance, const UTF16Char* documentPath, bool doRendering ):
mDocument( NULL ),
mRenderer( NULL ),
mTileGenerator( NULL )
{
// Because this class incorrectly contains the tile generator, it is now initialized in two ways: one to render the actual bands or previews and the seccond one to generate tile.
// Init with NULL, NULL, false for tile generation
	if(doRendering)
	{
		mDocument = new AOI_Document( instance->mInstance );
		mDocument->Open( documentPath );

		mRenderer = new AOI_SoftProofRenderer( mDocument );
	}
}

SoftProofRenderer::~SoftProofRenderer()
{
	delete mRenderer;
	delete mDocument;
	delete mTileGenerator;
}

bool SoftProofRenderer::Setup( const UTF16Char* profileFile, const UTF16Char* cmmFile, uint32_t maxNrOfChannels, bool useCustomProfile )
{	
	return mRenderer->Setup( profileFile, cmmFile, maxNrOfChannels, useCustomProfile );
}

bool SoftProofRenderer::SaveDocumentEmbeddedProfile( const UTF16Char* embeddedProfileSaveFolderPath, UTF16Char* profileName )
{
	return mRenderer->SaveDocumentEmbeddedProfile( embeddedProfileSaveFolderPath, profileName );
}

bool SoftProofRenderer::IsRGBDocumentColorSpace() const
{
	return mRenderer->IsRGBDocumentColorSpace();
}

uint32_t	SoftProofRenderer::GetDocumentChannelCount()
{
	return mRenderer->GetDocumentChannelCount();
}

bool SoftProofRenderer::InitRenderer()
{
	return initLCMS2IntPlugin();
}

const char*	SoftProofRenderer::GetDocumentChannelName( uint32_t index )
{
	return mRenderer->GetDocumentChannelName( index );
}

void SoftProofRenderer::GetSpotColorCIELab( uint32_t index, SpotColorCieLab& spotColor )
{
	 spotColor = mRenderer->GetSpotColorCIELab(index);
}

uint32_t SoftProofRenderer::GetDocumentSpotColorsCount()
{
	return mRenderer->GetDocumentSpotColorsCount();
}

void SoftProofRenderer::GetProfilePaperWhiteCIELab( SpotColorCieLab& spotColor )
{
	spotColor = mRenderer->GetProfilePaperWhiteCIELab();
}

/*!
	Renders the document creating raw data bands by using the specified TileSettings, and also a jpg preview of the document.
	It uses a multi-band algorithm that creates multiple bands based on the document height and the maximum usage of memory specified.
	It also renders the original document at double width/height and then downsampling it by two, increasing the quality of the output preview.
	Rendering at double size is needed as the renderer has no antialiasing support.

	\param	tileSettings		The settings containing information about the rendering and creation of document tiles.
	\param  outputPath			The output folder where the tiles and the preview will be saved.
	
	\return						True if the method is successfully executed otherwise false.
*/
bool SoftProofRenderer::RenderRawData( const TileSettings& tileSettings, const UTF16Char* outputPath, uint32_t pageNumber, uint32_t& outputDpi )
{
	TileGenerator tg( tileSettings );
	//Determine page size in units, calculate 
	float srcPageWidth, srcPageHeight;
	mDocument->GetPage( pageNumber )->GetDimensions( srcPageWidth, srcPageHeight );
	outputDpi = tg.InitPage( srcPageWidth, srcPageHeight );

	float dpu;
	//we need the dpu so we can pass it to the AOI renderer to determine how to scale the original document to the size we want
	//double the size as we will render at double size
	if( srcPageWidth > srcPageHeight )
	{ 
		dpu = tg.GetPageWidth() * 2 / srcPageWidth;
	}
	else 
	{ 
		dpu = tg.GetPageHeight() * 2 / srcPageHeight;
	}

	//prepare full size preview jpeg
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
	/* More stuff */
	FILE * fp;       /* target file */
	JSAMPROW row_pointer[1];  /* pointer to JSAMPLE row[s] */
	int row_stride;       /* physical row width in image buffer */
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

	ACPL::UString filePath;
	{
		ACPL::FileSpec f1;
		f1.Make( outputPath );
		if( !f1.DirectoryExists() )
			f1.CreateDirectory();
		ACPL::FileSpec f2;
		f2.Make( f1, L"Converted.jpg" );
		filePath = f2.GetPOSIXPath();
	}
#if ACPL_WIN
	fp = _wfopen( filePath.operator const aur::UniChar *(), L"wb" );
#else
	fp = fopen( filePath.ToUTF8(), "wb" );
#endif
	if( fp == NULL)
		return false;
	jpeg_stdio_dest(&cinfo, fp);
	uint32_t jpegY = 0; //we will write the preview jpeg line by line
	//end prepare jpeg

	// we set the tile generator for the max zoom level
	tg.SetZoomLevel( tg.GetNrZoomLevels() - 1 );
		
	
	uint32_t level = tg.GetPreviewDownsampleLevel(); //zoom level to get to preview size
	uint32_t sizeFactor = 1 << level; //how many times the image can be downsampled by 2 + 1 because it is double size
	uint32_t renderWidth = 2 * tg.GetScaledWidth(); //dimensions in pixels for the rendered image (double sized)
	uint32_t renderHeight = 2 * tg.GetScaledHeight();
	uint32_t renderBandHeight = 2 * tg.GetBandHeight();
	cinfo.image_width = renderWidth / sizeFactor;  /* image width and height, in pixels */
	cinfo.image_height = renderHeight / sizeFactor;
	cinfo.input_components = 3;       /* # of color components per pixel */
	cinfo.in_color_space = JCS_RGB;     /* colorspace of input image */
	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, tileSettings.mJpgQuality, TRUE /* limit to baseline-JPEG values */);
	jpeg_start_compress(&cinfo, TRUE);	
	row_stride = 2*tg.GetScaledWidth() / sizeFactor * 3;
	//for each band we render, save raw data and save also in the preview the RGB representation
	for( uint32_t band = 0; band != tg.GetNrBands(); ++band )
	{
		uint32_t outComponents = GetDocumentChannelCount(); //number of channels for raw data
		uint32_t rawBandSize = tg.GetBandHeight() * tg.GetScaledWidth() * outComponents; //size of a raw band in memory
		uint8_t* rawBandData = new uint8_t[ rawBandSize ];

		ACPL::FileStream* bandRawStream = NULL;
		ACPL::UString bandRawDataFilePath;
		bandRawDataFilePath.Format( L"Band%d.bin", band );
		{
			ACPL::FileSpec f1;
			f1.Make( outputPath, L"Bands" );
			if( !f1.DirectoryExists() )
				f1.CreateDirectory();
			ACPL::FileSpec f2;
			f2.Make( f1, bandRawDataFilePath );
			if( !f2.Exists() )
				f2.Create();
			bandRawStream = new ACPL::FileStream( f2 ); //this is where we will save the raw band data for the current band
		}

		//render the document at double width/height
		uint8_t* doubleBandRawData = mRenderer->RenderRawBand( band * renderBandHeight, renderWidth, renderBandHeight, dpu );
		//downsample by two raw data
		//The renderer creates additional channels like alpha channel and transparency groups so we must know the total number of color components for a pixel in the data provided by the renderer
		uint32_t inComponents = outComponents + ( mRenderer->HasAlpha() ? 1 : 0 ) + ( mRenderer->HasTransparencyGroup() ? 1 : 0 );
		//However we save just the output channels, igonring the additional alpha for ther raw data
		DownsampleBy2( doubleBandRawData, rawBandData, inComponents * renderWidth, renderBandHeight, renderWidth, inComponents, outComponents );
		//save raw band data
		SaveBand( rawBandData, *bandRawStream, rawBandSize );

		//cleanup raw processing
		delete[] rawBandData;
		delete bandRawStream;

		//prepare the thumbnail processing
		uint8_t* doubleBandData = NULL; 
		doubleBandData = new uint8_t[ renderBandHeight * renderWidth * CMYK_CHANNEL_COUNT ];
		uint32_t rgbBandSize = cinfo.image_width * cinfo.image_height * RGB_CHANNEL_COUNT;
		uint8_t* rgbBandData = new uint8_t[ rgbBandSize ];

		//transform raw data into rgb
		mRenderer->ConvertToOutputColorSpace( doubleBandData, renderWidth, renderBandHeight, renderWidth * CMYK_CHANNEL_COUNT );
		//downsample by n level to rgb to create thumbnail using doubleBandData
		DownsampleRGBByN(doubleBandData, rgbBandData, CMYK_CHANNEL_COUNT * renderWidth, renderBandHeight, renderWidth, CMYK_CHANNEL_COUNT, level);

		uint32_t previewY = 0;
		while( previewY < renderBandHeight / sizeFactor && jpegY < cinfo.image_height )
		{
			row_pointer[0] =  (JSAMPROW)&rgbBandData[previewY * row_stride];
			(void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
			previewY++;
			jpegY++;
		}
		delete[] doubleBandData;
		delete[] rgbBandData;
		
	}

	jpeg_finish_compress(&cinfo);
	/* After finish_compress, we can close the output file. */
	jpeg_destroy_compress(&cinfo);
	fflush( fp );
	fclose( fp );

	delete mTileGenerator;
	mTileGenerator = NULL;

	return true;
}

bool SoftProofRenderer::DownSampleTiles( uint8_t* outputProfileBuffer, uint32_t outputProfileBufferLength )
{
	//The cache.bin generated by CreateFirstLevelOfTile (1:1 zoom level)
	ACPL::FileSpec inputSpec( mTileGenerator->GetTemporaryDataFilePath().c_str() );
	ACPL::FileSpec outputSpec;

	int32_t currentZoomLevel = mTileGenerator->GetNrZoomLevels() - 2; // max zoom is already done

	while( currentZoomLevel >= 0 )
	{
		mTileGenerator->RemainingNfOfTiles() -= mTileGenerator->GetTilesHorizontal() * mTileGenerator->GetTilesVertical();
		mTileGenerator->SetZoomLevel( currentZoomLevel ); //TG needs to know the zoom level to figure out number of tiles for it
		
		outputSpec.CreateTemporary();
		{//use a block so streams are closed when block is finished
			ACPL::FileStream outputStream(outputSpec);
			ACPL::FileStream* inputFileStream = new ACPL::FileStream( inputSpec );

			for( uint32_t band = 0; band != mTileGenerator->GetNrBands(); ++band )
			{
				uint32_t bandSize = mTileGenerator->GetBandHeight() * mTileGenerator->GetScaledWidth() * RGB_CHANNEL_COUNT;
				uint8_t* bandData = new uint8_t[ bandSize ];
				
				LoadBand( bandData, *inputFileStream, bandSize );				

				for( uint32_t j = 0; j < mTileGenerator->GetTilesVerticalInBand() && mTileGenerator->VerticalTilesInCurrentZoomLevel() != mTileGenerator->GetTilesVertical(); j++ )
				{
					for( uint32_t i = 0; i < mTileGenerator->GetTilesHorizontal(); i++ )
					{
						mTileGenerator->WriteTile( i, j, bandData, outputProfileBuffer, outputProfileBufferLength ); //write each tile
						mTileGenerator->TilePositionInZoomLevel()++;
					}
					mTileGenerator->VerticalTilesInCurrentZoomLevel()++;
				}
				if( currentZoomLevel > 0 ) // prepare band data for next zoom level
				{
					uint32_t newBandSize = ( mTileGenerator->GetBandHeight() / 2 ) * ( mTileGenerator->GetScaledWidth() / 2 ) * RGB_CHANNEL_COUNT;
					uint8_t* newBand = new uint8_t[ newBandSize ];

					DownsampleRGBBy2( bandData, newBand, RGB_CHANNEL_COUNT * mTileGenerator->GetScaledWidth(), mTileGenerator->GetBandHeight(), mTileGenerator->GetScaledWidth(),
										RGB_CHANNEL_COUNT * ( mTileGenerator->GetScaledWidth() / 2 ), RGB_CHANNEL_COUNT );
					SaveBand( newBand, outputStream, newBandSize );

					delete[] newBand;
				}
				delete[] bandData;
			}
		
			delete inputFileStream;
		}

		inputSpec.Delete();
		inputSpec = outputSpec;			
		
		currentZoomLevel--;
	}
	//delete initial band buffer
	if( outputSpec.Exists() )
		outputSpec.Delete();

	return true;
}

bool SoftProofRenderer::CreateFirstLevelTiles( uint8_t* bandData, uint32_t bandSize, uint8_t* outputProfileBuffer, uint32_t outputProfilebufferLength )
{
	ACPL::FileStream outputStream(  ACPL::FileSpec( mTileGenerator->GetTemporaryDataFilePath().c_str() ) );
	
	for( uint32_t j = 0; j < mTileGenerator->GetTilesVerticalInBand() && mTileGenerator->VerticalTilesInCurrentZoomLevel() != mTileGenerator->GetTilesVertical(); j++ )
	{
		for( uint32_t i = 0; i < mTileGenerator->GetTilesHorizontal(); i++ )
		{
			mTileGenerator->WriteTile( i, j, bandData, outputProfileBuffer, outputProfilebufferLength);
			mTileGenerator->TilePositionInZoomLevel()++;
		}
		mTileGenerator->VerticalTilesInCurrentZoomLevel()++;
	}
		
	uint32_t newBandSize = ( mTileGenerator->GetBandHeight() / 2 ) * ( mTileGenerator->GetScaledWidth() / 2 ) * RGB_CHANNEL_COUNT;
	uint8_t* newBand = new uint8_t[ newBandSize ];

	DownsampleRGBBy2( bandData, newBand, RGB_CHANNEL_COUNT * mTileGenerator->GetScaledWidth(), mTileGenerator->GetBandHeight(),
						mTileGenerator->GetScaledWidth(), RGB_CHANNEL_COUNT * ( mTileGenerator->GetScaledWidth() / 2 ), RGB_CHANNEL_COUNT );
	SaveBand( newBand, outputStream, newBandSize );

	delete[] newBand;
	return true;
}

void SoftProofRenderer::InitTileSettings( const TileSettings& tileSettings, uint32_t pageNumber, const UTF16Char* pageOutputDatafolderPath, uint32_t pageWidth, uint32_t pageHeight )
{
	if(mTileGenerator != NULL)
		delete mTileGenerator;

	mTileGenerator = new TileGenerator(tileSettings);
	mTileGenerator->SetPageOutputDataFolder(pageOutputDatafolderPath);
		
	ACPL::FileSpec outputSpec( mTileGenerator->GetTemporaryDataFilePath().c_str() );
	if( !outputSpec.Exists() )
		outputSpec.Create();

	mTileGenerator->InitPage( pageWidth, pageHeight, true );

	mTileGenerator->SetZoomLevel( mTileGenerator->GetNrZoomLevels() - 1 );
}

void* SoftProofRenderer::CreateTransform( const char* inputProfilePath, const char* monitorProfilePath, const char* outputProfilePath, bool simulatePaperTint, uint32_t channels )
{
	uint32_t intent = simulatePaperTint ? INTENT_ABSOLUTE_COLORIMETRIC : INTENT_RELATIVE_COLORIMETRIC;

	cmsHPROFILE inputProfile = cmsOpenProfileFromFile( inputProfilePath, "r" );
	cmsHPROFILE monitorProfile = cmsOpenProfileFromFile( monitorProfilePath, "r" );
	cmsHPROFILE outputProfile = cmsOpenProfileFromFile( outputProfilePath, "r" );

	cmsHPROFILE	profiles[] = { inputProfile, monitorProfile, outputProfile };

	uint32_t cmsInputFormat;

	if( channels == CMYK_CHANNEL_COUNT )
	{
		cmsInputFormat = TYPE_CMYK_8;
	}
	else
	{
		cmsInputFormat = (COLORSPACE_SH(14 + channels)|CHANNELS_SH(channels)|BYTES_SH(1));
	}
	
	cmsHTRANSFORM transform =  cmsCreateMultiprofileTransform( profiles, 3, cmsInputFormat, TYPE_RGB_8, intent, 0 );

	cmsCloseProfile( inputProfile );
	cmsCloseProfile( monitorProfile );
	cmsCloseProfile( outputProfile );

	return transform;
}

void SoftProofRenderer::DeleteTransform( void* transform )
{
	cmsDeleteTransform( transform );
}

void SoftProofRenderer::DoTransform( void* transform, const void* inputBuffer, void* outputBuffer, uint32_t sizeInPixels, uint64_t channelMask, uint32_t channelCount )
{
	if(channelMask != ULONG_MAX)
	{
		ApplyMask((uint8_t*)inputBuffer, channelMask, channelCount);
	}

	cmsDoTransform( transform, inputBuffer, outputBuffer, sizeInPixels );
}

void SoftProofRenderer::ApplyMask( uint8_t* inputBuffer, uint64_t channelMask, uint32_t channelCount )
{
	uint8_t mask[MAX_CHANNELS];
	int32_t c, x, y;

	for( c = 0; c != channelCount; ++c )
		mask[c] = ( int64_t( 1 << c ) & channelMask ) == 0 ? 0 : 0xFFFF;

	for( y = 0; y != mTileGenerator->GetBandHeight(); ++y )
	{
		for( x = 0; x != mTileGenerator->GetScaledWidth(); ++x )
		{
			for( c = 0; c != channelCount; ++c )
			{
				*inputBuffer =  mask[c] & *inputBuffer;
				++inputBuffer;
			}
		}
	}
}

void SoftProofRenderer::DownsampleRGBBy2( uint8_t* inBandData, uint8_t* outBandData, uint32_t inRowbytes, uint32_t inHeight, uint32_t inWidth, uint32_t outRowBytes, uint32_t inComponents )
{
	uint8_t* dest8 = outBandData;
	uint8_t* line;
	bool	evenLine = false;
	bool	merge = true;
	uint32_t height = inHeight / 2;
	uint32_t width = inWidth / 2;
	uint32_t y = 0;
	uint32_t x = 0;
	// We accumulate 2 lines from the input into one line (sums) and then apply arithmetic mean
	uint16_t* extLine = new uint16_t[ width * inComponents ]; //we add 4 pixels to get 1 as the arithmetic mean so we need an uint16 buffer as sums will go over 255 before we apply the mean
	uint16_t* dest16;

	while( y != height )
	{
		line = inBandData;
		dest16 = extLine;
		if( !evenLine ) // cleanup extLine buffer
			memset( extLine, 0, 2 * width * inComponents );
		else // each two rows we increase the y 
			y++;
		x = 0;
		merge = true;
		// add each two pixels from input into extLine.
		while( x != width )
		{
			*dest16++ += *line++;
			*dest16++ += *line++;
			*dest16++ += *line++;
			line += inComponents - RGB_CHANNEL_COUNT;
			
			if( merge ) //go back so we add the second pixel
				dest16 -= RGB_CHANNEL_COUNT;
			else //move forward, we just added two pixels
				x++;
			merge = !merge;
		}
		//move to the next row in the input buffer. We use row bytes as there might be padding at the end of the row
		inBandData += inRowbytes;

		if( evenLine ) //we added up two lines from input, time to dump 8bit into the destination buffer
		{
			x = 0;
			dest16 = extLine;
			while( x != width )
			{
				*dest8++ = (uint8_t)( *dest16++ >> 2 );
				*dest8++ = (uint8_t)( *dest16++ >> 2 );
				*dest8++ = (uint8_t)( *dest16++ >> 2 );
				x++;
			}
		}
		evenLine = !evenLine;
	}
	delete[] extLine;
}

void SoftProofRenderer::DownsampleRGBByN( uint8_t* inBandData, uint8_t* outBandData, uint32_t inRowbytes, uint32_t inHeight, uint32_t inWidth, uint32_t inComponents, uint32_t level )
{
	uint8_t* dest8 = outBandData;
	uint8_t* line;
	bool	mergeLine = true;
	bool	mergeX = true;
	uint32_t sizefactor = 1 << level;
	uint32_t height = inHeight / sizefactor;
	uint32_t width = inWidth / sizefactor;
	uint32_t y = 0;
	uint32_t x = 0;

	uint32_t* extLine = new uint32_t[ width * inComponents ];
	uint32_t* dest16;
	
	while( y != inHeight )
	{
		line = inBandData;
		dest16 = extLine;
		if( mergeLine )
			memset( extLine, 0, 4 * width * inComponents );
		
		x = 0;
		mergeX = true;
		
		while( x != inWidth )
		{
			*dest16++ += *line++;
			*dest16++ += *line++;
			*dest16++ += *line++;
			line += inComponents - RGB_CHANNEL_COUNT;
			
			x++;
			mergeX = !(x % sizefactor == 0);

			if( mergeX )
				dest16 -= RGB_CHANNEL_COUNT;

			
		}
		inBandData += inRowbytes;

		y++;
		mergeLine = (y % sizefactor == 0);

		if( mergeLine ) //dump 8bit
		{
			x = 0;
			dest16 = extLine;
			while( x != width )
			{
				*dest8++ = (uint8_t)( *dest16++ >> (2 * level) );
				*dest8++ = (uint8_t)( *dest16++ >> (2 * level) );
				*dest8++ = (uint8_t)( *dest16++ >> (2 * level) );
				x++;
			}
		}		
	}
	delete[] extLine;
}

void SoftProofRenderer::DownsampleBy2( uint8_t* inBandData, uint8_t* outBandData, uint32_t inRowbytes, uint32_t inHeight, uint32_t inWidth, uint32_t inComponents, uint32_t outComponents )
{
	uint8_t* dest8 = outBandData;
	uint8_t* line;
	bool	evenLine = false;
	bool	merge = true;
	uint32_t height = inHeight / 2;
	uint32_t width = inWidth / 2;
	uint32_t y = 0;
	uint32_t x = 0;
	uint16_t alpha;
	uint16_t backDrop;

	uint16_t* extLine = new uint16_t[ width * outComponents ];
	uint16_t* dest16;

	while( y != height )
	{
		line = inBandData;
		dest16 = extLine;
		if( !evenLine )
			memset( extLine, 0, 2 * width * outComponents );
		else
			y++;
		x = 0;
		merge = true;
		while( x != width )
		{
			//Apply the alpha channel
			if( ( alpha = line[inComponents - 1] ) == 255 ) //no transparency, use it as it is
			{
				for( uint32_t i = 0; i < outComponents; ++i )
				{
					*dest16++ += *line++;
				}
			}
			else if( alpha == 0 ) //fully transparent, put white
			{
				for( uint32_t i = 0; i < outComponents; ++i )
				{
					*dest16++ += mRenderer->IsSubstractive() ? 0 : 255;
					++line;
				}
			}
			else //apply alpha
			{
				backDrop = ( 255 - alpha ) << 8;
				for( uint32_t i = 0; i < outComponents; ++i )
				{
					if(mRenderer->IsSubstractive())
					{
						*dest16++ += uint16_t( ( *line++ * alpha ) >> 8 );
					}
					else
					{
						*dest16++ += uint16_t( ( backDrop + *line++ * alpha ) >> 8 );
					}
				}
			}
			line += ( inComponents - outComponents );

			if( merge )
				dest16 -= outComponents;
			else
				x++;
			merge = !merge;
		}
		inBandData += inRowbytes;

		if( evenLine ) //dump 8bit
		{
			x = 0;
			dest16 = extLine;
			while( x != width )
			{
				for( uint32_t i = 0; i < outComponents; ++i )
				{
					*dest8++ = (uint8_t)( *dest16++ >> 2 );
				}
				x++;
			}
		}
		evenLine = !evenLine;
	}
	delete[] extLine;
}

void SoftProofRenderer::SaveBand( const uint8_t* bandData, ACPL::FileStream& fileStream, int32_t byteCount )
{
	fileStream.SetMarker( fileStream.GetLength(), eFromStart );
	fileStream.PutBytes( bandData, byteCount );
}

void SoftProofRenderer::LoadBand( uint8_t* bandData, ACPL::FileStream& fileStream, int32_t byteCount )
{
	fileStream.GetBytes( bandData, byteCount );
}

/*!
	Renders the document using the specified rectangle, and the profiles used in setup method.
	It also saves the output in the specified format to a file.

	\param	outputPath		The path where the rendering result will be saved.
	\param thumbnailType	jpg or png
	\param	x				X coordinate of the Top-Left corner of the desired tile
	\param	y				Y coordinate of the Top-Left corner of the desired tile
	\param	width			The width of the desired tile
	\param	height			The height of the desired tile
	\param quality			Quality factor when saving as JPEG.
	\param	visiblePlates	The mask that describes which channels should be included in the output

	\return					TRUE if the method is successfully executed otherwise false.
*/
bool SoftProofRenderer::RenderPage( const UTF16Char* outputPath, const char* thumbnailType, uint32_t thumbnailMaxSize, uint32_t pageNumber, uint32_t quality )
{
	return mRenderer->RenderPage( outputPath, thumbnailType, thumbnailMaxSize, pageNumber, quality );
}

void SoftProofRenderer::GetPageDimenssions( uint32_t pageNumber, float& width, float& height ) const
{
	mDocument->GetPage( pageNumber )->GetDimensions( width, height );
}

bool SoftProofRenderer::GetImageSize( int32_t& width, int32_t& height ) const
{
	return mRenderer->GetImageSize( width, height );
}

uint32_t SoftProofRenderer::GetPagesCount() const
{
	return mDocument->GetPageCount();
}

int SoftProofRenderer::CreateMergedProfile(const char* channelName, double* labValues, uint32_t steps, const char* inputProfilePath, const char* outputProfilePath, const char* tempProfilePath)
{
	int err = 0;
	
	struct DevLab* plateSteps = new DevLab[steps];
	const int sampleSize = 4;
	int sample = 0;

	while (sample < steps)
	{
		plateSteps[sample].dev = labValues[sample];
		plateSteps[sample].L =	labValues[(sample * sampleSize) + 1];
		plateSteps[sample].a = labValues[(sample * sampleSize) + 2];
		plateSteps[sample].b = labValues[(sample * sampleSize) +3];
		sample++;
	}

	err = CreateSpotProfile(steps, plateSteps, channelName, tempProfilePath, 1);
	if( !err )
		err = MergeProfiles(inputProfilePath, tempProfilePath, 1, 0., 0., 0., outputProfilePath, 1);

	delete[] plateSteps;

	return err;
}

int SoftProofRenderer::ReplaceProfileWhitePoint(const char* inputProfilePath, double newL, double newA, double newB, const char* outputProfilePath)
{
	return AdjustProfileWhitePoint(inputProfilePath, newL, newA, newB, outputProfilePath);
}
