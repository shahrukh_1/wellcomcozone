#pragma once
//#include "../../AOIBuild/OpenAPI/AOI_Instance.h"
#include <OpenAPI/AOI_Instance.h>

class  __declspec(dllexport) RenderInstance {
	friend class SoftProofRenderer;
	AOI_Instance* mInstance;

public:
	RenderInstance( bool useFontAntiAliasing, float greekSize );
	~RenderInstance();

	static void Startup(const char* uniqueID, const char* licenseKey, const char* appSignature = NULL);
	static void Terminate();
};