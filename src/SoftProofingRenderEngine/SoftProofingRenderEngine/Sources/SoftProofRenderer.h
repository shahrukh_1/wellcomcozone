#pragma once

#include <OpenAPI/AOI_SoftProofRenderer.h>
#include "TileSettings.h"
#include "RenderInstance.h"
#include "TileGenerator.h"
#include "ACPL/FileSpec.h"
#include <OpenAPI/AOI_Document.h>

namespace aur {namespace ACPL {
	class	FileStream;
}}

class __declspec(dllexport) SoftProofRenderer
{
public:
	SoftProofRenderer( RenderInstance* instance, const UTF16Char* documentPath, bool doRendering );
	~SoftProofRenderer();
	// Set the profiles: proof, output, max nr of channels, override embedded profiles. See AOI documentation. cmmFile should be renamed.
	bool			Setup( const UTF16Char* profileFile, const UTF16Char* cmmFile, uint32_t maxNrOfChannels, bool useCustomProfile );
	// Extract the embedded profile, see AOI documentation.
	bool			SaveDocumentEmbeddedProfile( const UTF16Char* embeddedProfileSaveFolderPath, UTF16Char* profileName );
	// Returns true if we have an RGB based document
	bool			IsRGBDocumentColorSpace() const;
	// Return the total number of channels at which the document will be rendered for raw data
	uint32_t		GetDocumentChannelCount();
	// Returns the channel name (raw data)
	const char*		GetDocumentChannelName( uint32_t index );
	//Returns the page dimensions in DPU (DotsPerUnit)
	void			GetPageDimenssions( uint32_t pageNumber, float& width, float& height ) const;
	// returns the pixel size of the first image in the document 
	bool			GetImageSize(int32_t& width, int32_t& height) const;
	// Number of pages of the document
	uint32_t		GetPagesCount() const;
	// Renders and saves the preview/thumbnail of a specific page
	bool			RenderPage( const UTF16Char* outputPath, const char* thumbnailType, uint32_t thumbnailMaxSize, uint32_t pageNumber, uint32_t quality = 100 );
	// Renders the document creating raw band data by using the specified TileSettings, and also a jpg preview of the document.
	// outputDpi returns the DPI resulting from setting a certain output size, compared to the page initial size
	bool			RenderRawData( const TileSettings& tileSettings, const UTF16Char* outputPath, uint32_t pageNumber, uint32_t& outputDpi );
	// Generates all the RGB tiles (expect the 1:1 zoom level) using the output from CreateFirstLevelTiles
	bool			DownSampleTiles( uint8_t* outputProfileBuffer, uint32_t utputProfilebufferLenght );
	// Creates the 1:1 RGB tiles. Parameters: RGB band data, size of buffer, embedded rgb profile for tiles files.
	// Updates the internal TileGenerator so it will know what to do with the next band of data.
	// Downsamples the data and saves it into the cache for the next zoom level
	bool			CreateFirstLevelTiles( uint8_t* bandData, uint32_t bandSize, uint8_t* outputProfileBuffer, uint32_t utputProfilebufferLenght );
	// Initializes the TileGenerator with the provided settings. Page size needs to be already determined in pixels and will override the DPI calculation.
	void			InitTileSettings( const TileSettings& tileSettings, uint32_t pageNumber, const UTF16Char* pageOutputDatafolderPath, uint32_t pageWidth, uint32_t pageHeight );
	//Initializes LittleCMS, call this once before generating tiles
	static bool		InitRenderer();
	//Create the LCMS transform using: proofing profile, monitor correction profile, rgb output profile
	// channels refers to the proofing profile number of channels which is usually CMYK+SPOTS
	//returns an opaque pointer which is passed to DoTransform
	static void* 	CreateTransform( const char* inputProfile, const char* monitorProfile, const char* outputProfile, bool simulatePaperTint, uint32_t channels );
	// Cleans the transform (close)
	static void		DeleteTransform( void* trasnform );
	//Applies the transform on raw data (such as CMYK+SPOTS) to get to RGB. Applies the channelMask (channels on/off). ChannelCount is the number of channels in raw data
	//Size in pixels is the size of the buffer in pixels. Buffers must be allocated and maanged outside of the method.
	void			DoTransform( void* trasnform, const void* inputBuffer, void* outputBuffer, uint32_t sizeInPixels, uint64_t channelMask, uint32_t channelCount );
	// Wrapper over the AOI method, gets the color information for a channel
	void			GetSpotColorCIELab( uint32_t index, SpotColorCieLab& spotColor );
	// Number of extra channels, see AOI documentation
	uint32_t		GetDocumentSpotColorsCount();
	// Returns the paper white information, see AOI documentation
	void			GetProfilePaperWhiteCIELab( SpotColorCieLab& spotColor );
	// Calls the GMG merge profile library. Given an existing ICC profile, add a new channel to it defined by name and Lab values resulting a new profile.
	// in order to use the LCMS transform to convert RAW data to RGB, a standard ICC profile of CMYK+SPOTS profile needs to be created using this method, only if additional spots are present.
	static int		CreateMergedProfile(const char* channelName, double* labValues, uint32_t steps,const char* inputProfilePath, const char* outputProfilePath, const char* tempProfilePath);
	// Calls the GMG merge profile lib to replace the white point in a profile. Please check the GMG header.
	static int		ReplaceProfileWhitePoint(const char* inputProfilePath, double newL, double newA, double newB, const char* outputProfilePath);

private:
	AOI_SoftProofRenderer*	mRenderer;
	AOI_Document*			mDocument;
	TileGenerator*			mTileGenerator;

	//use the passed stream to load byteCount bytes of data into the buffer. Buffer needs to be allocation before calling.
	void		LoadBand( uint8_t* bandData, aur::ACPL::FileStream& stream, int32_t byteCount );
	//save the band data into stream by appending it to the end of the file.
	void		SaveBand( const uint8_t* bandData, aur::ACPL::FileStream& stream, int32_t byteCount );
	//Downsample by 2 a band of data, one version optimized for RGB data, one for downsampling RAW data
	void		DownsampleRGBBy2( uint8_t* inBandData, uint8_t* outBandData, uint32_t inRowbytes, uint32_t inHeight, uint32_t inWidth, uint32_t outRowBytes, uint32_t inComponents );
	//Downsample of raw data has also one important job: the data from the renderer contains also an alpha channel. This is now interpreted by this method and the alpha channel is incorporated into the channels.
	void		DownsampleBy2( uint8_t* inBandData, uint8_t* outBandData, uint32_t inRowbytes, uint32_t inHeight, uint32_t inWidth, uint32_t inComponents, uint32_t outComponents );
	//Downsample n times a band of RGB data
	void		DownsampleRGBByN( uint8_t* inBandData, uint8_t* outBandData, uint32_t inRowbytes, uint32_t inHeight, uint32_t inWidth, uint32_t inComponents, uint32_t level );
	//apply channel mask on raw data by zeroing "off" channels 
	void		ApplyMask( uint8_t* rawBand, uint64_t channelMask, uint32_t channelCount );
};