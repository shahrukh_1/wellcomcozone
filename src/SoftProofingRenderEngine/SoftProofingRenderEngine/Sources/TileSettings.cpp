#include "TileSettings.h"

/*!
	Constructs a new AOI_TileSettings. This class contains the parameters for the rendering process
	of the tiles that are passed to the SoftProof renderer.

	\param	tileSize		The output size used when rendering tiles.
	\param  minZoomSize		The path where the 1:1 preview is saved.
	\param	maxMemoryUsage	The maximum memory that the SoftProofRenderer is allowed to use when rendering a document.
	\param	maxDpi 			The resolution used for rendering.
	\param	jpgQuality 		Jpeg quality of the output.
	\param	visiblePlates 	The mask containing the channels that should be rendered.
*/
TileSettings::TileSettings( uint32_t tileSize, uint32_t minZoomSize, uint32_t maxMemoryUsage, uint32_t maxOutputSize, float maxDpi, int16_t jpgQuality, uint32_t previewMinSize, uint64_t visiblePlates )
{
	mTileSize = tileSize;
	mMinZoomSize = minZoomSize;
	mMaxMemoryUsage = maxMemoryUsage;
	mMaxOutputSize = maxOutputSize;
	mMaxDpi = maxDpi;
	mJpgQuality = jpgQuality;
	mVisiblePlates = visiblePlates;
	mPreviewMinSize = previewMinSize;
}

/*!
	Sets the mask containing the channels that should be rendered.

	\param	visiblePlates	Mask containing the channels that will be rendered.

*/
void TileSettings::SetVisiblePlates( uint64_t visiblePlates )
{
	mVisiblePlates = visiblePlates;
}