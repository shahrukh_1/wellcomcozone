#pragma once
#include "../../lcms2_DLL/include/lcms2.h"
#include <stdint.h>
#include <string>

class __declspec(dllexport) ICCProfileManager {
	cmsHPROFILE		mProfile;
public:
	ICCProfileManager( void* profileData, uint32_t profileSize );
	ICCProfileManager( const char* profilePath );
	~ICCProfileManager();

	time_t		GetCalibrationDateTag();
	bool		GetProfileCreator( unsigned short* creator );
	void		GetWhitePointLab( cmsCIELab* whitePointLab );
	bool		IsValid() const;
	bool		IsValidAsSimulationProfile() const;
	bool		IsCMYKorRGB( bool &isRGB, cmsCIELab* whitePointLab );

	static double	CalculateDelta2000( const cmsCIELab* firstColor, const cmsCIELab* secondColor, double Kl, double Kc, double Kh );
};