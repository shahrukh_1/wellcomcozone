#pragma once
#include "TileSettings.h"
#include <OpenAPI/AOI_Types.h>
#include <string>

#define CMYK_CHANNEL_COUNT	4
#define RGB_CHANNEL_COUNT	3

class TileGenerator
{
public:
	TileGenerator( const TileSettings& );
	// Initializes the generator for a page. You can set the page size in pixels or set isDPIApplied to false and let it calculate everything based on tile settings
	//So page size has double meaning: either it is in units for isDPIApplied false or pixels otherwise. Note: these should be two different methods.
	//Return the output dpi
	uint32_t				InitPage( float pageWidth, float pageHeight, bool isDPIApplied = false );
	// Zoom level: 0 smallest, ZoomLevels-1 "original"
	// Will make the generator prepare the needed information to start processing this zoom level
	void					SetZoomLevel( uint32_t level );
	// Set the folder where the generator will dump the tiles for a certain page, could be moved to InitPage
	void					SetPageOutputDataFolder(const UTF16Char* pageOutputData );
	// Writes a tile image: horizontal tile index, vertical tile index in current band, bandData is the band buffer
	// profile buffer is the memory data of the icc profile to be embedded in the tile file, so we can embed the sRGB or AdobeRGB profiles in the tiles.
	bool					WriteTile( uint32_t x, uint32_t yInBand, uint8_t* bandData, uint8_t* outputProfileBuffer, uint32_t outputProfileBufferLength );
	//Saves the tile generation details as xml in a designated folder
	void					CreateTilesXML( const UTF16Char* outputPath );

	uint32_t				GetPageWidth() const;
	uint32_t				GetPageHeight() const;
	//Number of bands needed to process a page at the current zoom level
	uint32_t				GetNrBands() const;
	uint32_t				GetBandHeight() const;
	uint32_t				GetNrZoomLevels() const;
	//Number of horizontal tiles for the current zoom level
	uint32_t				GetTilesHorizontal() const;
	//Number of vertical tiles for the current zoom level
	uint32_t				GetTilesVertical() const;
	//How many tile rows does a band of data contain
	uint32_t				GetTilesVerticalInBand() const;
	//Width of page at current zoom level
	uint32_t				GetScaledWidth() const;
	//Height of page at current zoom level
	uint32_t				GetScaledHeight() const;
	uint32_t				GetCurrentZoomLevel() const;
	uint32_t				GetTotalNrOfTiles() const;
	// The needed zoom level to get the preview size
	uint32_t				GetPreviewDownsampleLevel() const;
	std::wstring			GetPageOutputDataFolder() const;
	uint32_t&				RemainingNfOfTiles();
	// How many tiles have been produced at the current zoom level. Must be incremented each time we write a tile so the system can figure out the tile group.
	uint32_t&				TilePositionInZoomLevel();
	// How many tile rows have been written already. Must be incremented each time a horizontal row of tiles is written.
	uint32_t&				VerticalTilesInCurrentZoomLevel();
	// Returns the absolute path of the file where the band data is cached
	std::wstring			GetTemporaryDataFilePath() const;

private:
	TileSettings			mSettings;
	uint32_t				mPageWidth;
	uint32_t				mPageHeight;
	uint32_t				mNrBands;
	uint32_t				mBandHeight;
	uint32_t				mNrOfzoomLevels;
	uint32_t				mCurrentZoomLevel;
	uint32_t				mTilesHorizontal;
	uint32_t				mTilesVertical;
	uint32_t				mScaledWidth;
	uint32_t				mScaledHeight;
	uint32_t				mTotalNrOfTiles;
	uint32_t				mRemainingNrOfTilesToProduce;
	uint32_t				mTilePositionInCurrentZoomLevel;
	uint32_t				mNrOfVerticalTilesInCurrentZoomLevel;
	std::wstring			mPageOutputDataFolder;
};