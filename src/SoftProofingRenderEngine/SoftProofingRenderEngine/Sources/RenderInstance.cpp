#include "RenderInstance.h"


RenderInstance::RenderInstance( bool useFontAntiAliasing, float greekSize )
{
	mInstance = new AOI_Instance();
	mInstance->SetGreekSize( greekSize );
	mInstance->SetFontAntiAliasing( useFontAntiAliasing );
}

RenderInstance::~RenderInstance()
{
	delete mInstance;
}

void RenderInstance::Startup(const char* uniqueID, const char* licenseKey, const char* appSignature)
{
	AOI_Instance::Startup( uniqueID, licenseKey, appSignature );
}

void RenderInstance::Terminate()
{
	AOI_Instance::Terminate();
}