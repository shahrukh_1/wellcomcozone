// tetraint6_multi.h

#ifndef TETRAINT6_MULTI_H
#define TETRAINT6_MULTI_H

#include "tetraint6.h"

#ifdef __cplusplus
extern "C" {
#endif

	// declare all variants
	void FUNCNAME(0,0)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAME(0,1)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAME(1,0)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAME(1,1)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

#ifdef __cplusplus
}
#endif

#endif // TETRAINT6_MULTI_H
