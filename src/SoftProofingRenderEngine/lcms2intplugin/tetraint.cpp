// tetraint.cpp
//
// multidimensional tetrahedral interpolation for uint16
// assumes native endianness.

#define DEBUG_INT 0

#ifndef TETRAINT_CPP
#define TETRAINT_CPP
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
enum { MaxChannels = 16 };
#endif // TETRAINT_CPP

#include "tetraint.h"

/* define PRECISE_CLUT_INTEGER to either 0 or 1. default: precise */
#ifndef PRECISE_CLUT_INTEGER
#define PRECISE_CLUT_INTEGER 1
#endif

/* define ROUND_INTEGER to either 0 or 1. default: round */
/* if ROUND128 == 128, the 16*16 bit multiplication results are rounded when reducing to 24 bits. */
#ifndef ROUND_INTEGER
#define ROUND_INTEGER 1
#endif

#undef ROUND128
#if ROUND_INTEGER
#define ROUND128 128
#else
#define ROUND128 0
#endif

// FUNCNAME() depends on BASENAME and compile-time variations, see headers.

void FUNCNAME(PRECISE_CLUT_INTEGER,ROUND_INTEGER)
	(uint16 *raw_in, uint16 *raw_out,
	 uint32 numPixels,
	 const uint16 *pclut,
	 unsigned int nin, unsigned int nout,
	 const unsigned int *nclut,
	 long stride_in, long stride_out)
{
    unsigned int iin, iout; /* input and output channel index */
	signed int siin; /* input channel index; signed needed for multidim loop */
	unsigned int is; /* input channel index for inserting/sorting */
    uint32 weight; /* interpolation weight of current CLUT point */
    uint16 outtmp[MaxChannels]; /* current CLUT entry, 16-bit */
    uint32 pout32[MaxChannels]; /* temporary result: 32-bit sum */
    uint32 findex[MaxChannels + 1]; /* floating-point index into CLUT */
    uint16 iindex[MaxChannels]; /* integer index into CLUT per channel */
	unsigned int factor[MaxChannels], ftmp;
	uint32 i; /* pixel count */

	int seq[MaxChannels + 1]; /* sequence of sorted fractions */
	unsigned int ishift; /* insertion place, ... */
	const uint16 *p, *q;

#if DEBUG_INT
    printf("--------------------------------------------------------------\n");
#endif
    uint16 *pin = raw_in, *pout = raw_out;

	ftmp = nout;
	for(siin = nin - 1; siin >= 0; siin--) {
		factor[siin] = ftmp;
		ftmp *= nclut[siin];
	}

    /* check limit due to adding weighted grid points with 24-bit */
    /* assert(nin <= 255); */

    if(stride_in == 0) stride_in = nin;
    if(stride_out == 0) stride_out = nout;

    /* floating-point code in [] for comparison */
    for(i = 0; i < numPixels; ++i) {
		for(iin = 0; iin < nin; ++iin) {
			/* per channel: calculate (lower) index into CLUT
			 * [findex[iin] = pin[iin] * (nclut[iin] - 1) / 65535.;]
			 * -> we use a 32-bit fixed point rep u16.16 or s15.16, which?
			 * 31-bit range: safe (pin is 16 bit, nclut rarely >8 bit)
			 */
			findex[iin] = pin[iin] * (nclut[iin] - 1);
#if PRECISE_CLUT_INTEGER
			/* replace expression x:65535 by x:65536 * 65536/65535
			 * and then 65536/65535 by 65537/65536, thus we have
			 * x:65535 ~= x:65536 * (1 + 1:65536) with an error of
			 * 1 bit in 32 bit (65537*65535)/(65536*65536).
			 * this is about as precise as a 32-bit float!
			 *
			 * just using x:65536 instead of x:65535 makes an error of
			 * 1 bit in 16 bit.
			 *
			 * but we always have the complication of handling
			 * an input value of 65535. */

			findex[iin] += (findex[iin] >> 16);
#endif

			/* let iindex (= actual CLUT step) be in range 0 .. ngrid - 2
			 * so that we can safely use iindex and iindex+1 for inter/extrap.
			 * [if(findex[iin] < 0.) iindex[iin] = 0; // cannot be negative]
			 * -> cannot happen, pin >= 0 always
			 * [else if(findex[iin] >= nclut[iin] - 1.) iindex[iin] = nclut[iin] - 2; ]
			 * [else iindex[iin] = int(floor(findex[iin]));]
			 * -> we start with a direct correspondence.
			 * -> later we might save some bit shifts
			 */
			if(findex[iin] >= (nclut[iin] - 1) << 16) iindex[iin] = nclut[iin] - 2;
			else iindex[iin] = (uint16)(findex[iin] >> 16);

			/* subtract integer index, get what's left (as interpolation weight)
			 * [findex[iin] -= iindex[iin];]
			 */
			findex[iin] -= iindex[iin] << 16;

			/* when interpolating, findex will be >= 0 and < 1 (here: < 65536)
			 * findex == 1 only together with iindex == ngrid-2
			 * when extrapolating, findex will be < 0 or > 1
			 */
		}

		/* int seq[MaxChannels + 1]; // sequence of sorted fractions
		 * fill seq array with indices so that
		 *   findex[seq[0]] >= findex[seq[1]] >= ...
		 * this will not work for extrapolation (findex < 0),
		 * possibly we have to use fabs()
		 */
		seq[0] = 0;
		for(iin = 1; iin < nin; ++iin) {
			/* find correct place "is" to insert */
			for(is = 0; is < iin; ++is)
				if(findex[seq[is]] < findex[iin]) break;
			/* insert (or append if is==iin) at position is */
			for(ishift = iin; ishift > is; ishift--)
				seq[ishift] = seq[ishift - 1];
			seq[is] = iin;
		}

		/* mark end of sequence by findex[seq[nin]] = 0.
		 * this is convenient for calculating weights, see below
		 */
		seq[nin] = nin;
		findex[nin] = 0;

		/* lookup of base point
		 * store offset to lower CLUT grid point
		 * because we want to calculate it only once
		 * ibase is not needed here
		 */
		p = pclut;
		for(iin = 0; iin < nin; ++iin) {
			p += iindex[iin] * factor[iin];
		}

		/* base lookup */
		q = (uint16*)p; /* native 16-bit access */
		for(iout = 0; iout < nout; ++iout) outtmp[iout] = *q++;

		/* [weight = 1. - findex[seq[0]];]
		 * -> our weight will be shifted by 16 bit, thus we have
		 */
		weight = 65536 - findex[seq[0]];

		/* put weighted result into result */
		for(iout = 0; iout < nout; iout++) {
			/* [pout[iout] = outtmp[iout] * weight;]
			 * -> the product is 32-bit safe, but not 31-bit safe, since both
			 * outtmp and weight can use the full 16 bit range.
			 * what if weight exactly == 65536 (17 bit)?
			 * no problem, since outtmp always < 65536, and weight <= 65536,
			 * the product is always <= 65535*65536, which is 32-bit.
			 * BUT!!! now we add up nin+1 32-bit products,
			 * which can easily exceed the 32-bit range.
			 * this is even worse with multilinear interpolation,
			 * which adds 2**nin products.
			 *
			 * option 1: use 64-bit integers
			 * option 2... shift all these products by 8 bits, which
			 * leaves 24 bit precision and is sufficient for
			 * 255 input channels (tetrahedral)
			 * 8 input channels (multilinear)
			 */
			pout32[iout] = (outtmp[iout] * weight + ROUND128) >> 8;
		}
		/* lookup of tetrahedral vertex sequence */
		for(is = 0; is < nin; is++) {
			p += factor[seq[is]]; /* directly perform ibase increment in p */
			weight = findex[seq[is]] - findex[seq[is+1]];
			/* this goes up to nin instead of nin-1
			 * (we made the arrays 1 larger)
			 */
#if DEBUG_INT
			printf(" w %5.3f\n", weight / 65536.);
#endif
			if(weight) { /* add only if non-zero weight */
				q = (uint16*)p; /* native 16-bit access */
				for(iout = 0; iout < nout; ++iout) outtmp[iout] = *q++;

				/* add weighted result to final result */
				for(iout = 0; iout < nout; iout++) {
					/* comments see above... */
					pout32[iout] += (outtmp[iout] * weight + ROUND128) >> 8;
				}
			}
		}
		/* tetrahedral interpolation done. */
		/* copy the added 24-bit results to the 16-bit output array */
		for(iout = 0; iout < nout; iout++) {
			pout[iout] = (uint16)((pout32[iout] + ROUND128) >> 8); /* rounding */
		}
		pin += stride_in;
		pout += stride_out;
    }
}
