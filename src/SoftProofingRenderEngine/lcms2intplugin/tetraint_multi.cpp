// tetraint_multi.cpp
//
// generates 4 variations of functions from tetraint.cpp

#define PRECISE_CLUT_INTEGER 0
#define ROUND_INTEGER 0
#include "tetraint.cpp"

#undef ROUND_INTEGER
#define ROUND_INTEGER 1
#include "tetraint.cpp"

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 0
#include "tetraint.cpp"

#undef ROUND_INTEGER
#define ROUND_INTEGER 1
#include "tetraint.cpp"
