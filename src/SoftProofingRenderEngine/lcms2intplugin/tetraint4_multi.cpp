// tetraint4_multi.cpp
//
// generates 4 variations of functions from tetraint4.cpp

#include "tetraint4_multi.h"

#undef FIXED_NCLUT
#undef FIXED_NOUT

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 0
#define ROUND_INTEGER 0
#include "tetraint4.cpp"

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 0
#define ROUND_INTEGER 1
#include "tetraint4.cpp"

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 0
#include "tetraint4.cpp"

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 1
#include "tetraint4.cpp"

// and also fixed variants
#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER
#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 1

#undef FIXED_NCLUT

#undef FIXED_NOUT
#define FIXED_NOUT 3
#include "tetraint4.cpp"
#undef FIXED_NOUT
#define FIXED_NOUT 4
#include "tetraint4.cpp"

#define FIXED_NCLUT 17

#undef FIXED_NOUT
#include "tetraint4.cpp"
#undef FIXED_NOUT
#define FIXED_NOUT 3
#include "tetraint4.cpp"
#undef FIXED_NOUT
#define FIXED_NOUT 4
#include "tetraint4.cpp"

#undef FIXED_NCLUT
#define FIXED_NCLUT 33

#undef FIXED_NOUT
#include "tetraint4.cpp"
#undef FIXED_NOUT
#define FIXED_NOUT 3
#include "tetraint4.cpp"
#undef FIXED_NOUT
#define FIXED_NOUT 4
#include "tetraint4.cpp"
