#include "..\lcms2_DLL\include\lcms2_plugin.h"
//#include "lcms2.h"

#include "tetraint2_multi.h"
#include "tetraint3_multi.h"
#include "tetraint4_multi.h"
#include "tetraint5_multi.h"
#include "tetraint6_multi.h"
#include "tetraint.h"

#define DEBUG_FACTORY 0

static
void tetran(register const cmsUInt16Number Input[],
			register cmsUInt16Number Output[],
			register const cmsInterpParams* pp)
{
	// single pixel? argh!
	tetra_uint16_interp_g_p1_r1((unsigned short*)Input, // no const here
								(unsigned short*)Output,
								1,
								(const unsigned short*) (pp->Table),
								(int) (pp->nInputs),
								(int) (pp->nOutputs),
								(const unsigned int *) (pp->nSamples),
								0,0);
}

static
void tetra2(register const cmsUInt16Number Input[],
			register cmsUInt16Number Output[],
			register const cmsInterpParams* pp)
{
	tetra_uint16_interp_2_p1_r1((unsigned short*)Input, // no const here
								(unsigned short*)Output,
								1,
								(const unsigned short*) (pp->Table),
								(int) (pp->nOutputs),
								(const unsigned int *) (pp->nSamples),
								0,0);
}

static
void tetra3(register const cmsUInt16Number Input[],
			register cmsUInt16Number Output[],
			register const cmsInterpParams* pp)
{
	tetra_uint16_interp_3_p1_r1((unsigned short*)Input, // no const here
								(unsigned short*)Output,
								1,
								(const unsigned short*) (pp->Table),
								(int) (pp->nOutputs),
								(const unsigned int *) (pp->nSamples),
								0,0);
}

static
void tetra4(register const cmsUInt16Number Input[],
			register cmsUInt16Number Output[],
			register const cmsInterpParams* pp)
{
	tetra_uint16_interp_4_p1_r1((unsigned short*)Input, // no const here
								(unsigned short*)Output,
								1,
								(const unsigned short*) (pp->Table),
								(int) (pp->nOutputs),
								(const unsigned int *) (pp->nSamples),
								0,0);
}

static
void tetra5(register const cmsUInt16Number Input[],
			register cmsUInt16Number Output[],
			register const cmsInterpParams* pp)
{
	tetra_uint16_interp_5_p1_r1((unsigned short*)Input, // no const here
								(unsigned short*)Output,
								1,
								(const unsigned short*) (pp->Table),
								(int) (pp->nOutputs),
								(const unsigned int *) (pp->nSamples),
								0,0);
}

static
void tetra6(register const cmsUInt16Number Input[],
			register cmsUInt16Number Output[],
			register const cmsInterpParams* pp)
{
	tetra_uint16_interp_6_p1_r1((unsigned short*)Input, // no const here
								(unsigned short*)Output,
								1,
								(const unsigned short*) (pp->Table),
								(int) (pp->nOutputs),
								(const unsigned int *) (pp->nSamples),
								0,0);
}

static
cmsInterpFunction my_Interpolators_Factory(cmsUInt32Number nInputChannels,
										   cmsUInt32Number nOutputChannels,
										   cmsUInt32Number dwFlags)
{
#if DEBUG_FACTORY
	if(nInputChannels != 1) // limit number of messages
		fprintf(stderr, "interpolator factory request for %d -> %d, flags %x\n",
				(int)nInputChannels, (int)nOutputChannels, dwFlags);
#else
	(void)nOutputChannels;
#endif

	cmsInterpFunction Interpolation;
	cmsBool IsFloat = (dwFlags & CMS_LERP_FLAGS_FLOAT);
	memset(&Interpolation, 0, sizeof(Interpolation));
	if (!IsFloat) {
		switch(nInputChannels) {
		case 1:
			break;
		case 2:
			Interpolation.Lerp16 = tetra2;
			break;
		case 3:
			Interpolation.Lerp16 = tetra3;
			break;
		case 4:
			Interpolation.Lerp16 = tetra4;
			break;
		case 5:
			Interpolation.Lerp16 = tetra5;
			break;
		case 6:
			Interpolation.Lerp16 = tetra6;
			break;
		default:
			Interpolation.Lerp16 = tetran;
		}
	}
	return Interpolation;
}

static
cmsPluginInterpolation Plugin = {
	{
		cmsPluginMagicNumber,
		2000,
		cmsPluginInterpolationSig,
		NULL
	},
	my_Interpolators_Factory
};

bool initLCMS2IntPlugin()
{
	cmsBool regok = cmsPlugin(&Plugin);
	return regok == TRUE;
}
