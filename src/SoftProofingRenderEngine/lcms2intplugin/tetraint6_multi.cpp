// tetraint6_multi.cpp
//
// generates 4 variations of functions from tetraint6.cpp

#include "tetraint6_multi.h"

#define PRECISE_CLUT_INTEGER 0
#define ROUND_INTEGER 0
#include "tetraint6.cpp"

#undef ROUND_INTEGER
#define ROUND_INTEGER 1
#include "tetraint6.cpp"

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 0
#include "tetraint6.cpp"

#undef ROUND_INTEGER
#define ROUND_INTEGER 1
#include "tetraint6.cpp"
