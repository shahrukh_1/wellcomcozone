	size_t ordfactor4 = factor4;
	size_t ordfactor3 = factor3;
	size_t ordfactor2 = factor2;
	size_t ordfactor1 = factor1;
	size_t ordfactor0 = factor0;
	/* enforce findex3 < findex4 */ if(findex3 >= findex4){ std::swap(findex3, findex4); std::swap(ordfactor3, ordfactor4); }
	/* enforce findex2 < findex4 */ if(findex2 >= findex4){ std::swap(findex2, findex4); std::swap(ordfactor2, ordfactor4); }
	/* enforce findex1 < findex4 */ if(findex1 >= findex4){ std::swap(findex1, findex4); std::swap(ordfactor1, ordfactor4); }
	/* enforce findex0 < findex4 */ if(findex0 >= findex4){ std::swap(findex0, findex4); std::swap(ordfactor0, ordfactor4); }
	/* enforce findex2 < findex3 */ if(findex2 >= findex3){ std::swap(findex2, findex3); std::swap(ordfactor2, ordfactor3); }
	/* enforce findex1 < findex3 */ if(findex1 >= findex3){ std::swap(findex1, findex3); std::swap(ordfactor1, ordfactor3); }
	/* enforce findex0 < findex3 */ if(findex0 >= findex3){ std::swap(findex0, findex3); std::swap(ordfactor0, ordfactor3); }
	/* enforce findex1 < findex2 */ if(findex1 >= findex2){ std::swap(findex1, findex2); std::swap(ordfactor1, ordfactor2); }
	/* enforce findex0 < findex2 */ if(findex0 >= findex2){ std::swap(findex0, findex2); std::swap(ordfactor0, ordfactor2); }
	/* enforce findex0 < findex1 */ if(findex0 >= findex1){ std::swap(findex0, findex1); std::swap(ordfactor0, ordfactor1); }
	p0 = p  + ordfactor4;
	p1 = p0 + ordfactor3;
	p2 = p1 + ordfactor2;
	p3 = p2 + ordfactor1;
	p4 = p3 + ordfactor0;
	weight  = 65536   - findex4;
	weight0 = findex4 - findex3;
	weight1 = findex3 - findex2;
	weight2 = findex2 - findex1;
	weight3 = findex1 - findex0;
	weight4 = findex0;
