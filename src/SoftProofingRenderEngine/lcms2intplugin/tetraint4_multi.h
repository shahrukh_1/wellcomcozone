// tetraint4_multi.h

#ifndef TETRAINT4_MULTI_H
#define TETRAINT4_MULTI_H

#include "tetraint4.h"

#ifdef __cplusplus
extern "C" {
#endif

	// declare all non-fixed variants
	void FUNCNAME(0,0)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAME(0,1)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAME(1,0)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAME(1,1)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	// declare all fixed variants
	void FUNCNAMEFIXED(1,1,3,n)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,4,n)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 const unsigned int *nclut,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,3,17)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,4,17)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,n,17)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,3,33)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,4,33)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 long stride_in, long stride_out);

	void FUNCNAMEFIXED(1,1,n,33)
		(unsigned short *raw_in, unsigned short *raw_out,
		 unsigned long numPixels,
		 const unsigned short *pclut,
		 unsigned int nout,
		 long stride_in, long stride_out);

#ifdef __cplusplus
}
#endif

#endif // TETRAINT4_MULTI_H
