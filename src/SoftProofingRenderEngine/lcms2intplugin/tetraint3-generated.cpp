    size_t ordfactor2 = factor2;
    size_t ordfactor1 = factor1;
    size_t ordfactor0 = factor0;
    /* enforce findex1 < findex2 */ if(findex1 >= findex2){ std::swap(findex1, findex2); std::swap(ordfactor1, ordfactor2); }
    /* enforce findex0 < findex2 */ if(findex0 >= findex2){ std::swap(findex0, findex2); std::swap(ordfactor0, ordfactor2); }
    /* enforce findex0 < findex1 */ if(findex0 >= findex1){ std::swap(findex0, findex1); std::swap(ordfactor0, ordfactor1); }

    p0 = p  + ordfactor2;
    p1 = p0 + ordfactor1;
    p2 = p1 + ordfactor0;
    weight  = 65536   - findex2;
    weight0 = findex2 - findex1;
    weight1 = findex1 - findex0;
    weight2 = findex0;
