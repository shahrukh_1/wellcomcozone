// tetraintf.h

#ifndef TETRAINTF_H
#define TETRAINTF_H

#ifdef __cplusplus
extern "C" {
#endif

void tetra_float_interp(float *raw_in, float *raw_out,
						unsigned long numPixels,
						const unsigned short *pclut,
						unsigned int nin, unsigned int nout,
						const unsigned int *nclut,
						long stride_in, long stride_out);

void tetra_uint16_interp_f(unsigned short *raw_in, unsigned short *raw_out,
						   unsigned long numPixels,
						   const unsigned short *pclut,
						   unsigned int nin, unsigned int nout,
						   const unsigned int *nclut,
						   long stride_in, long stride_out);

#ifdef __cplusplus
}
#endif

#endif // TETRAINT_H
