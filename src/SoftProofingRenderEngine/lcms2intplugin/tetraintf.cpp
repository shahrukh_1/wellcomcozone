// tetraintf.cpp
//
// multidimensional tetrahedral interpolation for uint16
// assumes native endianness.

#define DEBUG_INT 0

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
enum { MaxChannels = 16 };

#include "tetraintf.h"
#include <cmath>

void tetra_uint16_interp_f(uint16 *raw_in, uint16 *raw_out,
						   uint32 numPixels,
						   const unsigned short *pclut,
						   unsigned int nin, unsigned int nout,
						   const unsigned int *nclut,
						   long stride_in, long stride_out)
{
    if(stride_in == 0) stride_in = nin;
    if(stride_out == 0) stride_out = nout;

	// create compact float arrays
	float *rawf_in = new float[numPixels * nin];
	float *rawf_out = new float[numPixels * nout];

	// copy uint16 input data to float array
	uint16 *pi = raw_in;
	float *pf = rawf_in;
    for(uint32 i = 0; i < numPixels; ++i) {
		for(unsigned int iin = 0; iin < nin; ++iin) {
			*pf++ = (float) *pi++;
		}
		pi += stride_in - nin;
	}

	// transform
	tetra_float_interp(rawf_in, rawf_out, numPixels, pclut, nin, nout, nclut, 0, 0);

	// copy, clip and round output data
	pf = rawf_out;
	pi = raw_out;
    for(uint32 i = 0; i < numPixels; ++i) {
		for(unsigned int iout = 0; iout < nout; ++iout) {
			float v = *pf++;
			if(v < 0.0f) v = 0.0f; else if(v > 65535.0f) v = 65535.0f;
			*pi++ = (uint16)(v + 0.5f);
		}
		pi += stride_out - nout;
	}

	delete[] rawf_in;
	delete[] rawf_out;
}

void tetra_float_interp(float *raw_in, float *raw_out,
						uint32 numPixels,
						const unsigned short *pclut,
						unsigned int nin, unsigned int nout,
						const unsigned int *nclut,
						long stride_in, long stride_out)
{
    unsigned int iin, iout; // input and output channel index
	signed int siin; /* input channel index; signed needed for multidim loop */
    unsigned int is; // input channel index for inserting/sorting
    float weight; // interpolation weight of current CLUT point
    float outtmp[MaxChannels]; // current CLUT entry
    float findex[MaxChannels + 1]; // floating-point index into CLUT
    uint16 iindex[MaxChannels]; // integer index into CLUT per channel
	unsigned int factor[MaxChannels], ftmp;
	uint32 i; /* pixel count */

	int seq[MaxChannels + 1]; // sequence of sorted fractions
	unsigned int ishift; /* insertion place, ... */
	const uint16 *p, *q;

#if DEBUG_INT
    printf("--------------------------------------------------------------\n");
#endif
    float *pin = raw_in, *pout = raw_out;

	ftmp = nout;
	for(siin = nin - 1; siin >= 0; siin--) {
		factor[siin] = ftmp;
		ftmp *= nclut[siin];
	}

    if(stride_in == 0) stride_in = nin;
    if(stride_out == 0) stride_out = nout;

    for(i = 0; i < numPixels; ++i) {
		for(iin = 0; iin < nin; ++iin) {
			// per channel: calculate (lower) index into CLUT
			findex[iin] = pin[iin] * (nclut[iin] - 1) / 65535.0f;
			// let iindex (= actual CLUT step) be in range 0 .. ngrid - 2
			// so that we can safely use iindex and iindex+1 for inter/extrap.
			if(findex[iin] < 0.) iindex[iin] = 0; // cannot be negative
			else if(findex[iin] >= nclut[iin] - 1) iindex[iin] = nclut[iin] - 2;
			else iindex[iin] = uint16(floor(findex[iin]));
			// subtract integer index, get what's left (as interpolation weight)
			findex[iin] -= iindex[iin];
			// when interpolating, findex will be >= 0 and < 1
			// findex == 1 only together with iindex == ngrid-2
			// when extrapolating, findex will be < 0 or > 1
		}

		// fill seq array with indices so that
		//   findex[seq[0]] >= findex[seq[1]] >= ...
		// this will not work for extrapolation (findex < 0),
		// possibly we have to use fabs()
		seq[0] = 0;
		for(iin = 1; iin < nin; ++iin) {
			// find correct place "is" to insert
			for(is = 0; is < iin; ++is)
				if(findex[seq[is]] < findex[iin]) break;
			// insert (or append if is==iin) at position is
			for(ishift = iin; ishift > is; ishift--)
				seq[ishift] = seq[ishift - 1];
			seq[is] = iin;
		}
		
		// mark end of sequence by findex[seq[nin]] = 0.
		// this is convenient for calculating weights, see below
		seq[nin] = nin;
		findex[nin] = 0.;
		
		/* lookup of base point
		 * store offset to lower CLUT grid point
		 * because we want to calculate it only once
		 * ibase is not needed here
		 */
		p = pclut;
		for(iin = 0; iin < nin; ++iin) {
			p += iindex[iin] * factor[iin];
		}

		/* base lookup */
		q = p; /* native 16-bit access */
		for(iout = 0; iout < nout; ++iout) outtmp[iout] = *q++;

		weight = 1.0f - findex[seq[0]];
		// put weighted result into result
		for(iout = 0; iout < nout; iout++) {
			pout[iout] = outtmp[iout] * weight;
		}
		// lookup of tetrahedral vertex sequence
		for(is = 0; is < nin; is++) {
			p += factor[seq[is]]; /* directly perform ibase increment in p */
			weight = findex[seq[is]] - findex[seq[is+1]];
			// this goes up to nin instead of nin-1 (we made the arrays 1 larger)
#if DEBUG_INT
			printf(" w %5.3f\n", weight);
#endif
			if(weight) { // add only if non-zero weight
				q = p; /* native 16-bit access */
				for(iout = 0; iout < nout; ++iout) outtmp[iout] = *q++;

				// add weighted result to final result
				for(iout = 0; iout < nout; iout++) {
					pout[iout] += outtmp[iout] * weight;
				}
			}
		}
		// tetrahedral interpolation done.
		pin += stride_in;
		pout += stride_out;
    }
}
