// tetraint5_multi.cpp
//
// generates 4 variations of functions from tetraint5.cpp

#include "tetraint5_multi.h"

#undef FIXED_NCLUT
#undef FIXED_NOUT

#define PRECISE_CLUT_INTEGER 0
#define ROUND_INTEGER 0
#include "tetraint5.cpp"

#undef ROUND_INTEGER
#define ROUND_INTEGER 1
#include "tetraint5.cpp"

#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER

#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 0
#include "tetraint5.cpp"

#undef ROUND_INTEGER
#define ROUND_INTEGER 1
#include "tetraint5.cpp"

// and also fixed variants
#undef PRECISE_CLUT_INTEGER
#undef ROUND_INTEGER
#define PRECISE_CLUT_INTEGER 1
#define ROUND_INTEGER 1

#undef FIXED_NCLUT

#undef FIXED_NOUT
#define FIXED_NOUT 3
#include "tetraint5.cpp"
#undef FIXED_NOUT
#define FIXED_NOUT 4
#include "tetraint5.cpp"
