// tetraint5.cpp
//
// multidimensional tetrahedral interpolation for uint16
// assumes native endianness.

#ifndef TETRAINT5_CPP
#define TETRAINT5_CPP
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
#endif // TETRAINT5_CPP

#include "tetraint5.h"

/* define PRECISE_CLUT_INTEGER to either 0 or 1. default: precise */
#ifndef PRECISE_CLUT_INTEGER
#define PRECISE_CLUT_INTEGER 1
#endif

/* define ROUND_INTEGER to either 0 or 1. default: round */
/* if ROUND128 == 128, the 16*16 bit multiplication results are rounded when reducing to 24 bits. */
#ifndef ROUND_INTEGER
#define ROUND_INTEGER 1
#endif

#undef ROUND128
#if ROUND_INTEGER
#define ROUND128 128
#else
#define ROUND128 0
#endif

/* if defined, FIXED_NCLUT, FIXED_NOUT are the desired constants (e.g. 33, 3) */
 
#undef NAME_NOUT
#ifdef FIXED_NOUT
#define NAME_NOUT FIXED_NOUT
#else
#define NAME_NOUT n
#endif

#undef NAME_NCLUT
#ifdef FIXED_NCLUT
#define NAME_NCLUT FIXED_NCLUT
#else
#define NAME_NCLUT n
#endif

#define HASFIXED
#ifndef FIXED_NOUT
#ifndef FIXED_NCLUT
#undef HASFIXED
#endif
#endif

// create function name. FUNCNAME...() depends on BASENAME and compile-time variations, see headers.

#undef THISNAME
#ifdef HASFIXED
#define THISNAME FUNCNAMEFIXED(PRECISE_CLUT_INTEGER,ROUND_INTEGER,NAME_NOUT,NAME_NCLUT)
#else
#define THISNAME FUNCNAME(PRECISE_CLUT_INTEGER,ROUND_INTEGER)
#endif

void THISNAME
	(uint16 *raw_in, uint16 *raw_out,
	 uint32 numPixels,
	 const uint16 *pclut,
#ifndef FIXED_NOUT
	 unsigned int nout,
#endif
#ifndef FIXED_NCLUT
	 const unsigned int *nclut,
#endif
	 long stride_in, long stride_out)
{
    uint32 weight, weight0, weight1, weight2, weight3, weight4; /* interpolation weight of current CLUT point */
    uint32 findex0, findex1, findex2, findex3, findex4; /* floating-point index into CLUT */
    uint16 iindex0, iindex1, iindex2, iindex3, iindex4; /* integer index into CLUT per channel */

#ifdef FIXED_NOUT
	const unsigned int nout = FIXED_NOUT;
#endif

#ifndef FIXED_NCLUT
	const size_t factor4 = nout;
	const size_t factor3 = factor4 * nclut[4];
	const size_t factor2 = factor3 * nclut[3];
	const size_t factor1 = factor2 * nclut[2];
	const size_t factor0 = factor1 * nclut[1];

	const unsigned int mclut0 = nclut[0] - 1;
	const unsigned int mclut1 = nclut[1] - 1;
	const unsigned int mclut2 = nclut[2] - 1;
	const unsigned int mclut3 = nclut[3] - 1;
	const unsigned int mclut4 = nclut[4] - 1;
#else
	const size_t factor4 = nout;
	const size_t factor3 = factor4 * FIXED_NCLUT;
	const size_t factor2 = factor3 * FIXED_NCLUT;
	const size_t factor1 = factor2 * FIXED_NCLUT;
	const size_t factor0 = factor1 * FIXED_NCLUT;

	const unsigned int mclut0 = FIXED_NCLUT - 1;
	const unsigned int mclut1 = FIXED_NCLUT - 1;
	const unsigned int mclut2 = FIXED_NCLUT - 1;
	const unsigned int mclut3 = FIXED_NCLUT - 1;
	const unsigned int mclut4 = FIXED_NCLUT - 1;
#endif

	const uint16 *p, *p0, *p1, *p2, *p3, *p4; /* sorted pointers into CLUT */

    uint16 *pin = raw_in, *pout = raw_out;

    if(stride_in == 0) stride_in = 5;
    if(stride_out == 0) stride_out = nout;

    for(uint32 i = 0; i < numPixels; ++i) {
		/* base pointer p */
		p = (uint16*)pclut;

		findex0 = *pin++ * mclut0;
#if PRECISE_CLUT_INTEGER
		findex0 += (findex0 >> 16);
#endif
		if(findex0 >= mclut0 << 16) iindex0 = mclut0 - 1;
		else iindex0 = (uint16)(findex0 >> 16);
		findex0 -= iindex0 << 16;
		p += iindex0 * factor0;

		findex1 = *pin++ * mclut1;
#if PRECISE_CLUT_INTEGER
		findex1 += (findex1 >> 16);
#endif
		if(findex1 >= mclut1 << 16) iindex1 = mclut1 - 1;
		else iindex1 = (uint16)(findex1 >> 16);
		findex1 -= iindex1 << 16;
		p += iindex1 * factor1;

		findex2 = *pin++ * mclut2;
#if PRECISE_CLUT_INTEGER
		findex2 += (findex2 >> 16);
#endif
		if(findex2 >= mclut2 << 16) iindex2 = mclut2 - 1;
		else iindex2 = (uint16)(findex2 >> 16);
		findex2 -= iindex2 << 16;
		p += iindex2 * factor2;

		findex3 = *pin++ * mclut3;
#if PRECISE_CLUT_INTEGER
		findex3 += (findex3 >> 16);
#endif
		if(findex3 >= mclut3 << 16) iindex3 = mclut3 - 1;
		else iindex3 = (uint16)(findex3 >> 16);
		findex3 -= iindex3 << 16;
		p += iindex3 * factor3;

		findex4 = *pin++ * mclut4;
#if PRECISE_CLUT_INTEGER
		findex4 += (findex4 >> 16);
#endif
		if(findex4 >= mclut4 << 16) iindex4 = mclut4 - 1;
		else iindex4 = (uint16)(findex4 >> 16);
		findex4 -= iindex4 << 16;
		p += iindex4 * factor4;

		/* create sorted sequence findex[seq0] >= findex[seq1] >= findex[seq2] >= findex[seq3] >= findex[seq4] */
		/* this is similar to sorting by insertion */
#include "tetraint5-generated.cpp"
		/* add base with weight and tetrahedral vertex sequence p0, p1, p2, p3 */
		/* into result */
		for(unsigned int iout = 0; iout < nout; iout++) {
			uint32 result = (p[iout] * weight
							 + p0[iout] * weight0
							 + p1[iout] * weight1
							 + p2[iout] * weight2
							 + p3[iout] * weight3
							 + p4[iout] * weight4 + ROUND128) >> 8;
			/* tetrahedral interpolation done. */
			/* copy the added 24-bit results to the 16-bit output array */
			pout[iout] = (uint16)((result + ROUND128) >> 8); /* rounding */
		}
		pin += stride_in - 5;
		pout += stride_out;
    }
}
