echo .
echo Build OpenAPI
set ZLIB_PROJ_PATH="%AOI_SRC%\ZLib\\ZLib.sln"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %ZLIB_PROJ_PATH% /p:Configuration=Release /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %ZLIB_PROJ_PATH% /p:Configuration=Debug /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %ZLIB_PROJ_PATH% /p:Configuration=Release /p:Platform=x64
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %ZLIB_PROJ_PATH% /p:Configuration=Debug /p:Platform=x64

echo .
echo Build OpenAPI
set AOI_PROJ_PATH="%AOI_SRC%\PDFEngine\OpenAPI\OpenAPI_VS2010.sln"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %AOI_PROJ_PATH% /p:Configuration=Release /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %AOI_PROJ_PATH% /p:Configuration=Debug /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %AOI_PROJ_PATH% /p:Configuration=Release /p:Platform=x64
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %AOI_PROJ_PATH% /p:Configuration=Debug /p:Platform=x64

echo .
echo Build JPEGLibrary
set JPEG_PROJ_PATH="%AOI_SRC%\..\3rdParty\JPEG Library\JPEGLibrary.vcxproj"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %JPEG_PROJ_PATH% /p:Configuration=Release /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %JPEG_PROJ_PATH% /p:Configuration=Debug /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %JPEG_PROJ_PATH% /p:Configuration=Release /p:Platform=x64
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %JPEG_PROJ_PATH% /p:Configuration=Debug /p:Platform=x64

echo .
echo Build LCMS
set LCMS_PROJ_PATH="..\lcms2_DLL\lcms2_DLL.vcxproj"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_PROJ_PATH% /p:Configuration=Release /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_PROJ_PATH% /p:Configuration=Debug /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_PROJ_PATH% /p:Configuration=Release /p:Platform=x64
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_PROJ_PATH% /p:Configuration=Debug /p:Platform=x64

echo .
echo Build LCMS int plugin
set LCMS_INT_PLUGIN_PROJ_PATH="..\lcms2intplugin\lib_lcms2intplugin.vcxproj"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_INT_PLUGIN_PROJ_PATH% /p:Configuration=Release /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_INT_PLUGIN_PROJ_PATH% /p:Configuration=Debug /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_INT_PLUGIN_PROJ_PATH% /p:Configuration=Release /p:Platform=x64
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %LCMS_INT_PLUGIN_PROJ_PATH% /p:Configuration=Debug /p:Platform=x64

echo off
set INPUT_DEBUG= %AOI_SRC%\..\Output\Debug
set INPUT_RELEASE= %AOI_SRC%\..\Output\Release
set OUTPUT_DEBUG_32="..\Output\Win32\Debug"
set OUTPUT_DEBUG_64="..\Output\x64\Debug"
set OUTPUT_RELEASE_32="..\Output\Win32\Release"
set OUTPUT_RELEASE_64="..\Output\x64\Release"
set OUTPUT_DEFINE_DEPENDENCIES="..\ToDeploy"
set PROFILE_MERGE="..\ProfileMerge"

echo .
echo Constructing Debug
if not exist %OUTPUT_DEBUG_32% mkdir %OUTPUT_DEBUG_32%
if not exist %OUTPUT_DEBUG_64% mkdir %OUTPUT_DEBUG_64%

echo Copy ACPL
xcopy /d /y %INPUT_DEBUG%"\ACPL 32.dll" %OUTPUT_DEBUG_32%
xcopy /d /y %INPUT_DEBUG%"\ACPL 32.lib" %OUTPUT_DEBUG_32%
xcopy /d /y %INPUT_DEBUG%"\ACPL 32.pdb" %OUTPUT_DEBUG_32%

xcopy /d /y %INPUT_DEBUG%"\ACPL 64.dll" %OUTPUT_DEBUG_64%
xcopy /d /y %INPUT_DEBUG%"\ACPL 64.pdb" %OUTPUT_DEBUG_64%
xcopy /d /y %INPUT_DEBUG%"\ACPL 64.lib" %OUTPUT_DEBUG_64%

echo Copy AOI
xcopy /d /y %INPUT_DEBUG%"\AOI_32.dll" %OUTPUT_DEBUG_32%
xcopy /d /y %INPUT_DEBUG%"\AOI_32.pdb" %OUTPUT_DEBUG_32%
xcopy /d /y %INPUT_DEBUG%"\AOI_32.lib" %OUTPUT_DEBUG_32%

xcopy /d /y %INPUT_DEBUG%"\AOI_64.dll" %OUTPUT_DEBUG_64%
xcopy /d /y %INPUT_DEBUG%"\AOI_64.pdb" %OUTPUT_DEBUG_64%
xcopy /d /y %INPUT_DEBUG%"\AOI_64.lib" %OUTPUT_DEBUG_64%

echo Copy JPEG
xcopy /d /y %INPUT_DEBUG%"\JPEGLibrary32.lib" %OUTPUT_DEBUG_32%

xcopy /d /y %INPUT_DEBUG%"\JPEGLibrary64.lib" %OUTPUT_DEBUG_64%

echo Profile Merge
xcopy /d /y %PROFILE_MERGE%"\32\MergeProfile.dll" %OUTPUT_DEBUG_32%
xcopy /d /y %PROFILE_MERGE%"\32\MergeProfile.pdb" %OUTPUT_DEBUG_32%
xcopy /d /y %PROFILE_MERGE%"\32\MergeProfile.lib" %OUTPUT_DEBUG_32%

xcopy /d /y %PROFILE_MERGE%"\64\MergeProfile.dll" %OUTPUT_DEBUG_64%
xcopy /d /y %PROFILE_MERGE%"\64\MergeProfile.lib" %OUTPUT_DEBUG_64%

echo .
echo Constructing Release

if not exist %OUTPUT_RELEASE_32% mkdir %OUTPUT_RELEASE_32%
if not exist %OUTPUT_RELEASE_64% mkdir %OUTPUT_RELEASE_64%

echo Copy ACPL
xcopy /d /y %INPUT_RELEASE%"\ACPL 32.dll" %OUTPUT_RELEASE_32%
xcopy /d /y %INPUT_RELEASE%"\ACPL 32.lib" %OUTPUT_RELEASE_32%

xcopy /d /y %INPUT_RELEASE%"\ACPL 64.dll" %OUTPUT_RELEASE_64%
xcopy /d /y %INPUT_RELEASE%"\ACPL 64.lib" %OUTPUT_RELEASE_64%

echo Copy AOI
xcopy /d /y %INPUT_RELEASE%"\AOI_32.dll" %OUTPUT_RELEASE_32%
xcopy /d /y %INPUT_RELEASE%"\AOI_32.lib" %OUTPUT_RELEASE_32%

xcopy /d /y %INPUT_RELEASE%"\AOI_64.dll" %OUTPUT_RELEASE_64%
xcopy /d /y %INPUT_RELEASE%"\AOI_64.lib" %OUTPUT_RELEASE_64%

echo Copy JPEG
xcopy /d /y %INPUT_RELEASE%"\JPEGLibrary32.lib" %OUTPUT_RELEASE_32%

xcopy /d /y %INPUT_RELEASE%"\JPEGLibrary64.lib" %OUTPUT_RELEASE_64%

echo Profile Merge
xcopy /d /y %PROFILE_MERGE%"\32\MergeProfile.dll" %OUTPUT_RELEASE_32%
xcopy /d /y %PROFILE_MERGE%"\32\MergeProfile.pdb" %OUTPUT_RELEASE_32%
xcopy /d /y %PROFILE_MERGE%"\32\MergeProfile.lib" %OUTPUT_RELEASE_32%

echo Profile Merge
xcopy /d /y %PROFILE_MERGE%"\64\MergeProfile.dll" %OUTPUT_RELEASE_64%
xcopy /d /y %PROFILE_MERGE%"\64\MergeProfile.lib" %OUTPUT_RELEASE_64%

echo .
echo Build SoftProofingRenderEngine
set SOFTPROOFINGENGINE_PROJ_PATH="..\SoftProofingRenderEngine\SoftProofingRenderEngine.vcxproj"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %SOFTPROOFINGENGINE_PROJ_PATH% /p:Configuration=Release /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %SOFTPROOFINGENGINE_PROJ_PATH% /p:Configuration=Debug /p:Platform=Win32
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %SOFTPROOFINGENGINE_PROJ_PATH% /p:Configuration=Release /p:Platform=x64
C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe %SOFTPROOFINGENGINE_PROJ_PATH% /p:Configuration=Debug /p:Platform=x64

echo .
echo Gather Define/Tile server dependencies
if not exist %OUTPUT_DEFINE_DEPENDENCIES% mkdir %OUTPUT_DEFINE_DEPENDENCIES%

echo Copy ACPL
xcopy /d /y %INPUT_RELEASE%"\ACPL 32.dll" %OUTPUT_DEFINE_DEPENDENCIES%
xcopy /d /y %INPUT_RELEASE%"\ACPL 64.dll" %OUTPUT_DEFINE_DEPENDENCIES%
echo Copy AOI
xcopy /d /y %INPUT_RELEASE%"\AOI_32.dll" %OUTPUT_DEFINE_DEPENDENCIES%
xcopy /d /y %INPUT_RELEASE%"\AOI_64.dll" %OUTPUT_DEFINE_DEPENDENCIES%
echo Copy LCMS
xcopy /d /y %OUTPUT_RELEASE_32%"\lcms2_32.dll" %OUTPUT_DEFINE_DEPENDENCIES%
xcopy /d /y %OUTPUT_RELEASE_64%"\lcms2_64.dll" %OUTPUT_DEFINE_DEPENDENCIES%
echo Copy Render Engine
xcopy /d /y %OUTPUT_RELEASE_32%"\SoftProofingRenderEngine_32.dll" %OUTPUT_DEFINE_DEPENDENCIES%
xcopy /d /y %OUTPUT_RELEASE_64%"\SoftProofingRenderEngine_64.dll" %OUTPUT_DEFINE_DEPENDENCIES%
echo Profile Merge
xcopy /d /y %PROFILE_MERGE%"\64\MergeProfile.dll" %OUTPUT_DEFINE_DEPENDENCIES%

echo Done!
PAUSE