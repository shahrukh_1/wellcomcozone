#!/bin/sh

cd "`dirname "$0"`"

productsign --sign "Developer ID Installer: GMG GmbH & Co. KG (225AHUX33D)" build/SoftProofingAgent_unsigned.pkg build/SoftProofingAgent.pkg

exit 0