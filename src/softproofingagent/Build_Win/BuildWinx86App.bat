windeployqt ../Output/Win32/Release/GMGCoZoneSoftProofingAgent.exe

copy C:\Windows\SysWOW64\msvcp140.dll "../Output/Win32/Release/"
copy C:\Windows\SysWOW64\vcruntime140.dll "../Output/Win32/Release/"

copy "..\Build_Win\dlls\libeay32.dll" "../Output/Win32/Release/" 
copy "..\Build_Win\dlls\ssleay32.dll" "../Output/Win32/Release/"

copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Core.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Cored.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Gui.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Guid.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Network.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Networkd.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Widgetsd.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Widgets.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Xml.dll  "../Output/Win32/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Xmld.dll  "../Output/Win32/Release/"

copy "..\Output\Zlib\Win32\Release\Zlib64.lib" "../Output/Win32/Release/"