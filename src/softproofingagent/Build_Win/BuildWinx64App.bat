windeployqt ../Output/x64/Release/GMGCoZoneSoftProofingAgent.exe

copy C:\Windows\System32\msvcp140.dll "../Output/x64/Release/"
copy C:\Windows\System32\vcruntime140.dll "../Output/x64/Release/"

copy "..\Build_Win\dlls\libeay32.dll" "../Output/x64/Release/" 
copy "..\Build_Win\dlls\ssleay32.dll" "../Output/x64/Release/" 

copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Core.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Cored.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Gui.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Guid.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Network.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Networkd.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Widgetsd.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Widgets.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Xml.dll  "../Output/x64/Release/"
copy C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin\Qt5Xmld.dll  "../Output/x64/Release/"

copy "..\Output\Zlib\x64\Release\Zlib64.lib" "../Output/x64/Release/"