#!/bin/sh

echo "Removing build folders"
xcodebuild clean -project GMGCoZoneSoftProofingAgent.xcodeproj
echo "Building Release"
xcodebuild -project GMGCoZoneSoftProofingAgent.xcodeproj
echo "MacDeploy"
macdeployqt ../Output/Release/SoftProofingAgent.app
# build and zip installers
cd ../Output/Release
zip -rqm SoftProofingAgent.zip SoftProofingAgent.app