<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>ParserRunnable</name>
    <message>
        <location filename="../../../3rdParty/QJson/parserrunnable.cpp" line="41"/>
        <source>An error occured while parsing json: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="23"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="45"/>
        <source>Do you want to continue without saving ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="158"/>
        <source>BasICColor Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="264"/>
        <source>Invalid calibration data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="270"/>
        <source>Basic Color XML File Not found in output folder!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="276"/>
        <source>Basic Color Log File Not found in output folder!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="282"/>
        <source>Machine ID cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="288"/>
        <source>Username cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="294"/>
        <source>Password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/Preferences.cpp" line="300"/>
        <source>Account name cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesUI</name>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="14"/>
        <source>SoftProofingAgentMainUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="22"/>
        <source>Data Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="58"/>
        <source>Default Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="71"/>
        <source>Choose Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="78"/>
        <source>Account Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="88"/>
        <source>Profile Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="98"/>
        <source>Calibration Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="115"/>
        <source>Machine ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="131"/>
        <source>CoZone Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/Preferences.ui" line="144"/>
        <source>CoZone Username</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="24"/>
        <source>GMGCoZoneSoftProofingAgent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="25"/>
        <source>I couldn&apos;t detect any system tray on this system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/CalibrationTools/BasicColorDisplayCalibrationTool.cpp" line="18"/>
        <source>BasICColor Display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SoftProofingSystemTray</name>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="21"/>
        <source>&amp;Start Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="24"/>
        <source>&amp;Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="27"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="57"/>
        <source>CoZone Collaborate SoftProofing Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="77"/>
        <source>Failed to open the selected browser!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="161"/>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="174"/>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="274"/>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="313"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="162"/>
        <source>Upload data request failed: Selected Display not found in display List!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="175"/>
        <source>Upload Data request failed: BasicColorXML not found in folderLocation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="202"/>
        <source>Server not responding. Do you want to try to resend the request?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="289"/>
        <source>Authentication failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="294"/>
        <source>Authentication failed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="333"/>
        <source>Uploading calibration data failed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="300"/>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="344"/>
        <source>Succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="301"/>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="372"/>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="379"/>
        <source>In progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Sources/SoftProofingSystemTray.cpp" line="327"/>
        <source>Uploading calibration data failed </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadDataProgress</name>
    <message>
        <location filename="../../Forms/UploadDataProgress.cpp" line="17"/>
        <source>Upload Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UploadDataProgress.cpp" line="29"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UploadDataProgress.cpp" line="41"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpoadDataProgress</name>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="25"/>
        <source> Verify Account Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="35"/>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="52"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="42"/>
        <source>Upload Calibration Data Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="89"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="96"/>
        <source>Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Forms/UI/UploadDataProgress.ui" line="103"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
