#ifndef GMGCOZONESOFTPROOFINGAGENT_H
#define GMGCOZONESOFTPROOFINGAGENT_H

#include <QtWidgets/QDialog>
#include "ui_Preferences.h"
#include "../Sources/Globals.h"
#include "../Sources/SoftProofingWebClient.h"
#include "../Sources/BrowserInfo.h"
#include "../Sources/MonitorInfo.h"
#include "../Sources/CalibrationTools/BasicColorDisplayCalibrationTool.h"


class QSettings;

class Preferences : public QWidget
{
	Q_OBJECT
public:
										Preferences(QList<CalibrationToolBase*> calibrationTools, QWidget *parent = 0, Qt::WindowFlags flags = 0);

Q_SIGNALS:
	void								Close();
	void								OKButtonPressed();
	void								StartSessionButtonPressed();

protected:
	void								closeEvent( QCloseEvent *event );

private slots:
	void								DataLocationPressed();
	void								DisplayChanged( int index );
	void								CalibrationToolChanged( int index );
	void								OnStartSessionButtonPressed();
	void								OnOKButtonPressed();

private:
	Ui::PreferencesUI					ui;
	QList<MonitorInfo>					mMonitors;
	QList<BrowserInfo>					mBrowsers;
	QList<CalibrationToolBase*>			mCalibrationTools;

	void								FillControls();
	void								SaveSettings();
	void								LoadSettings();
	void								ApplyPreferences(bool isStartSession);
	bool								CheckIfUploadShouldBeDone();
	bool								IsUploadPreferencesChanged();
	bool								IsInputValid( QString& errorMessage );
	virtual void						keyPressEvent ( QKeyEvent * event );
	CalibrationToolBase*				GetCalibrationToolByName( const char* calibrationToolName );
	CalibrationToolBase*				GetSelectedCalibrationTool();
};

#endif // GMGCOZONESOFTPROOFINGAGENT_H
