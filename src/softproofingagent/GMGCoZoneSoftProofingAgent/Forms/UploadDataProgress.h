#ifndef GMGCOZONESOFTPROOFINGAGENT_UPLOADDATAPROGRESS_H
#define GMGCOZONESOFTPROOFINGAGENT_UPLOADDATAPROGRESS_H

#include <QDialog>
#include <QLabel>
#include <QtGui/QKeyEvent>
#include "ui_UploadDataProgress.h"

class UploadDataProgress: public QDialog
{
	Q_OBJECT
public:
	typedef enum CurrentAction
	{
		eNone,
		eVerifyAccount,
		eSendCalibrationData
	}	CurrentAction;
							UploadDataProgress( QWidget* parent = 0 );
	void					SetVerifyAccountStatus( const QString& newStatus );
	void					SetUploadDataStatus( const QString& newStatus );
	void					SetErrorMessage( const QString& errorMessage );
	void					ShowErrorMessage();
	void					HideErrorMessage();
	void					ResetStatuses();
	void					SetCurrentAction( CurrentAction action );
	CurrentAction			GetCurrentAction() const;

protected:
	void					closeEvent( QCloseEvent * closeEvent );
	void					keyPressEvent( QKeyEvent * event );
private:
	Ui::UpoadDataProgress	ui;
	CurrentAction			mCurrentAction;

Q_SIGNALS:
	void					ReleaseDialog();

private slots:
	void					OnOkPressed();
};

inline void UploadDataProgress::SetVerifyAccountStatus( const QString& newStatus )
{
	ui.verifyAccountStatusLabel->setText( newStatus );
}

inline void UploadDataProgress::SetUploadDataStatus( const QString& newStatus )
{
	ui.uploadCalibrationDataStatusLabel->setText( newStatus );
}

inline void UploadDataProgress::SetErrorMessage( const QString& errorMessage )
{
	ShowErrorMessage();
	ui.ErrorMessageBox->setText( errorMessage );
}

inline void UploadDataProgress::SetCurrentAction( CurrentAction action )
{
	mCurrentAction = action;
}

inline UploadDataProgress::CurrentAction UploadDataProgress::GetCurrentAction() const
{
	return mCurrentAction;
}

#endif