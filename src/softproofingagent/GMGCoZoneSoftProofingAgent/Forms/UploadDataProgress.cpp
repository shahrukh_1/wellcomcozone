#include "UploadDataProgress.h"
#include <QtGui/QIcon>


UploadDataProgress::UploadDataProgress( QWidget* parent ) :
QDialog( parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::MSWindowsFixedSizeDialogHint | Qt::FramelessWindowHint),
mCurrentAction( UploadDataProgress::eNone )
{
	ui.setupUi( this );

	connect( ui.okButton, SIGNAL( clicked() ), SLOT( OnOkPressed() ) );

	setAttribute( Qt::WA_DeleteOnClose );
	setWindowIcon( QIcon(":/GMGCoZoneSoftProofingAgent/favicon.png") );
	setWindowTitle( tr("Sync Progress") );
	setModal( true );

	HideErrorMessage();
	ResetStatuses();
}

void UploadDataProgress::HideErrorMessage()
{
	ui.ErrorMessageBox->setVisible( false );
	adjustSize();
}

void UploadDataProgress::ShowErrorMessage()
{
	ui.ErrorMessageBox->setVisible( true );
	adjustSize();
}

void UploadDataProgress::ResetStatuses()
{
	ui.uploadCalibrationDataStatusLabel->setText( "-" );
	ui.verifyAccountStatusLabel->setText( "-" );
	ui.ErrorMessageBox->setText("");
}

void UploadDataProgress::OnOkPressed()
{
	close();
}


void UploadDataProgress::keyPressEvent( QKeyEvent * event )
{
	if( event->key() == Qt::Key_Escape )
	{
		close();
	}
	QDialog::keyPressEvent( event );
}


void UploadDataProgress::closeEvent( QCloseEvent * closeEvent )
{
	emit ReleaseDialog();
	QDialog::closeEvent( closeEvent );
}