#include "Preferences.h"
#include <QtWidgets>
#include <QDesktopWidget>
#include "../Sources/AppConfig.h"
#include "../Sources/Utils.h"
#include "../Sources/Settings.h"

Preferences::Preferences( QList<CalibrationToolBase*> calibrationTools, QWidget *parent, Qt::WindowFlags flags )
	: QWidget(parent, flags),
	mCalibrationTools( calibrationTools )
{
	ui.setupUi(this);

	FillControls();

	LoadSettings();

	Utils::Log( LEVEL_INFO, "Application started ");
		
	setWindowTitle( tr("CoZone Agent Preferences") );
	setWindowIcon( QIcon(":/GMGCoZoneSoftProofingAgent/favicon.png") );
	setWindowState( (windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);

	activateWindow();
	raise();
}

void Preferences::OnStartSessionButtonPressed()
{
	ApplyPreferences( true );
}

void Preferences::OnOKButtonPressed()
{
	ApplyPreferences(false);
}

void Preferences::ApplyPreferences( bool isStartSession )
{
	if ( isVisible() )
	{
		bool showPopUp = false;
		QString errorMessage;
		if( !IsInputValid( errorMessage ) )
		{
			showPopUp = true;

		}
		else
		{
			SaveSettings();
			//if submit button pressed send calibration data, owherwise only just save preferences
			if(isStartSession)
			{
				emit StartSessionButtonPressed();
			}
			else
			{
				emit OKButtonPressed();
			}
		}

		if(showPopUp)
		{
			QMessageBox msgBox( this );
			msgBox.setText( errorMessage );
			msgBox.setWindowModality( Qt::WindowModal );
			msgBox.setIcon( QMessageBox::Warning );
			msgBox.setStandardButtons( QMessageBox::Ok);
			msgBox.setDefaultButton( QMessageBox::Ok );
			msgBox.exec();
		}
	}
}

void Preferences::keyPressEvent( QKeyEvent* e )
{
	QWidget::keyPressEvent(e);

	if( e->type() == QKeyEvent::KeyPress )
	{
		if( e->key() == Qt::Key_Escape )
		{
			close();
		}
	}
}

void Preferences::closeEvent( QCloseEvent* event )
{
	emit Close();
}

bool Preferences::IsUploadPreferencesChanged()
{
	if(	
		Settings::getInstance().GetDisplayName() != ui.displayComboBox->currentText()
		||Settings::getInstance().GetAccountName() != ui.accountNameLineEdit->text()
		|| Settings::getInstance().GetCoZoneUserName() != ui.cozoneUserLineEdit->text()
		|| Settings::getInstance().GetCoZoneUserPassword() != ui.cozonePasswordLineEdit->text()
		|| Settings::getInstance().GetDataLocation() != ui.dataLocationLineEdit->text()
		|| Settings::getInstance().GetBrowserName() != ui.browserComboBox->currentText()
		|| Settings::getInstance().GetCalibrationName() != ui.calibrationSoftComboBox->currentText()
		|| Settings::getInstance().GetDomainURL() != ui.environmentDomainLineEdit->text()
	  )	
	{
		return true;
	}

	return false;
}

void Preferences::FillControls()
{
	foreach( CalibrationToolBase* calibrationTool, mCalibrationTools )
	{
		ui.calibrationSoftComboBox->addItem( calibrationTool->GetToolName() );
	}

	ui.cozonePasswordLineEdit->setEchoMode( QLineEdit::Password );

	ui.environmentDomainLineEdit->setText( AppConfig::getInstance().GetEnvironmentDomain() );
	
	mMonitors = Utils::GetAvailableDisplays();
	foreach( MonitorInfo display, mMonitors )
	{
		ui.displayComboBox->addItem( display.Name, QVariant( display.ProfilePath ) ); 
	}
	
	if( mMonitors.count() > 0 )
	{
		if( mMonitors.count() == 1 )
		{
			ui.displayComboBox->setDisabled( true );
		}
		ui.profileLocationLineEdit->setText( mMonitors.first().ProfilePath );
		ui.profileLocationLineEdit->setToolTip( mMonitors.first().ProfilePath );
	}

    mBrowsers = Utils::GetAvailableBrowsers();
	foreach( BrowserInfo browser, mBrowsers )
	{
		ui.browserComboBox->addItem( browser.Name, QVariant::fromValue(browser.Path) );
	}

	if( !GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->GetOutputFolderLocation().isEmpty() )
	{
		ui.dataLocationLineEdit->setText( GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->GetOutputFolderLocation() );
		ui.dataLocationLineEdit->setToolTip( GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->GetOutputFolderLocation() );
	}

	connect( ui.dataLocationButton, SIGNAL( clicked() ), this, SLOT( DataLocationPressed() ) );
	connect( ui.displayComboBox, SIGNAL( currentIndexChanged( int ) ), this, SLOT( DisplayChanged( int ) ) );
	connect( ui.calibrationSoftComboBox, SIGNAL( currentIndexChanged( int ) ), this, SLOT( CalibrationToolChanged( int ) ) );
	connect( ui.btnCancel, SIGNAL( clicked() ), this, SLOT( close() ) );
	connect( ui.btnSubmit, SIGNAL( clicked() ), this, SLOT(OnStartSessionButtonPressed() ) );
	connect( ui.btnSave, SIGNAL( clicked() ), this, SLOT(OnOKButtonPressed() ) );
}

void Preferences::DisplayChanged(int index)
{
	ui.profileLocationLineEdit->setText( ui.displayComboBox->itemData( index ).toString() );
	ui.profileLocationLineEdit->setToolTip( ui.displayComboBox->itemData( index ).toString() );
}

void Preferences::CalibrationToolChanged( int index )
{
	if( ui.calibrationSoftComboBox->currentText() == tr( BasicColorDisplayCalibrationTool::TOOLNAME ) )
	{
		ui.dataLocationButton->setEnabled( true );
		ui.dataLocationLineEdit->setEnabled( true );
		ui.dataLocationLineEdit->setReadOnly( false );
		if( !GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->GetOutputFolderLocation().isEmpty() )
		{
			ui.dataLocationLineEdit->setText( GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->GetOutputFolderLocation() );
			ui.dataLocationLineEdit->setToolTip( GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->GetOutputFolderLocation() );
		}
	}
	else
	{
		ui.dataLocationButton->setEnabled( false );
		ui.dataLocationLineEdit->setEnabled( false );
		ui.dataLocationLineEdit->setReadOnly( true );
		ui.dataLocationLineEdit->clear();
		ui.dataLocationLineEdit->setToolTip( "" );
	}
}

void Preferences::DataLocationPressed()
{
	QFileDialog dialog( this );
	dialog.setFileMode( QFileDialog::Directory );
	dialog.setOption( QFileDialog::ShowDirsOnly );
	int result = dialog.exec();
	QString directory;

	if( result )
	{
		if( dialog.selectedFiles().count() > 0 )
		{
			QString folderName = dialog.selectedFiles()[0];
			ui.dataLocationLineEdit->setText( folderName );
			ui.dataLocationLineEdit->setToolTip( folderName );
			if( QFile().exists( folderName + "/" + kBasICColorXMLFile ) )
			{
				GetCalibrationToolByName( BasicColorDisplayCalibrationTool::TOOLNAME )->SetFolderLocation( folderName );
			}
		}
	}
}

void Preferences::SaveSettings()
{
	//Save user preferences using qt platform specific methods Ex : registry for WIN
	Settings::getInstance().SetBrowserName( ui.browserComboBox->currentText() );
	Settings::getInstance().SetBrowserPath( ui.browserComboBox->currentIndex() >= 0 ? ui.browserComboBox->itemData( ui.browserComboBox->currentIndex() ).toString() : "" );
	Settings::getInstance().SetCalibrationName( ui.calibrationSoftComboBox->currentText() );
	Settings::getInstance().SetDataLocation( ui.dataLocationLineEdit->text() );
	Settings::getInstance().SetAccountName( ui.accountNameLineEdit->text() );
	Settings::getInstance().SetDisplayName( ui.displayComboBox->currentText() );
	Settings::getInstance().SetCoZoneUserName( ui.cozoneUserLineEdit->text() );
	Settings::getInstance().SetCoZoneUserPassword( ui.cozonePasswordLineEdit->text() );
	Settings::getInstance().SetProfileLocation( ui.profileLocationLineEdit->text() );
	Settings::getInstance().SetDisplayIndex( ui.displayComboBox->currentIndex() );
	Settings::getInstance().SetDomainURL( ui.environmentDomainLineEdit->text() );

	foreach( MonitorInfo display, mMonitors )
	{
		if( ui.displayComboBox->currentText() == display.Name )
		{
			Settings::getInstance().SetDisplayHeight( display.Height );
			Settings::getInstance().SetDisplayWidth( display.Width );
			break;
		}
	}

	Settings::getInstance().SaveSettings();
}

void Preferences::LoadSettings()
{
	if( Settings::getInstance().GetBrowserName() != "" )
	{
		int index = ui.browserComboBox->findText( Settings::getInstance().GetBrowserName() );
		if( index >= 0 )
		{
			ui.browserComboBox->setCurrentIndex( index );
		}
	}

	if( Settings::getInstance().GetCalibrationName() != "" )
	{
		int index = ui.calibrationSoftComboBox->findText(Settings::getInstance().GetCalibrationName() );
		if( index >= 0 )
		{
			ui.calibrationSoftComboBox->setCurrentIndex( index );
		}
	}

	if( Settings::getInstance().GetDisplayName() != "" )
	{
		int index = ui.displayComboBox->findText( Settings::getInstance().GetDisplayName() );
		if( index >= 0 )
		{
			ui.displayComboBox->setCurrentIndex( index );
		}
	}

	if( !Settings::getInstance().GetDataLocation().isEmpty() )
	{
		ui.dataLocationLineEdit->setText(Settings::getInstance().GetDataLocation() );
	}

	if( !Settings::getInstance().GetDomainURL().isEmpty() )
	{
		ui.environmentDomainLineEdit->setText( Settings::getInstance().GetDomainURL() );
	}

	ui.accountNameLineEdit->setText( Settings::getInstance().GetAccountName() );
	ui.cozoneUserLineEdit->setText( Settings::getInstance().GetCoZoneUserName() );
	ui.cozonePasswordLineEdit->setText( Settings::getInstance().GetCoZoneUserPassword() );
	ui.machineIdLineEdit->setText( Settings::getInstance().GetMachineID() );
}

bool Preferences::IsInputValid( QString& errorMessage )
{
	if( ui.calibrationSoftComboBox->currentText() == tr( BasicColorDisplayCalibrationTool::TOOLNAME ) )
	{
		QFileInfo fileInfo( ui.dataLocationLineEdit->text() );

		if( !fileInfo.exists() )
		{
			errorMessage = tr("Invalid calibration data folder");
			return false;
		}

		if( !QFile().exists( ui.dataLocationLineEdit->text() + "/" + kBasICColorXMLFile ) )
		{
			errorMessage = tr("Basic Color XML File Not found in output folder!");
			return false;
		}

		if( !QFile().exists( ui.dataLocationLineEdit->text() + "/" + kBasICColorLogFile ) )
		{
			errorMessage = tr("Basic Color Log File Not found in output folder!");
			return false;
		}
	}

	if( ui.cozoneUserLineEdit->text().isEmpty() )
	{
		errorMessage = tr("Username cannot be empty");
		return false;
	}

	if( ui.cozonePasswordLineEdit->text().isEmpty() )
	{
		errorMessage = tr("Password cannot be empty");
		return false;
	}

	if( ui.accountNameLineEdit->text().isEmpty() )
	{
		errorMessage = tr("Account name cannot be empty");
		return false;
	}

	return true;
}

CalibrationToolBase* Preferences::GetCalibrationToolByName( const char* calibrationToolName )
{
	foreach( CalibrationToolBase* calibrationTool, mCalibrationTools )
	{
		if(calibrationTool->GetToolName() == tr( calibrationToolName ))
			return calibrationTool;
	}
}

CalibrationToolBase* Preferences::GetSelectedCalibrationTool()
{
	foreach(CalibrationToolBase* calibrationTool, mCalibrationTools)
	{
		if (calibrationTool->GetToolName() == Settings::getInstance().GetCalibrationName())
			return calibrationTool;
	}
	return NULL;
}