#ifndef GMGCOZONESOFTPROOFINGAGENT_SETTINGS_H
#define GMGCOZONESOFTPROOFINGAGENT_SETTINGS_H
#include <QString>
#include <QSettings>
#include "Globals.h"

class Settings{
	QString		mAccountName;
	QString		mDataLocation;
	QSettings*  mSettings;
	QString		mAppVersion;
	QString		mBrowserName;
	QString		mCalibrationName;
	QString		mDisplayName;
	QString		mCoZoneUserKey;
	QString		mCoZoneUserName;
	QString		mCoZoneUserPassword;
	QString		mBrowserPath;
	QString		mProfileLocation;
	QString		mMachineID;
	QString		mDomainURL;
	qint64		mLastSuccessfulUpload;
	int			mDisplayIndex;
	int			mDisplayWidth;
	int			mDisplayHeight;

public:
	static Settings& getInstance()
	{
		static Settings instance(APP_ORGANIZATION, APP_NAME);
		return instance;
	}

	Settings(const QString&, const QString&);
	~Settings();

	void		SetAccountName( const QString& accountName );
	QString		GetAccountName() const;
	void		SetDataLocation( const QString& dataLocation );
	QString		GetDataLocation() const;
	void		SetBrowserName( const QString& );
	QString		GetBrowserName() const;
	void		SetCalibrationName( const QString& );
	QString		GetCalibrationName() const;
	void		SetDisplayName( const QString& );
	QString		GetDisplayName() const;
	void		SetCoZoneUserKey( const QString&  userKey );
	QString		GetCoZoneUserKey() const;
	void		SetCoZoneUserName( const QString& userName );
	QString		GetCoZoneUserName() const;
	void		SetCoZoneUserPassword( const QString& userPassword );
	QString		GetCoZoneUserPassword() const;
	void		SetBrowserPath( const QString& browserPath );
	QString		GetBrowserPath() const;
	void		SetProfileLocation( const QString& profileLocation );
	QString		GetProfileLocation() const;
	void		SetLastSuccessfulUpload( qint64 date );
	qint64		GetLastSuccessfulUpload() const;
	void		SetDisplayIndex( int index );
	int			GetDisplayIndex() const;
	QString		GetMachineID() const;
	void		SetMachineID( const QString& machineID );
	QString		GetDomainURL() const;
	void		SetDomainURL( const QString& domainUrl );
	void		SetDisplayHeight( int height );
	int			GetDisplayHeight() const;
	void		SetDisplayWidth( int width );
	int			GetDisplayWidth() const;
	void		SetFirstUpload(bool firstUpload);
	bool		GetFirstUpload() const;
	
	void		LoadSettings(bool& isFirstRun);
	void		SaveSettings();

};

inline void Settings::SetAccountName( const QString& accountName )
{
	mAccountName = accountName;
}

inline QString Settings::GetAccountName() const
{
	return mAccountName;
}

inline void Settings::SetDataLocation( const QString& dataLocation )
{
	mDataLocation = dataLocation;
}

inline QString Settings::GetDataLocation() const
{
	return mDataLocation;
}

inline void Settings::SetBrowserName( const QString& name )
{
	mBrowserName = name;
}

inline QString Settings::GetBrowserName() const
{
	return mBrowserName;
}

inline void Settings::SetCalibrationName( const QString& calibrationName )
{
	mCalibrationName = calibrationName;
}

inline QString Settings::GetCalibrationName() const
{
	return mCalibrationName;
}

inline void Settings::SetDisplayName( const QString& displayName )
{
	mDisplayName = displayName;
}

inline QString Settings::GetDisplayName() const
{
	return mDisplayName;
}

inline void Settings::SetCoZoneUserName( const QString& userName )
{
	mCoZoneUserName = userName;
}

inline QString Settings::GetCoZoneUserName() const
{
	return mCoZoneUserName;
}

inline void Settings::SetCoZoneUserPassword( const QString& userPassword )
{
	mCoZoneUserPassword = userPassword;
}

inline QString Settings::GetCoZoneUserPassword() const
{
	return mCoZoneUserPassword;
}

inline void Settings::SetBrowserPath( const QString& browserPath )
{
	mBrowserPath = browserPath;
}

inline QString Settings::GetBrowserPath() const
{
	return mBrowserPath;
}

inline void Settings::SetProfileLocation( const QString& profileLocation )
{
	mProfileLocation = profileLocation;
}

inline QString Settings::GetProfileLocation() const
{
	return mProfileLocation;
}

inline void Settings::SetDomainURL( const QString& domainUrl )
{
	mDomainURL = domainUrl;
}

inline QString Settings::GetDomainURL() const
{
	return mDomainURL;
}

inline void Settings::SetDisplayIndex( int index )
{
	mDisplayIndex = index;
}

inline int Settings::GetDisplayIndex() const
{
	return mDisplayIndex;
}

inline QString Settings::GetMachineID() const
{
	return mMachineID;
}

inline void Settings::SetDisplayWidth( int width )
{
	mDisplayWidth = width;
}

inline int Settings::GetDisplayWidth() const
{
	return mDisplayWidth;
}

inline void Settings::SetDisplayHeight( int height )
{
	mDisplayHeight = height;
}

inline int Settings::GetDisplayHeight() const
{
	return mDisplayHeight;
}

#endif