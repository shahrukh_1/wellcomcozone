#include "AppConfig.h"
#include "../Forms/Preferences.h"
#include <QtXml/QDomDocument>
#include <QFile>
#include "Utils.h"


void AppConfig::LoadSettings()
{
    QString xmlFilePath;
#if COZONE_WIN
	xmlFilePath = QApplication::applicationDirPath() + "/Resources/Settings.xml";
#else
    xmlFilePath = QApplication::applicationDirPath() + "/../Resources/Settings.xml";
#endif
    QFile configFile (xmlFilePath);

	if( !configFile.open( QFile::ReadOnly ) )
	{
		Utils::Log( LEVEL_FATAL, "Config file Settings.xml not found!");
		return;
	}

	QDomDocument xmlDoc;
	QDomElement root;

	if( !xmlDoc.setContent( configFile.readAll() ) )
	{
		Utils::Log( LEVEL_FATAL, "Count not load content of Config File Settings.xml");
		return;
	}

	root = xmlDoc.documentElement();

	QDomNodeList securityTokenNode = root.elementsByTagName( "RestApiSecurityToken" );
	if(securityTokenNode.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "Security Token Node not found in Config File Settings.xml");
	}
	else
	{
		if( securityTokenNode.at(0).isElement() )
		{
			mSecurityToken = securityTokenNode.at(0).toElement().text();
		}
	}
		
	QDomNodeList serviceProtocol = root.elementsByTagName( "ServiceProtocol" );
	if(serviceProtocol.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "ServiceProtocol Node not found in Config File Settings.xml");
	}
	else
	{
		if( serviceProtocol.at(0).isElement())
		{
			mServiceProtocol = serviceProtocol.at(0).toElement().text();
		}
	}

	QDomNodeList verifyAccountUrl = root.elementsByTagName( "VerifyAccountURL" );
	if(verifyAccountUrl.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "VerifyAccountURL Node not found in Config File Settings.xml");
	}
	else
	{
		if(verifyAccountUrl.at(0).isElement())
		{
			mVerifyAccountURL = verifyAccountUrl.at(0).toElement().text();
		}
	}

	QDomNodeList logLevel = root.elementsByTagName( "LogLevel" );
	if(logLevel.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "LogLevel Node not found in Config File Settings.xml");
	}
	else
	{
		if(logLevel.at(0).isElement())
		{
			mLogLevel = logLevel.at(0).toElement().text().toInt();
		}
	}

	QDomNodeList uploadDataUrl = root.elementsByTagName( "UploadDataURL" );
	if(uploadDataUrl.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "UploadDataUrl Node not found in Config File Settings.xml");
	}
	else
	{
		if(uploadDataUrl.at(0).isElement())
		{
			mUploadDataURL = uploadDataUrl.at(0).toElement().text();
		}
	}

	QDomNodeList initSessionURL = root.elementsByTagName( "InitSessionURL" );
	if(initSessionURL.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "InitSessionURL Node not found in Config File Settings.xml");
	}
	else
	{
		if(initSessionURL.at(0).isElement())
		{
			mInitSessionURL = initSessionURL.at(0).toElement().text();
		}
	}

	QDomNodeList environmentDomain = root.elementsByTagName( "EnvironmentDomain" );
	if(environmentDomain.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "AccountDomains Node not found in Config File Settings.xml");
	}
	else
	{
		if(environmentDomain.at(0).isElement())
		{
			mEnvironmentDomain = environmentDomain.at(0).toElement().text();
		}
	}

	QDomNodeList language = root.elementsByTagName( "Language" );
	if(language.count() == 0)
	{
		Utils::Log( LEVEL_FATAL, "Language Node not found in Config File Settings.xml");
	}
	else
	{
		if(language.at(0).isElement())
		{
			mLanguage = language.at(0).toElement().text();
		}
	}

	configFile.close();
}