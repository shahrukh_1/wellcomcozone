﻿#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>
#include "Zipper.h"

#if COZONE_MAC
#include <utime.h>
#include <unistd.h>
#include <sys/stat.h>
#endif


const unsigned int BUFFERSIZE = 2048;

#if COZONE_MAC
static uLong GetFileAttributesW(const QString filePath )
{
	struct stat fs1;
	if( !stat( filePath.toUtf8(), &fs1 ) )
		return fs1.st_mode;
	return 0xFFFFFFFF;
}
#endif

static bool GetLastModified( const QString& path, struct tm *&sysTime)
{
#if COZONE_WIN	
	DWORD dwAttr = ::GetFileAttributesW( path.utf16() );

	if (dwAttr == 0xFFFFFFFF)
		return false;

	WIN32_FIND_DATAW findFileData;
	HANDLE hFind = ::FindFirstFileW( path.utf16(), &findFileData);

	if (hFind == INVALID_HANDLE_VALUE)
		return FALSE;

	FindClose(hFind);

	FILETIME ft = findFileData.ftLastWriteTime;
	SYSTEMTIME st;

	FileTimeToLocalFileTime(&findFileData.ftLastWriteTime, &ft);
	FileTimeToSystemTime(&ft, &st);

	sysTime = new struct tm();

	sysTime->tm_hour = st.wHour;
	sysTime->tm_mday = st.wDay;
	sysTime->tm_min = st.wMinute;
	sysTime->tm_mon = st.wMonth - 1;
	sysTime->tm_sec = st.wSecond;
	sysTime->tm_year = st.wYear - 1900;

	return true;
#else
	struct stat fs1;
	if( !stat( path.toUtf8(), &fs1 ) )
	{
		sysTime = localtime(&fs1.st_mtime);
		return true;
	}
	return false;
#endif
}

Zipper::Zipper() : m_uzFile(0)
{
    this->CloseZip();
}


Zipper::Zipper(QString filePath, QString rootFolder, bool bAppend) : m_uzFile(0)
{
    this->CloseZip();
    this->OpenZip(filePath, rootFolder, bAppend);
}


Zipper::~Zipper()
{
    this->CloseZip();
}


bool Zipper::CloseZip()
{
    int nRet = m_uzFile ? zipClose(m_uzFile, NULL) : ZIP_OK;

    m_uzFile = NULL;
    m_rootFolder = "";
    return (nRet == ZIP_OK);
}

void Zipper::GetFileInfo(ZipFileInfo& info)
{
    info = m_info;
}

bool Zipper::ZipFile(QString filePath, QString zipFilePath, int level)
{
    bool res = false;
    // make zip path
    /*if(zipFilePath.empty())
    {
       zipFilePath = ::boost::filesystem::change_extension(filePath, ".zip");
    }*/

    Zipper zip;
	QFileInfo filePathInfo;
    if (zip.OpenZip(zipFilePath, filePathInfo.dir().path(), false))
    {
        res = zip.AddFileToZip(filePath, false, level);
    }
    return res;
}

bool Zipper::ZipFolder(QString folderPath, QString zipFilePath, bool bIgnoreFilePath, int level)
{
    bool res = false;
	QDir folderDir(folderPath);
	folderDir.cdUp();

    // make zip path
    if(zipFilePath.isEmpty())
    {
        QString zipFile(folderPath + ".zip");
		
        zipFilePath = folderDir.currentPath() + "//" + zipFile ;
    }
    Zipper zip;
    if (zip.OpenZip(zipFilePath, folderDir.currentPath(), false))
    {
        res = zip.AddFolderToZip(folderPath, bIgnoreFilePath, level);
    }
    return res;
}

bool Zipper::AddFileToZip(QString filePath, bool bIgnoreFilePath, int level)
{
    if (!m_uzFile)
        return false;

	QString fileName;
    if (bIgnoreFilePath)
    {
		QFileInfo file(filePath);
        fileName = file.fileName();
    }
    else
    {
        fileName = RelativePath(m_rootFolder, filePath);
    }

    // save file attributes
    zip_fileinfo zfi;
    zfi.internal_fa = 0;
#if COZONE_WIN
    zfi.external_fa = GetFileAttributesW( filePath.utf16() );
#else
    zfi.external_fa = GetFileAttributesW( filePath );
#endif
    // save file time
	struct tm *st ;
	if( GetLastModified(filePath, st) )
	{
		zfi.dosDate = 0;
		zfi.tmz_date.tm_year = st->tm_year;
		zfi.tmz_date.tm_mon = st->tm_mon;
		zfi.tmz_date.tm_mday = st->tm_mday;
		zfi.tmz_date.tm_hour = st->tm_hour;
		zfi.tmz_date.tm_min = st->tm_min;
		zfi.tmz_date.tm_sec = st->tm_sec;
#if COZONE_WIN
		delete st;
#endif
	}

    // load input file
    std::ifstream inputFile;
    inputFile.open(filePath.toUtf8(), std::ios::in | std::ios::binary);
    if (!inputFile.is_open())
    {
        return false;
    }

    int nRet = zipOpenNewFileInZip(m_uzFile,
            fileName.toUtf8(),
            &zfi,
            NULL,
            0,
            NULL,
            0,
            NULL,
            Z_DEFLATED,
            level);

    if (nRet == ZIP_OK)
    {
        m_info.nFileCount++;

        // read the file and output to zip
        char pBuffer[BUFFERSIZE];
        unsigned long dwBytesRead = 0, dwFileSize = 0;

        while (nRet == ZIP_OK && inputFile.good())
        {
            inputFile.read(pBuffer, BUFFERSIZE);
            dwBytesRead = inputFile.gcount();
            dwFileSize += dwBytesRead;

            if (dwBytesRead)
                nRet = zipWriteInFileInZip(m_uzFile, pBuffer, dwBytesRead);
            else
                break;
        }

        m_info.dwUncompressedSize += dwFileSize;
    }

    zipCloseFileInZip(m_uzFile);
    inputFile.close();

    return (nRet == ZIP_OK);
}

bool Zipper::AddFileToZip(QString filePath, QString relFolderPath, int level)
{
    if (!m_uzFile)
        return false;

    if (relFolderPath.isEmpty())
        return false;

	// save file attributes
	zip_fileinfo zfi;
	zfi.internal_fa = 0;
#if COZONE_WIN
    zfi.external_fa = GetFileAttributesW( filePath.utf16() );
#else
    zfi.external_fa = GetFileAttributesW( filePath );
#endif

	// save file time
	struct tm *st ;
	if( GetLastModified(filePath, st) )
	{
		zfi.dosDate = 0;
		zfi.tmz_date.tm_year = st->tm_year;
		zfi.tmz_date.tm_mon = st->tm_mon;
		zfi.tmz_date.tm_mday = st->tm_mday;
		zfi.tmz_date.tm_hour = st->tm_hour;
		zfi.tmz_date.tm_min = st->tm_min;
		zfi.tmz_date.tm_sec = st->tm_sec;
#if COZONE_WIN
		delete st;
#endif
	}

    // load input file
    std::ifstream inputFile;
    inputFile.open(filePath.toUtf8(), std::ios::in | std::ios::binary);
    if (!inputFile.is_open())
    {
        return false;
    }

    QString fileName(QFileInfo(filePath).fileName());
    QString zipFilePath = relFolderPath + fileName;

    // open the file in the zip making sure we remove any leading '\'
    int nRet = zipOpenNewFileInZip(m_uzFile,
            zipFilePath.toUtf8(),
            &zfi,
            NULL,
            0,
            NULL,
            0,
            NULL,
            Z_DEFLATED,
            level);

    if (nRet == ZIP_OK)
    {
        m_info.nFileCount++;

        // read the file and output to zip
        char pBuffer[BUFFERSIZE];
        unsigned long dwBytesRead = 0, dwFileSize = 0;

        while (nRet == ZIP_OK && inputFile.good() )
        {
            inputFile.read(pBuffer, BUFFERSIZE);
            dwBytesRead = inputFile.gcount();
            dwFileSize += dwBytesRead;

            if (dwBytesRead)
                nRet = zipWriteInFileInZip(m_uzFile, pBuffer, dwBytesRead);
            else
                break;
        }

        m_info.dwUncompressedSize += dwFileSize;
    }

    zipCloseFileInZip(m_uzFile);
    inputFile.close();

    return (nRet == ZIP_OK);
}

bool Zipper::AddFolderToZip(QString folderPath, bool bIgnoreFilePath, int level)
{
    if (!m_uzFile)
        return false;

    m_info.nFolderCount++;

	// save file attributes
	zip_fileinfo zfi;
	zfi.internal_fa = 0;
#if COZONE_WIN
    zfi.external_fa = GetFileAttributesW( folderPath.utf16() );
#else
    zfi.external_fa = GetFileAttributesW( folderPath );
#endif

	// save file time
	struct tm *st ;
	if( GetLastModified(folderPath, st) )
	{
		zfi.dosDate = 0;
		zfi.tmz_date.tm_year = st->tm_year;
		zfi.tmz_date.tm_mon = st->tm_mon;
		zfi.tmz_date.tm_mday = st->tm_mday;
		zfi.tmz_date.tm_hour = st->tm_hour;
		zfi.tmz_date.tm_min = st->tm_min;
		zfi.tmz_date.tm_sec = st->tm_sec;
#if COZONE_WIN
		delete st;
#endif
	}

    QString folderName;
    if (bIgnoreFilePath)
    {
        folderName = QFileInfo(folderPath).fileName();
    }
    else
    {
        folderName = RelativePath(m_rootFolder, folderPath);
    }

    // open the file in the zip
    zipOpenNewFileInZip(m_uzFile,
            (folderName + "/").toUtf8(),
            &zfi,
            NULL,
            0,
            NULL,
            0,
            NULL,
            Z_DEFLATED,
            level);

    zipCloseFileInZip(m_uzFile);

	QDir dir(folderPath);
	if (dir.exists())
	{
		foreach(QFileInfo fileInfo, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs ))
		{
			if (fileInfo.isFile())
			{
				this->AddFileToZip( fileInfo.filePath(), bIgnoreFilePath, level );
			}
			if (fileInfo.isDir())
			{
				this->AddFolderToZip( fileInfo.filePath(), bIgnoreFilePath, level );
			}
		}
	}
    return true;
}

bool Zipper::OpenZip(QString filePath, QString rootFolder, bool bAppend)
{
    this->CloseZip();

    if (filePath.isEmpty())
        return false;

    int append = APPEND_STATUS_CREATE;
    /*
     * if the file pathname exist and append==APPEND_STATUS_CREATEAFTER,
     * the zip will be created at the end of the file.
     * (useful if the file contain a self extractor code)
     * if the file pathname exist and append==APPEND_STATUS_ADDINZIP, we will
     * add files in existing zip (be sure you don't add file that doesn't exist)
     */
    if (QFileInfo(filePath).exists())
    {
        append = (bAppend ? APPEND_STATUS_ADDINZIP : APPEND_STATUS_CREATE);
    }
    m_uzFile = zipOpen(filePath.utf16(), append);

    if (m_uzFile)
    {
        if (rootFolder.isEmpty())
        {
            m_rootFolder = QFileInfo(filePath).dir().path();
        }
        else
        {
            m_rootFolder = rootFolder;
        }
    }
    return (m_uzFile != NULL);
}

QString Zipper::RelativePath( const QString& rootFolder, const QString& file )
{	
	QString relativePath = QString();
	if( file == rootFolder )
		return relativePath;
	
	QFileInfo fileInfo(file);
	QString fileName = fileInfo.fileName();

	QString parentDir = fileInfo.dir().path();
	
	if(parentDir.startsWith(rootFolder))
	{
		parentDir = parentDir.remove(0, rootFolder.length() + 1);
		if(!parentDir.isEmpty())
			relativePath = parentDir + "/"+ fileName;
		else
			relativePath = fileName;
	}
	return relativePath;
}