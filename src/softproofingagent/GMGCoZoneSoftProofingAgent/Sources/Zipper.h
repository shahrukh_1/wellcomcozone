﻿#ifndef GMGCOZONESOFTPROOFINGAGENTZIPPER_H
#define GMGCOZONESOFTPROOFINGAGENTZIPPER_H

#include <QString>
#include <QFile>
#include <QDir>
#include "Globals.h"

#include "../../3rdParty/ZLib/MiniZip/zip.h"

struct ZipFileInfo
{
    int nFileCount;
    int nFolderCount;
    unsigned long dwUncompressedSize;
};

class Zipper
{
public:
    Zipper();

    Zipper(QString filePath, QString rootFolder, bool bAppend = false);

    virtual ~Zipper();

    static bool ZipFile(QString filePath, QString zipFilePath = "", int level = Z_DEFAULT_COMPRESSION); // saves as same name with .zip
    static bool ZipFolder(QString folderPath, QString zipFilePath = "", bool bIgnoreFilePath=false, int level = Z_DEFAULT_COMPRESSION); // saves as same name with .zip

    /*
     *  The compression level must be Z_DEFAULT_COMPRESSION, or between 0 and 9:
     *  1 gives best speed,
     *  9 gives best compression,
     *  0 gives no compression at all (the input data is simply copied a block at a time).
     *  Z_DEFAULT_COMPRESSION requests a default compromise between speed and compression (currently equivalent to level 6).
     */
    bool AddFileToZip(QString filePath, bool bIgnoreFilePath = false, int level = Z_DEFAULT_COMPRESSION );
    bool AddFileToZip(QString filePath, QString relFolderPath, int level = Z_DEFAULT_COMPRESSION); // replaces path info from filePath with folder
    bool AddFolderToZip(QString folderPath, bool bIgnoreFilePath = false, int level = Z_DEFAULT_COMPRESSION);

    // extended interface
    bool OpenZip(QString filePath, QString rootFolder = "", bool bAppend = false);
    bool CloseZip(); // for multiple reuse
    void GetFileInfo(ZipFileInfo& info);

    QString RelativePath( const QString& rootFolder, const QString& file );

protected:

    void* m_uzFile;
    QString m_rootFolder;
    ZipFileInfo m_info;

    unsigned long getFileAttributes(QString const filePath);
};

#endif // GMGCOZONESOFTPROOFINGAGENTZIPPER_H
