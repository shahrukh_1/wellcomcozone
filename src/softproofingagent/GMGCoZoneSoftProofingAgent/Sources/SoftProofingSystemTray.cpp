#include "SoftProofingSystemTray.h"
#include "SoftProofingWebClient.h"
#include "CalibrationTools/BasicColorDisplayCalibrationTool.h"
#include "CalibrationTools/OtherCalibrationTool.h"
#include "AppConfig.h"
#include "Utils.h"
#include "Settings.h"
#include <QAction>
#include <QDir>
#include <QTimer>
#include <QMenu>
#include <QtGui/QDesktopServices>

SoftProofingSystemTray::SoftProofingSystemTray( QObject* parent /* = 0 */ ) :
QSystemTrayIcon( parent ),
mProgressDialog( NULL ),
mPreferencesDialog ( NULL )
{
	bool isFirstRun;
	Settings::getInstance().LoadSettings(isFirstRun);
	if (isFirstRun)
	{
		SetStartAtLogin();
	}

	FillControls();

	mIsStartSession = false;
	//create a temporary folder path where to store intermediary data
	QString s = QStandardPaths::standardLocations(QStandardPaths::DataLocation ).first();
	mTemporaryFolderPath = QDir(s).absolutePath() + "/" + APP_NAME;
	QDir().mkpath( mTemporaryFolderPath );

	//create calibration tools list
	mCalibrationTools.append(new BasicColorDisplayCalibrationTool( mTemporaryFolderPath ));
	mCalibrationTools.append(new OtherCalibrationTool( mTemporaryFolderPath ));

	//create timer for http posts to CoZone(QT doesnt currently provide a timeout mechanism)
	mRequestsTimeoutTimer = new QTimer( this );
	mRequestsTimeoutTimer->setSingleShot( true );
	mRequestsTimeoutTimer->setInterval( 15000 ); //15 sec
	connect( mRequestsTimeoutTimer, SIGNAL( timeout() ), this, SLOT( NetworkTimeout() ) );

	connect( &SoftProofingWebClient::getInstance(), SIGNAL( finished( QNetworkReply* ) ), this, SLOT( ReplyReceived( QNetworkReply* ) ) );

	Utils::SetLogLevel( AppConfig::getInstance().GetLogLevel() );

	//check if a guid for that machine was generated or not
	if(Settings::getInstance().GetMachineID().isEmpty())
	{
		Settings::getInstance().SetMachineID( Utils::GenerateMachineID() );
	}
}

void SoftProofingSystemTray::FillControls()
{
	connect( this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
			 this, SLOT( IconActivated(QSystemTrayIcon::ActivationReason) ) );

	mStartSessionAction = new QAction(tr("&Start Session"), this);
	connect( mStartSessionAction, SIGNAL(triggered()), this, SLOT( StartSession() ));

	mPreferencesAction = new QAction(tr("&Preferences"), this);
	connect( mPreferencesAction, SIGNAL(triggered()), this, SLOT( ShowPreferencesDialog() ));

	mQuitAction = new QAction(tr("&Quit"), this);
	connect( mQuitAction, SIGNAL(triggered()), this, SLOT( QuitApplication() ) );

	mOpenAtLoginAction = new QAction(tr("&Auto-Start"), this);
	mOpenAtLoginAction->setCheckable( true );
	connect( mOpenAtLoginAction, SIGNAL(triggered()), this, SLOT( OpenAtLogin() ) );
#if COZONE_WIN
	QSettings settings( WINDOWS_STARTUP_REGISTRY, QSettings::NativeFormat );
	QSettings startupApprovedRegistry(WINDOWS_STARTUP_APPROVED_ENTIRE_REGISTRY, QSettings::NativeFormat);
	QByteArray value = startupApprovedRegistry.value(APP_NAME).toByteArray();
	if (settings.contains(APP_NAME) && (!startupApprovedRegistry.contains(APP_NAME) || value.at(0) == 0))
	{
		mOpenAtLoginAction->setChecked(true);
	}
	else 
	{
		settings.remove(APP_NAME);
	}
#else
    mOpenAtLoginAction->setChecked( Utils::IsLoginItemSet() );
#endif

	mTrayIconMenu = new QMenu();
	mTrayIconMenu->addAction( mStartSessionAction );
	mTrayIconMenu->addAction( mPreferencesAction );
	mTrayIconMenu->addAction( mOpenAtLoginAction );
	mTrayIconMenu->addSeparator();
	mTrayIconMenu->addAction( mQuitAction );

	setContextMenu( mTrayIconMenu );
	setIcon( QIcon( ":/GMGCoZoneSoftProofingAgent/favicon.png" ) );
	setToolTip( tr( "CoZone Collaborate SoftProofing Agent" ) );
}

int SoftProofingSystemTray::ShowMessageBox( QString title, QString message, QMessageBox::StandardButtons buttons )
{
	QMessageBox msgBox;
	msgBox.setWindowModality( Qt::WindowModal );
	msgBox.setIcon( QMessageBox::Information );
	msgBox.setText(title);
	if(!message.isEmpty())
	{
		msgBox.setInformativeText(message);
	}
	msgBox.setStandardButtons(buttons);
	return msgBox.exec();
}

void SoftProofingSystemTray::StartSession()
{
	if (CheckCalibrationChanges()) {
		QFileInfo browserPath(Settings::getInstance().GetBrowserPath());
		QString errStartBrowserFailed = tr("Failed to open the selected browser!");
		std::string explorerPath(Settings::getInstance().GetBrowserPath().toStdString());
		bool isMicrosoftEdge = explorerPath.find("microsoft-edge") != std::string::npos;

		if (isMicrosoftEdge || browserPath.exists())
		{
			SetIsStartSession(true);

			UploadData(true);
		}
		else
		{
			ShowMessageBox(errStartBrowserFailed);
		}
	}
}

void SoftProofingSystemTray::ResetProgressDialog()
{
	if( mProgressDialog == NULL )
	{
		//set preferences window as parent to show the modal pop-up on top of it
		mProgressDialog = new UploadDataProgress( mPreferencesDialog != NULL ? mPreferencesDialog : (QWidget*)0 );
		connect( mProgressDialog, SIGNAL( ReleaseDialog() ), this, SLOT( CloseProgressDialog() ) );
	}
	else
	{
		mProgressDialog->ResetStatuses();
	}
}

bool SoftProofingSystemTray::CheckCalibrationChanges()
{
	bool checkIsValid = true;

	QString errorMessage;
	CalibrationToolBase* currentCalibrationTool = GetSelectedCalibrationTool();
	if (currentCalibrationTool != NULL && currentCalibrationTool->IsUploadDataRequired(errorMessage))
	{
		ResetProgressDialog();
		QList<MonitorInfo> monitors = Utils::GetAvailableDisplays();

		if (Settings::getInstance().GetDisplayIndex() >= monitors.count())
		{
			errorMessage = tr("Start Session Failed: The selected display was not found in the display list!");
		}
	}

	if (!errorMessage.isEmpty())
	{
		checkIsValid = false;
		mProgressDialog->SetUploadDataStatus(tr("Error"));
		mProgressDialog->SetErrorMessage(errorMessage);
		mProgressDialog->show();
	}

	return checkIsValid;
}


void SoftProofingSystemTray::IconActivated(QSystemTrayIcon::ActivationReason reason)
{
	switch (reason) {
	case QSystemTrayIcon::Trigger:
		break;
	case QSystemTrayIcon::DoubleClick:
		ShowPreferencesDialog();
		break;
	case QSystemTrayIcon::MiddleClick:
		break;
	default:
		break;
	}
}

void SoftProofingSystemTray::NetworkTimeout()
{
	SoftProofingWebClient::getInstance().blockSignals( true );
	SoftProofingWebClient::getInstance().AbortConnection();
	SoftProofingWebClient::getInstance().blockSignals( false );

	mProgressDialog->SetErrorMessage( tr("Server not responding. Please retry the operation or contact support if the problem persist") );
}

void SoftProofingSystemTray::ShowPreferencesDialog()
{
	if( !mPreferencesDialog )
	{
		if( Settings::getInstance().GetDataLocation().isEmpty() )
		{
			foreach( CalibrationToolBase* calibrationTool, mCalibrationTools )
			{
				if( calibrationTool->GetOutputFolderLocation().isEmpty() )
				{
					calibrationTool->SetDefaultFolderLocation();
				}
			}
		}
        mPreferencesDialog = new Preferences( mCalibrationTools, 0, Qt::WindowTitleHint | Qt::WindowSystemMenuHint | Qt::MSWindowsFixedSizeDialogHint | Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
        mPreferencesDialog->show();
        mPreferencesDialog->raise();	

		connect(mPreferencesDialog, SIGNAL(OKButtonPressed()), this, SLOT(OKButtonHandler()));
		connect(mPreferencesDialog, SIGNAL(StartSessionButtonPressed()), this, SLOT(StartSessionButtonHandler()));
		connect(mPreferencesDialog, SIGNAL(Close()), this, SLOT(ClosePreferencesWindow()));
	}
	else
	{
		mPreferencesDialog->raise();
	}
}

void SoftProofingSystemTray::SetStartAtLogin()
{
#if COZONE_WIN
    QSettings startupSettings(WINDOWS_STARTUP_REGISTRY, QSettings::NativeFormat);
    startupSettings.setValue(APP_NAME, "\"" + QCoreApplication::applicationFilePath().replace("/", "\\") + "\"");
    
    UCHAR byteRegArray[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    HKEY hKey;
    LONG openRes = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT(WINDOWS_STARTUP_APPROVED_REGISTRY), 0, KEY_ALL_ACCESS, &hKey);
    RegSetValueEx(hKey, TEXT(APP_NAME), 0, REG_BINARY, (BYTE *)byteRegArray, sizeof(byteRegArray));
#else
    Utils::AddTrayToStartup(Utils::GetBundlePath());
#endif
}

void SoftProofingSystemTray::OpenAtLogin()
{
    if( mOpenAtLoginAction->isChecked() )
    {
        SetStartAtLogin();
    }
    else
    {
#if COZONE_WIN
        //for windows remove value from registry
        QSettings settings( WINDOWS_STARTUP_REGISTRY, QSettings::NativeFormat );
        settings.remove( APP_NAME );
#else
        Utils::RemoveTrayFromStartup();
#endif
    }
}

void SoftProofingSystemTray::OKButtonHandler()
{
	if( mPreferencesDialog != NULL )
	{
		UploadData( true );
	}
}

void SoftProofingSystemTray::StartSessionButtonHandler()
{
	if (mPreferencesDialog != NULL)
	{
		StartSession();
	}
}

void SoftProofingSystemTray::ClosePreferencesWindow()
{
    mPreferencesDialog->deleteLater();
	mPreferencesDialog = NULL;
}

void SoftProofingSystemTray::QuitApplication()
{
	qApp->quit();
}

void SoftProofingSystemTray::CloseProgressDialog()
{
	mProgressDialog = NULL;
	mPreferencesAction->setDisabled( false );
}

void SoftProofingSystemTray::ReplyReceived( QNetworkReply* reply )
{
	mRequestsTimeoutTimer->stop();

	QNetworkRequest originalRequest = reply->request();
	QString requestUrl = originalRequest.url().toString();
	int httpStatusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();

	if( reply->isOpen() || reply->open( QIODevice::ReadOnly ) )
	{
		QByteArray data = reply->readAll();

		QString errorMessage;
		if( requestUrl.contains( AppConfig::getInstance().GetVerifyAccountURL() ) )
		{
			if( reply->error() != QNetworkReply::NoError || !( httpStatusCode >= 200 && httpStatusCode < 300 ) )
			{
				SoftProofingSystemTray::SetIsStartSession(false);

				mProgressDialog->SetVerifyAccountStatus( "<font color='red'>" + tr("Error") + "</font>" );
				if( !data.isNull() && !data.isEmpty() )
				{
					errorMessage = QString( data );
					Utils::Log( LEVEL_ERROR, ( "Verify Account request failed: " + errorMessage + " Status Code: " + httpStatusCode ).toUtf8().constData() );
				}
				else
				{
					errorMessage = reply->errorString();
					Utils::Log( LEVEL_ERROR, ( "Verify Account request failed: " + errorMessage + " Status Code: " + httpStatusCode ).toUtf8().constData() );
				}

				if( httpStatusCode >= 300 && httpStatusCode < 400 )
				{
					//redirect
					mProgressDialog->SetErrorMessage( tr("Authentication failed.") );
				}
				else
				{
					//an error encountered
					mProgressDialog->SetErrorMessage( tr("Authentication failed: ") + errorMessage );
				}
			}
			else
			{
				if (mProgressDialog == NULL)
				{
					ResetProgressDialog();
				}

				// If verify account succeeds send upload data
				mProgressDialog->SetVerifyAccountStatus( "<font color='darkGreen'>" + tr("Verified") + "</font>" );
				mProgressDialog->SetUploadDataStatus( tr("In progress" ) );
				QString userKey = QString( data );
				Settings::getInstance().SetCoZoneUserKey( userKey );
				mProgressDialog->SetCurrentAction( UploadDataProgress::eSendCalibrationData );
				SendCalibrationDataRequest();
				mRequestsTimeoutTimer->start();
			}
		}
		else if ( requestUrl.contains( AppConfig::getInstance().GetUploadDataURL() ) )
		{
			if( reply->error() != QNetworkReply::NoError || !( httpStatusCode >= 200 && httpStatusCode < 300 ) )
			{
				SoftProofingSystemTray::SetIsStartSession(false);

				mProgressDialog->SetUploadDataStatus( "<font color='red'>" + tr("Error") + "</font>" );
				if( !data.isNull() && !data.isEmpty() )
				{
					errorMessage = QString( data );
					Utils::Log( LEVEL_ERROR, ( "Upload data request failed: " + errorMessage ).toUtf8().constData() );
				}
				else
				{
					errorMessage = reply->errorString();
					Utils::Log( LEVEL_ERROR, ( "Upload data request failed: " + errorMessage ).toUtf8().constData() );
				}
				if( httpStatusCode >= 300 && httpStatusCode < 400 )
				{
					//redirect
					mProgressDialog->SetErrorMessage( tr("Uploading calibration data failed ") );
				}
				else
				{
					//error
					
					mProgressDialog->SetErrorMessage( tr("Uploading calibration data failed: ") + errorMessage );
				}

				if( !mProgressDialog->isVisible() )
				{
					mProgressDialog->show();
				}
			}
			else
			{
				if (mProgressDialog == NULL)
				{
					ResetProgressDialog();
				}

				//send calibration data success
				mProgressDialog->SetUploadDataStatus( "<font color='darkGreen'>" + tr("Synchronized" ) + "</font>" );
				Settings::getInstance().SetLastSuccessfulUpload( QDateTime::currentMSecsSinceEpoch() );
				Utils::Log( LEVEL_DEBUG, "Upload data request succeeded" );

				// close dialog for start session&agent upload data requests in case the request succeeds
				if( mProgressDialog != NULL)
				{
					mProgressDialog->close();
				}

				if (SoftProofingSystemTray::GetIsStartSession())
				{
					LaunchBrowser();
				}

				CloseProgressDialog();
				ClosePreferencesWindow();
			}
		}
		else
		{
			SoftProofingSystemTray::SetIsStartSession(false);

			mProgressDialog->SetVerifyAccountStatus("<font color='red'>" + tr("Error") + "</font>");
			if (!data.isNull() && !data.isEmpty())
			{
				errorMessage = QString(data);
				Utils::Log(LEVEL_ERROR, ("Verify Account request failed: " + errorMessage + " Status Code: " + httpStatusCode).toUtf8().constData());
			}
			else
			{
				errorMessage = reply->errorString();
				Utils::Log(LEVEL_ERROR, ("Verify Account request failed: " + errorMessage + " Status Code: " + httpStatusCode).toUtf8().constData());
			}

			if (httpStatusCode >= 300 && httpStatusCode < 400)
			{
				//redirect
				mProgressDialog->SetErrorMessage(tr("Authentication failed."));
			}
			else
			{
				//an error encountered
				mProgressDialog->SetErrorMessage(tr("Authentication failed: ") + errorMessage);
			}
		}
	}
	else
	{
		Utils::Log( LEVEL_ERROR, ( "Reply message could not be open for reading" ) );
	}

	reply->close();
	reply->deleteLater();
}

void SoftProofingSystemTray::UploadData( bool verifyAccount )
{
	ResetProgressDialog();

	if( verifyAccount )
	{
		mProgressDialog->SetVerifyAccountStatus( tr("In progress") );
		VerifyAccount();
		mProgressDialog->show();
		mProgressDialog->SetCurrentAction( UploadDataProgress::eVerifyAccount );
	}
	else
	{
		mProgressDialog->SetUploadDataStatus( tr("In progress") );
		mProgressDialog->SetCurrentAction( UploadDataProgress::eSendCalibrationData );
		SendCalibrationDataRequest();
	}
	mRequestsTimeoutTimer->start();
	
}

void SoftProofingSystemTray::SendCalibrationDataRequest()
{
	CalibrationToolBase* currentCalibrationTool = GetSelectedCalibrationTool();
	if( currentCalibrationTool != NULL )
	{
		currentCalibrationTool->SetFolderLocation( Settings::getInstance().GetDataLocation() );
		QByteArray postData = currentCalibrationTool->GetSubmitDataRequest( Settings::getInstance().GetCoZoneUserKey(), Settings::getInstance().GetProfileLocation(), Settings::getInstance().GetDisplayIndex(), Settings::getInstance().GetDisplayName(), Settings::getInstance().GetMachineID() );
		SoftProofingWebClient::getInstance().UploadData( Settings::getInstance().GetAccountName() + Settings::getInstance().GetDomainURL(), postData );
	}
}

CalibrationToolBase* SoftProofingSystemTray::GetSelectedCalibrationTool()
{
	foreach(CalibrationToolBase* calibrationTool, mCalibrationTools)
	{
		if( calibrationTool->GetToolName() == Settings::getInstance().GetCalibrationName() )
			return calibrationTool;
	}
	return NULL;
}

void SoftProofingSystemTray::VerifyAccount()
{
	SoftProofingWebClient::getInstance().VerifyAccount(Settings::getInstance().GetAccountName() + Settings::getInstance().GetDomainURL(), Settings::getInstance().GetCoZoneUserName(), Settings::getInstance().GetCoZoneUserPassword());
}

void SoftProofingSystemTray::LaunchBrowser()
{
	QString sessionURL = AppConfig::getInstance().GetServiceProtocol() + "://" + Settings::getInstance().GetAccountName() + Settings::getInstance().GetDomainURL() + AppConfig::getInstance().GetInitSessionURL() + "?" + "UserGuid=" + Settings::getInstance().GetCoZoneUserKey() + "&MachineId=" + Settings::getInstance().GetMachineID();
	QString errStartBrowserFailed = tr("Failed to open the selected browser!");

#if COZONE_WIN			
	std::string explorerPath(Settings::getInstance().GetBrowserPath().toStdString());
	bool isMicrosoftEdge = explorerPath.find("microsoft-edge") != std::string::npos;
	if (isMicrosoftEdge) {
		explorerPath.append(sessionURL.toStdString());
	}

	if ((int)ShellExecuteA(0, 0, explorerPath.c_str(), sessionURL.toStdString().c_str(), 0, SW_SHOWMAXIMIZED) <= 32)
	{
        SoftProofingSystemTray::SetIsStartSession(false);
		ShowMessageBox(errStartBrowserFailed);
	}
#else
	FSRef appFSURL;

	OSStatus status = FSPathMakeRef((const UInt8 *)Settings::getInstance().GetBrowserPath().toUtf8().constData(), &appFSURL, NULL);

	if (status < 0)
	{
        SoftProofingSystemTray::SetIsStartSession(false);
		ShowMessageBox(errStartBrowserFailed);
		return;
	}

	LSApplicationParameters appParams;
	appParams.version = 0;
	appParams.flags = kLSLaunchDefaults;
	appParams.argv = NULL;
	appParams.environment = NULL;
	appParams.asyncLaunchRefCon = NULL;
	appParams.initialEvent = NULL;
	appParams.application = &appFSURL;

	CFArrayRef array;

	const char* cStr = sessionURL.toUtf8().constData();
	CFURLRef urls[1];
	urls[0] = CFURLCreateWithString(NULL, CFStringCreateWithCString(NULL, cStr, kCFStringEncodingUTF8), NULL);

	array = CFArrayCreate(kCFAllocatorDefault, (const void **)urls, 1, &kCFTypeArrayCallBacks);
	status = LSOpenURLsWithRole(array, kLSRolesAll, NULL, &appParams, NULL, 0);

	CFRelease(array);

	if (status < 0)
	{
        SoftProofingSystemTray::SetIsStartSession(false);
		ShowMessageBox(errStartBrowserFailed);
		return;
	}
#endif
    
	SoftProofingSystemTray::SetIsStartSession(false);
}

SoftProofingSystemTray::~SoftProofingSystemTray()
{
	delete mPreferencesAction;
	delete mStartSessionAction;
	delete mOpenAtLoginAction;
	delete mTrayIconMenu;
	delete mQuitAction;
	delete mRequestsTimeoutTimer;
	delete mProgressDialog;
	delete mPreferencesDialog;

	//clear calibration tools
	while( !mCalibrationTools.isEmpty() )
	{
		CalibrationToolBase* currentTool = mCalibrationTools.takeFirst();
		delete currentTool;
	}
}
