#include "Utils.h"
#include <QString>
#include <QFile>
#include <ctime>
#include <QApplication>
#include <QSettings>
#include <QDir>
#include <QUuid>
#include "BrowserInfo.h"

#if COZONE_MAC
#include <ApplicationServices/ApplicationServices.h>
#include <IOKit/graphics/IOGraphicsLib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/sysctl.h>
#endif

int Utils::mLogLevel = LEVEL_INFO;
QString Utils:: mLogFile = QString();

#if COZONE_WIN
QString Utils::GetGDIDeviceNameFromSource( LUID adapterId, UINT32 sourceId )
{
	DISPLAYCONFIG_SOURCE_DEVICE_NAME deviceName;
	DISPLAYCONFIG_DEVICE_INFO_HEADER header;
	header.size = sizeof(DISPLAYCONFIG_SOURCE_DEVICE_NAME);
	header.adapterId = adapterId;
	header.id = sourceId;
	header.type = DISPLAYCONFIG_DEVICE_INFO_GET_SOURCE_NAME;
	deviceName.header = header;
	DisplayConfigGetDeviceInfo( (DISPLAYCONFIG_DEVICE_INFO_HEADER*) &deviceName );
	return QString::fromUtf16(deviceName.viewGdiDeviceName);	
}

QString Utils::GetMonitorDevicePathFromTarget( LUID adapterId, UINT32 targetId )
{
	DISPLAYCONFIG_TARGET_DEVICE_NAME deviceName;
	DISPLAYCONFIG_DEVICE_INFO_HEADER header;
	header.size = sizeof(DISPLAYCONFIG_TARGET_DEVICE_NAME);
	header.adapterId = adapterId;
	header.id = targetId;
	header.type = DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME;
	deviceName.header = header;
	DisplayConfigGetDeviceInfo( (DISPLAYCONFIG_DEVICE_INFO_HEADER*) &deviceName );
	return QString::fromUtf16(deviceName.monitorDevicePath);
}

QString Utils::GetFriendlyNameFromTarget( LUID adapterId, UINT32 targetId )
{
	DISPLAYCONFIG_TARGET_DEVICE_NAME deviceName;
	DISPLAYCONFIG_DEVICE_INFO_HEADER header;
	header.size = sizeof(DISPLAYCONFIG_TARGET_DEVICE_NAME);
	header.adapterId = adapterId;
	header.id = targetId;
	header.type = DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME;
	deviceName.header = header;
	DisplayConfigGetDeviceInfo( (DISPLAYCONFIG_DEVICE_INFO_HEADER*) &deviceName );
	return QString::fromUtf16(deviceName.monitorFriendlyDeviceName);
}
#else

static void KeyArrayCallback(const void* key, const void* value, CFMutableArrayRef context) { CFArrayAppendValue(context, key);  }

QString Utils::CFScreenNameForDisplay( CGDirectDisplayID displayID )
{
    QString retName;
    CFStringRef localName = NULL;
    io_connect_t displayPort = CGDisplayIOServicePort(displayID);
    CFDictionaryRef dict = (CFDictionaryRef)IODisplayCreateInfoDictionary(displayPort, 0);
    CFDictionaryRef names = (CFDictionaryRef)CFDictionaryGetValue(dict, CFSTR(kDisplayProductName));
    if(names)
    {
    	CFArrayRef langKeys = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks );
    	CFDictionaryApplyFunction(names, (CFDictionaryApplierFunction)KeyArrayCallback, (void*)langKeys);
    	CFArrayRef orderLangKeys = CFBundleCopyPreferredLocalizationsFromArray(langKeys);
    	CFRelease(langKeys);
    	if(orderLangKeys && CFArrayGetCount(orderLangKeys))
    	{
    		CFStringRef langKey = (CFStringRef)CFArrayGetValueAtIndex(orderLangKeys, 0);
    		localName = (CFStringRef)CFDictionaryGetValue(names, langKey);
            retName = CFStringToQString( localName );
    		CFRetain(localName);
    	}
    	CFRelease(orderLangKeys);
    }
    CFRelease(dict);
    
    return retName;
}

void Utils::AddTrayToStartup( const char* appPath )
{
    char script[1024];
	::sprintf( script, "tell application \"System Events\"\n make login item at end with properties {path:\"%s\", hidden:false}\n end", appPath );
    
    ComponentInstance theComponent;
	AEDesc scriptTextDesc;
	OSStatus err;
	OSAID scriptID, resultID;
	AEDesc *resultData = new AEDesc();
	
	/* set up locals to a known state */
	theComponent = NULL;
	AECreateDesc(typeNull, NULL, 0, &scriptTextDesc);
	scriptID = kOSANullScript;
	resultID = kOSANullScript;
	
	/* open the scripting component */
	theComponent = OpenDefaultComponent(kOSAComponentType,
										typeAppleScript);
	if (theComponent == NULL) { err = paramErr; goto bail; }
	
	/* put the script text into an aedesc */
	err = AECreateDesc(typeChar, script, ::strlen(script), &scriptTextDesc);
	if (err != noErr) goto bail;
	
	/* compile the script */
	err = OSACompile(theComponent, &scriptTextDesc,
					 kOSAModeNull, &scriptID);
	if (err != noErr) goto bail;
	
	/* run the script */
	err = OSAExecute(theComponent, scriptID, kOSANullScript,
					 kOSAModeNull, &resultID);
	
	/* collect the results ñ if any */
	AECreateDesc(typeNull, NULL, 0, resultData);
	if (err == errOSAScriptError) {
		OSAScriptError(theComponent, kOSAErrorMessage,
					   typeChar, resultData);
	} else if (err == noErr && resultID != kOSANullScript) {
		OSADisplay(theComponent, resultID, typeChar,
				   kOSAModeNull, resultData);
	}
bail:
	delete resultData;
	AEDisposeDesc(&scriptTextDesc);
	if (scriptID != kOSANullScript) OSADispose(theComponent, scriptID);
	if (resultID != kOSANullScript) OSADispose(theComponent, resultID);
	if (theComponent != NULL) CloseComponent(theComponent);
}

void Utils::RemoveTrayFromStartup()
{
    char script[1024];
	::sprintf( script, "tell application \"System Events\" to delete login item \"%s\" \n end", "SoftProofingAgent" );
    
    ComponentInstance theComponent;
	AEDesc scriptTextDesc;
	OSStatus err;
	OSAID scriptID, resultID;
	AEDesc *resultData = new AEDesc();
	
	/* set up locals to a known state */
	theComponent = NULL;
	AECreateDesc(typeNull, NULL, 0, &scriptTextDesc);
	scriptID = kOSANullScript;
	resultID = kOSANullScript;
	
	/* open the scripting component */
	theComponent = OpenDefaultComponent(kOSAComponentType,
										typeAppleScript);
	if (theComponent == NULL) { err = paramErr; goto bail; }
	
	/* put the script text into an aedesc */
	err = AECreateDesc(typeChar, script, ::strlen(script), &scriptTextDesc);
	if (err != noErr) goto bail;
	
	/* compile the script */
	err = OSACompile(theComponent, &scriptTextDesc,
					 kOSAModeNull, &scriptID);
	if (err != noErr) goto bail;
	
	/* run the script */
	err = OSAExecute(theComponent, scriptID, kOSANullScript,
					 kOSAModeNull, &resultID);
	
	/* collect the results ñ if any */
	AECreateDesc(typeNull, NULL, 0, resultData);
	if (err == errOSAScriptError) {
		OSAScriptError(theComponent, kOSAErrorMessage,
					   typeChar, resultData);
	} else if (err == noErr && resultID != kOSANullScript) {
		OSADisplay(theComponent, resultID, typeChar,
				   kOSAModeNull, resultData);
	}
bail:
	delete resultData;
	AEDisposeDesc(&scriptTextDesc);
	if (scriptID != kOSANullScript) OSADispose(theComponent, scriptID);
	if (resultID != kOSANullScript) OSADispose(theComponent, resultID);
	if (theComponent != NULL) CloseComponent(theComponent);
}

bool Utils::IsLoginItemSet()
{
    UInt32 seedValue;
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    CFArrayRef loginItemsArray = LSSharedFileListCopySnapshot(loginItems, &seedValue);
    
    bool found = false;
    for(int i = 0; i < CFArrayGetCount(loginItemsArray); i++)
    {
        LSSharedFileListItemRef item = (LSSharedFileListItemRef)CFArrayGetValueAtIndex(loginItemsArray, i);
        
        CFStringRef name = LSSharedFileListItemCopyDisplayName(item);
        
        if(CFStringToQString( name ) == QString("SoftProofingAgent") )
        {
            found = true;
            break;
        }
         CFRelease(name);
         CFRelease(item);
    }

    CFRelease(loginItems);
    
    return found;
}

const  char* Utils::GetBundlePath()
{
    CFURLRef appUrlRef = CFBundleCopyBundleURL(CFBundleGetMainBundle());
    CFStringRef macPath = CFURLCopyFileSystemPath(appUrlRef, kCFURLPOSIXPathStyle);
    const char* pathPtr = CFStringGetCStringPtr(macPath, CFStringGetSystemEncoding());
    CFRelease(appUrlRef);
    CFRelease(macPath);

    return pathPtr;
}

#endif

QList<MonitorInfo> Utils::GetAvailableDisplays()
{
	QList<MonitorInfo> availableMonitorsList;
#if COZONE_WIN

	UINT32 num_of_paths = 0;
	UINT32 num_of_modes = 0;
	DISPLAYCONFIG_PATH_INFO* displayPaths = NULL; 
	DISPLAYCONFIG_MODE_INFO* displayModes = NULL;

	UINT32 num_of_paths2 = 0;
	UINT32 num_of_modes2 = 0;
	DISPLAYCONFIG_PATH_INFO* displayPaths2 = NULL; 
	DISPLAYCONFIG_MODE_INFO* displayModes2 = NULL;

	GetDisplayConfigBufferSizes(QDC_ALL_PATHS, &num_of_paths, &num_of_modes);

	// Allocate paths and modes dynamically
	displayPaths = (DISPLAYCONFIG_PATH_INFO*)calloc((int)num_of_paths, sizeof(DISPLAYCONFIG_PATH_INFO));
	displayModes = (DISPLAYCONFIG_MODE_INFO*)calloc((int)num_of_modes, sizeof(DISPLAYCONFIG_MODE_INFO));

	// Query for the information 
	QueryDisplayConfig(QDC_ALL_PATHS, &num_of_paths, displayPaths, &num_of_modes, displayModes, NULL);

	for (int i = 0; i < num_of_modes; i++) {

		switch (displayModes[i].infoType) {

			// This case is for all sources
		case DISPLAYCONFIG_MODE_INFO_TYPE_SOURCE:
			{
				//GetGDIDeviceNameFromSource(displayModes[i].adapterId, displayModes[i].id);
			}			
			break;

			// This case is for all targets
		case DISPLAYCONFIG_MODE_INFO_TYPE_TARGET:
			{
				QString monitorDevicePath = GetMonitorDevicePathFromTarget(displayModes[i].adapterId, displayModes[i].id);
				QString friendlyName = GetFriendlyNameFromTarget(displayModes[i].adapterId, displayModes[i].id);
				QString GDIDeviceName = GetGDIDeviceNameFromSource(displayModes[i].adapterId, i/2);
				
				if (friendlyName.isNull() || friendlyName.isEmpty())
				{
					friendlyName = "Default display";
				}

				// get monitor profile file path
				wchar_t filename[1024];
				HDC dc;
				DWORD size = sizeof(filename); 
				if( !GDIDeviceName.isEmpty() )
				{			
					dc = CreateDC( TEXT("DISPLAY"), GDIDeviceName.utf16(), NULL, NULL );
					if( dc != NULL )
					{
						if( GetICMProfile( dc, &size, filename ) )
						{
							MonitorInfo monitor;
							monitor.Name = friendlyName;
							monitor.Width = GetDeviceCaps(dc, HORZRES);
							monitor.Height = GetDeviceCaps(dc, VERTRES);
							monitor.ProfilePath = QString::fromUtf16( filename );
							availableMonitorsList.append(monitor);
						}
						DeleteDC( dc );
					}
				}

			}
			break;

		default:
			Utils::Log( LEVEL_ERROR, "Get available displays error" );
			break;
		}
	}

	free(displayPaths);
	free(displayModes);

#else
    int maxDisplays = 32;
    CGDirectDisplayID displays[maxDisplays];
    uint32_t numDisplays;
    uint32_t i;
    
    CGGetActiveDisplayList(maxDisplays, displays, &numDisplays);
    
    for( i = 0; i < numDisplays; i++ )
    {
		MonitorInfo monitor;
        monitor.Name = CFScreenNameForDisplay( displays[i] );
		monitor.Height = CGDisplayPixelsHigh( displays[i] );
		monitor.Width =	CGDisplayPixelsWide( displays[i] );
        
        monitor.ProfilePath = GetProfilePath(displays[i]);
		availableMonitorsList.append(monitor);
    }
#endif
    
    Log( LEVEL_INFO, "Get monitor systems success ");
	return availableMonitorsList;
}
#if COZONE_MAC
const char* Utils::GetProfilePath(CGDirectDisplayID id)
{
    CFUUIDRef mainDisplayID = CGDisplayCreateUUIDFromDisplayID( id );
    CFDictionaryRef deviceInfo = ColorSyncDeviceCopyDeviceInfo( kColorSyncDisplayDeviceClass, mainDisplayID );
    CFRelease(mainDisplayID);
    
    if( deviceInfo )
    {
        CFURLRef		profileURL = NULL;
        CFDictionaryRef factoryInfo = (CFDictionaryRef)CFDictionaryGetValue(deviceInfo, kColorSyncFactoryProfiles);
        if( factoryInfo )
        {
            CFStringRef		defaultProfileID = (CFStringRef)CFDictionaryGetValue(factoryInfo, kColorSyncDeviceDefaultProfileID);
            CFDictionaryRef customInfo = (CFDictionaryRef)CFDictionaryGetValue(deviceInfo, kColorSyncCustomProfiles);
            if( customInfo )
                profileURL = (CFURLRef)CFDictionaryGetValue( customInfo, defaultProfileID );
                if( profileURL == NULL )
                {
                    CFDictionaryRef factoryProfile = (CFDictionaryRef)CFDictionaryGetValue( factoryInfo, defaultProfileID );
                    if( factoryProfile )
                        profileURL = (CFURLRef)CFDictionaryGetValue(factoryProfile, kColorSyncDeviceProfileURL);
                        }
        }
        
        if( profileURL )
        {
            CFStringRef pathStr = CFURLCopyFileSystemPath(profileURL, kCFURLPOSIXPathStyle);
            
            const char* profilePath = CFStringGetCStringPtr(pathStr,kCFStringEncodingUTF8);
            CFRelease(deviceInfo);
            return profilePath;
        }
        CFRelease(deviceInfo);
        return NULL;
    }
}
#endif

#if COZONE_WIN
bool Utils::IsMicrosoftEdgeInstalled() 
{
	QString registryPath("HKEY_CLASSES_ROOT\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer\\Storage");
	QSettings settings(registryPath, QSettings::NativeFormat);

	QStringList storageList = settings.childGroups();
	foreach(QString storage, storageList)
	{
		if (storage.contains("microsoftedge"))
			return true;
	}
	return false;
}
#endif

QList<BrowserInfo> Utils::GetAvailableBrowsers()
{
	QList<BrowserInfo> availableBrowsersList;
#if COZONE_WIN
	//Get list of local installed Browsers Win Version
	QString registryPath("HKEY_LOCAL_MACHINE\\SOFTWARE\\Clients\\StartMenuInternet");
	QSettings settings( registryPath, QSettings::NativeFormat );

	QStringList browserList = settings.childGroups();	

	foreach( QString browser, browserList )
	{
		BrowserInfo browserInfo;
		// set the browser name
		QSettings browserName( registryPath + "\\" + browser, QSettings::NativeFormat );		
		
		// Uppercase for browser names
		QString strBrowserName = browserName.value("Default", "0").toString();
		strBrowserName = strBrowserName.left(1).toUpper() + strBrowserName.mid(1);
		browserInfo.Name = strBrowserName;

		// set the browser executable path
		QString regExecPath = registryPath + "\\" + browser + "\\shell\\open\\command";
		QSettings pathRegValue( regExecPath, QSettings::NativeFormat );
		browserInfo.Path = pathRegValue.value( "Default", "0" ).toString();
		if( browserInfo.Path.contains("\""))
		{
			browserInfo.Path.remove("\"");
		}
		availableBrowsersList.append( browserInfo );
	}	
	
	if (IsMicrosoftEdgeInstalled()) {
		BrowserInfo edgeBrowserInfo;
		edgeBrowserInfo.Name = "Microsoft Edge";
		edgeBrowserInfo.Path = "microsoft-edge:";
		availableBrowsersList.append(edgeBrowserInfo);
	}
	
#else
	CFArrayRef browserList = LSCopyAllHandlersForURLScheme(CFSTR("https"));

	for (int i = 0; i < CFArrayGetCount(browserList); i++)
	{
		CFStringRef cfBrowser = (CFStringRef)CFArrayGetValueAtIndex( browserList, i );
		QString browser = CFStringToQString( cfBrowser );
        
        CFArrayRef appURLList = LSCopyApplicationURLsForBundleIdentifier(cfBrowser, NULL);
        
        if (appURLList && CFArrayGetCount(appURLList) > 0)
        {
            CFURLRef appURL = (CFURLRef)CFArrayGetValueAtIndex(appURLList, 0 );
        
            if( !appURL )
            {
                Log( LEVEL_ERROR, "Cannot retrieve bundle ref for %s browser", browser.data());
                continue;
            }
        
            int lastPoint = browser.lastIndexOf(".");
            BrowserInfo browserInfo;
            browserInfo.Path = CFStringToQString( CFURLCopyPath( appURL ) );
            browserInfo.Path.replace("%20", " ");
            if( browserInfo.Path.endsWith("/"))
                browserInfo.Path.truncate( browserInfo.Path.length() - 1 );
            CFRelease( appURL );
        
            if( lastPoint != -1 )
            {
				QString strBrowserName = browser.right(browser.length() - lastPoint - 1);
				strBrowserName = strBrowserName.left(1).toUpper() + strBrowserName.mid(1);
                browserInfo.Name = strBrowserName;
            }
            else
            {
				QString strBrowserName = browser;
				strBrowserName = strBrowserName.left(1).toUpper() + strBrowserName.mid(1);
                browserInfo.Name = strBrowserName;
            }
            
            availableBrowsersList.append( browserInfo );
        }
	}
#endif
	return availableBrowsersList;
}

void Utils::Log( int level, const char *msg, ... )
{
	va_list args;
	char	str2[1024];
	time_t	c = time( NULL );
	struct tm* timeinfo;

	timeinfo = localtime ( &c );
	strftime( str2, 1024, "%x %X ", timeinfo );
	size_t l = strlen( str2 );
	va_start(args, msg);
	vsprintf( str2 + l, msg, args );
	va_end(args);

	if( level <= mLogLevel )
	{
		if( mLogFile.isEmpty() )
		{
			QString applicationPath = QApplication::applicationDirPath();
			mLogFile = applicationPath + "/" + "SoftProofingAgentLog.txt";
			QFile file( mLogFile );
			if( !file.exists() )
			{
				file.open( QIODevice::WriteOnly );
			}
		}
#if COZONE_WIN
		wchar_t* logFile;
		logFile = (wchar_t *)malloc(sizeof(wchar_t) * 1024);
		logFile = (wchar_t *)mLogFile.utf16();
		logFile[ mLogFile.size() ] = 0;
		FILE* f = _wfopen( logFile, L"a" );

		if( f )
		{
			fputs( str2, f );
			fputs( "\n", f );
			fclose( f );
		}
#else
		FILE* f = fopen( mLogFile.toUtf8(), "a" );
		if( f )
		{
			fputs( str2, f );
			fputs( "\n", f );
			fclose( f );
		}

#endif
	}
}

#if COZONE_MAC
QString Utils::CFStringToQString(CFStringRef str)
{
	if (!str)
		return QString();

	CFIndex length = CFStringGetLength(str);
	if (length == 0)
		return QString();

	QString string(length, Qt::Uninitialized);
	CFStringGetCharacters(str, CFRangeMake(0, length), reinterpret_cast<UniChar *>
		(const_cast<QChar *>(string.unicode())));
	return string;
}
#endif

QString Utils::GetComputerNetworkName()
{
#if COZONE_WIN
	char hostname[16];
	DWORD lenght = 16;
	GetComputerNameA( hostname, &lenght );
#else
	char hostname[257];
	::gethostname( hostname, 256 );
#endif

	return QString::fromLatin1( hostname );
}

QString Utils::GetInfoUser()
{
#if COZONE_WIN
	DWORD	nameLen = 256;
	unsigned short	userStr[256];
	::GetUserNameW( userStr, &nameLen );
	return QString::fromUtf16( userStr );
#else
	CFStringRef	userStr = ::CSCopyUserName( false );
	if( userStr == NULL )
		userStr = ::CSCopyUserName( true );
    QString userName = QString();
	if( userStr )
	{
        userName = Utils::CFStringToQString(userStr);
		::CFRelease( userStr );
	}
    return userName;
#endif
}

void Utils::DeleteFolderRecursivly( QString folderPath )
{
	QDir dvd_dir(folderPath);

	//First delete any files in the current directory
	QFileInfoList files = dvd_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files);
	for(int file = 0; file < files.count(); file++)
	{
		dvd_dir.remove(files.at(file).fileName());
	}

	//Now recursively delete any child directories
	QFileInfoList dirs = dvd_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs);
	for(int dir = 0; dir < dirs.count(); dir++)
	{
		DeleteFolderRecursivly(dirs.at(dir).absoluteFilePath ());
	}

	//Finally, remove empty parent directory
	dvd_dir.rmdir(dvd_dir.path());
}

QString Utils::GetTimeZoneOffset( QDateTime date)
{
	QDateTime utcDate = date.toUTC();
	QString test = utcDate.toString();
	date.setTimeSpec( Qt::UTC );

	int offset = utcDate.secsTo(date) / 3600;

	if (offset > 0)
		return QString("+%1").arg(offset);

	return QString("%1").arg(offset);
}

QString Utils::GenerateMachineID()
{
	//workaround to remove curly brackets
	QString guid = QString::fromLatin1(QUuid::createUuid().toRfc4122().toHex().data());
	return guid;
}
