#include "Settings.h"
#include "Utils.h"
#include "qcoreapplication.h"

const char* const kCurrentVersion	= "2.1.0.33";

const char* const kAppVersion		= "applicationVersion";
const char* const kAccountName		= "accountName";
const char* const kDataLocation		= "dataLocation";
const char* const kBrowserName		= "selectedBrowser";
const char* const kDisplayName		= "selectedDisplay";
const char* const kCalibrationName  = "selectedCalibration";
const char* const kDomainURL		= "selectedDomainURL";

const char* const kUserName			= "userName";
const char* const kCoZoneUserKey	= "CoZoneUserKey";
const char* const kUserPassword		= "userPassword";
const char* const kBrowserPath		= "selectedBrowserPath";
const char* const kProfileLocation  = "profileLocation";
const char* const kPreferences		= "Preferences";
const char* const kLastSuccessfulUpload = "lastSuccessfulUpload";
const char* const kDisplayIndex		= "selectedDisplayIndex";
const char* const kMachineID        = "machineID";
const char* const kDisplayWidth     = "displayWidth";
const char* const kDisplayHeight    = "displayHeight";

Settings::Settings( const QString& organization, const QString& applicationName ) :
mAccountName( "" ),
mDataLocation( "" ),
mBrowserName( "" ),
mCalibrationName( "" ),
mDisplayName( "" ),
mLastSuccessfulUpload( 0 ),
mMachineID( "" )
{
	mSettings = new QSettings( organization, applicationName );
}

void Settings::SaveSettings()
{
	if( mSettings == NULL )
		return;

	mSettings->beginGroup( kPreferences );

	//Save user preferences using qt platform specific methods Ex : registry for WIN
	mSettings->setValue( kAppVersion, kCurrentVersion );
	mSettings->setValue( kBrowserName, mBrowserName );
	mSettings->setValue( kCalibrationName, mCalibrationName );
	mSettings->setValue( kDataLocation, mDataLocation );
	mSettings->setValue( kAccountName, mAccountName );
	mSettings->setValue( kDisplayName, mDisplayName );
	mSettings->setValue( kCoZoneUserKey, mCoZoneUserKey );
	mSettings->setValue( kUserName, mCoZoneUserName );
	mSettings->setValue( kUserPassword, mCoZoneUserPassword );
	mSettings->setValue( kBrowserPath, mBrowserPath );
	mSettings->setValue( kProfileLocation, mProfileLocation );
	mSettings->setValue( kLastSuccessfulUpload, mLastSuccessfulUpload );
	mSettings->setValue( kDisplayIndex, mDisplayIndex );
	mSettings->setValue( kDisplayWidth, mDisplayWidth );
	mSettings->setValue( kDisplayHeight, mDisplayHeight );
	mSettings->setValue( kDomainURL, mDomainURL );

	mSettings->endGroup();
}

void Settings::LoadSettings(bool& isFirstRun)
{
	isFirstRun = false;
	if( mSettings->status() != QSettings::NoError )
		return;

	//Load user preferences using qt platform specific methods Ex: registry for WIN
	mSettings->beginGroup( kPreferences );

	mAppVersion	= mSettings->value( kAppVersion ).toString();
	if ( mAppVersion != kCurrentVersion )
	{
		mSettings->clear();
		isFirstRun = true;
	}
	mSettings->setValue(kAppVersion, kCurrentVersion);

	mBrowserName		= mSettings->value( kBrowserName ).toString();
	mCalibrationName	= mSettings->value( kCalibrationName ).toString();
	mDisplayName		= mSettings->value( kDisplayName ).toString();
	mDataLocation		= mSettings->value( kDataLocation ).toString();
	mAccountName		= mSettings->value( kAccountName ).toString();
	mCoZoneUserKey		= mSettings->value( kCoZoneUserKey ).toString();
	mCoZoneUserName		= mSettings->value( kUserName ).toString();
	mCoZoneUserPassword = mSettings->value( kUserPassword ).toString();
	mBrowserPath		= mSettings->value( kBrowserPath ).toString();
	mProfileLocation	= mSettings->value( kProfileLocation ).toString();
	mLastSuccessfulUpload = mSettings->value( kLastSuccessfulUpload ).toLongLong();
	mDisplayIndex		= mSettings->value( kDisplayIndex ).toInt();
	mMachineID			= mSettings->value( kMachineID ).toString();
	mDisplayWidth		= mSettings->value( kDisplayWidth ).toInt();
	mDisplayHeight		= mSettings->value( kDisplayHeight ).toInt();
	mDomainURL			= mSettings->value( kDomainURL ).toString();

	mSettings->endGroup();
}

void Settings::SetLastSuccessfulUpload( qint64 date )
{
	mLastSuccessfulUpload = date;
	mSettings->beginGroup( kPreferences );

	mSettings->setValue( kLastSuccessfulUpload, mLastSuccessfulUpload );

	mSettings->endGroup();
}

qint64 Settings::GetLastSuccessfulUpload() const
{
	return mLastSuccessfulUpload;
}

void Settings::SetCoZoneUserKey( const QString& userKey )
{
	mCoZoneUserKey = userKey;

	mSettings->beginGroup( kPreferences );
	mSettings->setValue( kCoZoneUserKey, mCoZoneUserKey );
	mSettings->endGroup();
}

QString Settings::GetCoZoneUserKey() const
{
	return mCoZoneUserKey;
}

void Settings::SetMachineID( const QString& machineID )
{
	mMachineID = machineID;

	mSettings->beginGroup( kPreferences );
	mSettings->setValue( kMachineID, mMachineID );
	mSettings->endGroup();
}

Settings::~Settings()
{
	delete mSettings;
}
