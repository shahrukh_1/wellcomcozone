#include "OtherCalibrationTool.h"
#include "../../../3rdParty/QJson/qjson.h"
#include "../../../3rdParty/QJson/serializer.h"
#include <QtCore/QFile>
#include <QtXml/QDomDocument>
#include <QtWidgets/QApplication>
#include <QtCore//QDir>
#include "../Utils.h"
#include "../AppConfig.h"
#include "../Zipper.h"
#include "../Settings.h"

const char* const kArchiveName = "CalibrationData.zip";
const char* OtherCalibrationTool::TOOLNAME = "Other";

OtherCalibrationTool::OtherCalibrationTool( const QString& workingFolder ) :
CalibrationToolBase(),
mWorkingFolder ( workingFolder )
{
	mToolName = QObject::tr( TOOLNAME );

	SetDefaultFolderLocation();
}

void OtherCalibrationTool::SetDefaultFolderLocation()
{
	mFolderLocation = mWorkingFolder;
}

QByteArray OtherCalibrationTool::GetSubmitDataRequest( const QString& userKey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID )
{
	QByteArray jsonData;
	Serializer jsonSerializer;
	QVariantMap message = CreateSubmitMessage( userKey, profileLocation, displayIndex, displayName, machineID );
	
	jsonData = jsonSerializer.serialize( message );
	return jsonData;
}

QVariantMap OtherCalibrationTool::CreateSubmitMessage( const QString& userkey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID )
{
	QString profileName;
	QVariantMap message = CalibrationToolBase::CreateSubmitMessage( userkey, displayIndex, displayName, machineID, TOOLNAME );
	message.insert( "FileData", CreateCalibrationDataPackage( profileLocation, profileName ) );
	message.insert( "CalibrationDateTimeUTC", GetCalibrationTimeUTC( profileLocation ) );
	message.insert( "DisplayProfileName", profileName );

	return message;
}

QByteArray OtherCalibrationTool::CreateCalibrationDataPackage( const QString& profileLocation, QString& profileName )
{
	QByteArray data;
	try
	{
		QString uploadFolderPath = mWorkingFolder + "/UploadData";
		QDir uploadDir (uploadFolderPath );
		if ( uploadDir.exists() )
		{
			Utils::DeleteFolderRecursivly( uploadFolderPath );
		}

		QDir().mkpath( uploadFolderPath );
		QFile profileFile ( profileLocation );
		QFileInfo profileInfo(profileLocation);
		profileName = profileInfo.fileName();
		QDir profileDir ( uploadFolderPath + "/DisplayProfile/Profile" );

		if( !profileDir.exists() )
		{
			profileDir.mkpath(  uploadFolderPath + "/DisplayProfile/Profile" );
		}
		if( profileFile.exists() )
		{
			QFile::copy( profileLocation, uploadFolderPath + "/DisplayProfile/Profile/" + profileName );
		}

		profileFile.close();

		Zipper zipper;
		zipper.OpenZip( uploadFolderPath + "/" + kArchiveName, uploadFolderPath + "/DisplayProfile" );
		zipper.AddFolderToZip( uploadFolderPath + "/DisplayProfile" );
		zipper.CloseZip();

		QFile archiveFile ( uploadFolderPath + "/" + kArchiveName );

		if( archiveFile.open( QFile::ReadOnly ) )
		{
			data = archiveFile.readAll().toBase64();
			archiveFile.close();
		}
		else
		{
			Utils::Log( LEVEL_ERROR, "Archive File Not found" );
		}

		if ( uploadDir.exists() )
		{
			Utils::DeleteFolderRecursivly( uploadFolderPath );
		}
	}
	catch (std::exception e)
	{
		Utils::Log( LEVEL_ERROR, ("CreateCalibrationDataPackage method failed with error" + QString::fromLatin1( e.what() ) ).toUtf8().constData() );
	}

	return  data;
	
}

QString OtherCalibrationTool::GetCalibrationTimeUTC( const QString& profileLocation )
{
	QFileInfo profileICC( profileLocation );

	if( profileICC.exists() )
	{
		return profileICC.lastModified().toUTC().toString( Qt::ISODate );
	}
	else
		return QString();
}

bool OtherCalibrationTool::IsUploadDataRequired( QString& errorMessage ) const
{
	QFileInfo profileFile ( Settings::getInstance().GetProfileLocation() );
	if( profileFile.exists() )
	{
		if(profileFile.lastModified().toMSecsSinceEpoch() > Settings::getInstance().GetLastSuccessfulUpload())
		{
			return true;
		}
	}

	return false;
}