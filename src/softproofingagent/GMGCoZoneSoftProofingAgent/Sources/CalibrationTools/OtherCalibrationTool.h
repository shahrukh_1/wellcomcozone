#ifndef GMGCOZONESOFTPROOFINGAGENT_OTHERCALIBRATIONTOOL_H
#define GMGCOZONESOFTPROOFINGAGENT_OTHERCALIBRATIONTOOL_H

#include <QtCore/QByteArray>
#include "CalibrationToolBase.h"
#include <QtCore/QString>

class OtherCalibrationTool : public CalibrationToolBase
{
	QString				mWorkingFolder;
	QByteArray			CreateCalibrationDataPackage( const QString& profileLocation, QString& profileName );

	QVariantMap			CreateSubmitMessage( const QString& userkey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID );
public:
	static const char*  TOOLNAME;

						OtherCalibrationTool( const QString& workingFolder = "" );
	virtual QByteArray	GetSubmitDataRequest( const QString& userkey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID );
	QString				GetCalibrationTimeUTC( const QString& profileLocation );
	void				SetDefaultFolderLocation();
	bool				IsUploadDataRequired( QString& errorMessage ) const;
	
};



#endif