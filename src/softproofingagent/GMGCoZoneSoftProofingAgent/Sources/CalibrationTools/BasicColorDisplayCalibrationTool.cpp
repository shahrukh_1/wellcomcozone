#include "BasicColorDisplayCalibrationTool.h"
#include "../../../3rdParty/QJson/qjson.h"
#include "../../../3rdParty/QJson/serializer.h"
#include <QtCore/QFile>
#include <QtXml/QDomDocument>
#include <QtWidgets/QApplication>
#include <QtCore//QDir>
#include "../Utils.h"
#include "../AppConfig.h"
#include "../Zipper.h"
#include "../Settings.h"

const char* const kArchiveName = "CalibrationData.zip";
const char* BasicColorDisplayCalibrationTool::TOOLNAME = "BasICColor Display";

BasicColorDisplayCalibrationTool::BasicColorDisplayCalibrationTool( const QString& workingFolder ) :
CalibrationToolBase(),
mWorkingFolder ( workingFolder )
{
	mToolName = QObject::tr( TOOLNAME );

	SetDefaultFolderLocation();
}

void BasicColorDisplayCalibrationTool::SetDefaultFolderLocation()
{
#if COZONE_WIN
	mFolderLocation = "C:/Users/" + Utils::GetInfoUser() + "/Documents/basICColor display 5/basICColor Jobs/Jobs display 5";
#else
	mFolderLocation = "/Users/" + Utils::GetInfoUser() + "/Documents/basICColor display 5/basICColor Jobs/Jobs display 5";
#endif
	if( !QDir( mFolderLocation ).exists() )
	{
		mFolderLocation = "";
	}
}

QByteArray BasicColorDisplayCalibrationTool::GetSubmitDataRequest( const QString& userKey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID )
{
	QByteArray jsonData;
	Serializer jsonSerializer;
	QVariantMap message = CreateSubmitMessage( userKey, profileLocation, displayIndex, displayName, machineID );
	
	jsonData = jsonSerializer.serialize( message );
	return jsonData;
}

QVariantMap BasicColorDisplayCalibrationTool::CreateSubmitMessage( const QString& userkey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID )
{
	QString profileName;
	QVariantMap message = CalibrationToolBase::CreateSubmitMessage( userkey, displayIndex, displayName, machineID, TOOLNAME );
	message.insert( "FileData", CreateCalibrationDataPackage( profileLocation, profileName ) );
	message.insert( "CalibrationDateTimeUTC", GetCalibrationTimeUTC() );
	message.insert( "DisplayProfileName", profileName );

	return message;
}

QByteArray BasicColorDisplayCalibrationTool::CreateCalibrationDataPackage( const QString& profileLocation, QString& profileName )
{
	QByteArray data;
	try
	{
		QString uploadFolderPath = mWorkingFolder + "/UploadData";
		QDir uploadDir (uploadFolderPath );
		if ( uploadDir.exists() )
		{
			Utils::DeleteFolderRecursivly( uploadFolderPath );
		}

		QDir().mkpath( uploadFolderPath );
		QFile profileFile ( profileLocation );
		QFileInfo profileInfo(profileLocation);
		profileName = profileInfo.fileName();
		QDir profileDir ( uploadFolderPath + "/DisplayProfile/Profile" );

		if( !profileDir.exists() )
		{
			profileDir.mkpath(  uploadFolderPath + "/DisplayProfile/Profile" );
		}
		if( profileFile.exists() )
		{
			QFile::copy( profileLocation, uploadFolderPath + "/DisplayProfile/Profile/" + profileName );
		}

		profileFile.close();

		Zipper zipper;
		zipper.OpenZip( uploadFolderPath + "/" + kArchiveName, mFolderLocation );
		zipper.AddFolderToZip( mFolderLocation );
		zipper.OpenZip( uploadFolderPath + "/" + kArchiveName, uploadFolderPath + "/DisplayProfile", true );
		zipper.AddFolderToZip( uploadFolderPath + "/DisplayProfile" );
		zipper.CloseZip();

		QFile archiveFile ( uploadFolderPath + "/" + kArchiveName );

		if( archiveFile.open( QFile::ReadOnly ) )
		{
			data = archiveFile.readAll().toBase64();
			archiveFile.close();
		}
		else
		{
			Utils::Log( LEVEL_ERROR, "Archive File Not found" );
		}

		if ( uploadDir.exists() )
		{
			Utils::DeleteFolderRecursivly( uploadFolderPath );
		}
	}
	catch (std::exception e)
	{
		Utils::Log( LEVEL_ERROR, ("CreateCalibrationDataPackage method failed with error" + QString::fromLatin1( e.what() ) ).toUtf8().constData() );
	}

	return  data;
	
}

QString BasicColorDisplayCalibrationTool::GetCalibrationTimeUTC()
{
	QString dateInUTC;

	QString filePath = mFolderLocation + "/" + kBasICColorXMLFile;
	QFile calibrationFile( filePath );

	if( !calibrationFile.open( QFile::ReadOnly ) )
	{
		Utils::Log( LEVEL_INFO, ("GetCalibrationTimeZoneOffset() Error : File " + filePath + "not found").toUtf8().constData() );
		return dateInUTC;
	}

	QDomDocument xmlDoc;
	QDomElement root;

	QString errorMessage;
	if( !xmlDoc.setContent( calibrationFile.readAll(), &errorMessage ) )
	{
		Utils::Log( LEVEL_ERROR, ("GetCalibrationTimeZoneOffset() Error: Could not open basic color xml for reading error: " + errorMessage).toUtf8().constData() );
		return dateInUTC;
	}

	root = xmlDoc.documentElement();

	QDomNode firstDisplay = root.firstChild();

	if( firstDisplay.isNull() )
	{
		Utils::Log( LEVEL_INFO, ("GetCalibrationTimeZoneOffset() Error: Display node not found in xml") );
		return dateInUTC;
	}

	QDomNodeList dateNode = firstDisplay.toElement().elementsByTagName("Date");

	if( dateNode.count() == 0 )
	{
		Utils::Log( LEVEL_INFO, ("GetCalibrationTimeZoneOffset() Error: Date node not found in xml") );
		return dateInUTC;
	}

	QString dates = dateNode.at( 0 ).toElement().text();
	QDateTime date = QDateTime::fromString( dates, Qt::ISODate );

	if( date.toString().isEmpty() )
	{
		Utils::Log(LEVEL_ERROR, "GetCalibrationTimeZoneOffset() convertind date node to QdateTime failed ");
	}
	
	dateInUTC = date.toUTC().toString( Qt::ISODate );

	return dateInUTC;
}

bool BasicColorDisplayCalibrationTool::IsUploadDataRequired( QString& errorMessage ) const
{
	if( !Settings::getInstance().GetDataLocation().isEmpty() )
	{
		QFileInfo basicColorXML( Settings::getInstance().GetDataLocation() + "/" + kBasICColorXMLFile );

		if( basicColorXML.exists() )
		{
			if(basicColorXML.lastModified().toMSecsSinceEpoch() > Settings::getInstance().GetLastSuccessfulUpload() )
			{
				return true;
			}
		}
		else 
		{
			errorMessage = QObject::tr("Upload Data request failed: BasicColorXML not found in folderLocation");
		}
	}

	return false;
}