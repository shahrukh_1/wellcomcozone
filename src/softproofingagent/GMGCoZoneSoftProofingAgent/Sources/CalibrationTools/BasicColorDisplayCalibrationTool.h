#ifndef GMGCOZONESOFTPROOFINGAGENT_BASICCOLORDISPLAYCALIBRATIONTOOL_H
#define GMGCOZONESOFTPROOFINGAGENT_BASICCOLORDISPLAYCALIBRATIONTOOL_H

#include <QtCore/QByteArray>
#include "CalibrationToolBase.h"
#include <QtCore/QString>

class BasicColorDisplayCalibrationTool : public CalibrationToolBase
{
	QString				mWorkingFolder;
	QByteArray			CreateCalibrationDataPackage( const QString& profileLocation, QString& profileName );
	QVariantMap			CreateSubmitMessage( const QString& userkey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID );
public:
	static const char*  TOOLNAME;

						BasicColorDisplayCalibrationTool( const QString& workingFolder = "" );
	virtual QByteArray	GetSubmitDataRequest( const QString& userkey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID );
	QString				GetCalibrationTimeUTC();
	void				SetDefaultFolderLocation();
	bool				IsUploadDataRequired( QString& errorMessage ) const;
	
};



#endif