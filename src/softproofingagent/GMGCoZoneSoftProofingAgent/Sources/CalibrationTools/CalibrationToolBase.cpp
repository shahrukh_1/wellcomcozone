#include "CalibrationToolBase.h"
#include "../Utils.h"
#include "../AppConfig.h"
#include "../Settings.h"
#include <QApplication>
#include <QDesktopWidget>

const char* const kArchiveName = "CalibrationData.zip";

QVariantMap CalibrationToolBase::CreateSubmitMessage( const QString& userKey, int displayIndex, const QString& displayName, const QString& machineID, const char* toolName )
{
	QVariantMap message;

	message.insert( "token", AppConfig::getInstance().GetRestApiSecurityToken() );
	message.insert( "MachineId", machineID );
	message.insert( "AuthKey", userKey );
	message.insert( "CalibrationSoftware", toolName );
	message.insert( "WorkstationName", Utils::GetComputerNetworkName() );
	message.insert( "DisplayIdentifier", displayIndex );
	message.insert( "DisplayName", displayName );
	message.insert( "FileName", kArchiveName );
	message.insert( "DisplayWidth", Settings::getInstance().GetDisplayWidth() );
	message.insert( "DisplayHeight", Settings::getInstance().GetDisplayHeight() );

	return message;
}