#ifndef GMGCOZONESOFTPROOFINGAGENT_CALIBRATIONTOOLBASE_H
#define GMGCOZONESOFTPROOFINGAGENT_CALIBRATIONTOOLBASE_H

#include <QtCore/QByteArray>
#include <QtCore/QString>
#include <QtCore/QVariant>

class CalibrationToolBase {
protected:
	QString				mFolderLocation;
	QString				mToolName;

	virtual QVariantMap	CreateSubmitMessage( const QString& userKey, int displayIndex, const QString& displayName, const QString& machineID, const char* toolName );
public:
	QString				GetToolName() const;
	void				SetFolderLocation( const QString& folderLocation );
	QString				GetOutputFolderLocation() const;
	virtual QByteArray	GetSubmitDataRequest( const QString& userKey, const QString& profileLocation, int displayIndex, const QString& displayName, const QString& machineID ) = 0;
	virtual void		SetDefaultFolderLocation() = 0;
	virtual bool		IsUploadDataRequired( QString& errorMessage ) const = 0;
    virtual             ~CalibrationToolBase();
};

inline QString CalibrationToolBase::GetToolName() const
{
	return mToolName;
}

inline void CalibrationToolBase::SetFolderLocation( const QString& folderLocation )
{
	mFolderLocation = folderLocation;
}

inline QString CalibrationToolBase::GetOutputFolderLocation() const
{
	return mFolderLocation;
}

inline CalibrationToolBase::~CalibrationToolBase()
{
    
}
#endif