#ifndef GMGCOZONESOFTPROOFINGAGENT_SOFTPROOFINGWEBCLIENT_H
#define GMGCOZONESOFTPROOFINGAGENT_SOFTPROOFINGWEBCLIENT_H

#include <QtNetwork/QNetworkAccessManager>

class QNetworkReply;

class SoftProofingWebClient : public QNetworkAccessManager {
	
	Q_OBJECT
	QNetworkReply*	mReplyObject;
public:
	static SoftProofingWebClient& getInstance()
	{
		static SoftProofingWebClient instance;
		return instance;
	}
					SoftProofingWebClient( QObject * parent = 0 );
	void			VerifyAccount( QString accountDomain, QString userName, QString userPassword );
	void			UploadData( QString serviceUrl, QByteArray postData );
	void			AbortConnection();
	QNetworkReply*	GetReplyObject() const;
};

inline QNetworkReply* SoftProofingWebClient::GetReplyObject() const
{
	return mReplyObject;
}
#endif