#ifndef GMGCOZONESOFTPROOFINGAGENT_BROWSERINFO_H
#define GMGCOZONESOFTPROOFINGAGENT_BROWSERINFO_H

#include <QString>

class BrowserInfo
{
public:
	QString			Name;
	QString			Path;
};

#endif