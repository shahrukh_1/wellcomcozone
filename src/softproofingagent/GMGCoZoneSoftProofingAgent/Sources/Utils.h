#ifndef GMGCOZONESOFTPROOFINGAGENT_UTILS_H
#define GMGCOZONESOFTPROOFINGAGENT_UTILS_H

#include <QHash>
#include <QStringList>
#include <QDateTime>
#include "Globals.h"
#include "BrowserInfo.h"
#include "MonitorInfo.h"

#if COZONE_MAC
#include <ApplicationServices/ApplicationServices.h>
#endif

class Utils
{
	static int				mLogLevel;
	static QString			mLogFile;
public:
	static QList<MonitorInfo>			GetAvailableDisplays();
	static QList<BrowserInfo>			GetAvailableBrowsers();



	static void							Log( int level, const char *msg, ... );
	static void							SetLogLevel ( int level );
	static QString						GetComputerNetworkName();
	static QString						GetInfoUser();
	static void							DeleteFolderRecursivly( QString folderPath );
	static QString						GetTimeZoneOffset( QDateTime date );
	static QString						GenerateMachineID();

#if COZONE_MAC
	static QString                      CFStringToQString( CFStringRef str );
    static QString                      CFScreenNameForDisplay( CGDirectDisplayID displayID );
    static void                         AddTrayToStartup( const char* appPath );
    static void                         RemoveTrayFromStartup();
    static bool                         IsLoginItemSet();
    static const char*                  GetBundlePath();
	static const char*                  GetProfilePath(CGDirectDisplayID id);
#else
	static QString						GetGDIDeviceNameFromSource( LUID adapterId, UINT32 sourceId );
	static QString						GetMonitorDevicePathFromTarget( LUID adapterId, UINT32 sourceId );
	static QString						GetFriendlyNameFromTarget( LUID adapterId, UINT32 sourceId );
	static bool							IsMicrosoftEdgeInstalled();
#endif
};

inline void Utils::SetLogLevel( int logLevel )
{
	Utils::mLogLevel = logLevel;
}

#endif
