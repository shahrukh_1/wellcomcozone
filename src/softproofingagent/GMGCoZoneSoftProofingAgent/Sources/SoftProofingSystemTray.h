#ifndef GMGCOZONESOFTPROOFINGAGENT_SOFTPROOFINGSYSTEMTRAY_H
#define GMGCOZONESOFTPROOFINGAGENT_SOFTPROOFINGSYSTEMTRAY_H

#include <QSystemTrayIcon>
#include <QMessageBox>
#include "Globals.h"
#include <QtNetwork/QNetworkReply>
#include "./CalibrationTools/CalibrationToolBase.h"
#include "../Forms/UploadDataProgress.h"
#include "../Forms/Preferences.h"

class QMenu;
class QAction;

class SoftProofingSystemTray : public QSystemTrayIcon
{
	Q_OBJECT;
	bool								mIsStartSession;

private:
	QMenu*								mTrayIconMenu;
	QAction*							mStartSessionAction;
	QAction*							mPreferencesAction;
	QAction*							mSyncCoZoneAction;
	QAction*							mOpenAtLoginAction;
	QAction*							mQuitAction;
	QTimer*								mRequestsTimeoutTimer;
	QString								mTemporaryFolderPath;
	QList<CalibrationToolBase*>			mCalibrationTools;
	UploadDataProgress*					mProgressDialog;

	Preferences*						mPreferencesDialog;

	void								UploadData( bool verifyAccount );
	void								FillControls();
	void								SendCalibrationDataRequest();
	void								ResetProgressDialog();
	bool								CheckCalibrationChanges();
	void								VerifyAccount();
	CalibrationToolBase*				GetSelectedCalibrationTool();
	int									ShowMessageBox( QString title, QString message = "", QMessageBox::StandardButtons buttons = QMessageBox::Ok );
	void								LaunchBrowser();

private slots:
	void								IconActivated( QSystemTrayIcon::ActivationReason reason );
	void								ShowPreferencesDialog();
	void								StartSession();
	void								QuitApplication();
	void								ReplyReceived( QNetworkReply* );
	void								NetworkTimeout();
	void								CloseProgressDialog();
	void								ClosePreferencesWindow();
	void								OKButtonHandler();
	void								StartSessionButtonHandler();
	void								OpenAtLogin();
	void								SetStartAtLogin();

public:
										SoftProofingSystemTray( QObject* parent = 0 );
										~SoftProofingSystemTray();
	void								SetIsStartSession(bool firstUpload);
	bool								GetIsStartSession() const;
};

inline void	SoftProofingSystemTray::SetIsStartSession(bool isStartSession)
{
	mIsStartSession = isStartSession;
}

inline bool SoftProofingSystemTray::GetIsStartSession() const
{
	return mIsStartSession;
}

#endif