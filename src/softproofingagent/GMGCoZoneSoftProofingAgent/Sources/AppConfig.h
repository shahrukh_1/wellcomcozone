#ifndef GMGCOZONESOFTPROOFINGAGENT_APPCONFIG_H
#define GMGCOZONESOFTPROOFINGAGENT_APPCONFIG_H
#include <QtCore//QStringListModel>


class AppConfig
{
public:
	static AppConfig& getInstance()
	{
		static AppConfig instance;
		return instance;
	}
	void		LoadSettings();
	QString		GetRestApiSecurityToken() const;
	QString		GetServiceProtocol() const;
	QString		GetVerifyAccountURL() const;
	QString		GetUploadDataURL() const;
	QString		GetInitSessionURL() const;
	QString		GetLanguage() const;
	QString		GetEnvironmentDomain() const;
	int			GetLogLevel() const;

private:
	AppConfig() {};
	AppConfig(AppConfig const&);
	void operator=(AppConfig const&);
	
	QString		mSecurityToken;
	QString		mServiceProtocol;
	QString		mVerifyAccountURL;
	QString		mUploadDataURL;
	QString		mInitSessionURL;
	QString		mEnvironmentDomain;
	int			mLogLevel;
	QString		mLanguage;
};

inline QString AppConfig::GetEnvironmentDomain() const
{
	return mEnvironmentDomain;
}

inline QString AppConfig::GetServiceProtocol() const
{
	return mServiceProtocol;
}

inline QString AppConfig::GetRestApiSecurityToken() const
{
	return mSecurityToken;
}

inline QString AppConfig::GetVerifyAccountURL() const
{
	return mVerifyAccountURL;
}

inline QString AppConfig::GetUploadDataURL() const
{
	return mUploadDataURL;
}

inline int AppConfig::GetLogLevel() const 
{
	return mLogLevel;
}

inline QString AppConfig::GetInitSessionURL() const
{
	return mInitSessionURL;
}

inline QString AppConfig::GetLanguage() const
{
	return mLanguage;
}

#endif

