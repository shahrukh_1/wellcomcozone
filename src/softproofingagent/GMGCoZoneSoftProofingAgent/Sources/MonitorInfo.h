#ifndef GMGCOZONESOFTPROOFINGAGENT_MONITORINFO_H
#define GMGCOZONESOFTPROOFINGAGENT_MONITORINFO_H

#include <QString>

class MonitorInfo
{
public:
	QString			Name;
	QString			ProfilePath;
	int				Height;
	int				Width;
};

#endif