#include "SoftProofingWebClient.h"
#include <QtNetwork/QNetworkReply>
#include "../3rdParty/QJson/qjson.h"
#include "../3rdParty/QJson/serializer.h"
#include "../Forms/Preferences.h"
#include "AppConfig.h"
#include "Utils.h"

SoftProofingWebClient::SoftProofingWebClient( QObject * parent ) :
QNetworkAccessManager( parent )
{
	
}

void SoftProofingWebClient::VerifyAccount( QString accountDomain, QString userName, QString userPassword )
{
	Serializer jsonSerializer;
	QVariantMap message;
	message.insert( "Username", userName );
	message.insert( "Password", userPassword );
	message.insert( "AccountUrl", accountDomain );
	message.insert( "token", AppConfig::getInstance().GetRestApiSecurityToken() );

	QByteArray jsonData = jsonSerializer.serialize( message );
	QNetworkRequest request( QUrl(AppConfig::getInstance().GetServiceProtocol() + "://" + accountDomain + AppConfig::getInstance().GetVerifyAccountURL() ) );

	request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
	request.setHeader( QNetworkRequest::ContentLengthHeader, jsonData.length() );

	mReplyObject = post( request, jsonData );
    mReplyObject->ignoreSslErrors();
}

void SoftProofingWebClient::UploadData( QString accountDomain, QByteArray postData )
{
	QNetworkRequest request( QUrl( AppConfig::getInstance().GetServiceProtocol() + "://" + accountDomain + AppConfig::getInstance().GetUploadDataURL() ) );

	request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
	request.setHeader( QNetworkRequest::ContentLengthHeader, postData.length() );

	mReplyObject = post( request, postData );
    mReplyObject->ignoreSslErrors();
}

void SoftProofingWebClient::AbortConnection()
{
	mReplyObject->abort();
}

