#ifndef GMGCOZONESOFTPROOFINGAGENT_GLOBALS_H
#define GMGCOZONESOFTPROOFINGAGENT_GLOBALS_H

#define APP_ORGANIZATION "GMG"
#define APP_NAME "CoZone SoftProofing Agent"
#define WINDOWS_STARTUP_REGISTRY "HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"
#define WINDOWS_STARTUP_APPROVED_ENTIRE_REGISTRY "HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartupApproved\\Run"
#define WINDOWS_STARTUP_APPROVED_REGISTRY "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartupApproved\\Run"

#define	LEVEL_BASE		0
#define	LEVEL_FATAL		1
#define	LEVEL_ERROR		2
#define	LEVEL_WARNING	3
#define	LEVEL_INFO		4
#define	LEVEL_DEBUG		5

const char* const kBasICColorXMLFile = "basICColor_display_status.xml";
const char* const kBasICColorLogFile = "basICColor_display_history.log";

#if defined( __GNUC__ )

#define COZONE_MAC 1
#define COZONE_WIN 0

#else

#define COZONE_MAC 0
#define COZONE_WIN 1
#include <Windows.h>

#endif


#endif