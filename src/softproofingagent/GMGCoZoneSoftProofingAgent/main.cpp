#include "Sources/SoftProofingSystemTray.h"
#include <QApplication>
#include <QSharedMemory>
#include <QString>
#include <QTranslator>
#include <QMessageBox>
#include "Sources/AppConfig.h"

int main(int argc, char *argv[])
{
	QSharedMemory sharedMemory;
	sharedMemory.setKey( APP_NAME );
	
	if( sharedMemory.attach() || !sharedMemory.create(1) )
	{
		return 0; // Exit already a process running 
	}
    
    Q_INIT_RESOURCE( Preferences );

    QApplication app( argc, argv );

    if ( !QSystemTrayIcon::isSystemTrayAvailable() )
    {
        QMessageBox::critical( 0, QObject::tr("GMGCoZoneSoftProofingAgent"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system.") );
        return 1;
    }

    AppConfig::getInstance().LoadSettings();

    QString language = AppConfig::getInstance().GetLanguage();

    QTranslator translator( 0 );

#if COZONE_WIN
    QString resPath = QCoreApplication::applicationDirPath() + "/Resources/Languages/";
#else
    QString resPath = QCoreApplication::applicationDirPath() + "/../Resources/Languages/";
#endif

    if(translator.load( QString::fromLatin1("gmgcozonesoftproofingagent_") + language, resPath) )
        app.installTranslator( &translator );

    QApplication::setQuitOnLastWindowClosed( false );

    SoftProofingSystemTray trayApp;
    trayApp.show();

    //qt application main loop
    int returnCode =  app.exec();

    //cleanup code after quitiing app
    if( sharedMemory.isAttached() )
    {
        sharedMemory.detach();
    }

    return returnCode;
    
}
