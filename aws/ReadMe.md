# Steps to deploy CoZone on AWS environment: 
1. Manually create: Key Pairs, AMIs(Web, MediaDefine, Email Sender, Services), ​S3 Buckets, IAM Roles.
2. Update parameters values of CloudFormation Template​.
3. Created a CloudFormation stack with template "CloudFormation Template.txt" (ATTENTION TO THESE OPTIONS: "Terminate instances before delete stack", "Rollback on failure").
4. Manually create the following resources:
- Load Balancers
- SNS Topics/SNS Subscriptions 
- SES Configuration - Simple Email Service is not available in Asia Pacific (Singapore) it should be configured manually in Ireland​ region.​
- RDS Instance 
- Route 53 hosted zone and records
- Security Groups (inbound/outbound policy)
4. Update autoscaling groups with the necessary number of instances.

**Note**: For Services and Email Sender servers there is no scaling policy defined.