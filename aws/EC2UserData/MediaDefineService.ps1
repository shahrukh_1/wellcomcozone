#TOFILL AWS credentials, region, bucketLocation, bucketArchiveName 
<powershell>
    Import-Module ServerManager
    import-module "C:\Program Files (x86)\AWS Tools\PowerShell\AWSPowerShell\AWSPowerShell.psd1"
    #set credentials for powershell-user
    Set-AWSCredentials -AccessKey AKIAJVPLUQNEPXRN7BPQ -SecretKey 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
    set-DefaultAWSRegion ap-southeast-1
	
    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Starting bootstrap'   
    
    $bucketLocation = "Production/"
    $bucketArchiveName = "MediaServer.zip"
    $serviceLocation =  "$env:ProgramFiles\GMGColor\GMG Media Define"
    mkdir $serviceLocation
	$serviceAssetsLocation =  "$env:systemdrive\CoZoneMediaServer.zip"
	
	#connect to s3 and download the website assets archive
    Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$bucketArchiveName -LocalFile $serviceAssetsLocation | out-file $env:temp/copy.log -Append

	#Unzip the website to destination folder
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    sz x $serviceAssetsLocation -o"C:\Program Files\GMGColor\GMG Media Define" -aoa
    #Delete the archive
    Remove-Item $serviceAssetsLocation
    
    #Install GMG Media Processor Service with InstallUtil
    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Install GMG Media Define Service'
    C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "C:\Program Files\GMGColor\GMG Media Define\GMGColorMediaDefine.exe" | out-file $env:temp\installservice.log -Append
	 
    #Start GMG Media Processor service
    function FuncCheckService {
        param($ServiceName)
        $arrService = Get-Service -Name $ServiceName
        Set-Service $ServiceName -startuptype "Automatic"
        if ($arrService.Status -ne "Running"){
            Start-Service $ServiceName
        }
    }
 
    $appConfig = ($serviceLocation + "\GMGColorMediaDefine.exe.config")
	$doc = (Get-Content $appConfig) -as [Xml]
	$configSetting = $doc.configuration.appSettings.add | where {$_.Key -eq 'ServiceName'}
	$serviceName = $configSetting.value

	#Install GMG Media Processor Service with InstallUtil
	Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Install GMG Media Define Service'
	C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "C:\Program Files\GMGColor\GMG Media Define\GMGColorMediaDefine.exe" | out-file $env:temp\installservice.log -Append

	FuncCheckService -ServiceName $serviceName

	#Start adding extra instances
	for ($i=1; $i -le 5; $i++)
	{
		$currentInstancePath = ($serviceLocation + $i.ToString())
		#Copy downloaded folder to create new instance of service
		Copy-item -Path $serviceLocation -Destination $currentInstancePath -Recurse -Force
		
		#Update configuration file to set serviceName unique for every instance
		$appConfig = ($currentInstancePath + "\GMGColorMediaDefine.exe.config")
		$doc = (Get-Content $appConfig) -as [Xml]
		$obj = $doc.configuration.appSettings.add | where {$_.Key -eq 'ServiceName'}
		$obj.value = ($serviceName + $i.ToString())
		$doc.Save($appConfig)
		
		#Install GMG Media Processor Service with InstallUtil
		Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message ('Install GMG Media Define Service' + $i.ToString())
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe ($currentInstancePath + "\GMGColorMediaDefine.exe") | out-file $env:temp\installservice.log -Append
		 
		FuncCheckService -ServiceName $obj.value
	}

    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Installation Completed'
</powershell>