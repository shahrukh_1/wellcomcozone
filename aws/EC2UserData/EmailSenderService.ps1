#TOFILL AWS credentials, region, public IP, bucketLocation 
<powershell>
    import-module "C:\Program Files (x86)\AWS Tools\PowerShell\AWSPowerShell\AWSPowerShell.psd1"
	
	#set credentials for powershell-user
	Set-AWSCredentials -AccessKey AKIAJVPLUQNEPXRN7BPQ -SecretKey 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
	set-DefaultAWSRegion ap-southeast-1

	#Instance ID captured through Instance meta data
	$webclient = new-object System.Net.WebClient 
	$instanceID = $webclient.DownloadString("http://169.254.169.254/latest/meta-data/instance-id")

	#associate predefined Elastic IP to the instance
	Register-EC2Address $instanceID -PublicIp 18.136.182.106
	
	$rootFolderPath = "C:\GMGServices\"
	$bucketLocation = "Production/"

	$emailSenderSrcName = "EmailSenderService.zip"

	#Copy the new builds from S3
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$emailSenderSrcName -LocalFile $rootFolderPath$emailSenderSrcName

	#Stop the services in case they are already installed
	$emailSenderServiceName = "CoZone EmailSender Service"

	if (Get-Service $emailSenderServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $emailSenderServiceName }

	if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) { throw "$env:ProgramFiles\7-Zip\7z.exe needed" }
	set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"

	#Unzip the services
	sz x $rootFolderPath$emailSenderSrcName -o"$rootFolderPath$emailSenderServiceName" -aoa

	#Install and start the services
	if (-NOT(Get-Service $emailSenderServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$emailSenderServiceName\GMG.CoZone.EmailSenderService.exe" | out-file $env:temp\installservice.log -Append
	}
	Set-Service $emailSenderServiceName -startuptype "Automatic"
	#Start the services
	Start-Service -displayname $emailSenderServiceName

</powershell>