#TOFILL AWS credentials, region, bucketLocation 
<powershell>
   
    import-module "C:\Program Files (x86)\AWS Tools\PowerShell\AWSPowerShell\AWSPowerShell.psd1"
	
	#set credentials for powershell-user
	Set-AWSCredentials -AccessKey AKIAJVPLUQNEPXRN7BPQ -SecretKey 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
	set-DefaultAWSRegion ap-southeast-1

	#Instance ID captured through Instance meta data
	$webclient = new-object System.Net.WebClient 
	$instanceID = $webclient.DownloadString("http://169.254.169.254/latest/meta-data/instance-id")
	
	#associate predefined Elastic IP to the instance
	Register-EC2Address $instanceID -PublicIp 52.74.243.242
	
	$rootFolderPath = "C:\GMGServices\"
	$bucketLocation = "Production/"

	$deliverSrcName = "DeliverService.zip"
	$emailSrcName = "EmailService.zip"
	$ftpLocalSrcName = "FTPService.zip"
    $ftpCustomProviderSrcName = "FTPCustomProvider.zip"
	$maintenanceLocalSrcName = "MaintenanceService.zip"
	$softproofingSrcName = "SoftProofingDataService.zip"
	$bouncedEmailTrackerSrcName = "BouncedEmailTrackerService.zip"
	$archiveCompletedJobsServiceSrcName = "ArchiveCompletedJobsService.zip"
	$notificationGenerationSrcName = "NotificationGenerationService.zip"

	if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) { throw "$env:ProgramFiles\7-Zip\7z.exe needed" }
	set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
	
	# Copy the new builds from S3
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$deliverSrcName -LocalFile $rootFolderPath$deliverSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$emailSrcName -LocalFile $rootFolderPath$emailSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$ftpLocalSrcName -LocalFile $rootFolderPath$ftpLocalSrcName
    Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$ftpCustomProviderSrcName -LocalFile $rootFolderPath$ftpCustomProviderSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$maintenanceLocalSrcName -LocalFile $rootFolderPath$maintenanceLocalSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$softproofingSrcName -LocalFile $rootFolderPath$softproofingSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$bouncedEmailTrackerSrcName -LocalFile $rootFolderPath$bouncedEmailTrackerSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$archiveCompletedJobsServiceSrcName -LocalFile $rootFolderPath$archiveCompletedJobsServiceSrcName
	Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key $bucketLocation$notificationGenerationSrcName -LocalFile $rootFolderPath$notificationGenerationSrcName
    
	$emailServiceName = "GMGCoZone Email Service"
	$ftpServiceName = "GMGCoZone FTP Service"
	$maintenanceServiceName = "GMGCoZone Maintenance Service"
	$deliverServiceName = "GMGCoZone Deliver"
	$softProofingDataServiceName ="GMGCoZone SoftProofingData"
	$bouncedEmailTrackerServiceName ="GMGCoZone BouncedEmailTracker Service"
	$archiveCompletedJobsServiceName ="GMGCoZone ArchiveCompletedJobs Service"
	$notificationGenerationServiceName ="GMGCoZone NotificationGeneration Service"

    # Stop the services in case they are already installed
	if (Get-Service $emailServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $emailServiceName }
	if (Get-Service $ftpServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $ftpServiceName }
	if (Get-Service $maintenanceServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $maintenanceServiceName }
	if (Get-Service $softProofingDataServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $softProofingDataServiceName }
	if (Get-Service $bouncedEmailTrackerServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $bouncedEmailTrackerServiceName }
	if (Get-Service $archiveCompletedJobsServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $archiveCompletedJobsServiceName }
	if (Get-Service $notificationGenerationServiceName -ErrorAction SilentlyContinue){ Stop-Service -displayname $notificationGenerationServiceName }

	# Unzip the services
	sz x $rootFolderPath$deliverSrcName -o"$rootFolderPath$deliverServiceName" -aoa
	sz x $rootFolderPath$emailSrcName -o"$rootFolderPath$emailServiceName" -aoa
	sz x $rootFolderPath$ftpLocalSrcName -o"$rootFolderPath$ftpServiceName" -aoa
	sz x $rootFolderPath$maintenanceLocalSrcName -o"$rootFolderPath$maintenanceServiceName" -aoa
	sz x $rootFolderPath$softproofingSrcName -o"$rootFolderPath$softProofingDataServiceName" -aoa
	sz x $rootFolderPath$bouncedEmailTrackerSrcName -o"$rootFolderPath$bouncedEmailTrackerServiceName" -aoa
	sz x $rootFolderPath$archiveCompletedJobsServiceSrcName -o"$rootFolderPath$archiveCompletedJobsServiceName" -aoa
	sz x $rootFolderPath$notificationGenerationSrcName -o"$rootFolderPath$notificationGenerationServiceName" -aoa
	
	
	# Install and start the services

	if (-NOT(Get-Service $emailServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$emailServiceName\GMG.CoZone.EmailService.exe" | out-file $env:temp\installservice.log -Append
	}
	if (-NOT(Get-Service $ftpServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$ftpServiceName\GMG.CoZone.FTPService.exe" | out-file $env:temp\installservice.log -Append
	}
	if (-NOT(Get-Service $maintenanceServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$maintenanceServiceName\GMG.CoZone.Maintenance.exe" | out-file $env:temp\installservice.log -Append
	}
	if (-NOT(Get-Service $softProofingDataServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$softProofingDataServiceName\GMG.CoZone.SoftProofingDataService.exe" | out-file $env:temp\installservice.log -Append
	}
	if (-NOT(Get-Service $bouncedEmailTrackerServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$bouncedEmailTrackerServiceName\GMG.CoZone.BouncedEmailTrackerService.exe" | out-file $env:temp\installservice.log -Append
	}
	if (-NOT(Get-Service $archiveCompletedJobsServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$archiveCompletedJobsServiceName\GMG.CoZone.ArchiveCompletedJobsService.exe" | out-file $env:temp\installservice.log -Append
	}
	if (-NOT(Get-Service $notificationGenerationServiceName -ErrorAction SilentlyContinue)){
		C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "$rootFolderPath$notificationGenerationServiceName\GMG.CoZone.NotificationGenerationService.exe" | out-file $env:temp\installservice.log -Append
	}

	# Start the services
	Start-Service -displayname $emailServiceName
	Start-Service -displayname $ftpServiceName
	Start-Service -displayname $maintenanceServiceName
	Start-Service -displayname $softProofingDataServiceName
	Start-Service -displayname $bouncedEmailTrackerServiceName
	Start-Service -displayname $archiveCompletedJobsServiceName	


	#Unzip and Install FTP Custom Provider in GAC
	$ftpCustomProviderName = "FTPCustomProvider"
	sz x $rootFolderPath$ftpCustomProviderSrcName -o"$rootFolderPath$ftpCustomProviderName" -aoa
	Set-location "C:\GMGServices\FTPCustomProvider"
	[System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	$publish = New-Object System.EnterpriseServices.Internal.Publish
	$publish.GacRemove("C:\GMGServices\FTPCustomProvider\GMG.CoZone.FTPCustomProvider.dll")
	$publish.GacInstall("C:\GMGServices\FTPCustomProvider\GMG.CoZone.FTPCustomProvider.dll")
	Restart-Service -Name "ftpsvc"
	Stop-Service -displayname $ftpServiceName	
	Start-Service -displayname $ftpServiceName
	iisreset	
</powershell>