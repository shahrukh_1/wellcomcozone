#TOFILL AWS credentials, region, bucket
<powershell>
    import-module "C:\Program Files (x86)\AWS Tools\PowerShell\AWSPowerShell\AWSPowerShell.psd1"
    #set credentials for powershell-user
    Set-AWSCredentials -AccessKey AKIAJVPLUQNEPXRN7BPQ -SecretKey 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
    set-DefaultAWSRegion ap-southeast-1
    
	#add high CPU alert
	$webclient = new-object System.Net.WebClient 
	$instanceID = $webclient.DownloadString("http://169.254.169.254/latest/meta-data/instance-id") 
	
	$highCPU = new-object Amazon.CloudWatch.Model.Dimension	
	$highCPU.Name = 'InstanceId'
	$highCPU.Value = $instanceID
	$alarmName = 'Web Instance High CPU: ' + $instanceID
	Write-CWMetricAlarm -AlarmName $alarmName -ActionsEnabled 1 -AlarmDescription 'Alert on EC2 CPU Utilization' -MetricName 'CPUUtilization' -Namespace 'AWS/EC2' -Statistic Average -Period 60 -Threshold 90 -ComparisonOperator GreaterThanOrEqualToThreshold -Dimensions $highCPU -EvaluationPeriods 1 -AlarmActions 'arn:aws:sns:ap-southeast-1:133862232599:web-server-high-cpu'
    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Starting bootstrap'

    $websiteLocation =  "$env:systemdrive\gmgcozone"
    $websiteAssetsLocation =  "$env:systemdrive\gmgcozone.zip"
    Write-Output "Installing website to $websiteLocation"
    mkdir $websiteLocation
	
     Write-EventLog -LogName Application -Source 'ec2config' -EntryType Information -EventId 300 -Message 'Copy S3 bucket archive'
	 
    #connect to s3 and download the website assets archive
    Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key "Production/CoZoneWeb.zip" -LocalFile $websiteAssetsLocation | out-file $env:temp/copy.log -Append
    
    #Unzip the website to destination folder
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    sz x $websiteAssetsLocation -oC:\gmgcozone -aoa
    #Delete the archive
    Remove-Item $websiteAssetsLocation	
	
	Write-EventLog -LogName Application -Source 'ec2config' -EntryType Information -EventId 300 -Message 'Copy S3 bucket archive completed'
	
	#connect to s3 and download the ClamAV archive
    Copy-S3Object -Region 'ap-southeast-1' -SourceBucket wellcom-cozone-builds -Key "Stage/ClamAV.zip" -LocalFile $clamdAssetsLocation | out-file $env:temp/copy.log -Append
    
    #Unzip the website to destination folder
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    sz x $clamdAssetsLocation -oC:\ClamAV -aoa
    #Delete the archive
    Remove-Item $clamdAssetsLocation	
	
	Write-EventLog -LogName Application -Source 'ec2config' -EntryType Information -EventId 300 -Message 'Copy ClamAV from S3 bucket archive completed'

	Start-Process 'C:\ClamAV\clamdinstallservice.bat'
	
    #Turn on asp.net state server
    Set-Service -name aspnet_state -StartupType Automatic
    Start-Service aspnet_state
  
    Write-EventLog -LogName Application -Source 'ec2config' -EntryType Information -EventId 300 -Message 'Configuring website'
    Import-Module WebAdministration
    
    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Remove default website'
    Remove-Website -Name "Default Web Site"

    New-Website -Name "GMGCoZone" -PhysicalPath $websiteLocation -ApplicationPool 'GMGCoZone' -Id '1'
    Set-ItemProperty 'IIS:\AppPools\GMGCoZone' -Name ProcessModel.IdentityType -value 0 

    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Starting GMGCoZone website'
    start-website -name "GMGCoZone"

    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Installation Completed'


    Write-EventLog -LogName Application -Source 'ec2config' -EntryType SuccessAudit -EventId 300 -Message 'Add SNS subscription for approvals progress'
    
    $webclient = new-object System.Net.WebClient 
	$instanceIPAddress = $webclient.DownloadString("http://169.254.169.254/latest/meta-data/public-ipv4") 
    Connect-SNSNotification -TopicARN arn:aws:sns:ap-southeast-1:133862232599:CoZoneMediaDocumentsProgressAP -Protocol HTTP -Endpoint http://$instanceIPAddress/SNSReceiver.ashx -Region ap-southeast-1

</powershell>